﻿using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class SalesForecastDataItem : MvxNotifyPropertyChanged
    {
        public SalesForecastDataItem ()
        {
            WeekTotalModified = false;    
        }

        private const string _READONLYCONSTANT = "r";

        public decimal m_weekTotal;
        public decimal WeekTotal
        { 
            get
            { 
                return m_weekTotal; 
            } 
            set
            {
                m_weekTotal = value;
                RaisePropertyChanged (() => WeekTotal);
            }
        }
        public bool WeekTotalModified { get; set; }
        public string ForecastGroupID { get; set; }
        public string AccessControlType { get; set; }
        public string MetricType { get; set; }
        public bool IsDerived { get; set; }
        public string ParentMetricID { get; set; }
        public int MetricSortOrder { get; set; }
        public string MetricName { get; set; }
        public string ForecastGroupName { get; set; }
        public string MetricID { get; set; }
        public List<SalesForecastDayItem> Days { get; set; }

        // for overview view, if -1 this will be true
        public bool IsOverview { get; set; }

        public void UpdateDaysForNewTotal(string total)
        {
            decimal? parsed = SalesForecastDayItem.CleanStringAndParse (total);

            if(parsed.HasValue)
            {
                decimal diff = parsed.Value - WeekTotal;

                // update all values
                if (WeekTotal > 0)
                {
                    for (int i = 0; i < Days.Count; i++)
                    {
                        decimal v = Days[i].DayValue.GetValueOrDefault ();

                        if (v > 0)
                        {
                            v += (v / WeekTotal) * diff;
                            Days[i].DayValue = v;
                            Days[i].Modified = true;
                        }
                    }

                    WeekTotalModified = true;
                }
            }
        }

        public void RecalculateTotal()
        {
            WeekTotal = Days.Sum (x => x.DayValue).GetValueOrDefault(0); 
        }

        private bool m_isEditable;
        public bool IsEditable
        {
            get
            {
                if (IsOverview)
                {
                    m_isEditable = false;
                }
                else
                {
                    m_isEditable = IsDerived || !AccessControlType.Equals (_READONLYCONSTANT);
                }
                return m_isEditable;
            }
            set
            {
                m_isEditable = value;
                RaisePropertyChanged (() => IsEditable);
            }
        }
    }
}

