﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class ResourcePlanningItem
    {
        [Flags]
        public enum OvertimeField 
        {
            None = 0, 
            OvertimeRequestedDay = 1,
            OvertimeRequestedNight = 2,
            OvertimeAgreedDay = 4,
            OvertimeAgreedNight = 8,
        }

        [JsonIgnore]
        public Action<ResourcePlanningItem,OvertimeField, float> OnUpdated  { get; set; }

        public int DepartmentId { get; set; }
        public DateTime ForDate { get; set; }
        public string Department { get; set; }

        public double ContractedHours { get; set; }
        public double PlannedTimeOff { get; set; }
        public double PlannedHours { get; set; }
        public double ScheduledHours { get; set; }
        public double UnfilledHours { get; set; }
        public bool ShouldFlag { get; set; }

        public bool IsWeekCell { get; set; }
        
        [JsonIgnore]
        /// <summary>
        /// Timeoff excluding holiday
        /// </summary>
        /// <value>The unplanned time off.</value>
        public double UnplannedTimeOff { get; set; }

        [JsonIgnore]
        public double? OvertimeRequestedDay { get; set; }
        [JsonIgnore]
        public double? OvertimeRequestedNight { get; set; }
        [JsonIgnore]
        public double OvertimeRequested { get { return OvertimeRequestedDay.GetValueOrDefault() + OvertimeRequestedNight.GetValueOrDefault(); } set { } }
        [JsonIgnore]
        public double? OvertimeAgreedDay { get; set; }
        [JsonIgnore]
        public double? OvertimeAgreedNight { get; set; }
        [JsonIgnore]
        public double OvertimeAgreed { get { return OvertimeAgreedDay.GetValueOrDefault() + OvertimeAgreedNight.GetValueOrDefault(); } set { } }
        [JsonIgnore]
        public OvertimeField DirtyFields { get; set; }

        [JsonIgnore]
        public bool CanSetOvertimeAgreed { get { return IsWeekCell; } set { } }

        public ResourcePlanningItem()
        {
        }

        public ResourcePlanningItem(ResourcePlanningItem fromItem)
        {
            this.ContractedHours = fromItem.ContractedHours;
            this.PlannedTimeOff = fromItem.PlannedTimeOff;
            this.UnplannedTimeOff = fromItem.UnplannedTimeOff;
            this.PlannedHours = fromItem.PlannedHours;
            this.ScheduledHours = fromItem.ScheduledHours;
            this.UnfilledHours = fromItem.UnfilledHours;
            this.ShouldFlag = fromItem.ShouldFlag;
        }

        public void EditRequestedHoursDay()
        {
            Mvx.Resolve<IAlertBox>().ShowInput
            (
                "Set Hours Overtime Requested For " + Department, 
                ForDate.ToString(Mvx.Resolve<IStringService>().Get("dd/MM/yyyy")) + " (Day)", 
                KeyboardType.Numeric, 
                OvertimeRequestedDay.ToString(), 
                "OK", 
                "Cancel", (int x, string s) => HandleRequestedValueChanged(x,s, OvertimeField.OvertimeRequestedDay)
                );
        }

        public void EditRequestedHoursNight()
        {
            Mvx.Resolve<IAlertBox>().ShowInput
            (
                "Set Hours Overtime Requested For " + Department, 
                ForDate.ToString(Mvx.Resolve<IStringService>().Get("dd/MM/yyyy")) + " (Night)", 
                KeyboardType.Numeric, 
                OvertimeRequestedNight.ToString(), 
                "OK", 
                "Cancel",(int x, string s) => HandleRequestedValueChanged(x,s, OvertimeField.OvertimeRequestedNight)
            );
        }

        public void EditAgreedHoursDay()
        {
            Mvx.Resolve<IAlertBox>().ShowInput
            (
                "Set Hours Overtime Agreed For " + Department, 
                ForDate.ToString(Mvx.Resolve<IStringService>().Get("dd/MM/yyyy"))+ " (Day)", 
                KeyboardType.Numeric, 
                OvertimeAgreedDay.ToString(), 
                "OK", 
                "Cancel",(int x, string s) => HandleRequestedValueChanged(x,s, OvertimeField.OvertimeAgreedDay)
            );
        }

        public void EditAgreedHoursNight()
        {
            Mvx.Resolve<IAlertBox>().ShowInput
            (
                "Set Hours Overtime Agreed For " + Department, 
                ForDate.ToString(Mvx.Resolve<IStringService>().Get("dd/MM/yyyy"))+ " (Night)", 
                KeyboardType.Numeric, 
                OvertimeAgreedNight.ToString(), 
                "OK", 
                "Cancel",(int x, string s) => HandleRequestedValueChanged(x,s, OvertimeField.OvertimeAgreedNight)
            );
        }

        void HandleRequestedValueChanged(int buttonSelected, string valueReturned, OvertimeField forField)
        {
            if(buttonSelected == 1)
            {
                float parsed = 0;

                if (float.TryParse(valueReturned, out parsed))
                {
                    DirtyFields |= forField;

                    if (OnUpdated != null)
                        OnUpdated(this, forField, parsed);
                }
            }
        }
    }
}