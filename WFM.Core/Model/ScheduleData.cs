﻿using System;
using System.Linq;
using System.Collections.Generic;
using Cirrious.CrossCore;
using AutoMapper;
using Newtonsoft.Json.Linq;

namespace WFM.Core
{
    public class ScheduleData
    {
        public const int c_sortName = -1;
        public const int c_sortHour = -2;
        public const int c_sortEarliestStart = -3;
        public const int c_unsorted = -4;

        public List<EmployeeData> FilteredEmployees { get { return Employees.Where(employee => !employee.VisibleInFilter).ToList(); } } 
        public List<EmployeeData> Employees { get; private set; }

        public List<DayInfo> Days { get; private set; }

        public int? StartDay { get; private set; }

        public int? DayIndex 
        {
            get; 
            private set; 
        }

        public DateTime StartDate { get; private set; }
        public double[] DayScheduled { get; private set; }
        public double[] DayCost { get; private set; }
        public double WeekScheduled { get; private set; }
        public double WeekCost { get; private set; }
        public bool IsModified { get; set; }
        public FilterGroup CurrentFilter { get; set; }
        public JDABusinessUnitSettings BUSettings { get; set; }
        public List<JDAScheduleRule> ScheduleRules { get; set; }

        public List<JDAShift> UnfilledShifts { get; private set; }
        public List<JDAShift> PendingFilledShifts { get; private set; }
        public List<JDAShift> PendingUnfilledShifts { get; private set; }
        public List<JDAShift> DeletedUnfilledShifts { get; private set; }

        public List<JDAShift> Shifts { get; private set; }
        public List<JDAJobAssignment> Jobs { get; private set; }
        public Dictionary <int, double>[] JobHours { get; private set; }

        private readonly List<JDAEmployee> m_sheduleEmployees;
        private readonly JDAScheduleData m_scheduleData;
        private readonly Dictionary<int, JDAEmployeeInfo> m_employeeLookup;
        private readonly IESSConfigurationService m_settings;

        public ScheduleData(JDAScheduleData data, Dictionary<int, JDAEmployeeInfo> employeeLookup, FilterGroup filter)
        {
            IsModified = false;
            StartDay = data.BUSettings.FirstDayOfWeekID;
            StartDate = data.DefaultStartDate;
            Employees = new List<EmployeeData>();
            UnfilledShifts = new List<JDAShift>(data.UnfilledShifts);
            PendingUnfilledShifts = new List<JDAShift>();
            PendingFilledShifts = new List<JDAShift>();
            DeletedUnfilledShifts = new List<JDAShift>();
            Shifts = new List<JDAShift>();
            Jobs = new List<JDAJobAssignment>();
            JobHours = new Dictionary<int, double>[7];

            m_scheduleData = data;
            m_sheduleEmployees = data.Employees;
            m_employeeLookup = employeeLookup;

            m_settings = Mvx.Resolve<IESSConfigurationService> ();
            CurrentFilter = filter;
            BuildEmployeeList();
            ApplyCurrentFilter();

            CalculateMetrics ();
        }

        void BuildEmployeeList()
        {
            foreach (var e in m_sheduleEmployees)
            {
                //Map SiteName
                e.Shifts = e.Shifts.Select (c =>
                    {
                        var site = m_scheduleData.Sites.FirstOrDefault (x => x.SiteID == c.SiteID); 
                        c.SiteName = (site != null) ? site.SiteName : "Unknown"; 
                        return c;
                    }).ToList ();
                
                var row = new EmployeeData (e, m_scheduleData.JobRoles, StartDate);

                // Lookup up employee info
                JDAEmployeeInfo info = null;
                if (m_employeeLookup.TryGetValue (e.EmployeeID, out info))
                    row.Info = info;

                // Calculate metrics for this employee

                BUSettings = m_scheduleData.BUSettings;
                BUSettings.BusinessUnitOffset = new DateTimeOffset (BUSettings.LocalBusinessUnitTime);
                ScheduleRules = m_scheduleData.ScheduleRules;

                row.CalculateMetrics(BUSettings, ScheduleRules);
                row.CalculateAllowableJobs(m_scheduleData.JobRoles);

                Shifts.AddRange(e.Shifts); 

                e.Shifts.All(shift =>
                    {
                        shift.Jobs.All(job =>
                            {
                                Jobs.Add(job);
                                return true; 
                            });
                        return true;
                    });

                Employees.Add(row);
            }
        }

        void ApplyCurrentFilter()
        {
            foreach (EmployeeData employee in Employees)
            {
                employee.VisibleInFilter = ShouldFilterEmployee(employee);
            }
        }

        bool ShouldFilterEmployee(EmployeeData employee)
        {
            // Using custom schedule request so filtering is already applied.
            if (m_settings.UseCustomScheduleRequest)
                return false;

            if (CurrentFilter != null)
            {
                string currentFilterType = CurrentFilter.Type;
                int[] selectedIndexes = CurrentFilter.GetSelectedList();

                var employeeFilter = CurrentFilter as IEmployeeFilterGroup;

                var scheduledFilter = CurrentFilter.MemoryFilter as IEmployeeFilterGroup;

                if (employeeFilter != null)
                {
                    if (!employeeFilter.ShouldApplyFilter(employee,currentFilterType,selectedIndexes, DayIndex))
                    {
                        return true;
                    }
                }

                if (!scheduledFilter.ShouldApplyFilter(employee, currentFilterType,selectedIndexes, DayIndex))
                {
                    return true;
                }
            }

            return false;
        }

        public void SetDay(int? dayIndex)
        {
            DayIndex = dayIndex;

            ApplyCurrentFilter();

            foreach (var e in Employees)
                e.DayIndex = dayIndex;
        }

        public void SetEdit(bool canEdit)
        {
            foreach (var e in Employees)
                e.CanEdit = canEdit;
        }

        public void CalculateMetrics()
        {
            DayScheduled = new double[7];
            DayCost = new double[7];
            WeekScheduled = 0;
            WeekCost = 0;

            // Add up totals
            for (int i = 0; i < 7; ++i)
            {
                foreach (var e in Employees)
                {
                    DayHoursData shiftHours = e.CalculateShiftHoursAndCostForFilter(i, CurrentFilter, ScheduleRules, BUSettings);
                    DayScheduled [i] +=  shiftHours.Hours;
                    DayCost [i] += shiftHours.Cost;
                }
                WeekScheduled += DayScheduled [i];
                WeekCost += DayCost [i];
            }
        }

        public void SortEarliestStart(int? dayIndex, bool sortAscending)
        {
            List<EmployeeData>[] splitEmployees = SplitEmployeesByWorkingAndTimeOff(dayIndex);

            List<EmployeeData> orderedEmployees = new List<EmployeeData>();

            if(dayIndex != null) // Day
            {
                int dayIndexInt = (int) dayIndex;

                if (sortAscending)
                {
                    orderedEmployees = splitEmployees[0]
                        .OrderBy(x => ShiftOverlaps(x, dayIndexInt))
                        .ThenBy (x => x.Days [dayIndexInt].EarliestStart)
                        .ThenBy (x => x.Name)
                        .ThenBy(x => x.Hours)
                        .ToList ();

                    orderedEmployees
                        .AddRange(splitEmployees[1]
                            .OrderBy (x => x.Days [dayIndexInt].EarliestStart)
                            .ThenBy (x => x.Name)
                            .ThenBy(x => x.Hours));  // Append time off employees

                    orderedEmployees
                        .AddRange(splitEmployees[2]
                            .OrderBy (x => x.Days [dayIndexInt].EarliestStart)
                            .ThenBy (x => x.Name)
                            .ThenBy(x => x.Hours));  // Append time off employees
                }
                else
                {
                    orderedEmployees = splitEmployees[0]
                        .OrderByDescending(x => ShiftOverlaps(x, dayIndexInt))
                        .ThenByDescending (x => x.Days [dayIndexInt].EarliestStart)
                        .ThenBy (x => x.Name)
                        .ThenBy(x => x.Hours)
                        .ToList();

                    orderedEmployees
                        .AddRange(splitEmployees[1]
                            .OrderByDescending (x => x.Days [dayIndexInt].EarliestStart)
                            .ThenBy (x => x.Name) 
                            .ThenBy(x => x.Hours)); // Append time off employees

                    orderedEmployees
                        .AddRange(splitEmployees[2]
                            .OrderByDescending (x => x.Days [dayIndexInt].EarliestStart)
                            .ThenBy (x => x.Name) 
                            .ThenBy(x => x.Hours)); // Append time off employees
                }
            }
            else // Week
            {
                // TODO Not yet implemented
            }

            Employees = orderedEmployees;
        }

        int ShiftOverlaps (EmployeeData arg, int dayIndex)
        {
            var currentDay = (dayIndex + StartDay - 1) % 7;
            var previousDay = ((dayIndex - 1)) % 7;
            List<ShiftData> shifts = new List<ShiftData>();
            if(previousDay < 0)
            {
                // Need to test previous week!
                shifts = new List<ShiftData>(arg.LastDayPreviousWeek.Shifts);
            }
            else
            {
                shifts = new List<ShiftData>(arg.Days[(int)previousDay].Shifts);
            }
            
            if(shifts.Any(x => (int)x.End.DayOfWeek == currentDay && (x.End - x.End.Date).TotalSeconds > 0))
            {
                return 0;
            }

            return 1;
        }

        public void SortNames(int? dayIndex, bool sortAscending)
        {
            List<EmployeeData> orderedEmployees = new List<EmployeeData>();

            if(dayIndex != null) // Day
            {
                int dayIndexInt = (int) dayIndex;

                if (sortAscending)
                {
                    orderedEmployees = Employees
                        .OrderBy (x => x.Name)
                        .ThenBy (x => x.Days [dayIndexInt].EarliestStart)
                        .ThenBy(x => x.Hours)
                        .ToList ();
                }
                else
                {
                    orderedEmployees = Employees
                        .OrderByDescending (x => x.Name)
                        .ThenBy (x => x.Days [dayIndexInt].EarliestStart)
                        .ThenBy(x => x.Hours)
                        .ToList ();
                }
            }
            else // Week
            {
                if (sortAscending)
                {
                    orderedEmployees = Employees
                        .OrderBy (x => x.Name)
                        .ThenBy(x => x.Hours)
                        .ToList ();
                }
                else
                {
                    orderedEmployees = Employees
                        .OrderByDescending (x => x.Name)
                        .ThenBy(x => x.Hours)
                        .ToList ();
                }
            }

            Employees = orderedEmployees;
        }

        public void SortHours(int? dayIndex, bool sortAscending)
        {
            List<EmployeeData>[] splitEmployees = SplitEmployeesByTimeOff(dayIndex);
            List<EmployeeData> orderedEmployees = new List<EmployeeData>();

            if(dayIndex != null) // Day
            {
                int dayIndexInt = (int) dayIndex;
            
                if (sortAscending)
                {
                    orderedEmployees = splitEmployees[0]
                        .OrderBy (x => x.Hours)
                        .ThenBy(x => x.Name)
                        .ThenBy(x => x.Days [dayIndexInt].EarliestStart)
                        .ToList ();

                    orderedEmployees
                        .AddRange(splitEmployees[1]
                        .OrderBy (x => x.Hours)
                        .ThenBy(x => x.Name)
                        .ThenBy(x => x.Days [dayIndexInt].EarliestStart)); // Append time off employees
                }
                else
                {
                    orderedEmployees = splitEmployees[0]
                        .OrderByDescending (x => x.Hours)
                        .ThenBy(x => x.Name)
                        .ThenBy (x => x.Days [dayIndexInt].EarliestStart)
                        .ToList ();

                    orderedEmployees
                        .AddRange(splitEmployees[1]
                        .OrderByDescending (x => x.Hours)
                        .ThenBy (x => x.Name) 
                        .ThenBy (x => x.Days [dayIndexInt].EarliestStart)); // Append time off employees
                }
            }
            else // Week
            {
                if (sortAscending)
                {
                    orderedEmployees = splitEmployees[0]
                        .OrderBy (x => x.Hours)
                        .ThenBy (x => x.Name)
                        .ToList ();

                    orderedEmployees
                        .AddRange (splitEmployees[1]
                        .OrderBy (x => x.Hours)
                        .ThenBy (x => x.Name)); // Append time off employees

                }
                else
                {
                    orderedEmployees = splitEmployees[0]
                        .OrderByDescending (x => x.Hours)
                        .ThenBy(x => x.Name)
                        .ToList ();

                    orderedEmployees
                        .AddRange(splitEmployees[1]
                        .OrderByDescending (x => x.Hours)
                        .ThenBy(x => x.Name)); // Append time off employees
                }
            }

            Employees = orderedEmployees;
        }

        private List<EmployeeData>[] SplitEmployeesByWorkingAndTimeOff(int? dayIndex)
        {
            List<EmployeeData>[] employeesSplitByTimeOff = new List<EmployeeData>[3];
            employeesSplitByTimeOff[0] = new List<EmployeeData>(); // Working employees
            employeesSplitByTimeOff[1] = new List<EmployeeData>(); // No Shift
            employeesSplitByTimeOff[2] = new List<EmployeeData>(); // Time off employees


            foreach(var employee in Employees)
            {
                bool employeeOff = false;
                bool employeeUnscheduled = false;

                if (dayIndex == null)
                {
                    employeeUnscheduled = employee.Days.All(day => day.Hours == 0);
                }
                else
                {
                    employeeUnscheduled = employee.Days.Where(day => (int)day.DayStart.DayOfWeek == (dayIndex.Value + StartDay - 1) % 7).All(day => day.Hours == 0);
                }

                foreach(var day in employee.Days)
                {
                    if(dayIndex == null || (int) day.DayStart.DayOfWeek == (dayIndex + StartDay - 1) % 7)
                    {
                        foreach(var unavail in day.UnavailabilityRanges)
                        {
                            if(unavail.Type == "t")
                            {
                                employeeOff = true;
                            }
                        }
                    }

                    //check for previous day
                    if(dayIndex != null)
                    {
                        var currentDay = (dayIndex + StartDay - 1) % 7;
                        var previousDay = ((dayIndex - 1) + StartDay - 1) % 7;
                        
                        List<ShiftData> shifts = new List<ShiftData>(day.Shifts);
                        
                        if(day.DayStart.DayOfWeek == (DayOfWeek)currentDay)
                        {
                            shifts.AddRange(employee.LastDayPreviousWeek.Shifts);
                        }
                        var overlaps = day.Shifts.Where(s => (int)s.Start.DayOfWeek == previousDay && (int)s.End.DayOfWeek == currentDay).ToList();
                        if(overlaps != null && overlaps.Any())
                        {
                            employeeUnscheduled = false;
                            employeeOff = false;
                        }
                    }
                }



                if (employeeOff)
                {
                    employeesSplitByTimeOff[2].Add(employee);
                }
                else if (employeeUnscheduled)
                {
                    employeesSplitByTimeOff[1].Add(employee);
                }
                else
                {
                    employeesSplitByTimeOff[0].Add(employee);
                }


            }

            return employeesSplitByTimeOff;
        }

        private List<EmployeeData>[] SplitEmployeesByTimeOff(int? dayIndex)
        {
            List<EmployeeData>[] employeesSplitByTimeOff = new List<EmployeeData>[2];
            employeesSplitByTimeOff[0] = new List<EmployeeData>(); // Working employees
            employeesSplitByTimeOff[1] = new List<EmployeeData>(); // Time off employees

            foreach(var employee in Employees)
            {
                bool employeeOff = false;

                foreach(var day in employee.Days)
                {
                    if(dayIndex == null || (int) day.DayStart.DayOfWeek == (dayIndex + StartDay - 1) % 7)
                    {
                        foreach(var unavail in day.UnavailabilityRanges)
                        {
                            if(unavail.Type == "t")
                            {
                                employeeOff = true;
                            }
                        }
                    }
                }

                if(employeeOff)
                {
                    employeesSplitByTimeOff[1].Add(employee);
                }
                else
                {
                    employeesSplitByTimeOff[0].Add(employee);
                }
            }

            return employeesSplitByTimeOff;
        }

        public void ClearTimeOff(int employeeID, DateTime day)
        {
            var employee = Employees.First (x => x.EmployeeID == employeeID);
            if (employee == null)
                return;

            foreach(var employeeDay in employee.Days)
            {
                if(employeeDay.DayStart.Date == day.Date)
                {
                    employeeDay.UnavailabilityRanges.Clear();
                }
            }
        }

        public void AssignUnfilledShift(JDAShift shift, EmployeeData employee)
        {
            IsModified = true;

            employee.AssignToUnfilledShift(shift);

            PendingFilledShifts.Add(shift);
            UnfilledShifts.Remove(shift);
        }

        public void UnfillShift(ShiftData shiftData, JDAShift shift, EmployeeData employee)
        {
            IsModified = true;

            employee.RemoveFromFilledShift(shiftData);

            foreach(var jdaShift in PendingFilledShifts)
            {
                if (shift != null && jdaShift.ShiftID == shift.ShiftID)
                {
                    PendingFilledShifts.Remove(jdaShift);
                    PendingUnfilledShifts.Add(jdaShift);
                    UnfilledShifts.Add(jdaShift); // HACK shady way of adding responsive behaviour without data request
                    break;
                }
            }

            if (shift != null)
            {
                UnfilledShifts.Add(shift); // If shift is not in pendingfilledshifts, add shift to unfilledshifts.
                PendingUnfilledShifts.Add(shift);
            }

            UnfilledShifts = UnfilledShifts
                .OrderBy(s => s.Start)
                .ThenBy(s => (s.End - s.Start).TotalHours)
                .ThenBy(s => s.Jobs.FirstOrDefault() == null ? string.Empty : s.Jobs.FirstOrDefault().JobName)
                .ToList();
        }

        public void UnfillUnsavedShift (ShiftData shiftData, EmployeeData employee)
        {
            IsModified = true;

            employee.RemoveFromFilledShift(shiftData);

            foreach(var jdaShift in PendingFilledShifts)
            {
                PendingFilledShifts.Remove(jdaShift);
                PendingUnfilledShifts.Add(jdaShift);
                UnfilledShifts.Add(jdaShift); // HACK shady way of adding responsive behaviour without data request
                break;
            }

            UnfilledShifts = UnfilledShifts
                .OrderBy(shift => shift.Start)
                .ThenBy(s => (s.End - s.Start).TotalHours)
                .ThenBy(s => s.Jobs.FirstOrDefault() == null ? string.Empty : s.Jobs.FirstOrDefault().JobName)
                .ToList();
        }

        public void UndoFillShift(ShiftData shiftData, JDAShift shift, EmployeeData employee)
        {
            foreach(var jdaShift in PendingFilledShifts)
            {
                if (shift != null && jdaShift.ShiftID == shift.ShiftID)
                {
                    PendingFilledShifts.Remove(jdaShift);
                    UnfilledShifts.Add(jdaShift); // HACK shady way of adding responsive behaviour without data request
                    break;
                }
            }

            UnfilledShifts = UnfilledShifts.OrderBy(unfilled => unfilled.Start).ToList();
        }

        public void DeleteUnfilledShift(JDAShift shift)
        {
            // Set Entire schedule as modified
            IsModified = true;

            DeletedUnfilledShifts.Add(shift);
            UnfilledShifts.Remove(shift);
        }

        public void UnfilledShiftSelect(JDAShift shift)
        {
        }

        public void ResetPendingUnfilledShifts()
        {
            PendingUnfilledShifts = new List<JDAShift>();
        }

        public double CalculateWorkingHoursInShift(JDAShift shift)
        {
            double shiftHours = 0.0;

            foreach (var job in shift.Jobs)
            {
                var jobHours = (job.End - job.Start).TotalHours;
                shiftHours += jobHours;
            }

            return shiftHours;
        }

        public EmployeeData GetEmployee(int employeeId)
        {
            return Employees.Where(x => x.EmployeeID == employeeId).FirstOrDefault();
        }

   }
}