﻿namespace WFM.Core
{
    public class ESSConfigurationItem <T>
    {
        public string AppConfig { get; set ;}
        public string JDAConfig { get; set ;}
        public T Default { get; set ;}

        public ESSConfigurationItem ( string appConfig, T defaultValue, string jdaConfig = "" )
        {
            AppConfig = appConfig;
            Default = defaultValue;
            JDAConfig = jdaConfig;
        }
    }
}

