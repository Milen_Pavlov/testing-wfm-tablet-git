﻿using System;
using AutoMapper;

namespace WFM.Core
{
    public class ShiftDetailData
    {
        public const string c_breakID = "b";
        public const string c_mealID = "m";
        public const string c_roleID = "r";

        public int ShiftDetailID { get; set; }
        public int ScheduledJobID { get; set; }
        public string PunchCode { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        
        public TimeSpan DurationSpan
        {
            get
            {
                return (Start.HasValue && End.HasValue) ? (End.Value - Start.Value) : TimeSpan.Zero; 
            }
        }
        
        public EditState Edit { get; set; }

        public DateTime? OriginalStart { get; set; }
        public DateTime? OriginalEnd { get; set; }
        public int OriginalScheduledJobID { get; set; }

        public int RoleId { get; set; }

        public bool IsBreak { get { return PunchCode == c_breakID; } }
        public bool IsMeal { get { return PunchCode == c_mealID; } }
        public bool IsRole {get { return PunchCode == c_roleID; } }
        public double Duration { get { return DurationSpan.TotalMinutes; } }

        public static ShiftDetailData Create(int uniqueID)
        {
            return new ShiftDetailData()
            {
                ShiftDetailID = uniqueID,
                Edit = EditState.Added,
                PunchCode = c_breakID
            };
        }

        public static ShiftDetailData CreateRoleDetail(JDAShiftDetail role, EditState editState = EditState.None)
        {
            return new ShiftDetailData
            {
                Edit = editState,
                PunchCode = c_roleID,
                ShiftDetailID = role.ShiftDetailID,
                Start = role.Start,
                End = role.End,
                ScheduledJobID = role.ScheduledJobID,
                RoleId = role.RoleID
            };
        }

        public void SetBreak() 
        { 
            PunchCode = c_breakID; 
        }

        internal void SetRole ()
        {
            PunchCode = c_roleID;
        }

        public void SetMeal() 
        { 
            PunchCode = c_mealID; 
        }

        public ShiftDetailData Copy()
        {
            ShiftDetailData newData = Mapper.Map<ShiftDetailData,ShiftDetailData> (this);
            newData.SetOriginalValues();
            return newData;
        }

        public void Move(TimeSpan offset)
        {
            if (offset.TotalMinutes == 0)
                return;

            if (Edit != EditState.Added)
                Edit = EditState.Modified;

            Start = Start.Value.Add(offset);
            End = End.Value.Add(offset);
        }

        public void Resize(DateTime start, DateTime end)
        {
            // Nothing to do?
            if (start == Start.Value && end == End.Value)
                return;

            if (Edit != EditState.Added)
                Edit = EditState.Modified;

            Start = start;
            End = end;
        }

        public void SetOriginalValues ()
        {
            OriginalStart = Start;
            OriginalEnd = End;
            OriginalScheduledJobID = ScheduledJobID;
        }

   }
}
