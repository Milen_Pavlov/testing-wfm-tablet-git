﻿using System;
namespace WFM.Core
{
    public class TermsOfUseConfig
    {
        public int Version { get; set; }
        public string TextFileLocation { get; set; }

        public TermsOfUseConfig ()
        {
        }
    }
}

