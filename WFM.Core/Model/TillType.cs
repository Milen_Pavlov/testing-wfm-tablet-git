﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Model
{
    public class TillType
    {
        #region Core properties

        // Properties
        public int TillTypeId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public int OpeningSequence { get; set; }

        // Parents

        // Children
        public IList<Till> Tills { get; set; }

        public void Add(Till till)
        {
            if (Tills == null)
            {
                Tills = new List<Till>();
            }

            Tills.Add(till);
            till.Type = this;
            till.TypeId = TillTypeId;
        }

        public void Add(IList<Till> tills)
        {
            foreach (var till in tills)
            {
                Add(till);
            }
        }

        #endregion
    }
}
