﻿using System;

namespace WFM.Core
{
    public class SelectedPickerItem
    {
        public DateTime Date { get; set; }
        public bool IsWeek { get; set; }

        public SelectedPickerItem()
        {
        }
    }
}

