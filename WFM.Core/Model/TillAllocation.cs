﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Model
{
    public class TillAllocation
    {
		public const string BufferTillType = "Buffer";
        public const string SSCSAYSTillType = "SSCSAYS";
		public const int NewBufferTillId = -99999;

        #region Core properties

        // Properties
        public int TillAllocationId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }

		public int? StampedTillId { get; set; }
        public int? TillId { get; set; }
        public string TillNumber { get; set; }
        public bool CheckoutSupport { get; set; }
        public string TillTypeKey { get; set; }
        public string TillStatusKey { get; set; }
        public int? TillOpeningSequence { get; set; }
        public int? TillDisplaySequence { get; set; }

        public bool IsSSCSAYS { get { return TillTypeKey == SSCSAYSTillType; } }

        // Parents
        public Till Till { get; set; }

        // Children
        public IList<TillAllocationTillWindow> TillWindows { get; set; }

        public void Add(TillAllocationTillWindow tillWindow)
        {
            if (TillWindows == null)
            {
                TillWindows = new List<TillAllocationTillWindow>();
            }

            TillWindows.Add(tillWindow);
            tillWindow.TillAllocation = this;
            tillWindow.TillAllocationId = TillAllocationId;
        }

        public void Add(IList<TillAllocationTillWindow> tillWindows)
        {
            foreach (var tillWindow in tillWindows)
            {
                Add(tillWindow);
            }
        }

        #endregion
    }
}
