﻿using System;

namespace WFM.Core
{
    public enum EditState
    {
        None = 0,
        Modified,
        Added,
        Deleted
    }
}

