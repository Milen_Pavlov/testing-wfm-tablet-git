﻿namespace WFM.Core
{
    public class MinHoursBetweenShiftsResult
    {
        public bool CreateWarning { get; set; }
        public double HoursDifference { get; set; }
    }
}

