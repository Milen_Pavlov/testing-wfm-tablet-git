﻿using System;

namespace WFM.Core
{
    public class DepartmentSummary
    {
        string DepartmentName { get; set; }
        int DepartmentID { get; set; }

        public DailySummary[] Summaries { get; set; }

        //TODO: Can this be auto calculated?
        public Summary TotalSummary { get; set; }
    }

    public class DailySummary 
    {
        public bool IsTotalRow { get; set; }

        public DateTime ForDate { get; set; }

        public Summary DaySummary { get; set; }
        public Summary NightSummary { get; set; }

        //TODO: Can this be auto calculated?
        public Summary TotalSummary { get; set; }
    }

    public class Summary
    {
        public float? SalesForecast { get; set; }
        public float LaborHours { get; set; }
        public float ScheduledHours { get; set;} 
        public float Coverage { get; set; }
        public float Accuracy { get; set; }
        public float LabourCost { get; set; }
        public float CostVariance { get; set; }
    }
}

