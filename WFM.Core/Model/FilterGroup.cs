﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace WFM.Core
{
    public class FilterGroup
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public List<FilterItem> Items { get; set; }

        public ScheduledFilterGroup MemoryFilter { get; set; }

        public FilterGroup()
        {
            Items = new List<FilterItem>();
        }

        public virtual FilterGroup Clone()
        {
            var group = new FilterGroup () {
                Name = string.Format("{0}", this.Name),
                Type = string.Format("{0}", this.Type),
                Items = new List<FilterItem>()
            };

            foreach (var item in Items)
                group.Items.Add(item.Clone());

            if (MemoryFilter != null)
                group.MemoryFilter = (ScheduledFilterGroup)MemoryFilter.Clone();

            return group;
        }

        public int[] GetSelectedList()
        {
            return Items.Where (x => x.Selected).Select (x => x.ID).ToArray();
        }

        public string GetDescription()
        {
            return string.Join(", ", Items.Where (x => x.Selected).Select (x => x.Name));
        }
    }
}
