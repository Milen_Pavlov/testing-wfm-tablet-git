﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Model
{
    public class StaticData
    {
        public IList<TillStatus> TillStatuses { get; set; }
        public IList<TillType> TillTypes { get; set; }
    }
}
