﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class EmployeeDayData
    {
        private readonly List<ShiftData> m_shifts = new List<ShiftData>();

        public EmployeeDayData(List<ShiftData> shifts)
        {
            EarliestStart = DateTime.MaxValue;
            UnavailabilityRanges = new List<JDAInterval> ();
            FixedShifts = new List<JDAEmployeeAssignedJob> ();
            JobTypeHours = new Dictionary<int, double>();
            m_shifts = shifts;
        }

        public Dictionary<int, double> JobTypeHours { get; set; }
        public double Hours { get; set; }
        public double Cost { get; set; }
        public DateTime DayStart { get; set; }
        public DateTime DayEnd 
        { 
            get  
            {
                return DayStart.AddDays(1);
            }
        }
        
        public DateTime EarliestStart { get; set; }
        
        public IList<ShiftData> Shifts 
        { 
            get
            {
                return m_shifts.Where(shift => shift.Start >= DayStart && shift.Start < DayEnd).ToList();
            }
        }

        internal bool TryUpdateShift (ShiftData shift)
        {
            var existingShift = m_shifts.FirstOrDefault(s => s.ShiftID == shift.ShiftID);

            if(existingShift != null)
            {
                var indexOfExistingShift = m_shifts.IndexOf(existingShift);
                m_shifts[indexOfExistingShift] = shift;
                m_shifts[indexOfExistingShift].Edit = EditState.Modified;
                m_shifts[indexOfExistingShift].Modified = true;

                return true;
            }

            return false;
        }
        
        public List<JDAInterval> UnavailabilityRanges { get; set; }
        public List<JDAEmployeeAssignedJob> FixedShifts { get; set; }

   }
}