﻿using System;

namespace WFM.Core
{
    public class WarningData
    {
        public int ExceptionDefinitionID { get; set; }
        public DateTime WeekStartBusinessDate { get; set; }
        public DateTime? BusinessDate { get; set; }
        public bool InDB { get; set; }
        public bool Display { get; set; }
        public EditState Edit { get; set; }
        public double HoursDiffrence { get; set;}
    }
}
