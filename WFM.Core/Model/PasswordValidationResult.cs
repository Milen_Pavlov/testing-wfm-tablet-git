﻿using System;
namespace WFM.Core
{
    public class PasswordValidationResult
    {
        public bool IsValid { get; set; }

        public string ValidationMessage { get; set; }
    }
}

