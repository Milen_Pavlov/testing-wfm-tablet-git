﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using System.Linq;

namespace WFM.Core
{
    public class FilterData
    {
        public const string WORKGROUP_FILTER_CODE = "w";
        public const string DEPARTMENT_FILTER_CODE = "d";
        public const string ATTRIBUTES_FILTER_CODE = "a";
        public const string LABORROLE_FILTER_CODE = "l";
        public const string JOB_FILTER_CODE = "j";

        public List<FilterGroup> Groups { get; private set; }

        public FilterData(
            JDAScheduleData data, 
            List<JDANameAndID> workgroups, 
            List<JDANameAndID> departments,
            List<JDAEmployeeAttribute> attributes, 
            List<JDACustomJob> customJobs,
            List<JDACustomRole> customRoles,
            SessionData sessionData,
            IESSConfigurationService config)
        {        
            IStringService localiser = Mvx.Resolve<IStringService>();
            Groups = new List<FilterGroup> ();

            bool filterWorkgroups = true;//Appears to always just be true
            bool filterAttributes = !config.JDAHideAttributes;;
            bool filterDepartments = config.JDAEnableDepartmentFiltering;
            bool filterJobsCustom = customJobs != null;
            bool filterJobsClassic = true;//Our built in fallback
            bool filterRolesCustom = customRoles != null;

            //Overrides
            if (config.CanFilterByAttribute.HasValue)
                filterAttributes = config.CanFilterByAttribute.Value;

            if (config.CanFilterByDepartment.HasValue)
                filterDepartments = config.CanFilterByDepartment.Value;

            if (config.CanFilterByJobClassic.HasValue)
                filterJobsClassic = config.CanFilterByJobClassic.Value;

            if (config.CanFilterByJobCustom.HasValue)
                filterJobsCustom = config.CanFilterByJobCustom.Value;

            if (config.CanFilterByRoleCustom.HasValue)
                filterRolesCustom = config.CanFilterByRoleCustom.Value;

            if (config.CanFilterByWorkgroup.HasValue)
                filterWorkgroups = config.CanFilterByWorkgroup.Value;

            if (filterWorkgroups)
            {
                if (workgroups != null)
                {
                    var wgGroup = new FilterGroup () { 
                        Items = new List<FilterItem> (),
                        Type = WORKGROUP_FILTER_CODE,
                        MemoryFilter = new ScheduledFilterGroup()
                    };
                    
                    if(config.WorkgroupsAreDepartments) //Why didn't we just run this through the localiser??!!
                        wgGroup.Name = localiser.Get("department_label");
                    else
                        wgGroup.Name = localiser.Get("workgroup_label");
                    
                    foreach (var wg in workgroups)
                    {
                        wgGroup.Items.Add (new FilterItem () {
                            Name = wg.Name,
                            ID = wg.ID
                        });
                    }
                    
                    Groups.Add (wgGroup);
                }
            }

            if (!config.WorkgroupsAreDepartments && filterDepartments)
            {
                if (departments != null)
                {
                    var deptGroup = new FilterGroup()
                    { 
                        Name = localiser.Get("department_label"),
                        Items = new List<FilterItem>(),
                        Type = DEPARTMENT_FILTER_CODE,
                        MemoryFilter = new ScheduledFilterGroup()
                    };

                    foreach (var dept in departments)
                    {
                        deptGroup.Items.Add(new FilterItem()
                            {
                                Name = dept.Name,
                                ID = dept.ID
                            });
                    }

                    Groups.Add(deptGroup);
                }
            }

            if (filterAttributes)
            {
                if (attributes != null)
                {
                    var attrGroup = new FilterGroup()
                    { 
                        Name = localiser.Get("attribute_label"),
                        Items = new List<FilterItem>(),
                        Type = ATTRIBUTES_FILTER_CODE,
                        MemoryFilter = new ScheduledFilterGroup()
                    };

                    foreach (var attr in attributes)
                    {
                        attrGroup.Items.Add(new FilterItem()
                            {
                                Name = attr.Name,
                                ID = attr.ID
                            });
                    }

                    Groups.Add(attrGroup);
                }
            }

            if (filterJobsCustom)
            {
                if (customJobs != null)
                {
                    var filter = new PrimaryJobRoleFilterGroup (customJobs.Select(x => new JDAJob(){ JobName = x.Name, JobID = x.ID, PrimaryRoleID = x.ID}).ToList());
                    Groups.Add(filter);
                }
            }
            else if(filterJobsClassic)
            {
                if (data != null)
                {
                    var filter = new PrimaryJobRoleFilterGroup (data.JobRoles);
                    Groups.Add (filter);
                }
            }

            if (filterRolesCustom)
            {
                if (customRoles != null)
                {
                    var rolesGroup = new FilterGroup()
                        { 
                            Name = localiser.Get("laborrole_label"),
                            Items = new List<FilterItem>(),
                            Type = LABORROLE_FILTER_CODE,
                            MemoryFilter = new ScheduledFilterGroup()
                        };

                    foreach (var role in customRoles)
                    {
                        // Don't add duplicate roles
                        if(rolesGroup.Items.Any(x => x.ID == role.ID))
                            continue;

                        rolesGroup.Items.Add(new FilterItem()
                            {
                                Name = role.Name,
                                ID = role.ID
                            });
                    }

                    Groups.Add(rolesGroup);
                }
            }
        }
    }
}