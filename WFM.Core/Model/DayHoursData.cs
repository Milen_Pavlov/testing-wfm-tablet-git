﻿using System;

namespace WFM.Core
{
    public class DayHoursData
    {
        public double Hours { get; set; }
        public double Cost { get; set; }

        public DayHoursData()
        {
            Hours = 0;
            Cost = 0;
        }
    }
}