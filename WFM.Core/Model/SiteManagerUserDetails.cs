﻿using System;

namespace WFM.Core
{
    public class SiteManagerUserDetails
    {
        public string UserID { get; set; }
        public string Username { get; set; }
    }
}

