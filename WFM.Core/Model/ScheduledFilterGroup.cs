﻿using System;
using System.Linq;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class ScheduledFilterGroup : FilterGroup, IEmployeeFilterGroup
    {
        public const string c_type = "s";

        public enum Item
        {
            Scheduled = 0,
            Unscheduled = 1
        }

        public bool ScheduledSelected
        {
            get
            {
                return Items.First(item => item.ID == (int)ScheduledFilterGroup.Item.Scheduled).Selected;
            }

            set
            {
                Items.First(item => item.ID == (int)ScheduledFilterGroup.Item.Scheduled).Selected = value;
            }
        }

        public bool UnscheduledSelected
        {
            get
            {
                return Items.First(item => item.ID == (int)ScheduledFilterGroup.Item.Unscheduled).Selected;
            }

            set
            {
                Items.First(item => item.ID == (int)ScheduledFilterGroup.Item.Unscheduled).Selected = value;
            }
        }

        public ScheduledFilterGroup()
        {
            IStringService localiser = Mvx.Resolve<IStringService>();
            Name = localiser.Get("scheduled_label");
            Type = c_type;

            Items.Add(new FilterItem()
                {
                    Name = ScheduledFilterGroup.Item.Scheduled.ToString(),
                    ID = (int)ScheduledFilterGroup.Item.Scheduled,
                    Selected = true,
                });

            Items.Add(new FilterItem()
                {
                    Name = ScheduledFilterGroup.Item.Unscheduled.ToString(),
                    ID = (int)ScheduledFilterGroup.Item.Unscheduled, 
                    Selected = true
                });
        }

        public override FilterGroup Clone()
        {
            var clone = new ScheduledFilterGroup();
            clone.Items.Clear();
            foreach (var item in Items)
                clone.Items.Add(item.Clone());

            return clone;
        }

        #region IEmployeeFilterGroup implementation

        public bool ShouldApplyFilter(EmployeeData employee, string filterGroup, int[] matchingFilterIds, int? onDayIndex)
        {
            //Both deselected, we don't filter (otherwise its always empty)
            if (!this.ScheduledSelected && !this.UnscheduledSelected)
                return true;
            else if (this.ScheduledSelected && this.UnscheduledSelected)
                return true;

            bool employeeWorkingFilterDuringPeriod = false;

            if (onDayIndex == null)
            {
                employeeWorkingFilterDuringPeriod = employee.Days.SelectMany(day => day.Shifts).SelectMany(shift => shift.Jobs).Any(job => JobMatchesFilter(job, filterGroup, matchingFilterIds));
            }
            else
            {
                employeeWorkingFilterDuringPeriod = employee.Days[onDayIndex.Value].Shifts.SelectMany(shift => shift.Jobs).Any(job => JobMatchesFilter(job, filterGroup, matchingFilterIds));
            }

            if (this.ScheduledSelected)
            {
                return employeeWorkingFilterDuringPeriod;
            }
            else
            {
                return !employeeWorkingFilterDuringPeriod;
            }
        }

        bool JobMatchesFilter(JobData job, string matchingFilterType, int[] matchingIDs)
        {
            if (matchingFilterType == FilterData.WORKGROUP_FILTER_CODE)
            {
                if (matchingIDs.Contains(job.WorkgroupID))
                {
                    return true;
                }
            }
            else if (matchingFilterType == PrimaryJobRoleFilterGroup.PRIMARYJOBROLE_FILTER_CODE)
            {
                if (matchingIDs.Contains(job.PrimaryRoleID.GetValueOrDefault(-1)))
                {
                    return true;
                }
            }
            else if (matchingFilterType == FilterData.DEPARTMENT_FILTER_CODE)
            {
                if (matchingIDs.Contains(job.DepartmentID))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion




    }
}

