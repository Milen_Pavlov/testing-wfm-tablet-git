﻿using System;
using System.Windows.Input;

namespace WFM.Core
{
    public class DrawerItem
    {
        public DrawerItem(string name, string image, bool indicator, Type type, object parameters, string notification = "")
        {
            Name = name;
            Image = image;
            Indicator = indicator;
            ModelType = type;
            Parameters = parameters;
            Notification = notification;
        }

        public string Name { get; set; }
        public string Image { get; set; }
        public bool Indicator { get; set; }
        public Type ModelType { get; set; }
        public object Parameters { get; set; }
        public string Notification { get; set; }
    }
}
