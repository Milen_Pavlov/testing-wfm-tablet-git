﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Model
{
    public class TillWindow
    {
        #region Core properties

        // Properties
        public int TillWindowId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        // Parents
        public int TillId { get; set; }
        public Till Till { get; set; }

        // Children

        #endregion
    }
}
