﻿using System;

namespace WFM.Core
{
    public class Accrual
    {
        public string Available { get; set; }
        public string Type { get; set; }

        public Accrual ()
        {
        }

        public Accrual(JDAAccrual request)
        {
            Available = request.Available.ToString ();
            Type = request.TypeName;
        }
    }
}

