﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class PayAllocation: MvxNotifyPropertyChanged
    {
        public JDATimeOffDetail JDADetail { get; set; }

        public bool IsDirty
        {
            get
            {
                if (JDADetail != null)
                    return JDADetail.PaidHours != PaidHours;
                else
                    return false;
            }
            set
            { }
        }

        public DateTime Date { get; set; }
        public DateTime StartOfWeek { get; set; }

        private double m_paidHours;
        public double PaidHours 
        { 
            get
            {
                return m_paidHours;
            }

            set
            {
                m_paidHours = value;

                RaisePropertyChanged(() => PaidHours);
                RaisePropertyChanged(() => IsDirty);
            }
        }

        public string Type { get; set; }
        public bool IsPaid { get; set; }

        public PayAllocation()
        {
        }

        public PayAllocation(JDATimeOffDetail detail)
        {
            PaidHours = detail.PaidHours.GetValueOrDefault(0);
            IsPaid = detail.IsPaid;
            Date = detail.Date;
            Type = detail.Name;
            StartOfWeek = detail.StartOfWeek;  

            JDADetail = detail;
        }
    }
}