﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace WFM.Core
{
    public class PeerRequest
    {
        public string EmployeeName { get; set; }
        public TimeOffStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public PeerRequest()
        {
        }

        public PeerRequest(JDATimeOffPeerRequest peerRequest, IEnumerable<JDATimeOffRequest> requestData)
        {
            //matchingRequest
            var timeOffRequest = requestData.FirstOrDefault(request => request.EmployeeID == peerRequest.EmployeeID);

            if(timeOffRequest != null)
            {
                var firstName = (string.IsNullOrEmpty(timeOffRequest.EmployeeNickName)) ? timeOffRequest.EmployeeFirstName : timeOffRequest.EmployeeNickName;
                EmployeeName = string.Format("{0}, {1}", timeOffRequest.EmployeeLastName, firstName);
            }

            Status = peerRequest.Status;
            StartDate = peerRequest.Start;
            EndDate = peerRequest.End;
        }
    }
}