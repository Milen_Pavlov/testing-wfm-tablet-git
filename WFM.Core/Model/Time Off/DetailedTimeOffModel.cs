﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WFM.Core
{
    /// <summary>
    /// Detailed time off model.
    /// Represents a full time off representation, complete with audit history, accurals, peer requests and pay allocations.
    /// </summary>
    public class DetailedTimeOffModel
    {
        private TimeSpan startOfDay = TimeSpan.Zero;
        private TimeSpan endOfDay = TimeSpan.FromDays(1).Subtract(TimeSpan.FromMinutes(1));

        [JsonIgnore]
        public JDADetailedTimeOffRequest TimeOffRequest
        {
            get
            {
                if (RequestDetails.Count == 0)
                {
                    return null;
                }
                else
                {
                    return RequestDetails[0];
                }
            }
        }

        public JDAEmployeeInfo EmployeeInfo
        {
            get;
            set;
        }

        public bool EmployeeViewOnly
        {
            get;
            set;
        }

        public List<JDADetailedTimeOffRequest> RequestDetails
        {
            get;
            set;
        }

        public List<TimeOffHistory> History
        {
            get;
            set;
        }

        public List<Accrual> Accruals
        {
            get;
            set;
        }

        public List<TimeOffComment> Comments
        {
            get;
            set;
        }

        public List<PeerRequest> PeerRequests
        {
            get;
            set;
        }

        public List<PayAllocation> PayAllocations
        {
            get;
            set;
        }

        public TimeOffStatus Status
        {
            get
            {
                if (TimeOffRequest != null)
                {
                    return TimeOffRequest.Status;
                }
                else
                {
                    return TimeOffStatus.Pending;
                }
            }
        }

        public DateTime StartDate
        {
            get
            {
                if (TimeOffRequest != null)
                    return TimeOffRequest.Start;
                else
                    return DateTime.MinValue;
            }
        }

        public DateTime EndDate
        {
            get
            {
                if (TimeOffRequest != null)
                    return TimeOffRequest.End;
                else
                    return DateTime.MaxValue;
            }
        }

        public string Type
        {
            get
            {
                if (TimeOffRequest != null)
                    return TimeOffRequest.TypeName;
                else
                    return string.Empty;
            }

            set
            {
                if (TimeOffRequest != null)
                    TimeOffRequest.TypeName = value;
            }
        }

        public bool IsTrackedAsWholeDays
        {
            get
            {
                if (TimeOffRequest != null)
                    return TimeOffRequest.Start.TimeOfDay == startOfDay && TimeOffRequest.End.TimeOfDay == endOfDay; //1 minute to midnight, thanks JDA
                else
                    return true;
            }
        }

        public string EmployeeName
        {
            get;
            set;
        }

        public DetailedTimeOffModel()
        {
        }

        public DetailedTimeOffModel(IEnumerable<JDAPayRule> payAllocations, 
            IEnumerable<JDATimeOffPeerRequest> peerRequests,
            IEnumerable<JDATimeOffRequest> peerTimeOffRecords,
            IEnumerable<JDAAccrual> employeeAccruals, 
            IEnumerable<JDATimeOffRequest> targetEmployeeTimeOffRecords, 
            IEnumerable<JDADetailedTimeOffRequest> requestDetails,
            JDAEmployeeInfo employeeInfo = null)
        {
            EmployeeInfo = employeeInfo;
            if (EmployeeInfo != null)
            {
                EmployeeViewOnly = true;
                var firstName = (string.IsNullOrEmpty(employeeInfo.NickName)) ? employeeInfo.FirstName : employeeInfo.NickName;
                EmployeeName = string.Format("{0}, {1}", employeeInfo.LastName, firstName);
            }

            if (requestDetails != null)
                RequestDetails = new List<JDADetailedTimeOffRequest> (requestDetails);
            else
                RequestDetails = new List<JDADetailedTimeOffRequest> ();

            History = new List<TimeOffHistory>();
            Comments = new List<TimeOffComment>();
            PeerRequests = new List<PeerRequest>();
            PayAllocations = new List<PayAllocation>();
            Accruals = new List<Accrual> ();

            //Build data from the detailed time off request
            if (TimeOffRequest != null)
            {
                foreach (var audit in TimeOffRequest.Audits)
                {
                    Comments.Add(new TimeOffComment(audit));    
                }

                foreach (var detail in TimeOffRequest.Details)
                {
                    PayAllocations.Add(new PayAllocation(detail));
                }

                //Because the employee name only comes down with a shallow time off request, we have to scan the list of all the employees time off requests (which this request should be part of)
                //And pull out the name from that.
                if(targetEmployeeTimeOffRecords != null)
                {
                   var timeOffRequest = targetEmployeeTimeOffRecords.FirstOrDefault(request => request.EmployeeID == TimeOffRequest.EmployeeID);

                   if (timeOffRequest != null)
                   {
                       var firstName = (string.IsNullOrEmpty(timeOffRequest.EmployeeNickName)) ? timeOffRequest.EmployeeFirstName : timeOffRequest.EmployeeNickName;
                       EmployeeName = string.Format("{0}, {1}", timeOffRequest.EmployeeLastName, firstName);
                   }
                }
            }

            if (targetEmployeeTimeOffRecords != null)
            {
                foreach (var request in targetEmployeeTimeOffRecords)
                {
                    if((TimeOffRequest!=null && request.EmployeeID == TimeOffRequest.EmployeeID) ||
                        (EmployeeInfo!=null && request.EmployeeID == EmployeeInfo.EmployeeID) && 
                        request.Status != TimeOffStatus.Deleted)
                        History.Add(new TimeOffHistory(request));
                }
            }

            if (peerRequests != null)
            {
                foreach (var pr in peerRequests)
                {
                    PeerRequests.Add(new PeerRequest(pr,peerTimeOffRecords));
                }
            }

            if (employeeAccruals != null)
            {
                foreach (var ac in employeeAccruals)
                {
                    Accruals.Add (new Accrual (ac));
                }
            }
        }
    }
}