﻿using System;

namespace WFM.Core
{
    public class TimeOffHistory
    {
        public TimeOffStatus Status { get; set; }
        public string Type { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public TimeOffHistory()
        {
        }

        public TimeOffHistory(JDATimeOffRequest request)
        {
            Status = request.Status;
            Type = request.PayAdjustmentCategoryName;
            From = request.Start;
            To = request.End;
        }
    }
}