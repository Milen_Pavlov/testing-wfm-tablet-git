﻿using System;
using System.Globalization;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class TimeOffComment
    {
        public string EmployeeName { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public TimeOffStatus Status { get; set; }

        private TimeOffStatusStringConverter m_statusConverter;

        public TimeOffComment()
        {
        }

        public TimeOffComment(JDATimeOffAudit audit)
        {
            var firstName = (string.IsNullOrEmpty(audit.NickName)) ? audit.FirstName : audit.NickName;
            EmployeeName = string.Format("{0}, {1}", audit.LastName, firstName);

            Message = audit.Comment ?? string.Empty;
            Date = audit.Timestamp;
            Status = TimeOffHelper.GetStatus(audit.StatusCode);
        }

        public override string ToString()
        {
            if (m_statusConverter == null)
                m_statusConverter = new TimeOffStatusStringConverter ();

            string status = (string) m_statusConverter.Convert (Status, typeof (string), null, CultureInfo.CurrentCulture);
            string appendedMessage = (string.IsNullOrEmpty(Message)) ? null : System.Environment.NewLine +System.Environment.NewLine + Message;
            string dateString = Date.ToString(Mvx.Resolve<IStringService>().Get("dd/MM/yy"));
            return string.Format("{0} {1} on {2}{3}", EmployeeName, status, dateString, appendedMessage);
        }
    }
}