﻿using System;

namespace WFM.Core
{
    public class DatePickerModel
    {
        public DateTime Date { get; set; }
        public int WeekNumber { get; set; }
        public bool IsCurrentWeek { get; set; }
        public DateTime? SelectedDate { get; set; }
        public bool IsWeekSelected { get; set; }
    }
}

