﻿using System;

namespace WFM.Core
{
    public class TimeOffData
    {
        public EmployeeData Employee { get; set; }
        public DateTime Day { get; set; }
    }
}

