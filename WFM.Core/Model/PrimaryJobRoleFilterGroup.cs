﻿using System;
using System.Collections.Generic;
using System.Linq;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class PrimaryJobRoleFilterGroup : FilterGroup, IEmployeeFilterGroup, IJobRoleFilterGroup
    {
        public const string PRIMARYJOBROLE_FILTER_CODE = "j";

        private Dictionary<int, JDAJob> m_jobMap;

        public PrimaryJobRoleFilterGroup()
        {
            IStringService localiser = Mvx.Resolve<IStringService>();
            Name = localiser.Get("primary_job_role");
            Type = PRIMARYJOBROLE_FILTER_CODE;

            m_jobMap = new Dictionary<int, JDAJob>();

            MemoryFilter = new ScheduledFilterGroup();
        }

        public PrimaryJobRoleFilterGroup(List<JDAJob> jobs)
        {
            IStringService localiser = Mvx.Resolve<IStringService>();
            Name = localiser.Get("primary_job_role");
            Type = PRIMARYJOBROLE_FILTER_CODE;

            m_jobMap = new Dictionary<int, JDAJob>();

            foreach (var job in jobs.OrderBy(job => job.JobName))
            {
                if (job.PrimaryRoleID != null)
                {
                    Items.Add(new FilterItem()
                        {
                            Name = job.JobName,
                            ID = (int)job.JobID
                        });

                    m_jobMap.Add(job.JobID, job);
                }
            }

            MemoryFilter = new ScheduledFilterGroup();
        }

        public override FilterGroup Clone()
        {
            var clone = new PrimaryJobRoleFilterGroup();

            foreach (var item in Items)
                clone.Items.Add(item.Clone());

            clone.m_jobMap = new Dictionary<int, JDAJob>(m_jobMap);

            return clone;
        }

        #region IEmployeeFilterGroup implementation

        public bool ShouldApplyFilter(EmployeeData employee, string currentFilterType, int[] selectedIndexes, int? onDayIndex)
        {
            int[] filterValues = GetSelectedList();

            if (!filterValues.Contains((employee.Info.PrimaryJobID)))
                return false;

            return true;
        }

        #endregion

        #region IJobRoleFilterGroup implementation

        public bool ApplyFilter(int roleId)
        {
            int[] filterValues = GetSelectedList();

            foreach (int filterValue in filterValues)
            {
                JDAJob job = null;
                m_jobMap.TryGetValue(filterValue, out job);
                if (job != null)
                {
                    if (job.PrimaryRoleID == roleId)
                        return true;
                }
            }

            return false;
        }

        public int GetJobTypeForWorkgroup(int workgroup)
        {
            return m_jobMap.Where(map => map.Value.WorkgroupID == workgroup).Select(map => map.Key).FirstOrDefault();
        }

        #endregion
    }
}

