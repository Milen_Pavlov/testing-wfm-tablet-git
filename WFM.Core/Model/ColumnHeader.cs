﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class ColumnHeader : MvxNotifyPropertyChanged
    {
        public int Index { get; set; }
        public int StartHour { get; set; }

        private string m_Title;
        public string Title 
        { 
            get{ return m_Title; }
            set
            { 
                m_Title = value;
                RaisePropertyChanged (() => Title);
            } 
        }

        private string m_Title1;
        public string Title1 
        { 
            get{ return m_Title1; }
            set
            { 
                m_Title1 = value;
                RaisePropertyChanged (() => Title1);
            } 
        }

        private string m_Title2;
        public string Title2 
        { 
            get{ return m_Title2; }
            set
            { 
                m_Title2 = value;
                RaisePropertyChanged (() => Title2);
            } 
        }

        public string Value1 { get; set; }

        public string Value2 { get; set; }

        public SortState Sort { get; set; }
    }
}
