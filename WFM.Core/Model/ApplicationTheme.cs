﻿using System;

namespace WFM.Core
{
    public class ApplicationTheme
    {
        public string LoginBarColor { get; set; }
        public string LoginTextColor { get; set; }
        public string PrimaryColor { get; set; }
        public string SecondaryColor { get; set; }
        public string HighlightColor { get; set; }
        public string SignOutColor { get; set; }
        public string DetailTintColor { get; set; }
        public string SecondaryTextColor { get; set; }
        public string VersionAndEnvironmentColor { get; set; }
        public bool ShowLoginBar { get; set; }

        public ApplicationTheme()
        {
            LoginBarColor = "#ffffff";
            LoginTextColor = "000000";
            PrimaryColor = "#000000";
            SecondaryColor = "#ffffff";
            HighlightColor = "#000000";
            SignOutColor = "#ff0000";
            DetailTintColor = "#000000";
            SecondaryTextColor = "#ffffff";
            VersionAndEnvironmentColor = "#000000";
            ShowLoginBar = true;
        }
    }
}

