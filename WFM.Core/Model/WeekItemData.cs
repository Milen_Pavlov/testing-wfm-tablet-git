﻿using System;

namespace WFM.Core
{
    public class WeekItemData
    {
        public int EmployeeId { get; set; }
        public EmployeeDayData Day { get; set; }
    }
}

