﻿using System;

namespace WFM.Core.MyHours
{
    public class Day
    {       
        public decimal Sales
        {
            get
            {
                return WeeklySales * Percentage;
            }
        }

        public decimal Percentage { get; private set; }
        private bool m_isProtected;
        public bool IsProtected { 
            get { return (DayPerChangeIndicator != 0 || CentralChangeIndicator != 0 || m_isProtected); } 
            private set { m_isProtected = value; }
        }

        public decimal WeeklySales { get; private set; }
        public decimal WeeklyHours { get; private set; }
        public decimal PlannedHours { get; private set; }

        private decimal UnprotectedPercentage { get; set; }

        private decimal InitialPercentage { get; set; }
        public int Spread { get; set; }
        public int DayPerChangeIndicator { get; set; }
        public int CentralChangeIndicator { get; set; }
        public int WeekChange { get; set; }

        public int Date { get; set; }
        public string DayOfWeek { get; set; }

        public Day()
        {

        }

        public ChangedByType GetDayChange()
        {
            if (CentralChangeIndicator > 0)
                return ChangedByType.Central;
            if (DayPerChangeIndicator > 0)
                return ChangedByType.Store;

            return ChangedByType.None;
        }

        public Day Clone()
        {
            Day day = new Day ();
            day.Percentage = Percentage;
            day.IsProtected = IsProtected;
            day.PlannedHours = PlannedHours;
            day.WeeklySales = WeeklySales;
            day.WeeklyHours = WeeklyHours;
            day.UnprotectedPercentage = UnprotectedPercentage;
            day.DayPerChangeIndicator = DayPerChangeIndicator;
            day.CentralChangeIndicator = CentralChangeIndicator;
            day.DayOfWeek = DayOfWeek;

            return day;
        }

        public Day(decimal percentage, decimal dayHours, decimal weekHours, decimal departmentSales, int date, string dayOfWeek)
        {
            Percentage = percentage;
            PlannedHours = dayHours;
            WeeklyHours = weekHours;
            WeeklySales = departmentSales;

            InitialPercentage = percentage;
            Spread = 7;

            Date = date;
            DayOfWeek = dayOfWeek;
        }            

        public void SetPercentage(decimal newPercentage)
        {            
            Percentage = newPercentage;
            PlannedHours = WeeklyHours * Percentage;
        }

        public void FixDayPercentage(decimal newPercentage)
        {
            Percentage = newPercentage;
            PlannedHours = WeeklyHours * Percentage;
            IsProtected = true;
            DayPerChangeIndicator = 1;
            CentralChangeIndicator = 0;
        }

        public void SetDepartmentHours(decimal hours)
        {
            WeeklyHours = hours;
            PlannedHours = WeeklyHours * Percentage;
        }
    }
}