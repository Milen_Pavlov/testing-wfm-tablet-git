﻿using System;
using WFM.Core.MyHours;

namespace WFM.Core
{
    public enum EditableCellType
    {
        None,
        WeekPlannedHours,
        DayPercentage,
    }

    public enum ChangedByType
    {
        None,
        Store,
        Central,
    }

    public class MyHoursCellData
    {
        public string ColumnTitle { get; set; }
        public string RowTitle { get; set; }

        public ChangedByType ChangedBy { get; set; }

        public bool IsLocked { get; set; }
        public bool IsModified { get; set; }
        public decimal Value { get; set; }
        public string Text { get; set; }
        public EditableCellType EditableType { get; set; }
        public Department Dept { get; set; }
        public DayOfWeek? Day { get; set; }

        public bool CanModify { get; set; }

        public bool DidUndo { get; set; }

        public bool IsEditable { get { return EditableType != EditableCellType.None && CanModify; } }
    }
}