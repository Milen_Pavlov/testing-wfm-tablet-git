﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class MyHoursRowData
    {
        public bool CanClickTitle { get; set; }
        public string ID { get; set; }
        public string Title { get; set; }
        public List<MyHoursCellData> Columns { get; set; }
    }
}

