﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core.MyHours
{
    public class Department
    {
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }

        public decimal TargetHours { get; set; }
        public decimal AllowedHours { get; set; }
        public decimal Sales { get; set; }
        public decimal PlannedHours { get; set; }

        public bool IsModified { get; set; }

        private bool m_isProtected;
        public bool IsProtected { 
            get { 
                if(m_isProtected == false)
                {
                    foreach (Day day in Days)
                    {   
                        if (day == null)
                            continue;

                        if (WeekChange != ChangedByType.None)
                            return true;
                        
                        if (day.DayPerChangeIndicator != 0)
                            return true;
                    }
                }
                return m_isProtected; 
            }
            set { m_isProtected = value; }
        }

        public bool UserCanModify { get; set; }

        public Day[] Days { get; set; }

        public ChangedByType WeekChange
        {
            get
            {
                foreach (var day in Days)
                {
                    if (day == null)
                        continue;

                    if (day.CentralChangeIndicator > 0)
                        return ChangedByType.Central;
                    if (day.WeekChange > 0)
                        return ChangedByType.Store;
                }
                return ChangedByType.None;
            }
        }

        public Department()
        {
            Days = new Day[7];
        }

        public Department Clone()
        {
            Department dept = new Department ();
            dept.DepartmentCode = DepartmentCode;
            dept.DepartmentName = DepartmentName;
            dept.TargetHours = TargetHours;
            dept.AllowedHours = AllowedHours;
            dept.Sales = Sales;
            dept.PlannedHours = PlannedHours;
            dept.IsModified = IsModified;
            dept.IsProtected = IsProtected;
            dept.UserCanModify = UserCanModify;
            dept.Days = new Day[7];
            for (int i = 0; i < dept.Days.Length; i++)
                dept.Days[i] = (Days[i]!=null) ? Days[i].Clone () : null;

            return dept;
        }

        public decimal GetMaxDayPercentage(DayOfWeek? ignoringDay)
        {
            //Get all locked day totals (not including this one if already 'locked')
            decimal lockedTotal = GetLockedTotal(ignoringDay);

            return 100m - (lockedTotal * 100m);
        }

        public decimal GetMinDayPercentage(DayOfWeek? ignoringDay)
        {
            //Similar to max, but make sure we don't set lower, this only comes into effect when all but 1 are locked
            int totalLocked = 0;
            for (int i = 0; i < 7; i++)
                if (Days[i].IsProtected && (ignoringDay != null && Days[i].DayOfWeek != ignoringDay.Value.ToString()))
                    totalLocked++;

            if (totalLocked == 6)
                return GetMaxDayPercentage (ignoringDay);
            else
                return 0;
        }

        private decimal GetLockedTotal(DayOfWeek? ignoringDay)
        {
            decimal lockedTotal = 0;
            for (int i = 0; i < 7; i++)
                if (Days[i].IsProtected && (ignoringDay != null && Days[i].DayOfWeek != ignoringDay.Value.ToString()))
                    lockedTotal += Days[i].Percentage;

            return lockedTotal;
        }

        public void SetDayPercentage(DayOfWeek dayOfWeek, decimal newPercentage)
        {
            //This process *can* cause rounding issues, so we've been told to "dump" the remainders on the last available day
            //So basically, we need to take a total before, calculate everything, take a total after, then add the different to
            //the last unprotected day
            decimal originalTotal =  Days.Sum(day => day.Percentage);

            decimal oldUnprotectedPercentage = 1m - Days.Where (day => day.IsProtected).Sum (day => day.Percentage);
            string dayString = dayOfWeek.ToString ();
            Day targetDay = Days.First(x => x.DayOfWeek == dayString);

            if (!targetDay.IsProtected)
            {
                oldUnprotectedPercentage -= targetDay.Percentage;
            }

            targetDay.FixDayPercentage (newPercentage);

            decimal unprotectedPercentage = 1m - Days.Where (day => day.IsProtected).Sum (day => day.Percentage);

            if (oldUnprotectedPercentage > 0)
            {
                foreach (var day in Days.Where(day => !day.IsProtected))
                {
                    day.SetPercentage ((day.Percentage / oldUnprotectedPercentage) * unprotectedPercentage);
                }
            }
            else
            {
                //Going from zero to a value, we need to equally distribute as we've lost the initial proportion over the percentage
                var unprotectedDays = Days.Where (day => !day.IsProtected);

                foreach (var day in unprotectedDays)
                {
                    day.SetPercentage (unprotectedPercentage / unprotectedDays.Count());
                }
            }

            //Get remainer and add on the end
            decimal newTotal = Days.Sum(day => day.Percentage);
            decimal remainder = originalTotal - newTotal;
            //Get last unprotected day, if null get the one were currently editing and "fudge"
            var fudgeDay = Days.LastOrDefault (day => !day.IsProtected);
            if (fudgeDay == null)
                fudgeDay = targetDay;

            //Fudge!
            fudgeDay.SetPercentage(fudgeDay.Percentage + remainder);
        }

        public void FixUnprotectedHours(decimal fixedHours)
        {
            UpdatePlannedHours(fixedHours);
            IsProtected = true;

            foreach (Day day in Days)
            { 
                day.CentralChangeIndicator = 0;
                day.WeekChange = 1;
            }
        }

        public void UpdatePlannedHours(decimal hours)
        {
            PlannedHours = hours;
            IsModified = true;

            foreach (Day day in Days)
            {
                if (day == null)
                    continue;
                
                day.SetDepartmentHours (PlannedHours);
            }
        }
    }
}