﻿using System;

namespace WFM.Core
{
    public class MyHoursTotalData
    {
        public string Total { get; set; }
        public bool Disabled { get; set; }
    }
}

