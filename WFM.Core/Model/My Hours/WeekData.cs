﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core.MyHours
{
    public class WeekData
    {       
        public int Store { get; set; }
        public int Week { get; set; }
        public decimal TargetHours { get; set; }
        public decimal AllowedHours { get; set; }
        public decimal Sales { get; set; }
        public decimal PlannedHours { get; set; }
        public decimal ForecastedSales { get; set; }
        public IEnumerable<Department> Departments { get; set; }

        /// <summary>
        /// Is this is a special protected department that can't be modified by My Hours?
        /// </summary>
        /// <value><c>true</c> if this instance is protected; otherwise, <c>false</c>.</value>
        public bool IsProtected { get; set; }

        public bool IsEmpty { get { return Departments == null || Departments.Count () == 0; } }
   
        public void SetDepartmentHours(string departmentCode, decimal departmentHours)
        {
            //This process *can* cause rounding issues, so we've been told to "dump" the remainders on the last available day
            //So basically, we need to take a total before, calculate everything, take a total after, then add the different to
            //the last unprotected day
            decimal originalTotal =  PlannedHours;

            decimal oldUnprotectedHours = PlannedHours - Departments.Where(dept => dept.IsProtected).Sum(day => day.PlannedHours);

            Department targetDept = Departments.First(dept => dept.DepartmentCode == departmentCode);

            if (!targetDept.IsProtected)
            {
                oldUnprotectedHours -= targetDept.PlannedHours;
            }

            targetDept.FixUnprotectedHours(departmentHours);

            decimal unprotectedHours = PlannedHours - Departments.Where(dept => dept.IsProtected).Sum(day => day.PlannedHours);

            if (oldUnprotectedHours > 0)
            {
                foreach (var dept in Departments.Where(dept => !dept.IsProtected))
                {
                    dept.UpdatePlannedHours((dept.PlannedHours / oldUnprotectedHours) * unprotectedHours);
                }
            }
            else
            {
                //Going from zero to a value, we need to equally distribute as we've lost the initial proportion over the percentage
                var unprotectedDepts = Departments.Where(day => !day.IsProtected);

                foreach (var dept in unprotectedDepts)
                {
                    dept.UpdatePlannedHours(unprotectedHours / unprotectedDepts.Count());
                }
            }

            //Get remainer and add on the end
            decimal newTotal =  Departments.Sum(dept => dept.PlannedHours);
            decimal remainder = originalTotal - newTotal;
            //Get last unprotected day, if null get the one were currently editing and "fudge"
            var fudgeDept = Departments.LastOrDefault (dept => !dept.IsProtected);
            if (fudgeDept == null)
                fudgeDept = targetDept;

            //Fudge!
            fudgeDept.UpdatePlannedHours(fudgeDept.PlannedHours + remainder);

            newTotal =  Departments.Sum(dept => dept.PlannedHours);
            remainder = originalTotal - newTotal;
        }

        public decimal GetMaxPlannedHours(string ignoreDept)
        {
            //Max value is the week total planned, can't go over
            //So get all locked values (not including this one if already 'locked') and take off from total
            decimal lockedTotal = TotalLocked(ignoreDept);
            return PlannedHours - lockedTotal;
        }

        public decimal GetMinPlannedHours(string ignoreDept)
        {
            //Similar to max, but make sure we don't set lower, this only comes into effect when all but 1 are locked
            int totalLocked = 0;
            foreach (var dept in Departments)
                if (dept.IsProtected && dept.DepartmentCode != ignoreDept)
                    totalLocked++;

            if (totalLocked == Departments.Count() - 1)
                return GetMaxPlannedHours (ignoreDept);
            else
                return 0;
        }

        private decimal TotalLocked(string ignoreDept)
        {
            decimal lockedTotal = 0;
            foreach (var dept in Departments)
                if (dept.IsProtected && dept.DepartmentCode != ignoreDept)
                    lockedTotal += dept.PlannedHours;

            return lockedTotal;
        }

        public WeekData Clone()
        {
            WeekData data = new WeekData ();
            data.Store = Store;
            data.Week = Week;
            data.TargetHours = TargetHours;
            data.AllowedHours = AllowedHours;
            data.Sales = Sales;
            data.PlannedHours = PlannedHours;
            data.Departments = null;

            if (Departments != null)
                data.Departments = Departments.Select (x => x.Clone()).ToList();

            return data;
        }

        public bool HasCentralMadeAnyChanges()
        {
            foreach (var department in Departments)
            {
                if (department.WeekChange == ChangedByType.Central)
                {
                    return true;
                }
            }

            return false;
        }
    }
}