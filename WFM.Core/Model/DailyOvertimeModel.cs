﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using System.Linq;

namespace WFM.Core
{
    public class DailyOvertimeModel : MvxNotifyPropertyChanged
    {
        public int DepartmentID { get; set; }
        public String DepartmentName { get; set; }
        public Dictionary<DayOfWeek, double> Hours { get; set; }

        private Dictionary<DayOfWeek, String> m_hourStrings; 
        public Dictionary<DayOfWeek, String> HourStrings { 
            get {return m_hourStrings; } 
            set 
            { 
                m_hourStrings = value;
                RaisePropertyChanged (() => HourStrings); 
            }
        }

        private Dictionary<DayOfWeek, bool> m_DayEdited; 
        public Dictionary<DayOfWeek, bool> DayEdited { 
            get {return m_DayEdited; } 
            set 
            { 
                m_DayEdited = value; 
                RaisePropertyChanged (() => DayEdited); 
            }
        }

        public double Total { get; set; }
        public double AdditionalResource { get; set; }
        public DayOfWeek SelectedDay { get; set; }

        private bool m_editable = false;
        public bool Editable
        {
            get { return m_editable; }
            set { m_editable = value; RaisePropertyChanged (() => Editable); }
        }

        public Action<DailyOvertimeModel, double> OnTotalUpdated  { get; set; }

        public DailyOvertimeModel ()
        {
            
        }

        private void UpdateTotal ()
        {
            Total = Hours.Sum (x => x.Value);
            RaisePropertyChanged (() => Total);
            if (OnTotalUpdated != null)
                OnTotalUpdated.Invoke (this, Total);
        }

        private String GetHoursString (DayOfWeek day)
        {
            return HourStrings[day];
        }

        private void SetHours(DayOfWeek day, string value)
        {
            HourStrings[day] = value;
        }

        public void UpdateHours()
        {
            for(int i = 0; i < 7; ++i)
            {
                double result = 0.0;
                if (double.TryParse (HourStrings[(DayOfWeek)i], out result))
                {
                    Hours[(DayOfWeek)i] = result;
                }
            }

            UpdateTotal();
        }

        public void EditedDay(DayOfWeek day, String hours)
        {
            HourStrings[day] = hours;
            DayEdited[day] = true;

            HourStrings = HourStrings;
            DayEdited = DayEdited;
        }
    }
}


