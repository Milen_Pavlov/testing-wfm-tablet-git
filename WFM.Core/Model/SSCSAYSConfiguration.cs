using System;

namespace WFM.Core
{
    public class SSCSAYSConfiguration
    {
        public string WorkgroupName { get; set; }
        public string JobName { get; set; }
        public string DisplayName { get; set; }
    }
    
}
