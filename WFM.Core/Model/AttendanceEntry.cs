﻿using System;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;

namespace WFM.Core
{
    public class AttendanceEntry : MvxNotifyPropertyChanged
    {
        public int EmployeeID { get; set; }
        public int ShiftID { get; set; }
        public List<JDAJobAssignment> Jobs{ get; set; }

        private string m_employeeName;
        public string EmployeeName
        {
            get { return m_employeeName; }
            set { m_employeeName = value; RaisePropertyChanged (() => EmployeeName); }
        }

        private bool m_isIn;
        public bool IsIn
        {
            get { return m_isIn; }
            set { m_isIn = value; RaisePropertyChanged (() => IsIn); }
        }

        private DateTime m_start;
        public DateTime Start
        {
            get { return m_start; }
            set { m_start = value; RaisePropertyChanged (() => Start); RaisePropertyChanged (() => StartFormatted); }
        }


        private DateTime m_end;
        public DateTime End
        {
            get { return m_end; }
            set { m_end = value; RaisePropertyChanged (() => End); RaisePropertyChanged (() => EndFormatted); }
        }

        private bool m_currentShift;
        public bool CurrentShift
        {
            get { return m_currentShift; }
            set { m_currentShift = value; RaisePropertyChanged (() => CurrentShift); }
        }

        private string m_firstTillName;
        public string FirstTillName
        {
            get { return m_firstTillName; }
            set { m_firstTillName = value; RaisePropertyChanged (() => FirstTillName); }
        }

        public string StartFormatted
        {
            get { return Start.ToString ("HH:mm"); }
        }

        public string EndFormatted
        {
            get { return string.Format ("- {0}", End.ToString ("HH:mm")); }
        }

        public AttendanceEntry()
        {
        }
    }
}

