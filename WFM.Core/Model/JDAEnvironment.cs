﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class Environment
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ConsortiumAddress { get; set; }
        public string ConsortiumUsername { get; set; }
        public string ConsortiumPassword { get; set; }
    }
}