﻿using System;
namespace WFM.Core
{
    public class LabourDemandResponse
    {
        public StoreAbsenceTargets StoreAbsenceTargets { get; set; }
    }

    public class AbsenceTargets
    {
        public int TescoYearWeek { get; set; }
        public decimal AbsenceTargetAsPercentage { get; set; }
        public decimal AbsenceTargetAsFactor { get; set; }
    }

    public class StoreAbsenceTargets
    {
        public int StoreNumber { get; set; }
        public AbsenceTargets AbsenceTargets { get; set; }
    }
}

