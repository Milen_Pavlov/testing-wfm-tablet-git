﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;

namespace WFM.Core
{
    [ConsortiumRoute("/stores/{StoreID}/{WeekNumber}/absenceTarget", HttpVerb.Get)]
    public class AbsenceTargetRequest
    {
        public string StoreID { get; set; }
        public string WeekNumber { get; set; }
    }
}

