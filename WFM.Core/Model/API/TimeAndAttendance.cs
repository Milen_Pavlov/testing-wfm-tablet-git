﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/TimeAndAttendance/TimeCard.asmx/GetSiteTimecards","POST")]
    public class GetSiteTimecardsRequest
    {
        public int siteID { get; set; }
        public DateTime date { get; set; }
    }

    public class GetSiteTimecardsResponse : JDAMessage<JDASiteTimeCardsForData>
    {

    }


    [ConsortiumRoute("data/sitemgr/services/TimeAndAttendance/Punches.asmx/GetUnpairedPunchesVisibleToSite", "POST")]
    public class GetUnpairedPunchesRequest
    {
        public int siteID { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
    }

    public class GetUnpairedPunchesResponse : JDAMessage<List<JDAUnpairedPunch>>
    {
    }


    [ConsortiumRoute("data/sitemgr/services/TimeAndAttendance/Punches.asmx/GetPayPeriodPunchExceptionsForSite", "POST")]
    public class GetPayPeriodPunchExceptionsForSiteRequest
    {
        public int siteID { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public bool includeBorrowedEmps { get; set; }
    }

    public class GetPayPeriodPunchExceptionsForSiteResponse : JDAMessage<List<JDAPunchException>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/TimeAndAttendance/Punches.asmx/GetPunchesForShift", "POST")]
    public class GetPunchesForShiftRequest
    {
        public int siteID { get; set; }
        public int shiftID { get; set; }
    }

    public class GetPunchesForShiftResponse : JDAMessage<JDAPunchesForSchedule>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/TimeAndAttendance/Punches.asmx/GetPunchesForSchedule", "POST")]
    public class GetPunchesForScheduleRequest
    {
        public int siteID { get; set; }
        public int scheduleID { get; set; }
    }

    public class GetPunchesForScheduleResponse : JDAMessage<JDAPunchesForSchedule>
    {
    }
}

