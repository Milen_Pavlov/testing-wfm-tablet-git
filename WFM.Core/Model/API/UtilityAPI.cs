﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/Utility.asmx/ProcessChangeCommands", "POST")]
    public class ProcessChangeCommandsRequest
    {
        public JDAChangeCommand[] jsChangeCommands { get; set; }
    }

    public class ProcessChangeCommandsReply : JDAMessage<JDAProcessingResult>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/Utility.asmx/ProcessChangeCommands?siteId={siteId}", "POST")]
    public class ProcessChangeCommandsSiteRequest
    {
        public int siteId { get; set; }
        public JDAChangeCommand[] jsChangeCommands { get; set; }
    }

    public class ProcessChangeCommandsSiteReply : JDAMessage<JDAProcessingResult>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/Utility.asmx/GetAuditReasons", "POST")]
    public class GetAuditReasonsRequest
    {
        public int siteId { get; set; }

    }

    public class GetAuditReasonsResponse : JDAMessage<List<JDANameAndID>>
    {
    }
}
