﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetOrgUnitsForCurrentUser","GET")]
    public class OrgUnitsRequest
    {
    }

    public class OrgUnitsReply : JDAMessage<List<JDAOrgUnit>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetSite","POST")]
    public class SiteRequest
    {
        public int siteID { get; set; }
    }

    public class SiteReply : JDAMessage<List<JDASite>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetRawScheduleAccuracyForSiteByDay","POST")]
    public class ScheduleAccuracyRequest
    {
        public int buID { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public JDAScheduleFilter filters { get; set; } //< not 100% the server actually handles this
    }

    [ConsortiumRoute ("data/sitemgr/CustomServices/BusinessUnit.asmx/GetRawScheduleAccuracyForSiteByDay", "POST")]
    public class ScheduleAccuracyCustomRequest
    {
        public int buID { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public JDAScheduleFilter filters { get; set; } //< not 100% the server actually handles this
    }

    public class ScheduleAccuracyReply : JDAMessage<JDAScheduleAccuracyData>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetAllDepartmentsByAuthJobs","POST")]
    public class DeptByAuthJobsRequest
    {
        public int siteID { get; set; }
    }

    public class DeptByAuthJobsReply : JDAMessage<List<JDANameAndID>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetWorkgroupBySite","POST")]
    public class WorkgroupBySiteRequest
    {
        public int siteID { get; set; }
    }

    public class WorkgroupBySiteReply : JDAMessage<List<JDANameAndID>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetEmployeeAttributesBySite","POST")]
    public class EmployeeAttributesRequest
    {
        public int siteID { get; set; }
    }

    public class EmployeeAttributesReply : JDAMessage<List<JDAEmployeeAttribute>>
    {
    }


    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetAllJobsForSite","POST")]
    public class GetAllJobsForSiteRequest
    {
        public int siteID { get; set; }
    }

    public class GetAllJobsForSiteResponse : JDAMessage<JDAJob[]>
    {

    }

    [ConsortiumRoute("data/sitemgr/CustomServices/Schedule.asmx/GetAllJobsForSite","POST")]
    public class GetAllJobsForSiteCustomRequest
    {
        public int siteID { get; set; }
    }

    public class GetAllJobsForSiteCustomResponse : JDAMessage<List<JDACustomJob>>
    {

    }

    [ConsortiumRoute("data/sitemgr/CustomServices/Schedule.asmx/GetRolesForSite","POST")]
    public class GetRolesForSiteCustomRequest
    {
        public int siteID { get; set; }
    }

    public class GetRolesForSiteCustomResponse : JDAMessage<List<JDACustomRole>>
    {

    }

    [ConsortiumRoute("data/sitemgr/services/BusinessUnit.asmx/GetEmployeeSummary","POST")]
    public class GetEmployeeTimecardSummaryRequest
    {
        public int siteID { get; set; }
        public int buID { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }

    public class GetEmployeeTimecardSummaryResponse : JDAMessage<JDAEmployeeTimecardSummary[]>
    {

    }
}
