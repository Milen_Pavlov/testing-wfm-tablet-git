﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;
using WFM.Shared.Constants;

namespace WFM.Core.MyHours
{
    [ConsortiumRoute("/stores/{StoreID}/combinedLabourDepartments", HttpVerb.Get)]
    public class GetCombinedDepartmentsRequest
    {
        public string StoreID { get; set; }
    }

    public class CombinedDepartmentsResponseData
    {
        public CombinedDepartmentStoreData StoreLabourDepartments { get; set; }
    }

    public class GetCombinedDepartmentsResponse : CombinedDepartmentsResponseData
    {
        
    }

    [ConsortiumRoute("/stores/{StoreID}/combinedLabourDepartments/{DepartmentID}", HttpVerb.Post)]
    public class SetCombinedDepartmentRequest
    {
        public string StoreID { get; set; }
        public string DepartmentID { get; set; }

        //Payload
        public List<string> labourdepartmentID { get; set; }
    }

    [ConsortiumRoute("/stores/{StoreID}/combinedLabourDepartments/{DepartmentID}", HttpVerb.Delete)]
    public class DeleteCombinedDepartmentRequest
    {
        public string StoreID { get; set; }
        public string DepartmentID { get; set; }
    }
}