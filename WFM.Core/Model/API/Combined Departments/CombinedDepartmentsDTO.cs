﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace WFM.Core
{
    public class CombinedDepartmentStoreData
    {
        public int StoreNumber { get; set; }

        public List<LeadCombinedDepartment> CombinedDepartments { get; set; }

        public List<CombinedDepartment> UncombinedDepartments { get; set; }
        
        public CombinedDepartmentStoreData() 
        { 
            CombinedDepartments = new List<LeadCombinedDepartment> ();
            UncombinedDepartments = new List<CombinedDepartment> ();
        }
    }

    public class CombinedDepartment
    {
        public string LabourDepartmentID { get; set; }

        public string LabourDepartmentDescription { get; set; }

        public CombinedDepartment() { }
    }

    public class LeadCombinedDepartment
    {
        public string LeadLabourDepartmentID { get; set; }

        public string LabourDepartmentDescription { get; set; }

        public List<CombinedDepartment> MemberDepartments { get; set; }

        public LeadCombinedDepartment()
        {
            MemberDepartments = new List<CombinedDepartment> ();
        }
    }

    public class PostCombinedDepartment
    {
        public string LabourDepartmentID { get; set; }
    }
}

