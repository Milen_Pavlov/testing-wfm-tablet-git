﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/Metrics/Forecast.asmx/GetForecastedMetric","POST")]
    public class SalesForecastDataRequest
    {
        public string siteID { get; set; }
        public DateTime startOfWeek { get; set; }
        public int groupID { get; set; }
    }

    public class SalesForcastDataResponse : JDAMessage<List<JDAForecastedData>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/Metrics/Forecast.asmx/GetForecastGroupsBySite","POST")]
    public class ForecastGroupsBySiteRequest
    {
        public string siteID { get; set; }
    }

    public class ForecastGroupsBySiteResponse : JDAMessage<List<JDANameAndID>>
    {
    }

}

