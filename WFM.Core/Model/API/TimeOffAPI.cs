﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetCurrentTORRequests","POST")]
    public class ListTimeOffRequest
    {
        public int buid { get; set; }
        public string dir { get; set; }
        public int EmployeeIDFilter { get; set; }
        public DateTime endTime { get; set; }
        public bool includeApproved { get; set; }
        public bool includeCanceled { get; set; }
        public bool includeDenied { get; set; }
        public bool includeRequested { get; set; }
        public string sort { get; set; }
        public DateTime startTime { get; set; }
    }

    public class ListTimeOffResponse : JDAMessage<JDATimeOffRequestsBody>
    {
        
    }

    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetTORRequests","POST")]
    public class GetTimeOffRequest
    {
        public int buid { get; set; }
        public string dir { get; set; }
        public int EmployeeIDFilter { get; set; }
        public DateTime endTime { get; set; }
        public bool includeApproved { get; set; }
        public bool includeCanceled { get; set; }
        public bool includeDenied { get; set; }
        public bool includeRequested { get; set; }
        public string sort { get; set; }
        public DateTime startTime { get; set; }
    }

    public class GetTimeOffResponse : JDAMessage<JDATimeOffRequestsBody>
    {

    }

    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetTimeOffRequest","POST")]
    public class TimeOffRequest
    {
        public int employeeId { get; set; }
        public int siteID { get; set; }
        public int timeOffRequestID { get; set;}
    }

    public class TimeOffResponse : JDAMessage<List<JDADetailedTimeOffRequest>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetTimeOffPayAllocationTypesForEmployee","POST")]
    public class PayAllocationRequest
    {
        public int employeeID { get; set; }
        public DateTime businessDate { get; set; }
    }

    public class PayAllocationResponse : JDAMessage<List<JDAPayRule>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetTimeOffPeerRequests","POST")]
    public class PeerRequestsRequest
    {
        public int employeeID { get; set; }
        public DateTime endSiteLocalTimestamp { get; set; }
        public int siteID { get; set; }
        public DateTime startSiteLocalTimestamp { get; set; }
    }

    public class PeerRequestsResponse : JDAMessage<List<JDATimeOffPeerRequest>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetEmployeeAccruals","POST")]
    public class EmployeeAccrualsRequest
    {
        public int employeeID { get; set; }
        public DateTime businessDate { get; set; }
    }

    public class EmployeeAccrualsResponse : JDAMessage<List<JDAAccrual>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GetTimeOffTypesForEmployee","POST")]
    public class TimeOffTypesForEmployeeRequest
    {
        public int employeeID { get; set; }
        public DateTime businessDate { get; set; }
        public string query { get; set; }
    }

    public class TimeOffTypesForEmployeeResponse : JDAMessage<IEnumerable<JDATimeOffType>>
    {

    }


    [ConsortiumRoute("data/sitemgr/services/TimeOffRequest.asmx/GenerateNewTimeOffRequestDetailsForTypeChange?siteId={siteID}","POST")]
    public class GenerateNewTimeOffRequestDetailsForTypeChangeRequest
    {
        public int siteID { get; set; }
        public int employeeID { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int timeOffRequestID { get; set; }//-1
        public int timeOffTypeID { get; set; }
    }

    public class GenerateNewTimeOffRequestDetailsForTypeChangeResponse : JDAMessage<IEnumerable<JDATimeOffDetail>>
    {

    }
}