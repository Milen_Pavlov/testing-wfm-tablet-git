﻿using System;
using System.Collections.Generic;
using Consortium.Model.Engine;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/Employee.asmx/GetEmployeeContactInfoByFutureActiveOrCurrentlyOnLeaveOfAbsenceAndHomeSite","POST")]
    public class GetAllEmployeeContactInfoRequest
    {
        public int siteID { get; set; }
        public DateTime? effectiveTimestamp { get; set; }
    }

    public class GetAllEmployeeContactInfoResponse : JDAMessage<JDAEmployeeInfo[]>
    {

    }
    
    [ConsortiumRoute("data/sitemgr/services/Employee.asmx/GetEmployeeContactInfo","POST")]
    public class EmployeeContactInfoRequest
    {
        public int[] employeeIDs;
        public DateTime? effectiveTimestamp;
        public int siteID { get; set; }
    }
}

