﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/api/vbeta1/Approvals/{siteId}","GET")]
    public class GetAllApprovalsRequest
    {
        public int siteId { get; set; }
        public int id { get; set; } 
    }

    public class GetAllApprovalsResponse : JDAMessage<JDAApprovalRequestsBody>
    {

    }


    [ConsortiumRoute("data/sitemgr/api/vbeta1/ApprovalsHistory/{siteId}","GET")]
    public class GetApprovalsHistoryRequest
    {
        public string start { get; set; }
        
        public int id { get; set; } 
        public int siteId { get; set; }
    }

    public class GetApprovalsHistoryResponse : JDAMessage<JDAApprovalHistoryBody>
    {

    }


    [ConsortiumRoute("data/sitemgr/services/Schedule.asmx/GetAwaitingSwapApproval?siteId={siteId}","POST")]
    public class GetAwaitingSwapApprovalRequest
    {
        public bool isSwapper { get; set; }
        public string swapID { get; set; }
        public string swappeeShiftID { get; set; } 
        public int siteId { get; set; }
    }

    public class GetAwaitingSwapApprovalResponse : JDAMessage<List<JDAAwaitingSwapApproval>>
    {

    }
}

