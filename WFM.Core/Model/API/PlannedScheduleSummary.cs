﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/CustomServices/PlannedScheduleSummary.asmx/GetReportData?siteId={siteId}", "POST")]
    public class PlannedScheduleSummaryRequest
    {
        public string siteId { get; set; }
        public DateTime selectedWeekStart { get; set; }
        public string[] requiredWorkgroups { get; set; }
        public string workgroupNames { get; set; }
        public int dayStartTimeHours { get; set; }
        public int dayStartTimeMinutes { get; set; }
        public int nightStartTimeHours { get; set; }
        public int nightStartTimeMinutes { get; set; }            
    }

    public class PlannedScheduleSummaryReply : JDAMessage<IEnumerable<JDADepartmentSummary>>
    {
    }

    [ConsortiumRoute("data/sitemgr/CustomServices/Workgroups.asmx/GetWorkgroups?siteId={SiteID}","POST")]
    public class WorkgroupsBySiteRequest
    {
        public string SiteID { get; set; }
    }

    public class WorkgroupsBySiteResponse : JDAMessage<List<JDANameAndID>>
    {
    }
}