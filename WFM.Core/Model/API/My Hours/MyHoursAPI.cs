﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;
using WFM.Shared.Constants;

namespace WFM.Core.MyHours
{
    [ConsortiumRoute("/GetDemand?storeNumber={StoreNumber}&weekNumber={WeekNumber}", HttpVerb.Get)]
    public class GetDemandDetailsRequest
    {
        public string StoreNumber { get; set; }
        public string WeekNumber { get; set; } 
    }

    public class GetDemandDetailsResponse : List<MyHoursData>
    {

    }

    [ConsortiumRoute("/ResetDemand?storeNumber={StoreNumber}&weekNumber={WeekNumber}", HttpVerb.Post)]
    public class ResetDemandDetailsRequest
    {
        public string StoreNumber { get; set; }
        public string WeekNumber { get; set; } 
    }

    [ConsortiumRoute("/SaveDemand", HttpVerb.Post)]
    public class SaveDemandDetailsRequest : List<MyHoursData>
    {

    }
}