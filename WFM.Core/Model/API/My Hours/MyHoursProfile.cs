﻿using System;
using AutoMapper;
using System.Collections.Generic;

namespace WFM.Core
{
    public class MyHoursProfile : Profile
    {
       protected override void Configure ()
       {
            ConfigureDepartmentMapping ();
            ConfigureDTOMapping ();
       }

        static void ConfigureDepartmentMapping ()
        {
            //DTO to model
            Mapper.CreateMap<MyHoursDepartment,MyHours.Department> ()
                .ForMember (dest => dest.DepartmentCode,
                o => o.MapFrom (src => src.Code))
                .ForMember (dest => dest.DepartmentName,
                o => o.MapFrom (src => src.Description))
                .ForMember (dest => dest.TargetHours,
                o => o.MapFrom (src => src.TotalTargetHours))
                .ForMember (dest => dest.AllowedHours,
                o => o.MapFrom (src => src.TotalAllowedHours))
                .ForMember (dest => dest.PlannedHours,
                o => o.MapFrom (src => src.TotalAdjustedAllowedHours))
                .ForMember (dest => dest.Sales,
                o => o.MapFrom (src => src.InitialSalesBudget))
                .ForMember (dest => dest.Days,
                o => o.ResolveUsing (src =>
                    {
                        MyHours.Day[] days = new WFM.Core.MyHours.Day[7];

                        for(int i = 0; i < 7; ++i)
                        {
                            WFM.Core.MyHours.Day newDay = null;
                            if(src.AdjustedAllowedHours != null && src.AdjustedAllowedHours.Length > i)
                            {
                                var adjustedHours = src.AdjustedAllowedHours[i];
                                newDay = new WFM.Core.MyHours.Day(adjustedHours.Percentage / 100, adjustedHours.Hours, src.TotalAdjustedAllowedHours, src.InitialSalesBudget, adjustedHours.Date, adjustedHours.Day);
                            }

                            if(newDay == null)
                            {
                                string day = GetMyHoursDayString(i);
                                newDay= new WFM.Core.MyHours.Day (0, 0, 0, 0, src.AdjustedAllowedHours[0].Date + i, day);
                            }
                            if(src.ChangeIndicator != null && src.ChangeIndicator.Length > i)
                            {
                                var changeIndicator = src.ChangeIndicator[i];
                                newDay.CentralChangeIndicator = changeIndicator.CentralChange;
                                newDay.DayPerChangeIndicator = changeIndicator.DayPercentageChange;
                                newDay.WeekChange = changeIndicator.WeekChange;
                            }
                            days[i] = newDay;
                        }
                            
                        return days;
                    }));

            //Model to DTO
            Mapper.CreateMap<MyHours.Department,MyHoursDepartment> ()
                .ForMember (dest => dest.Code,
                o => o.MapFrom (src => src.DepartmentCode))

                .ForMember (dest => dest.Description,
                o => o.MapFrom (src => src.DepartmentName))


                .ForMember (dest => dest.TotalTargetHours,
                o => o.MapFrom (src => src.TargetHours))
                .ForMember (dest => dest.TotalAllowedHours,
                o => o.MapFrom (src => src.AllowedHours))
                .ForMember (dest => dest.TotalAdjustedAllowedHours,
                o => o.MapFrom (src => src.PlannedHours))
                .ForMember (dest => dest.InitialSalesBudget,
                o => o.MapFrom (dest => dest.Sales))         
                .ForMember (dest => dest.AdjustedAllowedHours,
                    o => o.ResolveUsing (src =>
                        {
                            List<MyHoursAdjustedAllowedHours> days = new List<MyHoursAdjustedAllowedHours>();

                            for (int day = 0; day < 7; ++day)
                            {
                                if(src.Days[day] == null)
                                    continue;
                                    
                                MyHoursAdjustedAllowedHours newDay = new MyHoursAdjustedAllowedHours ()
                                {
                                    Date = src.Days[day].Date,
                                    Day = src.Days[day].DayOfWeek,
                                    Hours = src.Days[day].PlannedHours,
                                    Percentage = src.Days[day].Percentage * 100,
                                };
                                days.Add(newDay);
                            }
                            return days.ToArray();
                        }))
                .ForMember(dest => dest.ChangeIndicator,
                    o => o.ResolveUsing (src =>
                        {
                            List<MyHoursChangeIndicator> days = new List<MyHoursChangeIndicator>();

                            for (int day = 0; day < 7; ++day)
                            {
                                if(src.Days[day] == null)
                                    continue;
                                
                                MyHoursChangeIndicator newDay = new MyHoursChangeIndicator ()
                                    {
                                        Date = src.Days[day].Date,
                                        Day = src.Days[day].DayOfWeek,
                                        CentralChange = src.Days[day].CentralChangeIndicator,
                                        DayPercentageChange = src.Days[day].DayPerChangeIndicator,
                                        WeekChange = src.Days[day].WeekChange,
                                    };
                                days.Add(newDay);
                            }
                            return days.ToArray();
                        }));
        }

        static void ConfigureDTOMapping()
        {
            //DTO to model
            Mapper.CreateMap<MyHoursData,MyHours.WeekData> ()
                .ForMember (dest => dest.AllowedHours,
                    o => o.MapFrom(src => src.TotalAllowedHours))
                .ForMember (dest => dest.Departments,
                    o => o.MapFrom(src => src.Department))
                .ForMember(dest => dest.PlannedHours,
                    o => o.MapFrom(src => src.TotalAdjustedAllowedHours))
                .ForMember (dest => dest.Sales,
                    o => o.MapFrom(src => src.TotalSales))
                .ForMember (dest => dest.Store,
                    o => o.MapFrom(src => src.StoreNumber))
                .ForMember (dest => dest.TargetHours,
                    o => o.MapFrom(src => src.TotalTargetHours))
                .ForMember (dest => dest.Week,
                    o => o.MapFrom(src => src.Week));

            //Model to DTO
            Mapper.CreateMap<MyHours.WeekData,MyHoursData> ()
                .ForMember (dest => dest.TotalAllowedHours,
                    o => o.MapFrom(src => src.AllowedHours))
                .ForMember (dest => dest.Department,
                    o => o.MapFrom(src => src.Departments))
                .ForMember (dest => dest.TotalAdjustedAllowedHours,
                    o => o.MapFrom(src => src.PlannedHours))
                .ForMember (dest => dest.TotalSales,
                    o => o.MapFrom(src => src.Sales))
                .ForMember (dest => dest.StoreNumber,
                    o => o.MapFrom(src => src.Store))
                .ForMember (dest => dest.TotalTargetHours,
                    o => o.MapFrom(src => src.TargetHours))
                 .ForMember (dest => dest.ForecastedSales,
                    o => o.MapFrom (src => src.ForecastedSales))
                .ForMember (dest => dest.Week,
                    o => o.MapFrom(src => src.Week));
        }

        public static string GetMyHoursDayString(int i)
        {
            DayOfWeek day = DayOfWeek.Monday;
            switch (i)
            {
                case 0:
                    day = DayOfWeek.Monday;
                    break;
                case 1:
                    day = DayOfWeek.Tuesday;
                    break;
                case 2:
                    day = DayOfWeek.Wednesday;
                    break;
                case 3:
                    day = DayOfWeek.Thursday;
                    break;
                case 4:
                    day = DayOfWeek.Friday;
                    break;
                case 5:
                    day = DayOfWeek.Saturday;
                    break;
                case 6:
                    day = DayOfWeek.Sunday;
                    break;
                default:
                    break;
            }

            return day.ToString();
        }
    }
}