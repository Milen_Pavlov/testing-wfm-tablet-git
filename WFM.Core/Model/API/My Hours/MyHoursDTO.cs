﻿using System;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class MyHoursData
    {
        public int StoreNumber { get; set; }
        public int Week { get; set; }
        public decimal TotalTargetHours { get; set; }
        public decimal TotalAllowedHours { get; set; }
        public decimal TotalSales { get; set; }
        public decimal ForecastedSales { get; set; }
        public decimal TotalAdjustedAllowedHours { get; set; }

        public MyHoursDepartment[] Department { get; set; }
    }

    public class MyHoursDepartment
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal TotalTargetHours { get; set; }
        public decimal TotalAllowedHours { get; set; }
        public decimal TotalAdjustedAllowedHours { get; set; }
        public decimal InitialSalesBudget { get; set; }
               
        public MyHoursChangeIndicator[] ChangeIndicator { get; set; }
        public MyHoursAdjustedAllowedHours[] AdjustedAllowedHours { get; set; }
    }

    public class MyHoursChangeIndicator
    {
        public int Date { get; set; } // Format yyyyMMdd
        public string Day { get; set; }
        public int CentralChange { get; set; }
        public int DayPercentageChange { get; set; }
        public int WeekChange { get; set; }
    }

    public class MyHoursAdjustedAllowedHours
    {
        public int Date { get; set; } // Format yyyyMMdd
        public string Day { get; set; }
        public decimal Hours { get; set; }
        public decimal Percentage { get; set; }
    }
}

