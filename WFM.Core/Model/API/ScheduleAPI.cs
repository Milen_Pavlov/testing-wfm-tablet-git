﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("data/sitemgr/services/Schedule.asmx/GetScheduleData","POST")]
    public class ScheduleDataRequest
    {
        public int siteID { get; set; }
        public DateTime startDay { get; set; }
        public string filter { get; set; }
        public int[] filterIDs { get; set; }
        public int budgetID { get; set; }
        public int metricID { get; set; }
    }

    [ConsortiumRoute("data/sitemgr/services/Schedule.asmx/GetScheduleDataForEmployee","POST")]
    public class EmployeeScheduleDataRequest
    {
        public int siteID { get; set; }
        public DateTime startDay { get; set; }
        public int employeeID { get; set; }
    }

    public class ScheduleDataReply : JDAMessage<JDAScheduleData>
    {
    }

    [ConsortiumRoute("data/sitemgr/CustomServices/Schedule.asmx/GetScheduleData?siteId={siteID}&subsites=0","POST")]
    public class ScheduleDataCustomRequest
    {
        public int siteID { get; set; }
        public DateTime startDay { get; set; }
        public string filter { get; set; }
        public int[] filterIDs { get; set; }
        public int budgetID { get; set; }
        public int metricID { get; set; }
    }

    [ConsortiumRoute("data/sitemgr/services/Schedule.asmx/GetSalesForecast","POST")]
    public class SalesForecastRequest
    {
        public int siteID { get; set; }
        public int forecastGroupID { get; set; }
        public int metricID { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }

    public class SalesForecastReply : JDAMessage<decimal>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/Schedule.asmx/GetAvailableEmployees","POST")]
    public class AvailableEmployeesRequest
    {
        public int siteID { get; set; }
        public DateTime shiftStartTime { get; set; }
        public DateTime shiftEndTime { get; set; }
        public int jobID { get; set; }
    }

    public class AvailableEmployeesReply : JDAMessage<List<JDAAvailableEmployeeData>>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/Schedule.asmx/GetScheduleDataByScheduleID","POST")]
    public class ScheduleDataByScheduleIDRequest
    {
        public int schedID { get; set; }
        public bool excludeRoles { get; set; }
    }

    public class ScheduleDataByScheduleIDResponse : JDAMessage<List<JDASchedulePunches>>
    {
    }
}
