﻿using System;
using Consortium.Model.Engine;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumRoute("/", "GET")]
    public class SessionInitRequest
    {
        
    }

    [ConsortiumRoute("/data/login", "POST")]
    public class SessionLoginRequest
    {
        public string loginName { get; set; }
        public string password { get; set; }
    }

    public class SessionLoginReply : JDAMessage<JDALogin>
    {
    }

    [ConsortiumRoute ("/data/login", "POST")]
    public class SessionTokenRequest
    {
        public string rpopentoken { get; set; }
    }

    [ConsortiumRoute("/rp/logout", "GET")]
    public class SessionLogoutRequest
    {
    }

    [ConsortiumRoute("/data/changePassword", "POST")]
    public class ChangePasswordRequest
    {
        public string oldPwd { get; set; }
        public string newPwd { get; set; }
        public string confirmPwd { get; set; }
    }

    public class JDAChangePasswordResponse : JDAMessage<object>
    {        
    }

    [ConsortiumRoute("data/rp/admin/sessionKeepAlive","POST")]
    public class SessionKeepAliveRequest
    {
        public int siteID { get; set; }
    }

    public class SessionKeepAliveReply : JDAMessage<object>
    {
    }

    [ConsortiumRoute("data/sitemgr/services/FeatureConfiguration.asmx/GetFeatureConfiguration?siteId={siteId}","POST")]
    public class GetFeatureConfigurationRequest
    {
        public int siteId { get; set; }
    }

    public class GetFeatureConfigurationReply : JDAMessage<List<JDAConfig>>
    {
    }

    // TODO: Add GetFeatureConfigBoolValueForSessionUserAndRole
    // Iain, this is passing settingName: "enableDepartmentFiltering", this is used by the website to determine whether to enable the setting or not
    // Currently I think we're using an appconfig value to determine this.
}
