using System;
using System.Linq;
using System.Collections.Generic;
using Cirrious.CrossCore;
using AutoMapper;

namespace WFM.Core
{
    public class EmployeeData
    {
        public bool VisibleInFilter { get; set; }
        public int? DayIndex { get; set; }
        public bool CanEdit { get; set; }
        public JDAEmployeeInfo Info { get; set; }
        public bool SpareShift { get; set; }

        public int EmployeeID { get; private set; }
        public string Name { get; private set; }
        public DateTime WeekStart { get; private set; }
        public JDAMinorRule MinorRule { get; private set; } 
        public bool IsMinor { get; private set; }
        public int? ScheduleRule { get; private set; }
        public JDAScheduleConstraints Constraints { get; private set; }
        public List<JDAEmployeeAttribute> Attributes { get; private set; }
        public Dictionary<int, JDAEmployeeAssignedJob> CanWorkJobs { get; private set; }
        public IList<JobData> AllowableJobs { get; private set; }
        public List<EmployeeDayData> Days { get; private set; }
        public EmployeeDayData LastDayPreviousWeek { get; private set; }

        public List<ShiftData> DeletedShifts { get; private set; }
        public List<WarningData> DeletedDailyWarnings { get; private set; }
        public List<WarningData> DeletedWeeklyWarnings { get; private set; }

        public List<WarningData> DailyWarnings { get; private set; }
        public List<WarningData> WeeklyWarnings { get; private set; }
        
        public List<ShiftData> Shifts { get; private set; }
        public List<ShiftData> PreviousWeekShifts { get; private set; }

        // Metrics
        public double Hours { get; private set; }
        public double DaysWorked { get; private set; }
        public double Cost { get; private set; }
        public int MaxJobCount { get; private set; }
        public int WarningCount 
        { 
            get; 
            private set; 
        }

        public EmployeeData()
        {
            Info = new JDAEmployeeInfo ();
            Constraints = new JDAScheduleConstraints ();
            Attributes = new List<JDAEmployeeAttribute> ();
            CanWorkJobs = new Dictionary<int, JDAEmployeeAssignedJob> ();
            AllowableJobs = new List<JobData> ();
            Days = new List<EmployeeDayData> ();
            DeletedShifts = new List<ShiftData> ();
            DeletedDailyWarnings = new List<WarningData> ();
            DeletedWeeklyWarnings = new List<WarningData> ();
            DailyWarnings = new List<WarningData> ();
            WeeklyWarnings = new List<WarningData> ();
        }

        public EmployeeData(JDAEmployee e, IEnumerable<JDAJob> jobRoles, DateTime weekStart)
        {
            DeletedShifts = new List<ShiftData> ();
            DeletedDailyWarnings = new List<WarningData> ();
            DeletedWeeklyWarnings = new List<WarningData> ();
            
            Shifts = new List<ShiftData>();
            PreviousWeekShifts = new List<ShiftData>();

            EmployeeID = e.EmployeeID;
            Name = e.FullName;

            IsMinor = e.IsMinor;
            MinorRule = e.MinorRule;

            WeekStart = weekStart;
            WeeklyWarnings = Mapper.Map<List<JDAWarning>,List<WarningData>>(e.WeeklyWarnings);
            DailyWarnings = Mapper.Map<List<JDAWarning>,List<WarningData>>(e.DailyWarnings);
            Constraints = e.ScheduleConstraints;
            ScheduleRule = e.ScheduleRuleID;
            Attributes = e.Attributes;

            //Necessary for a special case of JDA warning type that screws up our implmentation
            PostProcessWeeklyWarnings ();

            // Init metrics
            Hours = 0;
            DaysWorked = 0;
            Cost = 0;
            MaxJobCount = 0;
            WarningCount = 0;

            // Create a job Lookup
            CanWorkJobs = new Dictionary<int, JDAEmployeeAssignedJob>();
            foreach (var job in e.CanWorkJobs)
                CanWorkJobs.Add (job.JobID, job);

            // Create the days
            Days = new List<EmployeeDayData>();
            for (int i = 0; i < 7; ++i)
            {
                EmployeeDayData dayData = new EmployeeDayData(Shifts)
                {
                    DayStart = weekStart.AddDays(i),
                };
                
                Days.Add(dayData);
            }

            LastDayPreviousWeek = new EmployeeDayData(PreviousWeekShifts) // Used to display overnight shift data that runs over from the previous week
            {
                DayStart = weekStart.AddDays(-1),
            };

            // Add Shifts and split per da
            foreach (JDAShift s in e.Shifts)
            {
                ShiftData shiftData = Mapper.Map<JDAShift, ShiftData> (s);
                
                foreach (var job in shiftData.Jobs)
                {
                    job.SetOriginalValues ();
                }

                // Copy shift details
                foreach (JDAJobAssignment j in s.Jobs)
                {
                    foreach (var b in j.Breaks)
                        shiftData.AddDetail(Mapper.Map<JDAShiftDetail, ShiftDetailData> (b), false);

                    foreach (var m in j.Meals)
                        shiftData.AddDetail(Mapper.Map<JDAShiftDetail, ShiftDetailData> (m), false);
                    
                    //Add roles as shift details
                    foreach (var role in j.Roles.Where(r => r.Start != DateTime.MinValue))
                    {
                        shiftData.AddDetailWithoutState(ShiftDetailData.CreateRoleDetail(role));
                    }
                }
                
                shiftData.Modified = false;

                shiftData.SetWarnings (s.Warnings);

                shiftData.AssignDepartmentsToJobs(jobRoles);

                AddShift (shiftData);
            }

            foreach (var u in e.ScheduleConstraints.UnavailabilityRanges)
                AddUnavailability (u);

            foreach (var f in e.ScheduleConstraints.FixedShifts)
                AddFixedShift (f);
        }

        public void CalculateAllowableJobs(IList<JDAJob> allJobs)
        {
            var allowableJobIds = CanWorkJobs.Select(j => j.Key).ToList();
            AllowableJobs = allJobs.Where(j => allowableJobIds.Contains(j.JobID)).Select(j => new JobData(j)).ToList();
        }

        public ShiftData AddDefaultShift(int lengthHours, DateTime dayStart, DateTime shiftMidpoint, List<JDAJob> jobRoles)
        {
            if (CanWorkJobs == null || CanWorkJobs.Count == 0 || lengthHours == 0)
                return null;

            var startTime = ScheduleTime.ClampInterval(shiftMidpoint.AddHours (-(lengthHours/2)));

            // Get limits of this space
            DateTime min = shiftMidpoint.Date;
            DateTime max = startTime.AddDays(1);

            foreach (var day in Days)
            {
                foreach (var shift in day.Shifts)
                {
                    if (shift.End > min && shift.End < shiftMidpoint)
                    {
                        min = shift.End;
                    }
                    else if (shift.Start < max && shift.Start > shiftMidpoint)
                    {
                        max = shift.Start;
                    }
                }
            }

            // Does the shift fit?
            if ((max - min).TotalHours < lengthHours)
                return null;


            var jobId = CanWorkJobs.Where(p => p.Value.IsPrimary == true).Select(p => p.Value.JobID).FirstOrDefault();
            JDAJob job = jobRoles.Where (x => x.JobID == jobId).FirstOrDefault();

            if (job == null)
                return null;

            var addJob = JobData.Create (job);
            addJob.Start = startTime >= min ? startTime : min;
            addJob.End = addJob.Start.AddHours(lengthHours);

            if (addJob.End > max)
            {
                addJob.End = max;
                addJob.Start = max.AddHours(-lengthHours);
            }

            var addShift = ShiftData.Create();
            addShift.EmployeeID = Info.EmployeeID;
            addShift.Start = addJob.Start;
            addShift.End = addJob.End;
            addJob.ScheduledJobID = addShift.ShiftID;
            addShift.Jobs.Add(addJob);

            AddShift (addShift);

            return addShift;
        }

        public ShiftData AddShift(DateTime shiftStart, DateTime shiftEnd, List<JDAJob> jobRoles)
        {
            if (CanWorkJobs == null || CanWorkJobs.Count == 0)
                return null;

            var jobId = CanWorkJobs.Where(p => p.Value.IsPrimary == true).Select(p => p.Value.JobID).FirstOrDefault();
            JDAJob job = jobRoles.Where (x => x.JobID == jobId).FirstOrDefault();

            if (job == null)
                return null;

            var addJob = JobData.Create (job);
            addJob.Start = shiftStart;
            addJob.End = shiftEnd;

            var addShift = ShiftData.Create();
            addShift.EmployeeID = Info.EmployeeID;
            addShift.Start = addJob.Start;
            addShift.End = addJob.End;
            addJob.ScheduledJobID = addShift.ShiftID;
            addShift.Jobs.Add(addJob);

            AddShift (addShift);

            return addShift;
        }

        public ShiftData AssignToUnfilledShift(JDAShift shift)
        {
            if (CanWorkJobs == null || CanWorkJobs.Count == 0)
                return null;

            var filledShift = ShiftData.Create(shift);
            filledShift.EmployeeID = Info.EmployeeID;

            foreach (var job in shift.Jobs)
            {
                var addJob = JobData.Create(job);
                addJob.Roles = new List<JDAShiftDetail>();

                filledShift.Start = addJob.Start;
                filledShift.End = addJob.End;

                //add roles to job if any
                foreach(var role in job.Roles)
                {
                    addJob.Roles.Add(role);
                }

                filledShift.Jobs.Add(addJob);
            }

            filledShift.Edit = EditState.Added;
            filledShift.Modified = true; 

            AddShift(filledShift);
            return filledShift;
        }

        public void RemoveFromFilledShift(ShiftData shift)
        {
            RemoveShift(shift.ShiftID);
        }

        public bool AssignShift(ShiftData shift)
        {
            if (DoesOverlap (shift.ShiftID, shift.Start, shift.End))
                return false;

            shift.EmployeeID = EmployeeID;

            AddShift(shift);

            return true;
        }

        public bool SwapShiftWithEmployee (ShiftData fromShift, ShiftData toShift, EmployeeData fromEmployee)
        {
            //Don't check overlaps, because there obviously is! Validator will say if it was ok before getting here
            fromShift.EmployeeID = EmployeeID;
            fromShift.Edit = EditState.Modified;
            fromShift.Modified = true;

            toShift.EmployeeID = fromEmployee.EmployeeID;
            toShift.Edit = EditState.Modified;
            toShift.Modified = true;

            foreach (var job in fromShift.Jobs)
            {
                job.Edit = EditState.Modified;

                foreach(var role in job.Roles)
                {
                    role.ScheduledJobID = job.ScheduledJobID;
                    role.EditState = EditState.Modified;
                }
            }

            foreach (var job in toShift.Jobs)
            {
                job.Edit = EditState.Modified;
            }

            fromShift.OriginalEmployeeId = fromEmployee.EmployeeID;
            toShift.OriginalEmployeeId = EmployeeID;

            RemoveShift (toShift.ShiftID);
            fromEmployee.RemoveShift (fromShift.ShiftID);

            AddShift (fromShift);
            fromEmployee.AddShift (toShift);

            return true;
        }

        public bool ReassignShift(ShiftData shift, EmployeeData fromEmployee)
        {
            if (DoesOverlap (shift.ShiftID, shift.Start, shift.End))
                return false;

            shift.EmployeeID = EmployeeID;
			shift.Edit = EditState.Modified;
            shift.Modified = true;

			foreach(var job in shift.Jobs)
			{
				job.Edit = EditState.Modified;

                foreach(var role in job.Roles)
                {
                    role.EditState = EditState.Modified;
                    role.ScheduledJobID = job.ScheduledJobID;
                }
			}

            if(fromEmployee != null)
                shift.OriginalEmployeeId = fromEmployee.EmployeeID;

            AddShift(shift);

            var originalShift = fromEmployee.GetShift(shift.ShiftID);
            fromEmployee.RemoveShift(originalShift.ShiftID);

            return true;
        }

        public bool UpdateShift(ShiftData shift)
        {
            if(shift != null)
            {
                if (DoesOverlap (shift.ShiftID, shift.Start, shift.End))
                    return false;

                var shiftDay = Days.FirstOrDefault(day => day.DayStart.Date == shift.Start.Date);

                if(shiftDay != null)
                {
                    return shiftDay.TryUpdateShift(shift);        
                }

                return false;
            }

            return false;
        }

        public void ResetEmployeeWarnings()
        {
            foreach(var weekly in WeeklyWarnings)
            {
                if(!DeletedWeeklyWarnings.Contains(weekly) && weekly.InDB == true)
                {
                    DeletedWeeklyWarnings.Add(weekly);
                }
            }

            WeeklyWarnings = new List<WarningData>();

            foreach(var daily in DailyWarnings)
            {
                if(!DeletedDailyWarnings.Contains(daily) && daily.InDB == true)
                {
                    DeletedDailyWarnings.Add(daily);
                }
            }

            DailyWarnings = new List<WarningData>();
        }          

        void PostProcessWeeklyWarnings()
        {
            //Warning type 44 (consecutive days cross weeks) screws up existing implementation.
            //For read only mode, hide any matching warning not in the current week as JDA uses the other warning as a marker.
            this.WeeklyWarnings.Where (warning => (warning.ExceptionDefinitionID == (int)JDAWarningType.MaxConsecutiveDaysCross && warning.WeekStartBusinessDate != WeekStart))
            .Each (warning =>
            {
                    warning.Display = false;
            });
        }

        public void AddWeeklyWarning(WarningData warning)
        {
            WeeklyWarnings.Add(warning);
        }

        public void AddDailyWarning(WarningData warning)
        {
            DailyWarnings.Add(warning);
        }

        private void RemoveShift(int shiftID)
        {
            foreach (var day in Days)
            {
                foreach (var shift in day.Shifts)
                {
                    if (shiftID == shift.ShiftID)
                    {
                        Shifts.Remove (shift);
                        return;
                    }
                }
            }
        }

        public void DeleteShift(int shiftID, bool ignoreAdded = false)
        {
            foreach (var day in Days)
            {
                foreach (var shift in day.Shifts)
                {
                    if (shiftID == shift.ShiftID)
                    {
                        // Only track shifts that havent just been added, unless we're explicitly deleting just added UnsavedShift (not synchronised with JDA)
                        if (shift.Edit != EditState.Added || ignoreAdded)
                        {
                            DeletedShifts.Add (shift);
                        }

                        shift.DeleteAllJobs ();
                        shift.DeleteAllDetails ();
                        shift.Edit = EditState.Deleted;
                        shift.Modified = true;
                        Shifts.Remove (shift);
                        return;
                    }
                }
            }
        }

        public bool CheckIfShiftOnPreviousWeek(int shiftID, int? dayIndex)
        {
            // Check last day previous week for shift
            foreach (var shift in LastDayPreviousWeek.Shifts)
            {
                if (shiftID == shift.ShiftID)
                    return true;
            }

            return false;
        }

        public ShiftData GetShift(int shiftID)
        {
            // Check day data for shift
            foreach (var day in Days)
            {
                foreach (var shift in day.Shifts)
                {
                    if (shiftID == shift.ShiftID)
                        return shift;
                }
            }

            return null;
        }

        public IEnumerable<ShiftData> GetShiftsOnDay(DateTime shiftDay)
        {
            return Days.Where(day => day.DayStart.Date == shiftDay.Date).SelectMany(day => day.Shifts);
        }

        public IEnumerable<ShiftData> GetOverlapShifts (DateTime start, DateTime end)
        {
            return Days.SelectMany (day => day.Shifts).Where (x => x.Start < end && start < x.End);
        }

        // Calculate hours worked and cost per day and in total
        public void CalculateMetrics(JDABusinessUnitSettings buSettings, List<JDAScheduleRule> rules)
        {
            Hours = 0;
            DaysWorked = 0;
            Cost = 0;
            MaxJobCount = 0;
            WarningCount = 0;

            // Initialise schedule rule variables
            JDAScheduleRule rule = FindMatchingScheduleRule(rules);
            bool scheduleRuleExists = true;
            int mealMinutesPaid = 0;
            int breakMinutesPaid = 0;
            bool mealPaidBU = buSettings.IsMealPaid;
            bool breakPaidBU = buSettings.IsBreakPaid;
            JDAScheduleAllocation allocation = null;

            if (rule == null)
            {
                Mvx.Trace("No matching schedule rule found for employee, using default BU metrics instead");
                scheduleRuleExists = false;
            }

            foreach (var daily in DailyWarnings)
            {
                if (daily.Display)
                    ++WarningCount;
            }

            foreach (var week in WeeklyWarnings)
            {
                if (week.Display)
                    ++WarningCount;
            }

            bool dayWorked = false;
            foreach (var day in Days)
            {
                dayWorked = false;
                day.Hours = 0;
                day.Cost = 0;

                // Add scheduled time
                int jobsInDay = 0;

                foreach (var shift in day.Shifts)
                {
                    dayWorked = true;
                    WarningCount += shift.Warnings.Count;
                    jobsInDay += shift.Jobs.Count;

                    if (shift.Start < day.EarliestStart)
                        day.EarliestStart = shift.Start;

                    // Find meal time paid and break time paid lengths on a per shift basis, finding best allocation match from rule
                    if(scheduleRuleExists)
                    {
                        allocation = shift.FindBestMatchAllocationFromRule(rule);

                        if(allocation != null)
                        {
                            mealMinutesPaid = allocation.MealDurationPaid;
                            breakMinutesPaid = allocation.BreakDurationPaid;
                        }
                        else
                        {
                            Mvx.Trace("No matching allocation found for schedule rule, using default BU metrics instead");
                            scheduleRuleExists = false;
                        }
                    }

                    foreach (JobData job in shift.Jobs)
                    {
                        
                        var duration = CalculateJobDuration(shift, job, scheduleRuleExists, breakMinutesPaid, mealMinutesPaid, breakPaidBU, mealPaidBU);

                        double hours = duration.TotalHours;
                        Hours += hours;
                        day.Hours += hours;

                        if(day.JobTypeHours.ContainsKey(job.JobID))
                            day.JobTypeHours[job.JobID] += hours;
                        else
                            day.JobTypeHours.Add(job.JobID, hours);

                        // Add up the cost
                        JDAEmployeeAssignedJob info;
                        if (CanWorkJobs.TryGetValue (job.JobID, out info))
                        {
                            double amount = hours * info.Rate;
                            Cost += amount;
                            day.Cost += amount;
                        }
                    }
                }

                // Select largest number of jobs for deciding row height
                MaxJobCount = Math.Max (jobsInDay, MaxJobCount);

                if(dayWorked)
                {
                    DaysWorked++;
                }
            }
        }

        // Calculate breaks for a shift
        //Should only be called on a resize or shift creation, not on moving or updating
        //This applies the break rules to a shift
        public void CalculateDetails(int shiftID, List<JDAScheduleRule> rules)
        {
            var shift = GetShift (shiftID);

            if (shift == null)
                return;
                
            if (!ScheduleRule.HasValue)
            {
                Mvx.Trace("Shift has no associated schedule rule, removing all breaks and meals");
                shift.DeleteAllDetails();
                return;
            }

            // Find the Rule
            JDAScheduleRule rule = rules.Where (x => x.ScheduleRuleID == ScheduleRule.Value).FirstOrDefault ();

            if (rule == null)
            {
                Mvx.Trace("No matching schedule rule found, removing all breaks and meals");
                shift.DeleteAllDetails();
                return;
            }

            shift.CalculateDetails (rule);
        }

        public void CalculateWarnings()
        {
            ResetEmployeeWarnings();

            ScheduleWarnings.AddDailyWarnings (this);
            ScheduleWarnings.AddWeeklyWarnings (this);

            foreach (ShiftData shift in Days.SelectMany(day => day.Shifts))
            {
                shift.ClearWarnings();

                ScheduleWarnings.AddShiftWarnings(this, shift);
            }
        }

        public string GetAttributes()
        {
            var attributes = Attributes.Select (x => x.Code).ToList();

            if (IsMinor && MinorRule != null)
            {
                attributes.Add (MinorRule.Code ?? "M");
            }

            if (Info != null && Info.IsBorrowed)
            {
                attributes.Add ("b");
            }

            return string.Join(" ", attributes.Select(code => string.Format("[{0}]",code)));
        }

        // Generate a textual list of all warnings
        public List<string> GetAllWarnings()
        {
            var warnings = new List<string> ();

            foreach (var day in Days)
            {
                foreach (var shift in day.Shifts)
                {
                    for(int i=0; i<shift.Warnings.Count; ++i)
                        warnings.Add(ScheduleWarnings.GetWarning(shift.Warnings[i], shift.Start, Hours, DaysWorked, this));
                }
            }

            foreach (var daily in DailyWarnings)
            {                                
                if (daily.Display)
                {
                    var warning = ScheduleWarnings.GetWarning(daily.ExceptionDefinitionID, daily.BusinessDate, Hours, DaysWorked, this);
                    if(!string.IsNullOrEmpty(warning))
                    {
                        warnings.Add(warning);
                    }
                }
            }

            foreach (var week in WeeklyWarnings)
            {
                if (week.Display)
                    warnings.Add(ScheduleWarnings.GetWarning(week.ExceptionDefinitionID, week.BusinessDate, Hours, DaysWorked, this));
            }

            return warnings;
        }
 
        private int TimeToDayIndex(DateTime time)
        {            
            TimeSpan diff = time - WeekStart;
            return (int)Math.Floor(diff.TotalDays);
        }

        public bool DoesOverlap(int shiftID, DateTime start, DateTime end)
        {
            foreach (var day in Days)
            {
                foreach (var shift in day.Shifts)
                {
                    if (shiftID != shift.ShiftID && end > shift.Start && start < shift.End)
                        return true;
                }
            }

            return false;
        }

        public bool DoesOverlapIgnoringID (int shiftID, DateTime start, DateTime end, int ignoreID)
        {
            foreach (var day in Days) {
                foreach (var shift in day.Shifts) {
                    if (shiftID != shift.ShiftID && ignoreID != shift.ShiftID && end > shift.Start && start < shift.End)
                        return true;
                }
            }

            return false;
        }

        private void AddUnavailability(JDAInterval interval)
        {            
            var periodStart = Math.Max(TimeToDayIndex(interval.Start), 0);
            var hoursOffset = (WeekStart - WeekStart.Date).TotalHours;
            var periodLength = interval.End - interval.Start.AddHours(-hoursOffset);

            var totalDays = (int)Math.Min(Math.Ceiling(periodLength.TotalDays),7-periodStart);

            for (int day = 0; day < totalDays; day++)
                Days[periodStart + day].UnavailabilityRanges.Add (interval);
        }

        private void AddFixedShift(JDAEmployeeAssignedJob job)
        {
            int day = TimeToDayIndex(job.Start);

            if (day < 0 || day >= 7)
                return;

            Days [day].FixedShifts.Add (job);
        }

        private void AddShift(ShiftData shift)
        {
            int day = TimeToDayIndex (shift.Start);

            if (day >= 0 && day < 7)
            {
               Shifts.Add (shift);
            }
            else if(day == -1)
            {
                //if(TimeToDayIndex (shift.End) == 0)  // shift runs onto first day of week from previous week removed restriction since data is needed for calculating weekly warnings
                //{
                    PreviousWeekShifts.Add(shift);
                //}
            }

            var pending = DeletedShifts.FirstOrDefault(deletedShift => deletedShift.ShiftID == shift.ShiftID);

            if(pending != null)
            {
                DeletedShifts.Remove(pending);
            }
        }

        public bool CanWorkJob(JDAJobAssignment jDAJobAssignment)
        {
            int jobID = jDAJobAssignment.JobID;

            return CanWorkJobs.ContainsKey(jobID);
        }

        public bool CanWorkShift(ShiftData shift)
        {
            foreach (JobData job in shift.Jobs)
            {
                if (!CanWorkJobs.ContainsKey(job.JobID))
                    return false;
            }

            return true;
        }

        public bool AvailableForShiftTimes(DateTime shiftStart, DateTime shiftEnd)
        {
            return AvailableForShiftTimes(shiftStart, shiftEnd, new string[] { });
        }

        public bool TimeOffConflictForShiftTimes(DateTime shiftStart, DateTime shiftEnd)
        {
            return !AvailableForShiftTimes(shiftStart, shiftEnd, new string[] { "t" });
        }

        /// <summary>
        /// Is the employee availabile for the specified shift time?
        /// If forUnavailabilityType is null or empty, assumes all types are selected.
        /// If forUnavailabilityType contains types, matches only against the types specified.
        /// </summary>
        /// <returns><c>true</c>, if for shift times was availabled, <c>false</c> otherwise.</returns>
        /// <param name="shiftStart">Shift start.</param>
        /// <param name="shiftEnd">Shift end.</param>
        /// <param name="forUnavailabilityType">Types of unavailability filter on.</param>
        public bool AvailableForShiftTimes(DateTime shiftStart, DateTime shiftEnd, string[] forUnavailabilityType)
        {
            // Check if shift overlaps with any unavailability range
            IEnumerable<JDAInterval> allUnavailability = Days.SelectMany(day => day.UnavailabilityRanges);

            foreach(var unavailability in allUnavailability)
            {
                if (forUnavailabilityType == null 
                    || forUnavailabilityType.Length == 0 
                    || forUnavailabilityType.Contains(unavailability.Type))
                {
                    if (TimePeriodHelper.DoesOverlap(shiftStart, shiftEnd, unavailability.Start, unavailability.End))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public bool CanWorkShiftEvenIfSplit(DateTime shiftDate)
        {
            if(Constraints.CanWorkSplit)
            {
                return true; // Can work split
            }
            else
            {
                foreach(var day in Days)
                {
                    if(shiftDate == day.DayStart)
                    {
                        if(day.Shifts.Count > 0)
                        {
                            return false; // Cannot work split, and already shift on the day
                        }
                        else
                        {
                            return true; // Cannot work split, but no others shifts on the day
                        }
                    }
                }

                // Also check for a shift on the previous week
                if(LastDayPreviousWeek != null && shiftDate == LastDayPreviousWeek.DayStart)
                {
                    if(LastDayPreviousWeek.Shifts.Count > 0)
                    {
                        return false; // Cannot work split, and already shift on the day
                    }
                    else 
                    {
                        return true; // Cannot work split, but no others shifts on the day
                    }
                }

                return false; // Matching day data could not be found, disallow shift
            }
        }

        public MinHoursBetweenShiftsResult CanWorkShiftMinHoursBetweenDays(ShiftData shift)
        {
            var result = new MinHoursBetweenShiftsResult();
            if(Constraints.MinHoursBetweenShifts != null)
            {                                
                foreach(EmployeeDayData day in Days)
                {
                    if(day.DayStart != shift.Start.Date) // Only want to check against other days than the day of the shift
                    {
                        for(int i = 0; i < day.Shifts.Count; i++)
                        {
                            DateTime otherStart = day.Shifts [i].Start;
                            if(shift.Start > otherStart)
                            {
                                DateTime otherEndPlusMinBetween = day.Shifts[i] .End.AddHours((double)Constraints.MinHoursBetweenShifts);
                                if(otherEndPlusMinBetween > shift.Start)
                                {
                                    result.CreateWarning = true;
                                    result.HoursDifference = (otherEndPlusMinBetween - shift.Start).SnapTo15Minutes().TotalHours;
                                    return result;
                                }
                            }
                        }
                    }
                }

                //include PreviousWeekShifts in the checks
                foreach (var prevshift in PreviousWeekShifts) 
                {
                    if(shift.Start > prevshift.Start) 
                    {
                        DateTime otherEndPlusMinBetween = prevshift.End.AddHours((double)Constraints.MinHoursBetweenShifts);
                        if(otherEndPlusMinBetween > shift.Start)
                        {
                            result.CreateWarning = true;
                            result.HoursDifference = (otherEndPlusMinBetween - shift.Start).SnapTo15Minutes().TotalHours;
                            return result;
                        }
                    }
                }
            }

            result.CreateWarning = false;
            result.HoursDifference = 0;    
            return result;
        }

        public TimeSpan CalculateJobDuration(ShiftData shift, JobData job, bool scheduleRuleExists, int breakMinutesPaid, int mealMinutesPaid, bool breakPaidBU, bool mealPaidBU)
        {
            var duration = job.End - job.Start;
            var details = shift.GetDetailsForJob (job.ScheduledJobID);

            foreach (var detail in details)
            {
                if(scheduleRuleExists)
                {
                    int detailMinutes = (int) (detail.End.Value - detail.Start.Value).TotalMinutes;
                    if (detail.IsBreak && breakMinutesPaid < detailMinutes) 
                    {
                        duration =  duration.Subtract(new TimeSpan(0, detailMinutes - breakMinutesPaid, 0));
                    }
                    else if (detail.IsMeal && mealMinutesPaid < detailMinutes)
                    {
                        duration = duration.Subtract(new TimeSpan(0, detailMinutes - mealMinutesPaid, 0));
                    }
                }
                else
                {
                    if ((detail.IsBreak && !breakPaidBU) || (detail.IsMeal && !mealPaidBU))
                        duration -= (detail.End.Value - detail.Start.Value);
                }
            }

            return duration;
        }

        public JDAScheduleRule FindMatchingScheduleRule(List<JDAScheduleRule> rules)
        {
            if (ScheduleRule == null)
                return null;

            return rules.Where (x => x.ScheduleRuleID == ScheduleRule.Value).FirstOrDefault ();
        }

        public DayHoursData CalculateShiftHoursAndCostForFilter(int dayIndex, FilterGroup filter, List<JDAScheduleRule> rules, JDABusinessUnitSettings buSettings)
        {
            DayHoursData result = new DayHoursData();

            // Check if filter relevant, else just return day data
            if(filter == null || (filter.Type != FilterData.WORKGROUP_FILTER_CODE && filter.Type != PrimaryJobRoleFilterGroup.PRIMARYJOBROLE_FILTER_CODE))
            {
                result.Hours += Days[dayIndex].Hours;
                result.Cost += Days[dayIndex].Cost;
                return result;
            }

            foreach(var shift in Days[dayIndex].Shifts)
            {
                double jobHours = 0;
                double jobCost = 0;
                foreach(var job in shift.Jobs)
                {
                    int matchingJobs = 0;
                    switch(filter.Type)
                    {
                        case FilterData.WORKGROUP_FILTER_CODE:
                            matchingJobs = filter.Items.Where (d => d.ID == job.WorkgroupID && d.Selected == true).Count ();
                            break;
                        case FilterData.DEPARTMENT_FILTER_CODE:
                            matchingJobs = filter.Items.Where (d => d.ID == job.DepartmentID && d.Selected == true).Count ();
                            break;
                        case FilterData.ATTRIBUTES_FILTER_CODE:
                        case FilterData.LABORROLE_FILTER_CODE:
                        case FilterData.JOB_FILTER_CODE:
                            matchingJobs = filter.Items.Where (d => d.Name == job.JobName && d.Selected == true).Count ();
                            break;
                        default:
                            break;
                    }

                    // Check if job in filter group
                    if(matchingJobs > 0) // Only count hours and cost for jobs in filter
                    {
                        JDAScheduleRule rule = FindMatchingScheduleRule(rules);
                        bool mealPaidBU = buSettings.IsMealPaid;
                        bool breakPaidBU = buSettings.IsBreakPaid;
                        int mealMinutesPaid = 0;
                        int breakMinutesPaid = 0;

                        if(rule != null)
                        {
                            var allocation = shift.FindBestMatchAllocationFromRule(rule);
                            if(allocation != null)
                            {
                                mealMinutesPaid = allocation.MealDurationPaid;
                                breakMinutesPaid = allocation.BreakDurationPaid;
                            }
                            else
                            {
                                rule = null;
                            }
                        }

                        jobHours = CalculateJobDuration(shift, job, rule != null, breakMinutesPaid, mealMinutesPaid, breakPaidBU, mealPaidBU).TotalHours;

                        // Add up the cost
                        JDAEmployeeAssignedJob info;
                        jobCost = 0;
                        if (CanWorkJobs.TryGetValue (job.JobID, out info))
                        {
                            jobCost = jobHours * info.Rate;
                        }
                    }
                }
            
                result.Hours += jobHours;
                result.Cost += jobCost;

            }

            return result;
        }

        public double GetBreaksDurationForShift(List<JDAScheduleRule> rules, JDAEmployeeAssignedJob shift, double totalHours)
        {
            ShiftData newShift = new ShiftData ()
            {
                    Start = shift.Start,
                    End = shift.End.GetValueOrDefault(shift.Start),
            };
            
            JDAScheduleRule rule = FindMatchingScheduleRule (rules);
            newShift.CalculateDetails (rule, true);

            double totalBreaks = 0.0;
            foreach (var detail in newShift.Details)
            {
                if (detail.IsBreak || detail.IsMeal)
                {
                    totalBreaks += detail.Duration;
                }
            }
            // Convert to hours
            return totalBreaks / 60.0;
        }

   }
}