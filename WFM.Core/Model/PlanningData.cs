﻿using System;
using System.Linq;

namespace WFM.Core
{
    public class PlanningData
    {
        public int? DayIndex { get; set; }

        public string Name { get; set; }
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public double[] Variance { get; set; }

        public double[] DayMax { get; set; }
        public double[] DayOver { get; set; }
        public double[] DayUnder { get; set; }

        public double[] DayCoverage { get; set; }
        public double[] DayDemand { get; set; }
        public double[] DayScheduled { get; set; }

        public double[] RawDayOver { get; set; }
        public double[] RawDayUnder { get; set; }

        public double WeekMax { get; set; }

        public PlanningData()
        {
            Variance = new double[ScheduleTime.WeekIntervals];
            DayMax = new double[7];
            DayOver = new double[7];
            DayUnder = new double[7];

            DayCoverage = new double[7];
            DayDemand = new double[7];
            DayScheduled = new double[7];
            RawDayOver = new double[7];
            RawDayUnder = new double[7];
        }

        public bool IsThereAnUnderOfLength(int? forDay, int lengthInQuartiles)
        {
            return IsThereARunOfLength(false, forDay, lengthInQuartiles);

        }

        public bool IsThereAnOverOfLength(int? forDay, int lengthInQuartiles)
        {
            return IsThereARunOfLength(true, forDay, lengthInQuartiles);
        }

        public bool IsThereARunOfLength(bool over, int? forDay, int lengthInQuartiles)
        {
            int runStart = 0; 
            int runLength = ScheduleTime.WeekIntervals;

            if (forDay != null)
            {
                runStart = forDay.Value * ScheduleTime.DayIntervals;
                runLength = runStart + ScheduleTime.DayIntervals;
            }

            int overStartedAt = -1;

            for(int i = runStart; i < runLength; i++)
            {
                if ((over && Variance[i] > 0) || (!over && Variance[i] < 0))
                {
                    if (overStartedAt < 0)
                        overStartedAt = i;

                    if (i - overStartedAt + 1 >= lengthInQuartiles)
                    {
                        return true;
                    }
                }
                else
                {
                    overStartedAt = -1;
                }
            }

            return false;
        }


        public double GetDayAccuracy(int forDay)
        {
            double coverage = DayCoverage[forDay];
            double over = RawDayOver[forDay];
            double under = RawDayUnder[forDay];

            double divide = coverage + over + under; //TODO: Should this be set correctly elsewhere?
            return divide > 0.001 ? Math.Round((DayCoverage[forDay] / divide) * 100.0) : DayCoverage[forDay] == 0 ? 100.0 : 0;
        }

        public double GetWeekAccuracy()
        {
            double divide = DayCoverage.Sum() + (RawDayOver.Sum()) + (RawDayUnder.Sum());
            return divide > 0.001 ? Math.Round((DayCoverage.Sum() / divide) * 100.0) : DayCoverage.Sum() == 0 ? 100.0 : 0;
        }

        public void Smooth(double hours, double cap)
        {
            int intervals = ScheduleTime.HoursToIntervals (hours);
            double inverseCap = -cap;

            int row = (int)cap;

            // Iterate on each row (can optimise by lowering row based on known max)
            for (int i=row; i>0; --i)
            {
                int start = 0;
                int check = 0;

                while (start < ScheduleTime.WeekIntervals)
                {
                    // Skip to next starting value
                    while (start < ScheduleTime.WeekIntervals && Math.Abs(Variance [start]) < i)
                        ++start;

                    check = start + 1;

                    // Skip non zero values
                    while (check < ScheduleTime.WeekIntervals && Math.Abs(Variance [check]) >= i)
                    {
                        if (Variance [check] > cap)
                        {
                            Variance [check] = cap;
                        }
                        else if (Variance [check] < inverseCap)
                        {
                            Variance [check] = inverseCap;
                        }

                        ++check;
                    }

                    // Clear values if intervals not large enough
                    if (check - start < intervals && start < ScheduleTime.WeekIntervals)
                    {
                        for (int j = start; j < check; ++j)
                        {
                            if (Variance [j] == i)
                            {
                                Variance [j] = i - 1;
                            }
                            else if (Variance [j] == -i)
                            {
                                Variance [j] = -i + 1;
                            }
                        }
                    }

                    start = check;
                    ++check;
                }
            }

            // Calculate max variance
            WeekMax = 0;

            for (int i = 0; i < 7; ++i)
            {
                DayMax[i] = 0;

                for (int j = 0; j < ScheduleTime.DayIntervals; ++j)
                {
                    int index = (i * ScheduleTime.DayIntervals) + j;

                    if (Variance [index] < 0)
                        DayUnder [i] += (-Variance [index]) * 0.25;

                    if (Variance [index] > 0)
                        DayOver [i] += Variance [index] * 0.25;

                    DayMax [i] = Math.Max (DayMax [i], Math.Abs(Variance [index]));
                }

                WeekMax = Math.Max (WeekMax, DayMax [i]);
            }
        }
    }
}

