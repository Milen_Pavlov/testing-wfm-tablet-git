﻿//using System;
//namespace WFM.Core
//{
//    public class ApplicationConfiguration
//    {
//        /// <summary>
//        /// App timeout in minutes
//        /// </summary>
//        /// <value>The timeout in minutes.</value>
//        public int TimeoutMinutes
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// The timeout in seconds used for default rest requests
//        /// </summary>
//        public int RestClientTimeoutSecs
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// The timeout in seconds used for the schedule rest request
//        /// </summary>
//        public int ScheduleTimeoutSecs
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Frequency of sending a keep alive message to the JDA server in seconds
//        /// </summary>
//        public int KeepAliveFreqSecs
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Determines whether the week number is displayed in the schedule and till allocation views.
//        /// </summary>
//        public bool WeekNumberDisplayed
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show mobile phone numbers in the app
//        /// </summary>
//        /// <value><c>true</c> if show mobile numbers; otherwise, <c>false</c>.</value>
//        public bool ShowMobileNumbers
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show schedules in the side menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowScheduleMenu
//        {
//            get;
//            set; 
//        }

//        /// <summary>>
//        /// Whether or not to show the My Hours in the side menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowMyHours
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Whether or not to show schedules in the top menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowPlanningTab
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show schedules in the top menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowResourcesTab
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show breaks in the top menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowBreaksTab
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show timeoff menu in the side menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowTimeOffMenu
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show till allocation in the side menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowTillAllocationMenu
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show night aisle planner in the side menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowNightAislePlannerMenu
//        {
//            get;
//            set;
//        }

//        public bool ShowSiteNumber
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Can the user change their password when currently logged in?
//        /// </summary>
//        /// <value><c>true</c> if the app should show the change password option in the drawer; otherwise, <c>false</c>.</value>
//        public bool ShowChangePassword
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Is the schedule cost visible in on the schedule editor screen
//        /// </summary>
//        public bool ShowScheduleCost
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Whether or not to show approvals menu in the side menu
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowApprovals
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show unfill and calloff shift options
//        /// </summary>
//        /// <value><c>true</c> if shown; otherwise, <c>false</c>.</value>
//        public bool ShowUnfillOptions
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not the user can edit the schedule
//        /// </summary>
//        /// <value><c>true</c> can edit; otherwise, <c>false</c>.</value>
//        public bool CanEditSchedule
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Gets or sets a value indicating whether the user can mark breaks as taken using the consortium service.
//        /// </summary>
//        /// <value><c>true</c> if this instance can mark breaks as taken; otherwise, <c>false</c>.</value>
//        public bool CanMarkBreaksAsTaken
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Gets or sets a value indicating whether the schedule can be printed or not
//        /// </summary>
//        /// <value><c>true</c> if the schedule can be printed, otherwise <c>false</c>.</value>
//        public bool CanPrintSchedule
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Gets or sets a value indicating whether we use the alternetive schedule view, mixing the graph and the planning view
//        /// In a single resizable control.
//        /// </summary>
//        /// <value><c>true</c> if we should use the alternative schedule view; otherwise, <c>false</c>.</value>
//        public bool AlternativeScheduleView
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Whether or not to show the additional Till Buffer for shifting till allocations around
//        /// </summary>
//        /// <value><c>true</c> if show till buffer; otherwise, <c>false</c>.</value>
//        public bool ShowTillAllocationBuffer
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show the multiple Till Buffer for shifting till allocations around IF ShowTillAllocationBuffer is TRUE
//        /// </summary>
//        /// <value><c>true</c> if show multi till buffer; otherwise, <c>false</c>.</value>
//        public bool ShowMultipleTillAllocationBuffers
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show the popover Employee TimeOff View
//        /// </summary>
//        /// <value><c>true</c> if popover version; otherwise, <c>false</c> to show the full view.</value>
//        public bool ShowPopoverEmployeeTimeOffView
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show all employee history in Employee popover
//        /// </summary>
//        /// <value><c>true</c> if we show all history; otherwise, <c>false</c> to show upcoming only.</value>
//        public bool ShowAllEmployeeHistoryInPopover
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show breaks in the schedule view (in the diagrams, edit boxes etc)
//        /// </summary>
//        /// <value><c>true</c> if we show all history; otherwise, <c>false</c> to show upcoming only.</value>
//        public bool ShowScheduleDetails
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Whether or not to show a sales forecast
//        /// </summary>
//        /// <value><c>true</c> if show sales forecast; otherwise, <c>false</c>.</value>
//        public bool ShowSalesForecast
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Whether or not to show breaks in the schedule view (in the diagrams, edit boxes etc)
//        /// </summary>
//        /// <value><c>true</c> if we show all history; otherwise, <c>false</c> to show upcoming only.</value>
//        public bool ShowDrawerNotifications
//        {
//            get;
//            set; 
//        }

//        /// <summary>
//        /// Gets or sets a value indicating whether this <see cref="WFM.Core.ApplicationConfiguration"/> hide rows containing zero demand in variance table.
//        /// </summary>
//        /// <value><c>true</c> if hide zero demand; otherwise, <c>false</c>.</value>
//        public bool HideDefaultVarianceValues
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Gets or sets a value indicating whether the user can approve time off requests
//        /// </summary>
//        /// <value><c>true</c> if this instance can approve time off requests; otherwise, <c>false</c>.</value>
//        public bool CanApproveTimeOff
//        {
//            get;
//            set;
//        }


//        /// <summary>
//        /// Gets or sets a value indicating whether the user can approve their own time off requests.
//        /// </summary>
//        /// <value><c>true</c> if this instance can approve time off requests; otherwise, <c>false</c>. Defaults to <c>true</c></value>
//        public bool CanApproveOwnTimeOff
//        {
//            get;
//            set;
//        }


//        /// <summary>
//        /// Controls whether the user is warned whether changing shifts generates 
//        /// </summary>
//        /// <value><c>true</c> if hide break warnings; otherwise, <c>false</c>.</value>
//        public bool HideBreakWarnings
//        {
//            get;
//            set;
//        }


//        public bool CheckForLockout
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Used when we want to check the lockout against another version of the app ->  fixes a problem experienced by virgin media where ipad app shouldnt be downloaded but should be availble on the app site.
//        /// </summary>
//        /// <value>The lockout bundle override.</value>
//        public string LockoutBundleOverride
//        {
//            get; 
//            set;
//        }

//        public bool DontGenerateWarnings
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Whether or not the user can see the (ASDA specific) planning summary screen
//        /// </summary>
//        public bool ShowPlannedScheduleSummary
//        {
//            get;
//            set; 
//        }

//        public bool CanEditEnvironments
//        {
//            get;
//            set;
//        }

//        public bool CanLockResources
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Used to set when the night shift begins (hours, e.g. 22 = 10pm)
//        /// </summary>
//        public int NightShiftStartTime
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Used to set how long the night shift lasts for (in hours)
//        /// </summary>
//        public int NightShiftDuration
//        {
//            get;
//            set;
//        }

//        public bool CanResetPassword
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Used to show/hide the overtime tracker in the drawer
//        /// </summary>
//        public bool ShowOvertimeTracker
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Used to show/hide the Daily OVerview menu in the drawer
//        /// </summary>
//        /// <value><c>true</c> if show daily overview menu; otherwise, <c>false</c>.</value>
//        public bool ShowDailyOverviewMenu
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Enable/Disable the spare shifts service.
//        /// </summary>
//        public bool ShowSpareShifts
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Enable/Disable delete shifts buttons.
//        /// </summary>
//        public bool CanDeleteShifts
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Enable/Disable store in Navbar
//        /// </summary>
//        public bool ShowStoreInNavBar
//        {
//            get;
//            set;
//        }


//        /// Enable/Disable "Add Meal", "Add Break" and ability to edit Actual punches for meals and breaks.
//        /// </summary>
//        public bool AllowPunchesForMealsAndBreaks
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Enable/Disable ability to edit Actual punches on Edit Shift in DailyOverview
//        /// </summary>
//        public bool CanEditOwnPunches
//        {
//            get;
//            set;
//        }

//        /// <summary>

//        /// Enabled/Disable the CombineDepartments VM from the drawer
//        /// </summary>
//        public bool ShowCombineDepartments
//        {
//            get;
//            set;
//        }

//        public int CombineDepartmentsMaxChildDepartments
//        {
//            get;
//            set;
//        }

//        public int PeriodFirstDay
//        {
//            get;
//            set;
//        }

//        public int PeriodFirstMonth
//        {
//            get;
//            set;
//        }

//        public int StartDay
//        {
//            get;
//            set;
//        }

//        public bool WorkgroupsAreDepartments
//        {
//            get;
//            set;
//        }

//        //Stores detals for being able to filter to, search jobs for and display SSC and SAYS till allocations
//        public SSCSAYSConfiguration[] SSCSAYSConfigurations
//        {
//            get;
//            set;
//        }

//        public bool ShowCombinedSSCSAYSTill
//        {
//            get;
//            set;
//        }

//        public bool HideEmptySSCSAYSTill
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Enable/Disable sending of exceptions updates, Walmart don't send them.
//        /// </summary>
//        public bool SendWeeklyWarningsUpdates
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Enable/Disable shift swapping on drag and drop
//        /// </summary>
//        public bool ShiftSwapOnDragAndDropEnabled 
//        {
//            get;
//            set;
//        }

//        //Optional configurations to override the dynamic filtering (null == dynamic, true == on (if possible), false == off)
//        public bool? CanFilterByWorkgroup { get; set; }
//        public bool? CanFilterByDepartment { get; set; }
//        public bool? CanFilterByAttribute { get; set; }
//        public bool? CanFilterByJobClassic { get; set; }
//        public bool? CanFilterByJobCustom { get; set; }
//        public bool? CanFilterByRoleCustom { get; set; }

//        // This blocks any changes to the schedule within the next 'x' hours
//        public int PreventChangesHours { get; set; }

//        public bool ShowFillUnfilledShifts {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Sets the name of the timeoff type
//        /// </summary>
//        /// <value>The type of the call off shift time off.</value>
//        public string CallOffShiftTimeOffType { get; set; }

//        public bool BlockEditingInPast { get; set; }

//        public ApplicationConfiguration()
//        {
//            TimeoutMinutes = 500;
//            RestClientTimeoutSecs = 120;
//            KeepAliveFreqSecs = 60;
//            ScheduleTimeoutSecs = 240;
//            WeekNumberDisplayed = false;
//            ShowMobileNumbers = true;
//            ShowScheduleMenu = true;
//            ShowTimeOffMenu = true;
//            ShowTillAllocationMenu = true;
//            ShowNightAislePlannerMenu = true;
//            CanEditSchedule = true;
//            CanMarkBreaksAsTaken = false;
//            CanPrintSchedule = true;
//            AlternativeScheduleView = false;
//            ShowPlanningTab = true;
//            ShowResourcesTab = false;
//            ShowBreaksTab = true;
//            ShowTillAllocationBuffer = false;
//            ShowMultipleTillAllocationBuffers = false;
//            ShowPopoverEmployeeTimeOffView = true;
//            ShowAllEmployeeHistoryInPopover = true;
//            ShowScheduleDetails = true;
//            ShowDrawerNotifications = true;
//            ShowChangePassword = false;
//            ShowScheduleCost = false;
//            ShowSalesForecast = false;
//            HideDefaultVarianceValues = false;
//            CanApproveTimeOff = false;
//            CanApproveOwnTimeOff = true;
//            CheckForLockout = false;
//            DontGenerateWarnings = false;
//            HideBreakWarnings = false;
//            ShowPlannedScheduleSummary = false;
//            CanEditEnvironments = false;
//            CanResetPassword = false;
//            CanLockResources = true;
//            NightShiftStartTime = 20;
//            NightShiftDuration = 9;
//            ShowOvertimeTracker = true;
//            ShowSpareShifts = false;
//            ShowDailyOverviewMenu = true;
//            CanDeleteShifts = true;
//            ShowStoreInNavBar = false;
//            AllowPunchesForMealsAndBreaks = true;
//            CanEditOwnPunches = true;
//            ShowCombineDepartments = false;
//            CombineDepartmentsMaxChildDepartments = -1;
//            AllowPunchesForMealsAndBreaks = true;
//            WorkgroupsAreDepartments = false;
//            SendWeeklyWarningsUpdates = true;
//            ShiftSwapOnDragAndDropEnabled = true;

//            CanFilterByAttribute = null;
//            CanFilterByDepartment = null;
//            CanFilterByJobClassic = null;
//            CanFilterByJobCustom = null;
//            CanFilterByRoleCustom = null;
//            CanFilterByWorkgroup = null;

//            //Def Accounting setup
//            StartDay = (int)DayOfWeek.Monday;
//            PeriodFirstDay = 2;
//            PeriodFirstMonth = 3;

//            SSCSAYSConfigurations = new SSCSAYSConfiguration[0];
//            ShowCombinedSSCSAYSTill = true;
//            HideEmptySSCSAYSTill = true;

//            PreventChangesHours = 0;
//            ShowFillUnfilledShifts = true;
//            BlockEditingInPast = false;
//        }
//    }
//}

