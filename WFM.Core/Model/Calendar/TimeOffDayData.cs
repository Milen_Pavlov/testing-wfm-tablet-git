﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;

namespace WFM.Core
{
	public class TimeOffDayData : ICalendarDayData
	{
		public DateTime Date { get; set; }

        public List<JDATimeOffRequest> Data { get; set; }
	}
}

