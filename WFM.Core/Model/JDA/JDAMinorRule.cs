﻿using System;

namespace WFM.Core
{
    public class JDAMinorRule
    {
        public string Name { get; set; }
        public int MaximumYearsOld { get; set; }
        public int MinimumYearsOld { get; set; }
        public string Code { get; set; }
    }
}

