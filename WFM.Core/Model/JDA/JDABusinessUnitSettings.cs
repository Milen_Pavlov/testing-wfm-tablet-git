﻿using System;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class JDABusinessUnitSettings
    {
        public int BUID { get; set; }
        public DateTime? BusinessDate { get; set; }
        public bool CanEditPastShifts { get; set; }
        public int FirstDayOfWeekID { get; set; }
        public bool IsBreakPaid { get; set; }
        public bool IsMealPaid { get; set; }
        public int LaborStartTimeMinutes { get; set; }
        public DateTime LastPayEngineTimestamp { get; set; }
        public DateTime LocalBusinessUnitTime { get; set; }
        [JsonIgnore]
        public DateTimeOffset BusinessUnitOffset { get; set; }
        public short? UnpairedPunchThreshold { get; set; }



    }
}

