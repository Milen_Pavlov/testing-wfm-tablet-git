﻿using System;

namespace WFM.Core
{
    public class JDATimeOffType
    {
        public bool IsTrackedAsWholeDays { get; set; }
        public string TypeName { get; set; }
        public int TypeID { get; set; }
        public bool TrackApprovedAccruals { get; set; }

        public JDATimeOffType()
        {
        }
    }
}