﻿using System;

namespace WFM.Core
{
    public class JDACancelTimeOffRequestCommand : JDAChangeCommand
    {
        public string __TYPE { get { return "SiteManager.Commands.Scheduling.CancelTimeOffRequest"; } }
        public int? TimeOffRequestID { get; set; }
        public string Comment { get; set; }
    }

    public class JDADenyTimeOffRequestCommand : JDAChangeCommand
    {
        public string __TYPE { get { return "SiteManager.Commands.Scheduling.DenyTimeOffRequest"; } }
        public int? TimeOffRequestID { get; set; }
        public string Comment { get; set; }
    }

    public class JDAApproveTimeOffRequestCommand : JDAChangeCommand
    {
        public string __TYPE { get { return "SiteManager.Commands.Scheduling.ApproveExistingTimeOffRequest"; } }
        public bool AllowAccrualBalanceViolation { get; set; }
        public bool AllowBlackoutPeriodOverlap { get; set; }
        public bool AllowTimeOffRequestLongerThanClientSetting { get; set; }
        public string Comment { get; set; }
        public JDATimeOffDetail[] Details { get; set; }
        public int EmployeeID { get; set; }
        public DateTime End { get; set; }
        public bool IsInClosedPayPeriod { get; set; }
        public bool IsTimeOffTypeDirty { get; set; }
        public DateTime Start { get; set; }
        public int? TimeOffRequestID { get; set; }
        public int? TimeOffTypeID { get; set; }
    }

    public class JDACreateAndApproveTimeOffRequestCommand : JDAChangeCommand
    {
        public string __TYPE { get { return "SiteManager.Commands.Scheduling.CreateAndApproveTimeOffRequest"; } }
        public bool AllowAccrualBalanceViolation { get; set; }
        public bool AllowBlackoutPeriodOverlap { get; set; }
        public bool AllowTimeOffRequestLongerThanClientSetting { get; set; }
        public string Comment { get; set; }
        public int EmployeeID { get; set; }
        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public int TimeOffRequestID { get; set; }
        public int TimeOffTypeID { get; set; }
        public JDACreateTimeOffRequestUpdatedHours[] UpdateHours { get; set; }
    }

    public class JDACreateTimeOffRequestUpdatedHours
    {
        public string AdjustmentCategoryID { get; set; }
        public bool AllowPayAdjustmentWarningThresholdViolation { get; set; }
        public string CategoryDefinitionID { get; set; }
        public DateTime Date { get; set; }
        public double NewHours { get; set; }
        public int NewPayAdjustmentID { get; set; }
        public string PayRuleID { get; set; }
    }
}
