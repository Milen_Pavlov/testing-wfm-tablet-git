﻿using System;

namespace WFM.Core
{
    public class JDAPayRule
    {
        public int? CategoryDefinitionID { get; set; }
        public int? PayAdjustmentID { get; set; }
        public string PayAdjustmentName { get; set; }
        public int? PayRuleID { get; set; }
        public string PayRulePayAdjustmentName { get; set; }
    }
}