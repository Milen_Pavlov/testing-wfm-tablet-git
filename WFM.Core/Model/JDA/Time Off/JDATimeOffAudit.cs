﻿using System;

namespace WFM.Core
{
    public class JDATimeOffAudit
    {
        public string Comment { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string StatusCode { get; set; }
        public DateTime Timestamp { get; set; }
    }
}