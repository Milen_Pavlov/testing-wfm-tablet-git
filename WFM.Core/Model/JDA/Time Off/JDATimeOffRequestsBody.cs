﻿using System;

namespace WFM.Core
{
    public class JDATimeOffRequestsBody
    {
        public JDATimeOffRequest[] Requests { get; set; }
        public int TotalCount { get; set; }
    }
}