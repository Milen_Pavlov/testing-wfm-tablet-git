﻿using System;

namespace WFM.Core
{
    public class JDATimeOffDetail
    {
        public DateTime Date { get; set; }
        public int? ID { get; set; }
        public bool IsPaid { get; set; }
        public string Name { get; set; }
        public double? PaidHours { get; set; }
        public int? PayAdjustmentID { get; set; }
        public DateTime StartOfWeek { get; set; }


        public int? CategoryDefinitionID { get; set; }
        public int? PayRuleId { get; set; }

        public int? TimeOffRequestDetailID { get; set; }

        public double? NewHours { get; set; }
        public double ChangeHours { get; set; }
        public bool? AllowPayAdjustmentWarningThresholdViolation { get; set; }
        public int? AdjustmentCategoryID { get; set; }
        public int? PayRuleID { get; set; }
    }
}