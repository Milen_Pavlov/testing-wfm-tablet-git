﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class JDATimeOffRequestSection : ISection
    {
        private IList m_items;
        public IList Items
        {
            get
            {
                return m_items;
            }
            set
            {
                m_items = value;
            }
        }

        private DateTime m_date;
        public DateTime Date
        {
            get { return m_date; }
            set { m_date = value; }
        }

        private TimeOffStatus m_status;
        public TimeOffStatus Status
        {
            get { return m_status; }
            set { m_status = value; }
        }

        public JDATimeOffRequestSection()
        {
            Items = new List<JDATimeOffRequest> ();
        }
    }

    public class JDATimeOffRequest
    {
        public string StatusCode { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeLastName { get; set; }
        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public string EmployeeFirstName { get; set; }
        public DateTime Submitted { get; set; }
        public string PayAdjustmentCategoryName { get; set; }
        public string EmployeeNickName { get;set; }
        public int? TimeOffID { get; set; }

        [JsonIgnore]
        public TimeOffStatus Status
        {
            get
            {
                return TimeOffHelper.GetStatus(StatusCode);
            }
        }

        [JsonIgnore]
        public string SubTitle
        {
            get
            {
                IStringService localiser = Mvx.Resolve<IStringService> ();
                return string.Format ("{0} - {1}", Start.ToString(localiser.Get("dd MMMMM yyyy")), End.ToString(localiser.Get("dd MMMMM yyyy")));
            }
        }
    }
}