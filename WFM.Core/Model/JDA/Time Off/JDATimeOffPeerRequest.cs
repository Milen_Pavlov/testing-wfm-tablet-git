﻿using System;

namespace WFM.Core
{
    public class JDATimeOffPeerRequest
    {
        public int? EmployeeID { get; set; }
        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public string StatusCode { get; set; }

        public TimeOffStatus Status
        {
            get
            {
                return TimeOffHelper.GetStatus(StatusCode);
            }
        }
    }
}