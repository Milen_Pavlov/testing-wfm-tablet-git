﻿using System;

namespace WFM.Core
{
    //TODO: Need a real environment where this is pulled from
    public class JDAAccrual
    {
        public float Future { get; set; }
        public float Available { get; set; }
        public bool TrackApprovedAccrualUsage { get; set; }
        public string TypeName { get; set; }
        //public int SequenceNumber { get; set; }
    }
}