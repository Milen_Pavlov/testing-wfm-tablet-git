﻿using System;

namespace WFM.Core
{
    public class JDADetailedTimeOffRequest
    {
        public JDATimeOffAudit[] Audits { get; set; }
        public JDATimeOffDetail[] Details { get; set; }
        
        public int EmployeeID { get; set; }
        public DateTime End { get; set; }
        public int? ID { get; set; }
        public bool IsTorReadOnly { get; set; }
        public bool IsTrackedAsWholeDays { get; set; }
        public bool OverlapsTransfer { get; set; }
        public int? PayAdjustmentID { get; set; }
        public DateTime Start { get; set; }
        public string StatusCode { get; set; }
        public int? TypeID { get; set; }
        public string TypeName { get; set; }

        public int? PayRuleID { get; set; }
        public double? PaidHours { get; set; }

        public TimeOffStatus Status
        {
            get
            {
                return TimeOffHelper.GetStatus(StatusCode);
            }
        }
    }
}