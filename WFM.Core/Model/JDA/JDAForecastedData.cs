﻿using System;

namespace WFM.Core
{
    public class JDAForecastedData
    {
        public decimal? Day1Value { get; set; }
        public decimal? Day2Value { get; set; }
        public decimal? Day3Value { get; set; }
        public decimal? Day4Value { get; set; }
        public decimal? Day5Value { get; set; }
        public decimal? Day6Value { get; set; }
        public decimal? Day7Value { get; set; }
        public decimal WeekTotal { get; set; }
        public string ForecastGroupID { get; set; }
        public string AccessControlType { get; set; }
        public string MetricType { get; set; }
        public bool IsDerived { get; set; }
        public string ParentMetricID { get; set; }
        public int MetricSortOrder { get; set; }
        public string MetricName { get; set; }
        public string ForecastGroupName { get; set; }
        public string MetricID { get; set; }
    }
}

