﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAEmployee
    {
        public List<JDAEmployeeAttribute> Attributes { get; set; }
        public string BadgeNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public List<JDAEmployeeAssignedJob> CanWorkJobs { get; set; }
        public List<JDAWarning> DailyWarnings { get; set; }
        public int EmployeeID { get; set; }
        public string FullName { get; set; }
        public int? HomeBusinessUnitID { get; set; }
        public bool IsBorrowed { get; set; }
        public bool IsMinor { get; set; }
        public int? LWeekConsecDays { get; set; }
        public JDAMinorRule MinorRule { get; set; }
        public int? NWeekConsecDays { get; set; }
        public List<JDARole> RestrictedRoles { get; set; }
        public JDAScheduleConstraints ScheduleConstraints { get; set; }
        public int? ScheduleRuleID { get; set; }
        public int SeniorityOrder { get; set; }
        public List<JDAShift> Shifts { get; set; }
        public List<JDAWarning> WeeklyWarnings { get; set; }

        // MISSING...
        //ContractVariance
        //GuaranteedHours
    }
}

