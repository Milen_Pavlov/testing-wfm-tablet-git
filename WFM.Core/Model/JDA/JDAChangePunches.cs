﻿using System;

namespace WFM.Core
{
    public class JDAAddPunchedShift : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.AddPunchedShift"; } }
        public int? PunchedShiftID { get; set; }
        public int EmployeeID { get; set; }
        public new int? JobID { get; set; }
        public DateTime Start { get; set; }
        public int? ScheduledShiftID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAAddPunchedShiftEnd : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.AddPunchedShiftEnd"; } }
        public int? PunchedShiftID { get; set; }
        public DateTime End { get; set; }
        public bool IsBackPay { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDADeletePunchedShift : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.DeletePunchedShift"; } }
        public int? PunchedShiftID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDADeletePunchedShiftEnd : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.DeletePunchedShiftEnd"; } }
        public int? PunchedShiftID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAModifyPunchedShiftStart : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.ModifyPunchedShiftStart"; } }
        public int? PunchedShiftID { get; set; }
        public DateTime? Start { get; set; }
        public int? ScheduledShiftID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; } 
    }

    public class JDAModifyPunchedShiftEnd : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.ModifyPunchedShiftEnd"; } }
        public int? PunchedShiftID { get; set; }
        public DateTime? End { get; set; }
        public bool IsBackPay { get; set; }
        public int? ScheduledShiftID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAModifyPunchedShiftJob : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.ModifyPunchedShiftJob"; } }
        public int? PunchedShiftID { get; set; }
        public int? JobID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAAddPunchDetail : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.AddPunchDetail"; } }
        public int? PunchedShiftID { get; set; }
        public int? PunchDetailID { get; set; }
        public string Type { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public bool IsBackPay { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAAddPunchDetailEnd : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.AddPunchDetailEnd"; } }
        public int? PunchedShiftID { get; set; }
        public int? PunchDetailID { get; set; }
        public string DetailStartType { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public bool IsBackPay { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDADeletePunchDetail : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.DeletePunchDetail"; } } 
        public int? PunchedShiftID { get; set; }
        public int? PunchDetailID { get; set; }
        public int? AuditReasonID { get; set; } 
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDADeletePunchDetailEnd : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.DeletePunchDetailEnd"; } } 
        public int? PunchedShiftID { get; set; }
        public int? PunchDetailID { get; set; }
        public int? AuditReasonID { get; set; } 
        public int? BackPayAuditReasonID { get; set; }
    }

    // Detail punches are for breaks / jobs
    public class JDAModifyDetailStart : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.ModifyDetailStart"; } }
        public int? PunchedShiftID { get; set; }
        public int? PunchDetailID { get; set; }
        public DateTime? Start { get; set; }
        public bool IsBackPay { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAModifyDetailEnd : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.ModifyDetailEnd"; } }
        public int? PunchedShiftID { get; set; }
        public int? PunchDetailID { get; set; }
        public DateTime? End { get; set; }
        public bool IsBackPay { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; }
    }

    public class JDAAddJobTransfer : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.AddJobTransfer"; } }
        public int? PunchedShiftID { get; set; }
        public int? JobTransferID { get; set; }
        public string Type { get; set; }
        public new int? JobID { get; set; }
        public DateTime? TransferTime { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; } 
        public string ShiftType { get; set; }
    }

    public class JDADeleteJobTransfer : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.DeleteJobTransfer"; } }
        public int? PunchedShiftID { get; set; }
        public int? JobTransferID { get; set; }
        public new int? JobID { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; } 
    }

    public class JDAModifyJobTransfer : JDAChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Commands.TimeAndAttendance.ModifyJobTransfer"; } }
        public int? PunchedShiftID { get; set; }
        public int? JobTransferID { get; set; }
        public new int? JobID { get; set; }
        public DateTime? TransferTime { get; set; }
        public int? AuditReasonID { get; set; }
        public int? BackPayAuditReasonID { get; set; } 
    }
}

