﻿using System;

namespace WFM.Core
{
    public class JDARole
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }
}

