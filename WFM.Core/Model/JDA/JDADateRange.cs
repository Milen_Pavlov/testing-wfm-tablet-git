﻿using System;

namespace WFM.Core
{
    public class JDADateRange
    {
        public DateTime? End { get; set; }
        public DateTime Start { get; set; }
    }
}

