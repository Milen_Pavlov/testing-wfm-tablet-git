﻿using System;

namespace WFM.Core
{
    public enum JDAWarningType
    {
        GeneralUnavailability = 18,
        MinHoursBetweenShifts = 19,
        TimeOffConflict = 24,
        MinHoursDay = 25,
        MaxHoursDay = 26,
        MinHoursWeek = 27,
        MaxDaysWeek = 29,
        MaxConsecutiveDays = 30,
        MinHoursBetweenSplit = 35,
        MaxHoursBetweenSplit = 36,
        MaxTimeBeforeRest = 41,
        MaxConsecutiveDaysCross = 44,
        UnassignedRole = 54,
        MaxHoursWeek = 55   
    };
}
