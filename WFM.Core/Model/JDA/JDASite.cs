﻿using System;

namespace WFM.Core
{
    public class JDASite
    {
        public string Name { get; set; }
        public string LongName { get; set; }
        public int SiteID { get; set; }
        public int StartOfWeek { get; set; }
        public DateTime? EffectiveStartOfWeek { get; set; }
        public DateTime? EarliestOpenPayPeriodStartTimestamp { get; set; }
        public DateTime? EarliestOpenPayPeriodEndTimestamp { get; set; }
        public DateTime? EarliestOpenPayPeriodStartDate { get; set; }
        public DateTime? EarliestOpenPayPeriodEndDate { get; set; }
        public DateTime? PayPeriodStartDate { get; set; }
        public DateTime? PayPeriodEndDate { get; set; }
        public DateTime? PayPeriodStartTimestamp { get; set; }
        public DateTime? PayPeriodEndTimestamp { get; set; }
        public DateTime? LatestSchedulePostTimestamp { get; set; }
        public DateTime? LastCommittedPayPeriodEndTimestamp { get; set; }
        public int EarliestOpenPayPeriodID { get; set; }
        public int LaborStartTimeMinutes { get; set; }
        public bool IsLaborProfileDefined { get; set; }
        public bool RequireSchedulePostingFlag { get; set; }
    }
}
    