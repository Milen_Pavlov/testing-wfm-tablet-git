﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAJobAssignment
    {
        public int ScheduledJobID { get; set; }
        public string PunchCode { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string JobColor { get; set; }
        public int JobID { get; set; }
        public string JobName { get; set; }
        public List<JDAShiftDetail> Breaks { get; set; }
        public List<JDAShiftDetail> Meals { get; set; }
        public List<JDAShiftDetail> Roles { get; set; }

        [JsonIgnore]
        public bool EndsAtMidnight { get { return End.TimeOfDay.Hours == 0 && End.TimeOfDay.Minutes == 0 && End.TimeOfDay.Seconds == 0; } }
    }
}

