﻿using System;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAEmployeeInfo
    {
        public string LoginName { get; set; }
        public string BadgeNumber { get; set; }
        public string CellPhone { get; set; }
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string HomePhone { get; set; }
        public string HomeSiteCode { get; set; }
        public int HomeSiteID { get; set; }
        public string HomeSiteName { get; set; }
        public bool IsActive { get; set; }
        public bool IsBorrowed { get; set; }
        public bool IsMinor { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public JDAPayClassCode PayClassCode { get; set; }
        public int PrimaryJobID { get; set; }
        public string PrimaryJobName { get; set; }
    }
}

