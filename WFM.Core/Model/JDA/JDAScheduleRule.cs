﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAScheduleRule
    {
        public List<JDAScheduleAllocation> ScheduleAllocations { get; set; }
        public int ScheduleRuleID { get; set; }
    }
}
