﻿using System;

namespace WFM.Core
{
    public enum JDAAuditChangeType
    {
        Added,
        Changed,
        Deleted,
    }
}

