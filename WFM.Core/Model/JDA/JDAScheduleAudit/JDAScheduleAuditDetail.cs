﻿using System;

namespace WFM.Core
{
    public class JDAScheduleAuditDetail
    {
        public string AuditDetailType { get; set; }
        public bool? primaryEditColumn { get; set; }
        public string OriginalStart { get; set; }
        public string OriginalEnd { get; set; }
        public int? OriginalJob { get; set; }
        public string ChangedStart { get; set; }
        public string ChangedEnd { get; set; }
        public int? ChangedJob { get; set; }
        public string AuditChangeType { get; set; }
        public int? ChangedRole { get; set; }
    }
}

