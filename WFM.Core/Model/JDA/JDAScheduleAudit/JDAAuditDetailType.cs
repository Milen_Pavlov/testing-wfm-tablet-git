﻿using System;

namespace WFM.Core
{
    public enum JDAAuditDetailType
    {
        Job,
        Break,
        Meal,
        Role
    }
}

