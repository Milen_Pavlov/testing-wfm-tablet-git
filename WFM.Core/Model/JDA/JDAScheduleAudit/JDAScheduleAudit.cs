﻿using System;

namespace WFM.Core
{
    public class JDAAddScheduleAuditCommand : ScheduleChange.ChangeCommand
    {
        public new string __TYPE { get { return "SiteManager.Classes.Commands.Scheduling.AddScheduleAudit"; } }
        public string UserIntent { get; set; }
        public int? EmployeeID { get; set; }
        public string ScheduleDate { get; set; }
        public string ScheduleType { get; set; }
        public int FirstJobID { get; set; }
        public JDAScheduleAuditDetail[] ScheduleAuditDetails { get; set; }
        public int? ShiftID { get; set; }
    }
}

