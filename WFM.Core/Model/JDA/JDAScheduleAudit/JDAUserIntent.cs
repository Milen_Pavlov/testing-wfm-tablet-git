﻿using System;

namespace WFM.Core
{
    public enum JDAUserIntent
    {
        ShiftAdded,
        ShiftChanged,
        ShiftDeleted,
        MealAdded,
        MealChanged,
        MealDeleted,
        BreakAdded,
        BreakChanged,
        BreakDeleted,
        RoleAdded,
        RoleChanged,
        RoleDeleted,
    }
}

