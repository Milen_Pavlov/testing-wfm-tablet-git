﻿using System;

namespace WFM.Core
{
    public class JDAScheduleAllocation
    {
        public DateTime? EndTime { get; set; }
        public DateTime? StartTime { get; set; }
        public int BreakDuration { get; set; }
        public int BreakDurationPaid { get; set; }
        public int BreakMinTimeAfterShiftStart { get; set; }
        public int BreakMinTimeBeforeShiftEnd { get; set; }
        public int MealDuration { get; set; }
        public int MealDurationPaid { get; set; }
        public int MealMinTimeAfterShiftStart { get; set; }
        public int MealMinTimeBeforeShiftEnd { get; set; }
        public int MinTimeAfterShiftStart { get; set; }
        public int MinTimeBeforeShiftEnd { get; set; }
        public int NumberOfBreaks { get; set; }
        public int NumberOfMeals { get; set; }
        public int TimeBetweenBreaks { get; set; }
        public decimal ShiftDuration { get; set; }
    }
}
