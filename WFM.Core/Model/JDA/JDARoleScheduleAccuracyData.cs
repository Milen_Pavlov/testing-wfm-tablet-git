﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDARoleScheduleAccuracyData
    {
        public class Interval
        {
            public DateTime S { get; set; }
            public DateTime E { get; set; }
            public double V { get; set; }
        }

        public int RoleID { get; set; }
        public List<Interval> SupplyData;
        public List<Interval> DemandData;
    }
}
