﻿using System;

namespace WFM.Core
{
    public enum JDAPayClassCode
    {
        FullTimeSalariedNonExempt = 1,
        FullTimeSalariedExempt,
        FullTimeHourlyNonExempt,
        FullTimeHourlyExempt,
        PartTimeSalariedNonExempt,
        PartTimeSalariedExempt,
        PartTimeHourlyNonExempt,
        PartTimeHourlyExempt
    }
}

