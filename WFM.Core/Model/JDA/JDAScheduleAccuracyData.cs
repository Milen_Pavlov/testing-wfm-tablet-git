﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAScheduleAccuracyData
    {
        public List<JDANameAndID> Roles;
        public List<JDAWorkgroupRoles> Workgroups;
        public List<JDARoleScheduleAccuracyData> RoleScheduleAccuracyData;
    }
}

