﻿using System;

namespace WFM.Core
{
    public class JDANameAndID
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
