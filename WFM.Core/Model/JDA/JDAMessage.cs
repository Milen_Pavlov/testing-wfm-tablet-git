﻿using System;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using Newtonsoft.Json;

namespace WFM.Core
{
    public abstract class JDAMessageBase
    {
        public string message { get; set; }
        public int count { get; set; }
        public string appStatus { get; set; }
        public int status { get; set; }
        public string type { get; set; }

        [JsonIgnore]
        public JDAError Error { get; set; }

        public bool IsSuccess
        {
            get
            {
                return status == 0;
            }
        }

        public string StatusText
        {
            get 
            {
                IStringService localiser = Mvx.Resolve<IStringService>();
                switch (status)
                {
                    case 0:
                        return "";
                    case 1200:
                        return localiser.Get("invalid_username_or_password");
                    case 1201:
                        return localiser.Get("password_expired_error");
                    default:
                        return string.Format ("{0} ({1})", message ?? localiser.Get("status"), status);
                }
            }
        }
    }

    public class JDAMessage<T> : JDAMessageBase
    {
        public T data { get; set; }
    }

    //This gets swapped out if JDA falls over
    public class JDAError
    {
        public string ExceptionType { get; set; }
        public JDAErrorMessage Message { get; set; }
        public string StackTrace { get; set; }
    }

    public class JDAErrorMessage
    {
        public object Arguments { get; set; }
        public string Type { get; set; }
        public string CollectionType { get; set; }
    }

}
