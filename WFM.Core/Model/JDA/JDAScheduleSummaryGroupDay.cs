﻿using System;

namespace WFM.Core
{
    //Base class to help the Data, Day, Night and Total be differentiated by cells
    public class JDAScheduleSummaryGroupBaseEntry
    {
        public string Title { get; set; }
        public bool IsTotal { get; set; }       

        public JDAScheduleSummaryGroupBaseEntry()
        {

        }

        public JDAScheduleSummaryGroupBaseEntry(string title, bool isTotal)
        {
            Title = title;
            IsTotal = isTotal;
        }
    }

    //Day, Night and Total
    public class JDAScheduleSummaryGroupDayEntry : JDAScheduleSummaryGroupBaseEntry
    {
        public JDADepartmentSummaryType SummaryType { get; set; }

        public float? SalesForecast { get; set; }
        public float LabourHours { get; set; }
        public float ScheduledHours { get; set; }
        public float Coverage { get; set; }
        public float Accuracy { get; set; }
        public float LabourCost { get; set; }
        public float ScheduledCost { get; set; }
        public float CostVariance { get; set; }

        public float DemandCovered { get; set; }
        public float DemandOver { get; set; }
        public float DemandUnder { get; set; }

        public JDAScheduleSummaryGroupDayEntry()
        {

        }

        public JDAScheduleSummaryGroupDayEntry(JDADepartmentSummary fromSummary, bool isTotal) : this(fromSummary, isTotal,"")
        {

        }

        public JDAScheduleSummaryGroupDayEntry(JDADepartmentSummary fromSummary) : this(fromSummary,false)
        {

        }

        public JDAScheduleSummaryGroupDayEntry(JDADepartmentSummary fromSummary, bool isTotal, string title) : base(title, isTotal)
        {
            SummaryType = fromSummary.SummaryType;

            SalesForecast = fromSummary.SalesForecast;
            LabourHours = fromSummary.LaborHours;

            ScheduledHours = fromSummary.ScheduledHours;
            Coverage = fromSummary.Coverage;
            Accuracy = fromSummary.Accuracy;
            LabourCost = fromSummary.LaborCost;
            ScheduledCost = fromSummary.ScheduledCost;
            CostVariance = fromSummary.CostVariance;

            DemandCovered = fromSummary.DemandCovered;
            DemandOver = fromSummary.DemandOver;
            DemandUnder = fromSummary.DemandUnder;
        }
    }
}

