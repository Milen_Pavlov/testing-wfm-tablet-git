﻿using System;

namespace WFM.Core
{
    public class JDAShiftSite
    {
        public string SiteName { get; set; }
        public int SiteID { get; set; }
    }
}

