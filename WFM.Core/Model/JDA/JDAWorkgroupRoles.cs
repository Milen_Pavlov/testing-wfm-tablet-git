﻿using System;

namespace WFM.Core
{
    public class JDAWorkgroupRoles
    {
        public int WorkgroupID { get; set; }
        public string WorkgroupName { get; set; }
        public int[] Roles { get; set; }
    }
}
