﻿using System;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAEmployeeAttribute
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
