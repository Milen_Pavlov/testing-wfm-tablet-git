﻿using System;

namespace WFM.Core
{
    public class JDAAvailableEmployeeData
    {
        public int EmployeeID { get; set; }
        public int PrimaryJobID { get; set; }
        public string HomeSiteNamec { get; set; }
        public float ScheduledHours { get; set; }
    }
}