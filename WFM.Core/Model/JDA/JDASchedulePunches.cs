﻿using System;

namespace WFM.Core
{
    public class JDASchedulePunches
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string Description { get; set; }
        public string PunchCode { get; set; }
        public int? ShiftID { get; set; }  
        public int? SchedShiftID { get; set; }
        public int? StartPunchID { get; set; }
        public int? EndPunchID { get; set; }
        public int? JobID { get; set; }

        public JDASchedulePunches ()
        {
            
        }
    }
}

