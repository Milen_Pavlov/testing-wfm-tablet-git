﻿using System;
using System.Linq;
using System.Collections.Generic;
using Cirrious.CrossCore;
using Newtonsoft.Json;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAShift
    {
        public int ShiftID { get; set; }
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public int? EmployeeID { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public List<JDAJobAssignment> Jobs { get; set; }
        public List<int> Warnings { get; set; }
        public bool IsReadOnly { get; set; }
        public string ScheduleType { get; set; }

        [JsonIgnore]
        public bool EndsAtMidnight { get { return End.TimeOfDay.Hours == 0 && End.TimeOfDay.Minutes == 0 && End.TimeOfDay.Seconds == 0; } }
    }
}
