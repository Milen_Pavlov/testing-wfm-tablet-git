﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAProcessingResult
    {
        public enum Codes
        {
            OK,
            ValidationFailed,
            UnexpectedError
        }

        public class ErrorOrWarning
        {
            public object Arguments { get; set; }
            public string Type { get; set; }
            public object Value { get; set; }
        }

        public class ResolvedTicket
        {
            public int NewID { get; set; }
            public int Ticket { get; set; }
        }

        public string DetailedErrorMessage { get; set; }
        public string ErrorMessage { get; set; }
        public ErrorOrWarning[] Errors { get; set; }
        public IEnumerable<ResolvedTicket> ResolvedTickets { get; set; }
        public Codes ResultCode { get; set; }
        public ErrorOrWarning[] Warnings { get; set; }
    }
}

