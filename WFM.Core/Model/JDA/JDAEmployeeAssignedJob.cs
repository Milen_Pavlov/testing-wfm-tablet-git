﻿using System;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAEmployeeAssignedJob
    {
        public DateTime? End { get; set; }
        public bool IsPrimary { get; set; }
        public int JobID { get; set; }
        public float Rate { get; set; }
        public DateTime Start { get; set; }
    }
}
