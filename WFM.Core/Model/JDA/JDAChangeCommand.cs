﻿using System;

namespace WFM.Core
{
    //TODO In WFM-Digital there is a JDAChangeCommand base class and custom change commands
    //extend this...having it all dumped in one massive change command object is a bit 
    //clunky - iain
    public class JDAChangeCommand
    {
        public string __TYPE { get; set; }
        public int? SiteID { get; set; }
        public int? ShiftID { get; set; }
        public int? ScheduledJobID { get; set; }
        public DateTime? ResultingStart { get; set; }
        public DateTime? ResultingEnd { get; set; }
        public int? JobID { get; set; }
        public string JobName { get; set; }
        public int EmployeeFromID { get; set; }
        public int? EmployeeToID { get; set; }
        public int? ShiftDetailID { get; set; }
        public int? WarningType { get; set; }
        public int? RoleID { get; set; }
        public string RoleName { get; set; }
        public DateTime? BusinessDate { get; set; }
        public DateTime? WeekStartDate { get; set; }

        // extra values for sales forecast
        public decimal? Value { get; set; }
        public string MetricID { get; set; }
        public string ForecastGroupID { get; set; }

        // extra values for shift swap approval - iain
        public int? SwapID { get; set; }
        public int? SwappeeShiftID { get; set; }
        public bool? isApprove { get; set; }

        public string Position { get; set; }
    }
}
