﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAJob
    {
        public List<JDARole> CanWorkRoles { get; set; }
        public decimal? DefaultShiftLength { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string JobColor { get; set; }
        public int JobID { get; set; }
        public string JobName { get; set; }
        public int? PrimaryRoleID { get; set; }
        public int WorkgroupID { get; set; }

        public JDAJob() { } 
    }

    //Custom request return different JOB data...YAY
    public class JDACustomJob
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    //Turns out JDA has this looking the same, but made seperate object for consistency sake
    public class JDACustomRole
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
