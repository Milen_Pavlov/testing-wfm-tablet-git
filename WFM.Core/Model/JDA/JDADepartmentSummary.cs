﻿using System;

namespace WFM.Core
{
    public enum JDADepartmentSummaryType
    {
        Unknown = -1,
        WorkgroupTitle = 0,
        DateTitle = 1,
        AllWeekTitle =2,
        DayRow =3,
        NightRow = 4,
        DateTotal = 5,
        WeeklyDaysTotal=6,
        WeeklyNightsTotal=7,
        OverallWeeklyTotal = 8,
    }

    public class JDADepartmentSummary
    {
        public float DemandCovered { get; set; }
        public string WorkgroupName { get; set; }
        public bool HasChildren { get; set; }
        public float SalesForecast { get; set; }
        public int SortOrder { get; set; }
        public int Type { get; set; }
        public float Accuracy { get; set; }
        public float DemandOver { get; set; }
        public float DemandUnder { get; set; }
        public float LaborCost { get; set; }
        public float Coverage { get; set; }
        public DateTime Date { get; set; }
        public float ScheduledHours { get; set; } 
        public float CostVariance { get; set; }
        public float ScheduledCost { get; set; }
        public int WorkgroupID { get; set; }
        public float LaborHours { get; set; }     

        public JDADepartmentSummaryType SummaryType
        {
            get
            {
                try
                {
                    return (JDADepartmentSummaryType)Type;
                }
                catch
                {                                        
                    return JDADepartmentSummaryType.Unknown;
                }
            }
        }
    }
}