﻿using System;
using Consortium.Client.Core;
using System.Collections;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAScheduleSummaryGroup : ISection
    {
        public string Name { get; set; }

        public float SalesForecast { get; set; }
        public float LabourHours { get; set; }
        public float ScheduledHours { get; set; }
        public float Coverage { get; set; }
        public float Accuracy { get; set; }
        public float LabourCost { get; set; }
        public float ScheduledCost { get; set; }
        public float CostVariance { get; set; }

        private List<JDAScheduleSummaryGroupBaseEntry> m_entries;
        public List<JDAScheduleSummaryGroupBaseEntry> Entries
        {
            get { return m_entries; }
            set { m_entries = value; }
        }

        public IList Items
        {
            get
            {
                return (IList)Entries;
            }
        }

        public JDAScheduleSummaryGroup ()
        {
            Entries = new List<JDAScheduleSummaryGroupBaseEntry> ();
        }

        public JDAScheduleSummaryGroup(JDADepartmentSummary titleGroup)
        {
            Name = titleGroup.WorkgroupName;
            Accuracy = titleGroup.Accuracy;
            CostVariance = titleGroup.CostVariance;
            Coverage = titleGroup.Coverage;
            LabourCost = titleGroup.LaborCost;
            LabourHours = titleGroup.LaborHours;
            SalesForecast = titleGroup.SalesForecast;
            ScheduledCost = titleGroup.ScheduledCost;
            ScheduledHours = titleGroup.ScheduledHours;

            Entries = new List<JDAScheduleSummaryGroupBaseEntry> ();
        }
    }
}