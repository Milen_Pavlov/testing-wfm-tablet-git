﻿using System;

namespace WFM.Core
{
    public class JDAInterval
    {
        public const string MinorUnavailabilityType = "m";
        public const string GeneralUnavailabilityType = "u";
        public const string TimeOffUnavailabilityType = "t";

        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public string Type { get; set; }
    }
}

