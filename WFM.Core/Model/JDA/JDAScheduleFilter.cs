﻿using System;

namespace WFM.Core
{
    public class JDAScheduleFilter
    {
        public string FilterType { get; set; }
        public int[] FilterIDs { get; set; }
    }
}
