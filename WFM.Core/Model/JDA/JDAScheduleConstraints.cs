﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAScheduleConstraints
    {
        public class DailyConstraint
        {
            public DateTime Date { get; set; }
            public decimal? MaxHours { get; set; }
            public decimal? MinHours { get; set; }
        }

        public class DayMaxHoursConstraint
        {
            public DateTime Date { get; set; }
            public decimal MaxHours { get; set; }
        }
            
        public List<JDAInterval> UnavailabilityRanges { get; set; }
        public List<JDAEmployeeAssignedJob> FixedShifts { get; set; }
        public bool CanWorkSplit { get; set; }
        public int? MaxConsecutiveDays { get; set; }
        public int? MaxConsecutiveDaysAcrossWeeks { get; set; }
        public int? MaxDaysPerWeek { get; set; }
        public decimal? MaxHoursBetweenSplits { get; set; }
        public decimal? MaxHoursPerWeek { get; set; }
        public decimal? MinHoursBetweenShifts { get; set; }
        public decimal? MinHoursBetweenSplits { get; set; }
        public decimal? MinHoursPerWeek { get; set; }
        public List<DailyConstraint> MinMaxHoursByDays { get; set; }
        public List<DayMaxHoursConstraint> MinorDayMaxHours { get; set; }
        public int? MinorMaxConsecDaysCrossWeek { get; set; }
        public int? MinorMaxConsecDaysInWeek { get; set; }
        public int? MinorMaxDaysPerWeek { get; set; }
        public decimal? MinorMaxHoursPerWeek { get; set; }
        public decimal? TimeOffAdjustmentHours { get; set; }

        public JDAScheduleConstraints()
        {
            UnavailabilityRanges = new List<JDAInterval> ();
            FixedShifts = new List<JDAEmployeeAssignedJob> ();
            MinMaxHoursByDays = new List<DailyConstraint> ();
            MinorDayMaxHours = new List<DayMaxHoursConstraint> ();
        }
    }
}
