﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace WFM.Core
{
    // Checked against 8.1 output
    public class JDAScheduleData
    {
        // MISSING...
        //Sites

        public List<JDAEmployeeInfo> EmployeeInfo { get; set; }
        public List<JDAEmployee> Employees { get; set; }
        public List<JDAJob> JobRoles { get; set; }
        public List<JDAScheduleRule> ScheduleRules { get; set; }
        public JDAWeeklyMetricData BudgetedMetric { get; set; }
        public JDABusinessUnitSettings BUSettings { get; set; }
        public DateTime DefaultStartDate { get; set; }
        public DateTime FirstDayInWeek { get; set; }
        public List<JDAShift> UnfilledShifts { get; set; }
        public List<int> AuthorizedDepartments { get; set; }
        public bool IsStoreManager { get; set; }
        public List<JDAShiftSite> Sites { get; set; }

        // Find the shift in the raw schedule
        public JDAShift GetShift(int employeeID, int shiftID)
        {
            var employee = Employees.Where(x => x.EmployeeID == employeeID).FirstOrDefault ();

            if (employee != null)
                return employee.Shifts.Where(x => x.ShiftID == shiftID).FirstOrDefault ();

            return null;
        }
    }
}
