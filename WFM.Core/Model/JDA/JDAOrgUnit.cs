﻿using System;

namespace WFM.Core
{
    public class JDAOrgUnit: IComparable<JDAOrgUnit>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool isDefault { get; set; }

        public int CompareTo (JDAOrgUnit other)
        {
            int firstID = 0;
            int secondID = 0;

            if (int.TryParse (this.Name, out firstID))
            {
                if (int.TryParse (other.Name, out secondID))
                {
                    //We are both ints
                    return firstID.CompareTo (secondID);
                }
                else
                {
                    //We're an int, they're not
                    return -1;
                }
            }
            else if (int.TryParse (other.Name, out secondID))
            {
                //They're an int, we're not
                return 1;
            }
            else
            {
                //Neither of us are ints, we must be strings
                return this.Name.CompareTo (other.Name);
            }
        }
    }
}
