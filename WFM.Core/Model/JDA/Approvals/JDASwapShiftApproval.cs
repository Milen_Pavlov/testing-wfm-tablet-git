﻿using System;

namespace WFM.Core
{
    public class JDASwapShiftApproval
    {
        public string StatusCode { get; set; } //i = invalidate, c = cancelled, r = requested

        public DateTime RequestedOn { get; set; }

        public DateTime RequestedFor { get; set; }

        public string ID { get; set; }

        public string SwappeeScheduledShiftID { get; set; }

        public JDAApprovalEmployeeInfo Swapper { get; set; }

        public JDAApprovalEmployeeInfo Swappee { get; set; }

        public JDASwapShiftApproval ()
        {
        }
    }
}

