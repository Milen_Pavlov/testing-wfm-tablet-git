﻿using System;

namespace WFM.Core
{
    public class JDAAwaitingSwapApproval
    {
        public double NetWeeklyTotalHours { get; set; }

        public DateTime StartOfWeek { get; set; }

        public bool swapperShift { get; set; }

        public double SwappeeNetWeeklyHoursDelta { get; set; }

        public string EmployeeName { get; set; }

        public JDAApprovalShift[] PendingApprovalShifts { get; set; }

        public JDAAwaitingSwapApproval ()
        {
        }
    }
}

