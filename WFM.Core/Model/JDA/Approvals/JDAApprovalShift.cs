﻿using System;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class JDAApprovalShift
    {
        public string TypeCodeString { get; set; }

        public DateTime Date { get; set; }

        public DateTime ShiftStart { get; set; }

        public DateTime ShiftEnd { get; set; }

        public int ShiftID { get; set; }

        public string JobNames { get; set; }

        public JDAApprovalShift ()
        {
        }
    }
}

