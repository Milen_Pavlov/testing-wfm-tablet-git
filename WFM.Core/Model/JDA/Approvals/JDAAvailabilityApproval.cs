﻿using System;

namespace WFM.Core
{
    public class JDAAvailabilityApproval
    {
        public string StatusCode { get; set; }

        public DateTime RequestedOn { get; set; }

        public DateTime RequestedFor { get; set; }

        public string ID { get; set; }

        public JDAApprovalEmployeeInfo Employee { get; set; }

        public JDAAvailabilityApproval ()
        {
        }
    }
}

