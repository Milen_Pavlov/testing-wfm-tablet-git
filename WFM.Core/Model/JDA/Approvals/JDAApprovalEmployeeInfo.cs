﻿using System;

namespace WFM.Core
{
    public class JDAApprovalEmployeeInfo
    {
        public string FullName { get; set; }

        public string ID { get; set; }

        public JDAApprovalEmployeeInfo ()
        {
        }
    }
}

