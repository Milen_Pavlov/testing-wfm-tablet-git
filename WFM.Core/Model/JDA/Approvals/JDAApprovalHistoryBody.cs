﻿using System;

namespace WFM.Core
{
    public class JDAApprovalHistoryBody
    {
        //TODO
        public object[] TransferApprovals { get; set; }
        public object[] ShiftPickupApprovals { get; set; }

        public JDASwapShiftApproval[] SwapShiftApprovals { get; set; }

        public JDAAvailabilityApproval[] AvailabilityApprovals { get; set; }

        public DateTime LaborWeekStart { get; set; }

        public DateTime LaborWeekEnd { get; set; }

        public string Uri { get; set; }

        public int SiteID { get; set; }

        public JDAApprovalHistoryBody ()
        {
        }
    }
}

