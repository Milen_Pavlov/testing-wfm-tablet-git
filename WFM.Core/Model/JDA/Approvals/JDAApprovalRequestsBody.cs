﻿using System;

namespace WFM.Core
{
    public class JDAApprovalRequestsBody
    {
        //TODO
        public object[] TransferApprovals { get; set; }
        public object[] ShiftPickupApprovals { get; set; }

        public JDASwapShiftApproval[] SwapShiftApprovals { get; set; }

        public JDAAvailabilityApproval[] AvailabilityApprovals { get; set; }

        public string Uri { get; set; }

        public int SiteID { get; set; }
    }
}

