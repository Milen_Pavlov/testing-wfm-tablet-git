﻿using System;

namespace WFM.Core
{
    public class JDAShiftDetail
    {
        public string PunchCodeCase { get; set; }
        public string PunchCode { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public EditState EditState { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int ShiftDetailID {get; set;}
        public int ScheduledJobID {get; set;}

        public void Move (TimeSpan startDelta)
        {
            Start = Start.Add(startDelta);
            End = End.Add(startDelta);
        }
    }
}

