﻿using System;
using System.Dynamic;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class BreakItem
    {
		public int EmployeeId { get; set; }
        public int BreakDetailID { get; set; }
        public int ShiftID { get; set; }
        public DateTime StartTime { get; set; }
        public double Duration { get; set; }
        public string EmployeeName { get; set; }
        public DateTime ShiftStartTime { get; set; }
        public DateTime ShiftEndTime { get; set; }
        public string Department { get; set; }
        public bool BreakTaken { get; set; }
        public string StartTimeFormatted { get; set; }

        [JsonIgnore]
        public DateTime EndTime 
        {
            get
            {
                return StartTime.AddMinutes(Duration);
            }
        }

    }
}

