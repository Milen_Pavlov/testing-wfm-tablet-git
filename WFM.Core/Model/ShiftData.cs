﻿using System;
using System.Linq;
using System.Collections.Generic;
using Cirrious.CrossCore;
using AutoMapper;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class ShiftData
    {       
        private static int s_uniqueID = 0;
        private static Object s_uniqueIDLock = new Object();
        private const int c_defaultMinDurationBeforeBreak = 60;
        private const int c_defaultBreakDuration = 15;

        public int ShiftID { get; set; }
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public int? OriginalEmployeeId { get; set; }
        public int? EmployeeID { get; set; }
        public string ScheduleType { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        
        public TimeSpan Duration { get { return End.Subtract(Start); } }
        
        public string StartTimeFormatted
        {
            get
            {
                return m_timeFormatService.FormatTime(Start);
            }
        }
        
        public string EndTimeFormatted
        {
            get
            {
                return m_timeFormatService.FormatTime(End);
            }
        }
        
        public List<JobData> Jobs { get; set; }
        public List<JobData> DeletedJobs { get; set; }
        public List<ShiftDetailData> Details { get; set; }
        public List<ShiftDetailData> DeletedDetails { get; set; }

        public List<int> Warnings { get; set; }
        public bool IsReadOnly { get; set; }
        public List<int> InitialWarnings { get; set; }
        public EditState Edit { get; set; }
        public bool Modified { get; set; }

        private readonly IESSConfigurationService m_settings;
        private readonly ITimeFormatService m_timeFormatService;
        private readonly IRolesService m_rolesService;

        public ShiftData()
        {
            Warnings = new List<int> ();
            InitialWarnings = new List<int> ();
            Jobs = new List<JobData> ();
            DeletedJobs = new List<JobData> ();
            Details = new List<ShiftDetailData> ();
            DeletedDetails = new List<ShiftDetailData> ();

            m_settings = Mvx.Resolve<IESSConfigurationService> ();
            m_rolesService = Mvx.Resolve<IRolesService>();
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
        }

        public ShiftData Copy()
        {
            var shift = Mapper.Map<ShiftData,ShiftData> (this);
            return shift;
        }

        public static ShiftData Create()
        {
            return new ShiftData () {
                ShiftID = GenerateUniqueID(),
                IsReadOnly = false,
                Edit = EditState.Added,
                Modified = true,
            };
        }

        public static ShiftData Create(JDAShift shift)
        {
            return new ShiftData () {
                ShiftID = shift.ShiftID,
                SiteID = shift.SiteID,
                SiteName = shift.SiteName,
                IsReadOnly = false,
                Edit = EditState.Modified,
                Modified = true,
            };
        }

        public void SetWarnings(List<int> warnings)
        {
            Warnings = new List<int> (warnings);
            InitialWarnings = new List<int> (warnings);
        }

        public void AssignDepartmentsToJobs(IEnumerable<JDAJob> jobMapping)
        {            
            foreach (JobData job in this.Jobs)
            {
                var matchingJob = jobMapping.SingleOrDefault(mapping => mapping.JobID == job.JobID);

                if (matchingJob != null)
                {
                    job.DepartmentID = matchingJob.DepartmentID;
                    job.WorkgroupID = matchingJob.WorkgroupID;
                    job.PrimaryRoleID = matchingJob.PrimaryRoleID;
                }
            }
        }

        public List<int> GetDeletedWarnings()
        {
            return InitialWarnings.Where (x => !Warnings.Contains (x)).ToList();
        }

        public List<int> GetAddedWarnings()
        {
            return Warnings.Where (x => !InitialWarnings.Contains (x)).ToList();
        }

        public void DeleteWarning(int toRemove)
        {
            Warnings.RemoveAll(warning => warning == toRemove);
        }
            
        public void ClearWarnings()
        {
            Warnings.Clear();
        }

        public void AddWarning(int toAdd)
        {
            Warnings.Add(toAdd);
        }

        public void Resize(DateTime start, DateTime end)
        {
            TimeSpan startDelta = start - Start;
            TimeSpan endDelta = end - End;
            TimeSpan sizeDelta = endDelta - startDelta;

            if (startDelta.TotalMinutes != 0 || endDelta.TotalMinutes != 0)
            {
                Start = start;
                End = end;

                if (Edit != EditState.Added)
                    Edit = EditState.Modified;

                Modified = true;

                if (Jobs.Count > 0)
                {
                    // Move all jobs by the start delta
                    foreach (var job in Jobs)
                    {
                        job.Move(startDelta);

                        m_rolesService.MoveRolesForJob(job, startDelta, endDelta, sizeDelta, Details);                  
                    }

                    // If endDelta > 0
                    // Move the last jobs end time to fill the shift
                    if (sizeDelta.TotalMinutes > 0)
                    {
                        var lastJob = Jobs.Last();
                        lastJob.Resize(lastJob.Start, lastJob.End.Add(sizeDelta));
                    }

                    // If endDelta < 0
                    // Condense the last job(s) end time
                    // Remove jobs that shrink to zero
                    if (sizeDelta.TotalMinutes < 0)
                    {
                        var lastJob = Jobs.Last();
                        while (lastJob != null && lastJob.End > End)
                        {
                            if (lastJob.Start >= End)
                            {
                                DeleteJob(lastJob.ScheduledJobID);
                            }
                            else
                            {
                                lastJob.Resize(lastJob.Start, End);
                            }

                            lastJob = Jobs.Last();
                        }
                    }
                }
            }
        }

        public void AddDefaultRole ()
        {
            //todo need to be able to select start end times for role here

            if(Jobs.IsNullOrEmpty())
            {
                return;
            }

            if (Edit != EditState.Added)
                Edit = EditState.Modified;

            Modified = true;
            var modifiedJob = Jobs.First();

            //todo need to be able to select from all available roles for shift
            UpdateAndValidate();
        }

        public JobData AddDefaultJob(double hourDuration)
        {
            if (Jobs.Count == 0 || (End - Start).TotalHours + hourDuration >= 24)
                return null;

            if (Edit != EditState.Added)
                Edit = EditState.Modified;

            Modified = true;

            // check above is not enough on its own, ensure 

            // Add a 2 hour duration job of the same type as the last entry
            var newJob = Jobs[Jobs.Count-1].Copy ();
            newJob.ScheduledJobID = JobData.AssignUniqueID();
            newJob.Start = Jobs [Jobs.Count - 1].End;
            newJob.End = newJob.Start.AddHours (hourDuration);
            newJob.Edit = EditState.Added;
            Jobs.Add (newJob);
            UpdateAndValidate ();

            return newJob;
        }

        public void DeleteJob(int scheduledJobID)
        {
            var job = GetJob (scheduledJobID);

            if (job != null)
            {
                Jobs.Remove (job);
                UpdateAndValidate ();

                if (Edit != EditState.Added)
                    Edit = EditState.Modified;
                Modified = true;

                if (job.Edit != EditState.Added && job.ScheduledJobID >= 0)
                    DeletedJobs.Add (job);
            }
        }

        public JobData GetJob(int scheduledJobID)
        {
            foreach (var j in Jobs)
            {
                if (j.ScheduledJobID == scheduledJobID)
                    return j;
            }

            return null;
        }

        public JobData GetJob(DateTime time)
        {
            foreach (var j in Jobs)
            {
                if (time >= j.Start && time < j.End)
                    return j;
            }

            return null;
        }

        public void DeleteAllJobs()
        {
            if (Jobs.Count == 0)
                return;

            if (Edit != EditState.Added)
                Edit = EditState.Modified;
            Modified = true;

            // Clear all details 1st
            DeleteAllDetails ();

            // Clear warnings
            Warnings = new List<int> ();

            // Set everything as deleted
            foreach (var j in Jobs)
            {
                if (j.Edit != EditState.Added && j.ScheduledJobID >= 0)
                {
                    j.Edit = EditState.Deleted;
                    DeletedJobs.Add (j);
                }
            }

            Jobs.Clear ();
        }

        public string GetDescription()
        {
            IStringService localiser = Mvx.Resolve<IStringService>();
            return string.Format (@"{0} - {1:h\:mm} {2}", Start.ToString(localiser.Get("dd/MM/yyyy")), (End - Start), localiser.Get("hours_label"));
        }

        public bool IsCurrentSite()
        {
            SessionData session = Mvx.Resolve<SessionData> ();
            return session!=null && session.Site!=null && session.Site.SiteID == SiteID;
        }

        public void CalculateDetails(JDAScheduleRule rule, bool allowNoJob = false)
        {
            if (Edit != EditState.Added)
                Edit = EditState.Modified;
                
            Modified = true;
            
            var roleDetails = new List<ShiftDetailData>(m_rolesService.GenerateRoleDetailsForShift(Jobs, Details));
            
            DeleteAllDetails();
            
            var mealBreakDetails = new AutoMealBreakGenerator(rule).RegenerateMealsAndBreaks(this);
            
            foreach(var detail in mealBreakDetails)
            {
                detail.SetOriginalValues();
                AddDetail(detail, true, allowNoJob);
            }
            
            UpdateRoleDetails(m_rolesService.UpdatedRoleDetailsForShift(this, roleDetails));
        }

        private void UpdateRoleDetails (List<ShiftDetailData> adjustedRoles)
        {
            foreach (var adjsutedRole in adjustedRoles) 
            {
                AddDetailWithoutState(adjsutedRole);
            }
        }

        public void AddDetail(ShiftDetailData detail, bool setAdded = true, bool allowNoJob = false)
        {
            if (!detail.Start.HasValue)
                return;

            var job = GetJob (detail.Start.Value);

            if (job != null || allowNoJob == true)
            {
                if (Edit != EditState.Added)
                    Edit = EditState.Modified;
                Modified = true;

                if (setAdded)
                    detail.Edit = EditState.Added;
                    
                if(allowNoJob == false)
                    detail.ScheduledJobID = job.ScheduledJobID;
                    
                detail.SetOriginalValues ();
                Details.Add (detail);
                Details.Sort ((x, y) => x.Start.Value.CompareTo (y.Start.Value));
                UpdateAndValidate ();
            }
        }

        public void AddDetailWithoutState(ShiftDetailData detail)
        {
            Details.Add (detail);
            Details.Sort ((x, y) => x.Start.Value.CompareTo (y.Start.Value));
            UpdateAndValidate ();
        }

        public ShiftDetailData AddDefaultDetail(bool isBreak = false)
        {
            if (Edit != EditState.Added)
                Edit = EditState.Modified;
            Modified = true;

            bool shiftLongEnough = false;

            if(isBreak)
            {
                shiftLongEnough = (End - Start).TotalMinutes > c_defaultMinDurationBeforeBreak + c_defaultBreakDuration;
            }

            var detail = ShiftDetailData.Create (GenerateUniqueID());

            detail.Start = isBreak && shiftLongEnough ? Start.AddMinutes(c_defaultMinDurationBeforeBreak) : Start;
            detail.End = isBreak && shiftLongEnough ? Start.AddMinutes(c_defaultMinDurationBeforeBreak + c_defaultBreakDuration) : Start.AddMinutes (c_defaultBreakDuration);
            detail.ScheduledJobID = GetJob (Start).ScheduledJobID;
            detail.SetOriginalValues ();
            Details.Add (detail);

            UpdateAndValidate ();

            return detail;
        }

        public void DeleteDetail(int detailID)
        {
            var detail = GetDetail (detailID);

            if (detail != null)
            {
                Details.Remove (detail);

                if (Edit != EditState.Added)
                    Edit = EditState.Modified;
                Modified = true;

                if (detail.Edit != EditState.Added)
                    DeletedDetails.Add (detail);
            }
        }

        public ShiftDetailData GetDetail(int detailID)
        {
            foreach (var d in Details)
            {
                if (d.ShiftDetailID == detailID)
                    return d;
            }

            return null;
        }

        public void DeleteAllDetails()
        {
            if (Details.Count == 0)
                return;

            if (Edit != EditState.Added)
                Edit = EditState.Modified;
                
            Modified = true;

            foreach (var d in Details)
            {
                if (d.Edit != EditState.Added)
                    DeletedDetails.Add (d);
            }

            Details.Clear ();
        }

        public IList<ShiftDetailData> GetDetailsForJob(int jobScheduledID)
        {
            var details = new List<ShiftDetailData> ();

            foreach (var d in Details)
            {
                if (d.ScheduledJobID == jobScheduledID)
                    details.Add(d);
            }

            return details;
        }

        public bool UpdateAndValidate()
        {
            // Fix jobs and update times
            if (Jobs.Count > 0)
            {
                // Sort jobs by start time
                Jobs.Sort((e1,e2) => {return e1.Start.CompareTo(e2.Start);});

                for (int i = 1; i < Jobs.Count; ++i)
                {
                    DateTime start = Jobs [i - 1].End;
                    DateTime end = Jobs [i].Duration < 30 ? Jobs [i].Start.AddMinutes(30) : Jobs [i].End;
                    Jobs [i].Resize (start, end);
                }


                Start = Jobs.First().Start;
                End = Jobs.Last ().End;
            }

            if (Details.Count > 0)
            {
                bool deletedDetail = false;

                // Sort details by start time
                Details.Sort((e1,e2) => {return e1.Start.Value.CompareTo(e2.Start.Value);});

                // List out of range shift details
                var deleteIDs = Details
                    .Where(d => !d.IsRole)
                    .Where (x => (x.Start.Value < Start || x.End.Value > End)
                            || (x.End.Value < x.Start.Value)
                            || (x.End.Value == x.Start.Value)).Select(x => x.ShiftDetailID)
                    .ToList();


                // Warn for overlapping breaks
                bool detailsSameStartTime = false;

                foreach(var detail in Details.Where(d => !d.IsRole))
                {
                    foreach(var secondDetail in Details.Where(d => !d.IsRole))
                    {
                        if(detail.ShiftDetailID != secondDetail.ShiftDetailID)
                        {
                            if(detail.Start != null && detail.End != null && secondDetail.Start != null && secondDetail.End != null)
                            {
                                if(TimePeriodHelper.DoesOverlap((DateTime) detail.Start, (DateTime) detail.End, (DateTime) secondDetail.Start, (DateTime) secondDetail.End))
                                {
                                    detailsSameStartTime = true;
                                    deleteIDs.Add(detail.ShiftDetailID);
                                }                                    
                            }
                        }
                    }

                    // It is possible to get a shift detail to span two shifts, if this is the case
                    // delete the detail
                    foreach (var job in Jobs)
                    {
                        bool detailBeginsInJob = detail.Start.Value >= job.Start && detail.Start.Value < job.End;;

                        if (detailBeginsInJob)
                        {
                            if (detail.End.Value > job.End)
                                deleteIDs.Add(detail.ShiftDetailID);
                        }
                    }
                }

                // Delete details pending deletion
                List<int> deleteIDsNoDuplicates = deleteIDs.Distinct().ToList();
                deletedDetail = deleteIDsNoDuplicates.Count > 0;
                foreach (int id in deleteIDsNoDuplicates)
                {
                    DeleteDetail (id);
                    deletedDetail = true;
                }

                if (!m_settings.HideBreakWarnings)
                {
                    if (detailsSameStartTime)
                    {
                        Mvx.Resolve<IAlertBox> ().ShowOK ("Break(s) or Meal(s) Deleted", "Overlapping break(s) or meal(s) deleted.");
                    }
                    else if (deletedDetail)
                    {
                        Mvx.Resolve<IAlertBox> ().ShowOK ("Break(s) or Meal(s) Deleted", "Break(s) or meal(s) outside of the shift time period deleted.");
                    }
                }

                return !deletedDetail;
            }

            return true; // no details to check means validation is successful by default
        }

        public void RecalculateDetailsForRoles ()
        {
            var adjustedRoleDetails = m_rolesService.UpdatedRoleDetailsForShift(this);

            UpdateRoleDetails(adjustedRoleDetails);
        }

        private int EqualMinutesBetweenDetails(int numDetails, int durationMins)
        {
            if (numDetails == 0)
                return 0;

            int result = durationMins / (numDetails + 1);
            return (result / 15) * 15; // Snap to 15 minutes
        }

        public JDAScheduleAllocation FindBestMatchAllocationFromRule(JDAScheduleRule rule)
        {
            double shiftDurationHours = (End - Start).TotalHours;
            return FindBestMatchAllocationFromRule (rule, shiftDurationHours);
        }

        static public JDAScheduleAllocation FindBestMatchAllocationFromRule(JDAScheduleRule rule, double shiftDurationHours)
        {
            // Filter the allocations so these cover the current shift
            var allocations = rule.ScheduleAllocations;

//         This filter on allocations has been removed due to the REPL Dev server Schedule rules not correctly implemented for start and end time - e.g. both set to 12:00:00  
//         var allocations = rule.ScheduleAllocations.
//                Where (x => !x.StartTime.HasValue || (Start.TimeOfDay >= x.StartTime.Value.TimeOfDay && Start.TimeOfDay <= x.EndTime.Value.TimeOfDay));

            if (allocations == null)
            {
                Mvx.Trace("No allocations found, could not calculate breaks and meals");
            }

            var allocation = allocations.
                Where (x => shiftDurationHours >= (double)x.ShiftDuration).
                OrderByDescending (x => x.ShiftDuration).
                FirstOrDefault();

            if(allocation == null)
            {
                // If matching failed, pick the smallest matching allocation
                allocation = allocations.
                    OrderBy (x => x.ShiftDuration).
                    FirstOrDefault();
            }
                
            return allocation;
        }

        public static int GenerateUniqueID()
        {
            lock (s_uniqueIDLock)
            {
                return --s_uniqueID;
            }
        }

        public bool HasScheduleType()
        {
            if (String.IsNullOrEmpty (ScheduleType))
                return false;

            return true;
        }

        public bool IsManualShift()
        {
            if (HasScheduleType() == false)
                return false;

            if (ScheduleType == "Manual" || Modified == true)
                return true;

            return false;
        }

        public bool CanEdit()
        {
            return CanEditAtStart(Start);
        }

        public bool CanEditAtStart(DateTime start)
        {
            return !IsWithinPreventChangesTime (start) && !EditedShiftIsInThePastAndAllowed(start) && !IsReadOnly;
        }

        public bool IsWithinPreventChangesTime(DateTime testTime)
        {
            int hours = m_settings.PreventChangesHours;
            if (hours <= 0)
                return false;

            if (testTime < DateTime.Now.AddHours (hours))
                return true;

            return false;
        }

        private bool EditedShiftIsInThePastAndAllowed (DateTime? timeToTest)
        {
            if(!m_settings.BlockEditingInPast)
                return false;

            if (timeToTest.HasValue == false)
                return false;

            return timeToTest.Value < DateTime.Now;
        }

   }
}
