﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class SalesForecastDayItem : MvxNotifyPropertyChanged
    {        
        private static readonly string _BLANKTEXT = "null";
        private static readonly string _FORMATSPECIFIER = "0,0.00";

        public string m_stringDayValue;
        public string StringDayValue 
        {
            get
            {
                return DayValue.GetValueOrDefault(0).ToString(_FORMATSPECIFIER); 
            }
            set
            {  
                m_stringDayValue = value;
            } 
        }

        private decimal? m_dayValue;
        public decimal? DayValue 
        { 
            get 
            { 
                return m_dayValue; 
            }
            set 
            { 
                m_dayValue = value;
            } 
        }

        public decimal? OriginalDayValue { get; set; }

        public bool m_modified;
        public bool Modified 
        { 
            get{ return m_modified; } 
            set{ m_modified = value; RaisePropertyChanged (() => Modified); } 
        }

        public DateTime Date { get; set; }

        public SalesForecastDayItem ()
        {
            Modified = false;
        }

        public static decimal? CleanStringAndParse(string input)
        {
            if(input.Equals(_BLANKTEXT))
            {
                return null;
            }

            // remove commas
            input = input.Replace(",", "");

            // convert to decimal
            decimal outResult;
            bool success = decimal.TryParse (input, out outResult);

            if (success)
            {
                return outResult;
            }
            else
            {
                return null;
            }
        }

        private void ShowAlertForNumbers()
        {
            Mvx.Resolve<IAlertBox>().ShowOK
            ("","You must only enter numbers",null);
        }

        private bool IsEditedValueEqualToPrevious(string input, string toCompare)
        {
            return input.Equals (toCompare);
        }

        public bool SetDayValue(string value)
        {
            decimal? parsed = CleanStringAndParse(value);
            if (!parsed.HasValue)
            {
                // show alert
                if(value.Length > 0)
                {
                    ShowAlertForNumbers();
                }
                StringDayValue = DayValue.GetValueOrDefault (0).ToString (_FORMATSPECIFIER);
                Modified = false;
                return false;
            }
                
            if (Math.Round(parsed.GetValueOrDefault(0),2,MidpointRounding.AwayFromZero) == Math.Round(OriginalDayValue.GetValueOrDefault(0), 2,MidpointRounding.AwayFromZero))
            {
                Modified = false;
                return false;
            }

            DayValue = parsed;
            Modified = true;
            return true;
        }
    }
}