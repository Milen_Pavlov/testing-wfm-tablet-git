﻿using Consortium.Client.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Core.Model
{
    public class Till
    {
        #region Core properties

        // Properties
        public int TillId { get; set; }

        public string TillNumber { get; set; }
        public int OpeningSequence { get; set; }
        public int SiteId { get; set; }

        // Parents
        public int StatusId { get; set; }
        public TillStatus Status { get; set; }

        public int TypeId { get; set; }
        public TillType Type { get; set; }

        // Children
        public IList<TillWindow> Windows { get; set; }

        public void Add(TillWindow window)
        {
            if (Windows == null)
            {
                Windows = new List<TillWindow>();
            }

            Windows.Add(window);
            window.Till = this;
            window.TillId = TillId;
        }

        public void Add(IList<TillWindow> windows)
        {
            foreach (var window in windows)
            {
                Add(window);
            }
        }

        public IList<TillAllocation> Allocations { get; set; }

        #endregion

        public ModelState State { get; set; }

        public bool IsClosed
        {
            get
            {
                return (Status.Key != TillStatusKey.Open);
            }
        }

    }
}
