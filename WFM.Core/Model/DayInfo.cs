﻿using System;

namespace WFM.Core
{
    public class DayInfo
    {
        public float Scheduled { get; set; }
        public float Cost { get; set; }
    }
}
