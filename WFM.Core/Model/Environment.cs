﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class Environment : MvxNotifyPropertyChanged
    {
        private string m_name;
        public string Name
        {
            get { return m_name; }
            set { m_name = value; RaisePropertyChanged (() => Name); }
        }

        private string m_address;
        public string Address { get; set; }

        private string m_consortiumAddress;
        public string ConsortiumAddress
        {
            get { return m_consortiumAddress; }
            set { m_consortiumAddress = value; RaisePropertyChanged (() => ConsortiumAddress); }
        }

        private string m_consortiumUsername;
        public string ConsortiumUsername
        {
            get { return m_consortiumUsername; }
            set { m_consortiumUsername = value; RaisePropertyChanged (() => ConsortiumUsername); }
        }

        private string m_consortiumPassword;
        public string ConsortiumPassword
        {
            get { return m_consortiumPassword; }
            set { m_consortiumPassword = value; RaisePropertyChanged (() => ConsortiumPassword); }
        }

        private string m_myHoursAddress;
        public string MyHoursAddress
        {
            get { return m_myHoursAddress; }
            set { m_myHoursAddress = value; RaisePropertyChanged (() => MyHoursAddress); }
        }

        private string m_MyHoursUsername;

        public string MyHoursUsername
        {
            get { return m_MyHoursUsername; }
            set { m_MyHoursUsername = value; RaisePropertyChanged (() => MyHoursUsername); }
        }

        private string m_MyHoursPassword;

        public string MyHoursPassword
        {
            get { return m_MyHoursPassword;  }
            set { m_MyHoursPassword = value; RaisePropertyChanged (() => MyHoursPassword); }
        }

        private string m_analyticsTrackingID;
        public string AnalyticsTrackingID
        {
            get { return m_analyticsTrackingID; }
            set { m_analyticsTrackingID = value; RaisePropertyChanged (() => AnalyticsTrackingID); }
        }

        //Extra debug values#
        private bool m_debugOnly;
        public bool DebugOnly
        {
            get { return m_debugOnly; }
            set { m_debugOnly = value; RaisePropertyChanged (() => DebugOnly); }
        }

        private bool m_isAddRow;
        public bool IsAddRow
        {
            get { return m_isAddRow; }
            set { m_isAddRow = value; RaisePropertyChanged (() => IsAddRow); }
        }

        private bool m_inEditMode;
        public bool InEditMode
        {
            get { return m_inEditMode; }
            set { m_inEditMode = value; RaisePropertyChanged (() => InEditMode); }
        }

        private bool m_editable;
        public bool Editable
        {
            get { return m_editable; }
            set { m_editable = value; RaisePropertyChanged (() => Editable); }
        }

        private string m_ssoStart;
        public string SSOStart
        {
            get { return m_ssoStart; }
            set { m_ssoStart = value; RaisePropertyChanged (() => SSOStart); }
        }

        private bool m_sendScheduleAudit;
        public bool SendScheduleAudit
        {
            get { return m_sendScheduleAudit; }
            set { m_sendScheduleAudit = value; RaisePropertyChanged (() => SendScheduleAudit); }
        }

        private string m_combinedDeptAddress;
        public string CombinedDeptAddress
        {
            get { return m_combinedDeptAddress; }
            set { m_combinedDeptAddress = value; RaisePropertyChanged (() => CombinedDeptAddress); }
        }

        private string m_combinedDeptUsername;
        public string CombinedDeptUsername
        {
            get { return m_combinedDeptUsername; }
            set { m_combinedDeptUsername = value; RaisePropertyChanged (() => CombinedDeptUsername); }
        }

        private string m_combinedDeptPassword;
        public string CombinedDeptPassword
        {
            get { return m_combinedDeptPassword;  }
            set { m_combinedDeptPassword = value; RaisePropertyChanged (() => CombinedDeptPassword); }
        }

        private bool m_useCustomScheduleRequest = false;
        public bool UseCustomScheduleRequest 
        {
            get { return m_useCustomScheduleRequest; }
            set { m_useCustomScheduleRequest = value; RaisePropertyChanged (() => UseCustomScheduleRequest); }
        }

        private bool m_sendWeeklyWarningsUpdates = true;
        public bool SendWeeklyWarningsUpdates 
        {
            get { return m_sendWeeklyWarningsUpdates; }
            set { m_sendWeeklyWarningsUpdates = value; RaisePropertyChanged (() => SendWeeklyWarningsUpdates); }
        }

        private string m_labourDemandAddress;
        public string LabourDemandAddress
        {
            get
            {
                return m_labourDemandAddress;
            }

            set
            {
                m_labourDemandAddress = value;
                RaisePropertyChanged(() => LabourDemandAddress);
            }
        }

        [JsonIgnore]
        public Dictionary<string,string> Settings { get; set;} 

        private IConfiguration m_config;

        [JsonIgnore]
        public IConfiguration Configuration
        {
            get
            {
                if (m_config == null)
                {
                    m_config = new ConfigurationStore();

                    if (Settings != null)
                    {
                        m_config.Append(Settings);
                    }
                }

                return m_config;
            }
        }
    }
}
