﻿using System;

namespace WFM.Core
{
    public class ScheduleTime
    {
        public const int HourIntervals = 4; // 15min intervals
        public const int DayIntervals = 24 * HourIntervals;
        public const int WeekIntervals = DayIntervals * 7;
        public const int IntervalMinutes = 60 / HourIntervals;
        public const int MinutesPerHour = 60;
        public const int HoursPerDay = 24;
        public const int MinutesPerDay = MinutesPerHour * HoursPerDay;

        public static int HoursToIntervals(double hours)
        {
            return (int)Math.Floor((hours * MinutesPerHour) / IntervalMinutes);
        }

        public static DateTime ClampInterval(DateTime time)
        {
            return new DateTime (time.Year, time.Month, time.Day, time.Hour, 
                (time.Minute / IntervalMinutes) * IntervalMinutes, 0);
        }
    }
}

