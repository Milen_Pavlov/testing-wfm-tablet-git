﻿using System;

namespace WFM.Core
{
    public interface IJobRoleFilterGroup
    {
        bool ApplyFilter(int roleId);
    }
}

