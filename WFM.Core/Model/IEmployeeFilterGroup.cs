﻿using System;

namespace WFM.Core
{
    public interface IEmployeeFilterGroup
    {
        bool ShouldApplyFilter(EmployeeData employee, string filterGroup, int[] matchingFilterIds, int? dayIndex);
    }
}

