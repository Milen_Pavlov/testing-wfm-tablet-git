﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    //Horrible JDA Response wrapper
    public class JDASiteTimeCardsForData
    {
        public JDASiteTimeCard[] Data { get; set; }

        public DateTime? BusinessDate { get; set; }
    }

    public class JDASiteTimeCard
    {
        public string CallOffScheduledShiftID { get; set; }

        public string PayAttributeID { get; set; }

        public double NetScheduledHours { get; set; }

        public List<string> PunchExceptions { get; set; }//TODO Find out

        public DateTime? ScheduledStart { get; set; }

        public double CallOffNetHours { get; set; }

        public DateTime? PunchedEnd { get; set; }

        public DateTime? CallOffStart { get; set; }

        public int? ScheduledShiftID { get; set; }

        public DateTime? PunchedStart { get; set; }

        public string LastPunchCode { get; set; }

        public bool IsWorkedTimeCallOff { get; set; }

        public int EmployeeID { get; set; }

        public double NetWorkedHours { get; set; }

        public int? PunchedShiftID { get; set; }

        public string PayAdjustmentID { get; set; }

        public DateTime? CallOffEnd { get; set; }

        public List<int> JobIDs { get; set; }

        public DateTime? ScheduledEnd { get; set; }

        public DateTime? LastPunchTime { get; set; }

        public bool IsCallOffShift { get; set; }

        public JDASiteTimeCard ()
        {
        }
    }
}

