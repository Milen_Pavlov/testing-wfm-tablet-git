﻿using System;
using System.Linq;

namespace WFM.Core
{
    public class EmployeeTimecardSummary
    {
        public string EmployeeName { get; set; }

        public int EmployeeID { get; set; }

        public double Scheduled { get; set; }

        public double NetHours { get; set; }

        public double Adjustments { get; set; }

        public double OtherHours { get; set; }

        public double TotalHours { get; set; }

        public int Exceptions { get; set; }

        public EmployeeTimecardSummary ()
        {
        }

        public EmployeeTimecardSummary(JDAEmployeeTimecardSummary summary, JDAEmployeeInfo[] employees)
        {
            EmployeeID = summary.EmployeeID;
            var employee = employees.FirstOrDefault (x => x.EmployeeID == EmployeeID);
            if (employee != null)
            {
                var firstName = (string.IsNullOrEmpty(employee.NickName) ? employee.FirstName : employee.NickName);
                EmployeeName = string.Format("{0}, {1}", employee.LastName, firstName);
            }

            Scheduled = summary.ScheduledHours.GetValueOrDefault();
            NetHours = summary.NetHours.GetValueOrDefault ();
            Adjustments = summary.AdjustmentHours.GetValueOrDefault ();
            OtherHours = summary.OtherHours.GetValueOrDefault ();
            TotalHours = summary.TotalHours.GetValueOrDefault ();
            Exceptions = summary.ExceptionCount.GetValueOrDefault ();
        }
    }
}

