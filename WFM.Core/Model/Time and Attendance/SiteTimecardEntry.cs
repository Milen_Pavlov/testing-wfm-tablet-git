using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public enum SiteTimecardStatus
    {
        NoData,
        OffClock,
        OnClock,
    }

    public class SiteTimecardEntry : MvxNotifyPropertyChanged
    {
        public string EmployeeName { get; set; }

        public int EmployeeID { get; set; }

        public List<JobData> Jobs { get; set; }

        public int ShiftID { get; set; }

        public DateTime? ShiftStartPlanned { get; set; }

        public DateTime? ShiftStartActual { get; set; }

        public DateTime? ShiftEndPlanned { get; set; }

        public DateTime? ShiftEndActual { get; set; }

        public double ShiftDurationHoursPlanned { get; set; }

        public double ShiftDurationHoursActual { get; set; }

        public SiteTimecardStatus Status { get; set; }

        public List<TimecardException> Exceptions { get; set; }//TODO

        public List<TimecardException> FilteredExceptions { get; set; }

        public bool DailyCell { get; set; } = true;

        public int? ScheduledShiftID { get; set; }

        public int? PunchedShiftID { get; set; }

        public DateTime? Date { get; set; }


        //Exception State helpers

        public bool EndTimeMajorException { get; set; }

        public bool StartTimeMajorException { get; set; }

        public bool DurationMajorException { get; set; }

        public bool EndTimeMinorException { get; set; }

        public bool StartTimeMinorException { get; set; }

        public bool DurationMinorException { get; set; }

        public bool DurationMissing { get; set; }

        public bool StartTimeMissing { get; set; }

        public bool EndTimeMissing { get; set; }

        public SiteTimecardEntry() 
        { 
            Jobs = new List<JobData> ();
            Exceptions = new List<TimecardException> ();
        }

        public SiteTimecardEntry(JDASiteTimeCard jda, JDAJob[] allJobs, JDAEmployeeInfo[] employees)
        {
            EmployeeID = jda.EmployeeID;
            SetNameIfPossible (employees);

            ScheduledShiftID = jda.ScheduledShiftID;
            PunchedShiftID = jda.PunchedShiftID;

            ShiftStartPlanned = jda.ScheduledStart;
            ShiftStartActual = jda.PunchedStart;

            ShiftEndPlanned = jda.ScheduledEnd;
            ShiftEndActual = jda.PunchedEnd;

            ShiftDurationHoursPlanned = jda.NetScheduledHours;
            ShiftDurationHoursActual = jda.NetWorkedHours;

            if (ShiftStartPlanned != null)
                Date = ShiftStartPlanned;
            else if (ShiftStartActual != null)
                Date = ShiftStartActual;
            else if (ShiftEndPlanned != null)
                Date = ShiftEndPlanned;
            else if (ShiftEndActual != null)
                Date = ShiftEndActual;
            
            if (ShiftStartActual.HasValue && ShiftEndActual.HasValue)
                Status = SiteTimecardStatus.OffClock;
            else if (ShiftStartActual.HasValue && !ShiftEndActual.HasValue)
                Status = SiteTimecardStatus.OnClock;
            else
                Status = SiteTimecardStatus.NoData;

            if(jda.ScheduledShiftID.HasValue)
                ShiftID = jda.ScheduledShiftID.Value;

            if(jda.PunchedShiftID.HasValue)
                PunchedShiftID = jda.PunchedShiftID.Value;

            Jobs = new List<JobData> ();
            Exceptions = new List<TimecardException> ();

            if(jda.JobIDs!=null)
                Jobs = jda.JobIDs.Select (x => allJobs.FirstOrDefault (y => y.JobID == x)).Where (x => x != null).Select (x => new JobData (x)).ToList ();

            if (jda.PunchExceptions != null)
                Exceptions = jda.PunchExceptions.Select (x => new TimecardException(x)).ToList();//TODO wrap into object for colour highlights (Major, Minor etc)

            GenerateExceptionState ();
        }

        public void SetNameIfPossible(JDAEmployeeInfo[] employees)
        {
            var employee = employees.FirstOrDefault (x => x.EmployeeID == EmployeeID);
            if (employee != null)
            {
                var firstName = (string.IsNullOrEmpty(employee.NickName) ? employee.FirstName : employee.NickName);
                EmployeeName = string.Format("{0}, {1}", employee.LastName, firstName);
            }
        }

        public void GenerateExceptionState()
        {
            StartTimeMinorException = false;
            StartTimeMajorException = false;
            StartTimeMissing = false;

            EndTimeMinorException = false;
            EndTimeMajorException = false;
            EndTimeMissing = false;

            DurationMajorException = false;
            DurationMinorException = false;
            DurationMissing = false;

            //Figure out which times are Major/Minor/None etc, which need "missing" or which durations need "???"
            //JDA helpful give us no exception IDs, just literally strings to display
            //So for now we can catch Unpaired as we add those ourselves....otherwise we have to scan strings for key words
            foreach (var exception in Exceptions)
            {
                if (exception.ExceptionType == TimecardExceptionType.Unpaired)
                {
                    //Mark start or end as "missing" and Major
                    //Mark duration as "???" and Major
                    if (!ShiftStartActual.HasValue)
                    {
                        StartTimeMajorException = true;
                        StartTimeMissing = true;
                    }

                    if (!ShiftEndActual.HasValue)
                    {
                        EndTimeMajorException = true;
                        EndTimeMissing = true;
                    }

                    DurationMajorException = true;
                    DurationMissing = true;
                }
                else
                {
                    //Horrible string scanning...In Late/Early, Out Late/Early, Maximum hours for week, Long Meal, Long Break, Working without schedule etc
                    if (exception.Text.Contains ("In"))
                    {
                        StartTimeMinorException = true;
                        DurationMinorException = true;
                    }
                    else if (exception.Text.Contains ("Out"))
                    {
                        EndTimeMinorException = true;
                        DurationMinorException = true;
                    }
                    else
                    {
                        //Just do it all, we don't know all errors and cases
                        StartTimeMinorException = true;
                        EndTimeMinorException = true;
                        DurationMinorException = true;
                    }
                }
            }
        }

        public void SetUnpairedPunchFilter(bool setFilter)
        {
            if (setFilter && Exceptions != null)
                FilteredExceptions = Exceptions.Where (x => x.ExceptionType == TimecardExceptionType.Unpaired).ToList ();
            else
                FilteredExceptions = Exceptions;

            RaisePropertyChanged (() => FilteredExceptions);
        }
    }
}

