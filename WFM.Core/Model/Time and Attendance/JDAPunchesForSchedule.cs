﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAPunchesForSchedule
    {
        public bool AllowPunchForBreak { get; set; }
        public bool AllowPunchForMeal { get; set; }
        public List<JDACanWorkJobs> CanWorkJobs { get; set; }
        public List<JDAShiftPunches> ShiftPunches { get; set;}

        public JDAPunchesForSchedule()
        {
        }
    }
}

