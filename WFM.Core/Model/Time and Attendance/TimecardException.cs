﻿using System;

namespace WFM.Core
{
    public enum TimecardExceptionType
    {
        Unpaired,
        JDA,
    }
    public class TimecardException
    {
        public bool IsMajor { get; set; }

        public string Text { get; set; }

        public TimecardExceptionType ExceptionType { get; set; }

        public TimecardException()
        {
            Text = string.Empty;
            ExceptionType = TimecardExceptionType.JDA;
        }

        public TimecardException (string text, TimecardExceptionType excType = TimecardExceptionType.JDA,  bool isMajor = false)
        {
            IsMajor = isMajor;
            Text = text;
            ExceptionType = excType;
        }
    }
}

