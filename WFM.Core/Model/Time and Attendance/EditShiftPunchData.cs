using System;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public enum PunchCodes
    {
        Shift,
        Job,
        Break,
        Meal,

        Unknown,
    }

    public class EditShiftPunchData : MvxNotifyPropertyChanged
    {
        public PunchCodes PunchCode { get; set; }
        private string m_description;
        public string Description { get { return m_description; }  set { m_description = value; RaisePropertyChanged(() => Description); } }
        public DateTime? Scheduled { get; set; }
        private DateTime? m_actual;
        public DateTime? Actual { get { return m_actual; } set { m_actual = value; CanDelete = true; RaisePropertyChanged(() => CanMatch); RaisePropertyChanged(() => Actual); } }
        public int? ShiftID { get; set; }
        public bool CanMatch{ get { return (Scheduled.HasValue && !Actual.HasValue && CanEdit); } set { RaisePropertyChanged(() => CanMatch); } }
        public int? ScheduleID { get; set; }
        public int? PunchID { get; set; }
        public int? PunchDetailID { get; set; }
        public int? JobID { get; set; }
        public bool m_isStart;
        public bool IsStart { get { return m_isStart; } set { m_isStart = value; RaisePropertyChanged(() =>IsStart); } }
        public int DetailIndex { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get { return m_actual != null && CanEdit; } set { RaisePropertyChanged (() => CanDelete); } }

        public EditShiftPunchData ()
        {
            
        }
            
        static public PunchCodes GetPunchCode(string punchString)
        {
            if (punchString == "s")
                return PunchCodes.Shift;

            if (punchString == "j")
                return PunchCodes.Job;

            if (punchString == "b")
                return PunchCodes.Break;

            if (punchString == "m")
                return PunchCodes.Meal;

            return PunchCodes.Unknown;
        }

        static public string GetPunchCode(PunchCodes punchCode)
        {
            switch (punchCode)
            {
                case PunchCodes.Shift:
                    return "s";
                case PunchCodes.Meal:
                    return "m";
                case PunchCodes.Break:
                    return "b";
                case PunchCodes.Job:
                    return "j";
                case PunchCodes.Unknown:
                default:
                    return String.Empty;    
            }
        }
    }
}

