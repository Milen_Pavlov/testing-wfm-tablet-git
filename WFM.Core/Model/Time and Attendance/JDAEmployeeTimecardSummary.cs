﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class JDAEmployeeTimecardSummary
    {
        public double? ProjectedHours { get; set; }
        public double? TotalHours { get; set; }
        public int EmployeeID { get; set; }
        public double? ScheduledHours { get; set; }
        public int? ExceptionCount { get; set; }
        public string LastPunchCode { get; set; }
        public double? PaidAmount { get; set; }
        public double? OtherHours { get; set; }
        public double? NetHours { get; set; }
        public double? AdjustmentHours { get; set; }
    }
}
