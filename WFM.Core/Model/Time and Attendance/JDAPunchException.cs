﻿using System;

namespace WFM.Core
{
    public class JDAPunchException
    {
        public DateTime? BusinessDate { get; set; }

        public string WorkedBusinessUnitName { get; set; }

        public string HomeBusinessUnitName { get; set; }

        public int? ShiftID { get; set; }

        public string EmployeeFullName { get; set; }

        public int? EmployeeID { get; set; }

        public int? HomeBusinessUnitID { get; set; }

        public string BadgeNumber { get; set; }

        public int? WorkedBusinessUnitID { get; set; }

        public string ExceptionName { get; set; }

        public JDAPunchException ()
        {
        }
    }
}

