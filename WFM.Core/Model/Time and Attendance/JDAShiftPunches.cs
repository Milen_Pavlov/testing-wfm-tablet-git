﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace WFM.Core
{
    public class JDAShiftPunches
    {
        public DateTime? End { get; set;}
        public DateTime? Start { get; set; }
        public int? EndPunchID { get; set; }
        public DateTime? ShiftStart { get; set; }
        public int? ShiftID { get; set; }
        public int? SchedShiftID { get; set; }
        public string JobName { get; set; }
        public int? StartPunchID { get; set; }
        public int? EmployeeID { get; set; }
        public string PunchCode { get; set; }

        public DateTime? ScheduledStartTime { get; set; }
        public DateTime? ScheduledEndTime { get; set; }
            
        public JDAShiftPunches()
        {
        }
    }

    public class JDAShiftPunchesComparer: IEqualityComparer<JDAShiftPunches>
    {
        public bool Equals(JDAShiftPunches x, JDAShiftPunches y)
        {
            if (!x.End.GetValueOrDefault().Equals (y.End.GetValueOrDefault()))
                return false;
            if (!x.Start.GetValueOrDefault().Equals (y.Start.GetValueOrDefault()))
                return false;
            if (!x.EndPunchID.GetValueOrDefault().Equals(y.EndPunchID.GetValueOrDefault()))
                return false;
            if (!x.ShiftStart.GetValueOrDefault().Equals (y.ShiftStart.GetValueOrDefault()))
                return false;
            if (!x.ShiftID.GetValueOrDefault().Equals (y.ShiftID.GetValueOrDefault()))
                return false;
            if (!x.SchedShiftID.GetValueOrDefault().Equals (y.SchedShiftID.GetValueOrDefault()))
                return false;
            if (!String.IsNullOrEmpty(x.JobName) && !x.JobName.Equals (y.JobName))
                return false;
            if (!String.IsNullOrEmpty(y.JobName) && !y.JobName.Equals (x.JobName))
                return false;
            if (!x.StartPunchID.GetValueOrDefault().Equals(y.StartPunchID.GetValueOrDefault()))
                return false;
            if (!x.EmployeeID.GetValueOrDefault().Equals(y.EmployeeID.GetValueOrDefault()))
                return false;
            if (!String.IsNullOrEmpty(x.PunchCode) && !x.PunchCode.Equals (y.PunchCode))
                return false;
            if (!String.IsNullOrEmpty(y.PunchCode) && !y.PunchCode.Equals (x.PunchCode))
                return false;
            if (!x.ScheduledStartTime.GetValueOrDefault().Equals (y.ScheduledStartTime.GetValueOrDefault()))
                return false;
            if (!x.ScheduledEndTime.GetValueOrDefault().Equals (y.ScheduledEndTime.GetValueOrDefault()))
                return false;
            return true;
        }

        public int GetHashCode (JDAShiftPunches obj)
        {
            return obj.End.GetValueOrDefault().GetHashCode () + 
                obj.Start.GetValueOrDefault().GetHashCode () +
                obj.EndPunchID.GetValueOrDefault().GetHashCode() + 
                obj.ShiftStart.GetValueOrDefault().GetHashCode () + 
                obj.ShiftID.GetValueOrDefault().GetHashCode () +
                obj.SchedShiftID.GetValueOrDefault().GetHashCode () + 
                (String.IsNullOrEmpty(obj.JobName) ? 0 : obj.JobName.GetHashCode ()) + 
                obj.StartPunchID.GetValueOrDefault().GetHashCode() + 
                obj.EmployeeID.GetValueOrDefault().GetHashCode () +
                (String.IsNullOrEmpty(obj.PunchCode) ? 0 : obj.PunchCode.GetHashCode ()) + 
                obj.ScheduledStartTime.GetValueOrDefault().GetHashCode () + 
                obj.ScheduledEndTime.GetValueOrDefault().GetHashCode ();
        }
    }
}

