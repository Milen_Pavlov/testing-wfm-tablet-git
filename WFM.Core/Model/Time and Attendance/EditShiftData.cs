﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class EditShiftData
    {
        public bool AllowMealPunches { get; set; }
        public bool AllowBreakPunches { get; set; }
        public string EmployeeName { get; set; }
        public string LoginName { get; set; }
        public int? EmployeeID { get; set; }
        public int? PrimaryJobID { get; set; }
        public List<JDACanWorkJobs> Jobs { get; set; }
        public List<EditShiftPunchDetails> Details { get; set; }
        public List<JDANameAndID> AuditReasons { get; set; }
        public bool IsReadOnly { get; set; }

        public int? AuditReason { get; set; }

        public EditShiftData ()
        {
            
        }

        public EditShiftData(JDAPunchesForSchedule punches, JDAEmployeeInfo[] employees, List<JDASchedulePunches> scheduledPunches, List<JDANameAndID> auditReasons,
                             bool allowMealPunches, bool allowBreakPunches, bool allowJobPunches)
        {
            AllowMealPunches = punches.AllowPunchForMeal;
            AllowBreakPunches = punches.AllowPunchForBreak;

            EmployeeID = punches.ShiftPunches[0].EmployeeID;

            var employee = employees.FirstOrDefault (x => x.EmployeeID == EmployeeID);
            if (employee != null)
            {
                var firstName = (string.IsNullOrEmpty(employee.NickName) ? employee.FirstName : employee.NickName);
                EmployeeName = string.Format("{0}, {1}", employee.LastName, firstName);
                LoginName = employee.LoginName;
            }

            Jobs = punches.CanWorkJobs;
            var primaryJob = Jobs.FirstOrDefault (x => x.IsPrimary);
            if(primaryJob != null)
            {
                PrimaryJobID = primaryJob.ID; 
            }
            Details = CreateDetails (punches.ShiftPunches, scheduledPunches, allowMealPunches, allowBreakPunches, allowJobPunches);
            AuditReasons = auditReasons;

            IsReadOnly = false;
        }

        public EditShiftData(SiteTimecardEntry timecard, JDAEmployeeInfo[] employees, List<JDANameAndID> auditReasons)
        {
            AllowMealPunches = false;
            AllowBreakPunches = false;

            EmployeeID = timecard.EmployeeID;

            var employee = employees.FirstOrDefault (x => x.EmployeeID == EmployeeID);
            if (employee != null)
            {
                var firstName = (string.IsNullOrEmpty(employee.NickName) ? employee.FirstName : employee.NickName);
                EmployeeName = string.Format("{0}, {1}", employee.LastName, firstName);
                LoginName = employee.LoginName;
                Jobs = new List<JDACanWorkJobs>() { new JDACanWorkJobs() { IsPrimary = true, ID = -1, Name = ""} };
                PrimaryJobID = -1; 
            }

            Details = new List<EditShiftPunchDetails>() 
            {
                new EditShiftPunchDetails() { JobID = -1, JobName = "", PunchCode = PunchCodes.Shift }
            };

            AuditReasons = auditReasons;

            IsReadOnly = true;
        }

        private List<EditShiftPunchDetails> CreateDetails(List<JDAShiftPunches> punches, List<JDASchedulePunches> scheduledPunches,
                                                          bool allowMealPunches, bool allowBreakPunches, bool allowJobPunches)
        {
            int? scheduleID = null;
            int? startPunchID = null;
            if (punches != null && punches.Count > 0)
            {
                scheduleID = punches[0].SchedShiftID;
                startPunchID = punches[0].StartPunchID.HasValue ? punches[0].StartPunchID.Value : Mvx.Resolve<DailyOverviewEditShiftService> ().GetNewPunchID();
            }
                
            List<EditShiftPunchDetails> result = new List<EditShiftPunchDetails> ();
            foreach (var scheduledPunch in scheduledPunches)
            {
                int? detailPunchID = Mvx.Resolve<DailyOverviewEditShiftService> ().GetNewPunchID();

                if (scheduledPunch.Start.HasValue)
                {
                    var existing = result.Where (x => x.JobName == scheduledPunch.Description && x.ShiftID == scheduledPunch.ShiftID && x.ScheduledStart == scheduledPunch.Start);
                    if (existing != null && existing.Count () > 0)
                        continue;
                 
                    EditShiftPunchDetails newDetail = new EditShiftPunchDetails ();
                    newDetail.ScheduleID = scheduleID;
                    newDetail.PunchCode = EditShiftPunchData.GetPunchCode (scheduledPunch.PunchCode);
                    newDetail.ShiftID = scheduledPunch.ShiftID;
                    newDetail.JobName = scheduledPunch.Description;
                    if(scheduledPunch.Description != null)
                        newDetail.JobID = Jobs.First (x => x.Name == scheduledPunch.Description).ID;
                    newDetail.OriginalJobID = newDetail.JobID;
                    newDetail.ScheduledStart = scheduledPunch.Start;
                    newDetail.StartPunchID = startPunchID;
                    newDetail.StartPunchDetailID = detailPunchID;
                    newDetail.ScheduledEnd = scheduledPunch.End;
                    newDetail.EndPunchID = scheduledPunch.EndPunchID.GetValueOrDefault (startPunchID.Value);
                    newDetail.EndPunchDetailID = detailPunchID;

                    // Filter out meal and break punches if punches aren't allowed for them.
                    if(newDetail.PunchCode == PunchCodes.Meal && (AllowMealPunches == false || allowMealPunches == false))
                        continue;

                    if(newDetail.PunchCode == PunchCodes.Break && (AllowBreakPunches == false || allowBreakPunches == false))
                        continue;

                    if(newDetail.PunchCode == PunchCodes.Job && allowJobPunches == false)
                        continue;
                    
                    result.Add (newDetail);
                }
            }

            // find matching actual punches, or add new entries for actual punches.
            punches = punches.Distinct(new JDAShiftPunchesComparer()).ToList();
            foreach (var punch in punches)
            {
                if (punch.Start.HasValue)
                {
                    var existing = result.Where (x => x.MatchesJDAShiftPunch (punch));
                    bool found = false;
                    if (existing != null)
                    {
                        foreach (var entry in existing)
                        {                            
                            entry.ActualStart = punch.Start;
                            entry.OriginalStart = punch.Start;
                            entry.ActualEnd = punch.End;
                            entry.OriginalEnd = punch.End;
                            entry.StartPunchDetailID = punch.StartPunchID;
                            entry.EndPunchDetailID = punch.EndPunchID;
                            found = true;
                            break;
                        }
                    }

                    int? detailPunchID = punch.StartPunchID.HasValue ? punch.StartPunchID.Value : Mvx.Resolve<DailyOverviewEditShiftService> ().GetNewPunchID ();
                    if(found == false)
                    {
                        EditShiftPunchDetails newDetail = new EditShiftPunchDetails ();
                        newDetail.ScheduleID = punch.SchedShiftID;
                        newDetail.ShiftID = punch.ShiftID;
                        newDetail.JobName = punch.JobName;
                        if(String.IsNullOrEmpty(punch.JobName) == false)
                            newDetail.JobID = Jobs.First (x => x.Name == punch.JobName).ID;
                        newDetail.OriginalJobID = newDetail.JobID;
                        newDetail.ActualStart = punch.Start;
                        newDetail.OriginalStart = punch.Start;
                        newDetail.StartPunchID = punch.ShiftID;
                        newDetail.StartPunchDetailID = detailPunchID;
                        newDetail.ActualEnd = punch.End;
                        newDetail.OriginalEnd = punch.End;
                        newDetail.EndPunchID = punch.ShiftID;
                        newDetail.EndPunchDetailID = punch.EndPunchID.GetValueOrDefault(detailPunchID.Value);
                        newDetail.PunchCode = EditShiftPunchData.GetPunchCode (punch.PunchCode);

                        // Filter out meal and break punches if punches aren't allowed for them.
                        // There shouldn't be any here if they are disabled, but we'll check to make sure.
                        // Filter out meal and break punches if punches aren't allowed for them.
                        if(newDetail.PunchCode == PunchCodes.Meal && (AllowMealPunches == false || allowMealPunches == false))
                            continue;

                        if(newDetail.PunchCode == PunchCodes.Break && (AllowBreakPunches == false || allowBreakPunches == false))
                            continue;

                        if(newDetail.PunchCode == PunchCodes.Job && allowJobPunches == false)
                            continue;
                        
                        result.Add (newDetail);
                    }
                }
            }
                
            return result;
        }
    }
}

