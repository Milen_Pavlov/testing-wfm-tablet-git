﻿using System;

namespace WFM.Core
{
    public class EditShiftPunchDetails
    {
        public PunchCodes PunchCode { get; set; }
        public int? ScheduleID { get; set; }
        public int? OriginalJobID { get; set; }
        public int? JobID { get; set; }
        public string JobName { get; set; }
        public DateTime? ScheduledStart { get; set; }
        public DateTime? ScheduledEnd{ get; set; }
        public DateTime? ActualStart { get; set; }
        public DateTime? ActualEnd { get; set; }
        public DateTime? OriginalStart { get; set; }
        public DateTime? OriginalEnd { get; set; }
        public int? ShiftID { get; set; }
        public int? StartPunchID { get; set; }
        public int? EndPunchID { get; set; }
        public int? StartPunchDetailID { get; set; }
        public int? EndPunchDetailID { get; set; }

        public EditShiftPunchDetails ()
        {
        }
            
        public bool MatchesJDAShiftPunch(JDAShiftPunches punch)
        {
            var punchCode = PunchCodes.Unknown;
            punchCode = EditShiftPunchData.GetPunchCode (punch.PunchCode);

            if (PunchCode != punchCode)
                return false;

            if(JobName != punch.JobName && (!String.IsNullOrEmpty(JobName) || !String.IsNullOrEmpty(punch.JobName)))
                return false;

            if (punchCode != PunchCodes.Shift)
            {
                if (ScheduledStart.HasValue && ActualStart.HasValue)
                    return false;
            }

            return true;
        }

        public bool HasChanges()
        {
            if (ActualStart.HasValue != OriginalStart.HasValue)
                return true;

            if (ActualStart.HasValue && ActualStart.Value != OriginalStart.Value)
                return true;

            if (OriginalStart.HasValue && OriginalStart.Value != ActualStart.Value)
                return true;

            if (ActualEnd.HasValue != OriginalEnd.HasValue)
                return true;

            if (ActualEnd.HasValue && ActualEnd.Value != OriginalEnd.Value)
                return true;

            if (OriginalEnd.HasValue && OriginalEnd.Value != ActualEnd.Value)
                return true;

            return false;
        }

        public bool IsStartPunchNew()
        {
            return IsPunchNew (OriginalStart, ActualStart);
        }

        public bool IsStartPunchDeleted()
        {
            return IsPunchDeleted (OriginalStart, ActualStart);
        }

        public bool IsStartPunchModified()
        {
            return IsPunchModified (OriginalStart, ActualStart);
        }

        public bool IsEndPunchNew()
        {
            return IsPunchNew (OriginalEnd, ActualEnd);
        }

        public bool IsEndPunchDeleted()
        {
            return IsPunchDeleted (OriginalEnd, ActualEnd);
        }

        public bool IsEndPunchModified()
        {
            return IsPunchModified (OriginalEnd, ActualEnd);
        }

        public bool IsPunchNew(DateTime? original, DateTime? actual)
        {
            if (actual.HasValue == false)
                return false;
            if (original.HasValue)
                return false;

            return true;   
        }

        public bool IsPunchDeleted(DateTime? original, DateTime? actual)
        {
            if (original.HasValue == false)
                return false;
            if (actual.HasValue)
                return false;

            return true;
        }

        public bool IsPunchModified(DateTime? original, DateTime? actual)
        {
            if (original.HasValue == false)
                return false;
            if (actual.HasValue == false)
                return false;
            if (original.Value == actual.Value)
                return false;

            return true;
        }

        public bool HasJobBeenDeleted()
        {
            return IsPunchDeleted (OriginalStart, ActualStart);
        }

        public bool HasJobChanged()
        {
            if (OriginalJobID.HasValue == false)
                return false;
            if (JobID.HasValue == false)
                return false;
            if (OriginalJobID.Value == JobID.Value)
                return false;
            
            return true;
        }
    }
}

