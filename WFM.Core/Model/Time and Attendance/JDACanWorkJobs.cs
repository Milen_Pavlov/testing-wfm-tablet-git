﻿using System;

namespace WFM.Core
{
    public class JDACanWorkJobs
    {
        public string Name { get ; set; }
        public int ID { get; set; }
        public bool IsPrimary { get; set;}

        public JDACanWorkJobs ()
        {
            
        }
    }
}

