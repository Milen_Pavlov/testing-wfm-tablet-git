﻿using System;

namespace WFM.Core
{
    public class JDAUnpairedPunch
    {
        public int EmployeeID { get; set; }

        public string UnpairedPunchCodes { get; set; }

        public DateTime? Start { get; set; }

        public int PunchedShiftID { get; set; }

        public string JobNames { get; set; }

        public JDAUnpairedPunch ()
        {
        }
    }
}

