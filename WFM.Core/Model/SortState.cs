﻿using System;

namespace WFM.Core
{
    public class SortState
    {
        public bool IsSorting { get; set; }
        public bool IsAscending { get; set; }
    }
}