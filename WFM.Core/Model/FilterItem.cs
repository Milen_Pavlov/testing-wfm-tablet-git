﻿using System;

namespace WFM.Core
{
    public class FilterItem
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public bool Selected { get; set; }

        public FilterItem Clone()
        {
            return new FilterItem () {
                Name = string.Format("{0}", this.Name),
                ID = this.ID,
                Selected = this.Selected
            };
        }
    }
}

