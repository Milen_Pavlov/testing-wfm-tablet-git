﻿using System;

namespace WFM.Core
{
    public class JDAConfig
    {
        //Const were interested in
        public static readonly string FORMAT_STRING = "{0}-{1}";//Group-Setting
        public static readonly string EnableDepartmentFilteringName = "SiteManager_Settings-enableDepartmentFiltering";
        public static readonly string HideAttributes = "SiteManager_EmployeeProfile-hideAttributes";

        public string Value { get; set; }

        public string Setting { get; set; }

        public string Group { get; set; }

        public int OrgHierarchyID { get; set; }

        public JDAConfig () { }
    }
}

