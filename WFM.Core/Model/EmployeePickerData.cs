﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class EmployeePickerData : MvxNotifyPropertyChanged
    {
        private bool m_picked;
        public bool Picked { get { return m_picked; } set { m_picked = value; RaisePropertyChanged(() => Picked); } }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        private bool m_assigned;
        public bool AlreadyAssigned { get { return m_assigned; } set { m_assigned = value; RaisePropertyChanged(() => AlreadyAssigned); } }

        public EmployeePickerData()
        {
        }

        public EmployeePickerData (EmployeeData employee, bool picked)
        {
            EmployeeId = employee.EmployeeID;
            EmployeeName = employee.Name;
            Picked = picked;
        }
    }
}

