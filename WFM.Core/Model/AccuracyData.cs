﻿using System;
using System.Linq;
using Consortium.Client.Core;
using System.Collections.Generic;

namespace WFM.Core
{
    public class AccuracyData
    {
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        public double[] Scheduled { get; private set; }
        public double[] Demand { get; private set; }

        public double[] DayAccuracy { get; private set; }
        public double[] DayScheduled { get; private set; }
        public double[] DayDemand { get; private set; }

        public double[] Overs { get; private set; }
        public double[] Unders { get; private set; }

        public double WeekScheduled { get; private set; }
        public double WeekDemand { get; private set; }
        public double WeekAccuracy { get; private set; }

        // Planning data
        public List<PlanningData> Planning { get; private set; }
        public double[] PlanningOver { get; private set; }
        public double[] PlanningUnder { get; private set; }

        private int TimeToInterval(DateTime time)
        {
            var diff = DateTimeUtility.Clamp(time, StartDate, EndDate) - StartDate;
            return (int)(diff.TotalMinutes / ScheduleTime.IntervalMinutes);
        }

        public AccuracyData(JDAScheduleAccuracyData data, DateTime startDate, FilterGroup filter)
        {
            StartDate = startDate;
            EndDate = startDate.AddDays (7);
            WeekScheduled = 0;
            WeekDemand = 0;

            DayDemand = new double[7];
            DayScheduled = new double[7];
            DayAccuracy = new double[7];

            PlanningOver = new double[7];
            PlanningUnder = new double[7];

            Scheduled = new double[ScheduleTime.WeekIntervals];
            Demand = new double[ScheduleTime.WeekIntervals];

            double[] dayOver = new double[7];
            double[] dayUnder = new double[7];
            double[] dayCovered = new double[7];

            // Create one planning data per workgroup
            Planning = new List<PlanningData> ();
            var roleToPlan = new Dictionary<int, PlanningData> ();

            foreach (var wg in data.Workgroups)
            {
                var plan = new PlanningData () {
                    ID = wg.WorkgroupID,
                    Name = wg.WorkgroupName,
                    StartDate = startDate
                };

                foreach (var roleID in wg.Roles)
                {
                    if (filter != null)
                    {
                        var jobRoleFilter = filter as IJobRoleFilterGroup;
                        if (jobRoleFilter != null)
                        {
                            if (!jobRoleFilter.ApplyFilter(roleID))
                                continue;
                        }
                    }
                    roleToPlan[roleID] = plan;
                }

                Planning.Add (plan);
            }

            foreach (var role in data.RoleScheduleAccuracyData)
            {
                if (filter != null)
                {
                    // Don't update demand when filtering by job
                    if (filter.Type == FilterData.JOB_FILTER_CODE)
                        continue;
                    
                    var jobRoleFilter = filter as IJobRoleFilterGroup;
                    if (jobRoleFilter != null)
                    {
                        if (!jobRoleFilter.ApplyFilter(role.RoleID))
                            continue;
                    }
                }

                PlanningData plan = null;
                roleToPlan.TryGetValue (role.RoleID, out plan);
                
                var scheduledTmp = new double[ScheduleTime.WeekIntervals];
                var demandTmp = new double[ScheduleTime.WeekIntervals];

                foreach (var supply in role.SupplyData)
                {
                    int startIndex = TimeToInterval (supply.S);
                    int endIndex = TimeToInterval (supply.E);
                    int length = endIndex - startIndex;

                    for (int i = 0; i < length; ++i)
                    {
                        Scheduled [startIndex + i] += supply.V;
                        scheduledTmp [startIndex + i] += supply.V;
                    }
                }

                foreach (var demand in role.DemandData)
                {
                    int startIndex = TimeToInterval (demand.S);
                    int endIndex = TimeToInterval (demand.E);
                    int length = endIndex - startIndex;

                    for (int i = 0; i < length; ++i)
                    {
                        Demand [startIndex + i] += demand.V;
                        demandTmp [startIndex + i] += demand.V;
                    }
                }

                for (int i = 0; i < 7; ++i)
                {
                    for (int j = 0; j < ScheduleTime.DayIntervals; ++j)
                    {
                        int index = (i * ScheduleTime.DayIntervals) + j;
                        double scheduled = scheduledTmp [index];
                        double demand = demandTmp [index];
                        double covered = Math.Min (demand, scheduled);

                        dayCovered [i] += covered;

                        plan.DayCoverage[i] += covered;
                        plan.DayDemand[i] += demand;
                        plan.DayScheduled[i] += (demand - covered);

                        if (scheduled > covered) // OVER
                        {
                            if(plan != null)
                                plan.RawDayOver[i] += scheduled - covered;

                            dayOver [i] += scheduled - covered;

                        }
                        else if (demand > covered) // UNDER
                        {
                            if(plan != null)
                                plan.RawDayUnder[i] += demand - covered;

                            dayUnder [i] += demand - covered;
                        }

                        // Add variance to the plan
                        if (plan != null)
                            plan.Variance[index] = scheduled - demand;
                    }
                }

                if (plan != null)
                    plan.Smooth (0, 10);
            }

            // Add up the totals
            double weekOver = 0;
            double weekUnder = 0;
            double weekCovered = 0;

            for (int i=0; i<7; ++i)
            {   
                for(int j=0; j<ScheduleTime.DayIntervals; ++j)
                {
                    int index = (i*ScheduleTime.DayIntervals) + j;
                    DayDemand[i] += Demand[index] / ScheduleTime.HourIntervals;
                    DayScheduled[i] += Scheduled[index] / ScheduleTime.HourIntervals; //< Note we dont use this one currently
                }

                // Planning data day totals
                foreach (var plan in Planning)
                {
                    PlanningOver [i] += plan.DayOver [i];
                    PlanningUnder [i] += plan.DayUnder [i];
                }

                WeekScheduled += DayScheduled [i];
                WeekDemand += DayDemand [i];

                weekOver += dayOver[i];
                weekUnder += dayUnder[i];
                weekCovered += dayCovered[i];

                double divide = dayCovered[i] + dayOver[i] + dayUnder[i];
                DayAccuracy[i] = divide > 0.001 ? Math.Round((dayCovered[i] / divide) * 100.0) : dayCovered[i] == 0 ? 100.0 : 0;
            }

            // Schedule Accuracy
            double divideBy = weekCovered + weekOver + weekUnder;
            WeekAccuracy = divideBy > 0.001 ? Math.Round((weekCovered / divideBy) * 100.0) : weekCovered == 0 ? 100.0 : 0;
        }

        public void SetDay(int? dayIndex)
        {
            foreach (var plan in Planning)
                plan.DayIndex = dayIndex;
        }

        public double GetScheduledInterval(int interval)
        {
            return Scheduled[interval];
        }

        public double GetDemandInterval(int interval)
        {
            return Demand[interval];
        }

        public double GetScheduledInterval(int day, int interval)
        {
            return Scheduled[(day*ScheduleTime.DayIntervals)+interval];
        }

        public double GetDemandInterval(int day, int interval)
        {
            return Demand[(day*ScheduleTime.DayIntervals)+interval];
        }

        public DateTime GetTime(int interval)
        {
            return StartDate.AddMinutes (interval * ScheduleTime.IntervalMinutes);
        }

        public DateTime GetTime(int day, int interval)
        {
            return StartDate.AddDays(day).AddMinutes (interval * ScheduleTime.IntervalMinutes);
        }
    }
}

