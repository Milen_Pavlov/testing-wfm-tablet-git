﻿using System;

namespace WFM.Core.WFMPlus
{
	public class ResetPasswordResult
	{
		public bool Success { get; set; }
		public string Message { get; set; }

        static public ResetPasswordResult OperationComplete(bool success)
		{
            return new ResetPasswordResult () { Success = success };
		}
	}
}

