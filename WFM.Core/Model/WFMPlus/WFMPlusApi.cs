﻿using System;
using Siesta;

namespace WFM.Core.WFMPlus
{
    [RestRoute("/wfmauth/sendresetpasswordemail", "POST")]
    public class ResetPasswordRequest
    {
        public string Username { get; set; }
    }

    public class ResetPasswordResponse
    {
    }
}
