﻿using System;

namespace WFM.Core
{
    public class GetAppResponse
    {
        public AppModel App { get; set; }
        public string DownloadLink { get; set; }
        public string DownloadUrl { get; set; }
        public string IconLink { get; set; }
    }

    public class AppModel
    {
        public string BundleId { get; set; }
        public string DeepLink { get; set; }
        public string Description { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Platform { get; set; }
        public string Version { get; set; }
    }
}