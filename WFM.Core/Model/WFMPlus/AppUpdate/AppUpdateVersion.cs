﻿using System;

namespace WFM.Core
{
    public class AppUpdateVersion
    {
        public string Version {get; set; }
        public string URL { get; set; }
    }
}