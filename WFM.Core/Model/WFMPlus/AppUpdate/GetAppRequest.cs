﻿using System;
using Siesta;
using Consortium.Model.Engine;

namespace WFM.Core
{
    [ConsortiumRoute("/app/latest/{AppId}/{Platform}", "GET")]
    public class GetAppRequest
    {
        public string AppId { get; set; }
        public string Platform { get; set; }
    }
}