﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class JobData
    {
        private static int s_uniqueID = 0;

        public int ScheduledJobID { get; set; }
        public string PunchCode { get; set; }
        
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        
        public string StartTimeFormatted
        {
            get
            {
                return m_timeFormatService.FormatTime(Start);
            }
        }
        
        public string EndTimeFormatted
        {
            get
            {
                return m_timeFormatService.FormatTime(End);
            }
        }
        
        public string JobColor { get; set; }
        public int JobID { get; set; }
        public string JobName { get; set; }
        public List<JDAShiftDetail> Roles { get; set; }
        public bool AddedWithoutSaving { get; set; }
        public bool IsTimeOff { get; set; }

        public decimal? DefaultShiftLength { get; set; }
        public int DepartmentID { get; set; }
        public int? PrimaryRoleID { get; set; }
        public int WorkgroupID { get; set; }

        public double Duration { get { return (End - Start).TotalMinutes; } }

        public DateTime OriginalStart { get; set; }
        public DateTime OriginalEnd { get; set; }
        public int OriginalJobID { get; set; }

        private EditState m_edit;
        public EditState Edit 
        { 
            get { return m_edit; }
            set 
            { 
                if(value == EditState.Added)
                {
                    AddedWithoutSaving = true;
                }

                m_edit = value;
            }
        }

        public string StartEnd
        {
            get { return string.Format("{0} - {1}", StartTimeFormatted, EndTimeFormatted); }
        }
        
        private readonly ITimeFormatService m_timeFormatService;

        public JobData()
        {
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
            Roles = new List<JDAShiftDetail> ();
            AddedWithoutSaving = false;
        }

        public JobData(JDAJob job)
        {
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
            JobID = job.JobID;
            JobName = job.JobName;
            JobColor = job.JobColor;
            AddedWithoutSaving = false;
            DefaultShiftLength = job.DefaultShiftLength;
            IsTimeOff = false;

            DepartmentID = job.DepartmentID;
            PrimaryRoleID = job.PrimaryRoleID;
            WorkgroupID = job.WorkgroupID;
        }

        public static int AssignUniqueID()
        {
            return --s_uniqueID;
        }

        public JobData Copy()
        {
            return Mapper.Map<JobData, JobData>(this);
        }

        public static JobData Create()
        {
            return new JobData()
            {
                ScheduledJobID = AssignUniqueID(),
                Edit = EditState.Added,
                IsTimeOff = false
            };
        }

        public static JobData Create(JDAJob job)
        {
            return new JobData () {
                ScheduledJobID = AssignUniqueID(),
                JobColor = job.JobColor,
                JobID = job.JobID,
                JobName = job.JobName,
                Edit = EditState.Added,
                DepartmentID = job.DepartmentID,
                WorkgroupID = job.WorkgroupID,
                PrimaryRoleID = job.PrimaryRoleID,
                IsTimeOff = false
            };
        }

        public static JobData Create(JDAJobAssignment job)
        {
            return new JobData()
            {
                ScheduledJobID = job.ScheduledJobID,
                PunchCode = job.PunchCode,
                Start = job.Start,
                End = job.End,
                JobColor = job.JobColor,
                JobID = job.JobID,
                JobName = job.JobName,
                OriginalStart = job.Start,
                OriginalEnd = job.End,
                OriginalJobID = job.JobID,
                Edit = EditState.Modified,
                IsTimeOff = false
            };
        }

        public void Move(TimeSpan offset)
        {
            if (offset.TotalMinutes == 0)
                return;

            if (Edit != EditState.Added)
                Edit = EditState.Modified;

            Start = Start.Add(offset);
            End = End.Add(offset);
        }

        public void Resize(DateTime start, DateTime end)
        {
            // Nothing to do?
            if (start == Start && end == End)
                return;
                
            if (Edit != EditState.Added)
                Edit = EditState.Modified;

            Start = start;
            End = end;
        }

        public void SetOriginalValues()
        {
            OriginalStart = Start;
            OriginalEnd = End;
            OriginalJobID = JobID;
        }
    }
}

