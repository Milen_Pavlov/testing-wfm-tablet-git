﻿using System;

namespace WFM.Core
{
    public class ReportItem
    {
        public string Time { get; set; }
        public double Scheduled { get; set; }
        public double Demand { get; set; }
        public double Variance { get; set; }
    }
}