﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class LabourDemandDepartment : MvxNotifyPropertyChanged
    {
        private string m_id;
        public string ID 
        { 
            get { return m_id; }
            set { m_id = value; RaisePropertyChanged (() => ID); }
        }

        private List<LabourDemandDepartment> m_children;
        public List<LabourDemandDepartment> Children 
        { 
            get { return m_children; }
            set { m_children = value; RaisePropertyChanged (() => Children); }
        }

        private bool m_isLead;
        public bool IsLead
        {
            get { return m_isLead; }
            set { m_isLead = value; RaisePropertyChanged (() => IsLead); }
        }

        private string m_leadID;
        public string LeadID 
        { 
            get { return m_leadID; }
            set { m_leadID = value; RaisePropertyChanged (() => LeadID); }
        }

        private string m_name;
        public string Name 
        { 
            get { return string.IsNullOrEmpty(m_name) ? ID : m_name; }
            set { m_name = value; RaisePropertyChanged (() => Name); }
        }

        private bool m_isSelected;
        public bool IsSelected
        {
            get { return m_isSelected; }
            set { m_isSelected = value; RaisePropertyChanged (() => IsSelected); }
        }

        public LabourDemandDepartment ()
        {
            Children = new List<LabourDemandDepartment> ();
        }
    }
}

