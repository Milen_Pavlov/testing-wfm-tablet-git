﻿using System;

namespace WFM.Core
{
    public class ShiftSwapDetails
    {
        public JDAAwaitingSwapApproval Swapper { get; set; }

        public JDAAwaitingSwapApproval Swappee { get; set; }

        public bool IsPending { get; set; }

        public int SwappeeShiftID { get; set; }

        public int SwapID { get; set; }

        public ShiftSwapDetails (JDAAwaitingSwapApproval swapper, JDAAwaitingSwapApproval swappee)
        {
            Swapper = swapper;
            Swappee = swappee;
        }
    }
}

