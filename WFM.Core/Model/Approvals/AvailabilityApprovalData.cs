﻿using System;

namespace WFM.Core
{
    public class AvailabilityApprovalData : IApprovalData
    {
        private DateTime m_startDate;
        public DateTime StartDate
        {
            get { return m_startDate; }
            private set { m_startDate = value; }
        }

        private string m_names;
        public string Names
        {
            get { return m_names; }
            private set { m_names = value; }
        }

        private string m_typeString;
        public string TypeString
        {
            get { return m_typeString; }
            private set { m_typeString = value; }
        }

        private ApprovalStatus m_status;
        public ApprovalStatus Status
        {
            get { return m_status; }
            private set { m_status = value; }
        }

        public AvailabilityApprovalData (JDAAvailabilityApproval approval)
        {
            StartDate = approval.RequestedOn;
            Names = approval.Employee.FullName;
            TypeString = "Availability";
            Status = approval.GetStatus ();
        }
    }
}

