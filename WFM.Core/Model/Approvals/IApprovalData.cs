﻿using System;

namespace WFM.Core
{
    public interface IApprovalData
    {
        DateTime StartDate { get; }
        string Names { get; }
        string TypeString { get; }
        ApprovalStatus Status { get; }
    }
}

