﻿using System;

namespace WFM.Core
{
    public class ShiftSwapApprovalData : IApprovalData
    {
        private DateTime m_startDate;
        public DateTime StartDate
        {
            get { return m_startDate; }
            private set { m_startDate = value; }
        }

        private string m_names;
        public string Names
        {
            get { return m_names; }
            private set { m_names = value; }
        }

        private string m_typeString;
        public string TypeString
        {
            get { return m_typeString; }
            private set { m_typeString = value; }
        }

        private ApprovalStatus m_status;
        public ApprovalStatus Status
        {
            get { return m_status; }
            private set { m_status = value; }
        }

        public string SwapID { get; set; }
        public string SwappeeShiftID { get; set; }
        public bool IsPending { get; set; }

        public ShiftSwapApprovalData (JDASwapShiftApproval approval, bool isPending)
        {
            StartDate = approval.RequestedFor;
            Names = string.Format ("{0}; {1}", approval.Swapper.FullName, approval.Swappee.FullName);
            TypeString = "Swap Shift";
            Status = approval.GetStatus ();

            //Approval ID is "SwapID-ShiftID"
            string[] ids = approval.ID.Split('-');
            SwapID = ids[0];

            SwappeeShiftID = approval.SwappeeScheduledShiftID;

            IsPending = isPending;
        }
    }
}

