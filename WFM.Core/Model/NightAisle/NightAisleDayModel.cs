﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core
{
    public class NightAisleDayModel : MvxNotifyPropertyChanged
    {
        public DayOfWeek Day { get; set; }
        public int[] EmployeeIds { get; set; }//TODO Change to cut down employee data object

        private string[] m_employees;
        public string[] Employees 
        {
            get { return m_employees; }
            set
            {
                m_employees = value;
                RaisePropertyChanged (() => Employees);
            }
        }

        private bool m_isEditable;
        public bool IsEditable 
        { 
            get { return m_isEditable; } 
            set
            {

                m_isEditable = value;
                RaisePropertyChanged (() => IsEditable);
            }
        }

        public NightAisleDayModel() { }
    }
}

