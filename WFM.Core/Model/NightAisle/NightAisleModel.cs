﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class NightAisleModel
    {
        public int AisleID { get; set; }
        public string AisleName { get; set; }
        public List<NightAisleDayModel> Days { get; set; }

        public NightAisleModel() { }
    }
}

