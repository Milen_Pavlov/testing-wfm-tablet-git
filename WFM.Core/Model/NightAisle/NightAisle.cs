﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core.Models;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.Core.NightAisle
{
    public class Aisle : MvxNotifyPropertyChanged
    {
        public int ID { get; set; }
        private string m_name;
        public string Name { get { return m_name; } set { m_name = value; RaisePropertyChanged (() => Name); } }
        public int? Priority { get; set; }

        private bool m_isDraggable;
        public bool IsDraggable { get { return m_isDraggable; } set { m_isDraggable = value; RaisePropertyChanged(() => IsDraggable); } }

        public void AisleNameChanged(string value)
        {
            Name = value;
        }
    }

    public class AisleMapping
    {
        public int AisleID { get; set; }
        public IDictionary<DayOfWeek,int[]> EmployeeIdDayMappings;
        public IDictionary<DayOfWeek,string[]> EmployeeDayMappings;
    }
}

