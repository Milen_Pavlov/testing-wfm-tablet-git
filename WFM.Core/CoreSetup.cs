﻿using System;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using AutoMapper;
using WFM.Core.RestClients;
using System.Diagnostics;
using WFM.Shared.DataTransferObjects.Breaks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace WFM.Core
{
    public class JDAProfile : Profile
    {
        protected override void Configure ()
        {
            Mapper.CreateMap<JDAScheduleConstraints,JDAScheduleConstraints>();
            Mapper.CreateMap<JDAEmployeeAttribute,JDAEmployeeAttribute>();
            Mapper.CreateMap<JDAWarning,JDAWarning>();
            Mapper.CreateMap<JDAEmployeeAssignedJob,JDAEmployeeAssignedJob>();

            Mapper.CreateMap<JDAWarning, WarningData> ();
            Mapper.CreateMap<JDAShift, ShiftData> ();
            Mapper.CreateMap<JDAJobAssignment, JobData> ();
            Mapper.CreateMap<JDAShiftDetail, ShiftDetailData> ();
        }
    }

    public class DeepCopyProfile : Profile
    {
        protected override void Configure ()
        {
            Mapper.CreateMap<EmployeeData,EmployeeData>();
            Mapper.CreateMap<EmployeeDayData,EmployeeDayData>();
            Mapper.CreateMap<DayInfo,DayInfo>();
            Mapper.CreateMap<ShiftData,ShiftData>();
            Mapper.CreateMap<JobData,JobData>();
            Mapper.CreateMap<ShiftDetailData,ShiftDetailData> ();
            Mapper.CreateMap<BreakRecord,BreakRecord>();
            Mapper.CreateMap<OvertimeRecord,OvertimeRecord>();
        }
    }

    public class CoreSetup : ICoreSetup
    {
        public void Initialise()
        {
            // JDA Mappings for deep copy
            Mapper.Initialize (cfg =>
                {
                    cfg.AddProfile<JDAProfile>();
                    cfg.AddProfile<DeepCopyProfile>();
                    cfg.AddProfile<MyHoursProfile>();
                });

            Mvx.RegisterSingleton<ScheduleBreaksService>(() => Mvx.IocConstruct<ScheduleBreaksService>());
            Mvx.LazyConstructAndRegisterSingleton<ICallOffService,CallOffService>();
            Mvx.LazyConstructAndRegisterSingleton<ISalesForecastService, SalesForecastService> ();
            Mvx.RegisterType<ILabourDemandService, LabourDemandService>();
            Mvx.RegisterType<ISettingsService, SettingsService>();
            Mvx.ConstructAndRegisterSingleton<IJDAConfigurationService, JDAConfigurationService>();
            Mvx.ConstructAndRegisterSingleton<IESSConfigurationService, ESSConfigurationService>();
            Mvx.RegisterType<IRolesService, RolesService>();
            Mvx.RegisterType<ITimeOffRequestBlackoutOverrideService, TimeOffRequestBlackoutOverrideService>();
         }
    }
}