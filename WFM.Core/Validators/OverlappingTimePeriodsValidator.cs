﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Validators
{
    public class OverlappingTimePeriodsValidator : Validator
    {
        public Func<IList<TimePeriod>> TimePeriodsToCompare { get; set; }

        public new Func<TimePeriod> FieldValue
        {
            get
            {
                return (Func<TimePeriod>)base.FieldValue;
            }
        }

        public OverlappingTimePeriodsValidator(string fieldName, Func<TimePeriod> fieldValue, Func<IList<TimePeriod>> timePeriodsToCompare, string errorMessageTemplate = "This {0} overlaps another {0}")
            : base(fieldName, fieldValue, errorMessageTemplate)
        {
            TimePeriodsToCompare = timePeriodsToCompare;
        }

        public override void Validate()
        {
            var timePeriod = FieldValue.Invoke();
            var timePeriodsToCompare = TimePeriodsToCompare.Invoke();

            if (timePeriod == null || timePeriodsToCompare == null || timePeriod.IsNull)
            {
                // Values must not be null. Use a RequiredValidator to enforce this.
                IsValid = true;
                return;
            }
            else if (timePeriod.StartTime > timePeriod.EndTime)
            {
                // StartTime must be greater than (or equal to) EndTime. Use a ComparisonValidator to enforce this.
                IsValid = true;
                return;
            }

            foreach (var timePeriodToCompare in timePeriodsToCompare)
            {
                if (timePeriodToCompare.StartTime > timePeriodToCompare.EndTime)
                {
                    // StartTime must be greater than (or equal to) EndTime. Use a ComparisonValidator to enforce this.
                    IsValid = true;
                    return;
                }

                var overlap = (!timePeriodToCompare.IsNull && timePeriod.StartTime < timePeriodToCompare.EndTime && timePeriod.EndTime > timePeriodToCompare.StartTime);
                if (overlap)
                {
                    IsValid = false;
                    return;
                }
            }

            IsValid = true;
        }

        public class TimePeriod
        {
            public bool IsNull { get; set; }
            public TimeSpan StartTime { get; set; }
            public TimeSpan EndTime { get; set; }

            public TimePeriod(TimeSpan? startTime, TimeSpan? endTime)
            {
                if (startTime == null || endTime == null)
                {
                    IsNull = true;
                }
                else
                {
                    IsNull = false;
                    StartTime = startTime.Value;
                    EndTime = endTime.Value;
                }
            }
        }
    }
}
