﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Validators
{
    public class ComparisonValidator : Validator
    {
        public string FieldNameToCompare { get; set; }
        public Func<object> FieldValueToCompare { get; set; }
        public ComparisonOperator Operator { get; set; }

        public ComparisonValidator(string fieldName, Func<object> fieldValue, string fieldNameToCompare, Func<object> fieldValueToCompare, ComparisonOperator @operator, string errorMessageTemplate = "{0} must {2} {1}")
            : base(fieldName, fieldValue, errorMessageTemplate)
        {
            FieldNameToCompare = fieldNameToCompare;
            FieldValueToCompare = fieldValueToCompare;
            Operator = @operator;
        }

        public override void Validate()
        {
            var value = FieldValue.Invoke();
            var valueToCompare = FieldValueToCompare.Invoke();

            if (value == null || valueToCompare == null)
            {
                // Values must not be null. Use a RequiredValidator to enforce this.
                IsValid = true;
                return;
            }

            var isValueComparible = value is IComparable;
            var isValueToCompareComparible = valueToCompare is IComparable;
            if (!isValueComparible || !isValueToCompareComparible)
            {
                throw new NotSupportedException();
            }

            var compareResult = (value as IComparable).CompareTo(valueToCompare);
            switch (Operator)
            {
                case ComparisonOperator.Equals:
                {
                    IsValid = (compareResult == 0);
                    break;
                }
                case ComparisonOperator.NotEquals:
                {
                    IsValid = (compareResult != 0);
                    break;
                }
                case ComparisonOperator.LessThan:
                {
                    IsValid = (compareResult < 0);
                    break;
                }
                case ComparisonOperator.LessThanEquals:
                {
                    IsValid = (compareResult <= 0);
                    break;
                }
                case ComparisonOperator.GreaterThan:
                {
                    IsValid = (compareResult > 0);
                    break;
                }
                case ComparisonOperator.GreaterThanEquals:
                {
                    IsValid = (compareResult >= 0);
                    break;
                }
                default:
                {
                    throw new NotSupportedException();
                }
            }
        }

        public override string FormattedErrorMessage
        {
            get
            {
                return string.Format(ErrorMessageTemplate, FieldName, FieldNameToCompare, FriendlyOperator);
            }
        }

        public string FriendlyOperator
        {
            get
            {
                switch (Operator)
                {
                    case ComparisonOperator.Equals:
                    {
                        return "be equal to";
                    }
                    case ComparisonOperator.NotEquals:
                    {
                        return "not be equal to";
                    }
                    case ComparisonOperator.LessThan:
                    {
                        return "be less than";
                    }
                    case ComparisonOperator.LessThanEquals:
                    {
                        return "be less than (or equal to)";
                    }
                    case ComparisonOperator.GreaterThan:
                    {
                        return "be greater than";
                    }
                    case ComparisonOperator.GreaterThanEquals:
                    {
                        return "be greater than (or equal to)";
                    }
                    default:
                    {
                        throw new NotSupportedException();
                    }
                }
            }
        }

        public enum ComparisonOperator
        {
            Equals,
            NotEquals,
            LessThan,
            LessThanEquals,
            GreaterThan,
            GreaterThanEquals,
        }
    }
}
