﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Validators
{
    public class RequiredValidator : Validator
    {
        public bool AllowEmptyStrings { get; set; }

        public RequiredValidator(string fieldName, Func<object> fieldValue, string errorMessageTemplate = "{0} is required", bool allowEmptyStrings = false)
            : base(fieldName, fieldValue, errorMessageTemplate)
        {
            AllowEmptyStrings = allowEmptyStrings;
        }

        public override void Validate()
        {
            var value = FieldValue.Invoke();
            if (value == null)
            {
                IsValid = false;
                return;
            }

            var valueIsString = value is string;
            if (!valueIsString)
            {
                IsValid = true;
                return;
            }

            var valueAsString = (string)value;
            IsValid = (AllowEmptyStrings ? !string.IsNullOrEmpty(valueAsString) : !string.IsNullOrWhiteSpace(valueAsString));
        }
    }
}
