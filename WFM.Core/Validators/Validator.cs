﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.Validators
{
    public abstract class Validator
    {
        public bool IsValid { get; set; }
        public string FieldName { get; set; }
        public Func<object> FieldValue { get; set; }
        public string ErrorMessageTemplate { get; set; }

        public Validator(string fieldName, Func<object> fieldValue, string errorMessageTemplate = "{0} is not valid")
        {
            FieldName = fieldName;
            FieldValue = fieldValue;
            ErrorMessageTemplate = errorMessageTemplate;
            IsValid = true;
        }

        public abstract void Validate();

        public virtual string FormattedErrorMessage
        {
            get
            {
                return string.Format(ErrorMessageTemplate, FieldName);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return (IsValid ? null : FormattedErrorMessage);
            }
        }
    }
}
