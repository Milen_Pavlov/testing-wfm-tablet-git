﻿using System;

namespace WFM.Core
{
    public interface IScheduleLoadingService
    {
        bool IsShowingLoadingSpinner { get; }
        void ShowLoadingSpinner(string message);
        void HideLoadingSpinner();
    }
}