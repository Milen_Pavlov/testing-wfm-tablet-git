using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WFM.Core
{
    public interface IDailyOverviewFilterService
    {
        List<FilterGroup> CurrentFilters { get; set; }
        string CurrentView { set; }
        Action OnFiltersChanged { get; set; }

        List<SiteTimecardEntry> FilterTimecards(List<SiteTimecardEntry> timecards);
        Task GenerateFilterGroups(int siteID);
        void ApplyFilter(FilterGroup filterToApply);
        void ClearFilter();
        void ClearAllFilters();
    }
}

