using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class ESSConfigurationService : IESSConfigurationService
    {
        private readonly SettingsService m_configService;
        private readonly IJDAConfigurationService m_jdaConfigService;

        public ESSConfigurationService(SettingsService configService, IJDAConfigurationService jdaConfigService)
        {
            m_configService = configService;
            m_jdaConfigService = jdaConfigService;
        }

        public T GetValue<T>(ESSConfigurationItem<T> item)
        {
            if (m_configService.AppConfig.HasValue(item.AppConfig))
            {
                var result = m_configService.AppConfig.GetObject<T>(item.AppConfig);
                return result;
            }

            if(m_jdaConfigService.JDAConfiguration.HasValue(item.JDAConfig))
            {
                var jdaResult = m_jdaConfigService.JDAConfiguration.GetObject<T>(item.JDAConfig);
                return jdaResult;
            }

            return item.Default;
        }

        public T GetEnvironmentValue<T>(ESSConfigurationItem<T> item)
        {
            if (m_configService.Current.Env.Configuration.HasValue(item.AppConfig))
            {
                var result = m_configService.Current.Env.Configuration.GetObject<T>(item.AppConfig);
                return result;
            }

            if(m_jdaConfigService.JDAConfiguration.HasValue(item.JDAConfig))
            {
                var jdaResult = m_jdaConfigService.JDAConfiguration.GetObject<T>(item.JDAConfig);
                return jdaResult;
            }

            return item.Default;
        }

        public string GetValue(ESSConfigurationItem<string> item)
        {
            if (m_configService.AppConfig.HasValue(item.AppConfig))
            {
                var result = m_configService.AppConfig.GetString(item.AppConfig);
                return result;
            }

            if(m_jdaConfigService.JDAConfiguration.HasValue(item.JDAConfig))
            {
                var jdaResult = m_jdaConfigService.JDAConfiguration.GetString(item.JDAConfig);
                return jdaResult;
            }

            return item.Default;
        }

        public string GetEnvironmentValue(ESSConfigurationItem<string> item)
        {
            if (m_configService.Current.Env.Configuration.HasValue(item.AppConfig))
            {
                var result = m_configService.Current.Env.Configuration.GetString(item.AppConfig);
                return result;
            }

            if(m_jdaConfigService.JDAConfiguration.HasValue(item.JDAConfig))
            {
                var jdaResult = m_jdaConfigService.JDAConfiguration.GetString(item.JDAConfig);
                return jdaResult;
            }

            return item.Default;
        }

        #region JDACustomSettings
        public const string IsScheduleEditorReadOnlyForUserRoleName = "SiteManager_Scheduling-WM_isScheduleEditorReadOnly";
        public bool IsScheduleEditorReadOnlyForUserRole
        {
            get 
            {
                return GetValue (new ESSConfigurationItem<bool> (string.Empty, false, IsScheduleEditorReadOnlyForUserRoleName));
            }
        }

        #endregion

        private const string WeekNumberDisplayedName = "WeekNumberDisplayed";
        public bool WeekNumberDisplayed {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(WeekNumberDisplayedName, false));
            }
        }

        private const string ShowFillUnfilledShiftsName = "ShowFillUnfilledShifts";
        public bool ShowFillUnfilledShifts 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowFillUnfilledShiftsName, true));
            }
        }

        private const string CanEditScheduleName = "CanEditSchedule";
        public bool CanEditSchedule {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanEditScheduleName, true));
            }
        }

        private const string AlternativeScheduleViewName = "AlternativeScheduleView";
        public bool AlternativeScheduleView 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(AlternativeScheduleViewName, false));
            }
        }

        private const string TimeOffTypesOverridingBlackoutPeriodName = "TimeOffTypesOverridingBlackoutPeriod";
        public string TimeOffTypesOverridingBlackoutPeriod
        {
            get
            {
                return GetValue(new ESSConfigurationItem<string>(TimeOffTypesOverridingBlackoutPeriodName, string.Empty));   
            }
        }

        private const string CanOverrideBlackoutPeriodName = "CanOverrideBlackoutPeriod";
        public bool CanOverrideBlackoutPeriod
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool>(CanOverrideBlackoutPeriodName, false));   
            }
        }

        private const string ShowPlanningTabName = "ShowPlanningTab";
        public bool ShowPlanningTab 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowPlanningTabName, true));
            }
        }

        private const string ShowBreaksTabName = "ShowBreaksTab";
        public bool ShowBreaksTab 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowBreaksTabName, true));
            }
        }

        private const string ShowResourcesTabName = "ShowResourcesTab";
        public bool ShowResourcesTab 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowResourcesTabName, false));
            }
        }

        private const string PreventChangesHoursName = "PreventChangesHours";
        public int PreventChangesHours 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(PreventChangesHoursName, 0));
            }
        }

        private const string ShowScheduleCostName = "ShowScheduleCost";
        public bool ShowScheduleCost 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowScheduleCostName, false));
            }
        }

        private const string RestClientTimeoutSecsName = "RestClientTimeoutSecs";
        public int RestClientTimeoutSecs 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(RestClientTimeoutSecsName, 120));
            }
        }

        private const string ShowApprovalsName = "ShowApprovals";
        private const string ShowApprovalsJDAName = "SiteManager_Scheduling-WM_showApprovals";
        public bool ShowApprovals 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowApprovalsName, false, ShowApprovalsJDAName));
            }
        }

        private const string ShowTImeOffMenuName = "ShowTimeOffMenu";
        public bool ShowTimeOffMenu 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowTImeOffMenuName, true));
            }
        }

        private const string ShowMyHoursName = "ShowMyHours";
        public bool ShowMyHours 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowMyHoursName, false));
            }
        }

        private const string ShowScheduleMenuName = "ShowScheduleMenu";
        public bool ShowScheduleMenu 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowScheduleMenuName, true));
            }
        }


        private const string ShowTillAllocationMenuName = "ShowTillAllocationMenu";
        public bool ShowTillAllocationMenu 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowTillAllocationMenuName, true));
            }
        }

        private const string ShowDailyOverviewMenuName = "ShowDailyOverviewMenu";
        public bool ShowDailyOverviewMenu 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowDailyOverviewMenuName, true));
            }
        }

        private const string ShowNightAislePlannerMenuName = "ShowNightAislePlannerMenu";
        public bool ShowNightAislePlannerMenu 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowNightAislePlannerMenuName, true));
            }
        }

        private const string ShowPlannedScheduleSummaryName = "ShowPlannedScheduleSummary";
        public bool ShowPlannedScheduleSummary 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowPlannedScheduleSummaryName, false));
            }
        }


        private const string CanViewTimeOffTypeDetailsName = "CanViewTimeOffTypeDetails";

        public bool CanViewTimeOffTypeDetails
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanViewTimeOffTypeDetailsName, true));
            }
        }

        private const string ShowSalesForecastName = "ShowSalesForecast";
        public bool ShowSalesForecast 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowSalesForecastName, false));
            }
        }

        private const string ShowOvertimeTrackerName = "ShowOvertimeTracker";
        public bool ShowOvertimeTracker 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowOvertimeTrackerName, true));
            }
        }

        private const string ShowChangePasswordName = "ShowChangePassword";
        public bool ShowChangePassword 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowChangePasswordName, false));
            }
        }

        private const string ShowSiteNumberName = "ShowSiteNumber";
        public bool ShowSiteNumber 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowSiteNumberName, false));
            }
        }

        private const string TimeoutMinutesName = "TimeoutMinutes";
        public int TimeoutMinutes 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(TimeoutMinutesName, 500));
            }
        }


        private const string CanResetPasswordName = "CanResetPassword";
        public bool CanResetPassword 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanResetPasswordName, false));
            }
        }

        private const string CheckForLockoutName = "CheckForLockout";
        public bool CheckForLockout 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CheckForLockoutName, false));
            }
        }

        private const string KeepAliveFreqSecsName = "KeepAliveFreqSecs";
        public double KeepAliveFreqSecs 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<double>(KeepAliveFreqSecsName, 60));
            }
        }

        private const string LockoutBundleOverrideName = "LockoutBundleOverride";
        public string LockoutBundleOverride 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<string>(LockoutBundleOverrideName, string.Empty));
            }
        }

        private const string HideBreakWarningsName = "HideBreakWarnings";
        public bool HideBreakWarnings 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(HideBreakWarningsName, false));
            }
        }

        private const string PeriodFirstDayName = "PeriodFirstDay";
        public int PeriodFirstDay 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(PeriodFirstDayName, 2));
            }
        }

        private const string PeriodFirstMonthName = "PeriodFirstMonth";
        public int PeriodFirstMonth 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(PeriodFirstMonthName, 3));
            }
        }

        private const string StartDayName = "StartDay";
        public int StartDay 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(StartDayName, (int)DayOfWeek.Monday));
            }
        }

        private const string ScheduleTimeoutSecsName = "ScheduleTimeoutSecs";
        public int ScheduleTimeoutSecs 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(ScheduleTimeoutSecsName, 240));
            }
        }

        private const string CanFilterByAttributeName = "CanFilterByAttribute";
        public bool? CanFilterByAttribute 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool?>(CanFilterByAttributeName, null));
            }
        }

        private const string CanFilterByDepartmentName = "CanFilterByDepartment";
        public bool? CanFilterByDepartment 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool?>(CanFilterByDepartmentName, null));
            }
        }

        private const string CanFilterByWorkgroupName = "CanFilterByWorkgroup";
        public bool? CanFilterByWorkgroup 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool?>(CanFilterByWorkgroupName, null));
            }
        }

        private const string CanFilterByJobCustomName = "CanFilterByJobCustom";
        public bool? CanFilterByJobCustom 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool?>(CanFilterByJobCustomName, null));
            }
        }

        private const string CanFilterByRoleCustomName = "CanFilterByRoleCustom";
        public bool? CanFilterByRoleCustom 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool?>(CanFilterByRoleCustomName, null));
            }
        }


        private const string CanFilterByJobClassicName = "CanFilterByJobClassic";
        public bool? CanFilterByJobClassic 
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool?>(CanFilterByJobClassicName, null));
            }
        }

        private const string WorkgroupsAreDepartmentsName = "WorkgroupsAreDepartments";
        public bool WorkgroupsAreDepartments 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(WorkgroupsAreDepartmentsName, false));
            }
        }

        private const string BlockEditingInPastName = "BlockEditingInPast";
        public bool BlockEditingInPast 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(BlockEditingInPastName, false));
            }
        }

        private const string ShiftSwapOnDragAndDropEnabledName = "ShiftSwapOnDragAndDropEnabled";
        public bool ShiftSwapOnDragAndDropEnabled 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShiftSwapOnDragAndDropEnabledName, true));
            }
        }

        private const string DontGenerateWarningsName = "DontGenerateWarnings";
        public bool DontGenerateWarnings 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(DontGenerateWarningsName, false));
            }
        }

        private const string CanEditEnvironmentsName = "CanEditEnvironments";
        public bool CanEditEnvironments 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanEditEnvironmentsName, false));
            }
        }

        private const string ShowSettingsButtonName = "ShowSettingsButton";
        public bool ShowSettingsButton
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowSettingsButtonName, false));
            }
        }

        private const string ShowCombinedSSCSAYSTillName = "ShowCombinedSSCSAYSTill";
        public bool ShowCombinedSSCSAYSTill 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowCombinedSSCSAYSTillName, true));
            }
        }

        private const string HideEmptySSCSAYSTillName = "HideEmptySSCSAYSTill";
        public bool HideEmptySSCSAYSTill 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(HideEmptySSCSAYSTillName, true));
            }
        }

        private const string ShowTillAllocationBufferName = "ShowTillAllocationBuffer";
        public bool ShowTillAllocationBuffer 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowTillAllocationBufferName, false));
            }
        }

        private const string ShowMultipleTillAllocationBuffersName = "ShowMultipleTillAllocationBuffers";
        public bool ShowMultipleTillAllocationBuffers 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowMultipleTillAllocationBuffersName, false));
            }
        }

        private const string CanDeleteShiftsName = "CanDeleteShifts";
        public bool CanDeleteShifts 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanDeleteShiftsName, true));
            }
        }

        private const string ShowUnfillOptionsName = "ShowUnfillOptions";
        public bool ShowUnfillOptions 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowUnfillOptionsName, false));
            }
        }

        private const string ShowScheduleDetailsName = "ShowScheduleDetails";
        public bool ShowScheduleDetails 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowScheduleDetailsName, true));
            }
        }

        private const string CanApproveOwnTimeOffName = "CanApproveOwnTimeOff";
        public bool CanApproveOwnTimeOff 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanApproveOwnTimeOffName, true));
            }
        }

        private const string ShowSpareShiftsName = "ShowSpareShifts";
        public bool ShowSpareShifts 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowSpareShiftsName, false));
            }
        }

        private const string HideDefaultVarianceValuesName = "HideDefaultVarianceValues";
        public bool HideDefaultVarianceValues 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(HideDefaultVarianceValuesName, false));
            }
        }

        private const string CanLockResourcesName = "CanLockResources";
        public bool CanLockResources 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanLockResourcesName, true));
            }
        }

        private const string IsTORReadOnlyName = "SiteManager_Scheduling-isTORReadOnly";
        private const string TimeOffReadOnlyName = "TimeOffReadOnly";
        public bool TimeOffReadOnly 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(TimeOffReadOnlyName, false, IsTORReadOnlyName));
            }
        }

        static public string CanCreateTimeOffName = "CanCreateTimeOff";
        public bool CanCreateTimeOff {
            get {
                return GetValue (new ESSConfigurationItem<bool> (CanCreateTimeOffName, true));
            }
        }

        private const string HideTimeOffTypeName = "HideTimeOffType";
        public bool HideTimeOffType
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool>(HideTimeOffTypeName, false));
            }
        }

        private const string ShowPopoverEmployeeTimeOffViewName = "ShowPopoverEmployeeTimeOffView";
        public bool ShowPopoverEmployeeTimeOffView 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowPopoverEmployeeTimeOffViewName, true));
            }
        }

        private const string ShowAllEmployeeHistoryInPopoverName = "ShowAllEmployeeHistoryInPopover";
        public bool ShowAllEmployeeHistoryInPopover 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowAllEmployeeHistoryInPopoverName, true));
            }
        }

        private const string SSCSAYSConfigurationsName = "SSCSAYSConfigurations";
        public SSCSAYSConfiguration [] SSCSAYSConfigurations 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<SSCSAYSConfiguration[]>(SSCSAYSConfigurationsName, new SSCSAYSConfiguration[0]));
            }
        }

        private const string NightShiftStartTimeName = "NightShiftStartTime";
        public int NightShiftStartTime 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(NightShiftStartTimeName, 20));
            }
        }

        private const string NightShiftDurationName = "NightShiftDuration";
        public int NightShiftDuration 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<int>(NightShiftDurationName, 9));
            }
        }

        private const string NightShiftDepartmentsName = "NightShiftDepartments";
        public string NightShiftDepartments
        {
            get
            {
                return GetValue(new ESSConfigurationItem<string>(NightShiftDepartmentsName, string.Empty));   
            }
        }

        private const string AllowPunchesForMealsName = "AllowPunchesForMeals";
        public bool AllowPunchesForMeals 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(AllowPunchesForMealsName, true));
            }
        }

        private const string AllowPunchesForBreaksName = "AllowPunchesForBreaks";
        public bool AllowPunchesForBreaks 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(AllowPunchesForBreaksName, true));
            }
        }

        static public string AllowPunchesForJobsName = "AllowPunchesForJobs";
        public bool AllowPunchesForJobs {
            get {
                return GetValue (new ESSConfigurationItem<bool> (AllowPunchesForJobsName, false));
            }
        }

        private const string CanEditOwnPunchesName = "CanEditOwnPunches";
        public bool CanEditOwnPunches 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanEditOwnPunchesName, true));
            }
        }

        static public string CanModifyPunchesInFutureName = "CanModifyPunchesInFuture";
        public bool CanModifyPunchesInFuture 
        {
            get 
            {
                return GetValue (new ESSConfigurationItem<bool> (CanModifyPunchesInFutureName, false));
            }
        }

        private const string CallOffShiftTimeOffTypeName = "CallOffShiftTimeOffType";
        public string CallOffShiftTimeOffType 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<string>(CallOffShiftTimeOffTypeName, string.Empty));
            }
        }
        
        private const string CallOffShiftUnpaidTimeOffTypeName = "CallOffShiftUnpaidTimeOffType";
        public string CallOffShiftUnpaidTimeOffType 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<string>(CallOffShiftUnpaidTimeOffTypeName, string.Empty));
            }
        }
        
        private const string CallOffUnpaidAppliesForFixedShiftsOnlyName = "CallOffUnpaidAppliesForFixedShiftsOnly";
        public bool CallOffUnpaidAppliesForFixedShiftsOnly 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CallOffUnpaidAppliesForFixedShiftsOnlyName, false));
            }
        }
        
        private const string CallOffAppliesForFixedShiftsOnlyName = "CallOffAppliesForFixedShiftsOnly";
        public bool CallOffAppliesForFixedShiftsOnly 
        {
            get 
            {
                return GetValue (new ESSConfigurationItem<bool> (CallOffAppliesForFixedShiftsOnlyName, false));
            }
        }
        
        private const string ExcludedUnplannedTimeOffTypeName = "ExcludedUnplannedTimeOffType";
        public string ExcludedUnplannedTimeOffType 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<string>(ExcludedUnplannedTimeOffTypeName, string.Empty));
            }
        }

        private const string CanPrintScheduleName = "CanPrintSchedule";
        public bool CanPrintSchedule 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(CanPrintScheduleName, true));
            }
        }

        private const string ShowMobileNumbersName = "ShowMobileNumbers";
        public bool ShowMobileNumbers 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowMobileNumbersName, true));
            }
        }

        private const string ShowStoreInNavBarName = "ShowStoreInNavBar";
        public bool ShowStoreInNavBar 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowStoreInNavBarName, false));
            }
        }

        private const string ShowCallOffPastName = "ShowCallOffPast";
        public bool ShowCallOffPast
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowCallOffPastName, false));
            }
        }
        
        private const string ShowCallOffCurrentDayName = "ShowCallOffCurrentDay";
        public bool ShowCallOffCurrentDay
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowCallOffCurrentDayName, false));
            }
        }
        
        private const string ShowCallOffFutureName = "ShowCallOffFuture";
        public bool ShowCallOffFuture
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowCallOffFutureName, false));
            }
        }

        private const string ShowCombineDepartmentsName = "ShowCombineDepartments";
        public bool ShowCombineDepartments 
        {
            get 
            {
                return GetValue(new ESSConfigurationItem<bool>(ShowCombineDepartmentsName, false));
            }
        }

        static public string CombineDepartmentsMaxChildDepartmentsName = "CombineDepartmentsMaxChildDepartments";
        public int CombineDepartmentsMaxChildDepartments 
        {
            get
            {
                return GetValue (new ESSConfigurationItem<int> (CombineDepartmentsMaxChildDepartmentsName, -1));
            }
        }

        static public string MaxShiftLengthHoursName = "MaxShiftLengthHours";
        public int MaxShiftLengthHours 
        {
            get 
            {
                return GetValue (new ESSConfigurationItem<int> (MaxShiftLengthHoursName, 24));
            }
        }

        private const string HasTermsOfServiceEnabledName = "HasTermsOfServiceEnabled";
        public bool HasTermsOfServiceEnabled
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool>(HasTermsOfServiceEnabledName, false));
            }
        }

        static public string TotalHoursPerDayDivisorName = "TotalHoursPerDayDivisor";
        public double TotalHoursPerDayDivisor 
        {
            get 
            {
                return GetValue (new ESSConfigurationItem<double> (TotalHoursPerDayDivisorName, 1.0f));
            }
        }

        #region EnvironmentSettings
        private const string AddressName = "Address";
        public string Address
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(AddressName, string.Empty));
            }
        }

        private const string ConsortiumAddressName = "ConsortiumAddress";
        public string ConsortiumAddress 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(ConsortiumAddressName, string.Empty));
            }
        }

        private const string ConsortiumUsernameName = "ConsortiumUsername";
        public string ConsortiumUsername 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(ConsortiumUsernameName, string.Empty));
            }
        }

        private const string ConsortiumPasswordName = "ConsortiumPassword";
        public string ConsortiumPassword 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(ConsortiumPasswordName, string.Empty));
            }
        }

        private const string UseCustomScheduleRequestName = "UseCustomScheduleRequest";
        public bool UseCustomScheduleRequest 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<bool>(UseCustomScheduleRequestName, false));
            }
        }


        private const string SendScheduleAuditName = "SendScheduleAudit";
        public bool SendScheduleAudit 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<bool>(SendScheduleAuditName, false));
            }
        }

        private const string MyHoursAddressName = "MyHoursAddress";
        public string MyHoursAddress 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(MyHoursAddressName, string.Empty));
            }
        }

        private const string MyHoursUsernameName = "MyHoursUsername";
        public string MyHoursUsername 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(MyHoursUsernameName, string.Empty));
            }
        }

        private const string MyHoursPasswordName = "MyHoursPassword";
        public string MyHoursPassword 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(MyHoursPasswordName, string.Empty));
            }
        }

        private const string CombinedDeptAddressName = "CombinedDeptAddress";
        public string CombinedDeptAddress 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(CombinedDeptAddressName, string.Empty));
            }
        }

        private const string CombinedDeptUsernameName = "CombinedDeptUsername";
        public string CombinedDeptUsername 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(CombinedDeptUsernameName, string.Empty));
            }
        }

        private const string CombinedDeptPasswordName = "CombinedDeptPassword";
        public string CombinedDeptPassword 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(CombinedDeptPasswordName, string.Empty));
            }
        }

        private const string LabourDemandAddressName = "LabourDemandAddress";
        public string LabourDemandAddress 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<string>(LabourDemandAddressName, string.Empty));
            }
        }

        private const string SendWeeklyWarningsUpdatesName = "SendWeeklyWarningsUpdates";
        public bool SendWeeklyWarningsUpdates 
        {
            get 
            {
                return GetEnvironmentValue(new ESSConfigurationItem<bool>(SendWeeklyWarningsUpdatesName, true));
            }
        }
        #endregion


        //We had 2 config values being used outside of this ESS Config service, so they are awkwardly put here...
        private const string JDAHideAttributesName = "SiteManager_EmployeeProfile-hideAttributes";
        public bool JDAHideAttributes 
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool>(JDAHideAttributesName, true, JDAHideAttributesName));
            }
        }
        private const string JDAEnableDepartmentFilteringName = "SiteManager_Settings-enableDepartmentFiltering";
        public bool JDAEnableDepartmentFiltering 
        {
            get
            {
                return GetValue(new ESSConfigurationItem<bool>(JDAEnableDepartmentFilteringName, true, JDAEnableDepartmentFilteringName));
            }
        }
    }
}