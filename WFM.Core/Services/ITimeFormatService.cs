﻿using System;

namespace WFM.Core
{
    public interface ITimeFormatService
    {
        string TimePickerLocale { get; }
        string FormatTime(DateTime dateToFormat);
        string FormatTimeForRuler(DateTime time);
    }
}