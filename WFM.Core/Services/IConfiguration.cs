﻿using System.Collections.Generic;

namespace WFM.Core
{
    public interface IConfiguration
    {
        Dictionary<string, string> Properties { get; }

        void ClearAll();
        void Append(IConfiguration configuration);
        void Append(IDictionary<string,string> properties);

        bool HasValue(string key);

        string GetString(string key);
        string GetString(string key, string defaultValue);
        void SetString(string key, string value);

        int GetInt(string key);
        int GetInt(string key, int defaultValue);
        void SetInt(string key, int value);

        bool GetBoolean(string key);
        bool GetBoolean(string key, bool defaultValue);
        void SetBoolean(string key, bool value);

        float GetFloat(string key);
        float GetFloat(string key, float defaultValue);
        void SetFloat(string key, float value);

        double GetDouble(string key);
        double GetDouble(string key, double defaultValue);
        void SetDouble(string key, double value);

        decimal GetDecimal(string key);
        decimal GetDecimal(string key, decimal defaultValue);
        void SetDecimal(string key, decimal value);

        T GetObject<T>(string key);
        T GetObject<T>(string key, T defaultValue);
        void SetObject<T>(string key, T value);

        IDictionary<string, string> GetList(List<string> keys);
    }
}