﻿using System;
using System.Collections.Generic;
using System.Text;
using Consortium.Client.Core;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class ConfigurationStore : IConfiguration, IBindingValueContainer
    {
        public Dictionary<string, string> Properties { get; private set; }
        public bool LogChanges { get; set; }

        // Case insensitive list of key names that we should redact the value out for when logging 
        private readonly string[] c_redactedConfigNames = new string[] { "password" };
        private readonly ILogger m_logger;

        public ConfigurationStore()
        {
            Properties = new Dictionary<string, string>();
            LogChanges = false;
        }

        public ConfigurationStore(IConfiguration configuration, ILogger logger)
        {
            m_logger = logger;
            Properties = new Dictionary<string, string>();
            Append(configuration);
        }

        public void ClearAll()
        {
            if (Properties != null)
                Properties.Clear();

            if (LogChanges)
                m_logger.Information("Configuration Cleared");
        }

        public bool HasValue(string key)
        {
            if(string.IsNullOrEmpty(key))
            {
                return false;
            }

            return Properties != null && Properties.ContainsKey(key);
        }

        public void Append(IConfiguration configuration)
        {
            if (configuration != null)
                Append(configuration.Properties);
        }

        public void Append(IDictionary<string,string> properties)
        {
            if (properties == null)
                return;

            if (LogChanges)
            {
                StringBuilder sb = new StringBuilder();

                foreach (KeyValuePair<string, string> kvp in properties)
                {
                    if (Properties.ContainsKey(kvp.Key))
                    {
                        Properties[kvp.Key] = kvp.Value;

                        if (IsRedacted(kvp.Key))
                        {
                            sb.Append("Update:" + kvp.Key + "=[REDACTED]" + " ");
                        }
                        else
                        {
                            sb.Append("Update:" + kvp.Key + "=" + kvp.Value + " ");
                        }
                    }
                    else
                    {
                        if (IsRedacted(kvp.Key))
                        {
                            sb.Append("Set:" + kvp.Key + "=[REDACTED]" + " ");
                        }
                        else
                        {
                            sb.Append("Set:" + kvp.Key + "=" + kvp.Value + " ");
                        }

                        Properties.Add(kvp.Key, kvp.Value);
                    }
                }

                if (!String.IsNullOrWhiteSpace(sb.ToString()))
                    m_logger.Information("Config Changes: " + sb.ToString());
            }
            else
            {
                foreach (KeyValuePair<string, string> kvp in properties)
                    Properties[kvp.Key] = kvp.Value;
            }
        }

        public string GetString(string key)
        {
            if (!Properties.ContainsKey(key))
                return string.Empty;

            return Properties[key] as string;
        }


        public string GetString(string key, string defaultValue)
        {
            string value = GetString(key);

            if (string.IsNullOrEmpty (value))
                return defaultValue;

            return value;
        }

        public void SetString(string key, string value)
        {
            if (LogChanges)
            {
                if (Properties.ContainsKey(key))
                {
                    m_logger.Information("Config Changes: Set:" + key + "=" + value);
                    Properties[key] = value;
                }
                else
                {
                    m_logger.Information("Config Changes: Add:" + key + "=" + value);
                    Properties.Add(key, value);
                }
            }
            else
            {
                Properties[key] = value;
            }
        }

        public int GetInt(string key)
        {
            return GetInt(key, 0);
        }

        public int GetInt(string key, int defaultValue)
        {
            if (!Properties.ContainsKey(key))
                return defaultValue;

            return int.Parse(Properties[key]);
        }

        public void SetInt(string key, int value)
        {
            SetString(key, value.ToString());
        }

        public bool GetBoolean(string key)
        {
            return GetBoolean(key, false);
        }

        public bool GetBoolean(string key, bool defaultValue)
        {
            if (!Properties.ContainsKey(key))
                return defaultValue;

            bool parsed = false;
            if (bool.TryParse(Properties[key], out parsed))
                return parsed;

            return defaultValue;
        }

        public void SetBoolean(string key, bool value)
        {
            string stringValue = value ? "true" : "false";
            SetString(key, stringValue);
        }

        public float GetFloat(string key)
        {
            return GetFloat(key, 0f);
        }

        public float GetFloat(string key, float defaultValue)
        {
            if (!Properties.ContainsKey(key))
                return defaultValue;

            return float.Parse(Properties[key]);
        }

        public void SetFloat(string key, float value)
        {
            SetString(key, value.ToString());
        }

        public double GetDouble(string key)
        {
            return GetDouble(key, 0);
        }

        public double GetDouble(string key, double defaultValue)
        {
            if (!Properties.ContainsKey(key))
                return defaultValue;

            return double.Parse(Properties[key]);
        }

        public void SetDouble(string key, double value)
        {
            SetString(key, value.ToString());
        }

        public decimal GetDecimal(string key)
        {
            return GetDecimal(key, 0);
        }

        public decimal GetDecimal(string key, decimal defaultValue)
        {
            if (!Properties.ContainsKey(key))
                return defaultValue;

            return decimal.Parse(Properties[key]);
        }

        public void SetDecimal(string key, decimal value)
        {
            SetString(key, value.ToString());
        }

        public void SetObject<T>(string key, T value)
        {
            SetString(key, JsonConvert.SerializeObject(value));
        }

        public T GetObject<T>(string key)
        {
            return GetObject<T>(key, default(T));
        }

        public T GetObject<T>(string key, T defaultValue)
        {
            if (!Properties.ContainsKey(key))
                return defaultValue;

            return JsonConvert.DeserializeObject<T>(GetString(key).ToLower());
        }

        public IDictionary<string, string> GetList(List<string> keys)
        {
            var values = new Dictionary<string, string>(keys.Count);
            string value;

            foreach (string key in keys) 
            {
                if (Properties.TryGetValue (key, out value)) 
                {
                    values.Add(key, value);
                } 
                else 
                {
                    m_logger.Warning("Unknown Configuration Item: " + key);
                }
            }

            return values;
        }

        // Implementation of IConsortiumValueContainer
        public object GetValueByName(string name, Type requestType)
        {
            // Try to map to the requested type for assignment
            if (requestType == typeof(int))
                return GetInt(name);
            if (requestType == typeof(bool))
                return GetBoolean(name);
            if (requestType == typeof(float))
                return GetFloat(name);
            if (requestType == typeof(double))
                return GetDouble(name);
            if (requestType == typeof(decimal))
                return GetDecimal(name);
            if (requestType == typeof(string))
                return GetString(name);

            throw new NotSupportedException(string.Format("Cannot bind from configuration value to type {0}", requestType.Name));
        }

        protected bool IsRedacted(string keyName)
        {
            if (keyName != null)
            {
                foreach (string redacted in c_redactedConfigNames)
                {
                    if (keyName.ToUpper().Contains(redacted.ToUpper()))
                        return true;
                }
            }

            return false;
        }
    }
}

