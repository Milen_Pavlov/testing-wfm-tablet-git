﻿using System;
using System.Threading.Tasks;

namespace WFM.Core
{
    public interface IEditModeLink
    {
        bool IsEdited { get; }
        Task Save();
        void Reset();
    }
}