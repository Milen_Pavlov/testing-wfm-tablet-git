﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Newtonsoft.Json;
using Siesta;
using WFM.Shared.Extensions;

namespace WFM.Core
{
    public class ExceptionFilterItem
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }

    [ConsortiumService]
    public class DailyOverviewFilterService : IDailyOverviewFilterService
    {
        public const string EMPLOYEE_FILTER_CODE = "e";
        public const string EXCEPTION_FILTER_CODE = "x";

        public const string EXCEPTIONS_FILTER_PATH = "assets://Theme/ExceptionsFilter.json";

        private readonly IEmployeeService m_employeeService;
        private readonly IFileSystem m_fileSystem;

        private MvxSubscriptionToken m_signOutToken;
        private MvxSubscriptionToken m_orgUnitChangedToken;

        private List<ExceptionFilterItem> ExceptionsToFilter { get; set; }

        public Action OnFiltersChanged { get; set; }

        private string m_currentView;
        public string CurrentView { 
            private get
            { 
                return m_currentView; 
            }
            set
            {
                m_currentView = value;
            }
        }
        private Dictionary<string, List<FilterGroup>> m_currentFilters;
        public List<FilterGroup> CurrentFilters
        {
            get
            {
                CreateFilterIfNull();
                if(m_currentFilters.ContainsKey(CurrentView))
                    return m_currentFilters[CurrentView];
                return null;
            }
            set
            {
                m_currentFilters[CurrentView] = value;   
            }
        }

        public List<FilterGroup> FilterGroups { get; set; }

        public DailyOverviewFilterService (IEmployeeService employee, IFileSystem fileSystem, IMvxMessenger messenger)
        {
            m_employeeService = employee;
            m_fileSystem = fileSystem;
            m_currentFilters = new Dictionary<string, List<FilterGroup>>();
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_orgUnitChangedToken = messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
        }

        private void OnSignOut(SignOutMessage message)
        {
            ClearAllFilters();
        }

        private async void OnOrgEnd(SetOrgEndMessage message)
        {
            ClearAllFilters();
            var sessionData = message.Sender as SessionData;
            await GenerateFilterGroups(sessionData.Site.SiteID);
        }

        public async Task GenerateFilterGroups(int siteID)
        {
            if(FilterGroups == null)
            {
                m_currentFilters.Clear();
                var result = new List<FilterGroup>();

                var employeeFilterGroup = GenerateEmployeeFilterGroup(siteID);
                result.Add(await employeeFilterGroup);
                result.Add(GenerateExceptionFilterGroup());

                FilterGroups = result;
            }

            if(OnFiltersChanged != null)
                OnFiltersChanged.Invoke();
        }

        private async Task<FilterGroup> GenerateEmployeeFilterGroup(int siteID)
        {
            var employeeRequest = m_employeeService.GetAllEmployees(siteID);
            var employeeResult = await employeeRequest;

            var employeeFilterGroup = new FilterGroup();
            var employeeItems = new List<FilterItem>();

            foreach(var employee in employeeResult)
            {
                employeeItems.Add(GenerateEmployeeFilterItem(employee));
            }

            employeeFilterGroup.Items = employeeItems.Where(x => x != null).ToList();
            employeeFilterGroup.Type = EMPLOYEE_FILTER_CODE;

            employeeFilterGroup.Name = "Employee";

            return employeeFilterGroup;
        }

        private FilterItem GenerateEmployeeFilterItem(JDAEmployeeInfo employeeInfo)
        {
            if(employeeInfo == null)
                return null;
            
            var firstName = (string.IsNullOrEmpty(employeeInfo.NickName)) ? employeeInfo.FirstName : employeeInfo.NickName;
            var employeeName = string.Format("{0}, {1}", employeeInfo.LastName, firstName);

            var employeeFilterItem = new FilterItem()
            {
                Name = employeeName,
                ID = employeeInfo.EmployeeID,
                Selected = false,
            };

            return employeeFilterItem;
        }

        private FilterGroup GenerateExceptionFilterGroup()
        {
            GetExceptionsToFilter();
            var exceptionFilterGroup = new FilterGroup();
            var exceptionItems = new List<FilterItem>();

            foreach(var exception in ExceptionsToFilter)
            {
                exceptionItems.Add(GenerateExceptionFilterItem(exception));
            }

            exceptionFilterGroup.Items = exceptionItems.Where(x => x != null).ToList();
            exceptionFilterGroup.Type = EXCEPTION_FILTER_CODE;

            exceptionFilterGroup.Name = "Exception";

            return exceptionFilterGroup;
        }

        private FilterItem GenerateExceptionFilterItem(ExceptionFilterItem exception)
        {
            var exceptionFilterItem = new FilterItem()
            {
                Name = exception.Name,
                ID = exception.ID,
                Selected = false,
            };

            return exceptionFilterItem;
        }

        public List<SiteTimecardEntry> FilterTimecards(List<SiteTimecardEntry> timecards)
        {
            if(CurrentFilters == null)
                return timecards;
            
            var filteredTimecards = timecards;
            foreach(var filter in CurrentFilters)
            {
                if(filter != null)

                switch(filter.Type)
                {
                    case EMPLOYEE_FILTER_CODE:
                        var employeeFilter = filter.Items.Where(x => x.Selected).Select(x => x.ID);
                        if(employeeFilter.Count() > 0)
                            filteredTimecards = filteredTimecards.Where(x => employeeFilter.Contains(x.EmployeeID)).ToList();
                        break;
                    case EXCEPTION_FILTER_CODE:
                        var exceptionList = filter.Items.Where(x => x.Selected).Select(x => x.Name);
                        if(exceptionList.Count() <= 0)
                            break;
                        filteredTimecards = filteredTimecards.Where(x => x.Exceptions != null && x.Exceptions.Count > 0).ToList();
                        if(filteredTimecards.Count > 0)
                        {
                            var filteredItems = new List<SiteTimecardEntry>();
                            
                            foreach(var timecard in filteredTimecards)
                            {
                                if(filteredItems.Contains(timecard))
                                    continue;

                                foreach(var exception in timecard.Exceptions)
                                {
                                    if(exceptionList.Contains(exception.Text))
                                    {

                                        filteredItems.Add(timecard);
                                        break;
                                    }
                                }
                            }
                            filteredTimecards = filteredItems;
                        }
                        break;
                    default:    
                        break;
                }
            }

            return filteredTimecards;
        }

        private void GetExceptionsToFilter()
        {
            ExceptionsToFilter = new List<ExceptionFilterItem>();

            if(m_fileSystem.Exists(EXCEPTIONS_FILTER_PATH))
            {
                string txt = m_fileSystem.ReadText(EXCEPTIONS_FILTER_PATH);
                try
                {
                    var exceptions = JsonConvert.DeserializeObject<Dictionary<string,bool>>(txt);
                    int exceptionCount = 1;
                    foreach(var exception in exceptions)
                    {
                        if(exception.Value == false)
                            continue;
                        
                        var newItem = new ExceptionFilterItem()
                        {
                            Name = exception.Key,
                            ID = exceptionCount,
                        };
                        ExceptionsToFilter.Add(newItem);
                        exceptionCount++;
                    }
                }
                catch (Exception e)
                {
                    Log.Debug(e.ToString());
                }
            }
        }

        public void ApplyFilter(FilterGroup filterToApply)
        {
            if(CurrentFilters == null)
                CurrentFilters = new List<FilterGroup>();

            for(int i = 0; i < CurrentFilters.Count; ++i)
            {
                if(CurrentFilters[i].Type == filterToApply.Type)
                {
                    CurrentFilters[i] = filterToApply.Clone();
                    return;
                }
            }

            CurrentFilters.Add(filterToApply.Clone());
        }

        public void ClearFilter()
        {
            foreach(var filter in CurrentFilters)
            {
                foreach(var item in filter.Items)
                {
                    item.Selected = false;
                }
            }
        }

        public void ClearAllFilters()
        {
            m_currentFilters.Clear();

            if(FilterGroups != null)
                FilterGroups.Clear();
            FilterGroups = null;
        }

        private void CreateFilterIfNull()
        {
            if(m_currentFilters.ContainsKey(CurrentView) == false)
            {
                if(FilterGroups != null)
                {
                    var newFilterGroup = new List<FilterGroup>();
                    foreach(var filter in FilterGroups)
                    {
                        newFilterGroup.Add(filter.Clone());
                    }
                    m_currentFilters.Add(CurrentView, newFilterGroup);
                }
            }
        }
    }
}

