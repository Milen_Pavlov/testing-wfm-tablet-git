﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using Siesta;

namespace WFM.Core
{
    
    [ConsortiumService]
    public class DailyOverviewEditShiftService : ManagedRestService
    {
        private readonly RestClient m_restClient;
        private readonly EmployeeService m_employeeService;
        private readonly IAlertBox m_alertBox;
        private readonly IStringService m_localiser;
        private readonly SessionData m_session;
        private readonly IESSConfigurationService m_essConfig;

        private CancellationTokenSource m_processChangeTokenSource;
        private int m_punchID = -1;

        public DailyOverviewEditShiftService (RestClient restClient, EmployeeService employee, IAlertBox alertBox, IStringService localiser, SessionData session, IESSConfigurationService essConfig)
        {
            m_restClient = restClient;
            m_employeeService = employee;
            m_alertBox = alertBox;
            m_localiser = localiser;
            m_session = session;
            m_essConfig = essConfig;
        }

        public async Task<EditShiftData> GetEditShiftData(int siteID, SiteTimecardEntry timecard, DateTime date, CancellationToken ct)
        {
            var shiftID = timecard.ShiftID;
            var punchedShiftID = timecard.PunchedShiftID.GetValueOrDefault();
            var scheduleRequest = m_restClient.Post<ScheduleDataByScheduleIDResponse> (new ScheduleDataByScheduleIDRequest ()
                {
                    schedID = shiftID,
                    excludeRoles = true,
                }, ct);
            
            var scheduledPunchesRequest = m_restClient.Post<GetPunchesForScheduleResponse> (new GetPunchesForScheduleRequest ()
                {
                    siteID = siteID,
                    scheduleID = shiftID,
                }, ct);

            var punchesRequest = m_restClient.Post<GetPunchesForShiftResponse> (new GetPunchesForShiftRequest ()
                {
                    siteID = siteID,
                    shiftID = punchedShiftID,
                }, ct);

            var auditRequest = m_restClient.Post<GetAuditReasonsResponse> (new GetAuditReasonsRequest ()
                {
                    siteId = siteID,
                }, ct);
            
            var allEmployeesRequest = m_employeeService.GetAllEmployees(siteID, date);

            var employeesResponse = await allEmployeesRequest;
            var punchesResponse = await punchesRequest;
            var scheduleResponse = await scheduleRequest;
            var scheduledPunchesResponse = await scheduledPunchesRequest;
            var auditResponse = await auditRequest;

            if (ct.IsCancellationRequested)
                return null;
            
            JDAPunchesForSchedule punchesResult = null;
            if (scheduledPunchesResponse.Success)
                punchesResult = scheduledPunchesResponse.Object.data;

            if (punchesResponse.Success)
            {
                if(punchesResult == null)
                    punchesResult = punchesResponse.Object.data;
                else
                    punchesResult.ShiftPunches.AddRange(punchesResponse.Object.data.ShiftPunches);
            }

            List<JDANameAndID> auditReasons = new List<JDANameAndID> ();
            if (auditResponse.Success && auditResponse.Object.data != null)
            {
                auditReasons = auditResponse.Object.data;
            }
            List<JDASchedulePunches> scheduleResult = new List<JDASchedulePunches> ();
            if (scheduleResponse.Success)
                scheduleResult = scheduleResponse.Object.data;

            var punchMeals = m_essConfig.AllowPunchesForMeals;
            var punchBreaks = m_essConfig.AllowPunchesForBreaks;
            var punchJobs = m_essConfig.AllowPunchesForJobs;

            EditShiftData shiftData = null;
            if(punchesResult != null)
                shiftData = new EditShiftData (punchesResult, employeesResponse, scheduleResult, auditReasons, punchMeals, punchBreaks, punchJobs);
            
            if(shiftData == null)
            {
                shiftData = new EditShiftData(timecard, employeesResponse, auditReasons);
            }

            return shiftData;
        }

        public async Task<List<JDANameAndID>> GetAuditReasons(int siteID, CancellationToken ct)
        {
            List<JDANameAndID> auditReasons = new List<JDANameAndID> ();
            var auditRequest = m_restClient.Post<GetAuditReasonsResponse> (new GetAuditReasonsRequest ()
                {
                    siteId = siteID,
                }, ct);

            var auditResponse = await auditRequest;

            if (ct.IsCancellationRequested)
                return auditReasons;

            if (auditResponse.Success && auditResponse.Object.data != null)
            {
                auditReasons = auditResponse.Object.data;
            }

            return auditReasons;
        }

        public bool ValidateEditShiftData(EditShiftData data, bool showErrors = true)
        {
            var shiftDetail = data.Details.FirstOrDefault (x => x.PunchCode == PunchCodes.Shift);
            if (shiftDetail == null)
            {
                m_alertBox.ShowOK (m_localiser.Get ("edit_shift_invalid_title"), m_localiser.Get ("edit_shift_invalid_no_schedule"));
                return false;
            }
            if (shiftDetail.ActualStart.HasValue == false)
            {
                var anyWithPunches = data.Details.Where (x => x.ActualStart != null || x.ActualEnd != null);
                if (anyWithPunches.Count () > 0)
                {
                    m_alertBox.ShowOK (m_localiser.Get ("edit_shift_invalid_title"), m_localiser.Get ("edit_shift_invalid_no_schedule_start"));
                    return false;
                }
            }

            // Store all punch times to check for overlapping breaks/meals
            List<Tuple<DateTime, DateTime?>> punchTimes = new List<Tuple<DateTime, DateTime?>> ();
            foreach (var detail in data.Details)
            {
                if (detail.PunchCode != PunchCodes.Shift && IsDetailOutsideOfShift(detail, shiftDetail))
                {
                    m_alertBox.ShowOK (m_localiser.Get ("edit_shift_invalid_title"), m_localiser.Get ("edit_shift_invalid_detail_outside_shift"));
                    return false; 
                }

                // detail has no start or end value
                if (detail.ActualStart.HasValue == false && detail.ActualEnd.HasValue == false)
                    continue;

                // detail has no start but does have an end
                if(detail.ActualStart.HasValue == false && detail.ActualEnd.HasValue && detail.PunchCode != PunchCodes.Job)
                {
                    m_alertBox.ShowOK (m_localiser.Get ("edit_shift_invalid_title"), m_localiser.Get ("edit_shift_invalid_no_detail_start"));
                    return false;
                }

                // detail ends before it starts
                if (detail.ActualStart.HasValue && detail.ActualEnd.HasValue && detail.ActualEnd.Value < detail.ActualStart.Value)
                {
                    m_alertBox.ShowOK (m_localiser.Get ("edit_shift_invalid_title"), m_localiser.Get ("edit_shift_invalid_end_before_start"));
                    return false;
                }

                // Only check for overlapping breaks/meals
                if (detail.PunchCode == PunchCodes.Break || detail.PunchCode == PunchCodes.Meal)
                {
                    //check for overlapping breaks/meals
                    if (DoesDetailOverlap (detail, punchTimes))
                    {
                        m_alertBox.ShowOK (m_localiser.Get ("edit_shift_invalid_title"), m_localiser.Get ("edit_shift_invalid_overlap"));
                        return false;
                    }

                    //Only add time with punches
                    if(detail.ActualStart.HasValue)
                        punchTimes.Add (new Tuple<DateTime, DateTime?> (detail.ActualStart.Value, detail.ActualEnd));
                }
            }

            return true;
        }

        private bool IsDetailOutsideOfShift(EditShiftPunchDetails detail, EditShiftPunchDetails shift)
        {
            // No shift start
            if (shift.ActualStart.HasValue == false)
                return false;

            // No detail start
            if (detail.ActualStart.HasValue == false)
                return false;

            // Detail start is before shift start
            if (detail.ActualStart.Value < shift.ActualStart.Value)
                return true;

            // No shift end
            if (shift.ActualEnd.HasValue == false)
                return false;

            // Detail start is after shift end
            if (detail.ActualStart.Value > shift.ActualEnd.Value)
                return true;

            // No detail end
            if (detail.ActualEnd.HasValue == false)
                return false;

            // Detail end is after shift end
            if (detail.ActualEnd.Value > shift.ActualEnd.Value)
                return true;

            return false;
        }

        private bool DoesDetailOverlap(EditShiftPunchDetails detail, List<Tuple<DateTime, DateTime?>> punchTimes)
        {
            if (punchTimes == null || punchTimes.Count <= 0)
            {
                return false;
            }

            foreach (var punchTime in punchTimes)
            {
                // If this detail Ends before the other item Starts, then no overlap
                if (detail.ActualEnd.HasValue && detail.ActualEnd.Value < punchTime.Item1)
                    continue;
                
                // If this detail Starts after the other item Ends, then no overlap
                if (punchTime.Item2.HasValue && detail.ActualStart.Value > punchTime.Item2.Value )
                    continue;

                // Detail hasn't finished and other item starts after the detail starts
                if (detail.ActualEnd.HasValue == false && detail.ActualStart.Value < punchTime.Item1)
                    return true;
                
                // Other item hasn't finished and detail starts after the other item starts
                if (punchTime.Item2.HasValue == false && punchTime.Item1 < detail.ActualStart)
                    return true;
            }

            return false;
        }

        public async Task<bool> WriteEditShiftData(EditShiftData data, int employeeID)
        {
            // create request with required data and change commands
            var request = new ProcessChangeCommandsRequest ();
            request.jsChangeCommands = GenerateChanges(data, employeeID);

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ProcessChangeCommandsReply>>(() =>
                        {
                            return m_restClient.Post<ProcessChangeCommandsReply>(request, token).Result;
                        });
                };

            m_processChangeTokenSource = new CancellationTokenSource();

            var result = await PerformOperation<RestReply<ProcessChangeCommandsReply>>(m_restClient.TimeoutMs, taskGenerator, m_processChangeTokenSource.Token);

            if (result.ResponseObject != null && result.ResponseObject.Success)
            {
                if (result.ResponseObject.Object.data.Errors != null && (result.ResponseObject.Object.data.Errors.Length > 0))
                {
                    // Try and show a more descriptive error message
                    if(ShowDescriptiveError(result.ResponseObject.Object.data.Errors[0].Type) == false)
                    {
                        // No error message set so show a generic one instead
                        m_alertBox.ShowOK (m_localiser.Get ("edit_shift_upload_error_title"), m_localiser.Get ("edit_shift_upload_error_message"));
                    }
                    return false;
                }
                return true;
            }

            return false;
        }

        private bool ShowDescriptiveError(string error)
        {
            if(string.IsNullOrEmpty(error))
                return false;
            
            if(error.Contains("CannotHaveZeroLengthPunchedShiftOrPunchDetails"))
            {
                m_alertBox.ShowOK (m_localiser.Get ("edit_shift_upload_error_title"), m_localiser.Get ("edit_shift_upload_zero_length_detail_message"));
                return true;
            }

            if(error.Contains("JobTransferOutsidePunchedShift"))
            {
                m_alertBox.ShowOK (m_localiser.Get ("edit_shift_upload_error_title"), m_localiser.Get ("edit_shift_upload_job_transfer_outside_shift_message"));
                return true;
            }

            return false;
        }

        public int GetNewPunchID ()
        {
            return --m_punchID;
        }

        private JDAChangeCommand[] GenerateChanges(EditShiftData data, int employeeID)
        {
            List<JDAChangeCommand> result = new List<JDAChangeCommand> ();
            foreach (var detail in data.Details)
            {
                if(result.Where(x => x.GetType() == typeof(JDADeletePunchedShift)).Count() > 0)
                    break;
                switch (detail.PunchCode)
                {
                    case PunchCodes.Shift:
                        result.AddRange (GenerateShiftPunchChanges (detail, employeeID, data.AuditReason));
                        break;
                    case PunchCodes.Meal:
                    case PunchCodes.Break:
                        result.AddRange (GenerateShiftDetailPunchChanges (detail, employeeID, data.AuditReason));
                        break;
                    case PunchCodes.Job:
                        result.AddRange (GenerateJobTransfer (detail, employeeID, data.AuditReason));
                        break;
                    case PunchCodes.Unknown:
                    default:
                        break;
                }
            }
            return result.ToArray (); 
        }

        private List<JDAChangeCommand> GenerateShiftPunchChanges (EditShiftPunchDetails detail, int employeeID, int? auditReason)
        {
            List<JDAChangeCommand> changes = new List<JDAChangeCommand> ();
            if (detail.IsStartPunchNew ())
            {
                // New Start shift punch
                changes.Add (new JDAAddPunchedShift ()
                    {
                        PunchedShiftID = detail.StartPunchID,
                        SiteID = m_session.Site.SiteID,
                        EmployeeID = employeeID,
                        JobID = detail.JobID,
                        Start = detail.ActualStart.Value,
                        ScheduledShiftID = detail.ScheduleID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    }); 
            }
            else if (detail.IsStartPunchDeleted ())
            {
                // Deleted Start shift punch
                changes.Add (new JDADeletePunchedShift ()
                    {
                        PunchedShiftID = detail.StartPunchID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
                return changes;
            }
            else if (detail.IsStartPunchModified())
            {
                // Modified Start shift punch
                changes.Add (new JDAModifyPunchedShiftStart ()
                    {
                        PunchedShiftID = detail.StartPunchID,
                        Start = detail.ActualStart,
                        ScheduledShiftID = detail.ScheduleID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null, 
                    });
            }

            if (detail.IsEndPunchNew ())
            {
                // New End shift punch
                changes.Add (new JDAAddPunchedShiftEnd ()
                    {
                        PunchedShiftID = detail.EndPunchID,
                        End = detail.ActualEnd.Value,
                        IsBackPay = false,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }
            else if (detail.IsEndPunchDeleted ())
            {
                // Deleted End shift punch
                changes.Add (new JDADeletePunchedShiftEnd ()
                    {
                        PunchedShiftID = detail.EndPunchID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }
            else if (detail.IsEndPunchModified())
            {
                changes.Add (new JDAModifyPunchedShiftEnd ()
                    {
                        PunchedShiftID = detail.EndPunchID,
                        End = detail.ActualEnd.Value,
                        ScheduledShiftID = detail.ScheduleID,
                        IsBackPay = false,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }

            if (detail.HasJobChanged())
            {
                changes.Add (new JDAModifyPunchedShiftJob ()
                    {  
                        PunchedShiftID = detail.ShiftID,
                        JobID = detail.JobID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }

            return changes;
        }

        private List<JDAChangeCommand> GenerateShiftDetailPunchChanges (EditShiftPunchDetails detail, int employeeID, int? auditReason)
        {
            List<JDAChangeCommand> changes = new List<JDAChangeCommand> ();
            if (detail.IsStartPunchNew ())
            {
                // New Start shift punch
                changes.Add (new JDAAddPunchDetail ()
                    {
                        PunchedShiftID = detail.StartPunchID,
                        PunchDetailID = detail.StartPunchDetailID,
                        Type = EditShiftPunchData.GetPunchCode (detail.PunchCode),
                        Start = detail.ActualStart,
                        End = detail.ActualEnd,
                        IsBackPay = false,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    }); 
            }
            else if (detail.IsStartPunchDeleted ())
            {
                // Deleted shift punch
                changes.Add (new JDADeletePunchDetail ()
                    {
                        PunchedShiftID = detail.StartPunchID,
                        PunchDetailID = detail.StartPunchDetailID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }
            else
            {
                if (detail.IsStartPunchModified ())
                {
                    // Modified Start shift punch
                    changes.Add (new JDAModifyDetailStart ()
                        {
                            PunchedShiftID = detail.StartPunchID,
                            PunchDetailID = detail.StartPunchDetailID,
                            Start = detail.ActualStart,
                            IsBackPay = false,
                            AuditReasonID = auditReason,
                            BackPayAuditReasonID = null, 
                        });
                }

                if (detail.IsEndPunchNew ())
                {
                    // New End shift punch
                    changes.Add (new JDAAddPunchDetailEnd ()
                        {
                            PunchedShiftID = detail.StartPunchID,
                            PunchDetailID = detail.StartPunchDetailID,
                            DetailStartType = EditShiftPunchData.GetPunchCode (detail.PunchCode),
                            Start = detail.ActualStart,
                            End = detail.ActualEnd,
                            IsBackPay = false,
                            AuditReasonID = auditReason,
                            BackPayAuditReasonID = null,
                        });
                }
                else if (detail.IsEndPunchDeleted ())
                {
                    // Deleted End shift punch
                    changes.Add (new JDADeletePunchDetailEnd ()
                        {
                            PunchedShiftID = detail.StartPunchID,
                            PunchDetailID = detail.StartPunchDetailID,
                            AuditReasonID = auditReason,
                            BackPayAuditReasonID = null,
                        });
                }
                else if (detail.IsEndPunchModified ())
                {
                    changes.Add (new JDAModifyDetailEnd ()
                        {
                            PunchedShiftID = detail.StartPunchID,
                            PunchDetailID = detail.StartPunchDetailID,
                            End = detail.ActualEnd,
                            IsBackPay = false,
                            AuditReasonID = auditReason,
                            BackPayAuditReasonID = null,
                        });
                } 
            }

            return changes;
        }

        private List<JDAChangeCommand> GenerateJobTransfer(EditShiftPunchDetails detail, int employeeID, int? auditReason)
        {
            List<JDAChangeCommand> changes = new List<JDAChangeCommand> ();

            if (detail.IsStartPunchNew ())
            {
                // New job transfer
                changes.Add (new JDAAddJobTransfer ()
                    {
                        PunchedShiftID = detail.ShiftID,
                        JobTransferID = detail.StartPunchDetailID,
                        Type = EditShiftPunchData.GetPunchCode (detail.PunchCode),
                        JobID = detail.JobID,
                        TransferTime = detail.ActualStart,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null, 
                        ShiftType = null,
                    });
            }
            else if (detail.HasJobBeenDeleted ())
            {
                // Deleted job transfer
                changes.Add (new JDADeleteJobTransfer ()
                    {
                        PunchedShiftID = detail.ShiftID,
                        JobTransferID = detail.StartPunchDetailID,
                        JobID = detail.JobID,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }
            else if( detail.IsStartPunchModified() || detail.HasJobChanged() )
            {
                // Modified job
                changes.Add (new JDAModifyJobTransfer ()
                    {
                        PunchedShiftID = detail.ShiftID,
                        JobTransferID = detail.StartPunchDetailID,
                        JobID = detail.JobID,
                        TransferTime = detail.ActualStart,
                        AuditReasonID = auditReason,
                        BackPayAuditReasonID = null,
                    });
            }
            return changes;
        }
    }
}

