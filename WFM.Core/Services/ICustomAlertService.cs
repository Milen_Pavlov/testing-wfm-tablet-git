﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;

namespace WFM.Core
{
    public enum AlertReturn
    {
        Positive,
        Negative,
        Neutral,
        Dismiss,
    }

    public interface ICustomAlertService
    {
        AlertBoxToken ShowMyHoursInput (string columnTitle, string rowTitle, string inputPlaceholder, Action<AlertReturn, string> onSelection);
    }
}

