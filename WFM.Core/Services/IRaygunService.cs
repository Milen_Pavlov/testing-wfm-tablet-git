﻿using System;
using System.Collections;

namespace WFM.Core
{
    public interface IRaygunService
    {
        void LogCustomError(Exception e);
        void LogCustomError(Exception e, IDictionary data);

        void ClearUserInfo();
        void SetCurrentUserInfo(string loginname, string siteId, string siteName, string environment);
    }
}

