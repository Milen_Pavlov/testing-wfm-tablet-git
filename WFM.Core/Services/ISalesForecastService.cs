﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;

namespace WFM.Core
{
    public interface ISalesForecastService
    {
        Task<List<JDAForecastedData>> GetSalesForecastData(DateTime date, int groupId, int delayMs = 0);
        Task<List<JDANameAndID>> GetForecastGroups();
        Task<SalesForcastDataResponse> ReadForecastAsync (DateTime date, int groupId, CancellationToken token, int msDelay);
        Task<bool> SubmitChanges (List<SalesForecastDataItem> values);
        void CancelForecastRead ();
        void CancelGroupForecastRead ();
        void CancelSubmitChanges();
        int GetSelectedIndex();
    }
}

