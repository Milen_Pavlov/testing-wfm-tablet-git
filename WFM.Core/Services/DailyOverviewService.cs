﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Consortium.Client.Core;
using System.Linq;
using System.Threading;

namespace WFM.Core
{
    public class SiteTimecardDataResult
    {
        public List<SiteTimecardEntry> Timecards { get; set; }

        public bool WasCancelled { get; set; }

        public bool Success { get; set; }

        public SiteTimecardDataResult()
        {
            Timecards = new List<SiteTimecardEntry>();
        }
    }

    [ConsortiumService]
    public class DailyOverviewService
    {
        private readonly RestClient m_restClient;
        private readonly EmployeeService m_employeeService;
        
        public DailyOverviewService (RestClient restClient, EmployeeService employeeService)
        {
            m_restClient = restClient;
            m_employeeService = employeeService;
        }

        public async Task<List<JDAPunchException>> GetPunchExceptionsForWeekStarting(int siteID, DateTime fromDate, CancellationToken ct)
        {
            var request = GetPunchExceptions (siteID, fromDate, fromDate.AddDays (7), ct);
            await request;

            return request.Result;
        }

        public async Task<List<JDAPunchException>> GetPunchExceptionsForDate(int siteID, DateTime fromDate, CancellationToken ct)
        {
            var request = GetPunchExceptions (siteID, fromDate, fromDate.Add(new TimeSpan(23,59,59)), ct);
            await request;

            return request.Result;
        }

        public async Task<List<JDAPunchException>> GetPunchExceptions(int siteID, DateTime fromDate, DateTime toDate, CancellationToken ct)
        {
            List<JDAPunchException> result = new List<JDAPunchException> ();

            DateTime utcFromDate = DateTime.SpecifyKind (fromDate.Date, DateTimeKind.Unspecified);
            DateTime utcToDate = DateTime.SpecifyKind (toDate.Date, DateTimeKind.Unspecified);

            var punchExceptionRequest = m_restClient.Post<GetPayPeriodPunchExceptionsForSiteResponse> (new GetPayPeriodPunchExceptionsForSiteRequest ()
                {
                    siteID = siteID,
                    startDate = utcFromDate,
                    endDate = utcToDate,
                    includeBorrowedEmps = true,
                }, ct);

            var punchExceptionResponse = await punchExceptionRequest;

            //TODO - We don't really show exceptions on other sites....what do we do?
            if (punchExceptionResponse.Success)
                result = punchExceptionResponse.Object.data.Where (x => x.WorkedBusinessUnitID.HasValue && x.WorkedBusinessUnitID.Value == siteID).ToList ();

            return result;
        }

        public async Task<List<JDAUnpairedPunch>> GetUnpairedPunchesForWeekStarting(int siteID, DateTime fromDate, CancellationToken ct)
        {
            List<JDAUnpairedPunch> result = new List<JDAUnpairedPunch> ();

            DateTime utcFromDate = DateTime.SpecifyKind (fromDate, DateTimeKind.Unspecified);
            DateTime utcToDate = DateTime.SpecifyKind (fromDate.AddDays(7), DateTimeKind.Unspecified);
            //Get unpaired punches so we can match up and put in our own punch exceptions for them
            var unpairedPunchesRequest = m_restClient.Post<GetUnpairedPunchesResponse> (new GetUnpairedPunchesRequest ()
                {
                    siteID = siteID,
                    startDateTime = utcFromDate,
                    endDateTime = utcToDate,
                }, ct);

            var unpairedResponse = await unpairedPunchesRequest;

            if (unpairedResponse.Success)
                result = unpairedResponse.Object.data;

            return result;
        }

        public async Task<SiteTimecardDataResult> GetSiteTimecardsForDate(int siteID, DateTime date, CancellationToken ct)
        {
            SiteTimecardDataResult result = new SiteTimecardDataResult ();

            DateTime utcFromDate = DateTime.SpecifyKind (date, DateTimeKind.Unspecified);
            DateTime utcToDate = DateTime.SpecifyKind (date.AddDays(1), DateTimeKind.Unspecified);

            var timecardsRequest = m_restClient.Post<GetSiteTimecardsResponse> (new GetSiteTimecardsRequest ()
                {
                    siteID = siteID,
                    date = utcFromDate,
                },ct);

            //Get all jobs as we need that too TODO shift to global OnSiteChange as these won't change...
            var allJobsRequest = m_restClient.Post<GetAllJobsForSiteResponse> (new GetAllJobsForSiteRequest ()
                {
                    siteID = siteID,
                }, ct);

            //Only employee IDs in the timecards, gotta do another get all lookup to get their damned names TODO: Maybe global OnSiteChange too??
            var allEmployeesRequest = m_employeeService.GetAllEmployees(siteID);

            //Get unpaired punches so we can match up and put in our own punch exceptions for them
            var unpairedPunchesRequest = m_restClient.Post<GetUnpairedPunchesResponse> (new GetUnpairedPunchesRequest ()
                {
                    siteID = siteID,
                    startDateTime = utcFromDate,
                    endDateTime = utcToDate,
                }, ct);

            //Get all punch exceptions as some aren't included and we need to merge them in...
            var punchExceptionsRequest = GetPunchExceptionsForDate(siteID, date, ct);

            var timecardsResponse = await timecardsRequest;
            var jobsResponse = await allJobsRequest;
            var employeesResponse = await allEmployeesRequest;
            var unpairedResponse = await unpairedPunchesRequest;
            var punchExceptionsResponse = await punchExceptionsRequest;

            if (ct.IsCancellationRequested)
            {
                result.WasCancelled = true;
            }
            else if (timecardsResponse.Success && jobsResponse.Success && unpairedResponse.Success && employeesResponse!=null) {
                List<JDAUnpairedPunch> unpaired = unpairedResponse.Object.data;

                result.Success = true;
                result.Timecards = timecardsResponse.Object.data.Data.Select (x => new SiteTimecardEntry (x, jobsResponse.Object.data, employeesResponse)).ToList ();
                

                //Add missing punch exceptions
                if (punchExceptionsResponse != null)
                    MergeMissingPunchExceptions (result, punchExceptionsResponse, employeesResponse);

                //Add custom exceptions
                //Unpaired Punch (These are int he daily overview but not marked, gotta match up)
                foreach (var entry in result.Timecards) {
                    if (unpaired.Any (x => x.EmployeeID == entry.EmployeeID &&
                           x.Start.HasValue &&
                           entry.ShiftStartActual.HasValue &&
                           x.Start == entry.ShiftStartActual))
                        entry.Exceptions.Add (new TimecardException ("Unpaired Punch", TimecardExceptionType.Unpaired, true));
                }

                foreach (var entry in result.Timecards)
                    entry.GenerateExceptionState ();
                    
                await PatchMissingEmployeeDetails (siteID, result);

                //Defaut ordering is by scheduled start
                result.Timecards.OrderBy (x => x.ShiftStartPlanned);

            }

            return result;
        }

        async Task PatchMissingEmployeeDetails (int siteID, SiteTimecardDataResult result)
        {
            var missingEmployeeTimeCards = result.Timecards.Where (timecard => String.IsNullOrEmpty (timecard.EmployeeName));

            if (missingEmployeeTimeCards.Count () > 0) 
            {
                var employees = await m_employeeService.GetEmployeesByID (siteID, missingEmployeeTimeCards.Select (timecard => timecard.EmployeeID).ToArray ());
                foreach (var timecard in missingEmployeeTimeCards) 
                {
                    timecard.SetNameIfPossible (employees);
                }
            }
        }

        private void MergeMissingPunchExceptions(SiteTimecardDataResult result, List<JDAPunchException> exceptions, JDAEmployeeInfo[] employees)
        {
            foreach (var exception in exceptions)
            {
                //This shouldn't ever happen..but just to be safe - No date, No idea where to put it
                if (!exception.BusinessDate.HasValue)
                    continue;

                //Try to find the timecard entry for this exception
                //We have employeeID, business date, shiftid?!
                var timecard = result.Timecards.FirstOrDefault (x =>
                        x.Date.HasValue && x.Date.Value.Date == exception.BusinessDate.Value.Date &&
                        x.EmployeeID == exception.EmployeeID.GetValueOrDefault () &&
                        ((x.ScheduledShiftID.HasValue && exception.ShiftID.HasValue && x.ScheduledShiftID.Value == exception.ShiftID.Value) ||
                        (x.PunchedShiftID.HasValue && exception.ShiftID.HasValue && x.PunchedShiftID.Value == exception.ShiftID.Value)));

                if (timecard == null)
                {
                    //This must mean there is an exception for someone with no schedule and nopunches
                    //This happens when we get exceptions for employees borrowed to other sites...
                    SiteTimecardEntry newEntry = new SiteTimecardEntry();
                    newEntry.EmployeeID = exception.EmployeeID.Value;
                    newEntry.EmployeeName = exception.EmployeeFullName;
                    newEntry.Date = exception.BusinessDate;
                    newEntry.Exceptions = new List<TimecardException> ();
                    newEntry.Exceptions.Add (new TimecardException (exception.ExceptionName));
                    newEntry.GenerateExceptionState ();

                    if (string.IsNullOrEmpty (newEntry.EmployeeName))
                        newEntry.SetNameIfPossible (employees);

                    result.Timecards.Add (newEntry);
                }
                else
                {
                    //Look for the exception string in the list
                    bool exists = timecard.Exceptions.Any(x => x.Text == exception.ExceptionName);
                    if (!exists)
                        timecard.Exceptions.Add(new TimecardException(exception.ExceptionName));

                }
            }
        }
    }
}

