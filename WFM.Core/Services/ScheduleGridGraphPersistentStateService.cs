﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleGridGraphPersistentStateService
    {
        private readonly IMvxMessenger m_messenger;

        protected MvxSubscriptionToken m_signOutToken;

        public ScheduleGridGraphViewModel.ScreenSizeState ScreenState
        {
            get;
            set;
        }

        public bool GraphMode
        {
            get;
            set;
        }

        public ScheduleGridGraphPersistentStateService (IMvxMessenger messenger)
        {
            m_messenger = messenger;
            GraphMode = true;
            ScreenState = ScheduleGridGraphViewModel.ScreenSizeState.TopMax;

            m_signOutToken = m_messenger.Subscribe<SignOutMessage> (OnSignOut);
        }

        void OnSignOut(SignOutMessage message)
        {
            GraphMode = true;
            ScreenState = ScheduleGridGraphViewModel.ScreenSizeState.TopMax;
        }
    }
}