﻿namespace WFM.Core
{
    public interface ITimeOffRequestBlackoutOverrideService
    {
        bool CanOverrideBlackoutPeriod (bool canOverrideBlackoutPeriod, string timeOffTypesThatCanOverrideBlackoutPeriod, string currentTimeOffRequestType);
    }
}

