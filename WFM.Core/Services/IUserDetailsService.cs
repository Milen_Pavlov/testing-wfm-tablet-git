﻿using System;

namespace WFM.Core
{
    public interface IUserDetailsService
    {
        void SetUsername(string username);
        string GetUsername();
        void ClearUsername();

        void SetPassword(string password);
        string GetPassword();
        void ClearPassword();

        void SetKeyValue(string key, string value);
        string GetValueForKey(string key);
        void RemoveValueForKey(string key);
    }
}