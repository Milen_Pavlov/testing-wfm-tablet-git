﻿using System;
using System.Threading.Tasks;
using Consortium.Client.Core;

namespace WFM.Core
{
    public interface ISiteMgrCredentialsService
    {
        Task<SiteManagerUserDetails> GetCurrentUserDetails(string siteName);
    }
}

