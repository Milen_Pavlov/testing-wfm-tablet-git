﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using Siesta;
using Cirrious.MvvmCross.Plugins.Messenger;
using Newtonsoft.Json.Converters;

namespace WFM.Core
{
    [ConsortiumService]
    public class ApprovalsService : ManagedRestService
    {
        private readonly RestClient m_restClient;
        private readonly static string _CHANGECOMMANDTYPE = "SiteManager.Commands.Scheduling.ApproveDenySwapShiftRequest";

        private const string c_previousListFilePath = "documents://{0}.previouspendingapprovalsrequests.json";
        private const string c_lastNotificationCountFilePath = "documents://{0}.lastPendingApprovalsCount.txt";

        private CancellationTokenSource m_getPendingTokenSource = new CancellationTokenSource();
        private CancellationTokenSource m_getHistoryTokenSource = new CancellationTokenSource();
        private CancellationTokenSource m_getSwapDetailsTokenSource = new CancellationTokenSource ();
        private CancellationTokenSource m_processChangeTokenSource = new CancellationTokenSource();

        private PendingApprovalsUpdatedMessage m_lastPublishedMessage;
        private readonly IFileSystem m_fileSystem;
        private readonly IMvxMessenger m_messenger;

        public ApprovalsService(RestClient restClient, IFileSystem fileSystem, IMvxMessenger messenger)
        {
            m_restClient = restClient;
            m_messenger = messenger;
            m_fileSystem = fileSystem;
        }

        public int GetPreviousNotificationCount(int siteID)
        {
            if (m_lastPublishedMessage != null)
            {
                return m_lastPublishedMessage.Total;
            }
            else
            {
                int cachedNotificationCount = 0;

                string filePath = string.Format (c_lastNotificationCountFilePath, siteID);

                if (m_fileSystem.Exists (filePath))
                {
                    string text = m_fileSystem.ReadText (filePath);

                    int.TryParse (text, out cachedNotificationCount);
                }

                return cachedNotificationCount;
            }
        }

        private void PublishPendingApprovalsNotificationCount(int siteID, int notificationCount)
        {
            m_lastPublishedMessage = new PendingApprovalsUpdatedMessage (this) { Total = notificationCount };

            m_messenger.Publish(m_lastPublishedMessage);

            m_fileSystem.WriteText (string.Format (c_lastNotificationCountFilePath,siteID), notificationCount.ToString ());
        }

        public async Task UpdatePendingApprovalsCount(int siteID)
        {
            await GetAllPendingApprovals (siteID);
        }

        public async Task<JDAApprovalRequestsBody> GetAllPendingApprovals(int siteID)
        {
            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<GetAllApprovalsResponse>(() =>
                        {
                            return ReadPendingApprovalsAsync(siteID, token).Result;
                        });
                };

            m_getPendingTokenSource = new CancellationTokenSource();
            var result = await PerformOperation<GetAllApprovalsResponse>(m_restClient.TimeoutMs, taskGenerator, m_getPendingTokenSource.Token);

            if (result.ResponseObject != null)
            {
                PublishPendingApprovalsNotificationCount(siteID, result.ResponseObject.data.SwapShiftApprovals.Length);
                return result.ResponseObject.data;
            }

            return null;
        }

        public async Task<GetAllApprovalsResponse> ReadPendingApprovalsAsync(int siteID, CancellationToken token)
        {
            //Remove Time zone god dammit
            var request = new GetAllApprovalsRequest ()
                {
                    siteId = siteID,
                    id = siteID,
                };

            var data = m_restClient.Get<GetAllApprovalsResponse>(request, token);
            await data.ConfigureAwait (false);
            token.ThrowIfCancellationRequested();

            if (data.Result != null && data.Result.Success)
                return data.Result.Object;

            return null;
        }

        public async Task<JDAApprovalHistoryBody> GetHistoryApprovals(int siteID, DateTime start)
        {
            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<GetApprovalsHistoryResponse>(() =>
                        {
                            return ReadApprovalsHistoryAsync(siteID, start, token).Result;
                        });
                };

            m_getHistoryTokenSource = new CancellationTokenSource();
            var result = await PerformOperation<GetApprovalsHistoryResponse>(m_restClient.TimeoutMs, taskGenerator, m_getHistoryTokenSource.Token);
            m_getHistoryTokenSource.Token.ThrowIfCancellationRequested ();

            if (result.ResponseObject != null)
                return result.ResponseObject.data;

            return null;
        }

        public async Task<GetApprovalsHistoryResponse> ReadApprovalsHistoryAsync(int siteID, DateTime start, CancellationToken token)
        {
            
            DateTime nonZoned = DateTime.SpecifyKind(start, DateTimeKind.Unspecified);
            
            //A bug in siesta means that when doing a GET request it incorrectly mangles the timezone component for the query string
            //to make all times get specified in UTC
            //JDA expects the times in unspecified which is what we do with converting the time to a string using the same format as 
            //other requests. This is a stop gap until we fix Siesta
            
            string formattedDate = nonZoned.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK");
            
            var request = new GetApprovalsHistoryRequest ()
            {
                siteId = siteID,
                id = siteID,
                start = formattedDate,
            };

            var data = m_restClient.Get<GetApprovalsHistoryResponse>(request, token);
            await data.ConfigureAwait (false);
            token.ThrowIfCancellationRequested();

            if (data.Result != null && data.Result.Success)
                return data.Result.Object;

            return null;
        }

        public async Task<ShiftSwapDetails> GetSwapApprovalDetail(int siteID, string swapID, string swappeeShiftID)
        {
            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<ShiftSwapDetails>(() =>
                {
                    return ReadSwapApprovalDetailyAsync(siteID, swapID, swappeeShiftID, token).Result;

                });
            };

            m_getSwapDetailsTokenSource = new CancellationTokenSource();
            var result = await PerformOperation<ShiftSwapDetails>(m_restClient.TimeoutMs, taskGenerator, m_getSwapDetailsTokenSource.Token);

            if (result.ResponseObject != null)
                return result.ResponseObject;

            return null;
        }

        public async Task<ShiftSwapDetails> ReadSwapApprovalDetailyAsync(int siteID, string swapID, string swappeeShiftID, CancellationToken token)
        {
            //Unlike ESS, SiteMgr is 2 requests, one for Swapper and one for Swappeee (yay JDA!)
            var swapperRequest = new GetAwaitingSwapApprovalRequest ()
                {
                    siteId = siteID,
                    isSwapper = true,
                    swapID = swapID,
                    swappeeShiftID = swappeeShiftID,
                };
            
            var swappeeRequest = new GetAwaitingSwapApprovalRequest ()
                {
                    siteId = siteID,
                    isSwapper = false,
                    swapID = swapID,
                    swappeeShiftID = swappeeShiftID,
                };

            var swapperData = m_restClient.Post<GetAwaitingSwapApprovalResponse>(swapperRequest, token);
            var swappeeData = m_restClient.Post<GetAwaitingSwapApprovalResponse>(swappeeRequest, token);

            await swapperData.ConfigureAwait (false);
            token.ThrowIfCancellationRequested();
            await swappeeData.ConfigureAwait (false);
            token.ThrowIfCancellationRequested();

            if (swapperData.Result != null && swapperData.Result.Success && swapperData.Result.Object != null && swapperData.Result.Object.data != null && swapperData.Result.Object.data.Count > 0 &&
                swappeeData.Result != null && swappeeData.Result.Success && swappeeData.Result.Object != null && swappeeData.Result.Object.data != null && swappeeData.Result.Object.data.Count > 0)
                return new ShiftSwapDetails(swapperData.Result.Object.data[0], swappeeData.Result.Object.data[0]);

            return null;
        }

        public async Task<JDAResponse> UpdateSwapShiftStatus(int siteId, ShiftSwapDetails details, bool approve)
        {
            // create request with required data and change commands
            var request = new ProcessChangeCommandsSiteRequest ();

            JDAChangeCommand approval = new JDAChangeCommand ()
            {
                __TYPE = _CHANGECOMMANDTYPE,
                isApprove = approve,
                SwappeeShiftID = details.SwappeeShiftID,
                SwapID = details.SwapID,
            };

            request.siteId = siteId;
            request.jsChangeCommands = new JDAChangeCommand[]{ approval };

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ProcessChangeCommandsSiteReply>>(() =>
                        {
                            return m_restClient.Post<ProcessChangeCommandsSiteReply>(request, token).Result;
                        });
                };
                    
            m_processChangeTokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<ProcessChangeCommandsSiteReply>>(m_restClient.TimeoutMs, taskGenerator, m_processChangeTokenSource.Token);


            JDAResponse response = new JDAResponse ();
            if (result.ResponseObject != null && result.ResponseObject.Success)
            {
                if (result.ResponseObject.Object.data.Errors != null && (result.ResponseObject.Object.data.Errors.Length > 0))
                {
                    //Attempt to parse errors, we don't know them all because JDA so this will grow as we continue production
                    bool parseSuccess = true;
                    string args = result.ResponseObject.Object.data.Errors[0].Arguments.ToString ();
                    string errorType = result.ResponseObject.Object.data.Errors[0].Type;
                    if (errorType == "ScheduledShiftCannotOverlap")
                    {
                        string employeeName = GetArgValueClass<string> (args, "EmployeeName", ref parseSuccess);
                        DateTime start = GetArgValueStruct<DateTime> (args, "ShiftStart", ref parseSuccess);

                        if (parseSuccess)
                            response.ErrorMessage = string.Format ("{0} has shifts that overlap on {1}", employeeName, start.DayOfWeek);
                    }
                }
                else
                {
                    response.Success = true;
                }
            }

            return response;
        }

        public void CancelGetHistory()
        {
            m_getHistoryTokenSource.Cancel();
        }
    }
}

