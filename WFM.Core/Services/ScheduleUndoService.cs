﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleUndoService
    {
        private Stack<ScheduleUndoStackOperation> m_undoStack;

        private readonly IMvxMessenger m_messenger;

        /// <summary>
        /// Is there any history in the undo stack for the undo service to roll back to?
        /// </summary>
        /// <value><c>true</c> if this instance can undo; otherwise, <c>false</c>.</value>
        public bool CanUndo
        {
            get
            {
                return m_undoStack.Count > 0;
            }
        }

        public ScheduleUndoService(IMvxMessenger messenger)
        {
            m_messenger = messenger;

            m_undoStack = new Stack<ScheduleUndoStackOperation>();
        }

        /// <summary>
        /// Adds the undo operation to the stack.
        /// </summary>
        /// <param name="change">Change.</param>
        public void Store(ScheduleUndoStackOperation change)
        {
            m_undoStack.Push(change);
        }

        /// <summary>
        /// Wipes the history from the undo stack.
        /// </summary>
        public void ClearHistory()
        {
            m_undoStack.Clear();
        }

        /// <summary>
        /// Performs an undo operation, sending out the appropriate shifteditmessage to listeners and removing the operation from the stack.
        /// </summary>
        public void Undo()
        {
            if (m_undoStack.Count > 0)
            {
                ShiftEditMessage editOperation = GenerateReversedUndoOperation(m_undoStack.Pop());

                if(editOperation != null)
                    m_messenger.Publish<ShiftEditMessage>(editOperation);
            }
            else
                throw new InvalidOperationException("Nothing to undo");
        }

        private ShiftEditMessage GenerateReversedUndoOperation(ScheduleUndoStackOperation undoOperation)
        {
            ShiftEditMessage shiftEditMessage = new ShiftEditMessage(this);

            switch (undoOperation.OperationType)
            {
                case ShiftEditMessage.Operation.ReplaceShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.ReplaceShift;
                    shiftEditMessage.UpdateShift = undoOperation.OriginalState;
                    shiftEditMessage.EmployeeID = undoOperation.EmployeeId;
                    shiftEditMessage.RecalculateDetails = false;
                    break;
                case ShiftEditMessage.Operation.UpdateShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.ReplaceShift;
                    shiftEditMessage.UpdateShift = undoOperation.OriginalState;
                    shiftEditMessage.EmployeeID = undoOperation.EmployeeId;
					shiftEditMessage.RecalculateDetails = false;
                    break;
                case ShiftEditMessage.Operation.CreateDefaultShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.DeleteShift;
                    shiftEditMessage.ShiftID = undoOperation.OriginalState.ShiftID;
                    shiftEditMessage.EmployeeID = undoOperation.EmployeeId;
                    break;
                case ShiftEditMessage.Operation.DeleteShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.CreateShift;
                    shiftEditMessage.UpdateShift = undoOperation.OriginalState;
                    shiftEditMessage.EmployeeID = undoOperation.EmployeeId;
                    shiftEditMessage.Start = undoOperation.ContextStart;
                    shiftEditMessage.End = undoOperation.ContextEnd;
                    shiftEditMessage.RecalculateDetails = true;
                    break;
                case ShiftEditMessage.Operation.ReassignShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.ReassignShift;
                    shiftEditMessage.ShiftID = undoOperation.OriginalState.ShiftID;
                    shiftEditMessage.NewEmployeeID = undoOperation.EmployeeId;
                    shiftEditMessage.EmployeeID = undoOperation.ReassignedToEmployeeId;
                    shiftEditMessage.Start = undoOperation.OriginalState.Start;
                    shiftEditMessage.End = undoOperation.OriginalState.End;
                    shiftEditMessage.RecalculateDetails = true;
                    break;
                case ShiftEditMessage.Operation.AssignUnfilledShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.UndoFilledShift;
                    shiftEditMessage.ShiftID = undoOperation.ShiftId;
                    shiftEditMessage.EmployeeID = undoOperation.EmployeeId;
                    shiftEditMessage.RecalculateDetails = true;
                    break;
                case ShiftEditMessage.Operation.SwapShift:
                    shiftEditMessage.Op = ShiftEditMessage.Operation.ReassignShift;
                    shiftEditMessage.ShiftID = undoOperation.OriginalState.ShiftID;
                    shiftEditMessage.NewEmployeeID = undoOperation.EmployeeId;
                    shiftEditMessage.EmployeeID = undoOperation.ContextId;
                    shiftEditMessage.Start = undoOperation.ContextStart;
                    shiftEditMessage.End = undoOperation.ContextEnd;
                    shiftEditMessage.RecalculateDetails = true;
                    break;

                default:
                    return null;
            }

            shiftEditMessage.StoreInUndoStack = false;

            return shiftEditMessage;
        }
    }

    /// <summary>
    /// Represents a shift edit operation that can be reversed. Stores information needed for generating a reversal edit message.
    /// </summary>
    public class ScheduleUndoStackOperation
    {
        public ShiftEditMessage.Operation OperationType { get; private set; }

        public int ShiftId { get; private set; }

        public int ContextId { get; private set; }

        public ShiftData OriginalState { get; private set; }

        public ShiftData ContextState { get; private set; }

        public int EmployeeId { get; private set; }

        public int ReassignedToEmployeeId { get; private set; }

        public DateTime ContextStart { get; private set; }

        public DateTime ContextEnd { get; private set; }


        public ScheduleUndoStackOperation (ShiftData sourceState, ShiftData targetState, int sourceEmployeeID, int targetEmployeeID, DateTime contextStart, DateTime contextEnd, ShiftEditMessage.Operation operationType)
        {
            OriginalState = sourceState;
            ContextState = targetState;
            EmployeeId = sourceEmployeeID;
            ContextId = targetEmployeeID;
            OperationType = operationType;
            ContextStart = contextStart;
            ContextEnd = contextEnd;
        }

        public ScheduleUndoStackOperation(ShiftData originalState, ShiftEditMessage.Operation operationType, int employeeId, DateTime contextStart, DateTime contextEnd)
        {
            OriginalState = originalState;
            OperationType = operationType;
            EmployeeId = employeeId;
            ContextStart = contextStart;
            ContextEnd = contextEnd;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WFM.Core.UndoStackOperation"/> class.
        /// Used for when making modifications to a shift state. The previous state is recorded and will be used to reverse the operation.
        /// By storing the previous state it allows the undo system to simply re-apply the state over the top of the existing shift.
        /// </summary>
        /// <param name="originalState">Original state.</param>
        /// <param name="operationType">Operation type.</param>
        /// <param name="employeeId">Employee identifier.</param>
        public ScheduleUndoStackOperation(ShiftData originalState, ShiftEditMessage.Operation operationType, int employeeId)
        {
            OriginalState = originalState;
            OperationType = operationType;
            EmployeeId = employeeId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WFM.Core.UndoStackOperation"/> class.
        /// Used for when making modifications to a shift state. The previous state is recorded and will be used to reverse the operation.
        /// By storing the previous state it allows the undo system to simply re-apply the state over the top of the existing shift.
        /// </summary>
        /// <param name="originalState">Original state.</param>
        /// <param name="operationType">Operation type.</param>
        /// <param name="employeeId">Employee identifier.</param>
        /// <param name="newEmployeeId">The employee ID this shift WAS reassigned to.</param>
        public ScheduleUndoStackOperation(ShiftData originalState, ShiftEditMessage.Operation operationType, int employeeId, int newEmployeeId)
        {
            OriginalState = originalState;
            OperationType = operationType;
            EmployeeId = employeeId;
            ReassignedToEmployeeId = newEmployeeId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WFM.Core.UndoStackOperation"/> class.
        /// Used for when a new shift has been created. ID of new shift is passed in for the undo system to reverse by sending the appropriate delete message.
        /// </summary>
        /// <param name="shiftId">Shift identifier.</param>
        /// <param name="operationType">Operation type.</param>
        /// <param name="employeeId">Employee identifier.</param>
        public ScheduleUndoStackOperation(int shiftId, ShiftEditMessage.Operation operationType, int employeeId)
        {
            ShiftId = shiftId;
            OperationType = operationType;
            EmployeeId = employeeId;
        }
    }
}