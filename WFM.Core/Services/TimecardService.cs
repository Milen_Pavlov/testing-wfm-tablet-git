﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;
using System.Collections.Generic;

namespace WFM.Core
{
    public class TimecardOverviewResult
    {
        public List<EmployeeTimecardSummary> Timecards { get; set; }

        public bool WasCancelled { get; set; }

        public bool Success { get; set; }

        public TimecardOverviewResult()
        {
            Timecards = new List<EmployeeTimecardSummary>();
        }
    }

    [ConsortiumService]
    public class TimecardService : JDAService
    {
        private readonly RestClient m_restClient;
        private readonly EmployeeService m_employeeService;

        public TimecardService(RestClient restClient, EmployeeService employeeService)
        {
            m_restClient = restClient;
            m_employeeService = employeeService;
        }

        public async Task<TimecardOverviewResult> GetTimecardSummaryForWeekBeginning(int siteID, DateTime fromDate, CancellationToken ct)
        {
            TimecardOverviewResult result = new TimecardOverviewResult ();

            DateTime utcFromDate = DateTime.SpecifyKind (fromDate.Date, DateTimeKind.Unspecified);
            DateTime utcToDate = DateTime.SpecifyKind (fromDate.Date.AddDays(6), DateTimeKind.Unspecified);

            var request = m_restClient.Post<GetEmployeeTimecardSummaryResponse>(new GetEmployeeTimecardSummaryRequest()
                {
                    siteID = siteID,
                    buID = siteID,
                    startDate = utcFromDate.Date,
                    endDate = utcToDate.Date,
                });

            //Only employee IDs in the timecards, gotta do another get all lookup to get their damned names TODO: Maybe global OnSiteChange too??
            var allEmployeesRequest = m_employeeService.GetAllEmployees(siteID);

            var timecards = await request;
            var employees = await allEmployeesRequest;

            if (ct.IsCancellationRequested)
            {
                result.WasCancelled = true;
            }
            else if (timecards.Success && employees != null)
            {
                result.Success = true;
                result.Timecards = timecards.Object.data.Select(x => new EmployeeTimecardSummary(x, employees)).ToList();
                result.Timecards.OrderBy (x => x.EmployeeName);
            }

            return result;
        }

    }
}

