﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public interface IRolesService
    {
        List<ShiftDetailData> GenerateRoleDetailsForShift(List<JobData> jobs, List<ShiftDetailData> details);

        List<ShiftDetailData> UpdatedRoleDetailsForShift(ShiftData shiftData, List<ShiftDetailData> originalDetailList = null);

        void MoveRolesForJob (JobData job, TimeSpan startDelta, TimeSpan endDelta, TimeSpan sizeDelta, List<ShiftDetailData> details);
    }
}

