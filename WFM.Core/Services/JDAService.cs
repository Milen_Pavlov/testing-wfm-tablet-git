﻿using System;
using Siesta;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class JDAResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }

    public abstract class JDAService
    {
        /// <summary>
        /// Builds the result object from a rest response task.
        /// </summary>
        /// <returns>A typed result object.</returns>
        /// <param name="jdaResponseOperation">Jda response operation.</param>
        /// <typeparam name="ResultType">The type of the result to create.</typeparam>
        /// <typeparam name="ResponseType">The type of the response to build the result from.</typeparam>
        protected Result<ResultType> ConstructResultObject<ResultType,ResponseType>(Task<RestReply<ResponseType>> jdaResponseOperation) where ResponseType : JDAMessageBase, new()
        {
            Result<ResultType> result = new Result<ResultType>();

            if (jdaResponseOperation.IsCompleted)
            {
                //Have to rebuild the message if the serializer has failed to serialize to our strong type. 
                //This happens if JDA returns an error object rather than what we're expecting and siesta has a funny turn.
                if(jdaResponseOperation.Result.Code == (int)RestError.UnhandledContent)
                {
                    ParseResponseAsJDAError<ResponseType>(jdaResponseOperation);
                }

                var response = jdaResponseOperation.Result;
                if (!response.Success)
                {
                    //Rest failure
                    result.Message = response.Message;
                    result.Code = response.Code;
                    result.ErrorSource = ErrorSource.REST;
                    result.Success = false;
                }
                else
                {
                    if (response.Object.status != 0)
                    {
                        result.Message = response.Object.message ?? "Unknown Error";
                        result.Code = response.Object.status;
                        result.ErrorSource = ErrorSource.JDA;
                        result.Success = false;
                    }
                    else
                    {
                        result.Message = response.Object.message;
                        result.Code = response.Object.status;
                        result.ErrorSource = ErrorSource.JDA;
                        result.Success = true;
                    }
                }
            }
            else
            {
                //System failure
                result.Message = "Operation failed";
                result.Code = (int)RestError.UnknownError;
                result.ErrorSource = ErrorSource.REST;
                result.Success = false;
            }

            return result;
        }

        private void ParseResponseAsJDAError<ResponseType>(Task<RestReply<ResponseType>> jdaResponseOperation) where ResponseType : JDAMessageBase, new()
        {
            try
            {
                string jsonPayload = System.Text.Encoding.UTF8.GetString(jdaResponseOperation.Result.Content, 0, jdaResponseOperation.Result.Content.Length);

                var error = JsonConvert.DeserializeObject<JDAMessage<JDAError>>(jsonPayload);

                jdaResponseOperation.Result.Object = new ResponseType();
                jdaResponseOperation.Result.Object.appStatus = error.appStatus;
                jdaResponseOperation.Result.Object.count = error.count;
                jdaResponseOperation.Result.Object.message = error.message;
                jdaResponseOperation.Result.Object.status = error.status;
                jdaResponseOperation.Result.Object.type = error.type;

                //This copy happens so we can access the object from within the original stongly typed JDAMessage
                jdaResponseOperation.Result.Object.Error = error.data;

                //Technically we've worked at this point so reset the rest error as the JDA message is now restored to the original state.
                jdaResponseOperation.Result.Code = 200;
            }
            catch (Exception ex)
            {
                //TinyIoCContainer.Current.Resolve<Logger>().Error("Unable to parse JDA error response.", ex);
            }
        }
    }
}