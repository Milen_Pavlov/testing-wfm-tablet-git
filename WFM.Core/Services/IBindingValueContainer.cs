﻿using System;

namespace WFM.Core
{
    public interface IBindingValueContainer
    {
        object GetValueByName(string name, Type requestType);
    }
}