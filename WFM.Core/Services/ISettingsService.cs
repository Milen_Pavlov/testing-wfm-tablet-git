﻿namespace WFM.Core
{
    public interface ISettingsService
    {
        IConfiguration AppConfig { get;}
        AppSettings Current { get;}
    }
}