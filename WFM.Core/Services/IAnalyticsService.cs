﻿using System;

namespace WFM.Core
{
    public interface IAnalyticsService
    {
        string TrackingID { set; }

        void SetSessionParam(int paramIndex, string param);

        void TrackScreenView(string screenName);

        void TrackAction(string categoryName, string actionName); 
        void TrackAction(string categoryName, string actionName, string value); 
    }
}