﻿using Consortium.Client.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.StaticData;
using WFM.Core.Model;
using WFM.Core.RestClients;
using System.Threading;

namespace WFM.Core.Services
{
    [ConsortiumService]
    public class StaticDataService: ManagedRestService
    {
        public RestClient m_client { get; set; }

        public StaticDataService(ConsortiumRestClient client)
        {
            m_client = client;            
        }

        public async Task<StaticData> SafeReadAsync()
        {
            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<StaticData>(() =>
                        {
                            return ReadAsync().Result;
                        });
                };
                    
            var result = await PerformOperation<StaticData>(timeout, taskGenerator,new CancellationToken());

            return result.ResponseObject;
        }

        public async Task<StaticData> ReadAsync()
        {
            var request = new ReadRequest();

            var response = await m_client.Get<ReadResponse>(request);

//            if (HasOperationTimedOut)
//            {
//                return null;
//            }

            if (!response.Success || !response.Object.Success)
            {
                return null;
            }

            var staticData = new StaticData()
            {
                TillStatuses = response.
                    Object.
                    TillStatuses.
                    Select(s => new TillStatus()
                    {
                        TillStatusId = s.TillStatusId,
                        Key = s.Key,
                        Name = s.Name,
                        DisplayOrder = s.DisplayOrder,
                    }).
                    ToList(),

                TillTypes = response.
                    Object.
                    TillTypes.
                    Select(t => new TillType()
                    {
                        TillTypeId = t.TillTypeId,
                        Key = t.Key,
                        Name = t.Name,
                        DisplayOrder = t.DisplayOrder,
                        OpeningSequence = t.OpeningSequence,
                    }).
                    ToList(),
            };

            return staticData;
        }
    }
}
