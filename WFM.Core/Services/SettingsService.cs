﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using Cirrious.CrossCore;

namespace WFM.Core
{
    [ConsortiumService]
    public class SettingsService : ISettingsService
    {
        const string c_appConfigurationPath = "assets://AppConfiguration.json";
        const string c_themeAppConfigurationPath = "assets://Theme/AppConfiguration.json";
        const string c_settingsPath = "documents://Settings.json";
        const string c_environmentsPath = "assets://Theme/Environments.json";
        const string c_userEnvironmentsPath = "documents://UserEnvironments.json";
        const string c_themePath = "assets://Theme/Theme.json";

        public List<Environment> Environments { get; private set; }
        public AppSettings Current { get; private set; }
        public IConfiguration AppConfig { get; private set; }
        public ApplicationTheme AppTheme { get; private set; }

        private IFileSystem m_file;
        private IAnalyticsService m_analytics;
        private readonly ICryptographyService m_cryptographyService;
        private readonly ILogger m_logger;
        private readonly IMobileApp m_mobileApp;

        public SettingsService(IFileSystem file, 
                               IAnalyticsService analytics,
                               ICryptographyService cryptographyService,
                               ILogger logger,
                               IMobileApp mobileAppService)
        {
            m_file = file;

            m_analytics = analytics;
            m_cryptographyService = cryptographyService;
            m_logger = logger;
            m_mobileApp = mobileAppService;

            LoadAppConfiguration();
            LoadAppTheme();
            LoadEnvironments();
            LoadSettings();
        }

        void LoadEnvironments()
        {
            Environments = ReadEncryptedEnvironmentData();
 
            //Add Custom Environments
            if (m_file.Exists(c_userEnvironmentsPath))
            {
                var userEnv = m_file.ReadText(c_userEnvironmentsPath);
                Environments.AddRange(JsonConvert.DeserializeObject<List<Environment>>(userEnv));
            }

            //Strip Debug Environments
            #if !DEBUG
                Environments = Environments.Where(x => !x.DebugOnly).ToList();
            #endif
        }

        private List<Environment> ReadEncryptedEnvironmentData ()
        {
            var environments = new List<Environment> ();
            var fileContents = m_file.ReadText(c_environmentsPath);
            var decryptedFileContent = string.Empty;

            if (string.IsNullOrEmpty (fileContents))
            {
                return environments;
            }
         
            try
            {
                //trying to deserialize file as if there's no encryption. If there is we'll catch and try to decrypt.
                environments = JsonConvert.DeserializeObject<List<Environment>>(fileContents);
            }
            catch (JsonException ex)
            {
                try 
                {
                    decryptedFileContent = m_cryptographyService.Decrypt (GenerateKey (), fileContents);

                    environments = JsonConvert.DeserializeObject<List<Environment>> (decryptedFileContent);
                } 
                catch(JsonException jsonException)
                {
                    //decryption currrenly appends empty string in the beginning of the string 
                    decryptedFileContent = decryptedFileContent.Remove(0,1);
                    environments = JsonConvert.DeserializeObject<List<Environment>>(decryptedFileContent);
                }
                catch (Exception exception) 
                {
                    m_logger.Error (exception);
                }
            }
            catch(Exception ex)
            {
                m_logger.Error (ex);
            }

            return environments ?? new List<Environment>();
        }

        private string GenerateKey()
        {
            return m_mobileApp.Id + m_mobileApp.Version;
        }

        void LoadSettings()
        {
            var settings = m_file.ReadText(c_settingsPath);

            string currentEnvironmentName = null;

            if (!String.IsNullOrEmpty(settings))
            {
                if(!TryDeserializeEnvironmentDetails (settings,ref currentEnvironmentName))
                {
                    currentEnvironmentName = settings;
                } 
            }

            SetCurrentEnvironmentFromName (currentEnvironmentName);
        }

        void SetCurrentEnvironmentFromName (string currentEnvironmentName)
        {
            var matchingEnvironment = Environments.FirstOrDefault (env => env.Name == currentEnvironmentName);

            if (matchingEnvironment != null)
                SaveEnvironment (matchingEnvironment);
            else if (Environments.Count > 0)
                SaveEnvironment (Environments [0]);
            else
            {
                //No matching environment, default env is a bit pap.
                SaveEnvironment (new Environment ());
            }
        }

        //backwards compatible lookup
        static bool TryDeserializeEnvironmentDetails (string settings, ref string currentEnvironmentName)
        {
            try
            {
                var environment = JsonConvert.DeserializeObject<AppSettings> (settings);

                if(environment != null && environment.Env != null && !String.IsNullOrEmpty(environment.Env.Name))
                {
                    currentEnvironmentName = environment.Env.Name;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                //nop, seralization failure is not interesting
            }

            return false;
        }

        void LoadAppConfiguration()
        {
            var config = m_file.ReadText(c_appConfigurationPath);

            if (config != null)
            {
                JObject baseConfig = JObject.Parse (m_file.ReadText (c_appConfigurationPath));

                if(m_file.Exists(c_themeAppConfigurationPath))
                {
                    string themeAppConfig = m_file.ReadText (c_themeAppConfigurationPath);

                    if (!String.IsNullOrEmpty (themeAppConfig))
                    {
                        JObject themeConfig = JObject.Parse (themeAppConfig);

                        baseConfig.Merge (themeConfig, new JsonMergeSettings () { MergeArrayHandling = MergeArrayHandling.Union });
                    }
                }

                var configValues = new Dictionary<string, string>();
                foreach (var configEntry in baseConfig) 
                {
                    configValues.Add(configEntry.Key, configEntry.Value.ToString());
                }


                AppConfig = new ConfigurationStore();
                AppConfig.Append(configValues);   
            }
            else
            {
                AppConfig = new ConfigurationStore();
            }
        }

        void LoadAppTheme()
        {
            var config = AppConfig == null ? null : m_file.ReadText(c_themePath);

            if (config != null)
            {
                AppTheme = JsonConvert.DeserializeObject<ApplicationTheme>(config);
            }
            else
            {
                AppTheme = new ApplicationTheme();
            }
        }

        public int GetEnvironmentIndex()
        {
            if (Current.Env == null)
                return -1;

            for (int i=0; i<Environments.Count; ++i)
            {
                if (Environments[i].Address == Current.Env.Address && Environments [i].Name == Current.Env.Name)
                    return i;
            }

            return 0;
        }

        public void SaveEnvironment(Environment environment)
        {
            Current = new AppSettings ()
            {
                Env = environment
            };

            var environmentAsJson = JsonConvert.SerializeObject(environment, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
            Current.Env.Settings = JObject.Parse(environmentAsJson).ToObject<Dictionary<string, string>>();

            m_analytics.TrackingID = Current.Env.AnalyticsTrackingID;

            if (environment != null)
                m_file.WriteText (c_settingsPath, environment.Name);
            else
                m_file.WriteText (c_settingsPath, String.Empty);
        }

        public void SaveCustomEnvironments(List<Environment> envs)
        {
            try
            {
                Environments = envs;
                var custom = (Environments != null) ? Environments.Where(x => x.Editable).ToList() : null;

                if (custom != null && custom.Count > 0)
                    m_file.WriteText(c_userEnvironmentsPath, JsonConvert.SerializeObject(custom));
                else
                    m_file.Delete(c_userEnvironmentsPath);
            }
            catch(Exception e)
            {
                Mvx.Error(string.Format("Failed saving custom environments : {0}", e.Message));
            }
        }

        public List<int> IndicesForEnvironmentName(string name)
        {
            return Environments.Where(x => x.Name == name).Select(x => Environments.IndexOf(x)).ToList();
        }
    }
}
