using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Newtonsoft.Json;
using Siesta;
using Siesta.Client;
using WFM.Core.Model;
using WFM.Core.RestClients;
using WFM.Core.Services;
using WFM.Core.WFMPlus;

namespace WFM.Core
{
    [ConsortiumService]
    public class SessionData : ISessionData
    {
        public static string JDA_SESSION_COOKIE = "JDA-CSRF";
        public static string REFS_SESSION_COOKIE = "REFSSessionID";

        public static void CustomClearCookies(SiestaClient client)
        {
            var c = client.Cookies.GetCookies(client.BaseUri);
            foreach (Cookie co in c)
            {
                if(co.Name == JDA_SESSION_COOKIE || co.Name == REFS_SESSION_COOKIE)
                    co.Expires = DateTime.Now.Subtract(TimeSpan.FromDays(1));
            }
        }


        public const int ORG_UNIT_ANALYTICS_DIMENSION = 1;
        public const int ORG_COUNT_ANALYTICS_DIMENSION = 2;
        public const string SAML_ERROR = "SAMLError";
        public const string SAML_TOKEN = "rpopentoken";

        private RestClient m_client;
        private bool m_requiresReset = true;

        private RestClient Client
        {
            get
            {
                if(m_requiresReset)
                {                    
                    m_client.BaseUri = new Uri(m_essConfigurationService.Address);
                    m_client.DeserialiseOnError = true;
                    m_client.ClearAuthentication ();
                    CustomClearCookies(m_client);

                    m_requiresReset = false;
                }

                return m_client;
            }
        }

        private readonly IMvxMessenger m_messenger;
        private readonly IStoredCredentialsService m_storedCredentials;
        private readonly IUserDetailsService m_userDetailsService;
        //private readonly SettingsService m_settings;
        private readonly IESSConfigurationService m_essConfigurationService;
        private readonly IMobileApp m_mobileApp;
        private readonly IMobileDevice m_device;
        private readonly IAnalyticsService m_analytics;
        private readonly IRaygunService m_raygunService;
        private readonly IJDAConfigurationService m_jdaConfigurationService;
        private readonly ISiteMgrCredentialsService m_siteMgrCredentialsService;
        private readonly ISpinnerService m_spinnerService;

        public JDASite Site { get; set; }
        public JDAOrgUnit Org { get; set; }
        public JDAOrgUnit[] OrgUnits { get; set; }

        public StaticDataService StaticDataService { get; set; }

        private Timer m_keepAliveTimer;

        public void CancelStaticDataLoad()
        {
            //StaticDataService.Cancel();
        }

        private StaticData m_staticData;
        public async Task<StaticData> GetStaticDataAsync()
        {
            if (m_staticData == null)
            {
                m_staticData = await StaticDataService.SafeReadAsync();
            }

            return m_staticData;
        }
        
        public int TimeoutDurationInMinutes
        {
            get { return m_essConfigurationService.TimeoutMinutes; }
        }

        public bool TimeoutWarningsEnabled
        {
            get { return false; }
        }

        public int TimeoutWarningDurationInSeconds
        {
            get { return 2; }
        }

        //TODO NOTE - SSO CAN MESS UP USE OF THIS!!
        public string CurrentUsername
        {
            get
            {
                return m_storedCredentials.Username;
            }
        }

        public string CurrentUserID
        {
            get
            {
                return m_storedCredentials.UserUID;
            }
        }

        public bool CanResetPassword
        {
            get
            {
                return !String.IsNullOrWhiteSpace(m_essConfigurationService.ConsortiumAddress) && m_essConfigurationService.CanResetPassword;
            }
        }

        public SessionData(RestClient client, 
                           StaticDataService staticDataService, 
                           IMvxMessenger messenger, 
                           IStoredCredentialsService credentialStore, 
                           IUserDetailsService userDetailsService, 
                           IESSConfigurationService essConfigurationService, 
                           IMobileApp mobileApp,
                           IMobileDevice device,
                           IAnalyticsService analytics,
                           IRaygunService raygunService,
                           IJDAConfigurationService jdaConfigurationService,
                           ISiteMgrCredentialsService siteMgrCredentialsService,
                           ISpinnerService spinnerService)
        {
            m_client = client;
            m_messenger = messenger;
            m_userDetailsService = userDetailsService;
            StaticDataService = staticDataService;
            m_storedCredentials = credentialStore;
            m_essConfigurationService = essConfigurationService;
            m_mobileApp = mobileApp;
            m_device = device;
            m_analytics = analytics;
            m_raygunService = raygunService;
            m_jdaConfigurationService = jdaConfigurationService;
            m_siteMgrCredentialsService = siteMgrCredentialsService;
            m_spinnerService = spinnerService;
        }

        public Action CancelSignIn
        {
            get { return null; }
            set {  }
        }


        //DROID USES THIS FOR 2 SERVICES
        //TODO NOTE - SSO CAN MESS UP USE OF THIS!!
        //TODO - Remove this and use StoredCredentialsService, or CurrentUsername within this class
        //      Droid currently doesn't have an implementation of it. SIGH
        public string SessionUsername
        {
            get;
            set;
        }

        public static SignInResult BuildSignInResult (RestReply<string> reply)
        {
            // Check login reply assuming we have the cookie
            if (!reply.Success || reply.Object == null)
            {
                SignInFailReason reason;

                switch (reply.Code)
                {
                    case 400: 
                        reason = SignInFailReason.InvalidPassword; 
                        break;
                    case (int)RestError.NetworkError:
                        //case (int)RestError.ConnectionError:
                        reason = SignInFailReason.NetworkError;
                        break;
                    case (int)RestError.Timeout:
                        reason = SignInFailReason.Timeout;
                        break;
                    default:
                        reason = SignInFailReason.Other;
                        break;
                }

                if ((reason == SignInFailReason.Other || reason == SignInFailReason.InvalidPassword) && reply.Object != null)
                {
                    try {
                        var loginReply = JsonConvert.DeserializeObject<SessionLoginReply> (reply.Object);
                        switch (loginReply.status) {
                            case 1200: reason = SignInFailReason.InvalidPassword; break;
                            case 1201: reason = SignInFailReason.PasswordExpired; break;
                            case 1205: reason = SignInFailReason.Other; break; //< Account has been locked
                        }

                    } catch (Exception) {
                            reason = SignInFailReason.Other;
                    }
                }

                return new SignInResult () {
                    Success = false,
                    //Message = reply.Object != null ? reply.Object.StatusText : null,
                    FailReason = reason
                };
            }

            return new SignInResult ()
            {
                Success = true
            };
        }

        public async Task<bool> CheckExists(string host)
        {
            SiestaClient client = new SiestaClient (new SiestaHttpClient());
            client.DeserialiseOnError = true;
            client.BaseUri = new Uri(host);
            client.ClearAuthentication ();
            CustomClearCookies(client);

            var reply = await client.Send<SessionLoginReply>(new RestRequest() {
                Method = RestMethod.POST,
                BodyObject = new SessionLoginRequest () { loginName = string.Empty, password = string.Empty },
                Headers = new Dictionary<string, string>{ { "Accept", "*/*" }, { "Content-Type", "application/x-www-form-urlencoded" } }
            });

            // Check login reply
            return reply.Object != null;

        }

        public async Task<SignInResult> OnSignIn(RestClient client, string username, string password)
        {
            m_requiresReset = true;

            if (m_essConfigurationService.CheckForLockout)
            {
                bool isLockedOut = await CheckIsAppLockedOut ();

                if (isLockedOut)
                {
                    return new SignInResult () { FailReason = SignInFailReason.Other, Success = false };
                }
            }

            // Handle either user/pass or token based
            RestReply<string> loginReply = null;

            // If SSO is configured for this environment, only allow SSO sign in
            if (username == SAML_TOKEN) {
                loginReply = await Client.Send<string> (new RestRequest () {
                    Method = RestMethod.POST,
                    BodyObject = new SessionTokenRequest () { rpopentoken = password },
                    Headers = new Dictionary<string, string> { { "Accept", "*/*" }, { "Content-Type", "application/x-www-form-urlencoded" } }
                });
            } else {
                loginReply = await Client.Send<string> (new RestRequest () {
                    Method = RestMethod.POST,
                    BodyObject = new SessionLoginRequest () { loginName = username, password = password },
                    Headers = new Dictionary<string, string>{ { "Accept", "*/*" }, { "Content-Type", "application/x-www-form-urlencoded" } }
                });
            }

            // Check login reply
            var signInResult = BuildSignInResult (loginReply);

            if (!signInResult.Success)
            {
                return signInResult;
            }

            // Check the session cookie is there, if its missing then login failed
            string setCookie;
            if (!loginReply.Headers.TryGetValue ("Set-Cookie", out setCookie) || !setCookie.Contains (REFS_SESSION_COOKIE))
            {
                return new SignInResult () {
                    Message = "Sign in error, please ensure you have permissions to access this site",
                    FailReason = SignInFailReason.Other,
                    ReplyCode = loginReply.Code
                };
            }

            // Get the org units for this user
            var orgUnitReply = await Client.Get<OrgUnitsReply>(new OrgUnitsRequest());
            IStringService localiser = Mvx.Resolve<IStringService>();

            if (!orgUnitReply.Success)
            {
                return new SignInResult () { 
                    Success = false, 
                    Message = localiser.Get("failed_org_units_retrieve"),
                    FailReason = SignInFailReason.DataError
                };
            }

            if (orgUnitReply.Object.data.Count == 0)
            {
                return new SignInResult () { 
                    Success = false, 
                    Message =  localiser.Get("no_org_units_assigned"),
                    FailReason = SignInFailReason.DataError
                };
            }

            OrgUnits = orgUnitReply.Object.data.ToArray();
            Array.Sort (OrgUnits);

            //Load last store if it exists
            string storeIDStr = m_userDetailsService.GetValueForKey(UserDetailsUtility.StoreKey);

            int storeID = 0;
            
            if (!string.IsNullOrEmpty (storeIDStr) && int.TryParse (storeIDStr, out storeID))
            {
                //Look for it
                Org = orgUnitReply.Object.data.Find (x => x.ID == storeID);

                if (Org == null)
                    m_userDetailsService.RemoveValueForKey (UserDetailsUtility.StoreKey);
            }
            else
            {
                Org = null;
            }

            if(Org == null)
                Org = orgUnitReply.Object.data.Find (x => x.isDefault) ?? OrgUnits[0];

            // Get site
            bool setOrg = await SetOrgUnit(username, Org);

            if (!setOrg)
            {
                return new SignInResult () { 
                    Success = false, 
                    Message =  localiser.Get("failed_to_retrieve_site_details"),
                    FailReason = SignInFailReason.DataError
                };
            }

            // Successful login attempt
            SessionUsername = username;//TODO - This is droid only at the mo as Credentials service is not implemented....remove this
            SetupEnvironment();

            // Horrible, but get current user id by HTML scraping
            string siteid = Site.SiteID.ToString();
            string siteName = Site.Name;
            
            var userDetails = await m_siteMgrCredentialsService.GetCurrentUserDetails(siteName);
            
            string userUID = userDetails.UserID;
            
            if(string.IsNullOrEmpty(userUID))
                userUID = userDetails.Username;
            
            m_storedCredentials.StoreCredentials (username, password, siteid, userUID);

            //// Web request for login messes with cookies so expire the JDA-CSRF cookie (COPIED FROM WFM-DIGITAL)
            var c = Client.Cookies.GetCookies(Client.BaseUri);
            foreach (Cookie co in c)
            {
                if(co.Name == JDA_SESSION_COOKIE)
                    co.Expires = DateTime.Now.Subtract(TimeSpan.FromDays(1));
            }

            m_analytics.SetSessionParam (ORG_UNIT_ANALYTICS_DIMENSION, Org.Name);
            m_analytics.SetSessionParam (ORG_COUNT_ANALYTICS_DIMENSION, OrgUnits.Length.ToString());

            m_keepAliveTimer = ScheduledTimer.CreateRepeating(TimeSpan.FromSeconds(m_essConfigurationService.KeepAliveFreqSecs),  KeepAlive);
            var initReply = await Client.Send<object>(new RestRequest() {
                Method = RestMethod.POST,
                BodyObject = new SessionKeepAliveRequest(),
                Headers = new Dictionary<string, string>{ { "Accept", "*/*" }, { "Content-Type", "application/x-www-form-urlencoded" } }
            });

            return new SignInResult() { Success = true };
        }

        private void SetupEnvironment()
        {
            // CHECK environment
            if (String.IsNullOrWhiteSpace(m_essConfigurationService.ConsortiumAddress))
            {
                Debug.WriteLine("### FATAL ERROR: Missing consortium service address, till allocation will not work!! Please try clearing the App cache by uninstalling ###");
            }
            else
            {
                var consortiumClient = Mvx.Resolve<ConsortiumRestClient>();

                consortiumClient.BaseUri = new Uri(m_essConfigurationService.ConsortiumAddress);
                consortiumClient.SetTrustUnsignedSSL(true);

                if(m_essConfigurationService.ConsortiumUsername != null || m_essConfigurationService.ConsortiumPassword != null)
                {
                    consortiumClient.SetAuthentication(m_essConfigurationService.ConsortiumUsername, m_essConfigurationService.ConsortiumPassword, consortiumClient.BaseUri);
                }
                else
                {
                    Debug.WriteLine("### FATAL ERROR: Missing consortium service credentials, till allocation will not work!! Please try clearing the App cache by uninstalling ###");
                }
            }
        }

		public async Task OnSignOut (RestClient client, bool isTimeOut)
		{
            IStringService localiser = Mvx.Resolve<IStringService>();
            var spinner = m_spinnerService.Show(localiser.Get("signing_out_ellipsis"));

            var result = await InvalidateJDASession ();

            if (!result)
            {
                Debug.WriteLine ("Error invalidating JDA Session");
            }

            SessionUsername = null;

            if (!isTimeOut)
            {
                m_storedCredentials.ClearCredentials ();   
            }

            m_raygunService.ClearUserInfo ();
                
            ScheduledTimer.KillTimer(m_keepAliveTimer);

            m_requiresReset = true;
            m_spinnerService.Dismiss(spinner);
		}

        public async void SetOrgUnitAsync(JDAOrgUnit org)
        {
            await SetOrgUnit(SessionUsername, org);
        }

        //TODO: Refactor all this stuff, its not sane
        public async Task<bool> SetOrgUnit(string username, JDAOrgUnit org)
        {
            // Set the org unit and notify listers of this event
            m_messenger.Publish (new SetOrgBeginMessage (this));
            var siteRequest = Client.Post<SiteReply>(new SiteRequest() { siteID = org.ID });

            //Comfort Break TODO: Remove, shouldnt be done in the service
            await Task.Delay (1000);

            var siteReply = await siteRequest;

            // Assuming unhandled content is a session timeout but may need to refine this later
            if (siteReply.Code == (int)RestError.UnhandledContent)
            {
                m_messenger.Publish (new SetOrgEndMessage (this, false) { SessionTimeout = true });
                return false;
            }
            else if (!siteReply.Success || siteReply.Object.data.Count == 0 || siteReply.Object.data[0] == null)
            {
                m_messenger.Publish (new SetOrgEndMessage (this, false));
                return false;
            }

            Site = siteReply.Object.data[0];

            //Fetch new JDA config for current user in current site
            await UpdateJDAConfigValues();

            string env = "unknown";
            var settings = Mvx.Resolve<SettingsService> ();
            if (settings != null && settings.Current!=null && settings.Current.Env!=null)
                env = settings.Current.Env.Address;

            m_raygunService.SetCurrentUserInfo (username, Site.SiteID.ToString (), Site.Name, env );

            m_storedCredentials.UpdateSiteID(Site.SiteID.ToString());
            m_userDetailsService.SetKeyValue (UserDetailsUtility.StoreKey, Site.SiteID.ToString ());
            m_messenger.Publish (new SetOrgEndMessage(this, true));

            return true;
        }

        public async void KeepAlive()
        {
            await Client.Send<SessionKeepAliveRequest>(new RestRequest() {
                Method = RestMethod.POST,
                BodyObject = new SessionKeepAliveRequest () { siteID =  Site.SiteID }
            });
        }

        public async Task<ChangePasswordResponse> ChangePassword(string oldPassword, string newPassword, string confirmPassword)
        {
            var request = new ChangePasswordRequest()
            {
                oldPwd = oldPassword, 
                newPwd = newPassword,
                confirmPwd = confirmPassword
            };

            if (newPassword != confirmPassword)
            {
                return new ChangePasswordResponse()
                {
                    Success = false,
                    Message = "Passwords do not match"
                };
            }
            else
            {
                RestReply<JDAChangePasswordResponse> response = await Client.Send<JDAChangePasswordResponse>(
                                                                new RestRequest()
                    {
                        Method = RestMethod.POST,
                        BodyObject = request,
                        Headers = new Dictionary<string, string>{ { "Accept", "*/*" }, { "Content-Type", "application/x-www-form-urlencoded" } }
                    });

                return BuildPasswordResponse(response);
            }
        }

        ChangePasswordResponse BuildPasswordResponse(RestReply<JDAChangePasswordResponse> response)
        {
            var result = new ChangePasswordResponse();

            if (response.Object != null)
            {
                switch (response.Object.status)
                {
                    case 0: 
                        result.Success = true;
                        break;
                    case 1302: 
                        result.Success = false;
                        result.Message = "Incorrect Password";
                        break;
                    case 1303: 
                        result.Success = false;
                        result.Message = "Password is too short";
                        break;
                    case 1305: 
                        result.Success = false;
                        result.Message = "Passwords do not match";
                        break;
                    case 1307:  
                        result.Success = false;
                        result.Message = "Password too recent";
                        break;
                    default:
                        result.Success = false;
                        result.Message = "Unknown error";
                        break;
                }
            }
            else
            {
                result.Success = false;
                result.Message = "Operation failed";
            }

            return result;
        }


        public async Task<ResetPasswordResult> ResetPassword(string username)
        {
            SetupEnvironment ();

            var client = Mvx.Resolve<ConsortiumRestClient>();

            var reply = await client.Post<ResetPasswordResponse>(
                new ResetPasswordRequest() { Username = username });

            if (reply.Success)
            {
                return new ResetPasswordResult() { Success = true };
            }
            else
            {
                return new ResetPasswordResult()
                {
                    Success = false,
                    Message = reply.Message
                };
            }
        }

        public async Task<bool> CheckIsAppLockedOut()
        {
            SetupEnvironment ();

            var request = new GetAppRequest()
                {
                    AppId = m_mobileApp.Id.ToLower(), //Convert to lower to fix a bug on the app manager where its treating app ids as case sensitive but not respecting the case of the bundle id.
                    Platform = GetPlatformString()
                };

            if (!string.IsNullOrEmpty (m_essConfigurationService.LockoutBundleOverride))
            {
                request.AppId = m_essConfigurationService.LockoutBundleOverride.ToLower();
            }

            var client = Mvx.Resolve<ConsortiumRestClient>();

            var response = await client.Get<GetAppResponse>( request );

            if( response.Success )
            {
                if( response.Object != null && response.Object.App != null 
                    && response.Object.DownloadUrl != null )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public string GetPlatformString()
        {
            string platform = string.Empty;

            if (m_device.Platform == Platform.Android)
                platform = "androidos";
            else if (m_device.Platform == Platform.IOS)
                platform = "iphoneos";

            return platform;
        }

        private async Task<bool> InvalidateJDASession ()
        {
            var request = m_client.Get<string>(new SessionLogoutRequest());

            var response = await request;

            return response.Success;
        }

        public async Task UpdateJDAConfigValues()
        {
            await m_jdaConfigurationService.LoadJDAConfig(Client, Site.SiteID);
        }
    }
}