﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using Consortium.Client.Core;
using Cirrious.CrossCore.IoC;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace WFM.Core
{
    public abstract class ManagedRestService
    {
        public class ManagedRestResponse<T>
        {
            public bool Cancelled { get; set; }
            public bool TimeOut { get; set; }
            public T ResponseObject { get; set; }
        }

        public enum ContinueState
        {
            Working,
            Cancel,
            Continue,
            Retry,
            Completed
        };

        /// <summary>
        /// Note: This cancellation token is not the same as the token recieved by the managed rest service.
        /// This token is for the caller to cancel the operation as a whole.
        /// </summary>
        public delegate Task TaskGenerator(CancellationToken t); 

        /// <summary>
        /// Gets or sets a value indicating whether this instance has been cancelled by siesta.
        /// This is the secret shame of the MRS, without this we've been unable to detect internal cancelation.
        /// Maybe passing a CTS in and cancelling that would be better.
        /// </summary>
        /// <value><c>true</c> if this instance has been cancelled by siesta; otherwise, <c>false</c>.</value>
        public bool CancelledBySiesta
        {
            get;
            set;
        }

        [MvxInject]
        public IAlertBox Alert
        {
            get;
            set;
        }

        private SemaphoreSlim m_semaphore = new SemaphoreSlim(1);
        private CancellationTokenSource m_tokenSource;

        protected AlertBoxToken m_currentAlertToken = null;

        public async Task<ManagedRestResponse<T>> PerformOperation<T>(
            int timeoutMs, 
            TaskGenerator taskGenerator,
            CancellationToken token) 
            where T : class
        {
            try
            {
            await m_semaphore.WaitAsync();

            bool isTimeout = false;
            bool isExternallyCancelled = false;
            ContinueState cs = ContinueState.Working;

            // Create and start initial task
            m_tokenSource = new CancellationTokenSource();

            Task<T> task = (Task<T>)taskGenerator(m_tokenSource.Token);
            task.Start();

            do
            {
                CancelledBySiesta = false;

                if (cs == ContinueState.Retry) // Retry task
                {
                    m_tokenSource = new CancellationTokenSource();

                    task = (Task<T>)taskGenerator(m_tokenSource.Token);
                    task.Start();
                }
                    
                Task delay = Task.Delay(timeoutMs, token);

                await Task.WhenAny(new Task[] { task, delay });

                if (delay.IsCanceled)
                {
                    //Terminate the internal operation, if used.
                    m_tokenSource.Cancel();

                    cs = ContinueState.Cancel;
                                       
                    isExternallyCancelled = true;
                }
                //Else if the internal task has cancelled itself (for example if a rest request has failed.
                else if (m_tokenSource.IsCancellationRequested)
                {
                    cs = await ShowCancelledBySiestaAlert();

                    isTimeout = true;
                }
                //Else if the timeout has finished but the task has not, the request is considered to have timed out.
                else if (!task.IsCompleted)
                { 
                    cs = await ShowTimeOutAlert();

                    if (cs == ContinueState.Cancel)
                        m_tokenSource.Cancel();

                    isTimeout = true;
                }
                else
                {
                    cs = ContinueState.Completed;
                    m_semaphore.Release();
                    return new ManagedRestResponse<T>() { ResponseObject = task.Result, TimeOut = false, Cancelled = false };
                }
            }
            while(cs == ContinueState.Continue || cs == ContinueState.Retry);

            //This is a timeout
            return new ManagedRestResponse<T>() { ResponseObject = null, TimeOut = isTimeout, Cancelled = isExternallyCancelled };
            }
            finally
            {
                m_semaphore.Release();
            }
        }

        async Task<ContinueState> ShowTimeOutAlert()
        {
            var tcs = new TaskCompletionSource<ContinueState>();

            m_currentAlertToken = Alert.ShowOptions("Network connection very slow.", string.Empty, 
                new List<string>(){"Retry Operation", "Cancel Operation"}, 
                "Continue Waiting", (option) => {
                    if(option == 1)
                    {
                        tcs.SetResult(ContinueState.Retry);
                    }
                    else if(option == 2)
                    {
                        tcs.SetResult(ContinueState.Cancel);
                    }
                    else
                    {
                        tcs.SetResult(ContinueState.Continue);
                    }
                }
            );

            return await tcs.Task;
        }

        async Task<ContinueState> ShowCancelledBySiestaAlert()
        {
            var tcs = new TaskCompletionSource<ContinueState>();

            m_currentAlertToken = Alert.Show("Operation Failed", "Request timed timed out.","Retry", "Cancel", 
                (option) => {
                    if(option == 0)
                    {
                        tcs.SetResult(ContinueState.Cancel);
                    }
                    else 
                    {
                        tcs.SetResult(ContinueState.Retry);
                    }
                }
            );

            return await tcs.Task;
        }

        public T GetArgValueStruct<T>(string args, string name, ref bool success) where T : struct
        {
            //Json serialize
            JContainer argsObj = JsonConvert.DeserializeObject<JContainer> (args);
            T obj = new T();
            try
            {
                foreach (JToken token in argsObj)
                {
                    if (token.Path == name)
                    {
                        var valToken = token.Value<JProperty> ();
                        obj = valToken.Value.ToObject<T>();
                    }
                }

            }
            catch (Exception)
            {
                success = false;
            }

            return obj;
        }

        public T GetArgValueClass<T>(string args, string name, ref bool success) where T : class
        {
            //Json serialize
            JContainer argsObj = JsonConvert.DeserializeObject<JContainer> (args);
            T obj = null;
            try
            {
                foreach (JToken token in argsObj)
                {
                    if (token.Path == name)
                    {
                        var valToken = token.Value<JProperty> ();
                        obj = valToken.Value.ToObject<T>();
                    }
                }

            }
            catch (Exception)
            {
                success = false;
            }

            return obj;
        }
    }
}