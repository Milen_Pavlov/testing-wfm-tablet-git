﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    [ConsortiumService]
    public class EmployeeService : IEmployeeService
    {
        private readonly RestClient m_restClient;

        public EmployeeService(RestClient restClient)
        {
            m_restClient = restClient;
        }

        /// <summary>
        /// Gets all employee contact info
        /// </summary>
        /// <returns>All Employee details.</returns>
        /// <param name="siteID">Site ID</param>
        /// <param name="effectiveTimestamp">Start date.</param>
        public async Task<JDAEmployeeInfo[]> GetAllEmployees(int siteID, DateTime? effectiveTimestamp = null)
        {
            var request = m_restClient.Post<GetAllEmployeeContactInfoResponse>(new GetAllEmployeeContactInfoRequest()
                {
                    siteID = siteID,
                    effectiveTimestamp = effectiveTimestamp,
                });

            var result = await request;

            if (result.Success)
            {
                return result.Object.data;
            }
            else
            {
                return null;
            }
        }
        
        public async Task<JDAEmployeeInfo[]> GetEmployeesByID(int siteID, IEnumerable<int> employeeIds)
        {           
            var result = await m_restClient.Post<GetAllEmployeeContactInfoResponse>(new EmployeeContactInfoRequest()
            {
                siteID = siteID,
                employeeIDs = employeeIds.ToArray()
            });

            if (result.Success && result.Object != null && result.Object.data != null)
            {
               return result.Object.data;
            }

            return new JDAEmployeeInfo[0];
        }
    }
}

