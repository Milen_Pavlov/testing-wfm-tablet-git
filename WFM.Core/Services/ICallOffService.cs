﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WFM.Core
{
    public interface ICallOffService
    {
        Task<Result> CallOffDays (int siteId, int employeeId, IEnumerable<DateTime> daysToCallOff, string holidayType);
        Task<Result> CallOffShift (int siteId, int employeeId, DateTime shiftStartTime, DateTime shiftEndTime, string holidayType);
    }
}