﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    [ConsortiumService]
    public class TimeOffService : JDAService, ITimeOffService
    {        
        private const string c_previousListFilePath = "documents://{0}.previousPendingTORCount.json";
        private const string c_lastNotificationCountFilePath = "documents://{0}.lastPendingTORCount.txt";

        private readonly RestClient m_restClient;
        private readonly IFileSystem m_fileSystem;
        private readonly IMvxMessenger m_messenger;
        private readonly IStringService m_localiser;
        
        private PendingTORUpdatedMessage m_lastPublishedMessage;

        public TimeOffService(RestClient restClient, IFileSystem fileSystem, IMvxMessenger messenger, IStringService localiser)
        {
            m_restClient = restClient;
            m_fileSystem = fileSystem;
            m_messenger = messenger;
            m_localiser = localiser;
        }

        //Notification count shiz
        public int GetPreviousNotificationCount(int siteID)
        {
            if (m_lastPublishedMessage != null)
            {
                return m_lastPublishedMessage.Total;
            }
            else
            {
                int cachedNotificationCount = 0;

                string filePath = string.Format (c_lastNotificationCountFilePath, siteID);

                if (m_fileSystem.Exists (filePath))
                {
                    string text = m_fileSystem.ReadText (filePath);

                    int.TryParse (text, out cachedNotificationCount);
                }

                return cachedNotificationCount;
            }
        }
        public async Task UpdatePendingTORCount(int siteID)
        {
            await GetAllPendingTimeOffRequests (siteID);
        }

        public async Task<JDATimeOffRequest[]> GetAllPendingTimeOffRequests(int siteID)
        {
            var request = m_restClient.Post<GetTimeOffResponse>(new GetTimeOffRequest()
                {
                    EmployeeIDFilter = 0,
                    buid = siteID,
                    dir = string.Empty,
                    includeApproved = false,
                    includeCanceled = false,
                    includeDenied = false,
                    includeRequested = true,
                    sort = string.Empty,
                    startTime = DateTime.Now,
                    endTime = DateTime.Now.AddYears(100)
                });

            var result = await request;

            if (result.Success)
            {
                PublishPendingTORNotificationCount(siteID, result.Object.data.TotalCount);
                return result.Object.data.Requests;
            }
            else
            {
                return null;
            }
        }

        private void PublishPendingTORNotificationCount(int siteID, int notificationCount)
        {
            m_lastPublishedMessage = new PendingTORUpdatedMessage (this) { Total = notificationCount };

            m_messenger.Publish(m_lastPublishedMessage);

            m_fileSystem.WriteText (string.Format (c_lastNotificationCountFilePath,siteID), notificationCount.ToString ());
        }

        /// <summary>
        /// Gets all time requests of all state for a site between certain dates
        /// </summary>
        /// <returns>The all time requests.</returns>
        /// <param name="siteID">Site ID</param>
        /// <param name="startDate">Start date.</param>
        /// <param name="endDate">End date.</param>
        public async Task<JDATimeOffRequest[]> GetAllTimeOffRequests(int siteID, DateTime startDate, DateTime endDate)
        {
            var request = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = -1,
                    buid = siteID,
                    dir = string.Empty,
                    includeApproved = true,
                    includeCanceled = false,
                    includeDenied = true,
                    includeRequested = true,
                    sort = string.Empty,
                    startTime = startDate,
                    endTime = endDate
                });

            var result = await request;

            if (result.Success)
            {
                return result.Object.data.Requests;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<JDATimeOffRequest>> GetTimeOffHistoryRequest(int siteID, JDAEmployeeInfo info)
        {
            DateTime currentDate = DateTime.Now.Date;
            var requestsOperation = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = info.EmployeeID,
                    buid = siteID,
                    dir = "desc", 
                    includeApproved = true,
                    includeCanceled = true,
                    includeDenied = true,
                    includeRequested = true,
                    sort = "Start",
                    startTime = currentDate.AddMonths(-3)
                });

            var history = new List<JDATimeOffRequest>();
            var requestsResult = await requestsOperation;
            if (requestsResult.Success)
            {
                foreach (var request in requestsResult.Object.data.Requests)
                {
                    if(info!=null && request.EmployeeID == info.EmployeeID)
                        history.Add(request);
                }
            }
    
            return history;
        }

        /// <summary>
        /// For a given employee, and for a given site, return the detailed employee info used for building the VM for the time off detail page employee view.
        /// </summary>
        /// <returns>The detailed time off request or null if there was an issue in retrieving any of the details.</returns>
        /// <param name="siteID">Site ID</param>
        /// <param name="forRequest">For the time off request selected.</param>
        public async Task<DetailedTimeOffModel> GetDetailedEmployeeRequest(int siteID, JDAEmployeeInfo info)
        {          
            DateTime currentDate = DateTime.Now.Date;

            var employeeAccrualsOperation = m_restClient.Post<EmployeeAccrualsResponse>(new EmployeeAccrualsRequest()
                {
                    businessDate = currentDate,
                    employeeID = info.EmployeeID,
                });

            var requestsOperation = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = info.EmployeeID,
                    buid = siteID,
                    dir = "desc", 
                    includeApproved = true,
                    includeCanceled = true,
                    includeDenied = true,
                    includeRequested = true,
                    sort = "Start",
                    startTime = currentDate.AddMonths(-3)
                });

            var requestsResult = await requestsOperation;
            var accrualResponse = await employeeAccrualsOperation;

            if (requestsResult.Success && accrualResponse.Success)
            {
                return new DetailedTimeOffModel(null, null, null, accrualResponse.Object.data, requestsResult.Object.data.Requests, null, info);
            }
            else
            {
                return null;
            }
        }

        public async Task<List<JDAAccrual>> GetAccruals(DateTime date, int employeeID)
        {
            var employeeAccrualsOperation = m_restClient.Post<EmployeeAccrualsResponse>(new EmployeeAccrualsRequest()
                {
                    businessDate = date.Date,
                    employeeID = employeeID
                });

            var employeeAccrualsResult = await employeeAccrualsOperation;

            if (employeeAccrualsResult.Success)
            {
                return employeeAccrualsResult.Object.data.ToList();
            }

            return new List<JDAAccrual> ();
        }

        public async Task<JDATimeOffRequest[]> GetHistory(int siteID, int employeeID, DateTime date)
        {
            var otherRequests = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = employeeID,
                    buid = siteID,
                    dir = "desc", 
                    includeApproved = true,
                    includeCanceled = true,
                    includeDenied = true,
                    includeRequested = true,
                    sort = "Start",
                    startTime = date.Date.AddMonths(-3)
                });

            var listOtherRequests = await otherRequests;

            if (listOtherRequests.Success)
            {
                return listOtherRequests.Object.data.Requests;
            }

            return new JDATimeOffRequest[0];
        }

        public async Task<List<PeerRequest>> GetPeerRequests(int siteID, int employeeID, DateTime start, DateTime end)
        {
            var peerRequestsOperation = m_restClient.Post<PeerRequestsResponse>(new PeerRequestsRequest()
                {
                    employeeID = employeeID,
                    siteID = siteID,
                    startSiteLocalTimestamp = start.Date,
                    endSiteLocalTimestamp = end.Date
                });

            var peerTimeOffRequests = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = -1,
                    buid = siteID,
                    dir = "desc",
                    includeApproved = true,
                    includeCanceled = true,
                    includeDenied = true,
                    includeRequested = true,
                    sort = "Start",
                    endTime = end.Date.AddDays(7),
                    startTime = start.Date.AddDays(-7)
                });

            var peerRequestResult = await peerRequestsOperation;
            var peerTimeOff = await peerTimeOffRequests;

            if (peerRequestResult.Success && peerTimeOff.Success)
            {
                List<PeerRequest> requests = new List<PeerRequest> ();
                foreach (var pr in peerRequestResult.Object.data)
                {
                    requests.Add (new PeerRequest (pr, peerTimeOff.Object.data.Requests));
                }

                return requests;
            }

            return new List<PeerRequest> ();
        }

        public async Task<List<JDATimeOffDetail>> GetNewTimeOffDetails(int siteID, int employeeID, DateTime start, DateTime end, int typeID)
        {
            var detailsOperation = m_restClient.Post<GenerateNewTimeOffRequestDetailsForTypeChangeResponse>(new GenerateNewTimeOffRequestDetailsForTypeChangeRequest()
                {
                    siteID = siteID,
                    employeeID = employeeID,
                    start = DateTime.SpecifyKind(start,DateTimeKind.Utc),
                    end = DateTime.SpecifyKind(end,DateTimeKind.Utc),
                    timeOffRequestID = -1,
                    timeOffTypeID = typeID,
                });

            var detailsResult = await detailsOperation;

            if (detailsResult.Success)
            {
                return detailsResult.Object.data.ToList();
            }

            return new List<JDATimeOffDetail> ();
        }

        /// <summary>
        /// For a given request, and for a given site, return the detailed request used for building the VM for the time off detail page.
        /// </summary>
        /// <returns>The detailed time off request or null if there was an issue in retrieving any of the details.</returns>
        /// <param name="siteID">Site ID</param>
        /// <param name="forRequest">For the time off request selected.</param>
        public async Task<DetailedTimeOffModel> GetDetailedTimeOffRequest(int siteID, JDATimeOffRequest forRequest)
        {
            var payAllocationOperation = m_restClient.Post<PayAllocationResponse>(new PayAllocationRequest()
                {
                    businessDate = forRequest.Start.Date,
                    employeeID = forRequest.EmployeeID.Value
                });

            var peerRequestsOperation = m_restClient.Post<PeerRequestsResponse>(new PeerRequestsRequest()
                {
                    employeeID = forRequest.EmployeeID.Value,
                    siteID = siteID,
                    startSiteLocalTimestamp = forRequest.Start.Date,
                    endSiteLocalTimestamp = forRequest.End.Date
                });
                        
            var employeeAccrualsOperation = m_restClient.Post<EmployeeAccrualsResponse>(new EmployeeAccrualsRequest()
            {
                    businessDate = forRequest.Start.Date,
                    employeeID = forRequest.EmployeeID.Value
            });

            var peerTimeOffRequests = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = -1,
                    buid = siteID,
                    dir = "desc",
                    includeApproved = true,
                    includeCanceled = true,
                    includeDenied = true,
                    includeRequested = true,
                    sort = "Start",
                    endTime = forRequest.End.Date.AddDays(7),
                    startTime = forRequest.Start.Date.AddDays(-7)
                });

            var otherRequests = m_restClient.Post<ListTimeOffResponse>(new ListTimeOffRequest()
                {
                    EmployeeIDFilter = forRequest.EmployeeID.Value,
                    buid = siteID,
                    dir = "desc", 
                    includeApproved = true,
                    includeCanceled = true,
                    includeDenied = true,
                    includeRequested = true,
                    sort = "Start",
                    startTime = forRequest.Start.Date.AddMonths(-3)
                });

            var detailRequest = m_restClient.Post<TimeOffResponse>(new TimeOffRequest()
                {
                    employeeId = forRequest.EmployeeID.Value,
                    siteID = siteID,
                    timeOffRequestID = forRequest.TimeOffID.Value,
                });

            //var employeeRequest = m_restClient.Post<GetEmployeeD

            var payAllocationResult = await payAllocationOperation;
            var peerRequestResult = await peerRequestsOperation;
            var accrualResponse = await employeeAccrualsOperation;
            var listOtherRequests = await otherRequests;
            var detailedTimeOffRequest = await detailRequest;
            var peerTimeOff = await peerTimeOffRequests;

            if (payAllocationResult.Success && peerRequestResult.Success && listOtherRequests.Success && detailedTimeOffRequest.Success && accrualResponse.Success && peerTimeOff.Success)
            {
                return new DetailedTimeOffModel(payAllocationResult.Object.data, peerRequestResult.Object.data,peerTimeOff.Object.data.Requests, accrualResponse.Object.data, listOtherRequests.Object.data.Requests, detailedTimeOffRequest.Object.data);
            }
            else
            {
                return null;
            }
        }

        public async Task<Result<JDAProcessingResult>> ApproveHolidayRequest(JDADetailedTimeOffRequest timeoffreq, string comment = "", List<PayAllocation> allocationsChanged = null)
        {
            List<JDATimeOffDetail> timeOffDetails = new List<JDATimeOffDetail>(); 

            if (allocationsChanged != null)
            {
                foreach (var kvp in allocationsChanged)
                {
                    timeOffDetails.Add (new JDATimeOffDetail ()
                    {
                        TimeOffRequestDetailID = kvp.JDADetail.ID,
                        Date = kvp.Date,
                        NewHours = kvp.PaidHours,
                        ChangeHours = kvp.PaidHours - kvp.JDADetail.PaidHours.GetValueOrDefault(0),
                        AllowPayAdjustmentWarningThresholdViolation = false,
                        AdjustmentCategoryID = kvp.JDADetail.PayAdjustmentID.GetValueOrDefault(),
                        PayRuleID = kvp.JDADetail.PayRuleID.GetValueOrDefault(),
                        CategoryDefinitionID = kvp.JDADetail.CategoryDefinitionID.GetValueOrDefault(),
                    });
                }
            }

            var request = m_restClient.Post<ProcessChangeCommandsReply>(new ProcessChangeCommandsRequest()
            {
                //TODO What should the bools be?
                jsChangeCommands = new JDAChangeCommand[]{
                    new JDAApproveTimeOffRequestCommand(){
                        AllowAccrualBalanceViolation = false,
                        AllowBlackoutPeriodOverlap = false,
                        AllowTimeOffRequestLongerThanClientSetting = false,
                        Comment = comment,
                        Details = timeOffDetails.ToArray(),//timeoffreq.Details, SiteMgr site appears to just send nothing....odd, doesn't work if we set the Details
                        EmployeeID = timeoffreq.EmployeeID,
                        End = timeoffreq.End,
                        IsInClosedPayPeriod = false,
                        IsTimeOffTypeDirty = false,
                        Start = timeoffreq.Start,
                        TimeOffRequestID = timeoffreq.ID,
                        TimeOffTypeID = timeoffreq.TypeID,
                    },
                },
            });

            await request;

            //We make a cachabe result really just to handle the errors
            Result<JDAProcessingResult> result = ConstructResultObject<JDAProcessingResult,ProcessChangeCommandsReply>(request);
            result = ParseJDAErrorType(result);
            
            if (result.Success && request.Result.Object.data != null)
            {
                result.Object = request.Result.Object.data;

                if (result.Object.Errors != null && result.Object.Errors.Length > 0)
                {
                    result.Success = false;
                    result = ParseJDAErrorType(result);
                }
            }

            return result;
        }

        public async Task<Result<JDAProcessingResult>> DenyHolidayRequest(JDADetailedTimeOffRequest timeoffreq, string comment = "")
        {
            var request = m_restClient.Post<ProcessChangeCommandsReply>(new ProcessChangeCommandsRequest()
            {
                //TODO What should the bools be?
                jsChangeCommands = new JDAChangeCommand[]{
                    new JDADenyTimeOffRequestCommand(){
                        Comment = comment,
                        TimeOffRequestID = timeoffreq.ID,
                    },
                },
            });

            await request;

            //We make a cachabe result really just to handle the errors
            Result<JDAProcessingResult> result = ConstructResultObject<JDAProcessingResult,ProcessChangeCommandsReply>(request);
            result = ParseJDAErrorType(result);
            
            if (result.Success && request.Result.Object.data != null)
            {
                result.Object = request.Result.Object.data;

                if (result.Object.Errors != null && result.Object.Errors.Length > 0)
                {
                    result.Success = false;
                    result = ParseJDAErrorType(result);
                }
            }

            return result;
        }

        public async Task<Result<JDAProcessingResult>> CancelHolidayRequest(JDADetailedTimeOffRequest timeoffreq, string comment = "")
        {
            var request = m_restClient.Post<ProcessChangeCommandsReply>(new ProcessChangeCommandsRequest()
            {
                //TODO What should the bools be?
                jsChangeCommands = new JDAChangeCommand[]{
                    new JDACancelTimeOffRequestCommand(){
                        Comment = comment,
                        TimeOffRequestID = timeoffreq.ID,
                    },
                },
            });

            await request;

            //We make a cachabe result really just to handle the errors
            Result<JDAProcessingResult> result = ConstructResultObject<JDAProcessingResult,ProcessChangeCommandsReply>(request);
            result = ParseJDAErrorType(result);
            
            if (result.Success && request.Result.Object.data != null)
            {
                result.Object = request.Result.Object.data;

                if (result.Object.Errors != null && result.Object.Errors.Length > 0)
                {
                    result.Success = false;
                    result = ParseJDAErrorType(result);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<JDATimeOffType>>> GetTimeOffTypesForEmployee(int employeeId)
        {
            var request = m_restClient.Post<TimeOffTypesForEmployeeResponse>(new TimeOffTypesForEmployeeRequest()
                {
                    businessDate = DateTime.SpecifyKind (DateTime.UtcNow.Date, DateTimeKind.Unspecified),
                    employeeID = employeeId
                });

            await request;

            //We make a cachabe result really just to handle the errors
            Result<IEnumerable<JDATimeOffType>> result = ConstructResultObject<IEnumerable<JDATimeOffType>,TimeOffTypesForEmployeeResponse>(request);
            if (result.Success && request.Result.Object.data != null)
            {
                result.Object = request.Result.Object.data;
            }

            return result;
        }


        public async Task<Result<JDAProcessingResult>> CreateAndApproveHoliday(int siteID, int employeeID, DateTime start, DateTime end, int timeOffTypeID, string comment = "", List<PayAllocation> allocationChanges = null, bool allowBlackout = false)
        {
            //Build allocationchanges if any
            List<JDACreateTimeOffRequestUpdatedHours> allocations = new List<JDACreateTimeOffRequestUpdatedHours> ();
            if (allocationChanges != null && allocationChanges.Count > 0)
            {
                foreach (PayAllocation change in allocationChanges)
                {
                    allocations.Add (new JDACreateTimeOffRequestUpdatedHours ()
                        {
                            AllowPayAdjustmentWarningThresholdViolation = false,
                            CategoryDefinitionID = (change.JDADetail.CategoryDefinitionID.HasValue) ? change.JDADetail.CategoryDefinitionID.Value.ToString() : string.Empty,
                            PayRuleID = (change.JDADetail.PayRuleID.HasValue) ? change.JDADetail.PayRuleID.Value.ToString() : string.Empty,
                            Date = change.Date,
                            NewHours = change.PaidHours,
                            NewPayAdjustmentID = change.JDADetail.PayAdjustmentID.GetValueOrDefault(),
                            AdjustmentCategoryID = (change.JDADetail.PayAdjustmentID.HasValue) ? change.JDADetail.PayAdjustmentID.Value.ToString() : string.Empty,
                        });
                }
            }

            var request = m_restClient.Post<ProcessChangeCommandsReply>(new ProcessChangeCommandsSiteRequest()
                {
                    //TODO What should the bools be?
                    siteId = siteID,
                    jsChangeCommands = new JDAChangeCommand[]{
                        new JDACreateAndApproveTimeOffRequestCommand(){
                            AllowAccrualBalanceViolation = false,
                            AllowBlackoutPeriodOverlap = allowBlackout,
                            AllowTimeOffRequestLongerThanClientSetting = false,
                            Comment = comment,
                            EmployeeID = employeeID,
                            End = DateTime.SpecifyKind(end, DateTimeKind.Unspecified),
                            Start = DateTime.SpecifyKind(start, DateTimeKind.Unspecified),
                            TimeOffRequestID = -1,
                            TimeOffTypeID = timeOffTypeID,
                            UpdateHours = allocations.ToArray(),
                        },
                    },
                });

            await request;

            //We make a cachable result really just to handle the errors
            Result<JDAProcessingResult> result = ConstructResultObject<JDAProcessingResult,ProcessChangeCommandsReply>(request);
            

            if (result.Success && request.Result.Object.data != null)
            {
                result.Object = request.Result.Object.data;

                if (result.Object.Errors != null && result.Object.Errors.Length > 0)
                {
                    int code = 0;

                    if (result.Object.Errors[0].Value != null)
                    {
                        int.TryParse(result.Object.Errors[0].Value.ToString(), out code);
                    }

                    result.Code = code;
                    result.Message = result.Object.Errors[0].Type;
                    result.Success = false;
                }
            }

            return result;
        }
        
        /// <summary>
        /// Processes the JDA Error.
        /// </summary>
        /// <returns>The JDA Error.</returns>
        /// <param name="result">Result.</param>
        private Result<JDAProcessingResult> ParseJDAErrorType(Result<JDAProcessingResult> result)
        {
            if(!result.Success)
            {
                string errorType = "UNKNOWN";
                
                if(result.Object.Errors != null && result.Object.Errors.Length > 0)
                {
                    //For time being we just look at the first error
                    var firstError = result.Object.Errors[0];
                    
                    errorType = firstError.Type;
                    
                    string localisedErrorKey = null;
                    
                    switch(errorType)
                    {
                        case "TimeOffBlackoutPeriodOverlap":
                            localisedErrorKey = "timeoff_approval_blackout_error";
                            break;
                        case "CannotAddRequestForInactiveEmployee":
                            localisedErrorKey = "timeoff_approval_inactiveemployee_error";
                            break;
                        case "CannotEditTimeOffRequestInAClosedPayPeriod":
                            localisedErrorKey = "timeoff_approval_closedpayperiod_error";
                            break;
                        case "CannotEditTimeOffRequestWhichStartsAndEndsInAClosedPayPeriod":
                            localisedErrorKey = "timeoff_approval_startendclosedpayperiod_error";
                            break;
                        case "CannotApproveTimeOffRequestWithoutTimeOffType":
                            localisedErrorKey = "timeoff_approval_missingtimeofftype_error";
                            break;
                        case "TimeOffRequestCannotOverlapExistingTimeOffRequestForSingleDay":
                            localisedErrorKey = "timeoff_approval_singleoverlap_error";
                            break;
                        case "TimeOffRequestCannotOverlapExistingTimeOffRequestForMultipleDay":
                            localisedErrorKey = "timeoff_approval_multioverlap_error";
                            break;
                        case "TimeOffRequestLongerThan365Days":
                            localisedErrorKey = "timeoff_approval_toolong_error";
                            break;
                        case "CannotModifyOwnRequestNotInPendingStatus":
                            localisedErrorKey = "timeoff_approval_ownrequest_error";
                            break;
                        case "CannotChangeAllDayPartialDayWhenStartIsInClosedPayPeriodOrBeforeCurrentBusinessDate":
                            localisedErrorKey = "timeoff_approval_startclosedpayperiod_error";
                            break;
                        case "CannotCancelOwnPastTimeOffRequest":
                            localisedErrorKey = "timeoff_approval_cancelownpast_error";
                            break;
                        case "TimeOffRequestTypeMustBeProvided":
                            localisedErrorKey = "timeoff_approval_missingtype_error";
                            break;
                        case "MinTimeInAdvanceForSelf":
                            localisedErrorKey = "timeoff_approval_maxtimeself_error";
                            break;
                        case "TimeOffRequestDetailHoursCannotBeNegative":
                            localisedErrorKey = "timeoff_approval_negativehours_error";
                            break;
                        case "TimeOffRequestDetailHoursCannotBeGreaterThanTwentyFour":
                            localisedErrorKey = "timeoff_approval_detailtwentyfour_error";
                            break;
                        case "TimeOffRequestIsStale":
                            localisedErrorKey = "timeoff_approval_error";
                            break;
                        case "CannotAddRequestInClosedPayPeriod":
                            localisedErrorKey = "timeoff_approval_createclosedperiod_error";
                            break;
                        default:
                            localisedErrorKey = string.Empty;
                        break;
                   }
                   
                    if(!String.IsNullOrEmpty(localisedErrorKey))
                        result.Message = m_localiser.Get(localisedErrorKey);
                }
                
                if(String.IsNullOrEmpty(result.Message))
                {
                    result.Message = String.Format(m_localiser.Get("timeoff_approval_error"),result);
                }
            }
            
            return result;
        }
    }
}