using System;
using Consortium.Client.Core;
using Consortium.Client.iOS;

namespace WFM.Core
{
	public interface IStoredCredentialsService
	{

        string Username
        {
            get;
        }

        string Password
        {
            get;
        }

        string SiteID
        {
            get;
        }

        string UserUID
        {
            get;
        }

        void StoreCredentials(string username, string password, string siteID, string userUID);
        void ClearCredentials();
        void UpdateSiteID(string newSiteId);       
	}
}