﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class TimeOffRequestBlackoutOverrideService : ITimeOffRequestBlackoutOverrideService
    {
        public bool CanOverrideBlackoutPeriod (bool canOverrideBlackoutPeriod, string timeOffTypesThatCanOverrideBlackoutPeriod, string currentTimeOffRequestTypeName)
        {
            var timeOffTypes = GetTimeOffTypesOverridingBlackoutPeriod(timeOffTypesThatCanOverrideBlackoutPeriod);

            if(canOverrideBlackoutPeriod)
            {
                return true;
            }

            if(timeOffTypes.Any(t => string.Equals(t.Trim(), currentTimeOffRequestTypeName, StringComparison.CurrentCultureIgnoreCase)))
            {
                return true;
            }

            return false;
        }

        private List<string> GetTimeOffTypesOverridingBlackoutPeriod (string timeOffTypesThatCanOverrideBlackoutPeriod)
        {
            var timeOffTypes = new List<string>();

            if(!string.IsNullOrEmpty(timeOffTypesThatCanOverrideBlackoutPeriod))
            {
                try 
                {
                    timeOffTypes = timeOffTypesThatCanOverrideBlackoutPeriod.Split(',').ToList();
                } 
                catch
                {
                }
            }

            return timeOffTypes ?? new List<string>();
        }
    }
}

