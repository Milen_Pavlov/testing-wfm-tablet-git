﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class CallOffService : ICallOffService
    {
        public enum CallOffErrorType
        {
            UnableToLoadTimeOffList = 1,
            CannotRaiseCallOffType = 2,
            ErrorDuringHolidayCreation = 3,
            InvalidCallOffDates = 4,
            None = 0
        }

        private readonly ITimeOffService m_timeOffService;
        private readonly IStringService m_stringService;

        public CallOffService (ITimeOffService timeOffService, IStringService stringService)
        {
            m_timeOffService = timeOffService;
            m_stringService = stringService;
        }

        /// <summary>
        /// Given an employee, and a site, and a list of days, create a time off request booking for each day 
        /// </summary>
        /// <returns>The off days.</returns>
        /// <param name="siteId">For site identifier.</param>
        /// <param name="employeeId">For employee identifier.</param>
        /// <param name="daysToCallOff">Days to call off.</param>
        public async Task<Result> CallOffDays (int siteId, int employeeId, IEnumerable<DateTime> daysToCallOff, string timeOffTypeName)
        {
            Result callOffResult = new Result ()
            {
                Success = true,
                Code = (int)CallOffErrorType.None
            };
            
            if(daysToCallOff.IsNullOrEmpty())
            {
                callOffResult.Code = (int)CallOffErrorType.InvalidCallOffDates;
                callOffResult.Success = false;
                callOffResult.Message = m_stringService.Get("calloff_failedparameter_error");

            }
            else
            {
                Result<int> timeOffTypeResult = await retrieveTimeOffTypeId (employeeId, timeOffTypeName);
    
                if (timeOffTypeResult.Success) 
                {
                    foreach (DateTime date in daysToCallOff) 
                    {
                        var createHolidayResult = await m_timeOffService.CreateAndApproveHoliday (siteId, employeeId, date.Date, date.Date.AddDays(1).AddMinutes(-1), timeOffTypeResult.Object);
    
                        if (!createHolidayResult.Success) 
                        {
                            callOffResult.Code = (int)CallOffErrorType.ErrorDuringHolidayCreation;
                            callOffResult.Success = false;
                            callOffResult.Message = string.Format ( m_stringService.Get("calloff_failedraisingholidaydateformat_error"), date.Date);
    
                            break;
                        }
                    }
                } 
                else 
                {
                    return (Result)timeOffTypeResult;
                }
            }

            return callOffResult;
        }

        /// <summary>
        /// Given an employee ID and a type name, find the JDA time off type ID that we need to book for in order to match
        /// </summary>
        /// <returns>The time off type identifier.</returns>
        /// <param name="employeeId">Employee identifier.</param>
        /// <param name="timeOffTypeName">Time off type name.</param>
        private async Task<Result<int>> retrieveTimeOffTypeId (int employeeId, string timeOffTypeName)
        {
            Result<int> timeOffTypeResult = new Result<int> ();            

            var availableHolidayTypes = await m_timeOffService.GetTimeOffTypesForEmployee (employeeId);

            if (!availableHolidayTypes.Success || availableHolidayTypes.Object.IsNullOrEmpty ()) 
            {
                timeOffTypeResult.Code = (int)CallOffErrorType.UnableToLoadTimeOffList;
                timeOffTypeResult.Success = false;
                timeOffTypeResult.Message = m_stringService.Get("calloff_failedloadingtimeofflist_error");
            } 
            else 
            {
                var callOffType = availableHolidayTypes.Object.FirstOrDefault (type => String.Equals (type.TypeName, timeOffTypeName));

                if (callOffType == null) 
                {
                    timeOffTypeResult.Code = (int)CallOffErrorType.CannotRaiseCallOffType;
                    timeOffTypeResult.Success = false;
                    timeOffTypeResult.Message = m_stringService.Get("calloff_failedmatchingcallofftype_error");
                }
                else
                {
                    timeOffTypeResult.Success = true;
                    timeOffTypeResult.Object = callOffType.TypeID;
                }
            }

            return timeOffTypeResult;
        }

        public async Task<Result> CallOffShift (int siteId, int employeeId, DateTime shiftStartTime, DateTime shiftEndTime, string timeOffName)
        {
            Result callOffResult = new Result ()
            {
                Success = true,
                Code = (int)CallOffErrorType.None
            };
            
            var duration = shiftEndTime.Subtract(shiftStartTime);
            
            if(duration <= TimeSpan.Zero)
            {
                callOffResult.Code = (int)CallOffErrorType.InvalidCallOffDates;
                callOffResult.Success = false;
                callOffResult.Message = m_stringService.Get("calloff_failedparameter_error");
            }
            else
            {
                Result<int> timeOffTypeResult = await retrieveTimeOffTypeId (employeeId, timeOffName);
    
                if (timeOffTypeResult.Success) 
                {                    
                    var createHolidayResult = await m_timeOffService.CreateAndApproveHoliday (siteId, employeeId, shiftStartTime, shiftEndTime, timeOffTypeResult.Object);

                    if (!createHolidayResult.Success) 
                    {
                        callOffResult.Code = (int)CallOffErrorType.ErrorDuringHolidayCreation;
                        callOffResult.Success = false;
                        callOffResult.Message = string.Format ( m_stringService.Get("calloff_failedraisingholidaydateformat_error"), shiftStartTime.Date);
                    }
                } 
                else 
                {
                    return (Result)timeOffTypeResult;
                }
            }

            return callOffResult;
        }
    }
}