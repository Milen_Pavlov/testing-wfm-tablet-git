﻿using System;
using System.Collections.Generic;
using System.Linq;
using Consortium.Client.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class FilteredSiteService
    {
        private readonly SessionData m_sessionData;

        private List<JDAOrgUnit> OrgUnits { get; set; }
        private string Filter { get; set; }

        private Action<List<JDAOrgUnit>> OnUpdated;

        public FilteredSiteService (SessionData sessionData)
        {
            m_sessionData = sessionData;
        }

        public void Subscribe (Action<List<JDAOrgUnit>> action)
        {
            OnUpdated += action;
        }

        public void PublishSites ()
        {
            List<JDAOrgUnit> filteredOrgUnits = null;
            if (string.IsNullOrEmpty (Filter))
                filteredOrgUnits = OrgUnits;
            else
                filteredOrgUnits = OrgUnits.Where (x => x.Name.ToLower().Contains (Filter.ToLower())).ToList();

            if (OnUpdated != null)
                OnUpdated.Invoke (filteredOrgUnits);
        }

        public void UpdateSites ()
        {
            OrgUnits = new List<JDAOrgUnit>(m_sessionData.OrgUnits);
            PublishSites ();
        }

        public void SetFilter (string newFilter)
        {
            Filter = newFilter;
            PublishSites ();
        }
    }
}

