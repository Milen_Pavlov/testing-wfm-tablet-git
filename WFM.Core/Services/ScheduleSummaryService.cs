﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Consortium.Client.Core;
using Siesta;
using System.Threading;
using System.Linq;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleSummaryService : ManagedRestService
    {
        private readonly SessionData m_sessionData;
        private readonly RestClient m_client;
        private readonly IStringService m_localiser;

        private CancellationTokenSource m_tokenSource;

        public ScheduleSummaryService (SessionData data, RestClient client, IStringService localiser)
        {
            m_client = client;
            m_sessionData = data;
            m_localiser = localiser;
        }

        public async Task<List<JDANameAndID>> GetSummaryWorkgroups()
        {
            var workgroupsBySiteRequest = new WorkgroupsBySiteRequest()
            {
                SiteID = m_sessionData.Site.SiteID.ToString()
            };

            var result = await m_client.Post<WorkgroupsBySiteResponse>(workgroupsBySiteRequest);
   
            if (result.Object != null && result.Success)
            {
                return result.Object.data;
            }

            return null;
        }

        public void Cancel()
        {
            m_tokenSource.Cancel ();
        }

        public async Task<List<JDAScheduleSummaryGroup>> GetDepartmentSummaries(DateTime forWeek, IEnumerable<JDANameAndID> forDepartments)
        {
            PlannedScheduleSummaryRequest request = new PlannedScheduleSummaryRequest ();
            request.dayStartTimeHours = 6;
            request.dayStartTimeMinutes = 0;
            request.nightStartTimeHours = 22;
            request.nightStartTimeMinutes = 0;
            request.requiredWorkgroups = forDepartments.Select (d => d.ID.ToString ()).ToArray ();
            request.workgroupNames = string.Join(",",forDepartments.Select (d => d.Name).ToArray ());
            request.selectedWeekStart = forWeek.Date;
            request.siteId = m_sessionData.Site.SiteID.ToString ();

            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<RestReply<PlannedScheduleSummaryReply>>(() =>
                {
                    return m_client.Post<PlannedScheduleSummaryReply>(request, token).Result;
                });
            };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<PlannedScheduleSummaryReply>>(m_client.TimeoutMs, taskGenerator, m_tokenSource.Token);

            if (!result.Cancelled && result.ResponseObject != null && result.ResponseObject.Success && result.ResponseObject.Object.IsSuccess)
            {
                return BuildDepartmentSummaryResponse (result.ResponseObject.Object.data);
            }
            else
            {
                return null;
            }
        }

        public List<JDAScheduleSummaryGroup> BuildDepartmentSummaryResponse(IEnumerable<JDADepartmentSummary> fromResponseData)
        {
            List<JDAScheduleSummaryGroup> result = new List<JDAScheduleSummaryGroup> ();

            var groups = fromResponseData.GroupBy (response => response.WorkgroupID);

            foreach (IGrouping<int, JDADepartmentSummary> g in groups)
            {

                JDADepartmentSummary workgroupTitle = g.First (sum => sum.SummaryType == JDADepartmentSummaryType.WorkgroupTitle);

                //Start with the title
                JDAScheduleSummaryGroup currentGroup = new JDAScheduleSummaryGroup(workgroupTitle);

                //Extract each day in turn
                IEnumerable<IGrouping<DateTime, JDADepartmentSummary>> dateRecordGroups = 
                    g.Where(dateGroup => dateGroup.Date > DateTime.MinValue) //Remove anything that looks like a title row
                        .GroupBy (summary => summary.Date);               

                //need to filter out the title rows
                foreach (IGrouping<DateTime, JDADepartmentSummary> dateGroups in dateRecordGroups)
                {
                    List<JDAScheduleSummaryGroupBaseEntry> summaryGroup = new List<JDAScheduleSummaryGroupBaseEntry> ();

                    summaryGroup.Add (new JDAScheduleSummaryGroupBaseEntry (dateGroups.Key.ToString(m_localiser.Get("ddd, dd/MM")), true));

                    foreach (JDADepartmentSummary summary in dateGroups)
                    {
                        JDAScheduleSummaryGroupBaseEntry groupEntry = null;

                        if (summary.SummaryType == JDADepartmentSummaryType.DayRow)
                        {
                            groupEntry = new JDAScheduleSummaryGroupDayEntry (summary,false,"Day");
                        }
                        else if(summary.SummaryType == JDADepartmentSummaryType.NightRow)
                        {
                            groupEntry = new JDAScheduleSummaryGroupDayEntry (summary,false,"Night");
                        }
                        else if (summary.SummaryType == JDADepartmentSummaryType.DateTotal)
                        {
                            groupEntry = new JDAScheduleSummaryGroupDayEntry (summary,true,"Total");
                        }

                        if(groupEntry != null)
                            summaryGroup.Add (groupEntry);
                    }

                    currentGroup.Entries.AddRange(summaryGroup);
                }

                //Extract the weekly totals

                currentGroup.Entries.Add (new JDAScheduleSummaryGroupBaseEntry ("All Week", true));

                JDADepartmentSummary workgroupDailyTotal = g.First (sum => sum.SummaryType == JDADepartmentSummaryType.WeeklyDaysTotal);               

                currentGroup.Entries.Add (new JDAScheduleSummaryGroupDayEntry (workgroupDailyTotal, true, "Days Total"));

                JDADepartmentSummary workgroupNightlyTotal = g.First (sum => sum.SummaryType == JDADepartmentSummaryType.WeeklyNightsTotal);

                currentGroup.Entries.Add (new JDAScheduleSummaryGroupDayEntry (workgroupNightlyTotal, true, "Nights Total"));

                JDADepartmentSummary workgroupOverallTotal = g.First (sum => sum.SummaryType == JDADepartmentSummaryType.OverallWeeklyTotal);

                currentGroup.Entries.Add (new JDAScheduleSummaryGroupDayEntry (workgroupOverallTotal, true, "Week Total"));

                result.Add (currentGroup);
            }

            return result;
        }
    }
} 