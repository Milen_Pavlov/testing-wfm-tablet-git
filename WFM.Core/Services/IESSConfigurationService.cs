using System;
namespace WFM.Core
{
    public interface IESSConfigurationService
    {
        bool WeekNumberDisplayed { get; }
        bool ShowFillUnfilledShifts { get; }
        bool CanEditSchedule { get; }
        bool AlternativeScheduleView { get; }
        bool ShowPlanningTab { get; }
        bool ShowBreaksTab { get; }
        bool ShowResourcesTab { get; }
        int PreventChangesHours { get; }
        bool ShowScheduleCost { get; }
        bool IsScheduleEditorReadOnlyForUserRole { get; }
        int RestClientTimeoutSecs { get; }
        bool ShowApprovals { get; }
        bool ShowCallOffPast { get; }
        bool ShowCallOffCurrentDay { get; }
        bool ShowCallOffFuture { get; }
        bool ShowTimeOffMenu { get; }
        bool ShowMyHours { get; }
        bool ShowScheduleMenu { get; }
        bool ShowTillAllocationMenu { get; }
        bool ShowDailyOverviewMenu { get; }
        bool ShowNightAislePlannerMenu { get; }
        bool ShowPlannedScheduleSummary { get; }
        bool ShowCombineDepartments { get; }
        int CombineDepartmentsMaxChildDepartments { get; } 
        bool ShowSalesForecast { get; }
        bool ShowOvertimeTracker { get; }
        bool ShowChangePassword { get; }
        bool ShowSiteNumber { get; }
        int TimeoutMinutes { get; }
        bool CanResetPassword { get; }
        bool CheckForLockout { get; }
        bool CanOverrideBlackoutPeriod { get; }
        string TimeOffTypesOverridingBlackoutPeriod { get; }
        double KeepAliveFreqSecs { get; }
        string LockoutBundleOverride { get; }
        bool HideBreakWarnings { get; }
        int PeriodFirstDay { get; }
        int PeriodFirstMonth { get; }
        int StartDay { get; }
        int ScheduleTimeoutSecs { get; }
        bool? CanFilterByAttribute { get; }
        bool? CanFilterByDepartment { get; }
        bool? CanFilterByWorkgroup { get; }
        bool? CanFilterByJobCustom { get; }
        bool? CanFilterByRoleCustom { get; }
        bool? CanFilterByJobClassic { get; }
        bool WorkgroupsAreDepartments { get; }
        bool BlockEditingInPast { get; }
        bool ShiftSwapOnDragAndDropEnabled { get; }
        bool DontGenerateWarnings { get; }
        bool SendWeeklyWarningsUpdates { get; }
        bool CanEditEnvironments { get; }
        bool ShowSettingsButton { get; }
        bool ShowCombinedSSCSAYSTill { get; }
        bool HideEmptySSCSAYSTill { get; }
        bool ShowTillAllocationBuffer { get; }
        bool ShowMultipleTillAllocationBuffers { get; }
        bool CanDeleteShifts { get; }
        bool ShowUnfillOptions { get; }
        bool ShowScheduleDetails { get; }
        bool CanApproveOwnTimeOff { get; }
        bool ShowSpareShifts { get; }
        bool HideDefaultVarianceValues { get; }
        bool CanLockResources { get; }
        bool TimeOffReadOnly { get; }
        bool CanCreateTimeOff { get; }
        bool CanEditOwnPunches { get; }
        bool CanModifyPunchesInFuture { get; }
        bool ShowPopoverEmployeeTimeOffView { get; }
        bool ShowAllEmployeeHistoryInPopover { get; }
        SSCSAYSConfiguration [] SSCSAYSConfigurations { get; }
        int NightShiftStartTime { get; }
        int NightShiftDuration { get; }
        string NightShiftDepartments { get; }
        bool AllowPunchesForMeals { get; }
        bool AllowPunchesForBreaks { get; }
        bool AllowPunchesForJobs { get; }
        string CallOffShiftTimeOffType { get; }
        string CallOffShiftUnpaidTimeOffType { get; }
        string ExcludedUnplannedTimeOffType { get; }
        
        bool CallOffAppliesForFixedShiftsOnly { get; }
        bool CallOffUnpaidAppliesForFixedShiftsOnly { get; }
        
        bool CanPrintSchedule { get; }
        bool ShowMobileNumbers { get; }
        bool ShowStoreInNavBar { get; }
        bool HideTimeOffType { get; }
        bool CanViewTimeOffTypeDetails { get; }
        int MaxShiftLengthHours { get; }
        
        bool HasTermsOfServiceEnabled { get; }
        double TotalHoursPerDayDivisor { get; }

        string Address { get; }
        string ConsortiumAddress { get; }
        string ConsortiumUsername { get; }
        string ConsortiumPassword { get; }
        bool UseCustomScheduleRequest { get; }
        bool SendScheduleAudit { get; }
        string MyHoursAddress { get; }
        string MyHoursUsername { get; }
        string MyHoursPassword { get; }
        string CombinedDeptAddress { get; }
        string CombinedDeptUsername { get; }
        string CombinedDeptPassword { get; }
        string LabourDemandAddress { get; }

        bool JDAHideAttributes { get; }
        bool JDAEnableDepartmentFiltering { get; }
    }
}