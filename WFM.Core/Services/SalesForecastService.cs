﻿using System;
using System.Threading;
using Siesta;
using System.Threading.Tasks;
using Consortium.Client.Core;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumService]
    public class SalesForecastService : ManagedRestService, ISalesForecastService
    {
        private readonly RestClient m_client;
        private readonly SessionData m_sessionData;
        private readonly static string _CHANGECOMMANDTYPE = "SiteManager.Commands.Forecasting.ForecastEdit";
        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();
        private CancellationTokenSource m_groupTokenSource = new CancellationTokenSource();
        private CancellationTokenSource m_processChangeTokenSource = new CancellationTokenSource();

        private int m_selectedGroupIndex;
        public int GetSelectedIndex()
        {
            return m_selectedGroupIndex;
        }

        public SalesForecastService (RestClient client, 
            SessionData sessionData)
        {
            m_client = client;
            m_sessionData = sessionData;
            m_selectedGroupIndex = 0;
        }
            
        public async Task<List<JDAForecastedData>> GetSalesForecastData(DateTime date, int groupId, int delayMs = 0)
        {
            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<SalesForcastDataResponse>(() =>
                        {
                            return ReadForecastAsync(date, groupId, token, delayMs).Result;
                        });
                };
            
            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<SalesForcastDataResponse>(m_client.TimeoutMs, taskGenerator, m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                return result.ResponseObject.data;
            }
            return null;
        }
            
        public async Task<SalesForcastDataResponse> ReadForecastAsync(DateTime date, int groupId, CancellationToken token, int msDelay)
        {
            m_selectedGroupIndex = groupId;
            var salesForecastRequest = new SalesForecastDataRequest ()
                { 
                    siteID = m_sessionData.Site.SiteID.ToString(),
                    groupID = groupId,
                    startOfWeek = date
                };

            //This acts as a buffer, allowing us to cancel the operations quickly before transmission. This means JDA doesn't get overwhelmed by multiple requests that don't expect a response.
            //Typical use case is when cycling through the weeks.
            await Task.Delay(msDelay);

            var forecastData = m_client.Post<SalesForcastDataResponse>(salesForecastRequest, token);
            await forecastData.ConfigureAwait (false);
            token.ThrowIfCancellationRequested();

            if (!forecastData.Result.Success)
            {
                if (forecastData.Result.IsCancelled)
                {
                    return null;
                }

                return null;
            }
            else
            {
                return forecastData.Result.Object;
            }
        }

        public async Task<List<JDANameAndID>> GetForecastGroups()
        {
            var forecastGroupsRequest = new ForecastGroupsBySiteRequest()
                {
                    siteID = m_sessionData.Site.SiteID.ToString()
                };

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ForecastGroupsBySiteResponse>>(() =>
                        {
                            return m_client.Post<ForecastGroupsBySiteResponse>(forecastGroupsRequest, token).Result;
                        });
                };

            m_groupTokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<ForecastGroupsBySiteResponse>>(m_client.TimeoutMs, taskGenerator, m_groupTokenSource.Token);

            if (result.ResponseObject != null && result.ResponseObject.Success)
            {
                return result.ResponseObject.Object.data;
            }
            
            return null;
        }

        public async Task<bool> SubmitChanges(List<SalesForecastDataItem> values)
        {
            // create request with required data and change commands
            var request = new ProcessChangeCommandsRequest ();
           
            List<JDAChangeCommand> items = new List<JDAChangeCommand> ();
            foreach(SalesForecastDataItem d in values)
            {
                foreach(var day in d.Days)
                {
                    JDAChangeCommand cc = new JDAChangeCommand ()
                        {
                            __TYPE = _CHANGECOMMANDTYPE,
                            SiteID = m_sessionData.Site.SiteID,
                            Value = day.DayValue,
                            BusinessDate = day.Date,
                            MetricID = d.MetricID,
                            ForecastGroupID = d.ForecastGroupID
                        }; 
                    items.Add (cc);
                }
            }
            request.jsChangeCommands = items.ToArray ();

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ProcessChangeCommandsReply>>(() =>
                        {
                            return m_client.Post<ProcessChangeCommandsReply>(request, token).Result;
                        });
                };

            m_processChangeTokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<ProcessChangeCommandsReply>>(m_client.TimeoutMs, taskGenerator, m_processChangeTokenSource.Token);

            if (result.ResponseObject != null && result.ResponseObject.Success)
            {
                if (result.ResponseObject.Object.data.Errors != null && (result.ResponseObject.Object.data.Errors.Length > 0))
                {
                    return false;
                }
                return true;
            }

            return false;
        }

        public void CancelForecastRead()
        {
            m_tokenSource.Cancel();
        }

        public void CancelGroupForecastRead()
        {
            m_groupTokenSource.Cancel ();   
        }

        public void CancelSubmitChanges()
        {
            m_processChangeTokenSource.Cancel ();
        }
    }
}

