﻿using System;
namespace WFM.Core
{
    public class AppThemeChangedEventArgs : EventArgs
    {
        public string ThemeName { get; set; }
    }
}

