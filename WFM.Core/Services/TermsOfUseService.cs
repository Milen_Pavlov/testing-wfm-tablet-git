﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Newtonsoft.Json;

namespace WFM.Core
{
    [ConsortiumService]
    public class TermsOfUseService : ITermsOfUseService
    {
        public const string TOS_FILE_PATH = "assets://Theme/TermsOfUseText.json";
        public const string USER_ACCEPTED_FILENAME = "TermsOfServiceAcceptance.json";
        public const string USER_ACCEPTED_DIR = "documents://ToS";

        private readonly IFileSystem m_fileSystem;
        private readonly IESSConfigurationService m_settings;

        private string UserAcceptedFile { get { return string.Format("{0}/{1}", USER_ACCEPTED_DIR, USER_ACCEPTED_FILENAME); } }

        private Dictionary<string, int> UserVersionsAccepted { get; set; }

        public TermsOfUseConfig CurrentToS { get; private set; }

        public TermsOfUseService (IFileSystem fileSystem, IESSConfigurationService settings)
        {
            m_fileSystem = fileSystem;
            m_settings = settings;

            LoadState();
        }

        public void LoadState()
        {
            CurrentToS = null;

            if(m_fileSystem.Exists(TOS_FILE_PATH))
            {
                string txt = m_fileSystem.ReadText(TOS_FILE_PATH);
                
                if(txt != null)
                    CurrentToS = JsonConvert.DeserializeObject<TermsOfUseConfig>(txt);
            }
            
            //Make the terms of service acceptance folder location in case it doesn't exist
            m_fileSystem.MkDir(USER_ACCEPTED_DIR);
            UserVersionsAccepted = new Dictionary<string, int>();
            
            //Load in current accepted versions if the file exists
            if(m_fileSystem.Exists(UserAcceptedFile))
            {
                string txt = m_fileSystem.ReadText(UserAcceptedFile);
                UserVersionsAccepted = JsonConvert.DeserializeObject<Dictionary<string, int>>(txt);
            }
        }

        public void ClearAcceptedState()
        {
            UserVersionsAccepted = new Dictionary<string, int>();
            SaveUserAcceptanceData();
        }

        public bool ShouldShowToS(string userID)
        {
            //Check if ToS is even enabled in this build
            if(!m_settings.HasTermsOfServiceEnabled)
                return false;

            //Check if we even HAVE a ToS file loaded
            if(CurrentToS == null || CurrentToS.Version == 0 || string.IsNullOrEmpty(CurrentToS.TextFileLocation))
                return false;

            //Check if the user has EVER accepted any ToS
            if(!UserVersionsAccepted.ContainsKey(userID))
                return true;

            //Check if the current version is greater than the last accepted ToS by this user
            if(UserVersionsAccepted[userID] < CurrentToS.Version)
                return true;

            return false;
        }

        public void MarkUserAsAccepted(string userID)
        {
            if(!ShouldShowToS(userID))
                return;
            
            int currentVersion = CurrentToS.Version;
            if(UserVersionsAccepted.ContainsKey(userID))
                UserVersionsAccepted[userID] = currentVersion;
            else
                UserVersionsAccepted.Add(userID, currentVersion);

            //Save to file
            SaveUserAcceptanceData();
        }

        private void SaveUserAcceptanceData()
        {
            string json = JsonConvert.SerializeObject(UserVersionsAccepted);
            m_fileSystem.WriteText(UserAcceptedFile, json);
        }
    }
}