﻿using System;

namespace WFM.Core
{
    public interface ICryptographyService
    {
        string Decrypt(string key, string textToDecrypt);
    }
}

