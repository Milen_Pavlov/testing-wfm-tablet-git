﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Siesta;
using Siesta.Client;

namespace WFM.Core
{
    public class JDAConfigurationService : IJDAConfigurationService
    {
        public IConfiguration JDAConfiguration { get; set; } 

        public JDAConfigurationService ()
        {
            JDAConfiguration = new ConfigurationStore();
        }

        public async Task LoadJDAConfig(SiestaClient client, int siteID)
        {
            JDAConfiguration.ClearAll();

            var configRequest = client.Post<GetFeatureConfigurationReply>(new GetFeatureConfigurationRequest() { siteId = siteID });
            var configReply = await configRequest;

            var siteManagerSettings = CreateDictionaryFromReply(configReply);
            if(siteManagerSettings != null)
                JDAConfiguration.Append(siteManagerSettings);
        }

        private Dictionary<string, string> CreateDictionaryFromReply(RestReply<GetFeatureConfigurationReply> reply)
        {
            if (!reply.Success || reply.Object == null || reply.Object.data == null)
                return null;

            var newSettings = new Dictionary<string, string>();
            foreach (var config in reply.Object.data)
            {
                string key = string.Format (JDAConfig.FORMAT_STRING, config.Group, config.Setting);
                if(!newSettings.ContainsKey(key))
                    newSettings.Add(key, config.Value);
            }

            return newSettings;
        }
    }
}

