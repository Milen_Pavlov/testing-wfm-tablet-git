﻿using System;
using System.Threading.Tasks;

namespace WFM.Core
{
    public interface IEmployeeService
    {
        Task<JDAEmployeeInfo[]> GetAllEmployees(int siteID, DateTime? effectiveTimestamp = null);
    }
}

