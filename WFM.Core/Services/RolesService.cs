﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class RolesService : IRolesService
    {
        public List<ShiftDetailData> GenerateRoleDetailsForShift (List<JobData> jobs, List<ShiftDetailData> details = null)
        {
            if(jobs.IsNullOrEmpty())
            {
                return new List<ShiftDetailData>();
            }

            var detailList = new List<ShiftDetailData>();
            foreach(var role in jobs.SelectMany(j => j.Roles.Where(r => r.Start != DateTime.MinValue)))
            {
                var detail = ShiftDetailData.CreateRoleDetail(role);

                if(details.Any())
                {
                    var existingDetail = details.FirstOrDefault(d => d.IsRole && d.Start == detail.Start && d.End == detail.End && d.ScheduledJobID == detail.ScheduledJobID);
                    if(existingDetail != null)
                    {
                        existingDetail.Edit = EditState.Modified;
                        detailList.Add(existingDetail);
                    }
                    else
                    {
                        detail.SetOriginalValues();
                        detailList.Add(detail);
                    }
                }
                else
                {
                    detail.SetOriginalValues();
                    detailList.Add(detail);
                }
            }

            return detailList;
        }

        public List<ShiftDetailData> UpdatedRoleDetailsForShift (ShiftData shiftData, List<ShiftDetailData> originalDetailList)
        {
            if(originalDetailList == null)
            {
                originalDetailList = BuildDetailList(shiftData.Details);
                DeleteRoleDetails(shiftData);
            }

            var adjustedRoles = GetAdjustedRolesForShift(originalDetailList.Where(d => d.IsRole), shiftData);

            UpdateDeletedDetailList(adjustedRoles, shiftData.DeletedDetails);


            return adjustedRoles;

        }

        private void UpdateDeletedDetailList (List<ShiftDetailData> adjustedRoles, List<ShiftDetailData> deletedDetails)
        {
            foreach(var adjustedRole in adjustedRoles)
            {
                //check if role we adjsuted is in modified or none state - is so delete from deletedshifts buffer
                if(adjustedRole.Edit == EditState.Modified || adjustedRole.Edit == EditState.None)
                {
                    var deletedDetailMatching = deletedDetails.FirstOrDefault(deletedDetal => DetalIsMatchingRole(deletedDetal, adjustedRole));

                    if(deletedDetailMatching != null)
                    {
                        deletedDetails.Remove(deletedDetailMatching);
                    }
                }
            }
        }

        private bool DetalIsMatchingRole (ShiftDetailData deletedDetal, ShiftDetailData adjustedRole)
        {
            return deletedDetal.IsRole && adjustedRole.ShiftDetailID == deletedDetal.ShiftDetailID 
                               && TimePeriodHelper.DoesOverlap(deletedDetal.Start.Value, deletedDetal.End.Value, adjustedRole.Start.Value, adjustedRole.End.Value);
        }

        private List<ShiftDetailData> BuildDetailList (List<ShiftDetailData> Details)
        {
            var detailList = new List<ShiftDetailData>();

            foreach(var detail in Details)
            {
                detailList.Add(detail.Copy());
            }

            return detailList;
        }

        private void DeleteRoleDetails (ShiftData shiftData)
        {
            if (shiftData.Details.Where(d => d.IsRole).Count() == 0)
                return;

            if (shiftData.Edit != EditState.Added)
                shiftData.Edit = EditState.Modified;
            shiftData.Modified = true;

            foreach (var d in shiftData.Details.Where(d => d.IsRole))
            {
                shiftData.DeletedDetails.Add (d.Copy());
            }

            shiftData.Details.RemoveAll(d => d.IsRole);
        }

        private List<ShiftDetailData> GetAdjustedRolesForShift (IEnumerable<ShiftDetailData> roleDetails, ShiftData shiftData)
        {
            var roleSplit = false;
            var roleAdded = false;
            var adjustedRoles  = new List<ShiftDetailData>();

            if(shiftData.Jobs.IsNullOrEmpty())
            {
                return new List<ShiftDetailData>();
            }

            foreach (var job in shiftData.Jobs) 
            {
                if(roleDetails.IsNullOrEmpty())
                {
                    return new List<ShiftDetailData>();
                }

                var anyDeletedDetails = shiftData.DeletedDetails.Any();

                //check if role needs adjusting start or end time and then check if overlaps with reclculated breaks and meals
                foreach (var roleDetail in roleDetails.Where(r => r.ScheduledJobID == job.ScheduledJobID))           
                {   
                    roleSplit = false;
                    roleAdded = false;
                    //Change Role Start / End times based on updated Shift Start / End times

                    //if updated Shift Start is before Role Start (dragging shift left) and Shift end is after Role end we do not update Role Start or Role End
                    if(shiftData.Start <= roleDetail.Start && shiftData.End >= roleDetail.End)
                    {
                        if(roleDetail.Edit != EditState.Modified)
                            roleDetail.Edit = EditState.None;
                    }
                    //if updated Shift Start is after Role start but Shift End is after Role end
                    else if(shiftData.Start > roleDetail.Start && shiftData.End >= roleDetail.End)
                    {
                        roleDetail.Start = shiftData.Start;
                        roleDetail.Edit = EditState.Added;
                    }
                    //if updated Shift Start is before Role Start and Role End is after updated Shift End reduce Role End time
                    else if(shiftData.Start <= roleDetail.Start && shiftData.End < roleDetail.End)
                    {
                        roleDetail.End = shiftData.End;
                        roleDetail.Edit = EditState.Added;
                    }
                    //if both role start and end are outside shift hours reduce them to shift hours
                    else if(shiftData.Start > roleDetail.Start && shiftData.End < roleDetail.End)
                    {
                        roleDetail.Start = shiftData.Start;
                        roleDetail.End = shiftData.End;
                        roleDetail.Edit = EditState.Added;
                    }

                    var breaksAndMeals = shiftData.Details
                        .Where(d => !d.IsRole)
                        .Where(d => d.ScheduledJobID == job.ScheduledJobID)
                        .ToList();

                    //if no breaks for job just add the role
                    if(!breaksAndMeals.Any())
                    {
                        adjustedRoles.Add(roleDetail);
                    }

                    //once andjusted check if overlaps with any of the recalculated breaks abd meals;
                    foreach (var detail in breaksAndMeals) 
                    {
                        roleSplit = false;
                        //find id overlaps and define overlapping start / end
                        if(TimePeriodHelper.DoesOverlap(roleDetail.Start.Value, roleDetail.End.Value, detail.Start.Value, detail.End.Value))
                        {
                            //check break/meal overlap times relation to our role

                            //1. overlaped (from break or meal) start end completely inside role
                            if(roleDetail.Start <= detail.Start && roleDetail.End >= detail.Start && roleDetail.Start <= detail.End && roleDetail.End >= detail.End)
                            {
                                //split the role on two 
                                //remove the first deleted role
                                var deletedRole = shiftData.DeletedDetails.Where(d => d.ShiftDetailID == roleDetail.ShiftDetailID)
                                                                .OrderBy(d => d.Start).FirstOrDefault();
                                if(deletedRole != null)
                                {
                                    shiftData.DeletedDetails.Remove(deletedRole);
                                }

                                if(roleDetail.Start != detail.Start)
                                {
                                    var updatedDetail1 = new ShiftDetailData { 
                                        Edit = deletedRole != null || !anyDeletedDetails ? EditState.Modified : EditState.Added,
                                        RoleId = roleDetail.RoleId, 
                                        PunchCode = "r", 
                                        ScheduledJobID = roleDetail.ScheduledJobID,
                                        ShiftDetailID = roleDetail.ShiftDetailID,
                                        OriginalStart = roleDetail.OriginalStart.HasValue ? roleDetail.OriginalStart.Value : roleDetail.Start, 
                                        OriginalEnd = roleDetail.OriginalEnd.HasValue ? roleDetail.OriginalEnd.Value : roleDetail.End,
                                        Start = roleDetail.Start,
                                        End = detail.Start
                                    };

                                    adjustedRoles.Add(updatedDetail1);
                                }


                                var updatedDetail2 = new ShiftDetailData { 
                                    Edit = roleDetail.Start == detail.Start ? EditState.Modified : EditState.Added,
                                    RoleId = roleDetail.RoleId, 
                                    PunchCode = "r", 
                                    ScheduledJobID = roleDetail.ScheduledJobID,
                                    ShiftDetailID = roleDetail.ShiftDetailID,
                                    OriginalStart = roleDetail.OriginalStart.HasValue ? roleDetail.OriginalStart.Value : roleDetail.Start, 
                                    OriginalEnd = roleDetail.OriginalEnd.HasValue ? roleDetail.OriginalEnd.Value : roleDetail.End,
                                    Start = detail.End,
                                    End = roleDetail.End
                                };

                                adjustedRoles.Add(updatedDetail2);

                                roleSplit = true;
                                roleAdded = true;
                            }

                            //2. Role is completely in break or meal
                            else if(roleDetail.Start >= detail.Start && roleDetail.Start <= detail.End && roleDetail.End >= detail.End && roleDetail.End <= detail.End)
                            {
                                //do nothing break/meal covers role completely - role is completely removed 
                            }

                            //3.Role start is in break meal interval
                            else if(roleDetail.Start >= detail.Start && roleDetail.Start <= detail.End && roleDetail.End >= detail.Start && roleDetail.End >= detail.End)
                            {
                                //adjust role start
                                roleDetail.Start = detail.End;
                                roleDetail.Edit = EditState.Modified;
                                roleDetail.OriginalStart = roleDetail.OriginalStart;
                                roleDetail.OriginalEnd = roleDetail.OriginalEnd;
                            }

                            //4.Role end is in break or meal
                            else if(roleDetail.Start <= detail.Start && roleDetail.Start <= detail.End && roleDetail.End >= detail.Start && roleDetail.End <= detail.End)
                            {
                                roleDetail.End = detail.Start;
                                roleDetail.Edit = EditState.Modified;
                                roleDetail.OriginalStart = roleDetail.OriginalStart;
                                roleDetail.OriginalEnd = roleDetail.OriginalEnd;
                            }

                        }
                        else
                        {
                            //does not overlap with with breaks or meals add it as is
                        }

                        if(!roleSplit && !roleAdded)
                        {                        
                            adjustedRoles.Add(roleDetail);
                            roleAdded = true;
                        }
                    }
                }
            }

            adjustedRoles.RemoveAll(r => r.Start == r.End);

            var duplicatesRoles = adjustedRoles.GroupBy(r => r)
                                               .Where(g => g.Count() > 1)
                                               .Select(g => g.Key)
                                               .ToList();

            var rolesToRemove = new List<ShiftDetailData>();

            foreach (var duplicatedRole in duplicatesRoles) 
            {
                var roleToRemove = adjustedRoles.FirstOrDefault(r => r.Start == duplicatedRole.Start &&
                                                                r.End == duplicatedRole.End &&
                                                                r.ScheduledJobID == duplicatedRole.ScheduledJobID &&
                                                                r.ShiftDetailID == duplicatedRole.ShiftDetailID);
                if(roleToRemove != null)
                {
                    rolesToRemove.Add(roleToRemove);
                }
            }

            foreach (var roleToRemove in rolesToRemove) 
            {
                adjustedRoles.Remove(roleToRemove);
            }

            return adjustedRoles;
        }

        public void MoveRolesForJob (JobData job, TimeSpan startDelta, TimeSpan endDelta, TimeSpan sizeDelta, List<ShiftDetailData> details)
        {
            //resize affected roles for resized job
            foreach(var roleDetail in details.Where(d => d.IsRole && d.ScheduledJobID == job.ScheduledJobID))
            {
                //only move role if entire shift is moving
                if((int)startDelta.TotalMinutes != 0 && (int)endDelta.TotalMinutes != 0 && startDelta == endDelta)
                {
                    roleDetail.Edit = EditState.Modified;
                    roleDetail.Move(startDelta);
                }
            }

            //resize job.Roles too
            foreach(var role in job.Roles.Where(d => d.ScheduledJobID == job.ScheduledJobID))
            {
                //only move role if entire shift is moving
                if((int)startDelta.TotalMinutes != 0 && (int)endDelta.TotalMinutes != 0 && startDelta == endDelta)
                {
                    if((int)startDelta.TotalMinutes != 0 && (int)endDelta.TotalMinutes != 0 && startDelta == endDelta)
                    {
                        role.EditState = EditState.Modified;
                        role.Move(startDelta);
                    }
                }
            }
        }
    }
}

