﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{    
    [ConsortiumService]
    public class TimeFormatService: ITimeFormatService
    {     
        const string DEFAULT_TIME_FORMAT = "HH:mm";
        const string DEFAULT_TIME_PICKER_LOCALE = "nl";
        const string DEFAULT_RULER_TIME_FORMAT = "%H";

        private string m_timeFormat = null;
        public string TimeFormat 
        {
            get 
            {
                if(m_timeFormat == null)
                {
                    m_timeFormat = DEFAULT_TIME_FORMAT;
                    
                    string localisedFormat = m_stringService.Get(DEFAULT_TIME_FORMAT);
                    
                    if(!string.IsNullOrEmpty(localisedFormat))
                        m_timeFormat = localisedFormat;
                }
                
                return m_timeFormat;
            }
        }
        
        private string m_rulerTimeFormat = null;
        public string RulerTimeFormat 
        {
            get 
            {
                if(m_rulerTimeFormat == null)
                {
                    m_rulerTimeFormat = DEFAULT_RULER_TIME_FORMAT;
                    
                    string localisedFormat = m_stringService.Get("ruler_time_format");
                    
                    if(!string.IsNullOrEmpty(localisedFormat) && localisedFormat != "ruler_time_format")
                        m_rulerTimeFormat = localisedFormat;
                }
                
                return m_rulerTimeFormat;
            }
        }

        private string m_timePickerLocale = null;
        public string TimePickerLocale 
        {
            get 
            {
                if(m_timePickerLocale == null)
                {
                    m_timePickerLocale = DEFAULT_TIME_PICKER_LOCALE;

                    string localisedLocale = m_stringService.Get("time_picker_locale");
                    
                    if(!string.IsNullOrEmpty(localisedLocale) && localisedLocale != "time_picker_locale")
                        m_timePickerLocale = localisedLocale;
                }
                
                return m_timePickerLocale;                
            }
        }

        private readonly IStringService m_stringService;
        
        public TimeFormatService(IStringService stringService)
        {
            m_stringService = stringService;
        }
        
        public string FormatTime(DateTime time)
        {
            return time.ToString(TimeFormat);
        }

        public string FormatTimeForRuler (DateTime time)
        {
            return time.ToString(RulerTimeFormat);
        }
    }
}