﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WFM.Core
{
    public interface ITimeOffService
    {
        Task<Result<JDAProcessingResult>> CreateAndApproveHoliday(int siteID, int employeeID, DateTime start, DateTime end, int timeOffTypeID, string comment = "", List<PayAllocation> allocationChanges = null, bool allowBlackout = false);
        Task<Result<IEnumerable<JDATimeOffType>>> GetTimeOffTypesForEmployee(int employeeId);
        Task<JDATimeOffRequest[]> GetAllTimeOffRequests(int siteID, DateTime startDate, DateTime endDate);
    }
}