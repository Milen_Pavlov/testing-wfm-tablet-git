﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Consortium.Client.Core;
using System.Linq;

namespace WFM.Core
{
    [ConsortiumService]
    public class WeeklyOverviewService
    {
        private readonly DailyOverviewService m_dailyOverviewService;

        public DateTime CurrentDate { get; set; }

        public WeeklyOverviewService (DailyOverviewService dailyOverview)
        {
            m_dailyOverviewService = dailyOverview;
        }

        public async Task<SiteTimecardDataResult> GetSiteTimecardsForRange(int siteID, DateTime startDate, DateTime endDate, CancellationToken ct)
        {
            SiteTimecardDataResult result = new SiteTimecardDataResult (); 
            DateTime requestDate = startDate.Date;
            List<Task<SiteTimecardDataResult>> requests = new List<Task<SiteTimecardDataResult>> ();
            
            do
            {
                requests.Add(m_dailyOverviewService.GetSiteTimecardsForDate(siteID, requestDate, ct));
                requestDate = requestDate.AddDays(1);
            } 
            while(requestDate < endDate.Date);

            if (ct.IsCancellationRequested)
            {
                result.WasCancelled = false;
            }

            result.Success = true;

            foreach (var request in requests)
            {
                await request;
                if (request.Result.WasCancelled == true)
                {
                    result.WasCancelled = true;
                    continue;
                }

                if (request.Result.Success == false)
                {
                    result.Success = false;
                    continue;
                }

                result.Timecards.AddRange(request.Result.Timecards);
            }

            foreach (var timecard in result.Timecards)
            {
                timecard.DailyCell = false;
            }

            return result;
        }
    }
}