﻿using System;
using System.Threading.Tasks;
using Consortium.Client.Core;
using Newtonsoft.Json;

namespace WFM.Core
{
    [ConsortiumService]
    public class SiteMgrCredentialsService : ISiteMgrCredentialsService
    {
        // For Parsing HTML
        private class Globals
        {
            public string USERNAME { get; set; }
            public string USERID { get; set; }
        }

        private readonly RestClient m_client;

        public SiteMgrCredentialsService(RestClient client)
        {
            m_client = client;
        }

        public async Task<SiteManagerUserDetails> GetCurrentUserDetails(string siteName)
        {
            // Get site module & session information.
            var sitemgr4 = await m_client.Get<string> (string.Format("rp/{0}/SiteMgrHome?portal=true", siteName)); // < loads first page if authenticated

            SiteManagerUserDetails userDetails = GetUserDetailsFromHTML(sitemgr4.Object);

            //TODO If we find different versions of JDA that parse it different, or have different endpoitns, try them here sequentially
            return userDetails;
        }

        private SiteManagerUserDetails GetUserDetailsFromHTML(string html)
        {
            SiteManagerUserDetails smDetails = new SiteManagerUserDetails();
            
            try
            {
                const string c_start = "_globals =";
                const string c_end = ";";
    
                int start = html.IndexOf (c_start);
    
                if (start >= 0)
                {
                    start += c_start.Length;
                    int end = html.IndexOf (c_end, start);
        
                    if (end >= 0)
                    {
                        string json = html.Substring (start, end - start);
                        var globals = JsonConvert.DeserializeObject<Globals> (json);
            
                        if(globals != null)
                        {
                            smDetails.UserID = globals.USERID;
                            smDetails.Username = globals.USERNAME;
                        }
                    }
                }
            }
            catch(Exception)
            {}

            return smDetails;
        }
    }
}