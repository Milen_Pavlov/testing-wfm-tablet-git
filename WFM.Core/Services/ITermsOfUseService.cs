﻿using System;
namespace WFM.Core
{
    public interface ITermsOfUseService
    {
        TermsOfUseConfig CurrentToS { get; }

        void LoadState();
        void ClearAcceptedState();
        bool ShouldShowToS(string userID);
        void MarkUserAsAccepted(string userID);
    }
}

