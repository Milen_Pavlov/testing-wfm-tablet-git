﻿using Consortium.Client.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Core.Model;
using WFM.Core.RestClients;
using WFM.Shared.DataTransferObjects.TillAllocation;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Threading;
using Siesta;
using WFM.Shared.Constants;

namespace WFM.Core.Services
{
    [ConsortiumService]
	public class TillAllocationService: ManagedRestService
    {
		private struct TillReallocation
		{
			public int AllocationID { get; set; }

			public int TillId { get; set; }
		}

        private readonly SessionData m_data;
        private readonly ConsortiumRestClient m_client;
        private readonly IMvxMessenger m_messenger;
        private readonly TillService m_tillService;
        private readonly ScheduleService m_scheduleService;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<TillAllocation> TillAllocations { get; set; }

		public int? MaxHours { get; set; }

        protected MvxSubscriptionToken m_signOutToken;

		private List<TillReallocation> m_tillAllocationUpdates = new List<TillReallocation>();
		private List<TillReallocation> m_originalTillSetup = new List<TillReallocation>();
        private StaticData m_staticData;

        public SSCSAYSConfiguration[] SSCSAYSConfigurations
        {
            get;
            private set;
        }

        public string CombinedSSCSAYSTillName
        {
            get;
            private set;
        }

        public bool IsLoadingAllocations
        {
            get;
            private set;
        }


        public TillAllocationService(
            SessionData data, 
            ConsortiumRestClient client, 
            IMvxMessenger messenger,
            TillService tills,
            ScheduleService scheduleService, 
            IESSConfigurationService configurationService)
        {
            m_data = data;
            m_client = client;
            m_tillService = tills;
            m_messenger = messenger;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_scheduleService = scheduleService;

            //Get the SSC/SAYS department names
            SSCSAYSConfigurations = configurationService.SSCSAYSConfigurations;

            var allDisplayNames = (SSCSAYSConfigurations!=null) ? SSCSAYSConfigurations.Select(x => x.DisplayName).ToList() : new List<string>();
            CombinedSSCSAYSTillName = string.Join (", ", allDisplayNames.Take (allDisplayNames.Count () - 1)) + (allDisplayNames.Count () > 1 ? " and " : string.Empty) + allDisplayNames.LastOrDefault ();
        }
                       
        public async Task<List<TillAllocation>> ReadAsync(DateTime from, DateTime to)
        {
            IsLoadingAllocations = true;

			//Clear pending changes
			m_tillAllocationUpdates.Clear();
			m_originalTillSetup.Clear ();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<List<TillAllocation>>(() =>
                        {
                            return ReadAsyncOperation(from, to,token).Result;
                        });
                };
                    
            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<List<TillAllocation>>(timeout, taskGenerator, m_tokenSource.Token);

            IsLoadingAllocations = false;
            m_messenger.Publish<OnTillAllocationDataRetrievedMessage>(new OnTillAllocationDataRetrievedMessage(this) { Success = result.ResponseObject != null });

            return result.ResponseObject;
        }
            
        async Task<List<TillAllocation>> ReadAsyncOperation(DateTime from, DateTime to, CancellationToken token)
        {
            var request = new ReadRequest()
            {
                SiteId = m_data.Site.SiteID,
                JdaSessionId = m_client.JdaSessionId,
                From = from,
                To = to,
            };
                    
            TillAllocations = new List<TillAllocation>();

            Task<StaticData> staticDataTask = m_data.GetStaticDataAsync();
            m_staticData = null;

            var allocationsResponse = m_client.Post<ReadResponse>(request, token);
            var tillsResponse = m_tillService.ReadAsync(m_data.Site.SiteID, token);
            var allocationsResult = await allocationsResponse;
            var tillsResult = await tillsResponse;
            m_staticData = await staticDataTask;

            token.ThrowIfCancellationRequested();

            if (allocationsResult.Success && allocationsResult.Object.Success)
            {
                StartDate = from;
                EndDate = to;

                var tillOrder = BuildTillOrder (tillsResult);
                var tillAllocations = BuildTillAllocations(allocationsResult.Object.TillAllocations, tillOrder);
                var combinedTillAllocations = CombineTillAllocations (tillAllocations, tillsResult);
                TillAllocations = UpdateTillStatus (combinedTillAllocations, tillsResult);
            }

            return this.TillAllocations;
        }

		public async Task<int?> UpdateMaxHoursAsync(int? maximumHours)
		{
			var request = new BasketSetMaxHoursRequest () 
			{
				SiteId = m_data.Site.SiteID,
				MaximumHours = maximumHours
			};

			int timeout = m_client.TimeoutMs;

			TaskGenerator taskGenerator = (token) => 
			{
				return new Task<RestReply<BasketSetMaxHoursResponse>>(() =>
					{
						return m_client.Post<BasketSetMaxHoursResponse> (request, token).Result;
					});
			};

			m_tokenSource = new CancellationTokenSource();
			var result = await PerformOperation<RestReply<BasketSetMaxHoursResponse>>(timeout, taskGenerator, m_tokenSource.Token);

			if(result.ResponseObject != null && result.ResponseObject.Success)
			{
				MaxHours = maximumHours;
			}

			return MaxHours;
		}

		public async Task<int?> GetMaxHoursAsync()
		{
			var request = new BasketGetMaxHoursRequest () 
			{
				SiteId = m_data.Site.SiteID
			};

			int timeout = m_client.TimeoutMs;

			TaskGenerator taskGenerator = (token) => 
			{
				return new Task<RestReply<BasketGetMaxHoursResponse>>(() =>
					{
						return m_client.Get<BasketGetMaxHoursResponse> (request, token).Result;
					});
			};

			m_tokenSource = new CancellationTokenSource();
			var result = await PerformOperation<RestReply<BasketGetMaxHoursResponse>>(timeout, taskGenerator, m_tokenSource.Token);

			if(result.ResponseObject != null && result.ResponseObject.Success)
			{
				MaxHours = result.ResponseObject.Object.MaxHours;
			}

			return MaxHours;
		}

        static List<TillAllocation> BuildTillAllocations(WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation[] allocations, Dictionary<int, int> tillOrder)
        {
            List<TillAllocation> tillAllocations = new List<TillAllocation>();

            foreach (var tillAllocationServiceModel in allocations)
            {
                var tillAllocation = new TillAllocation() {
                    TillAllocationId = tillAllocationServiceModel.TillAllocationId,
                    StartTime = tillAllocationServiceModel.StartTime,
                    EndTime = tillAllocationServiceModel.EndTime,
                    SiteId = tillAllocationServiceModel.SiteId,
                    SiteName = tillAllocationServiceModel.SiteName,
                    EmployeeId = tillAllocationServiceModel.EmployeeId,
                    EmployeeFullName = tillAllocationServiceModel.EmployeeFullName,
                    TillId = tillAllocationServiceModel.TillId,
                    StampedTillId = tillAllocationServiceModel.StampedTillId,
                    TillNumber = tillAllocationServiceModel.TillNumber,
                    CheckoutSupport = tillAllocationServiceModel.CheckoutSupport,
                    TillTypeKey = tillAllocationServiceModel.TillTypeKey,
                    TillStatusKey = tillAllocationServiceModel.TillStatusKey,
                    TillOpeningSequence = tillAllocationServiceModel.TillOpeningSequence,
                };

                if (tillAllocationServiceModel.StampedTillId.HasValue && tillOrder.ContainsKey(tillAllocationServiceModel.StampedTillId.Value))
                    tillAllocation.TillDisplaySequence = tillOrder[tillAllocationServiceModel.StampedTillId.Value];
                else
                    tillAllocation.TillDisplaySequence = 0;
                
                if (tillAllocationServiceModel.TillWindows != null)
                {
                    foreach (var tillWindowServiceModel in tillAllocationServiceModel.TillWindows)
                    {
                        var tillWindow = new TillAllocationTillWindow() {
                            TillAllocationTillWindowId = tillWindowServiceModel.TillAllocationTillWindowId,
                            StartTime = tillWindowServiceModel.StartTime,
                            EndTime = tillWindowServiceModel.EndTime,
                        };

                        tillAllocation.Add(tillWindow);
                    }
                }

                tillAllocations.Add(tillAllocation);
            }

            return tillAllocations;
        }

        public async Task<bool> CreateAsync(DateTime from, DateTime to)
        {
			//Clear pending changes
			m_tillAllocationUpdates.Clear();
			m_originalTillSetup.Clear ();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            var request = new CreateRequest()
                {
                    SiteId = m_data.Site.SiteID,
                    JdaSessionId = m_client.JdaSessionId,
                    From = from,
                    To = to,
                };

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<CreateResponse>>(() =>
                    {
                        return m_client.Post<CreateResponse>(request,token).Result;
                    });
                };

            var result = await PerformOperation<RestReply<CreateResponse>>(timeout, taskGenerator, new CancellationToken());

            if (result.ResponseObject != null && result.ResponseObject.Object != null && result.ResponseObject.Object.Success)
                return true;
            else
                return false;
        }

		private async Task<bool> MoveAllocations(List<WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation> changes)
        {
			if(changes.Count == 0)
				return true;

            int timeout = m_client.TimeoutMs;

            var request = new AllocationMoveRequest()
                {
                    SiteId = m_data.Site.SiteID,
                    Changes = changes
                };

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<AllocationMoveResponse>>(() =>
                        {
                            return m_client.Post<AllocationMoveResponse>(request,token).Result;
                        });
                };

            var result = await PerformOperation<RestReply<AllocationMoveResponse>>(timeout, taskGenerator, new CancellationToken());

            if (result.ResponseObject != null && result.ResponseObject.Object != null && result.ResponseObject.Object.Success)
                return true;
            else
                return false;
        }

        private void OnSignOut(SignOutMessage message)
        {
            Cancel();
            TillAllocations = null;
            StartDate = default(DateTime);
            EndDate =  default(DateTime);
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }

		public Task Save(IList<AllocationsViewModel.TillModel> tills)
		{
			List<WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation> newAllocations = new List<WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation>();

			//Collapse all pending chnages into a core changeset 
			//(Multiple commits for hte same till may be present, and the till might be put back where it was
			Dictionary<int, int> allocations = new Dictionary<int, int>();
			for (int i = m_tillAllocationUpdates.Count - 1; i >= 0; i--) 
			{
				if (allocations.ContainsKey (m_tillAllocationUpdates [i].AllocationID))
					continue;//Ignore as we've already "placed" this

				allocations.Add (m_tillAllocationUpdates [i].AllocationID, m_tillAllocationUpdates [i].TillId);
			}

			//Remove any allocations that are back where they were in the first place
			foreach (KeyValuePair<int, int> record in allocations) 
			{
				foreach (TillReallocation original in m_originalTillSetup) 
				{
					if (original.AllocationID == record.Key && original.TillId != record.Value) 
					{
						//Get Actual Till Allocation
						TillAllocation currentAllocation = null;
						foreach (TillAllocation ta in TillAllocations) 
						{
							if (ta.TillAllocationId == record.Key)
								currentAllocation = ta;
						}

						//Create Till Allocation Change and fill out allocation data
						WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation allocation = new WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation();
						allocation.TillAllocationId = record.Key;
						allocation.StartTime = currentAllocation.StartTime;
						allocation.EndTime = currentAllocation.EndTime;
						allocation.SiteId = currentAllocation.SiteId;
						allocation.SiteName = currentAllocation.SiteName;
						allocation.EmployeeFullName = currentAllocation.EmployeeFullName;
						allocation.EmployeeId = currentAllocation.EmployeeId;

						//Fill out till data
						foreach (AllocationsViewModel.TillModel till in tills) 
						{
							if (till.TillId == record.Value) 
							{
								allocation.TillId = till.TillId;
								allocation.StampedTillId = till.TillId;
								allocation.TillNumber = till.TillNumber;
								allocation.CheckoutSupport = till.CheckoutSupport;
								allocation.TillTypeKey = till.TillTypeKey;
								allocation.TillStatusKey = till.TillStatusKey;
								allocation.TillOpeningSequence = till.OpeningSequence;
                                allocation.TillDisplaySequence = till.DisplaySequence;

								//Copy windows
								List<WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocationTillWindow> windows = new List<WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocationTillWindow>();

								foreach (AllocationsViewModel.TillWindowModel window in till.TillWindows) 
								{
									windows.Add (new WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocationTillWindow () { StartTime = window.StartTime,
										EndTime = window.EndTime,
										TillAllocationTillWindowId = window.TillAllocationTillWindowId,
									});
								}

								allocation.TillWindows = windows.ToArray ();
							}
						}

						newAllocations.Add (allocation);
					}
				}
			}

			//Clear
			m_tillAllocationUpdates.Clear();

			return MoveAllocations(newAllocations);
		}

		public bool IsEdited
		{
			get
			{
				return m_tillAllocationUpdates.Count > 0;
			}
		}


		public bool UpdateTillAllocation(int allocationId, int tillId)
		{
			//Is this till where we are already anyway?
			int currentTillId = TillAllocation.NewBufferTillId;
			for (int i = m_tillAllocationUpdates.Count - 1; i >= 0; i--) 
			{
				if (m_tillAllocationUpdates [i].AllocationID == allocationId) 
				{
					currentTillId = m_tillAllocationUpdates [i].TillId;
					break;
				}
			}

			if (currentTillId == TillAllocation.NewBufferTillId) 
			{
				foreach (TillAllocation allocation in TillAllocations) 
				{
					if (allocation.TillAllocationId == allocationId) 
					{
						//Store off original if its not already there
						currentTillId = (int)allocation.StampedTillId;

						bool found = false;
						foreach (TillReallocation original in m_originalTillSetup) 
						{
							if (original.AllocationID == allocationId) 
							{
								found = true;
								break;
							}
						}

						if (!found)
							m_originalTillSetup.Add (new TillReallocation {
								AllocationID = allocationId,
								TillId = (int)allocation.StampedTillId
							});
					}
				}
			}

			//Don't set if its not changing anything
			if (currentTillId == tillId)
				return false;


			m_tillAllocationUpdates.Add (new TillReallocation { AllocationID = allocationId, TillId = tillId });

			//Update for the UI to visually look "right"
			foreach (TillAllocation allocation in TillAllocations) 
			{
				if (allocation.TillAllocationId == allocationId)
				{
					allocation.TillId = tillId;
					allocation.StampedTillId = tillId;
				}
			}

			return true;
		}

		public void UndoAll()
		{
			bool revert = true;
			while (revert) 
			{
				revert = Undo();
			}

			//Should be clear anyway but clear
			m_tillAllocationUpdates.Clear ();
			m_originalTillSetup.Clear ();
		}

		public bool Undo()
		{
			if (m_tillAllocationUpdates.Count == 0)
				return false;

			//Remove last one
			TillReallocation realloc = m_tillAllocationUpdates[m_tillAllocationUpdates.Count - 1];
			m_tillAllocationUpdates.RemoveAt (m_tillAllocationUpdates.Count - 1);

			//Revert
			int newTillID = TillAllocation.NewBufferTillId;
			for (int i = m_tillAllocationUpdates.Count - 1; i >= 0; i--) 
			{
				if (m_tillAllocationUpdates [i].AllocationID == realloc.AllocationID) 
				{
					newTillID = m_tillAllocationUpdates [i].TillId;
					break;
				}
			}

			if (newTillID == TillAllocation.NewBufferTillId) 
			{
				//Revert back to original
				foreach (TillReallocation original in m_originalTillSetup) 
				{
					if (original.AllocationID == realloc.AllocationID) 
					{
						newTillID = original.TillId;
					}
				}
			}


			//Set
            if (TillAllocations != null)
            {
                foreach (TillAllocation alloc in TillAllocations) 
                {
                    if (alloc.TillAllocationId == realloc.AllocationID) 
                    {
                        alloc.TillId = newTillID;
                        alloc.StampedTillId = newTillID;
                    }
                }
            }

			return true;
		}

        List<TillAllocation> CombineTillAllocations(List<TillAllocation> allocations, IList<Till> tills)
        {
            List<TillAllocation> combined = new List<TillAllocation> (allocations);

            DateTime date = StartDate.Date;

            int basketTypeId = m_staticData.TillTypes.Single(t => t.Key == TillTypeKey.Basket).TillTypeId;
            int openStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.Open).TillStatusId;
            int outOfOrderStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.OutOfOrder).TillStatusId;
            int unusedStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.Unused).TillStatusId;

            do
            {
                foreach (var till in tills)
                {

                    int tillDisplaySequence = till.OpeningSequence;
                    int unallocatedTillOffset = int.MaxValue / 2;
                    int closedTillOffset = unallocatedTillOffset + (int.MaxValue / 4);

                    String tillType = TillTypeKey.MainBank;
                    if(till.TypeId == basketTypeId)
                    {
                        tillType = TillTypeKey.Basket;
                    }

                    String statusType = TillStatusKey.Unused;
                    if(till.StatusId == openStatusId)
                    {
                        statusType = TillStatusKey.Open;
                        tillDisplaySequence = till.OpeningSequence + unallocatedTillOffset;
                    }
                    else if(till.StatusId == outOfOrderStatusId || till.StatusId == unusedStatusId)
                    {
                        statusType = TillStatusKey.OutOfOrder;
                        tillDisplaySequence = till.OpeningSequence + closedTillOffset;
                    }


                    var tillAllocation = new TillAllocation ()
                    {
                        TillAllocationId = 0,
                        StartTime = date,
                        EndTime = date,
                        SiteId = m_data.Site.SiteID,
                        SiteName = m_data.Site.Name,
                        EmployeeId = 0,
                        EmployeeFullName = String.Empty,
                        TillId = null,
                        StampedTillId = till.TillId,
                        TillNumber = till.TillNumber,
                        CheckoutSupport = false,
                        TillTypeKey = tillType,
                        TillStatusKey = statusType,
                        TillOpeningSequence = till.OpeningSequence,
                        TillDisplaySequence = tillDisplaySequence,
                    };

                    if(till.Windows != null)
                    {
                        foreach (var tillWindowServiceModel in till.Windows)
                        {
                            var tillWindow = new TillAllocationTillWindow ()
                                {
                                    TillAllocationTillWindowId = tillWindowServiceModel.TillWindowId,
                                    StartTime = tillWindowServiceModel.StartTime - tillWindowServiceModel.StartTime.Date,
                                    EndTime = tillWindowServiceModel.EndTime - tillWindowServiceModel.StartTime.Date,
                                };

                            tillAllocation.Add (tillWindow);
                        }  
                    }
                    combined.Add (tillAllocation);
                }
                date = date.AddDays(1);
            } while(date <= EndDate);
                
            return combined;
        }

        Dictionary<int, int> BuildTillOrder(IList<Till> tills)
        {
            Dictionary<int, int> tillOrder = new Dictionary<int, int> ();
            foreach(var till in tills)
            {
                tillOrder.Add (till.TillId, till.OpeningSequence);
            }

            return tillOrder;
        }

        List<TillAllocation> UpdateTillStatus(List<TillAllocation> allocations, IList<Till> tills)
        {
            int openStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.Open).TillStatusId;
            int outOfOrderStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.OutOfOrder).TillStatusId;
            int unusedStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.Unused).TillStatusId;

            List<TillAllocation> returnAllocations = new List<TillAllocation>(allocations);
            foreach(var allocation in returnAllocations)
            {
                if (allocation.StampedTillId.HasValue)
                {
                    foreach (var till in tills)
                    {
                        if (till.TillId == allocation.StampedTillId)
                        {
                            if (till.StatusId == openStatusId)
                            {
                                allocation.TillStatusKey =  TillStatusKey.Open;
                            }
                            else if (till.StatusId == outOfOrderStatusId || till.StatusId == unusedStatusId)
                            {
                                allocation.TillStatusKey =  TillStatusKey.OutOfOrder;
                            }
                            continue;
                        }
                    }
                }
            }

            return returnAllocations;
        }


        //Helper functions for dealing with the schedule service and getting it into the correct FILTER and DATA if necessary
        public bool CheckoutFilterApplied
        {
            get
            {
                return m_scheduleService.CurrentFilter != null
                    && m_scheduleService.CurrentFilter.Items != null
                    && m_scheduleService.CurrentFilter.Type == FilterData.WORKGROUP_FILTER_CODE
                    && m_scheduleService.CurrentFilter.Items.Count (x => x.Selected) > 0//TODO we don't really know how many things *should* be selected...
                    && m_scheduleService.CurrentFilter.Items.Where(x => x.Selected).All(x => WorkgroupIsRelevant(x.Name))
                    && m_scheduleService.CurrentFilter.MemoryFilter != null
                    && m_scheduleService.CurrentFilter.MemoryFilter.ScheduledSelected
                    && m_scheduleService.CurrentFilter.MemoryFilter.UnscheduledSelected;
            }
        }

        private bool CanApplyCheckoutFilter
        {
            get
            {
                if(m_scheduleService.Filters != null && m_scheduleService.Filters.Groups != null)
                {
                    FilterGroup workgroupFilter = m_scheduleService.Filters.Groups.SingleOrDefault (g => g.Type == FilterData.WORKGROUP_FILTER_CODE);

                    if(workgroupFilter != null)
                    {
                        return workgroupFilter.Items.Any(item => WorkgroupIsRelevant(item.Name));
                    }
                }

                return false;
            }
        }   

        public bool ReloadScheduleIfNotCorrect()
        {
            if (m_scheduleService.IsBusy)
                return true;

            //We have to re-request the data with the correct filter as the first load is to get the list of filters available to us
            if (!CheckoutFilterApplied && CanApplyCheckoutFilter)
            {
                var filter = FindCheckoutFilter ();
                m_scheduleService.Cancel ();
                m_scheduleService.ClearData ();
                m_scheduleService.CurrentFilter = filter;
                m_scheduleService.FilterNeedsResetOnRefresh = true;
                m_scheduleService.Refresh (false);
                return true;
            }
            else
                return false;
        }

        private FilterGroup FindCheckoutFilter()
        {
            FilterGroup workGroup = m_scheduleService.Filters.Groups.SingleOrDefault (g => g.Type == FilterData.WORKGROUP_FILTER_CODE).Clone();

            foreach (var filter in workGroup.Items)
            {
                filter.Selected = WorkgroupIsRelevant (filter.Name);
            }

            return workGroup;
        }

        public bool IsEmployeeInRelevantTillJob(EmployeeData e)
        {
            if (e == null)
                return false;

            foreach(var job in e.CanWorkJobs)
            {
                string jobName = string.Empty;

                if(m_scheduleService != null && m_scheduleService.JobNameLookup != null)
                    m_scheduleService.JobNameLookup.TryGetValue(job.Key, out jobName);


                if (JobIsRelevant (jobName))
                    return true;
            }

            return false;
        }

        public string GetSSCSAYSDisplayName(string jobName)
        {
            if (SSCSAYSConfigurations == null)
                return jobName;

            foreach (var v in SSCSAYSConfigurations)
            {
                if (v.JobName.ToLower () == jobName.ToLower ())
                    return v.DisplayName;
            }

            return jobName;
        }

        public bool JobIsRelevant(string name, bool includeCheckout = true)
        {
            if (string.IsNullOrEmpty (name))
                return false;

            return (includeCheckout && name.ToLower ().Contains ("checkout")) || SSCSAYSConfigurations.Any(x => x.JobName.ToLower() == name.ToLower());
        }

        public bool WorkgroupIsRelevant(string name, bool includeCheckout = true)
        {
            if (string.IsNullOrEmpty (name))
                return false;

            return (includeCheckout && name.ToLower ().Contains ("checkout")) || SSCSAYSConfigurations.Any(x => x.WorkgroupName.ToLower() == name.ToLower());
        }
    }
}