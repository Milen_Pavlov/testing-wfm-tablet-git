﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using WFM.Core.MyHours;
using System.Collections.Generic;
using System.Threading;

namespace WFM.Core
{
    [ConsortiumService]
    public class MyHoursDataService
    {
        public static string DecimalSpecifier = "#,0.##";
        public static string IntegerSpecifier = "#,0";
        public static string PercentageSpecifier = "P1";

        private readonly MyHoursService m_myHoursService;
        private readonly ICustomAlertService m_customAlertBox;
        private readonly IAlertBox m_alertBox;
        private readonly IUndoService m_undoService;
        private readonly IStringService m_localiser;
        private readonly IRaygunService m_raygunService;
        private readonly ScheduleService m_schedule;

        public event EventHandler OnDataUpdated;
        public event EventHandler OnBeforeUndo;
        public event EventHandler OnAfterUndo;

        private WeekData m_currentWeekData;
        public WeekData CurrentWeekData
        {
            get { return m_currentWeekData; }
            set { m_currentWeekData = value; }
        }

        private DateTime m_weekStart;
        public DateTime WeekStart
        {
            get { return m_weekStart; }
            set { m_weekStart = value; }
        }

        private MyHoursCellData m_editingCell;
        public MyHoursCellData EditingCell
        {
            get { return m_editingCell; }
            set { m_editingCell = value; }
        }

        public CancellationTokenSource m_tokenSource = new CancellationTokenSource ();
        public CancellationTokenSource m_resetTokenSource = new CancellationTokenSource ();

        public MyHoursDataService (
            IAlertBox alertBox,
            IStringService localiser,
            ICustomAlertService customAlertService,
            MyHoursService myHoursService,
            ScheduleService schedule,
            IUndoService undoService,
            IRaygunService raygunService)
        {
            m_myHoursService = myHoursService;
            m_alertBox = alertBox;
            m_customAlertBox = customAlertService;
            m_undoService = undoService;
            m_localiser = localiser;
            m_raygunService = raygunService;
            m_schedule = schedule;

            m_undoService.OnUndo += OnUndo;
        }

        public async void FetchData(DateTime weekStart)
        {
            if (m_tokenSource != null)
                m_tokenSource.Cancel ();

            m_tokenSource = new CancellationTokenSource ();
            var token = m_tokenSource.Token;

            m_undoService.ClearHistory ();

            WeekStart = weekStart;
            int year = m_schedule.AccountingHelper.GetStartOfAccountingYear(WeekStart).Year;
            int week = m_schedule.AccountingHelper.GetWeekNumberForDate(WeekStart);
            var result = await m_myHoursService.LoadData (year, week, token);

            if (token.IsCancellationRequested)
                return;
            
            if (result.Success && result.Object != null)
            {
                CurrentWeekData = result.Object;
                if (CurrentWeekData.HasCentralMadeAnyChanges ())
                {
                    // Show alertbox.
                    m_alertBox.ShowOK (m_localiser.Get ("my_hours_central_changes_header"), m_localiser.Get ("my_hours_central_changes_body"));
                }
            }
            else
            {
                CurrentWeekData = null;
                m_alertBox.ShowOK (m_localiser.Get ("my_hours_fetch_error_header"), string.Format (m_localiser.Get ("my_hours_fetch_error_body"), week, year, result.Code, result.Message));
            }
        

            if (OnDataUpdated != null)
                OnDataUpdated (CurrentWeekData, EventArgs.Empty);
        }

        public async void ResetData()
        {          
            if (m_resetTokenSource != null)
                m_resetTokenSource.Cancel ();

            m_resetTokenSource = new CancellationTokenSource ();
            var token = m_resetTokenSource.Token;

            m_undoService.ClearHistory ();

            int year = m_schedule.AccountingHelper.GetStartOfAccountingYear(WeekStart).Year;
            int week = m_schedule.AccountingHelper.GetWeekNumberForDate(WeekStart);
            var result = await m_myHoursService.ResetData (year, week, token);

            if (result.Success && result.Object != null)
            {
                CurrentWeekData = result.Object;
            }
            else
            {
                m_alertBox.ShowOK (m_localiser.Get("my_hours_fetch_error_header"), string.Format(m_localiser.Get("my_hours_reset_error_body"), week, result.Code));
            
                //Dump full error to Raygun
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add ("Message", result.Message);
                data.Add ("Code", result.Code);
                data.Add ("ErrorSource", result.ErrorSource);
                data.Add ("Object (WeekData)", result.Object);
                data.Add ("Week", week);
                data.Add ("Year", year);
                m_raygunService.LogCustomError(new Exception("MyHours Fetch Failed"), data);
            }


            if (OnDataUpdated != null)
                OnDataUpdated (CurrentWeekData, EventArgs.Empty);
        }

        public async void SubmitData()
        {            
            int year = m_schedule.AccountingHelper.GetStartOfAccountingYear(WeekStart).Year;
            int week = m_schedule.AccountingHelper.GetWeekNumberForDate(WeekStart);

            Result result = await m_myHoursService.SaveData (CurrentWeekData);

            if (!result.Success)
            {
                m_alertBox.ShowOK (m_localiser.Get("my_hours_fetch_error_header"), string.Format(m_localiser.Get("my_hours_submit_error_body"), week, result.Code));

                //Dump full error to Raygun
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add ("Message", result.Message);
                data.Add ("Code", result.Code);
                data.Add ("ErrorSource", result.ErrorSource);
                data.Add ("Data Sent (WeekData)", CurrentWeekData);
                data.Add ("Week", week);
                data.Add ("Year", year);
                m_raygunService.LogCustomError(new Exception("MyHours Save Failed"), data);

                if (OnDataUpdated != null)
                    OnDataUpdated (CurrentWeekData, EventArgs.Empty);
            }
            else
            {
                //Reload
                FetchData (this.WeekStart);
            }
        }

        public void ShowEditDialogueForCell(MyHoursCellData data)
        {
            if (!data.IsEditable || EditingCell != null)
                return;

            //Crappy hack - Take the text value (as we're secretly round) but this means remove the %
            var text = data.Text.Replace("%",string.Empty).Trim();

            EditingCell = data;
            m_customAlertBox.ShowMyHoursInput
            (
                data.ColumnTitle, 
                data.RowTitle, 
                text, 
                (x, s) => HandleDataValueChanged (data, x, s)
            );
        }

        public void HandleDataValueChanged(MyHoursCellData data, AlertReturn buttonSelected, string valueReturned)
        {
            EditingCell = null;

            if (buttonSelected == AlertReturn.Positive)
            {
                //Validate the input
                string error = string.Empty;

                decimal val = 0;
                decimal maxVal = decimal.MaxValue;
                decimal minVal = 0;
                decimal total = decimal.MaxValue;

                if (string.IsNullOrEmpty (valueReturned))
                {
                    error = m_localiser.Get("my_hours_input_invalid");
                }
                else if (!decimal.TryParse (valueReturned, out val))
                {
                    error = string.Format (m_localiser.Get("my_hours_input_invalid_value"), valueReturned);
                }
                else
                {
                    //Set min/max values
                    switch (data.EditableType)
                    {
                        case EditableCellType.WeekPlannedHours:
                            maxVal = m_currentWeekData.GetMaxPlannedHours (data.Dept.DepartmentCode);
                            minVal = m_currentWeekData.GetMinPlannedHours (data.Dept.DepartmentCode);
                            total = m_currentWeekData.PlannedHours;
                            break;

                        case EditableCellType.DayPercentage:
                            maxVal = data.Dept.GetMaxDayPercentage (data.Day.Value);
                            minVal = data.Dept.GetMinDayPercentage (data.Day.Value);
                            total = 100;
                            break;
                    }

                    if (val < minVal)
                        error = string.Format (m_localiser.Get("my_hours_input_invalid_value_too_small"), valueReturned, minVal.ToString(DecimalSpecifier), total.ToString(DecimalSpecifier));

                    if (val > maxVal)
                        error = string.Format (m_localiser.Get("my_hours_input_invalid_value_too_large"), valueReturned, maxVal.ToString(DecimalSpecifier), total.ToString(DecimalSpecifier));
                }

                if (!string.IsNullOrEmpty (error))
                {
                    m_alertBox.Show (m_localiser.Get("my_hours_input_invalid_header"), error, m_localiser.Get("my_hours_input_invalid_retry"), m_localiser.Get("my_hours_input_invalid_cancel"), (int i) =>
                        {
                            if (i == 1)
                            {
                                ShowEditDialogueForCell (data);
                            }
                        });
                }
                else
                {
                    CommitChange (data, data.Day, val);
                }
            }
        }

        void CommitChange (MyHoursCellData data, DayOfWeek? day, decimal val)
        {
            //Build Undo event
            MyHoursUndoStackOperation undoOp = new MyHoursUndoStackOperation ();
            undoOp.Before = CurrentWeekData.Clone ();
            undoOp.EditableType = data.EditableType;
            undoOp.DeptCode = data.Dept.DepartmentCode;
            undoOp.Day = day;
            undoOp.After = val;
            //Store Undo
            m_undoService.Store (undoOp);
            //Update value and refresh data
            switch (data.EditableType)
            {
                case EditableCellType.WeekPlannedHours:
                    CurrentWeekData.SetDepartmentHours (data.Dept.DepartmentCode, val);
                    break;
                case EditableCellType.DayPercentage:
                    data.Dept.SetDayPercentage (day.Value, val / 100m);
                    break;
            }
            if (OnDataUpdated != null)
                OnDataUpdated (CurrentWeekData, EventArgs.Empty);
        }

        public void OnUndo(object sender, EventArgs e)
        {
            MyHoursUndoStackOperation op = sender as MyHoursUndoStackOperation;

            if (op == null)
                throw new Exception ("Undo operation was null");

            if (OnBeforeUndo != null)
                OnBeforeUndo (op, EventArgs.Empty);

            //Set new base
            CurrentWeekData = op.Before.Clone();

            if (OnDataUpdated != null)
                OnDataUpdated (CurrentWeekData, EventArgs.Empty);

            if (OnAfterUndo != null)
                OnAfterUndo (op, EventArgs.Empty);
        }
    }
}