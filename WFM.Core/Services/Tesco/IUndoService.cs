﻿using System;

namespace WFM.Core
{
    public interface IUndoService
    {
        event EventHandler OnUndo;
        bool CanUndo { get; }
        void Store(UndoStackOperation change);
        void ClearHistory();
        void Undo();
        UndoStackOperation Peek();
    }
}

