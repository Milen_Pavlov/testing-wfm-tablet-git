﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using WFM.Core.NightAisle;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Core
{
    public interface INightAisleService
    {
        Task<Result<IEnumerable<Aisle>>> GetAisleConfiguration(int forSiteID, CancellationToken token);
        Task<Response> SetAisleConfiguration(int forSiteID, IEnumerable<Aisle> configuration);
        Task<Result<IEnumerable<AisleMapping>>> GetMappingsForWeekStarting(int forSiteID, DateTime forWeekStarting, CancellationToken token);
        Task<Response> SetMappingsForWeekStarting(int forSiteID, DateTime forWeekStarting, IList<NightAisleModel> aisleMappings);
        Task<Result<Tuple<double, double>>> GetReplenishmentStartAndEndTime(int forSiteID, CancellationToken token);
        Task<Response> SetReplenishmentStartAndEndTime(int forSiteID, double start, double end);
    }
}

