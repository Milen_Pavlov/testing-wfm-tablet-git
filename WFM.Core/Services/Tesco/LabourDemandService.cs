﻿using System.Threading.Tasks;
using Consortium.Client.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class LabourDemandService : ILabourDemandService
    {
        private readonly LabourDemandRestClient m_restClient;

        public LabourDemandService (LabourDemandRestClient restClient)
        {
            m_restClient = restClient;
        }

        public async Task<Result<LabourDemandResponse>> GetAbsenceTargetAsPercentage (string storeId, string weekNumber)
        {
            var request = m_restClient.Get<LabourDemandResponse>(new AbsenceTargetRequest
            {
                StoreID = storeId,
                WeekNumber = weekNumber
            });

            var minDelay = Task.Delay (500);
            await minDelay;

            var response = await request;
            var result = new Result<LabourDemandResponse>();

            if(!response.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.REST;
                result.Code = response.Code;
                result.Message = response.Message;
                result.Object = new LabourDemandResponse();
            }
            else if(response.Object == null)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.LABOUR_DEMAND;
                result.Code = -1;
                result.Message = "Empty Response";
            }
            else
            {
                result.Success = true;
                result.Object = response.Object;
            }

            return result;
        }
    }

    public interface ILabourDemandService
    {
        Task<Result<LabourDemandResponse>> GetAbsenceTargetAsPercentage(string storeId, string weekNumber);
    }
}

