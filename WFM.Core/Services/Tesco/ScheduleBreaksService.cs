﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using WFM.Shared.DataTransferObjects.Breaks;
using System.Threading.Tasks;
using AutoMapper;
using System.Linq;
using Consortium.Client.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleBreaksService: IEditModeLink
    {
        private readonly BreaksService m_breaksService;
        private readonly IMvxMessenger m_messenger;
        protected MvxSubscriptionToken m_signOutToken;
        private readonly SessionData m_sessionData;

        //Stateful members that control the data currently being shown to the user
        private IList<BreakRecord> m_originalState;
        private IList<BreakRecord> m_state;
        public bool Cancelled;
        public bool Timeout;

        private Dictionary<int, BreakRecord> m_breakStatusUpdated = new Dictionary<int, BreakRecord>();

        public bool IsEdited
        {
            get
            {
                return m_breakStatusUpdated.Count > 0;
            }
        }

        public ScheduleBreaksService(
            BreaksService breaksService, 
            IMvxMessenger messenger,
            SessionData sessionData)
        {
            m_breaksService = breaksService;
            m_messenger = messenger;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_sessionData = sessionData;
        }

        public async Task<IList<BreakRecord>> ReadBreaks(List<int> ShiftIDs, bool forceUpdate)
        {
            if (forceUpdate || m_state == null)
            {
                m_breakStatusUpdated.Clear();

                var result = await m_breaksService.SafeReadBreaks(ShiftIDs);

                if(result != null)
                {
                    Cancelled = result.Cancelled;
                    Timeout = result.TimeOut;

                    m_originalState = new List<BreakRecord>();

                    m_state = result.ResponseObject;

                    m_originalState = Mapper.Map<IList<BreakRecord>>(m_state);
                }
            }           

            return m_state;
        }

        public void RecordChange(int breakID, int shiftID, bool breakTaken)
        {
            bool sendMessage = true;

            if (IsEdited)
                sendMessage = false;

            if (m_breakStatusUpdated.ContainsKey(breakID))
            {
                m_breakStatusUpdated[breakID].BreakTaken = breakTaken;
                m_breakStatusUpdated[breakID].ShiftID = shiftID;
            }
            else
            {
                m_breakStatusUpdated.Add(breakID, new BreakRecord() 
                { 
                    BreakID = breakID, 
                    BreakTaken = breakTaken,
                    SiteID = m_sessionData.Site.SiteID,
                    ShiftID = shiftID
                });                   
            }

            if(sendMessage)
                m_messenger.Publish(new OnEditModeMessage(this));
        }

        #region IEditModeLink implementation

        public Task Save()
        {
            List<BreakRecord> changes = new List<BreakRecord>();

            foreach (KeyValuePair<int, BreakRecord> record in m_breakStatusUpdated)
            {
                changes.Add(record.Value);
            }

            m_breakStatusUpdated.Clear();

            return m_breaksService.SetBreaks(changes);
        }

        public void Reset()
        {
            m_breakStatusUpdated.Clear();
            m_state = null;
        }

        #endregion

        private void OnSignOut(SignOutMessage message)
        {
            Cancel();
            m_state = null;
            m_originalState = null;
            m_breakStatusUpdated = new Dictionary<int, BreakRecord>();
        }

        public void Cancel()
        {
            m_breaksService.Cancel();
        }
    }
}

