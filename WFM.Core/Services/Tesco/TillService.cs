using Consortium.Client.Core;
using Consortium.Client.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Core.Model;
using WFM.Shared.DataTransferObjects.Till;
using WFM.Shared.DataTransferObjects.Shared;
using WFM.Shared.Extensions;
using WFM.Core.RestClients;
using WFM.Shared.Constants;
using System.Threading;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core.Models;

namespace WFM.Core.Services
{
    [ConsortiumService]
    public class TillService : ManagedRestService
    {
        public const int c_minimumRequiredWindowsForBasketTills = 3;
        public RestClient m_client { get; set; }
        private readonly IAlertBox m_alert;
        private readonly IStringService m_localiser;
        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();
        protected MvxSubscriptionToken m_signOutToken;

        // Graceful request failures
        public bool FirstSuccessfulLoad;
        public bool Loading;

        public bool IsEdited;
        public bool IsSaving;

        public TillService(
            ConsortiumRestClient client,
            IMvxMessenger messenger,
            IAlertBox alert,
            IMobileDevice mobileDevice,
            IStringService localiser)
        {
            m_client = client;
            m_alert = alert;
            m_localiser = localiser;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            Loading = false;
        }

        public async Task<IList<Till>> SafeReadAsync(int siteId)
        {
            Loading = true;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<IList<Till>>(() =>
                        {
                            return ReadAsync(siteId,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();
            ManagedRestResponse<IList<Till>> response = await PerformOperation<IList<Till>>(m_client.TimeoutMs, taskGenerator, m_tokenSource.Token);

            Loading = false;

            if (!response.TimeOut && !response.Cancelled)
            {
                return response.ResponseObject;
            }
            else
            {
                return null;
            }
        }

        public async Task<IList<Till>> ReadAsync(int siteId, CancellationToken token)
        {
            var request = new ReadBySiteRequest()
            {
                SiteId = siteId
            };
                        
            var response = await m_client.Get<ReadBySiteResponse>(request, token);

            token.ThrowIfCancellationRequested();

            if(!response.Success || !response.Object.Success)
            {
                CancelledBySiesta = true;
                return null;
            }

            var tills = new List<Till>();
            foreach (var t in response.Object.Tills)
            {
                var till = new Till()
                {
                    TillId = t.TillId,
                    TillNumber = t.TillNumber,
                    OpeningSequence = t.OpeningSequence,
                    SiteId = t.SiteId,
                    StatusId = t.StatusId,
                    TypeId = t.TypeId,
                };
                tills.Add(till);

                if (t.Windows != null)
                {
                    foreach (var w in t.Windows)
                    {
                        var window = new TillWindow()
                        {
                            TillWindowId = w.TillWindowId,
                            StartTime = DateTime.SpecifyKind(w.StartTime, DateTimeKind.Utc),
                            EndTime = DateTime.SpecifyKind(w.EndTime, DateTimeKind.Utc),
                        };
                        till.Add(window);
                    }
                }
            }

            return tills;
        }

        public async Task<bool> SafeWriteAsync(IList<Till> tills)
        {
            TaskGenerator taskGenerator = (token) =>
                {
                    return new Task<TillWriteEndMessage>(() =>
                        {
                            return WriteAsync(tills,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();
            var response = await PerformOperation<TillWriteEndMessage>(m_client.TimeoutMs, taskGenerator, m_tokenSource.Token);

            if (response.Cancelled || response.TimeOut) // Request cancelled/timeout - leave in edited state
            {
                return false;
            }
            else if(!response.ResponseObject.Success) // Request rejected - force data reload
            {
                FirstSuccessfulLoad = false;
            }

            return response.ResponseObject.Success;
        }

        public async Task<bool> SafeWriteReadAsync(IList<Till> tills, int site)
        {
            var writeOperation = await SafeWriteAsync(tills);

            if(!FirstSuccessfulLoad)
            {
                await SafeReadAsync(site);
            }

            return writeOperation;
        }

        public async Task<TillWriteEndMessage> WriteAsync(IList<Till> tills, CancellationToken token)
        {
            // Validate
            if (!tills.Any())
            {
                return new TillWriteEndMessage(this, true);
            }

            var siteIds = tills.Select(t => t.SiteId).Distinct().ToList();
            if (siteIds.Count != 1)
            {
                return new TillWriteEndMessage(this, false);
            }

            // Create or update tills
            var newOrEditedTills = tills.
                _NewIfNull().
                ToList().Where(till => !till.State.DeletedInMemory );

            //var tillOrderAtSave = tills.Select(till => till.TillId);

            int openingSequence = 0;

            var saveRequest = new SaveManyRequest()
            {
                SiteId = siteIds.Single(),
                Tills = newOrEditedTills.
                    Select(t => new SaveManyRequest.Till()
                    {
                        TillId = t.TillId,
                        TillNumber = t.TillNumber,
                        StatusId = t.StatusId,
                        TypeId = t.TypeId,
                        OpeningSequence = openingSequence++,
                        Windows = t.
                            Windows.
                            _NewIfNull().
                            Select(w => new SaveManyRequest.TillWindow()
                            {
                                StartTime = DateTime.SpecifyKind(w.StartTime,DateTimeKind.Utc),
                                EndTime = DateTime.SpecifyKind(w.EndTime,DateTimeKind.Utc)
                            }).
                            ToArray(),
                    }).
                    ToArray()
            };

            var saveResponse = await m_client.Post<SaveManyResponse>(saveRequest, token);

            token.ThrowIfCancellationRequested();

            if (!saveResponse.Success || !saveResponse.Object.Success)
            {
                // If any of the operations were cancelled then return without warning
                if (saveResponse.IsCancelled)
                {
                    CancelledBySiesta = true;
                    return new TillWriteEndMessage(this, false);
                }
                else
                {
                    string errorMessages = string.Empty;
                    foreach(var error in saveResponse.Object.Messages)
                    {
                        if(error.Severity == MessageSeverity.Error)
                        {
                            errorMessages += "• " + error.FriendlyMessage + "\n";
                        }
                    }
                    m_alert.ShowOK(m_localiser.Get("till_validation_error_title"), errorMessages);

                    return new TillWriteEndMessage(this, false);
                }
            }

            // Delete tills
            var deletedTillIds = tills.
                _NewIfNull().
                Where(t => t.State.NeedsDeletingInDatabase).
                Select(t => t.TillId).
                ToList();
            
            if(deletedTillIds.Any())
            {
                var deleteRequest = new DeleteManyRequest()
                {
                    TillIds = deletedTillIds.ToArray()
                };
                
                var deleteResponse = await m_client.Delete<DeleteManyResponse>(deleteRequest);
                if (!deleteResponse.Success || !deleteResponse.Object.Success)
                {
                    m_alert.ShowOK(m_localiser.Get("till_validation_error_title"), m_localiser.Get("till_validation_error_delete_body"));
                    return new TillWriteEndMessage(this, false);
                }
            }

            return new TillWriteEndMessage(this, true);
        }

        public async Task<bool> UpdateOpeningSequenceAsync(List<int> tillIdsOrderedByOpeningSequence, int siteId)
        {
            var messages = new List<Message>();
            var request = new UpdateOpeningSequencesRequest()
            {
                SiteId = siteId,
                TillIdsOrderedByOpeningSequence = tillIdsOrderedByOpeningSequence.ToArray(),
            };

            var response = await m_client.Post<UpdateOpeningSequencesResponse>(request);
            messages.AddRange(response.Object.Messages);
            return response.Success;
        }

        private void OnSignOut(SignOutMessage message)
        {
            Cancel();
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }
    }
}