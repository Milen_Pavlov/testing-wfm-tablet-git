﻿using System;
using Consortium.Client.Core;
using WFM.Core.RestClients;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Breaks;
using System.Collections.Generic;
using System.Threading;
using Cirrious.MvvmCross.Plugins.Messenger;
using Siesta;

namespace WFM.Core
{
    [ConsortiumService]
    public class BreaksService: ManagedRestService
    {
        private readonly ConsortiumRestClient m_client;
        private readonly SessionData m_sessionData;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public BreaksService(
            ConsortiumRestClient client, 
            SessionData sessionData)
        {
            m_client = client;
            m_sessionData = sessionData;
        }

        public async Task<ManagedRestResponse<IList<BreakRecord>>> SafeReadBreaks(List<int> shiftIDs)
        {
            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<IList<BreakRecord>>(() =>
                    {
                        return ReadBreaks(shiftIDs,token).Result;
                    });
            };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<IList<BreakRecord>>(timeout, taskGenerator,m_tokenSource.Token);

            return result;
        }

        public async Task<IList<BreakRecord>> ReadBreaks(List<int> shiftIDs, CancellationToken token)
        {
            ReadBreaksRequest request = new ReadBreaksRequest();
            request.ShiftIDs = shiftIDs;
            request.SiteId = m_sessionData.Site.SiteID;

            var response = await m_client.Post<ReadBreaksResponse>(request,token);

            token.ThrowIfCancellationRequested();

            if (response.Success)
            {
                return response.Object.Breaks ?? new List<BreakRecord>();
            }
            else
            {
                return new List<BreakRecord>();
            }
        }

        public async Task<bool> SetBreaks(List<BreakRecord> breakItems)
        {
            //Perform Op
            int timeout = m_client.TimeoutMs;

            SetBreaksRequest request = new SetBreaksRequest();
            request.Breaks = breakItems;

            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<RestReply<SetBreaksResponse>>(() =>
                    {
                        return m_client.Post<SetBreaksResponse>(request,token).Result;
                    });
            };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<SetBreaksResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null && result.ResponseObject.Object != null)
                return true;
            else
                return false;
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }
    }
}