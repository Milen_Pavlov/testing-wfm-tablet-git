﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using WFM.Core.RestClients;
using WFM.Core.MyHours;
using Cirrious.CrossCore;
using System.Linq;
using WFM.Shared.Constants;
using System.Text;

namespace WFM.Core
{ 
    public class StoreCombinedDeptSetup
    {
        public List<LabourDemandDepartment> Combined { get; set; }
        public List<LabourDemandDepartment> Uncombined { get; set; }

        public StoreCombinedDeptSetup()
        {
            Combined = new List<LabourDemandDepartment> ();
            Uncombined = new List<LabourDemandDepartment> ();
        }
    }

    [ConsortiumService]
    public class CombinedDeptService
    {
        private readonly CombinedDeptRestClient m_restClient;
        private readonly IFileSystem m_file;
        private readonly IAlertBox m_alertService;
        private readonly IRaygunService m_raygunService;
        private readonly IStringService m_localiser;

        //TODO Confirm what "deparment" codes/names/ids this service even uses
        const string c_departmentMappingFile = "assets://Theme/DepartmentCodeMappings.json";
        private Dictionary<string,string> m_departmentMappings = new Dictionary<string,string>(StringComparer.OrdinalIgnoreCase);

        public CombinedDeptService (CombinedDeptRestClient restClient, 
            IFileSystem fileSystem, 
            IAlertBox alertService, 
            IRaygunService raygunService,
            IStringService localiser)
        {
            m_file = fileSystem;
            m_restClient = restClient;
            m_alertService = alertService;
            m_raygunService = raygunService;
            m_localiser = localiser;

            LoadDepartmentMappings ();
        }

        private void LoadDepartmentMappings()
        {
            if (m_file.Exists (c_departmentMappingFile))
            {
                string mappingFile = m_file.ReadText (c_departmentMappingFile);

                try
                {
                    var fileMappings = JsonConvert.DeserializeObject<Dictionary<string,string>> (mappingFile);

                    m_departmentMappings = new Dictionary<string, string>(fileMappings,StringComparer.OrdinalIgnoreCase);   
                }
                catch
                {
                    Mvx.Error ("Unable to deserialize department map from {0}", c_departmentMappingFile);
                }
            }                
        }

        public async Task<StoreCombinedDeptSetup> GetCombinedDepartmentsForStore(string storeId)
        {
            var request = m_restClient.Get<GetCombinedDepartmentsResponse>(new GetCombinedDepartmentsRequest()
                {
                    StoreID = storeId,
                });

            var minDelay = Task.Delay (500);
            var result = await request;
            await minDelay;


            if (result.Code == 204) 
            {
                //No content - Store has no departments. (documentation states a HTTP response of 204 is returned)
                m_alertService.ShowOK(m_localiser.Get("combined_dept_error_header"), m_localiser.Get("combined_dept_fetch_error_nodepts"));
                return null;
            }

            CombinedDepartmentStoreData departments = null;
            // For some reason the call to get departments is always returning a 404 error, but the Content does contain the info we need.
            if (result.Code == 404)
            {
                try
                {
                    string jsonPayload = System.Text.Encoding.UTF8.GetString (result.Content, 0, result.Content.Length);
                    var departmentsResponse = JsonConvert.DeserializeObject<CombinedDepartmentsResponseData> (jsonPayload);
                    departments = departmentsResponse.StoreLabourDepartments;
                }
                catch
                {
                    //No content - Store has no departments. (documentation states a HTTP response of 404 is returned)
                    m_alertService.ShowOK (m_localiser.Get ("combined_dept_error_header"), m_localiser.Get ("combined_dept_fetch_error_nodepts"));
                    return null;
                }
            }

            if (result.Success && result.Object != null)
            {
                departments = result.Object.StoreLabourDepartments;
            }
                
            if (departments != null)
            {
                List<LabourDemandDepartment> uncombined = new List<LabourDemandDepartment> ();
                List<LabourDemandDepartment> combined = new List<LabourDemandDepartment> ();

                if (departments.UncombinedDepartments != null)
                {
                    foreach (var dept in departments.UncombinedDepartments)
                        uncombined.Add (new LabourDemandDepartment ()
                            {
                                ID = dept.LabourDepartmentID, 
                                Name = dept.LabourDepartmentDescription, 
                                IsLead = false, 
                                LeadID = string.Empty
                            });
                }

                if (departments.CombinedDepartments != null)
                {
                    foreach (var dept in departments.CombinedDepartments)
                        combined.Add (new LabourDemandDepartment ()
                            { 
                                ID = dept.LeadLabourDepartmentID, 
                                Name = dept.LabourDepartmentDescription,
                                IsLead = true, 
                                LeadID = string.Empty,
                                Children = dept.MemberDepartments.Select (x => new LabourDemandDepartment ()
                                    { 
                                        ID = x.LabourDepartmentID, 
                                        Name = x.LabourDepartmentDescription,
                                        IsLead = false, 
                                        LeadID = dept.LeadLabourDepartmentID 
                                    }).ToList (),
                            });
                }

                StoreCombinedDeptSetup data = new StoreCombinedDeptSetup ();
                data.Combined = combined;
                data.Uncombined = uncombined;
                return data;
            }
            else
            {
                #if DEBUG
                return STUB_Data ();
                #else
                m_alertService.ShowOK(m_localiser.Get("combined_dept_error_header"), string.Format(m_localiser.Get("combined_dept_fetch_error"), result.Code));
                
                //Create error for raygun (Only errors on RELEASE)
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add ("Content", (result.Content != null) ? Encoding.UTF8.GetString(result.Content, 0, result.Content.Length) : null);
                data.Add ("Object", result.Object);
                data.Add ("Message", result.Message);
                data.Add ("Code", result.Code);
                data.Add ("StoreId", storeId);
                m_raygunService.LogCustomError (new Exception ("GetCombinedDepartments"), data);

                return null;
                #endif
            }
        }

        public async Task<bool> SubmitChanges(string storeId, List<ChangeCommit> changes)
        {
            //The Tesco combine department service doesn't let us just commit all combined departments, we have to commit each "change" one at
            //a time. This also means the VM has built the changes in such a way that DELETES/UNCOMBINES will occur before the ADD/COMBINE to
            //ensure if the thing falls over half way through the resulting state isn't invalid.

            //So we have to go through each change one at a time and commit each and wait for the success/fail before continuing
            var minDelay = Task.Delay(1000);
            bool failed = false;
            string error = string.Empty;
            int code = 0;
            ChangeCommit failedCommit = null;

            foreach (var commit in changes.Where(x => x.Method == HttpVerb.Delete))
            {
                Task<Siesta.RestReply<string>> request = null;

                request = m_restClient.Delete<string> (new DeleteCombinedDepartmentRequest ()
                    {
                        StoreID = storeId,
                        DepartmentID = commit.Data.ID,
                    });
                var result = await request;

                //Check success (this is what the document says but appears wrong)
                //Delete - 200 = Success (documentation states a HTTP response of 200 is returned)
                //Post - 201 = Success (documentation states a HTTP response of 201 is returned)

                //                bool success = (commit.Method == HttpVerb.Delete && result.Code == 200) ||
                //                               (commit.Method == HttpVerb.Post && result.Code == 201);

                bool success = result.Code == 200 || result.Code == 201;

                if (!success)
                {
                    //Failed
                    failed = true;
                    failedCommit = commit;
                    error = result.Message;
                    code = result.Code;
                    break;
                }
            }

            foreach (var commit in changes.Where(x => x.Method == HttpVerb.Post))
            {
                Task<Siesta.RestReply<string>> request = null;   

                // Remove the lead department first.
                // Then post the lead department with all children, 
                // otherwise we get an error as we are trying to add departments that already exist in the lead department
                await m_restClient.Delete<string> (new DeleteCombinedDepartmentRequest ()
                    {
                        StoreID = storeId,
                        DepartmentID = commit.Data.ID,
                    });
                    
                request = m_restClient.Post<string> (new SetCombinedDepartmentRequest ()
                    {
                        StoreID = storeId,
                        DepartmentID = commit.Data.ID,
                        labourdepartmentID = commit.Data.Children.Select (x => x.ID).ToList (),
                    });
                var result = await request;

                //Check success (this is what the document says but appears wrong)
                //Delete - 200 = Success (documentation states a HTTP response of 200 is returned)
                //Post - 201 = Success (documentation states a HTTP response of 201 is returned)

//                bool success = (commit.Method == HttpVerb.Delete && result.Code == 200) ||
//                               (commit.Method == HttpVerb.Post && result.Code == 201);

                bool success = result.Code == 200 || result.Code == 201;

                if (!success)
                {
                    //Failed
                    failed = true;
                    failedCommit = commit;
                    error = result.Message;
                    code = result.Code;
                    break;
                }
            }

            await minDelay;

            if (!string.IsNullOrEmpty (error))
            {
                //Create error for raygun (Only errors on RELEASE)
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add ("Message", error);
                data.Add ("Code", code);
                data.Add ("StoreId", storeId);
                data.Add ("Changes", changes);
                data.Add ("FailedChange", failedCommit);
                m_raygunService.LogCustomError (new Exception ("SetCombinedDepartments"), data);

                if(failedCommit == changes[0])
                    m_alertService.ShowOK(m_localiser.Get("combined_dept_error_header"), string.Format(m_localiser.Get("combined_dept_submit_error_none"), code));
                else
                    m_alertService.ShowOK(m_localiser.Get("combined_dept_error_header"), string.Format(m_localiser.Get("combined_dept_submit_error_partial"), code));
            }

            return !failed;
        }

        #if DEBUG
        public StoreCombinedDeptSetup STUB_Data()
        {
            StoreCombinedDeptSetup data = new StoreCombinedDeptSetup ();

            foreach (var entry in m_departmentMappings)
                data.Uncombined.Add (new LabourDemandDepartment () { ID = entry.Key, Name = entry.Value });

            //Pick 3 to be leads
            for (int i = 0; i < 15; i++)
            {
                if (data.Uncombined.Count > 0)
                {
                    var combined = data.Uncombined[0];
                    data.Uncombined.Remove (combined);
                    if (i > 0 && i < 3)
                    {
                        //Add some combined depts
                        for (int j = 0; j < 3; j++)
                        {
                            if (data.Uncombined.Count > 0)
                            {
                                var child = data.Uncombined[0];
                                child.IsLead = false;
                                child.LeadID = combined.ID;
                                combined.Children.Add (child);
                                data.Uncombined.Remove (child);
                            }
                        }
                    }

                    combined.IsLead = true;
                    combined.LeadID = string.Empty;
                    data.Combined.Add (combined);
                }
            }

            return data;
        }
        #endif
    }
}
