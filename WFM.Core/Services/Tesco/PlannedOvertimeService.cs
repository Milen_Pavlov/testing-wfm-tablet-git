﻿using System;
using Consortium.Client.Core;
using WFM.Core.RestClients;
using System.Threading;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.OvertimePlanning;
using Siesta;
using System.Collections.Generic;

namespace WFM.Core
{
    [ConsortiumService]
    public class PlannedOvertimeService : ManagedRestService
    {
        private readonly ConsortiumRestClient m_client;
        private readonly SessionData m_sessionData;
        private readonly ScheduleService m_schedule;
        private readonly IStringService m_localiser;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public PlannedOvertimeService (ConsortiumRestClient client, 
            SessionData sessionData,
            ScheduleService schedule,
            IStringService localiser)
        {
            m_client = client;
            m_sessionData = sessionData;
            m_schedule = schedule;
            m_localiser = localiser;
        }

        public async Task<Result<IEnumerable<ReadPlannedOvertimeResponse.Department>>> ReadPlannedOvertime(DateTime forDate)
        {
            Result<IEnumerable<ReadPlannedOvertimeResponse.Department>> toReturn = new Result<IEnumerable<ReadPlannedOvertimeResponse.Department>>();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ReadPlannedOvertimeResponse>>(() =>
                        {
                            var request = new ReadPlannedOvertimeRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    DateString = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date.ToString(m_localiser.Get("yyyy-MM-dd"))
                                };

                            return m_client.Get<ReadPlannedOvertimeResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<ReadPlannedOvertimeResponse>> result = await PerformOperation<RestReply<ReadPlannedOvertimeResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                toReturn.Success = true;

                if (result.ResponseObject.Object != null && result.ResponseObject.Object.Departments != null && result.ResponseObject.Object.Departments.Length > 0)
                {
                    toReturn.Object = result.ResponseObject.Object.Departments;
                }
            }
            else
            {
                toReturn.Success = false;
            }

            return toReturn;
        }

        public async Task<Result> WritePlannedOvertime(DateTime forDate, List<DailyOvertimeModel> dataToSet)
        {
            Result toReturn = new Result();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            var departments = GenerateSaveData (dataToSet);

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<SetPlannedOvertimeResponse>>(() =>
                        {
                            var request = new SetPlannedOvertimeRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    DateString = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date.ToString(m_localiser.Get("yyyy-MM-dd")),
                                    Departments = departments
                                };

                            return m_client.Post<SetPlannedOvertimeResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<SetPlannedOvertimeResponse>> result = await PerformOperation<RestReply<SetPlannedOvertimeResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null && 
                result.ResponseObject.Success &&
                result.ResponseObject.Object != null &&
                result.ResponseObject.Object.Success)
            {
                toReturn.Success = true;
            }
            else
            {
                toReturn.Success = false;
                //TODO: parse error message
            }

            return toReturn;
        }

        private SetPlannedOvertimeRequest.Department[] GenerateSaveData(List<DailyOvertimeModel> dataToSet)
        {
            List<SetPlannedOvertimeRequest.Department> departments = new List<SetPlannedOvertimeRequest.Department>();

            foreach(var item in dataToSet)
            {
                if (item.DepartmentID < 0)
                    continue;
                
                SetPlannedOvertimeRequest.Department department = new SetPlannedOvertimeRequest.Department ()
                    {
                        DepartmentId = item.DepartmentID,
                        DepartmentName = item.DepartmentName,
                    };

                department.Hours = new Dictionary<DayOfWeek, float> ();
                foreach (var hour in item.Hours)
                {
                    department.Hours.Add (hour.Key, (float)hour.Value);
                }
                departments.Add(department);
            }

            return departments.ToArray ();
        }
    }
}

