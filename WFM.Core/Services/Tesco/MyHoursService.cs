﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using WFM.Core.RestClients;
using WFM.Core.MyHours;
using Cirrious.CrossCore;
using System.Linq;
using System.Threading;

namespace WFM.Core
{ 
    [ConsortiumService]
    public class MyHoursService
    {
        private readonly MyHoursRestClient m_restClient;
        private readonly SessionData m_sessionData;
        private readonly IFileSystem m_file;

        //HDT = Tech Support
        //CHE = Checkout Operators
        //HSP = PERSONAL SHOPPERS
        //HSD = CUSTOMER DELIVERY ASSISTANTS
        private readonly string[] PROTECTED_DEPARTMENT_CODES = new string[] { "HSP", "HSD", "CHE" };

        const string c_departmentMappingFile = "assets://Theme/DepartmentCodeMappings.json";

        private Dictionary<string,string> m_departmentMappings = new Dictionary<string,string>(StringComparer.OrdinalIgnoreCase);

        public MyHoursService (MyHoursRestClient restClient, SessionData sessionData, IFileSystem fileSystem)
        {
            m_file = fileSystem;
            m_restClient = restClient;
            m_sessionData = sessionData;

            LoadDepartmentMappings ();
        }

        private void LoadDepartmentMappings()
        {
            if (m_file.Exists (c_departmentMappingFile))
            {
                string mappingFile = m_file.ReadText (c_departmentMappingFile);

                try
                {
                    var fileMappings = JsonConvert.DeserializeObject<Dictionary<string,string>> (mappingFile);

                    m_departmentMappings = new Dictionary<string, string>(fileMappings,StringComparer.OrdinalIgnoreCase);   
                }
                catch
                {
                    Mvx.Error ("Unable to deserialize department map from {0}", c_departmentMappingFile);
                }
            }                
        }

        /// <summary>
        /// Returns the my hours data for a week in tesco week <param name="forWeekNumber"> in the tesco year <param name="forYear"/>
        /// Note that the year is not the year of the week, but rather the year that week 1 would have been in. e.g for week 52 in 2015
        /// the year will be 2014 as its week 52 of the tesco year starting in March 2014. Confused yet?
        /// </summary>
        /// <returns>The data.</returns>
        /// <param name="forYear">For tesco year.</param>
        /// <param name="forWeekNumber">For week number.</param>
        public async Task<Result<MyHours.WeekData>> LoadData(int forYear, int forWeekNumber, CancellationToken cancelToken)
        {
            string storeID = m_sessionData.Site.Name;

            Siesta.RestReply<GetDemandDetailsResponse> response = await m_restClient.Get <GetDemandDetailsResponse> (
                new GetDemandDetailsRequest () { StoreNumber = storeID, WeekNumber = string.Format ("{0}{1}", forYear, forWeekNumber) }, cancelToken);

            Result<MyHours.WeekData> result = new Result<WeekData> ();

            if (!response.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.REST;
                result.Code = response.Code;
                result.Message = response.Message;
                result.Object = new WeekData();
            }
            else if (response.Object == null)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.MY_HOURS;
                result.Code = -1;
                result.Message = "Empty Response";
            }
            else
            {
                if (response.Object.Count > 0)
                {
                    result.Object = BuildMyHoursModel (response.Object[0]);
                }

                result.Success = true;
            }

            //Debug
//            result.Object = GenerateTestData();
//            result.Success = true;

            return result;
        }

        public async Task<Result> SaveData(MyHours.WeekData data)
        {
            string storeID = m_sessionData.Org.Name;

            var myHoursData = AutoMapper.Mapper.Map<MyHours.WeekData,MyHoursData> (data);

            SaveDemandDetailsRequest request = new SaveDemandDetailsRequest ();
            request.Add (myHoursData);

            Siesta.RestReply<string> response = await m_restClient.Post<string> (request);

            Result result = new Result();

            if (!response.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.REST;
                result.Code = response.Code;
                result.Message = response.Message;
            }
            else 
            {
                result.Message = response.Object;
                result.Success = true;
            }

            return result;
        }

        public async Task<Result<MyHours.WeekData>> ResetData(int forYear, int forWeekNumber, CancellationToken token)
        {
            string storeID = m_sessionData.Org.Name;

            Siesta.RestReply<string> response = await m_restClient.Post <string> (
                new ResetDemandDetailsRequest () { StoreNumber = storeID, WeekNumber = string.Format ("{0}{1:00}", forYear, forWeekNumber) }, token);

            Result<MyHours.WeekData> result = new Result<WeekData> ();

            if (!response.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.REST;
                result.Code = response.Code;
                result.Message = response.Message;
            }
            else if (response.Object == null)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.MY_HOURS;
                result.Code = -1;
                result.Message = "Empty Response";
            }
            else if(!token.IsCancellationRequested)
            {
                return await LoadData (forYear, forWeekNumber, token);
            }

            return result;
        }

        WeekData BuildMyHoursModel (MyHoursData fromData)
        {
            WeekData model = AutoMapper.Mapper.Map<MyHoursData, MyHours.WeekData> (fromData);

            foreach (Department department in model.Departments)
            {
                if (string.IsNullOrEmpty (department.DepartmentName))
                {
                    //API may or may not send down the name, mapping file is a backup.
                    if (m_departmentMappings.ContainsKey (department.DepartmentCode))
                    {
                        department.DepartmentName = m_departmentMappings[department.DepartmentCode];
                    }
                    else
                    {
                        department.DepartmentName = department.DepartmentCode;
                    }
                }

                if (PROTECTED_DEPARTMENT_CODES.Contains (department.DepartmentCode, StringComparer.OrdinalIgnoreCase))
                {
                    department.IsProtected = true;
                    department.UserCanModify = false;
                }
                else
                {
                    department.IsProtected = false;
                    department.UserCanModify = true;
                }
            }

            return model;
        }

       private WeekData GenerateTestData()
        {
            var data = new WeekData()
                {
                    Week = 1,
                    AllowedHours = 100,
                    PlannedHours = 200,
                    Sales = 1000,
                    Store = 123,
                    TargetHours = 314
                };
            int date = 20160606;

            Random r = new Random();

            List<Department> departments = new List<Department>();

            for(int i = 0; i < 10; i ++)
            {
                int allowedHours = r.Next (1000);

                departments.Add(new Department()
                    {
                        DepartmentCode = i.ToString(),
                        DepartmentName = string.Format("Department {0}", i),
                        AllowedHours = allowedHours,
                        Sales = new decimal(r.Next(1000)),
                        TargetHours = new decimal(r.Next(1000)),
                        PlannedHours = new decimal(r.Next(1000)),

                        Days = new Day[7]
                            {
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date, DayOfWeek.Monday.ToString()),
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date + 1, DayOfWeek.Tuesday.ToString()),
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date + 2, DayOfWeek.Wednesday.ToString()),
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date + 3, DayOfWeek.Thursday.ToString()),
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date + 4, DayOfWeek.Friday.ToString()),
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date + 5, DayOfWeek.Saturday.ToString()),
                                new Day(1m / 7m, r.Next(1000), r.Next(1000), r.Next(1000), date + 6, DayOfWeek.Sunday.ToString()),
                            }
                    });
            }

            data.Departments = departments;

            return data;
        }
    }
}
