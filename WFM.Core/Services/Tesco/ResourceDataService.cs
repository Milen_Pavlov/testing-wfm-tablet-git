﻿using System;
using Consortium.Client.Core;
using WFM.Core.RestClients;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Breaks;
using System.Collections.Generic;
using System.Threading;
using Cirrious.MvvmCross.Plugins.Messenger;
using Siesta;
using WFM.Shared.DataTransferObjects.Resources;
using Newtonsoft.Json;
using WFM.Shared.DataTransferObjects.OvertimeAgreed;

namespace WFM.Core
{
    [ConsortiumService]
    public class ResourceDataService: ManagedRestService
    {
        private readonly ConsortiumRestClient m_client;
        private readonly SessionData m_sessionData;
      
        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public ResourceDataService(
            ConsortiumRestClient client, 
            SessionData sessionData,
            IAlertBox alert,
            IMobileDevice mobileDevice)
        {
            m_client = client;
            m_sessionData = sessionData;
        }

        public async Task<Result<ResourcePlanningData>> GetLockedData(DateTime forDate)
        {
            Result<ResourcePlanningData> toReturn = new Result<ResourcePlanningData>();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<GetResourceDataResponse>>(() =>
                        {
                            var request = new GetResourceDataRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    Date = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date
                                };
                            
                            return m_client.Get<GetResourceDataResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<GetResourceDataResponse>> result = await PerformOperation<RestReply<GetResourceDataResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                toReturn.Success = true;

                if (result.ResponseObject.Object != null && result.ResponseObject.Object.Resources != null)
                {
                    try
                    {
                        toReturn.Object = JsonConvert.DeserializeObject<ResourcePlanningData>(result.ResponseObject.Object.Resources.JsonData);
                    }
                    catch
                    {
                    }

                    if (toReturn.Object != null)
                    {
                        toReturn.Object.LockedDate = result.ResponseObject.Object.Resources.Date;
                    }
                }
            }
            else
            {
                toReturn.Success = false;
            }

            return toReturn;
        }

        public async Task<Result> UnsetLockedData(DateTime forDate)
        {
            Result toReturn = new Result();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<UnsetResourceDataResponse>>(() =>
                        {
                            var request = new UnsetResourceDataRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    Date = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date,
                                };

                            return m_client.Post<UnsetResourceDataResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<UnsetResourceDataResponse>> result = await PerformOperation<RestReply<UnsetResourceDataResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                if (result.ResponseObject.Object != null && result.ResponseObject.Object.Success)
                {
                    toReturn.Success = true;
                }
            }
            else
            {
                toReturn.Success = false;
                //TODO: parse error message
            }

            return toReturn;
        }


        public async Task<Result> SetLockedData(DateTime forDate, ResourcePlanningData dataToSet)
        {
            Result toReturn = new Result();

            string json = JsonConvert.SerializeObject(dataToSet);

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<RestReply<SetResourceDataResponse>>(() =>
                    {
                        var request = new SetResourceDataRequest()
                            {                                    
                                SiteId = m_sessionData.Site.SiteID,
                                Json = json,
                                Date = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date,
								Username = m_sessionData.SessionUsername
                            };

                        return m_client.Post<SetResourceDataResponse>(request,token).Result;
                    });
            };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<SetResourceDataResponse>> result = await PerformOperation<RestReply<SetResourceDataResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                if (result.ResponseObject.Object != null && result.ResponseObject.Object.Success)
                {
                    toReturn.Success = true;
                }
            }
            else
            {
                toReturn.Success = false;
                //TODO: parse error message
            }

            return toReturn;
        }


        public async Task<Result<double?>> ReadOvertimeAgreedValue(DateTime forDate)
        {
            Result<double?> toReturn = new Result<double?>();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ReadOvertimeAgreedResponse>>(() =>
                        {
                            var request = new ReadOvertimeAgreedRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    Week = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date
                                };

                            return m_client.Get<ReadOvertimeAgreedResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<ReadOvertimeAgreedResponse>> result = await PerformOperation<RestReply<ReadOvertimeAgreedResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                toReturn.Success = true;

                if (result.ResponseObject.Object != null && result.ResponseObject.Object.OvertimeAgreed != null && result.ResponseObject.Object.OvertimeAgreed.Count > 0)
                {
                    toReturn.Object = result.ResponseObject.Object.OvertimeAgreed[0].OvertimeAgreedAmount;
                }
            }
            else
            {
                toReturn.Success = false;
            }

            return toReturn;
        }



        public void Cancel()
        {
            m_tokenSource.Cancel();
        }
    }
}