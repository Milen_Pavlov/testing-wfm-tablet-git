﻿using System;
using WFM.Core.MyHours;

namespace WFM.Core
{
    public class MyHoursUndoStackOperation : UndoStackOperation
    {
        //Week state before change
        public WeekData Before { get; set; }

        //Whats changing
        public EditableCellType EditableType { get; set; }
        public DayOfWeek? Day { get; set; }
        public string DeptCode { get; set; }
        public decimal After { get; set; }
    }
}

