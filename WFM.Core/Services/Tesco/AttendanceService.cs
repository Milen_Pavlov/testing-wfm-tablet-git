﻿using System;
using Consortium.Client.Core;
using WFM.Core.RestClients;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Threading;
using WFM.Shared.DataTransferObjects.Attendance;
using System.Collections.Generic;
using Siesta;
using System.Linq;
using System.Threading.Tasks;

namespace WFM.Core
{
    [ConsortiumService]
    public class AttendanceService
    {
        private readonly SessionData m_data;
        private readonly ConsortiumRestClient m_client;
        private readonly IMvxMessenger m_messenger;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        protected MvxSubscriptionToken m_signOutToken;

        public AttendanceService(
            SessionData data, 
            ConsortiumRestClient client, 
            IMvxMessenger messenger)
        {
            m_data = data;
            m_client = client;
            m_messenger = messenger;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
        }

        private void OnSignOut(SignOutMessage message)
        {
            Cancel();
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }

        public async Task<Result<List<int>>> GetCurrentShiftsIn(List<int> currentShifts)
        {
            if (currentShifts == null || currentShifts.Count <= 0)
            {
                return new Result<List<int>> () { Success = true, Object = new List<int>() };
            }
            //Annoyingly, Siesta doesn't do this for lists itself.
            //Its a GET that takes any number of shift ids
            //So it has to be SID=X&SID=Y&SID=Z etc
            string sidStr = string.Empty;
            if (currentShifts.Count > 0)
                sidStr = currentShifts[0].ToString();
            if (currentShifts.Count > 1)
            {
                for(int i = 1; i<currentShifts.Count; i++)
                    sidStr += string.Format("&SID={0}", currentShifts[i]);
            }
            var request = new ReadAttendanceRequest ()
                {
                    SiteId = m_data.Site.SiteID,
                    SID = sidStr,
                };

            m_tokenSource = new CancellationTokenSource();
            RestReply<ReadAttendanceResponse> response = await m_client.Get <ReadAttendanceResponse> (request, m_tokenSource.Token);

            Result<List<int>> result = new Result<List<int>> ();
            if (!response.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.REST;
                result.Code = response.Code;
                result.Message = response.Message;
            }
            else if (response.Object == null)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.ATTENDANCE;
                result.Code = -1;
                result.Message = "Empty Response";
            }
            else
            {
                if(response.Object.ShiftsTaken != null)
                    result.Object = response.Object.ShiftsTaken.ToList();
                
                result.Success = true;
            }

            return result;
        }

        public async Task<Result> SetCurrentAttendance(List<int> shiftsIn, List<int> shiftsOut)
        {
            var request = new SetAttendanceRequest ()
            {
                SiteId = m_data.Site.SiteID,
                ShiftIdsSet = (shiftsIn != null) ? shiftsIn.ToArray () : new int[0],
                ShiftIdsUnSet = (shiftsOut != null) ? shiftsOut.ToArray () : new int[0],
            };

            m_tokenSource = new CancellationTokenSource ();
            RestReply<SetAttendanceResponse> response = await m_client.Post<SetAttendanceResponse> (request, m_tokenSource.Token);

            Result result = new Result ();

            if (!response.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.REST;
                result.Code = response.Code;
                result.Message = response.Message;
            }
            else if (response.Object == null)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.ATTENDANCE;
                result.Code = -1;
                result.Message = "Empty Response";
            }
            else if (!response.Object.Success)
            {
                result.Success = false;
                result.ErrorSource = ErrorSource.ATTENDANCE;
                result.Code = -1;
                result.Message = "Set Attendance Failed";
            }
            else
            {
                result.Success = true;
            }

            return result;
        }
    }
}

