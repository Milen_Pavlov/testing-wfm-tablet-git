﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;

namespace WFM.Core
{
    /// <summary>
    /// Represents a shift edit operation that can be reversed. Stores information needed for generating a reversal edit message.
    /// Used as a base class TODO Generic? Have generic "Before" and "After" parameters?
    /// </summary>
    public class UndoStackOperation
    {
    }

    [ConsortiumService]
    public class UndoService : IUndoService
    {
        private readonly Stack<UndoStackOperation> m_undoStack;

        public event EventHandler OnUndo;

        /// <summary>
        /// Is there any history in the undo stack for the undo service to roll back to?
        /// </summary>
        /// <value><c>true</c> if this instance can undo; otherwise, <c>false</c>.</value>
        public bool CanUndo
        {
            get
            {
                return m_undoStack.Count > 0;
            }
        }

        public UndoService ()
        {
            m_undoStack = new Stack<UndoStackOperation> ();
        }

        /// <summary>
        /// Adds the undo operation to the stack.
        /// </summary>
        /// <param name="change">Change.</param>
        public void Store(UndoStackOperation change)
        {
            m_undoStack.Push(change);
        }

        /// <summary>
        /// Wipes the history from the undo stack.
        /// </summary>
        public void ClearHistory()
        {
            m_undoStack.Clear();
        }

        /// <summary>
        /// Performs an undo operation, sending out the appropriate event
        /// </summary>
        public void Undo()
        {
            if (m_undoStack.Count > 0)
            {
                var head = m_undoStack.Pop ();
                if (OnUndo != null)
                    OnUndo (head, EventArgs.Empty);
            }
            else
                throw new InvalidOperationException("Nothing to undo");
        }

        public UndoStackOperation Peek()
        {
            return m_undoStack.Count > 0 ? m_undoStack.Peek () : null;
        }
    }
}

