﻿using System;
using Consortium.Client.Core;
using WFM.Core.RestClients;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using AutoMapper;
using System.Linq;
using System.Threading;
using Siesta;
using WFM.Shared.DataTransferObjects.Audits;

namespace WFM.Core
{
    [ConsortiumService]
    public class OvertimeService : ManagedRestService
    {
        private readonly ConsortiumRestClient m_client;
        private readonly SessionData m_sessionData;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();
   
        public OvertimeService(
            ConsortiumRestClient client, 
            SessionData sessionData)
        {
            m_client = client;
            m_sessionData = sessionData;
        }

        public async Task<IList<OvertimeRecord>> SafeReadDepartmentOvertime(DateTime forDate)
        {
            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<IList<OvertimeRecord>>(() =>
                        {
                            return ReadDepartmentOvertime(forDate, token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<IList<OvertimeRecord>>(timeout, taskGenerator, m_tokenSource.Token);

            return result.ResponseObject ?? new List<OvertimeRecord>();
        }

        public async Task<IList<OvertimeRecord>> ReadDepartmentOvertime(DateTime forDate, CancellationToken token)
        {
            ReadOvertimeRequest request = new ReadOvertimeRequest();
            request.ForDate = forDate;
            request.SiteId = m_sessionData.Site.SiteID;

            var response = await m_client.Post<ReadOvertimeResponse>(request, token);

            token.ThrowIfCancellationRequested();

            if (response.Success)
            {
                return response.Object.Departments ?? new List<OvertimeRecord>();
            }
            else
            {
                return new List<OvertimeRecord>();
            }
        }

        public async Task<bool> SetDepartmentsOvertime(DateTime startDate, List<OvertimeRecord> departments)
        {
            SetOvertimeRequest request = new SetOvertimeRequest();
            request.ForDate = DateTime.SpecifyKind(startDate.Date,DateTimeKind.Utc).Date;
            request.SiteId = m_sessionData.Site.SiteID;
            request.Overtime = departments;
            request.Username = m_sessionData.SessionUsername;

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<RestReply<SetOvertimeResponse>>(() =>
                {
                    return m_client.Post<SetOvertimeResponse>(request,token).Result;
                });
            };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<SetOvertimeResponse>>(timeout, taskGenerator, m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IList<AuditRecord>> GetAuditRecordsForDate(DateTime startDate)
        {
            GetAuditsRequest request = new GetAuditsRequest();
            request.Date = DateTime.SpecifyKind(startDate.Date, DateTimeKind.Utc).Date;
            request.Site = m_sessionData.Site.SiteID;

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<GetAuditsResponse>>(() =>
                        {
                            return m_client.Get<GetAuditsResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<GetAuditsResponse>>(timeout, taskGenerator, m_tokenSource.Token);

            if (result.ResponseObject != null && result.ResponseObject.Object != null)
            {
                if (result.ResponseObject.Object.Audits != null)
                {
                    return result.ResponseObject.Object.Audits;
                }
                else
                {
                    return new List<AuditRecord>();
                }
            }
            else
            {
                return null;
            }
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }
    }
}