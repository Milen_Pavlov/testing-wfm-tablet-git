﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using WFM.Shared.DataTransferObjects.Overtime;
using System.Threading.Tasks;
using AutoMapper;
using System.Linq;
using Consortium.Client.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleOvertimeService: IEditModeLink
    {
        private readonly OvertimeService m_overtimeService;
        private readonly IMvxMessenger m_messenger;
        protected MvxSubscriptionToken m_signOutToken;

        //Stateful members that control the data currently being shown to the user
        private DateTime m_forDate;
        private IList<OvertimeRecord> m_originalState;
        private IList<OvertimeRecord> m_state;
        private Dictionary<int, OvertimeRecord> m_overtimeHoursUpdated = new Dictionary<int, OvertimeRecord>();

        public ScheduleOvertimeService(
            OvertimeService overtimeService, 
            IMvxMessenger messenger)
        {
            m_overtimeService = overtimeService;
            m_messenger = messenger;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
        }

        public void Cancel()
        {
            m_overtimeService.Cancel();
        }

        /// <summary>
        /// Reads the list of department overtime from the overtime service.
        /// This wipes out any internal state so this should only be called after confirming with the suer
        /// </summary>
        /// <returns>The department overtime.</returns>
        /// <param name="forDate">For date.</param>
        public async Task<IList<OvertimeRecord>> ReadDepartmentOvertime(DateTime forDate, bool forceUpdate)
        {
            if (forceUpdate || m_state == null || m_forDate != forDate)
            {
                m_forDate = forDate;
                m_overtimeHoursUpdated.Clear();

                m_state = await m_overtimeService.SafeReadDepartmentOvertime(forDate);
                m_originalState = Mapper.Map<IList<OvertimeRecord>>(m_state);
            }

            return m_state;
        }

        /// <summary>
        /// Records a change against the overtime hours. Publishes an edit schedule message at the same time
        /// </summary>
        /// <param name="departmentId">Department identifier.</param>
        /// <param name="departmentName">Department name.</param>
        /// <param name="hours">Hours.</param>
        public void RecordChange(int departmentId, string departmentName, KeyValuePair<ResourcePlanningItem.OvertimeField,float> changedValue)
        {
            bool sendMessage = true;

            if (IsEdited)
                sendMessage = false;

            OvertimeRecord recordToChange;

            if (m_overtimeHoursUpdated.ContainsKey(departmentId))
            {
                recordToChange = m_overtimeHoursUpdated[departmentId];
            }
            else
            {
                recordToChange = new OvertimeRecord() { JDADepartmentId = departmentId, DepartmentName = departmentName };   

                m_overtimeHoursUpdated.Add(departmentId,recordToChange);
            }

            var existingState = m_state.FirstOrDefault(state => state.JDADepartmentId == departmentId);

            if (existingState != null)
            {
                recordToChange.AllocatedHours = existingState.AllocatedHours;
                recordToChange.ResourceAgreedNight = existingState.ResourceAgreedNight;
                recordToChange.ResourceRequestDay = existingState.ResourceRequestDay;
                recordToChange.ResourceRequestNight = existingState.ResourceRequestNight;
            }

            switch(changedValue.Key)
            {
                case ResourcePlanningItem.OvertimeField.OvertimeAgreedDay:
                    recordToChange.AllocatedHours = changedValue.Value;
                    break;
                case ResourcePlanningItem.OvertimeField.OvertimeAgreedNight:
                    recordToChange.ResourceAgreedNight = changedValue.Value;
                    break;
                case ResourcePlanningItem.OvertimeField.OvertimeRequestedDay:
                    recordToChange.ResourceRequestDay = changedValue.Value;
                    break;
                case ResourcePlanningItem.OvertimeField.OvertimeRequestedNight:
                    recordToChange.ResourceRequestNight = changedValue.Value;
                    break;
                default:
                    break;
            }

            m_overtimeHoursUpdated[departmentId] = recordToChange;
                    
            if(sendMessage)
                m_messenger.Publish(new OnEditModeMessage(this));
        }

        #region IEditModeLink implementation

        /// <summary>
        /// Commits current changes to the service
        /// </summary>
        public Task Save()
        {
            List<OvertimeRecord> changes = new List<OvertimeRecord>();

            foreach (KeyValuePair<int, OvertimeRecord> record in m_overtimeHoursUpdated)
            {
                changes.Add(record.Value);
            }

            m_overtimeHoursUpdated.Clear();

            return m_overtimeService.SetDepartmentsOvertime(m_forDate, changes);
        }

        /// <summary>
        /// Has this instance been edited since the last time it was read?
        /// </summary>
        /// <value><c>true</c> if this instance has been edited; otherwise, <c>false</c>.</value>
        public bool IsEdited
        {
            get
            {
                return m_overtimeHoursUpdated.Count > 0;
            }
        }

        public void Reset()
        {
            m_overtimeHoursUpdated.Clear();
            m_state = Mapper.Map<IList<OvertimeRecord>>(m_originalState);
        }

        #endregion

        public ResourcePlanningItem.OvertimeField GetDirtyFieldFlags(int jdaDepartmentId)
        {
            ResourcePlanningItem.OvertimeField dirtyFields = ResourcePlanningItem.OvertimeField.None;

            if (m_overtimeHoursUpdated.ContainsKey(jdaDepartmentId))
            {
                var updatedRecord = m_overtimeHoursUpdated[jdaDepartmentId];

                var existingState = m_originalState.FirstOrDefault(state => state.JDADepartmentId == jdaDepartmentId);

                if (existingState != null)
                {
                    if (updatedRecord.AllocatedHours != existingState.AllocatedHours)
                    {
                        dirtyFields |= ResourcePlanningItem.OvertimeField.OvertimeAgreedDay;
                    }

                    if (updatedRecord.ResourceAgreedNight != existingState.ResourceAgreedNight)
                    {
                        dirtyFields |= ResourcePlanningItem.OvertimeField.OvertimeAgreedNight;
                    }

                    if (updatedRecord.ResourceRequestDay != existingState.ResourceRequestDay)
                    {
                        dirtyFields |= ResourcePlanningItem.OvertimeField.OvertimeRequestedDay;
                    }

                    if (updatedRecord.ResourceRequestNight != existingState.ResourceRequestNight)
                    {
                        dirtyFields |= ResourcePlanningItem.OvertimeField.OvertimeRequestedNight;
                    }
                }
            }

            return dirtyFields;
        }

        private void OnSignOut(SignOutMessage message)
        {
            m_forDate = default(DateTime);
            m_state = null;
            m_overtimeHoursUpdated = new Dictionary<int, OvertimeRecord>();
        }
    }
}