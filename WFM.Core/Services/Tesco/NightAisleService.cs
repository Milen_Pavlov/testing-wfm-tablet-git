﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Consortium.Client.Core;
using Consortium.Client.Core.Models;
using WFM.Shared.DataTransferObjects.Aisles;
using WFM.Core;
using WFM.Shared.DataTransferObjects.Shared;
using WFM.Core.RestClients;
using System.Linq;
using System.Threading;

namespace WFM.Core.NightAisle
{
    [ConsortiumService]
    public class NightAisleService : INightAisleService
    {
        private ConsortiumRestClient m_restClient;
        private readonly IStringService m_localiser;

        public NightAisleService (ConsortiumRestClient consortiumRestClient, IStringService localiser)
        {
            m_restClient = consortiumRestClient;
            m_localiser = localiser;
        }

        public async Task<Result<IEnumerable<Aisle>>> GetAisleConfiguration(int forSiteID, CancellationToken token)
        {
            Siesta.RestReply<ReadAislesBySiteResponse> response = await m_restClient.Get<ReadAislesBySiteResponse>(new ReadAislesBySiteRequest() {SiteId = forSiteID}, token);

            if (response.Success)
            {
                if (response.Object != null)
                {
                    List<Aisle> aisles = new List<Aisle> ();
                    if(response.Object.Aisles != null)
                    {
                        foreach (ReadAislesBySiteResponse.Aisle responseAisle in response.Object.Aisles)
                        {
                            Aisle aisle = new Aisle ()
                                {
                                    ID = responseAisle.AisleId,
                                    Name = responseAisle.AisleName,
                                    Priority = responseAisle.Order,
                                };
                            aisles.Add (aisle);
                        }
                    }
                    return new Result<IEnumerable<Aisle>> () {
                        Success = true,
                        Object = aisles,
                    };
                }
            }
                
            return new Result<IEnumerable<Aisle>> () {
                Success = false,
                Object = new List<Aisle>(),
            };
        }

        public async Task<Response> SetAisleConfiguration(int forSiteID, IEnumerable<Aisle> configuration)
        {
            List<SaveAislesRequest.Aisle> aisles = new List<SaveAislesRequest.Aisle> ();
            foreach (Aisle aisle in configuration)
            {
                SaveAislesRequest.Aisle requestAise = new SaveAislesRequest.Aisle ()
                    {
                        AisleId = aisle.ID,
                        AisleName = aisle.Name,
                        Order = aisle.Priority?? -1,
                    };
                aisles.Add (requestAise);
            }
            
            Siesta.RestReply<SaveAislesResponse> response = await m_restClient.Post<SaveAislesResponse>(new SaveAislesRequest() {SiteId = forSiteID, Aisles = aisles.ToArray()});

            return response.Object;
        }

        public async Task<Result<IEnumerable<AisleMapping>>> GetMappingsForWeekStarting(int forSiteID, DateTime forWeekStarting, CancellationToken token)
        {         
            Siesta.RestReply<ReadAisleAllocationResponse> response = await m_restClient.Get<ReadAisleAllocationResponse> (new ReadAisleAllocationRequest () { SiteId = forSiteID, dateString = forWeekStarting.ToString(m_localiser.Get("yyyy-MM-dd")) }, token);

            if (response.Success)
            {
                if (response.Object != null)
                {
                    List<AisleMapping> aisleMappings = new List<AisleMapping> ();

                    if(response.Object.Aisles != null)
                    {
                        foreach (ReadAisleAllocationResponse.Aisle responseAisle in response.Object.Aisles)
                        {
                            AisleMapping aisleMapping = new AisleMapping();

                            aisleMapping.AisleID = responseAisle.AisleId;

                            Dictionary<DayOfWeek, int[]> employeeIdDayMappings = new Dictionary<DayOfWeek, int[]> ();
                            Dictionary<DayOfWeek, string[]> employeeDayMappings = new Dictionary<DayOfWeek, string[]> ();

                            foreach (DayOfWeek dayOfWeek in responseAisle.Allocations.Keys)
                            {
                                List<int> employeeIds = new List<int> ();
                                List<string> employeeNames = new List<string> ();

                                foreach (ReadAisleAllocationResponse.Allocation allocation in responseAisle.Allocations[dayOfWeek])
                                {
                                    employeeIds.Add(int.Parse(allocation.EmployeeId));
                                    employeeNames.Add (allocation.EmployeeName);
                                }
                                employeeDayMappings.Add (dayOfWeek, employeeNames.ToArray ());
                                employeeIdDayMappings.Add (dayOfWeek, employeeIds.ToArray ());

                                employeeIds.Clear ();
                                employeeNames.Clear ();
                            }

                            aisleMapping.EmployeeDayMappings = employeeDayMappings;
                            aisleMapping.EmployeeIdDayMappings = employeeIdDayMappings;

                            aisleMappings.Add (aisleMapping);
                        }
                    }

                    return new Result<IEnumerable<AisleMapping>> ()
                    {
                        Success = true,
                        Object = aisleMappings,
                    };

                }
            }

            return new Result<IEnumerable<AisleMapping>> ()
            {
                Success = false,
                Object = new List<AisleMapping> ()
            };
        }

        public async Task<Response> SetMappingsForWeekStarting(int forSiteID, DateTime forWeekStarting, IList<NightAisleModel> aisleMappings)
        {
            var requestAiles = new List<SetAisleAllocationRequest.Aisle> ();
            foreach (NightAisleModel aisleModel in aisleMappings)
            {
                var requestAile = new SetAisleAllocationRequest.Aisle ();

                requestAile.AisleId = aisleModel.AisleID;
                requestAile.Allocations = new Dictionary<DayOfWeek, SetAisleAllocationRequest.Allocation[]> ();


                foreach (var dayOfWeek in aisleModel.Days)
                {
                    string[] dayMappings = dayOfWeek.Employees;
                    int[] idMappings = dayOfWeek.EmployeeIds;

                    if (dayMappings.Length != idMappings.Length)
                        return new Response () { Success = false };

                    SetAisleAllocationRequest.Allocation[] allocations = new SetAisleAllocationRequest.Allocation[dayMappings.Length];

                    for(int i = 0; i < dayMappings.Length; i++)
                    {
                        allocations[i] = new SetAisleAllocationRequest.Allocation();
                        allocations[i].EmployeeName = dayMappings[i];
                        allocations[i].EmployeeId = idMappings[i];
                    }

                    requestAile.Allocations.Add (dayOfWeek.Day, allocations);
                }
                requestAiles.Add (requestAile);
            }

            Siesta.RestReply<SetAisleAllocationResponse> response = await m_restClient.Post<SetAisleAllocationResponse>(new SetAisleAllocationRequest() { SiteId = forSiteID, WeekString = forWeekStarting.ToString(m_localiser.Get("yyyy-MM-dd")), Aisles = requestAiles.ToArray()});

            return response.Object;
        }

        // TODO: Remove this 
        private double m_start = 20;
        private double m_duration = 9;

        public async Task<Result<Tuple<double, double>>> GetReplenishmentStartAndEndTime(int forSiteID, CancellationToken token)
        {
            //TODO: Replace with actual rest call once the server part is done
            await Task.Delay(100);
            return new Result<Tuple<double, double>>() { Object = new Tuple<double, double>(m_start, m_duration) };
        }

        public async Task<Response> SetReplenishmentStartAndEndTime(int forSiteID, double start, double duration)
        {
            m_start = start;
            m_duration = duration;
            await Task.Delay(100);
            //TODO: Replace with actual rest call once the server part is done
            return new SaveReplenishmentTimeResponse() { Success = true };

            //Siesta.RestReply<SaveReplenishmentTimeResponse> response = await m_restClient.Post<SaveReplenishmentTimeResponse>(new SaveReplenishmentTimeRequest() { SiteId = forSiteID, Start = start, Duration = duration } );
            //return response.Object;
        }
    }
}    