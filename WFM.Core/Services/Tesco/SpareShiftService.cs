﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Linq;
using Cirrious.CrossCore;
using WFM.Shared.Extensions;
using WFM.Core.RestClients;
using System.Threading;
using WFM.Shared.DataTransferObjects.SpareShifts;
using Siesta;
using System.Runtime.ExceptionServices;

namespace WFM.Core
{
    public class SpareShiftsEmployee
    {
        public DateTime WeekStart { get; set; }
        public List<Tuple<DateTime, DateTime>> ShiftTimes { get; set; } // Start time and end time for shift
    }

    [ConsortiumService]
    public class SpareShiftService : ManagedRestService, IEditModeLink
    {
        private readonly ScheduleService m_schedule;
        private readonly IESSConfigurationService m_settings;
        private readonly SessionData m_sessionData;
        private readonly ConsortiumRestClient m_client;
        private readonly IRaygunService m_raygunService;

        private static int s_uniqueId = -1;

        private int Site { get; set; }
        private int DepartmentID { get; set; }
        private string DepartmentName { get; set; }
        private DateTime Date { get; set; }
        private List<SpareShiftsEmployee> m_spareShifts = null;
        private List<EmployeeData> m_filteredSpareShifts = null;
        public bool IsEdited { get; set; }

        private bool m_canEdit = false;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public static int AssignUniqueID()
        {
            return --s_uniqueId;
        }

        public SpareShiftService (ScheduleService schedule, 
            IESSConfigurationService settings, 
            SessionData sessionData, 
            ConsortiumRestClient client, 
            IRaygunService raygunService)
        {
            m_schedule = schedule;
            m_settings = settings;
            m_sessionData = sessionData;
            m_client = client;
            m_raygunService = raygunService;
        }

        public List<EmployeeData> GetSpareShifts(int departmentId, string departmentName, DateTime day, int? dayIndex)
        {
            if (m_settings.ShowSpareShifts == false)
            {
                return new List<EmployeeData> ();
            }

            int forSiteID = m_sessionData.Site.SiteID;
            if (forSiteID != Site || departmentId != DepartmentID)
            {
                // requires refresh
                ClearShifts();
            }

            Site = forSiteID;
            DepartmentID = departmentId;
            DepartmentName = departmentName;

            if (m_filteredSpareShifts == null || m_filteredSpareShifts.Count <= 0)
            {
                m_filteredSpareShifts = new List<EmployeeData> (); 

                List<SpareShiftsEmployee> loadedEmployees = m_spareShifts;
                if (loadedEmployees != null)
                {
                    List<JDAJob> jobRoles = Mvx.Resolve<ScheduleService> ().GetJobRolesForDepartment (departmentId);
                    // Add any employees that have a shift for the current day.
                    foreach (var employee in loadedEmployees)
                    {
                        var newEmployee = CreateDummyEmployeeData (jobRoles, employee.WeekStart);
                        foreach (var shift in employee.ShiftTimes)
                        {
                            newEmployee.AddShift (shift.Item1, shift.Item2, jobRoles);
                        }
                    }
                }
            }

            EmployeeData emptyRow = null;
            bool getFullWeek = (dayIndex.HasValue == false);
            DateTime startDay = day.Date.AddDays (dayIndex.HasValue ? dayIndex.Value : 0);
            List<EmployeeData> employeeList = new List<EmployeeData> ();

            foreach(var employee in m_filteredSpareShifts)
            {
                bool addedEmployee = false;

                foreach (var testDay in employee.Days)
                {
                    if(emptyRow == null && getFullWeek == false && testDay.DayStart == startDay)
                    {
                        if (testDay.Shifts.Count == 0)
                        {
                            emptyRow = employee;
                        }
                    }
                    if (addedEmployee == true)
                        continue;

                    if (testDay.Shifts.Count > 0)
                    {
                        if(employee.WeekStart == day.Date)
                        {
                            if (getFullWeek == true || testDay.DayStart == startDay)
                            {
                                employeeList.Add (employee);
                                addedEmployee = true;
                            }
                        }
                    }
                }
            }

            if (emptyRow != null)
            {
                employeeList.Add (emptyRow);
            }
            else
            {
                employeeList.Add (CreateDummyEmployeeData (departmentId, m_schedule.StartDate));
            }

            return employeeList;
        }
            
        public EmployeeData CreateDummyEmployeeData(int departmentId, DateTime weekStart)
        {
            List<JDAJob> jobRoles = Mvx.Resolve<ScheduleService> ().GetJobRolesForDepartment (departmentId);
            return CreateDummyEmployeeData (jobRoles, weekStart);
        }

        public EmployeeData CreateDummyEmployeeData(List<JDAJob> jobRoles, DateTime weekStart)
        {
            JDAScheduleConstraints constraints = new JDAScheduleConstraints () {
                UnavailabilityRanges = new List<JDAInterval>(),
                FixedShifts = new List<JDAEmployeeAssignedJob>(),
                MinMaxHoursByDays = new List<JDAScheduleConstraints.DailyConstraint>(),
                CanWorkSplit = true,
            };

            JDAEmployeeAttribute attribute = new JDAEmployeeAttribute () {
                //Code = "",
            };

            List<JDAEmployeeAttribute> attributeList = new List<JDAEmployeeAttribute> ();
            attributeList.Add (attribute);

            JDAEmployee jdaEmployee = new JDAEmployee () {
                EmployeeID = AssignUniqueID(),
                FullName = Mvx.Resolve<IStringService>().Get ("spare_shifts_label"),
                IsMinor = false,
                CanWorkJobs = new List<JDAEmployeeAssignedJob> (),
                Shifts = new List<JDAShift>(),
                ScheduleConstraints = constraints,
                Attributes = attributeList,
            };
              
            foreach (var job in jobRoles)
            {
                JDAEmployeeAssignedJob jdaJob = new JDAEmployeeAssignedJob ();
                jdaJob.JobID = job.JobID;
                jdaJob.IsPrimary = true;
                jdaEmployee.CanWorkJobs.Add (jdaJob);
            }

            JDAEmployeeInfo employeeInfo = new JDAEmployeeInfo () {
                EmployeeID = jdaEmployee.EmployeeID,
                HomeSiteID = m_schedule.GetSiteId(),
            };

            EmployeeData employee = new EmployeeData (jdaEmployee, jobRoles, weekStart) { 
                CanEdit = m_canEdit,
                Info = employeeInfo,
                DayIndex = m_schedule.DayIndex,
                SpareShift = true,
            };

            employee.CalculateAllowableJobs (jobRoles);

            m_filteredSpareShifts.Add (employee);

            return employee;
        }

        public async Task Save()
        {
            if (m_filteredSpareShifts == null)
            {
                return;
            }

            List<SpareShiftsEmployee> employeeList = new List<SpareShiftsEmployee> ();
            if (m_filteredSpareShifts != null)
            {
                foreach (var employee in m_filteredSpareShifts)
                {
                    SpareShiftsEmployee newEmployee = new SpareShiftsEmployee ();
                    newEmployee.WeekStart = employee.WeekStart;
                    newEmployee.ShiftTimes = new List<Tuple<DateTime, DateTime>> ();
                    // Add the shifts
                    foreach (var day in employee.Days)
                    {
                        foreach (var shift in day.Shifts)
                        {
                            newEmployee.ShiftTimes.Add (new Tuple<DateTime, DateTime> (shift.Start, shift.End));
                        }
                    }
                    employeeList.Add (newEmployee);
                }
            }

            Dictionary<DayOfWeek, IEnumerable<SetSpareShiftsRequest.SpareShift>> saveData = ConvertToSaveData (employeeList);
            var writeResultTask = WriteSpareShifts (saveData);
            await writeResultTask;//TODO Error handling?

            return;
        }

        public void Revert()
        {
            ClearShifts ();
        }

        private void ClearShifts()
        {
            m_spareShifts = null;
            m_filteredSpareShifts = null;
            s_uniqueId = -1;
        }

        public EmployeeData GetEmployee(int employeeId)
        {
            if (m_settings.ShowSpareShifts == false)
                return null;

            if (m_filteredSpareShifts == null || m_filteredSpareShifts.Count == 0)
                return null;
            
            
            EmployeeData employee = m_filteredSpareShifts.FirstOrDefault (x => x.EmployeeID == employeeId);
            if (employee != null)
                return employee;

            return null;
        }

        public void SetEdit(bool canEdit)
        {
            m_canEdit = canEdit;
            if (m_filteredSpareShifts == null)
                return;
            
            foreach (var employee in m_filteredSpareShifts)
            {
                employee.CanEdit = canEdit;
            }
        }

        public void SetDay(int dayOfWeek)
        {
            if (m_filteredSpareShifts == null)
                return;
            
            foreach (var employee in m_filteredSpareShifts)
            {
                employee.DayIndex = dayOfWeek;
            } 
        }
            
        public async Task Refresh(bool shouldSave, FilterGroup filter)
        {
            //We've been getting crashes here but due to awaits we can't get the actual error on Raygun
            //so now we try catch round the whole thing and send the error to Raygun and display alert to user
            Exception exception = null;
            ExceptionDispatchInfo exceptionDispatchInfo = null;//If we need to rethrow we can do this.Throw();
            try
            {
                if (shouldSave)
                {
                    await Save ();
                }
                
                ClearShifts ();
                Date = m_schedule.StartDate;
                Site = m_sessionData.Site.SiteID;
                if (filter != null && filter.Items != null)
                {
                    List<FilterItem> filteredItems = filter.Items.Where (x => x != null && x.Selected == true).ToList ();
                    if (filteredItems.Count == 1)
                    {
                        DepartmentID = filteredItems[0].ID;
                        DepartmentName = filteredItems[0].Name;
                    }

                    else
                        return;
                }
                else
                    return;
                
                var spareShiftResponse = ReadSpareShifts (m_schedule.StartDate);
                var result = await spareShiftResponse;
                var employees = result.Object;
                List<SpareShiftsEmployee> loadedEmployees = ConvertToSpareShiftEmployees(employees);            
                m_spareShifts = loadedEmployees;
            }
            catch(Exception e)
            {
                exception = e;
                exceptionDispatchInfo = ExceptionDispatchInfo.Capture (e);
            }

            if (exception != null)
            {
                //Clean up
                m_spareShifts = new List<SpareShiftsEmployee>();

                //Raygun error
                //Create error for raygun
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add ("Message", exception.Message);
                data.Add ("StackTrace", exception.StackTrace);
                data.Add ("Source", exception.Source);
                data.Add ("Data", exception.Data);
                m_raygunService.LogCustomError(new Exception("SpareShiftServiceRefreshException"), data);
            }
        }

        public async Task<Result<Dictionary<DayOfWeek, IEnumerable<ReadSpareShiftsResponse.SpareShift>>>> ReadSpareShifts(DateTime forDate)
        {
            Result<Dictionary<DayOfWeek, IEnumerable<ReadSpareShiftsResponse.SpareShift>>> toReturn = new Result<Dictionary< DayOfWeek, IEnumerable<ReadSpareShiftsResponse.SpareShift>>>();

            //Perform Op
            int timeout = m_client.TimeoutMs;

            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<ReadSpareShiftsResponse>>(() =>
                        {
                            var request = new ReadSpareShiftsRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    JDADepartmentId = DepartmentID,
                                    DateString = DateTime.SpecifyKind(forDate,DateTimeKind.Utc).Date.ToString("yyyy-MM-dd"),
                                };

                            return m_client.Get<ReadSpareShiftsResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<ReadSpareShiftsResponse>> result = await PerformOperation<RestReply<ReadSpareShiftsResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null)
            {
                toReturn.Success = true;

                if (result.ResponseObject.Object != null && result.ResponseObject.Object.SpareShifts != null)
                {
                    toReturn.Object = result.ResponseObject.Object.SpareShifts;
                }
            }
            else
            {
                toReturn.Success = false;
            }

            return toReturn;
        }

        public async Task<Result> WriteSpareShifts(Dictionary<DayOfWeek, IEnumerable<SetSpareShiftsRequest.SpareShift>> dataToSet)
        {
            Result toReturn = new Result();

            //Perform Op
            int timeout = m_client.TimeoutMs;


            TaskGenerator taskGenerator = (token) => 
                {
                    return new Task<RestReply<SetSpareShiftsResponse>>(() =>
                        {
                            var request = new SetSpareShiftsRequest()
                                {                                    
                                    SiteId = m_sessionData.Site.SiteID,
                                    JDADepartmentId = DepartmentID,
                                    DepartmentName = DepartmentName,
                                    SpareShifts = dataToSet,
                                    DateString = DateTime.SpecifyKind(Date,DateTimeKind.Utc).Date.ToString("yyyy-MM-dd"),
                                };
                                    
                            return m_client.Post<SetSpareShiftsResponse>(request,token).Result;
                        });
                };

            m_tokenSource = new CancellationTokenSource();

            ManagedRestResponse<RestReply<SetSpareShiftsResponse>> result = await PerformOperation<RestReply<SetSpareShiftsResponse>>(timeout, taskGenerator,m_tokenSource.Token);

            if (result.ResponseObject != null && 
                result.ResponseObject.Success &&
                result.ResponseObject.Object != null &&
                result.ResponseObject.Object.Success)
            {
                toReturn.Success = true;
            }
            else
            {
                toReturn.Success = false;
                //TODO: parse error message
            }

            return toReturn;
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }
            
        private List<SpareShiftsEmployee> ConvertToSpareShiftEmployees(Dictionary<DayOfWeek, IEnumerable<ReadSpareShiftsResponse.SpareShift>> dayList)
        {
            if (dayList == null)
                return new List<SpareShiftsEmployee>();
            
            Dictionary<int, SpareShiftsEmployee> employees = new Dictionary<int, SpareShiftsEmployee> ();

            foreach (var day in dayList)
            {
                int dayOfWeek = (int)day.Key - (m_schedule.StartDay.GetValueOrDefault(0) - 1);
                if (dayOfWeek < 0)
                    dayOfWeek += 7;
                DateTime dayStart = Date.AddDays (dayOfWeek);
                foreach(var shift in day.Value)
                {
                    SpareShiftsEmployee employee = employees.ContainsKey(shift.JobId) ? employees[shift.JobId] : new SpareShiftsEmployee();
                    if (employee.ShiftTimes == null)
                    {
                        employee.ShiftTimes = new List<Tuple<DateTime, DateTime>> ();
                        employee.WeekStart = Date;
                    }
                    
                    employee.ShiftTimes.Add(new Tuple<DateTime, DateTime>(dayStart.Add(shift.ShiftStart), dayStart.Add(shift.ShiftStart + shift.ShiftDuration)));

                    employees[shift.JobId] = employee;
                }
            }

            List<SpareShiftsEmployee> toReturn = new List<SpareShiftsEmployee> ();
            foreach (var employee in employees)
            {
                toReturn.Add (employee.Value);
            }
            return toReturn;
        }

        private Dictionary<DayOfWeek, IEnumerable<SetSpareShiftsRequest.SpareShift>> ConvertToSaveData(List<SpareShiftsEmployee> employeeList)
        {
            Dictionary<DayOfWeek, IEnumerable<SetSpareShiftsRequest.SpareShift>> saveData = new Dictionary<DayOfWeek, IEnumerable<SetSpareShiftsRequest.SpareShift>> ();
            saveData.Add (DayOfWeek.Monday, new List<SetSpareShiftsRequest.SpareShift> ());
            saveData.Add (DayOfWeek.Tuesday, new List<SetSpareShiftsRequest.SpareShift> ());
            saveData.Add (DayOfWeek.Wednesday, new List<SetSpareShiftsRequest.SpareShift> ());
            saveData.Add (DayOfWeek.Thursday, new List<SetSpareShiftsRequest.SpareShift> ());
            saveData.Add (DayOfWeek.Friday, new List<SetSpareShiftsRequest.SpareShift> ());
            saveData.Add (DayOfWeek.Saturday, new List<SetSpareShiftsRequest.SpareShift> ());
            saveData.Add (DayOfWeek.Sunday, new List<SetSpareShiftsRequest.SpareShift> ());

            for (int i = 0; i < employeeList.Count; ++i)
            {
                SpareShiftsEmployee employee = employeeList[i];
                foreach (var shift in employee.ShiftTimes)
                {
                    SetSpareShiftsRequest.SpareShift newShift = new SetSpareShiftsRequest.SpareShift ()
                    {
                            JobId = i,
                            JobName = String.Empty,
                            ShiftStart = shift.Item1 - shift.Item1.Date,
                            ShiftDuration = shift.Item2 - shift.Item1,
                    };

                    (saveData[shift.Item1.Date.DayOfWeek] as List<SetSpareShiftsRequest.SpareShift>).Add (newShift);
                }
            }

            return saveData;
        }

        public void Reset ()
        {
            Refresh(false, m_schedule.CurrentFilter);
        }
    }
}

