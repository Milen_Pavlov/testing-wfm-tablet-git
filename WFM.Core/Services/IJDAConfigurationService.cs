﻿using System.Threading.Tasks;
using Siesta;

namespace WFM.Core
{
    public interface IJDAConfigurationService
    {
        IConfiguration JDAConfiguration { get; set; }
        Task LoadJDAConfig(SiestaClient client, int siteID);
    }
}

