﻿using System;

namespace WFM.Core
{
    public class AccountingHelper : IAccountingHelper
    {
        private IAccountingConfiguration m_accountingConfig;

        public AccountingHelper (IAccountingConfiguration accountingConfig)
        {
            m_accountingConfig = accountingConfig;
        }

        public int GetWeekNumberForDate (DateTime date)
        {
            var accountingYearStart = GetStartOfAccountingYear (date);

            var weekNumber = ((date - accountingYearStart).Days / 7) + 1; //Starting from 1

            return weekNumber;
        }

        public DateTime GetStartOfWeekForDate (DateTime date)
        {
            var dayOffset = (date.DayOfWeek - m_accountingConfig.StartDay);

            if (dayOffset < 0)
                dayOffset += 7;

            return date.AddDays (-dayOffset).Date;
        }

        public int GetAccountingPeriodWeeks (DateTime date)
        {

            return GetWeekNumberForDate (GetEndOfAccountingYear (date));
        }

        public DateTime GetStartOfAccountingYear (DateTime date)
        {
            var dayInFirstWeek = new DateTime (date.Year, m_accountingConfig.PeriodFirstMonth, m_accountingConfig.PeriodFirstDay);
            var firstday = GetStartOfWeekForDate (dayInFirstWeek);
            //And then we look at the week starting at the configured day of week that contains 1st march
            int dayOffset = (firstday.DayOfWeek - m_accountingConfig.StartDay);

            if (dayOffset < 0)
                dayOffset += 7;

            var firstDate = firstday.AddDays (-dayOffset);

            //If our input is before the first date for this year, then we calculate the year start for 1st march for the year before as we're somewhere between 
            if (date < firstDate && date != DateTime.MinValue)                 
            {
                var previousDay = new DateTime (date.Year - 1, m_accountingConfig.PeriodFirstMonth, m_accountingConfig.PeriodFirstDay);
                var previousFirstDay = GetStartOfWeekForDate (previousDay);
                dayOffset = (previousFirstDay.DayOfWeek - m_accountingConfig.StartDay);

                if (dayOffset < 0)
                    dayOffset += 7;

                return previousFirstDay.AddDays (-dayOffset);
            } 
            else
                return firstDate;
        }

        public DateTime GetEndOfAccountingYear (DateTime date)
        {
            DateTime currentYearStartDate = GetStartOfAccountingYear (date);

            DateTime nextPeriodTargetStartDay = GetStartOfAccountingYear (new DateTime (currentYearStartDate.Year + 1, m_accountingConfig.PeriodFirstMonth, m_accountingConfig.PeriodFirstDay));

            int dayOffset = (nextPeriodTargetStartDay.DayOfWeek - m_accountingConfig.StartDay);

            if (dayOffset < 0)
                dayOffset += 7;

            //Return the day before the start of the next period
            DateTime endDate = nextPeriodTargetStartDay.Subtract (TimeSpan.FromDays (dayOffset + 1));

            return endDate;
        }
    }
}

