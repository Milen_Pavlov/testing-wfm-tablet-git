﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public static class ScheduleExtensions
    {
        /// <summary>
        /// Gets the type of the hours for job.
        /// </summary>
        /// <returns>The hours for job type.</returns>
        /// <param name="day">Day. If null,gets hours for the week</param>
        /// <param name="jobId">Job identifier.</param>
        public static double GetHoursForJobType(this ScheduleData data, int day, int jobId)
        {
            double hours = 0;

            foreach (var employee in data.Employees)
            {
                if (employee.Days[day].JobTypeHours.ContainsKey(jobId))
                    hours += employee.Days[day].JobTypeHours[jobId];
            }

            return hours;
        }

        /// <summary>
        /// Gets the type of the hours for job.
        /// </summary>
        /// <returns>The hours for job type.</returns>
        /// <param name="day">Day. If null,gets hours for the week</param>
        /// <param name="jobId">Job identifier.</param>
        public static double GetContractedHoursForJobType(this ScheduleData data, int day, int jobId)
        {
            double hours = 0;

            foreach (var employee in data.Employees.Where(employee => employee.Info.PrimaryJobID == jobId))
            {
                double contractedHours = 0;
                if (double.TryParse (employee.Info.CellPhone, out contractedHours))
                {
                    double hoursForDay = employee.Days[day].FixedShifts
                        .Where (shift => shift.End != null)
                        .Sum (shift => shift.End.Value.Subtract (shift.Start).TotalHours);
                    double hoursForWeek = 0.0;
                    foreach(var shiftDay in employee.Days)
                    {
                        hoursForWeek += shiftDay.FixedShifts
                            .Where (shift => shift.End != null)
                            .Sum (shift => shift.End.Value.Subtract (shift.Start).TotalHours);
                    }

                    if (hoursForWeek > 0)
                    {
                        if (hoursForDay > 0 && contractedHours > 0)
                            contractedHours = contractedHours * (hoursForDay / hoursForWeek);
                        else
                            contractedHours = 0;
                    }
                    else
                    {
                        // No Fixed shifts so just divide into days
                        contractedHours = contractedHours / 7;
                    }
                }
                    
                hours += contractedHours;
            }

            return hours;
        }
        
        /// <summary>
        /// Gets the type of the contracted time off hours for job.
        /// </summary>
        /// <returns>The contracted time off hours for job type.</returns>
        /// <param name="data">Data.</param>
        /// <param name="day">Day.</param>
        /// <param name="jobId">Job identifier.</param>
        public static double GetContractedTimeOffHoursForJobType(this ScheduleData data, int day, int jobId)
        {
            double hours = 0;

            foreach (EmployeeData employee in data.Employees)
            {
                if (employee.Info.PrimaryJobID == jobId)
                {
                    var fixedShifts = employee.Days[day].FixedShifts
                        .Where(shift => shift.End != null);

                    if (fixedShifts.Count() > 0)
                    {
                        var unavailabilityRanges = employee.Days[day].UnavailabilityRanges.Where(range => range.Type == JDAInterval.TimeOffUnavailabilityType);

                        if (unavailabilityRanges.Count() > 0)
                        {
                            foreach (var range in unavailabilityRanges)
                            {
                                foreach (var shift in fixedShifts)
                                {
                                    double totalHours = TimePeriodHelper.GetOverlap(range.Start, range.End, shift.Start, shift.End.Value).TotalHours;
                                    hours += totalHours;
                                    hours -= employee.GetBreaksDurationForShift (data.ScheduleRules, shift, totalHours);// Remove breaks within this shift.
                                }
                            }
                        }
                    }
                }
            }

            return hours;
        }
        
        /// <summary>
        /// Gets the type of the contracted time off hours for job.
        /// </summary>
        /// <returns>The contracted time off hours for job type.</returns>
        /// <param name="data">Schedule Data.</param>
        /// <param name="day">Day to lookup.</param>
        /// <param name="jobId">Job identifier.</param>
        public static double GetContractedTimeOffHoursForJobType(this ScheduleData data, int day, int jobId, IEnumerable<JDATimeOffRequest> matchingTimeOffRequests)
        {
            double hours = 0;

            foreach (EmployeeData employee in data.Employees)
            {
                if (employee.Info.PrimaryJobID == jobId)
                {
                    var fixedShifts = employee.Days[day].FixedShifts
                        .Where(shift => shift.End != null);

                    if (fixedShifts.Count() > 0)
                    {
                        var employeeTimeOffRecords = matchingTimeOffRequests.Where(request => request.EmployeeID.GetValueOrDefault(-1).Equals(employee.EmployeeID));
                        
                        foreach (var request in employeeTimeOffRecords)
                        {
                            foreach (var shift in fixedShifts)
                            {
                                //Ugh, JDA's time off requests come down as ending 1 minute before a sane person would think they end.
                                DateTime normalisedEnding = request.End;
                                
                                if(request.End.Minute == 59)
                                    normalisedEnding = request.End.AddMinutes(1);
                            
                                double totalHours = TimePeriodHelper.GetOverlap(request.Start, normalisedEnding, shift.Start, shift.End.Value).TotalHours;
                                if(totalHours > 0)
                                {
                                    hours += totalHours;
                                    hours -= employee.GetBreaksDurationForShift (data.ScheduleRules, shift, totalHours);// Remove breaks within this shift.
                                }
                            }
                        }
                    }
                }
            }

            return hours;
        }
        
        
    }
}