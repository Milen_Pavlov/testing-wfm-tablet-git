﻿namespace WFM.Core
{    
    public static class ErrorSource
    {
        public const string JDA = "JDA";
        public const string REST = "REST";
        public const string MY_HOURS = "MY HOURS";
        public const string ATTENDANCE = "ATTENDANCE";
        public const string UNKNOWN = "UNKNOWN";

        public const string LABOUR_DEMAND = "LABOUR DEMAND";
    }

    /// <summary>
    /// Base result class for wrapping operations performed on a webservice. 
    /// </summary>
    public class Result
    {
        public string ErrorSource { get; set; }
        public int Code { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    /// <summary>
    /// Base result class for wrapping operations performed on a webservice. Generic parameter allows return of data either processed by service or direct from rest client.
    /// </summary>
    public class Result<T> : Result
    {
        public T Object { get; set; }
    }
}