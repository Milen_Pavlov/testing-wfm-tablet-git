﻿using System;

namespace WFM.Core
{
    public class UserDetailsUtility
    {
        public const string UsernameKey = "wfm-username";
        public const string PasswordKey = "wfm-password";
        public const string StoreKey = "wfm-store";
    }
}

