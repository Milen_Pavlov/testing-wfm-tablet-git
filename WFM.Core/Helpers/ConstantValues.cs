﻿using System;

namespace WFM.Core
{
    public static class ConstantValues
    {
        public const int c_comfortBreakMillis = 1000;
		public static readonly string[] ItemBackgrounds = new string[] { "#B22222", "#7498E4", "#008000", "#FFA500", "#9370DB", "#9ACD32", "#21b2b2", "#2122b2", "#6a21b2", "#e374cf", "#74e388", "#804000", "#0059ff", "#da926f" };
		public static readonly string[] ItemBorders = new string[] { "#D89090", "#B9CBF1", "#7FBF7F", "#FFD27F", "#C9B7ED", "#CCE698", "#90d7d7", "#9090d7", "#b490d7", "#f1b9e6", "#b9f1c3", "#bf9f7f", "#7fabff", "#edc8b7" };
    }
}

