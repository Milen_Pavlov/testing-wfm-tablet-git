﻿using System;

namespace WFM.Core
{
    public static class WeekHelper
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static int GetJDADayIndexForDateTime(this DateTime dateTime, int scheduleStartDay)
        {
            return ((int)dateTime.DayOfWeek - (scheduleStartDay - 1) + 7) % 7;
        }
    }
}

