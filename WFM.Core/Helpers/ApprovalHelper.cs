﻿using System;

namespace WFM.Core
{
    public enum ApprovalStatus
    {
        None = 0,
        Pending,
        AwaitingManager,
        ManagerApproved,
        Denied,
        SenderCancelled,
        Cancelled,
    }

    public enum ApprovalShiftType
    {
        Normal = 0,
        Losing,
        Gaining,
    }

    public static class ApprovalHelper
    {

        public static ApprovalShiftType GetShiftType(this JDAApprovalShift shift)
        {
            switch (shift.TypeCodeString)
            {
                case "Normal":
                    return ApprovalShiftType.Normal;
                case "Gaining":
                    return ApprovalShiftType.Gaining;
                case "Losing":
                    return ApprovalShiftType.Losing;
                default:
                    return ApprovalShiftType.Normal;
            }
        }

        public static ApprovalStatus GetStatus(this JDASwapShiftApproval status)
        {
            switch (status.StatusCode)
            {
                case "r":
                    return ApprovalStatus.Pending;
                case "w":
                    return ApprovalStatus.AwaitingManager;
                case "a":
                    return ApprovalStatus.ManagerApproved;
                case "x":
                    return ApprovalStatus.Denied;
                case "c":
                    return ApprovalStatus.SenderCancelled;
                case "i":
                    return ApprovalStatus.Cancelled;
                default:
                    return ApprovalStatus.None;
            }
        }

        public static ApprovalStatus GetStatus(this JDAAvailabilityApproval status)
        {
            switch (status.StatusCode)
            {
                case "r":
                    return ApprovalStatus.Pending;
                case "w":
                    return ApprovalStatus.AwaitingManager;
                case "a":
                    return ApprovalStatus.ManagerApproved;
                case "x":
                    return ApprovalStatus.Denied;
                case "c":
                    return ApprovalStatus.SenderCancelled;
                case "i":
                    return ApprovalStatus.Cancelled;
                default:
                    return ApprovalStatus.None;
            }
        }
    }
}

