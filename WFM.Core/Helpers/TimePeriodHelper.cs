﻿using System;

namespace WFM.Core
{
    public static class TimePeriodHelper
    {
        /// <summary>
        /// Check whether a give set of two start and end times overlap
        /// </summary>
        /// <returns>Whether an overlap occurs as a bool.</returns>
        /// <param name="startShift">Shift start time</param>
        /// <param name="endShift">Shift end time</param>
        /// <param name="start">Comparison time period start time</param>
        /// <param name="end">Comparison time period end time</param>
        public static bool DoesOverlap(DateTime startShift, DateTime endShift, DateTime start, DateTime end)
        {
            if (startShift < end && start < endShift)
            {
                return true;
            }
            return false;

            //return GetOverlap(startShift, endShift, start, end) != TimeSpan.Zero;
        }

        public static TimeSpan GetOverlap(DateTime periodAStart, DateTime periodAEnd, DateTime periodBStart, DateTime periodBEnd)
        {
            if (periodBStart <= periodAStart && periodBEnd >= periodAEnd)
            {
                return periodAEnd - periodAStart;
            }
            else if (periodAStart <= periodBStart && periodAEnd >= periodBEnd)
            {
                return periodBEnd - periodBStart;
            }
            else 
            if (periodBStart < periodAStart)
            {
                if (periodBEnd <= periodAStart)
                {
                    return TimeSpan.Zero;
                }
                else
                {
                    return periodBEnd - periodAStart;
                }
            }
            else if (periodBEnd > periodAEnd)
            {
                if (periodBStart >= periodBEnd)
                {
                    return TimeSpan.Zero;
                }
                else
                {
                    return periodAEnd - periodBStart;
                }
            }
            else
            {
                return TimeSpan.Zero;
            }
        }

    }
}