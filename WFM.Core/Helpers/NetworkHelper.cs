﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    public static class NetworkHelper
    {
        public static void CheckNetworkConnection(IMobileDevice mobileDevice, IAlertBox alert)
        {
            if (mobileDevice.DetailedNetworkStatus != DetailedNetworkStatus.Connected)
            {
                alert.ShowOK("Network Warning", "No active network connection, data transfer may fail.");
            }
        }
    }
}