﻿using System;
using Cirrious.CrossCore;
using System.Collections.Generic;

namespace WFM.Core
{
    [Flags]
    public enum TimeOffStatus
    {
        None = 0,
        Pending = 1,
        Approved = 2,
        Denied = 4,
        Deleted = 8,
    }

    public class JDARequestTimeOffComparer : EqualityComparer<JDATimeOffRequest>
    {
        public override bool Equals (JDATimeOffRequest x, JDATimeOffRequest y)
        {
            return x.Status == y.Status;
        }

        public override int GetHashCode (JDATimeOffRequest x)
        {
            return x.Status.GetHashCode ();
        }
    }
        
    public static class TimeOffHelper
    {
        private const string c_approvedStatusCode = "a";
        private const string c_pendingStatusCode = "r";
        private const string c_deniedStatusCode = "d";
        private const string c_deletedStatusCode = "c";

        public static TimeOffStatus GetStatus(string statusCode, TimeOffStatus defaultStatus = TimeOffStatus.Pending)
        {
            if (string.Equals(statusCode, c_approvedStatusCode, StringComparison.CurrentCultureIgnoreCase))
            {
                return TimeOffStatus.Approved;
            }
            else if (string.Equals(statusCode, c_deniedStatusCode, StringComparison.CurrentCultureIgnoreCase))
            {
                return TimeOffStatus.Denied;
            }
            else if (string.Equals(statusCode, c_pendingStatusCode, StringComparison.CurrentCultureIgnoreCase))
            {
                return TimeOffStatus.Pending;
            }
            else if (string.Equals(statusCode, c_deletedStatusCode, StringComparison.CurrentCultureIgnoreCase))
            {
                return TimeOffStatus.Deleted;
            }
            else
            {
                Mvx.Warning("Status code {0} not handled, assuming {1}",statusCode, defaultStatus.ToString());
                return defaultStatus;
            }
        }

        public static string GetStatusCode(TimeOffStatus status)
        {
            switch (status)
            {
                case TimeOffStatus.Approved:
                    return c_approvedStatusCode;
                case TimeOffStatus.Denied:
                    return c_deniedStatusCode;
                case TimeOffStatus.Pending:
                    return c_pendingStatusCode;
                case TimeOffStatus.Deleted:
                    return c_deletedStatusCode;
                default:
                    return c_pendingStatusCode;
            }
        }
    }
}