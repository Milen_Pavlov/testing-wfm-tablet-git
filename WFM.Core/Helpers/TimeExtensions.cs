﻿using System;

namespace WFM.Core
{
    public static class TimeExtensions
    {
        public static TimeSpan SnapTo15Minutes(this TimeSpan span)
        {
            return TimeSpan.FromMinutes(((int)(span.TotalMinutes/15))*15);
        }
        
        public static TimeSpan GetDuration(this Tuple<DateTime, DateTime> period)
        {
            return period.Item2.Subtract(period.Item1);
        }
        
        public static DateTime GetStart(this Tuple<DateTime, DateTime> period)
        {
            return period.Item1;
        }
        
        public static DateTime GetEnd(this Tuple<DateTime, DateTime> period)
        {
            return period.Item2;
        }
    }
}