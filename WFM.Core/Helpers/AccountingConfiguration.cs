﻿using System;

namespace WFM.Core
{
    public class AccountingConfiguration : IAccountingConfiguration
    {
        public DayOfWeek StartDay { get; set; }
        public int PeriodFirstDay { get; set; }
        public int PeriodFirstMonth { get; set;}
    } 
}

