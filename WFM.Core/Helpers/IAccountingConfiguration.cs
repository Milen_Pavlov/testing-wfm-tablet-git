﻿using System;

namespace WFM.Core
{
    public interface IAccountingConfiguration
    {
        DayOfWeek StartDay { get; set; }
        int PeriodFirstDay { get; set; }
        int PeriodFirstMonth { get; set; }
    }
}

