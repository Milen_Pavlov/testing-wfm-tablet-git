﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public static class IEnumerableExtensions
    {
       
        public static bool IsNullOrEmpty<T> (this IEnumerable<T> sourceCollection)
        {
            return sourceCollection == null || !sourceCollection.Any ();
        }
    }
}

