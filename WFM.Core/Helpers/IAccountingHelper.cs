﻿using System;

namespace WFM.Core
{
    public interface IAccountingHelper
    {
        int GetWeekNumberForDate(DateTime forDate);
        int GetAccountingPeriodWeeks(DateTime forDate);
        DateTime GetStartOfWeekForDate(DateTime forDate);
        DateTime GetStartOfAccountingYear(DateTime forDate);
        DateTime GetEndOfAccountingYear(DateTime forDate);
    }
}

