﻿using System;
using Consortium.Client.Core;
using Cirrious.CrossCore.IoC;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class WFMViewModel : ConsortiumViewModel
    {
        private readonly IMvxMessenger m_messenger;

        public virtual string AnalyticsScreenName 
        {
            get
            {
                return null;
            }
        }

        public IAnalyticsService Analytics { get; private set; }
        public IStringService Localiser { get; set; }
        public ITimeFormatService TimeFormatter { get; set; }
        public SettingsService Settings { get; private set; }
        public IESSConfigurationService ConfigurationService { get; private set; }
        public ApplicationTheme Theme { get; private set; }
        public bool RefreshTitle 
        { 
            get
            { 
                return true; 
            } 
            set 
            { 
                RaisePropertyChanged (() => RefreshTitle); 
            } 
        }

        protected MvxSubscriptionToken m_orgBeginToken;
        protected MvxSubscriptionToken m_orgEndToken;

        public string Watermark
        {
            get 
            { 
                return "assets://Theme/Watermark.png"; 
            }

            set 
            {
                RaisePropertyChanged(() => Watermark);
            }
        }

        public WFMViewModel()
        {
            Localiser = Mvx.Resolve<IStringService> ();
            Settings = Mvx.Resolve<SettingsService> ();
            Analytics = Mvx.Resolve<IAnalyticsService> ();
            TimeFormatter = Mvx.Resolve<ITimeFormatService>();
            m_messenger = Mvx.Resolve<IMvxMessenger> ();
            Theme = Settings.AppTheme;
            ConfigurationService = Mvx.Resolve<IESSConfigurationService>();
            m_orgBeginToken = m_messenger.SubscribeOnMainThread<SetOrgBeginMessage> (OnOrgBegin);
            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage> (OnOrgEnd);
        }

        public override void OnViewOpened ()
        {
            if(!String.IsNullOrEmpty(AnalyticsScreenName))
                Analytics.TrackScreenView (AnalyticsScreenName);

            base.OnViewOpened ();
        }

        public override void OnViewClosed ()
        {
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
            base.OnViewClosed ();
        }

        protected virtual void OnOrgBegin(SetOrgBeginMessage message)
        {
        }

        protected virtual void OnOrgEnd(SetOrgEndMessage message)
        {
            RefreshTitle = true;
        }
    }
}
