﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Consortium.Client.Core;
using System.Threading;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Linq;

namespace WFM.Core
{
    public class WeeklyOverviewExceptionsViewModel : WeeklyOverviewTabViewModel
    {
        private readonly WeeklyOverviewService m_weeklyOverviewService;
        private readonly IDailyOverviewFilterService m_dailyOverviewFilterService;

        public override string AnalyticsScreenName
        {
            get
            {
                return "WeeklyOverviewExceptions";
            }
        }

        private List<SiteTimecardEntry> m_exceptions;
        public List<SiteTimecardEntry> Exceptions
        {
            get { return m_exceptions; }
            set { m_exceptions = value; RaisePropertyChanged (() => Exceptions); }
        }

        private List<SiteTimecardEntry> m_filteredExceptions;
        public List<SiteTimecardEntry> FilteredExceptions
        {
            get { return m_filteredExceptions; }
            set { m_filteredExceptions = value; RaisePropertyChanged (() => FilteredExceptions); }
        }

        private bool m_majorFilter;
        public bool MajorFilter
        {
            get { return m_majorFilter; }
            set { m_majorFilter = value; RaisePropertyChanged(() => MajorFilter); }
        }

        private List<FilterGroup> m_filters;
        public List<FilterGroup> Filters
        {
            get { return m_filters; }
            set { m_filters = value; RaisePropertyChanged(() => Filters); }
        }

        private FilterGroup m_currentFilter;
        public FilterGroup CurrentFilter
        {
            get { return m_currentFilter; }
            set { m_currentFilter = value; RaisePropertyChanged(() => CurrentFilter); }
        }

        private string m_employeeNameTitle;
        public string EmployeeNameTitle
        {
            get { return m_employeeNameTitle; }
            set { m_employeeNameTitle = value; RaisePropertyChanged (() => EmployeeNameTitle); }
        }

        public DateTime CurrentWeekStart { get; set; }

        public WeeklyOverviewExceptionsViewModel (
            SettingsService settingsService,
            WeeklyOverviewService weeklyOverviewService,
            IMvxMessenger messengerService,
            SessionData sessionData,
            IDailyOverviewFilterService dailyOverviewFilterService
        ) : base(settingsService, messengerService, sessionData)
        {
            m_weeklyOverviewService = weeklyOverviewService;
            EmployeeNameTitle = Localiser.Get ("employee_name_label");
            m_dailyOverviewFilterService = dailyOverviewFilterService;
        }

        protected override async Task Load(DateTime forWeek)
        {
            //Cancel any previous loads, if they are done no bother, if not this will speed things up a touch
            m_loadToken.Cancel ();

            Loading = true;
            LoadingMessage = "Loading Exceptions";

            CurrentWeekStart = forWeek;
            DateTime startDate = forWeek;
            DateTime endDate = startDate.AddDays (7);
            m_loadToken = new CancellationTokenSource();
            var timecardsRequest = m_weeklyOverviewService.GetSiteTimecardsForRange (m_sessionData.Site.SiteID, startDate, endDate, m_loadToken.Token);
            var filterGroupsRequest = m_dailyOverviewFilterService.GenerateFilterGroups(m_sessionData.Site.SiteID);
            await Task.Delay (500);//Min spinner length
            var timecardsResult = await timecardsRequest;
            await filterGroupsRequest;
            OnFiltersChanged();

            if (timecardsResult.WasCancelled)
                return;

            Exceptions = timecardsResult.Timecards.Where (x => x.Exceptions != null && x.Exceptions.Count > 0).ToList ();
	        SetMajorFilter(false);
            UpdateFilteredTimecards();

            if (!timecardsResult.Success)
                EmptyMessage = "Failed to load Daily Overview";
            else
                SetEmptyLabel ();

            if (!timecardsResult.Success)
                EmptyMessage = "Failed to load Exceptions";
            else
                SetEmptyLabel ();

            Loading = false;
            LoadingMessage = string.Empty;

            //Tell root view we are done
            m_messengerService.Publish<WeeklyOverviewTabLoaded> (new WeeklyOverviewTabLoaded (this));
        }

        private void OnFiltersChanged()
        {
            Filters = m_dailyOverviewFilterService.CurrentFilters;
            CurrentFilter = null;
        }

        public void SetEmptyLabel()
        {
            if (FilteredExceptions == null || FilteredExceptions.Count == 0)
                EmptyMessage = (MajorFilter) ? "No Unpaired Punch Exceptions" : "No Exceptions";
            else
                EmptyMessage = string.Empty;
        }

        public void OnApplyFilter(FilterGroup filter)
        {
            if(filter == null)
                m_dailyOverviewFilterService.ClearFilter();
            else
                m_dailyOverviewFilterService.ApplyFilter(filter);

            UpdateFilteredTimecards();
        }

        public void OnMajorFilter()
        {
            if (MajorFilter)
            {
                // End Edit Mode
                SetMajorFilter (false);
            }
            else
            {
                // Start Edit mode
                SetMajorFilter (true);
            }
        }

        //Used if we need filter unpaired punches in DailyOverview too
        public void SetMajorFilter(bool setFilter)
        {
            MajorFilter = setFilter;

            if (Exceptions != null)
            {
                foreach (var timecard in Exceptions)
                {
                    timecard.SetUnpairedPunchFilter(setFilter);
                }

                UpdateFilteredTimecards();
            }

            SetEmptyLabel ();
        }

        private void UpdateFilteredTimecards()
        {
            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Exceptions);

            // Now filter using the Major/Minor toggle
            if(MajorFilter)
                FilteredExceptions = filteredTimecards.Where (x => x.Exceptions != null && DoesContainUnpairedPunch (x.Exceptions)).ToList ();
            else
                FilteredExceptions = filteredTimecards;

            SetEmptyLabel ();
        }

        private bool DoesContainUnpairedPunch(List<TimecardException> exceptions)
        {
            var unpairedPunches = exceptions.Where (x => x.ExceptionType == TimecardExceptionType.Unpaired);
            if (unpairedPunches.Any ())
                return true;

            return false;
        }

        public void OnTimecardSelected(SiteTimecardEntry timecard)
        {
            ShowViewModel<DailyOverviewEditShiftViewModel> ( new { timecard = timecard, date = timecard.Date  } );
        }

        public void OnReturn(bool shiftChanged)
        {
            if(shiftChanged)
                Load (CurrentWeekStart).ConfigureAwait(false);
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            m_dailyOverviewFilterService.OnFiltersChanged += OnFiltersChanged;
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            m_dailyOverviewFilterService.OnFiltersChanged -= OnFiltersChanged;
        }
    }
}

