﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace WFM.Core
{
    public class WeeklyOverviewTimecardViewModel : WeeklyOverviewTabViewModel
    {
        private readonly TimecardService m_timecardService;

        private List<EmployeeTimecardSummary> m_timecards;
        public List<EmployeeTimecardSummary> Timecards
        {
            get { return m_timecards; }
            set { m_timecards = value; RaisePropertyChanged (() => Timecards); CalculateTotals (); }
        }

        private double m_totalScheduled;
        public double TotalScheduled
        {
            get { return m_totalScheduled; }
            set { m_totalScheduled = value; RaisePropertyChanged (() => TotalScheduled); }
        }

        private double m_totalNetHours;
        public double TotalNetHours
        {
            get { return m_totalNetHours; }
            set { m_totalNetHours = value; RaisePropertyChanged (() => TotalNetHours); }
        }

        private double m_totalAdjustments;
        public double TotalAdjustments
        {
            get { return m_totalAdjustments; }
            set { m_totalAdjustments = value; RaisePropertyChanged (() => TotalAdjustments); }
        }

        private double m_totalOtherHours;
        public double TotalOtherHours
        {
            get { return m_totalOtherHours; }
            set { m_totalOtherHours = value; RaisePropertyChanged (() => TotalOtherHours); }
        }

        private double m_totalTotalHours;
        public double TotalTotalHours
        {
            get { return m_totalTotalHours; }
            set { m_totalTotalHours = value; RaisePropertyChanged (() => TotalTotalHours); }
        }

        private int m_totalExceptions;
        public int TotalExceptions
        {
            get { return m_totalExceptions; }
            set { m_totalExceptions = value; RaisePropertyChanged (() => TotalExceptions); }
        }

        private string m_employeeNameTitle;
        public string EmployeeNameTitle
        {
            get { return m_employeeNameTitle; }
            set { m_employeeNameTitle = value; RaisePropertyChanged (() => EmployeeNameTitle); }
        }

        public WeeklyOverviewTimecardViewModel (
            SettingsService settingsService,
            TimecardService timecardService,
            IMvxMessenger messengerService,
            SessionData sessionData
        ) : base(settingsService, messengerService, sessionData)
        {
            m_timecardService = timecardService;
            EmployeeNameTitle = Localiser.Get ("employee_name_label");
        }

        protected override async Task Load(DateTime forWeek)
        {
            //Cancel any previous loads, if they are done no bother, if not this will speed things up a touch
            m_loadToken.Cancel();

            Loading = true;
            LoadingMessage = "Loading Timecard Summary";

            DateTime startDate = forWeek;
            m_loadToken = new CancellationTokenSource();
            var timecardsRequest = m_timecardService.GetTimecardSummaryForWeekBeginning (m_sessionData.Site.SiteID, startDate, m_loadToken.Token);

            await Task.Delay (500);//Min spinner length
            var timecardsResult = await timecardsRequest;

            if (timecardsResult.WasCancelled)
                return;

            Timecards = timecardsResult.Timecards;

            if (!timecardsResult.Success)
                EmptyMessage = "Failed to load Timecard Summary";
            else if (Timecards == null || Timecards.Count == 0)
                EmptyMessage = "No Timecard Summary";
            else
                EmptyMessage = string.Empty;

            Loading = false;
            LoadingMessage = string.Empty;

            //Tell root view we are done
            m_messengerService.Publish<WeeklyOverviewTabLoaded> (new WeeklyOverviewTabLoaded (this));
        }

        private void CalculateTotals()
        {
            TotalScheduled = 0;
            TotalNetHours = 0;
            TotalAdjustments = 0;
            TotalOtherHours = 0;
            TotalTotalHours = 0;
            TotalExceptions = 0;

            if(Timecards!=null)
            {
                foreach (var entry in Timecards)
                {
                    TotalScheduled += entry.Scheduled;
                    TotalNetHours += entry.NetHours;
                    TotalAdjustments += entry.Adjustments;
                    TotalOtherHours += entry.OtherHours;
                    TotalTotalHours += entry.TotalHours;
                    TotalExceptions += entry.Exceptions;
                }
            }
        }
    }
}

