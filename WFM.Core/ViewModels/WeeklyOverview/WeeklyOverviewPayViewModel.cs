﻿using System;

namespace WFM.Core
{
    public class WeeklyOverviewPayViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "WeeklyOverviewPay";
            }
        }

        public WeeklyOverviewPayViewModel ()
        {
        }
    }
}

