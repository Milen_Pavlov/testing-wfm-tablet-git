﻿using System;

namespace WFM.Core
{
    public class WeeklyOverviewClosePayPeriodViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "WeeklyOverviewClosePayPeriod";
            }
        }

        public WeeklyOverviewClosePayPeriodViewModel ()
        {
        }
    }
}

