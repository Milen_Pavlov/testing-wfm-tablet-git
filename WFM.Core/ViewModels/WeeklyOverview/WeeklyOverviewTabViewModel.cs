﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Threading;
using System.Threading.Tasks;

namespace WFM.Core
{
    public class WeeklyOverviewTabViewModel : WFMViewModel
    {
        protected readonly SettingsService m_settingsService;
        protected readonly IMvxMessenger m_messengerService;
        protected readonly SessionData m_sessionData;

        protected MvxSubscriptionToken m_dateChangeToken;
        protected CancellationTokenSource m_loadToken = new CancellationTokenSource();

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set 
            { 
                m_loading = value; 
                RaisePropertyChanged(() => Loading);
            }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private string m_emptyMessage;
        public string EmptyMessage
        {
            get { return m_emptyMessage; }
            set { m_emptyMessage = value; RaisePropertyChanged(() => EmptyMessage); }
        }

        public WeeklyOverviewTabViewModel (
            SettingsService settingsService,
            IMvxMessenger messengerService,
            SessionData sessionData
        )
        {
            m_messengerService = messengerService;
            m_sessionData = sessionData;
        }

        public async Task Init(DateTime startOfWeek)
        {
            //Register messenger
            m_dateChangeToken = m_messengerService.Subscribe<WeeklyOverviewDateMessage>(OnWeekChanged);

            var load = Load (startOfWeek);
            await load;
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            if (m_dateChangeToken == null)
                m_dateChangeToken = m_messengerService.Subscribe<WeeklyOverviewDateMessage>(OnWeekChanged);
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            if (m_dateChangeToken != null)
            {
                m_messengerService.Unsubscribe<WeeklyOverviewDateMessage> (m_dateChangeToken);
                m_dateChangeToken = null;
            }
        }

        public async void OnWeekChanged(WeeklyOverviewDateMessage message)
        {
            var load = Load (message.WeekStarting);
            await load;
        }

        protected virtual async Task Load(DateTime forWeek)
        {
        }
    }
}

