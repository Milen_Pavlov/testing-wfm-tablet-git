using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class WeeklyOverviewViewModel : WFMViewModel
    {
        private readonly ScheduleService m_scheduleService;//Only used for Week Starting
        private readonly SessionData m_sessionData;//Used to get start of week too
        private readonly IMvxMessenger m_messengerService;
        private readonly IDailyOverviewFilterService m_dailyOverviewFilterService;

        private MvxSubscriptionToken m_loadingToken;
        private bool m_weekNumberDisplayed;

        public override string AnalyticsScreenName
        {
            get
            {
                return "WeeklyOverview";
            }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private List<string> m_segments;
        public List<string> Segments
        {
            get { return m_segments; }
            set { m_segments = value; RaisePropertyChanged(() => Segments); }
        }

        private DateTime m_currentStartOfWeek;
        public DateTime CurrentStartOfWeek
        {
            get { return m_currentStartOfWeek; }
            set { m_currentStartOfWeek = value; }
        }

        private string m_title;
        public string Title
        {
            get { return m_title; }
            set 
            {
                m_title = value;
                RaisePropertyChanged (() => Title);
            }
        }

        public WeeklyOverviewViewModel (
            ScheduleService scheduleService,
            SessionData sessionData,
            IMvxMessenger messengerService,
            IDailyOverviewFilterService dailyOverviewFilterService
        )
        {
            m_scheduleService = scheduleService;
            m_sessionData = sessionData;
            m_weekNumberDisplayed = ConfigurationService.WeekNumberDisplayed;
            m_messengerService = messengerService;
            m_dailyOverviewFilterService = dailyOverviewFilterService;
        }

        public void Init ()
        {
            Title = Localiser.Get ("weekly_overview");

            CurrentStartOfWeek = m_sessionData.Site.EffectiveStartOfWeek.Value;

            m_dailyOverviewFilterService.CurrentView = Title;

            m_orgEndToken = m_messengerService.Subscribe<SetOrgEndMessage>(OnOrgEnd);

            Load ();

            ShowViewModel<WeeklyOverviewExceptionsViewModel> ( CurrentStartOfWeek );
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            if (m_loadingToken == null)
                m_loadingToken = m_messengerService.Subscribe<WeeklyOverviewTabLoaded>(OnTabLoaded);
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            if (m_loadingToken != null)
            {
                m_messengerService.Unsubscribe<WeeklyOverviewTabLoaded> (m_loadingToken);
                m_loadingToken = null;
            }
            if(m_orgEndToken != null)
            {
                m_messengerService.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
                m_orgEndToken = null;
            }

        }

        protected override void OnOrgEnd (SetOrgEndMessage message)
        {
            base.OnOrgEnd (message);
            Load();
        }
        public void OnTabLoaded(WeeklyOverviewTabLoaded message)
        {
            Loading = false;
        }

        public void Load()
        {
            UpdateDate (CurrentStartOfWeek);
            //Broadcast refresh messages to tabs
            Loading = true;
            m_messengerService.Publish (new WeeklyOverviewDateMessage (this) { WeekStarting = CurrentStartOfWeek } );
        }
            
        private void UpdateDate(DateTime start)
        {
            int weekNumber = m_scheduleService.AccountingHelper.GetWeekNumberForDate (start);
            
            String startingString = m_weekNumberDisplayed ?
                string.Format (Localiser.Get("week_starting_label"), weekNumber, start.ToString(Localiser.Get("dddd, dd/MM/yyyy"))) :
                string.Format (Localiser.Get("starting_label"), start.ToString(Localiser.Get("dddd, dd/MM/yyyy")));

            Date = startingString;

            // Update days in segments
            List<string> daySegments = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                int dayIndex = (int)start.AddDays(i).DayOfWeek;

                switch(dayIndex)
                {
                    case 0: daySegments.Add(Localiser.Get("sunday_short")); break;
                    case 1: daySegments.Add(Localiser.Get("monday_short")); break;
                    case 2: daySegments.Add(Localiser.Get("tuesday_short")); break;
                    case 3: daySegments.Add(Localiser.Get("wednesday_short")); break;
                    case 4: daySegments.Add(Localiser.Get("thursday_short")); break;
                    case 5: daySegments.Add(Localiser.Get("friday_short")); break;
                    case 6: daySegments.Add(Localiser.Get("saturday_short")); break;
                }
            }

            if(Segments == null || Segments.Count == 0 || Segments[1] != daySegments[1])
                Segments = daySegments;
        }

        public void OnRefresh()
        {
            Load();
        }

        public void OnMoveNext()
        {
            CurrentStartOfWeek = CurrentStartOfWeek.AddDays(7);
            Load();
        }

        public void OnMovePrev()
        {
            CurrentStartOfWeek = CurrentStartOfWeek.AddDays(-7);
            Load();
        }

        public void OnTabSelect(int index)
        {
            switch (index)
            {
                case 0:
                    ShowViewModel<WeeklyOverviewExceptionsViewModel> ( CurrentStartOfWeek );
                    break;
                case 1:
                    ShowViewModel<WeeklyOverviewTimecardViewModel> ( CurrentStartOfWeek );
                    break;
                case 2:
                    ShowViewModel<WeeklyOverviewPayViewModel> ( CurrentStartOfWeek );
                    break;
                case 3:
                    ShowViewModel<WeeklyOverviewClosePayPeriodViewModel> ( CurrentStartOfWeek );
                    break;
                default:
                    throw new ArgumentOutOfRangeException ();
            }
        }

        public void OnFilter()
        {
            
        }

        public void OnDateSelect()
        {
            ShowViewModel<DatePickerViewModel>(new{ currentWeek = CurrentStartOfWeek, numberOfWeeks = 2000, isWeekSelected = false, startingWeekNumber = m_scheduleService.AccountingHelper.GetWeekNumberForDate (CurrentStartOfWeek), currentDay = CurrentStartOfWeek });
        }

        public void OnReturn(SelectedPickerItem item)
        {
            CurrentStartOfWeek = m_scheduleService.AccountingHelper.GetStartOfWeekForDate (item.Date);
            Load();
        }
    }
}

