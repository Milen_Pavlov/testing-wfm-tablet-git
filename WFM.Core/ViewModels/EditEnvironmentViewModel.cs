﻿using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Consortium.Client.Core;
using Siesta;
using Siesta.Client;
using WFM.Core;
using System;

namespace WFM.Core
{
    public class EditEnvironmentViewModel : WFMViewModel
    {
        public static double ANIM_TIME = 0.25;

        private readonly IAlertBox m_alertService;
        private readonly SessionData m_session;

        public enum EditStage
        {
            SetName,
            SetAddress,
            PingAddress,
            Done,
        }

        private Environment m_environment;
        public Environment Environment
        {
            get { return m_environment; }
            set
            {
                m_environment = value;
                RaisePropertyChanged(() => Environment);
            }
        }

        private EditStage m_currentStage;
        public EditStage CurrentStage
        {
            get { return m_currentStage; }
            set
            {
                m_currentStage = value;
                RaisePropertyChanged(() => CurrentStage);
                RaisePropertyChanged(() => NextEnabled);
            }
        }

        private string m_currentTextHint;
        public string CurrentTextHint
        {
            get { return m_currentTextHint; }
            set
            {
                m_currentTextHint = value;
                RaisePropertyChanged(() => CurrentTextHint);
            }
        }

        private string m_currentTitle;
        public string CurrentTitle
        {
            get { return m_currentTitle; }
            set
            {
                m_currentTitle = value;
                RaisePropertyChanged(() => CurrentTitle);
            }
        }

        private string m_currentButtonTitle;
        public string CurrentButtonTitle
        {
            get { return m_currentButtonTitle; }
            set
            {
                m_currentButtonTitle = value;
                RaisePropertyChanged(() => CurrentButtonTitle);
            }
        }

        private bool m_closing;
        public bool Closing
        {
            get { return m_closing; }
            set
            {
                m_closing = value;
                RaisePropertyChanged(() => Closing);
            }
        }

        private bool m_reset;
        public bool Reset
        {
            get { return m_reset; }
            set
            {
                m_reset = value;
                RaisePropertyChanged(() => Reset);
            }
        }

        private bool m_checkingAddress;
        public bool CheckingAddress
        {
            get { return m_checkingAddress; }
            set
            {
                m_checkingAddress = value;
                RaisePropertyChanged(() => CheckingAddress);
            }
        }

        public bool NextEnabled
        {
            get
            {
                switch (CurrentStage)
                {
                    case EditStage.SetName:
                        return !string.IsNullOrEmpty(Environment.Name);
                    case EditStage.SetAddress:
                        return !string.IsNullOrEmpty(Environment.Address);
                }

                return false;
            }
        }

        public string CurrentText
        {
            get { return GetTextValue(); }
            set
            {
                SetTextValue(value);
            }
        }

        public int EditingIndex { get; set; }

        public EditEnvironmentViewModel(IAlertBox alertService, SessionData sessionService)
        {
            m_alertService = alertService;
            m_session = sessionService;
        }

        public void Init()
        {
            Init (null, -1);
        }

        public void Init(Environment environment, int index)
        {
            EditingIndex = index;

            if (environment == null)
                Environment = new Environment () { Address = "http://localhost/" };
            else
                Environment = environment;

            CurrentStage = EditStage.SetName;
            CurrentTextHint = "Enter Name";
            CurrentTitle = "Environment Name";
            CurrentButtonTitle = "Next";
            CheckingAddress = false;
        }

        public void SetTextValue(string s)
        {
            switch(CurrentStage)
            {
                case EditStage.SetName:
                    Environment.Name = s;
                    break;
                case EditStage.SetAddress:
                    Environment.Address = s;
                    break;
            }

            RaisePropertyChanged(() => NextEnabled);
        }

        public string GetTextValue()
        {
            switch(CurrentStage)
            {
                case EditStage.SetName:
                    return Environment.Name;
                case EditStage.SetAddress:
                    return Environment.Address;
            }

            return string.Empty;
        }

        public async void OnNext()
        {
            RaisePropertyChanged(() => NextEnabled);

            switch (CurrentStage)
            {
                case EditStage.SetName:
                    if (!string.IsNullOrEmpty(Environment.Name))
                    {
                        //Check if Unique
                        var indices = Settings.IndicesForEnvironmentName(Environment.Name);
                        if (indices != null && indices.Count > 0 && !indices.Contains(EditingIndex))
                        {
                            m_alertService.ShowOK("Invalid Name", "There is already an environment configured with that name");
                        }
                        else
                        {
                            CurrentStage = EditStage.SetAddress;
                            CurrentTextHint = "Enter Address";
                            CurrentTitle = "Environment URL";
                            Reset = true;
                        }
                    }
                    break;
                case EditStage.SetAddress:
                    if (!string.IsNullOrEmpty(Environment.Address))
                    {
                        //Check valid URI
                        bool valid = Uri.IsWellFormedUriString(Environment.Address, UriKind.Absolute);
                        if (!valid)
                        {
                            m_alertService.ShowOK("Invalid Address", "The address you entered is not a valid URL format");
                        }
                        else
                        {
                            //Ping address as a check
                            CheckingAddress = true;
                            CurrentStage = EditStage.PingAddress;
                            CurrentTitle = "Checking Address...";

                            //Alter address - This is hacky but we have an issue with Siesta not ignoring hte case when checking URLs to Cookies
                            //so all JDA environments that are URL/Retail/ need to be captial R, URL/retail/ will fail...

                            var res = await m_session.CheckExists(Environment.Address);
                            if (Closing)//Took too long
                                return;

                            if (res)
                            {
                                m_alertService.ShowOK("Verified", "The address has been verified as a valid environment",(int i) =>
                                    {
                                        CheckingAddress = false;
                                        OnDone();
                                    });
                            }
                            else
                            {
                                m_alertService.ShowYesNo("Invalid", "The address could not be verified as a valid environment, do you wish to continue anyway?",(int i) =>
                                    {
                                        if(i==1)
                                        {
                                            OnDone();
                                        }
                                        else
                                        {
                                            CurrentStage = EditStage.SetAddress;
                                            CurrentTextHint = "Enter Address";
                                            CurrentTitle = "Environment URL";
                                            CurrentButtonTitle = "Next";
                                            Reset = true;
                                        }

                                        CheckingAddress = false;
                                    });
                            }

                        }
                    }
                    break;
            }
        }

        public async void OnDone()
        {
            CurrentStage = EditStage.Done;
            Closing = true;

            await Task.Delay((int)(ANIM_TIME * 1000));

            Close(new { environment = Environment });
        }

        public async void OnCancel()
        {
            CurrentStage = EditStage.Done;
            Closing = true;

            await Task.Delay((int)(ANIM_TIME * 1000));

            Close();
        }
    }
}

