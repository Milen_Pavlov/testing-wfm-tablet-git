﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ForecastGroupViewModel : WFMViewModel
    {
        private List<JDANameAndID> m_groups;
        public List<JDANameAndID> Groups
        {
            get { return m_groups; }
            set { m_groups = value; RaisePropertyChanged(() => Groups); }
        }

        private int m_groupIndex;
        public int GroupIndex
        {
            get { return m_groupIndex; }
            set { m_groupIndex = value; RaisePropertyChanged(() => GroupIndex); }
        }

        public ForecastGroupViewModel ()
        {            
        }

        public void Init(List<JDANameAndID> groups)
        {
            m_groups = groups;
        }

        public void OnSelectGroup(JDANameAndID group)
        {
            Close (group);
        }
    }
}

