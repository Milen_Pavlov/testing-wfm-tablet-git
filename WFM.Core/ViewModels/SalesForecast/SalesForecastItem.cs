﻿using System;
using System.Collections;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class SalesForecastItem : ISection
    {
        public string Title { get; set; }
        public IList Items { get; set; }
    }
}

