﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace WFM.Core
{
    public class SalesForecastViewModel : WFMViewModel
    {
        private readonly ISalesForecastService m_salesForecastService;
        private readonly SessionData m_data;

        private List<JDANameAndID> m_groups;
        public List<JDANameAndID> Groups
        {
            get { return m_groups; }
            set 
            { 
                m_groups = value; 
                RaisePropertyChanged(() => Groups); 
                RaisePropertyChanged (() => ReceivedGroupData); 
            }
        }

        private List<SalesForecastItem> m_items;
        public List<SalesForecastItem> Items
        {
            get { return m_items; }
            set { m_items = value; RaisePropertyChanged(() => Items); }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        private List<string> m_dates;
        public List<string> Dates
        {
            get { return m_dates; }
            set { m_dates = value; RaisePropertyChanged(() => Dates); }
        }

        private JDANameAndID m_selectedGroup;
        public JDANameAndID SelectedGroup
        {
            get { return m_selectedGroup; }
            set { m_selectedGroup = value; RaisePropertyChanged(() => SelectedGroup); }
        }

        private DateTime m_startDate;
        public DateTime StartDate
        {
            get { return m_startDate; }
            set 
            { 
                m_startDate = value; 
                RaisePropertyChanged(() => StartDate);
            }
        }

        private bool m_ShowLoadingView;
        public bool IsLoadingViewHidden
        {
            get { return m_ShowLoadingView; }
            set { m_ShowLoadingView = value; RaisePropertyChanged(() => IsLoadingViewHidden ); }
        }

        private bool m_enableCancelButton;
        public bool EnableCancelButton
        {
            get { return m_enableCancelButton; }
            set { m_enableCancelButton = value; RaisePropertyChanged(() => EnableCancelButton ); }
        }

        private bool m_enableSubmitButton;
        public bool EnableSubmitButton
        {
            get { return m_enableSubmitButton; }
            set { m_enableSubmitButton = value; RaisePropertyChanged(() => EnableSubmitButton ); }
        }

        public bool ReceivedGroupData
        {
            get
            {
                return Groups != null;
            }
            set
            {
                RaisePropertyChanged (() => ReceivedGroupData);
            }
        }

        public SalesForecastViewModel (ISalesForecastService forecastService,
            SessionData data)
        {
            m_salesForecastService = forecastService;
            m_data = data;
            IsLoadingViewHidden = false;
            Mapper.CreateMap<JDAForecastedData, SalesForecastDataItem> ();
        }

        public async void Init()
        {
            // update the date
            StartDate = m_data.Site.EffectiveStartOfWeek.Value;
            UpdateDate (StartDate);

            // get groups, add all option to the end, set current
            IsLoadingViewHidden = false;

            Groups = await m_salesForecastService.GetForecastGroups ();

            IsLoadingViewHidden = true;

            Groups.Add (new JDANameAndID (){ ID = -1, Name = "All Forecast Groups" });

            int selectedIndex = m_salesForecastService.GetSelectedIndex ();
            SelectedGroup = Groups.Where (x => x.ID.Equals (selectedIndex)).FirstOrDefault ();   

            if(SelectedGroup == null)
                SelectedGroup = Groups.FirstOrDefault ();

            // get the items
            GetForecastData ();
        }

        public void OnReturn(JDANameAndID item)
        {
            SelectedGroup = item;
            GetForecastData ();
        }

        public void OnMoveNext()
        {
            StartDate = StartDate.AddDays (7);
            UpdateDate (StartDate);
             
            m_salesForecastService.CancelForecastRead ();
            GetForecastData ();
        }

        public void OnMovePrev()
        {
            StartDate = StartDate.AddDays (-7);
            UpdateDate (StartDate);

            m_salesForecastService.CancelForecastRead ();
            GetForecastData ();
        }

        public void ShowGroupList()
        {
            ShowViewModel<ForecastGroupViewModel> (new { groups = Groups });
        }

        protected void UpdateDate(DateTime start)
        {
            Date = string.Format (Localiser.Get ("starting_label"), start.ToString (Localiser.Get("dd/MM/yyyy"))); 
            List<string> formattedDates = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                formattedDates.Add(start.AddDays(i).ToString(Localiser.Get("dd/MM")));
            }
            Dates = formattedDates;
        }

        public async void GetForecastData()
        {
            if (SelectedGroup == null)
                throw new Exception ("No Group");
            EnableCancelButton = false;
            EnableSubmitButton = false;
            IsLoadingViewHidden = false;
            var salesForecastItems = await m_salesForecastService.GetSalesForecastData (StartDate, SelectedGroup.ID, 500);
            IsLoadingViewHidden = true;

            if (salesForecastItems != null)
            {
                // first get items by department
                var grouped = salesForecastItems.GroupBy(item => item.ForecastGroupName);
                List<SalesForecastItem>  forTable = new List<SalesForecastItem> ();

                // store
                foreach(var groupItems in grouped)
                {
                    var filtered = (from r in groupItems
                        where !r.AccessControlType.Equals ("h")
                        orderby r.MetricSortOrder
                        select r
                    ).ToList ();

                    // now put into new data format
                    List<SalesForecastDataItem> itemsToInsert = new List<SalesForecastDataItem> ();
                    foreach(var item in filtered)
                    {
                        SalesForecastDataItem data = Mapper.Map<SalesForecastDataItem> (item);
                        data.IsOverview = SelectedGroup.ID.Equals("-1");
                        data.Days = new List<SalesForecastDayItem> ()
                        {
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day1Value, DayValue = item.Day1Value, Date = StartDate },
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day2Value, DayValue = item.Day2Value, Date = StartDate.AddDays(1) },
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day3Value, DayValue = item.Day3Value, Date = StartDate.AddDays(2) },
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day4Value, DayValue = item.Day4Value, Date = StartDate.AddDays(3) },
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day5Value, DayValue = item.Day5Value, Date = StartDate.AddDays(4) },
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day6Value, DayValue = item.Day6Value, Date = StartDate.AddDays(5) },
                                new SalesForecastDayItem (){  OriginalDayValue = item.Day7Value, DayValue = item.Day7Value, Date = StartDate.AddDays(6) },
                        };
                        itemsToInsert.Add (data);
                    }
                    forTable.Add (new SalesForecastItem (){ Title = groupItems.Key, Items = itemsToInsert });
                }

                Items = forTable.OrderBy(x => x.Title).ToList();
            }
        }

        public async void SubmitChanges()
        {
            IsLoadingViewHidden = false;
            // go through sections, find edited items
            List<SalesForecastDataItem> modifiedItems = new List<SalesForecastDataItem>();
            foreach(var item in Items)
            {
                List<SalesForecastDataItem> its = item.Items as List<SalesForecastDataItem>;
                var m = its.Select (p => p).Where(p => p.Days.Any(x => x.Modified)).ToList ();

                if(m != null && (m.Count > 0))
                {
                    // remove any days that are not modified
                    foreach(var moddedItem in m)
                    {
                        moddedItem.Days.RemoveAll (x => x.Modified == false);
                    }
                    modifiedItems.AddRange (m);
                }
            }
            // send these off to jda to create 
            if(modifiedItems.Count > 0)
            {
                bool success = await m_salesForecastService.SubmitChanges (modifiedItems);
                if(success)
                {
                    GetForecastData ();
                }
            }
            EnableCancelButton = false;
            EnableSubmitButton = false;
            IsLoadingViewHidden = true;
        }


        public void CancelChanges()
        {
            m_salesForecastService.CancelSubmitChanges ();

            foreach(var item in Items)
            {
                List<SalesForecastDataItem> its = item.Items as List<SalesForecastDataItem>;
                var m = its.Select (p => p).Where(p => p.Days.Any(x => x.Modified)).ToList ();

                if(m != null && (m.Count > 0))
                {
                    // remove any days that are not modified
                    foreach(var moddedItem in m)
                    {
                        var toReset = moddedItem.Days.Select (p => p).Where (x => x.Modified == true);
                        foreach (var day in toReset)
                        {
                            day.DayValue = day.OriginalDayValue;
                            day.Modified = false;
                        }
                        moddedItem.WeekTotal = moddedItem.Days.Sum (x => x.DayValue).GetValueOrDefault (0);
                    }
                }
            }
            Items = Items;
            EnableCancelButton = false;
            EnableSubmitButton = false;
        }

        public void OnRowChanged()
        {
            bool modified = false;
            List<bool> potentiallyModified = new List<bool> ();
            foreach(var item in Items)
            {
                List<SalesForecastDataItem> its = item.Items as List<SalesForecastDataItem>;
                modified = its.Any (p => p.Days.Any (x => x.Modified == true));
            }
            EnableSubmitButton = modified;
            EnableCancelButton = modified;
        }
    }
}

