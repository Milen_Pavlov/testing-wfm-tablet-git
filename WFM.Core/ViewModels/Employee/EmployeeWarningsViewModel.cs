﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class EmployeeWarningsViewModel : WFMViewModel
    {
        private List<string> m_warnings;
        public List<string> Warnings
        {
            get { return m_warnings; }
            set { m_warnings = value; RaisePropertyChanged(() => Warnings); }
        }

        public EmployeeWarningsViewModel()
        {
        }

        public void Init(string info)
        {
            Warnings = (List<string>)JsonConvert.DeserializeObject<List<string>>(info);
        }
    }
}

