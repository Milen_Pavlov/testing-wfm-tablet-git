﻿using System;
using Consortium.Client.Core;
using Newtonsoft.Json;

namespace WFM.Core
{
    public class EmployeeInfoViewModel : WFMViewModel
    {
        private string m_employeeName;
        public string EmployeeName
        {
            get { return m_employeeName; }
            set { m_employeeName = value; RaisePropertyChanged(() => EmployeeName); }
        }

        private string m_homeSite;
        public string HomeSite
        {
            get { return m_homeSite; }
            set { m_homeSite = value; RaisePropertyChanged(() => HomeSite); }
        }

        private string m_primaryJob;
        public string PrimaryJob
        {
            get { return m_primaryJob; }
            set { m_primaryJob = value; RaisePropertyChanged(() => PrimaryJob); }
        }

        private string m_homePhone;
        public string HomePhone
        {
            get { return m_homePhone; }
            set { m_homePhone = value; RaisePropertyChanged(() => HomePhone); }
        }

        private string m_mobilePhone;
        public string MobilePhone
        {
            get { return m_mobilePhone; }
            set { m_mobilePhone = value; RaisePropertyChanged(() => MobilePhone); }
        }

        public EmployeeInfoViewModel()
        {
        }

        public void Init(string info)
        {
            var employeeInfo = JsonConvert.DeserializeObject<JDAEmployeeInfo>(info);

            EmployeeName = employeeInfo.FirstName + " " + employeeInfo.LastName;
            HomeSite = employeeInfo.HomeSiteName;
            PrimaryJob = employeeInfo.PrimaryJobName;
            HomePhone = employeeInfo.HomePhone;
            MobilePhone = employeeInfo.CellPhone;
        }
    }
}

