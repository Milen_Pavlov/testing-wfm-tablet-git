﻿using System;

namespace WFM.Core
{
    public class ModalTextInputViewModel : WFMViewModel
    {
        public class ReturnParameters
        {
            public bool IsSubmitted { get; set; }

            public string Text { get; set; }
        }

        private string m_Title;
        public string Title
        {
            get
            {
                return m_Title;
            }
            set
            {
                m_Title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        private string m_Text;
        public string Text
        {
            get
            {
                return m_Text;
            }
            set
            {
                m_Text = value;
                RaisePropertyChanged(() => Text);
            }
        }

        public ModalTextInputViewModel ()
        {
            
        }

        public void Init(string title)
        {
            Title = title;
        }

        
        public void OnCancel()
        {
            Close (new ReturnParameters () { IsSubmitted = false });
        }

        public void OnSubmit()
        {
            Close (new ReturnParameters () { IsSubmitted = true, Text = this.Text });
        }
    }
}

