﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using WFM.Core.NightAisle;

namespace WFM.Core
{
    public class NightAislePlannerSettingsViewModel : WFMViewModel
    {
        private readonly INightAisleService m_nightAisleService;
        private readonly IMvxMessenger m_messenger;
        private readonly IAlertBox m_alertBox;
        private readonly ITimeFormatService m_timeFormatService;
        private readonly IESSConfigurationService m_essConfig;
        private readonly SessionData m_sessionData;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public override string AnalyticsScreenName
        {
            get
            {
                return "Night Aisle Planner - Settings Tab";
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private bool m_canSave;
        public bool CanSave
        {
            get { return m_canSave; }
            set 
            { 
                m_canSave = value; 
                RaisePropertyChanged(() => CanSave);
            }
        }

        //Is save valid?
        public bool IsValid
        {
            get
            {
                return true;
            }
        }

        public bool IsEdited
        {
            get;
            set;
        }

        private DateTime m_startTime;
        public DateTime StartTime
        {
            get { return m_startTime; }
            set {
                m_startTime = value; 
                RaisePropertyChanged(() => StartTime); 
                RaisePropertyChanged(() => StartTimeText); 
            }
        }

        private DateTime m_endTime;
        public DateTime EndTime
        {
            get { return m_endTime; }
            set { 
                m_endTime = value; 
                RaisePropertyChanged(() => EndTime);
                RaisePropertyChanged(() => EndTimeText);
            }
        }

        public string StartTimeText
        {
            get { return m_timeFormatService.FormatTime(m_startTime); }
        }

        public string EndTimeText
        {
            get { return m_timeFormatService.FormatTime(m_endTime); }
        }
   
        public NightAislePlannerSettingsViewModel (INightAisleService nightAisleService,
                                                   IMvxMessenger messenger,
                                                  IAlertBox alertBox,
                                                  ITimeFormatService timeFormatService,
                                                  IESSConfigurationService essConfig,
                                                  SessionData sessionData)
        {
            m_nightAisleService = nightAisleService;
            m_messenger = messenger;
            m_alertBox = alertBox;
            m_timeFormatService = timeFormatService;
            m_essConfig = essConfig;
            m_sessionData = sessionData;
        }

        public void Init()
        {
            Refresh ();
        }

        public void Refresh()
        {
            if (Loading)
                return;

            if (CanSave)
            {
                m_alertBox.ShowYesNo(
                              Localiser.Get("discard_changes_message"),
                              Localiser.Get("allocations_confirm_discard_message"),
                              (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                            {
                                DoRefresh();
                                break;
                            }
                        }
                    }
                             );
            }
            else
            {
                DoRefresh();
            }
        }

        private async void DoRefresh()
        {
            LoadingMessage = Localiser.Get("allocations_loading_message");
            Loading = true;

            if (m_tokenSource != null)
                m_tokenSource.Cancel ();

            m_tokenSource = new CancellationTokenSource ();
            var token = m_tokenSource.Token;

            //Load Settings
            StartTime = DateTime.Now.Date.AddHours(m_essConfig.NightShiftStartTime);
            EndTime = StartTime.AddHours(m_essConfig.NightShiftDuration);

            var request = m_nightAisleService.GetReplenishmentStartAndEndTime(m_sessionData.Site.SiteID, token);

            await Task.Delay (500);//Min delay

            var response = await request;

            if (token.IsCancellationRequested)
                return;

            StartTime = DateTime.Now.Date.AddHours(response.Object.Item1);
            EndTime = StartTime.AddHours(response.Object.Item2);

            CanSave = false;
            Loading = false;
            IsEdited = false;
        }

        public void OnSave()
        {
            if (CanSave)
            {
                m_alertBox.ShowOptions
                          (
                              Localiser.Get("allocations_save_message"),
                              Localiser.Get("confirm_save_message"),
                              new List<string>() { Localiser.Get("save_changes_message"),  Localiser.Get("discard_changes_message") },  
                              Localiser.Get("continue_editing_message"),
                              (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Save
                            {
                                TrySave();
                                break;
                            }
                            case 2: // Revert
                            {
                                DoRefresh();
                                CanSave = false;
                                break;
                            }
                        }
                    }
                             );
            }
        }

        private void TrySave()
        {
            if (!IsValid)
            {
                m_alertBox.ShowOK(Localiser.Get("validation_errors_message"), Localiser.Get("night_aisle_planner_aisles_cannot_be_saved_message"));
            }
            else
            {
                DoSave(() => { CanSave = false; });
            }
        }

        private async void DoSave(Action completed)
        {
            Loading = true;
            LoadingMessage = Localiser.Get("saving_ellipsis");

            // Save the Settings here
            var request = m_nightAisleService.SetReplenishmentStartAndEndTime(m_sessionData.Site.SiteID, (StartTime - StartTime.Date).TotalHours, (EndTime - StartTime).TotalHours);
            var response = await request;

            bool needsRefresh = response.Success;

            if (needsRefresh)
            {
                completed.Invoke ();
                DoRefresh ();
            } 
            else 
            {
                Loading = false;
                LoadingMessage = string.Empty;
            }
        }

        private void CheckModified(Action<int> saveCallback)
        {
            if (IsEdited)
            {
                m_alertBox.ShowOptions (Localiser.Get("allocation_modified"), Localiser.Get("schedule_changes_message"), 
                                        new List<string> () { Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                                        Localiser.Get("continue_editing_message"), saveCallback );
            }
            else
            {
                saveCallback(2);
            }
        }

        public void OnStartTimeClicked()
        {
            ShowTimePicker();
        }

        public void OnEndTimeClicked()
        {
            ShowTimePicker();
        }

        private void ShowTimePicker()
        {
            ShowViewModel<ShiftTimeViewModel>(new { ShiftStart = StartTime, ShiftEnd = EndTime });
        }

        public void OnReturn(ShiftItem item, DateTime start, DateTime end)
        {
            StartTime = start;
            EndTime = end;
            while(EndTime < StartTime)
            {
                EndTime = EndTime.AddHours(24);
            }
            while(StartTime.AddHours(24) < EndTime)
            {
                EndTime = EndTime.AddHours(-24);
            }

            IsEdited = true;
            CanSave = true;
        }
    }
}

