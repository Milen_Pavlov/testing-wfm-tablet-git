﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using WFM.Core.NightAisle;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class NightAislePlannerViewModel : WFMViewModel
    {
        public enum NightAisleMode
        {
            Planner,
            Aisles,
            Settings,
        }

        public override string AnalyticsScreenName
        {
            get
            {
                return "Night Aisle Planner";
            }
        }

        List<NightAisleMode> Tabs
        {
            get
            {
                List<NightAisleMode> tabs = new List<NightAisleMode> ();
                tabs.Add (NightAisleMode.Planner);
                tabs.Add (NightAisleMode.Aisles);
                tabs.Add (NightAisleMode.Settings);

                return tabs;
            }
        }

        public bool EditMode { get ; set;}

        protected MvxSubscriptionToken m_editModeActive;
        protected IMvxMessenger m_messenger;

        public List<string> TabNames
        {
            get
            {
                List<string> tabNames = new List<string> ();
                foreach(NightAisleMode tab in this.Tabs)
                {
                    switch(tab)
                    {
                        case NightAisleMode.Planner:
                            tabNames.Add(Localiser.Get("night_aisle_planner_planner"));
                            break;
                        case NightAisleMode.Aisles:
                            tabNames.Add(Localiser.Get("night_aisle_planner_aisles"));
                            break;
                            case NightAisleMode.Settings:
                            tabNames.Add(Localiser.Get("night_aisle_planner_settings"));
                        break;
                        default:
                            break;
                    }
                }

                return tabNames;
            }
        }
            
        public NightAislePlannerViewModel (IMvxMessenger messenger, IAlertBox alertBox)
        {
            this.m_messenger = messenger;
        }

        public void Init ()
        {
            m_editModeActive = m_messenger.SubscribeOnMainThread<OnEditModeMessage> (OnEditModeChanged);
            OnTabSelect (0);
        }

        void OnEditModeChanged(OnEditModeMessage obj)
        {
            EditMode = obj.Editing;
        }

        public void OnTabSelect(int index)
        {            
            var tabs = Tabs;

            switch (tabs [index])
            {
                case NightAisleMode.Planner:
                    ShowViewModel<NightAislePlannerPlannerViewModel> ();
                    break;
                case NightAisleMode.Aisles:
                    ShowViewModel<NightAislePlannerAislesViewModel> ();
                    break;
                case NightAisleMode.Settings:
                    ShowViewModel<NightAislePlannerSettingsViewModel> ();
                    break;
                default:
                    throw new ArgumentOutOfRangeException ();
            }
        }
    }
}

