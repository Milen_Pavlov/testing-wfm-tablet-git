﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Newtonsoft.Json;
using System.Threading.Tasks;
using WFM.Core;
using WFM.Shared.Extensions;
using Consortium.Client.Core.Models;
using System.Threading;

namespace WFM.Core.NightAisle
{
    public class NightAislePlannerAislesViewModel : WFMViewModel
    {
        private readonly INightAisleService m_nightAisleService;
        private readonly SessionData m_sessionData;
        private readonly IAlertBox m_alertBox;
        private readonly IMvxMessenger m_messenger;
        private readonly SessionService m_session;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public override string AnalyticsScreenName
        {
            get
            {
                return "Night Aisle Planner";
            }
        }

        private IList<Aisle> m_aisles;
        public IList<Aisle> Aisles {
            get { return m_aisles; }
            set
            {
                m_aisles = value;
                RaisePropertyChanged (() => Aisles);
            }
        }

        private Aisle m_selectedAisle;
        public Aisle SelectedAisle
        {
            get { return m_selectedAisle; }
            set
            {
                m_selectedAisle = value;
                UpdateSelectedAisleIndex ();
                RaisePropertyChanged (() => SelectedAisle);
            }
        }

        private int m_selectedAisleIndex = 0;
        public int SelectedAisleIndex
        {
            get { return m_selectedAisleIndex; }
            set { m_selectedAisleIndex = value; RaisePropertyChanged (() => SelectedAisleIndex);}
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set 
            { 
                m_editMode = value; 
                RaisePropertyChanged(() => EditMode);
                if (Aisles != null)
                {
                    foreach (var aisle in Aisles)
                    {
                        aisle.IsDraggable = m_editMode;
                    }
                }
                m_messenger.Publish (new OnEditModeMessage (this) { Editing = value} );
            }
        }

        public bool IsValid
        {
            get
            {
                return true;
            }
        }

        public NightAislePlannerAislesViewModel (INightAisleService nightAisle, 
            SessionData sessionData, 
            IAlertBox alert,
            IMvxMessenger messenger,
            SessionService session)
        {
            m_nightAisleService = nightAisle;
            m_sessionData = sessionData;
            m_alertBox = alert;
            m_messenger = messenger;
            m_session = session;
        }

        public void Init()
        {
            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            Refresh ();
        }

        protected override void OnOrgEnd (SetOrgEndMessage message)
        {
            base.OnOrgEnd (message);

            if (message.SessionTimeout)
            {
                m_session.SignOut ();
            }
            else
            {
                Refresh ();
            }
        }

        private void Refresh()
        {
            if (EditMode)
            {
                m_alertBox.ShowYesNo
                (
                    Localiser.Get("discard_changes_message"),
                    Localiser.Get("allocations_confirm_discard_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    DoRefresh();
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                DoRefresh();
            }
        }

        private async void DoRefresh()
        {
            LoadingMessage = Localiser.Get("allocations_loading_message");
            Loading = true;

            m_aisles = null;

            if (m_tokenSource != null)
                m_tokenSource.Cancel ();

            m_tokenSource = new CancellationTokenSource ();
            var token = m_tokenSource.Token;

            // Load aisle data here.
            var aisles = m_nightAisleService.GetAisleConfiguration(m_sessionData.Site.SiteID, token);
            await Task.Delay (500);
            await aisles;

            if (token.IsCancellationRequested)
                return;

            Aisles = aisles.Result.Object.OrderBy(a => a.Priority).ToList();
            SelectedAisle = null;

            EditMode = false;
            Loading = false;
        }

        public void OnRefresh()
        {
            Refresh();
        }

        public void OnEdit()
        {
            SwitchEditMode();
        }

        private void SwitchEditMode()
        {
            if (EditMode)
            {
                m_alertBox.ShowOptions
                (
                    Localiser.Get("allocations_save_message"),
                    Localiser.Get("confirm_save_message"),
                    new List<string>() { Localiser.Get("save_changes_message"),  Localiser.Get("discard_changes_message") },  
                    Localiser.Get("continue_editing_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Save
                                {
                                    TrySave();
                                    break;
                                }
                            case 2: // Revert
                                {
                                    DoRefresh();
                                    EditMode = false;
                                    SelectedAisle = null;
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                EditMode = true;
            }
        }

        private void TrySave()
        {
            if (!IsValid)
            {
                m_alertBox.ShowOK(Localiser.Get("validation_errors_message"), Localiser.Get("night_aisle_planner_aisles_cannot_be_saved_message"));
            }
            else
            {
                DoSave(() => { EditMode = false; });
            }
        }

        private async void DoSave(Action completed)
        {
            Loading = true;
            LoadingMessage = Localiser.Get("saving_ellipsis");

            // Save the Aisles here
            bool needsRefresh;
            var save = m_nightAisleService.SetAisleConfiguration (m_sessionData.Site.SiteID, Aisles);
            await Task.Delay (200);
            await save;

            needsRefresh = save.Result.Success;

            if (needsRefresh)
            {
                completed.Invoke ();
                DoRefresh ();
            } 
            else 
            {
                Loading = false;
                LoadingMessage = string.Empty;
            }
        }

        public void OnCreateAisle()
        {
            Aisles = Aisles._NewIfNull();

            int nextId = 0;
            int nextPriority = 0;
            foreach( var a in Aisles)
            {
                if(a.ID >= nextId)
                    nextId = a.ID + 1;

                if (a.Priority.GetValueOrDefault () >= nextPriority)
                    nextPriority = a.Priority.GetValueOrDefault () + 1;
            }
            var newAisle = new Aisle () { Name = "New Aisle", ID = nextId, IsDraggable = EditMode, Priority = nextPriority };
            Aisles.Add(newAisle);
            var temp = Aisles;
            Aisles = null;
            Aisles = temp;
            SelectedAisle = newAisle;  
        }

        public void OnAisleOrderChanged(object sender, AisleOrderChangedEventArgs e)
        {
            if (Aisles == null || e.AisleOrder == null)
                return;

            foreach (var aisle in Aisles)
            {
                aisle.Priority = e.AisleOrder.IndexOf (aisle.ID);
            }
            
            Aisles = Aisles.OrderBy (a => a.Priority).ToList ();

            UpdateSelectedAisleIndex ();
        }

        public void OnAisleSelected(Aisle selectedAisle)
        {
            SelectedAisle = selectedAisle;
        }

        private void UpdateSelectedAisleIndex()
        {
            //Refind our selected aisle if we can
            if (SelectedAisle != null)
            {
                for (int i = 0; i < Aisles.Count; i++)
                {
                    if (Aisles[i].ID == SelectedAisle.ID)
                        SelectedAisleIndex = i;
                }
            }
            else
                SelectedAisleIndex = -1;
        }

        public void OnDeleteAisle(Aisle aisleToDelete)
        {
            m_alertBox.ShowYesNo(
                Localiser.Get("night_aisle_planner_delete_aisle_message"),
                Localiser.Get("night_aisle_planner_confirm_delete_aisle_message"),
                (option) =>
                {
                    switch (option)
                    {
                        case 1: // Yes
                            {
                                DoDeleteAisle(aisleToDelete);
                                break;
                            }
                    }
                });
        }

        private void DoDeleteAisle(Aisle aisle)
        {
            if (aisle != null)
            {
                Aisles.Remove(aisle);
                var temp = Aisles;
                Aisles = null;
                Aisles = temp;
                SelectedAisle = null;
            }
        }
    }

    public class AisleOrderChangedEventArgs : EventArgs
    {
        public List<int> AisleOrder { get; set; }

        public AisleOrderChangedEventArgs(List<int> aisleOrder)
        {
            AisleOrder = aisleOrder;
        }
    }
}