using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Newtonsoft.Json;
using System.Threading.Tasks;
using WFM.Core.NightAisle;
using WFM.Shared.Extensions;
using Cirrious.MvvmCross.ViewModels;
using WFM.Core.Schedule;
using System.Threading;

namespace WFM.Core
{
    public class NightAislePlannerPlannerViewModel : WFMViewModel
    {
        private readonly INightAisleService m_nightAisleService;
        private readonly IAlertBox m_alertBox;
        private readonly SessionData m_sessionData;
        private readonly IMvxMessenger m_messenger;
        private readonly JDAScheduleService m_scheduleService;
        private readonly IESSConfigurationService m_essConfig;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        public override string AnalyticsScreenName
        {
            get
            {
                return "Night Aisle Planner - Planner Tab";
            }
        }

        private IList<Aisle> m_aisles;
        public IList<Aisle> Aisles 
        {
            get { return m_aisles; }
            set
            {
                m_aisles = value;
                RaisePropertyChanged (() => Aisles);
            }
        }

        private IList<AisleMapping> m_mappings;
        public IList<AisleMapping> Mappings
        {
            get { return m_mappings; }
            set
            {
                m_mappings = value;
                RaisePropertyChanged (() => Mappings);
            }
        }

        private IList<NightAisleModel> m_data;
        public IList<NightAisleModel> Data
        {
            get { return m_data; }
            set
            {
                m_data = value;
                RaisePropertyChanged (() => Data);
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set 
            { 
                m_editMode = value; 
                RaisePropertyChanged(() => EditMode);
                m_messenger.Publish (new OnEditModeMessage (this) { Editing = value });
            }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        private DateTime m_weekStart;
        public DateTime WeekStart
        {
            get { return m_weekStart; }
            set { m_weekStart = value; RaisePropertyChanged (() => WeekStart);}
        }

        private List<ColumnHeader> m_headers;
        public List<ColumnHeader> Headers
        {
            get { return m_headers; }
            set { m_headers = value; RaisePropertyChanged(() => Headers); }
        }

        private List<ScheduleData> m_scheduleData;
        public List<ScheduleData> ScheduleData
        {
            get { return m_scheduleData; }
            set { m_scheduleData = value; }
        }

        //Is save valid?
        public bool IsValid
        {
            get
            {
                return true;
            }
        }

        public bool IsEdited
        {
            get;
            set;
        }

        private bool m_weekNumberDisplayed;
        private NightAisleDayModel m_pickedAisleDay;

        private List<EmployeePickerData> m_employeeList;

        private double m_startTime;
        private double m_duration;

        private List<string> m_jobFilter;

        public NightAislePlannerPlannerViewModel (
            JDAScheduleService scheduleService,
            INightAisleService nightAisle, 
            SessionData sessionData, 
            IAlertBox alertService, 
            IMvxMessenger messenger,
            IESSConfigurationService essConfig)
        {
            m_nightAisleService = nightAisle;
            m_alertBox = alertService;
            m_sessionData = sessionData;
            m_messenger = messenger;
            m_weekNumberDisplayed = ConfigurationService.WeekNumberDisplayed;
            m_scheduleService = scheduleService;
            m_essConfig = essConfig;

            WeekStart = m_sessionData.Site.EffectiveStartOfWeek.Value;
        }

        public void Init()
        {
            m_jobFilter = GetJobFilter();
            Refresh ();
        }

        //Date navigation
        public void OnMoveNext()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        TrySave();

                    WeekStart = WeekStart.AddDays(7);

                    DoRefresh();
                }
            });
        }

        public void OnMovePrev()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        TrySave();

                    WeekStart = WeekStart.AddDays(-7);

                    DoRefresh();
                }
            });
        }

        public void SetDate(DateTime date)
        {
            //Get start of week
            DateTime start = m_scheduleService.AccountingHelper.GetStartOfWeekForDate(date);

            if (start.Date == WeekStart)
                return;
           
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        TrySave();

                    WeekStart = start.Date;

                    DoRefresh();
                }
            });
        }

        private void UpdateDate()
        {
            int weekNumber = m_scheduleService.AccountingHelper.GetWeekNumberForDate (WeekStart);
            Date = m_weekNumberDisplayed ?
                string.Format (Localiser.Get("week_starting_label") , weekNumber, WeekStart.ToString (Localiser.Get("dd/MM/yyyy"))) :
                string.Format (Localiser.Get("starting_label"),  WeekStart.ToString (Localiser.Get("dd/MM/yyyy"))) ;

            UpdateHeaders ();
        }

        public void Refresh()
        {
            if (Loading)
                return;

            if (EditMode)
            {
                m_alertBox.ShowYesNo
                (
                    Localiser.Get("discard_changes_message"),
                    Localiser.Get("allocations_confirm_discard_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    DoRefresh();
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                DoRefresh();
            }
        }

        private async void DoRefresh()
        {
            LoadingMessage = Localiser.Get("allocations_loading_message");
            Loading = true;

            UpdateDate ();

            m_aisles = null;

            if (m_tokenSource != null)
                m_tokenSource.Cancel ();

            m_tokenSource = new CancellationTokenSource ();
            var token = m_tokenSource.Token;

            //Load Schedule
            var schedule = m_scheduleService.GetScheduleDataForPeriod(WeekStart, WeekStart.AddDays(8), token);

            // Load aisle data here.
            var aisles = m_nightAisleService.GetAisleConfiguration(m_sessionData.Site.SiteID, token);

            // Load aisle mappings here
            var mappings = m_nightAisleService.GetMappingsForWeekStarting(m_sessionData.Site.SiteID, WeekStart, token);

            // Load Replenishment times here
            var replenishment = m_nightAisleService.GetReplenishmentStartAndEndTime(m_sessionData.Site.SiteID, token);

            await Task.Delay (500);//Min delay
            await schedule;
            await aisles;
            await mappings;
            await replenishment;

            if (token.IsCancellationRequested)
                return;

            Aisles = aisles.Result.Object.OrderBy (x => x.Priority).ToList ();
            Mappings = mappings.Result.Object.ToList ();
            ScheduleData = schedule.Result.Object;

            m_startTime = replenishment.Result.Object.Item1;
            m_duration = replenishment.Result.Object.Item2;

            BuildData ();

            EditMode = false;
            Loading = false;
            IsEdited = false;
        }

        private void BuildData()
        {
            //Take Aisles, Take Mappings and merge for table controls
            List<NightAisleModel> data = new List<NightAisleModel>();

            if (Aisles != null)
            {
                foreach (var aisle in Aisles)
                {
                    //Get Aisle TODO Error on missing aisle

                    if (aisle != null)
                    {
                        var defaultMapping = new AisleMapping (){ AisleID = aisle.ID, EmployeeDayMappings = new Dictionary<DayOfWeek, string[]> (), EmployeeIdDayMappings = new Dictionary<DayOfWeek, int[]> () };
                        var mapping = Mappings.DefaultIfEmpty(defaultMapping).FirstOrDefault(x => x != null && x.AisleID == aisle.ID);
                        NightAisleModel model = new NightAisleModel ();
                        model.AisleID = aisle.ID;
                        model.AisleName = aisle.Name;

                        //Build days
                        DateTime start = m_scheduleService.AccountingHelper.GetStartOfWeekForDate(WeekStart);
                        model.Days = new List<NightAisleDayModel> ();
                        for (int i = 0; i < 7; i++)
                        {
                            NightAisleDayModel day = new NightAisleDayModel ();
                            day.Day = start.AddDays (i).DayOfWeek;
                            int[] employeeIds = new int[0];
                            if (mapping != null && mapping.EmployeeIdDayMappings != null && mapping.EmployeeIdDayMappings.ContainsKey (day.Day))
                            {
                                employeeIds = mapping.EmployeeIdDayMappings[day.Day];
                            }
                            day.EmployeeIds = employeeIds;

                            string[] employees = new string[0];
                            if (mapping != null && mapping.EmployeeDayMappings != null && mapping.EmployeeDayMappings.ContainsKey (day.Day))
                            {
                                employees = mapping.EmployeeDayMappings[day.Day];
                            }
                            // Strip employees that aren't working anymore
                            List<EmployeePickerData> validEmployeeList = GenerateEmployeePickerData (day);
                            day.Employees = employees.Where (x => DoesListContainEmployee(validEmployeeList, x)).ToArray();;

                            model.Days.Add(day);
                        }

                        data.Add (model);
                    }
                }
            }

            Data = data;
        }

        private void UpdateHeaders()
        {
            var headers = new List<ColumnHeader> ();

            for (int i = 0; i < 7; ++i)
            {
                var header = new ColumnHeader ()
                {
                    Index = i,
                    Title = WeekStart.AddDays (i).ToString (Localiser.Get("ddd dd/MM")),
                };

                headers.Add (header);
            }

            Headers = headers;
        }

        public void OnEdit()
        {
            SwitchEditMode();
            UpdateEditableDays ();
        }
            
        private void SwitchEditMode()
        {
            if (EditMode)
            {
                m_alertBox.ShowOptions
                (
                    Localiser.Get("allocations_save_message"),
                    Localiser.Get("confirm_save_message"),
                    new List<string>() { Localiser.Get("save_changes_message"),  Localiser.Get("discard_changes_message") },  
                    Localiser.Get("continue_editing_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Save
                                {
                                    TrySave();
                                    break;
                                }
                            case 2: // Revert
                                {
                                    DoRefresh();
                                    EditMode = false;
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                EditMode = true;
            }
        }

        private void UpdateEditableDays()
        {
            foreach (var data in Data)
            {
                foreach (var day in data.Days)
                {
                    day.IsEditable = EditMode;
                }
            }
        }

        private void TrySave()
        {
            if (!IsValid)
            {
                m_alertBox.ShowOK(Localiser.Get("validation_errors_message"), Localiser.Get("night_aisle_planner_aisles_cannot_be_saved_message"));
            }
            else
            {
                DoSave(() => { EditMode = false; });
            }
        }

        private async void DoSave(Action completed)
        {
            Loading = true;
            LoadingMessage = Localiser.Get("saving_ellipsis");

            // Save the Aisles here
            var save = m_nightAisleService.SetMappingsForWeekStarting (m_sessionData.Site.SiteID, WeekStart, Data);
            await Task.Delay (200);//Min delay
            await save;

            bool needsRefresh = save.Result.Success;

            if (needsRefresh)
            {
                completed.Invoke ();
                DoRefresh ();
            } 
            else 
            {
                Loading = false;
                LoadingMessage = string.Empty;
            }
        }

        public void OnReturn(List<EmployeePickerData> employeeList)
        {
            if (employeeList == null)
                return;
            
            // Update employee list for the selected cell here
            List<int> pickedEmployeeIds = new List<int>();
            List<string> pickedEmployees = new List<string> ();
            foreach (var e in employeeList)
            {
                if (e.Picked)
                {
                    pickedEmployees.Add (e.EmployeeName);
                    pickedEmployeeIds.Add (e.EmployeeId);
                }
            }

            m_pickedAisleDay.EmployeeIds = pickedEmployeeIds.ToArray ();
            m_pickedAisleDay.Employees = pickedEmployees.ToArray ();

            IsEdited = true;

            RaisePropertyChanged (() => Data);
        }

        private void CheckModified(Action<int> saveCallback)
        {
            if (IsEdited)
            {
                m_alertBox.ShowOptions (Localiser.Get("allocation_modified"), Localiser.Get("schedule_changes_message"), 
                    new List<string> () { Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                    Localiser.Get("continue_editing_message"), saveCallback );
            }
            else
            {
                saveCallback(2);
            }
        }

        public void OnAislePicked(NightAisleDayModel aisleDay)
        {
            //if (aisleDay.IsEditable == false)
                //return;

            m_pickedAisleDay = aisleDay;
            m_employeeList = GenerateEmployeePickerData (aisleDay);

            string subtitle = string.Empty;
            var aisle = Data.First (x => x.Days.Contains (aisleDay));
            if (aisle != null)
            {
                subtitle += aisle.AisleName;
                subtitle += ": ";
            }
            int dayOffset = (aisleDay.Day - WeekStart.DayOfWeek);
            if (dayOffset < 0)
            {
                dayOffset += 7;
            }
            DateTime date = WeekStart.AddDays (dayOffset);
            DateTime startTime = date.AddHours (m_startTime);
            subtitle += startTime.ToString (Localiser.Get("dd/MM"));
            EditMode = true;

            ShowViewModel<EmployeePickerViewModel> ( new { employeeList = m_employeeList, subtitle = subtitle} );
        }

        private List<EmployeePickerData> GenerateEmployeePickerData(NightAisleDayModel aisleDay)
        {
            List<EmployeePickerData> employees = new List<EmployeePickerData> ();

            if (aisleDay == null || ScheduleData == null)
                return employees;

            //This is a bit cack, maybe optimise when confirmed to work
            int dayOffset = (aisleDay.Day - WeekStart.DayOfWeek);
            if (dayOffset < 0)
            {
                dayOffset += 7;
            }
            DateTime date = WeekStart.AddDays (dayOffset);
            DateTime nightShiftStart = date.Date.AddHours(m_startTime);
            DateTime nightShiftEnd = nightShiftStart.AddHours(m_duration);

            foreach (var schedule in ScheduleData)
            {
                foreach (var e in schedule.Employees)
                {
                    // Get all the shifts that are valid on the start and end dates.
                    var shifts = e.GetShiftsOnDay (nightShiftStart.Date);
                    foreach (var shift in shifts)
                    {
                        if(IsShiftInJobFilter(shift) == false)
                        {
                            continue;
                        }
                        if (shift.Start >= nightShiftStart)
                        {
                            employees.Add (new EmployeePickerData (e, false));
                            // break to stop duplicate employees.
                            break;
                        }
                    }
                    
                    if (nightShiftStart.Date != nightShiftEnd.Date)
                    {
                        // get shifts for the next day
                        var nextDayShifts = e.GetShiftsOnDay (nightShiftEnd.Date);
                        foreach (var shift in nextDayShifts)
                        {
                            // Add any employees taht start before the night shift ends.
                            if (shift.Start.Date == nightShiftEnd.Date && shift.Start <= nightShiftEnd)
                            {
                                employees.Add (new EmployeePickerData (e, false));
                                // break to stop duplicate employees.
                                break;
                            }
                        }
                    }
                }
            }

            if (aisleDay.EmployeeIds != null)
            {
                List<int> selectedEmployees = aisleDay.EmployeeIds.ToList ();
                foreach (var e in employees)
                {
                    e.Picked = (selectedEmployees.Contains (e.EmployeeId));
                }
            }

            if (aisleDay.Employees != null)
            {
                foreach (var aisle in Data)
                {
                    if( aisle.Days[dayOffset] != aisleDay)
                    {
                        foreach (var e in employees)
                        {
                            if (aisle.Days[dayOffset].EmployeeIds.Contains (e.EmployeeId))
                            {
                                e.AlreadyAssigned = true;
                            }
                        }
                    }
                }
            }

            return employees;
        }

        private bool DoesListContainEmployee(List<EmployeePickerData> validEmployeeList, string employeeName)
        {
            foreach(var validEmployee in validEmployeeList)
            {
                if(validEmployee.EmployeeName == employeeName)
                    return true; 
            }

            return false;
        }

        private bool IsShiftInJobFilter(ShiftData shift)
        {
            if(m_jobFilter == null || m_jobFilter.Any() == false)
                return true; // Allow all jobs if the filter is empty.
            
            foreach(var job in shift.Jobs)
            {
                if(m_jobFilter.Contains(job.JobName.ToLower()))
                    return true;
            }
            return false;
        }

        private List<string> GetJobFilter()
        {
            var jobFilter = new List<string>();

            if(!string.IsNullOrEmpty(m_essConfig.NightShiftDepartments))
            {
                try 
                {
                    jobFilter = m_essConfig.NightShiftDepartments.Split(';').ToList();
                } 
                catch
                {
                    
                }
            }

            var jobFilterLwr = new List<string>();
            foreach(var job in jobFilter)
            {
                jobFilterLwr.Add(job.ToLower());
            }

            return jobFilterLwr;
        }
    }
}

