﻿using System;
using System.Collections;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using Newtonsoft.Json;

namespace WFM.Core
{
    public interface IShiftHandler
    {
        void OnShiftItemJob(ShiftItem shift);
        void OnShiftItemTime(ShiftItem shift);
        void OnShiftItemDelete(ShiftItem shift);
    }

    public class ShiftSection : ISection
    {
        public string Title { get; set; }
        public IList Items { get; set; }
    }

    public class ShiftItem
    {
        public int ScheduledJobID { get; set; }
        public int DetailID { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }

        [JsonIgnore]
        public IShiftHandler Handler { get; set; }

        public void OnJobEdit() { Handler.OnShiftItemJob (this); }
        public void OnTimeEdit() { Handler.OnShiftItemTime (this); }
        public void OnDelete() { Handler.OnShiftItemDelete (this); }

        public string Time
        { 
            get 
            { 
                var timeFormat = Mvx.Resolve<ITimeFormatService>();
                
                return string.Format ("{0} - {1}", timeFormat.FormatTime (Start), timeFormat.FormatTime (End)); 
            }
        }
    }
}