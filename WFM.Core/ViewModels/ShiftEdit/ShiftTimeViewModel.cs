﻿using System;
using System.Linq;
using Consortium.Client.Core;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ShiftTimeViewModel : ConsortiumViewModel
    {
        private ShiftItem m_item;
		private DateTime m_shiftStart;
		private DateTime m_shiftEnd;

        private DateTime m_start;
        public DateTime Start
        { 
            get { return m_start; }
            set { m_start = value; RaisePropertyChanged(() => Start); }
        }

        private DateTime m_end;
        public DateTime End
        { 
            get { return m_end; }
            set { m_end = value; RaisePropertyChanged(() => End); }
        }

		public DateTime ShiftStartTime 
		{
			get{ return m_shiftStart; }
		}

		public DateTime ShiftEndTime 
		{
			get{ return m_shiftEnd; }
		}

        /// <summary>
        /// Gets a value indicating whether we are modifying the length of the shift(true). Or the length of a detail within a shift (false).
        /// </summary>
        /// <value><c>true</c> if modifying shift; otherwise, <c>false</c>.</value>
        public bool ModifyingShift
        {
            get { return (this.m_item != null) ? this.m_item.DetailID == 0 : true; }
        }

        public ShiftTimeViewModel()
        {
        }

        public void Init(ShiftItem item, DateTime ShiftStart, DateTime ShiftEnd)
        {
            m_item = item;
            Start = item.Start;
            End = item.End;
			m_shiftStart = ShiftStart;
			m_shiftEnd = ShiftEnd;
        }

        public void Init(DateTime ShiftStart, DateTime ShiftEnd)
        {
            m_item = null;
            Start = ShiftStart;
            End = ShiftEnd;
            m_shiftStart = ShiftStart;
            m_shiftEnd = ShiftEnd;
        }

        public void OnApply(DateTime start, DateTime end)
        {
            Close (ViewCloseType.CurrentView, new { item = m_item, start = DateTime.SpecifyKind(start, DateTimeKind.Unspecified), end = DateTime.SpecifyKind(end, DateTimeKind.Unspecified) });
        }

        // Legacy for iOS build
        public void OnApply(DateTime start, TimeSpan duration)
        {
            Close (ViewCloseType.CurrentView, new { item = m_item, start = DateTime.SpecifyKind(start, DateTimeKind.Unspecified), end = DateTime.SpecifyKind(start.Add(duration), DateTimeKind.Unspecified) });
        }

        public void OnCancel()
        {
            Close (ViewCloseType.CurrentView);
        }
    }
}

