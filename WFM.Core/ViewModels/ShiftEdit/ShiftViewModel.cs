﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Consortium.Client.Core;
using Consortium.Client.Core.Extensions;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.CrossCore.UI;

namespace WFM.Core
{
    //TODO: Localise this whole class
    public class ShiftViewModel : WFMViewModel, IShiftHandler
    {
        private const double c_defaultJobDuration = 1.0;

        private ScheduleService m_service;
        private SpareShiftService m_spareShiftService;
        private readonly IMvxMessenger m_messenger;
        private readonly IAlertBox m_alert;
        private readonly SessionData m_sessionData;
        private readonly IToastService m_toastService;

        private EmployeeData m_employee;
        private int? m_dayIndex;
        private ShiftData m_mainShift; // < Main shift, make a copy to edit

        private bool IsClosing { get; set; }

        private ShiftData m_shift;
        public ShiftData Shift
        {
            get { return m_shift; }
            set { m_shift = value; RaisePropertyChanged(() => Shift); }
        }

        private string m_fullName;
        public string FullName
        { 
            get { return m_fullName; }
            set { m_fullName = value; RaisePropertyChanged(() => FullName); }
        }

        private string m_shiftDescription;
        public string ShiftDescription
        { 
            get { return m_shiftDescription; }
            set { m_shiftDescription = value; RaisePropertyChanged(() => ShiftDescription); }
        }

        private bool m_editable;
        public bool Editable
        { 
            get { return m_editable; }
            set { m_editable = value; RaisePropertyChanged(() => Editable); }
        }

        private bool m_isShowing;
        public bool IsShowing
        { 
            get { return m_isShowing; }
            set { m_isShowing = value; RaisePropertyChanged(() => IsShowing); }
        }

        private IList<ShiftSection> m_shiftItems;
        public IList<ShiftSection> ShiftItems
        { 
            get { return m_shiftItems; }
            set { m_shiftItems = value; RaisePropertyChanged(() => ShiftItems); }
        }

        private bool m_ShowCallOff;

        public bool ShowCallOff
        {
            get { return m_ShowCallOff; }
            set { m_ShowCallOff = value; RaisePropertyChanged (() => ShowCallOff); }
        }

        public bool AllowDelete
        {
            get { return ConfigurationService.CanDeleteShifts; }
            set { }
        }
        
        private bool m_hasMatchingFixedShift = false;
        private bool HasMatchingFixedShift 
        {
            get
            {
                return m_hasMatchingFixedShift;
            }
            set
            {
                m_hasMatchingFixedShift =value;
                RaisePropertyChanged(() => HasMatchingFixedShift);
                RaisePropertyChanged(() => CanCallOffPaidShift);
                RaisePropertyChanged(() => CanCallOffUnpaidShift); 
            }
        }

        public bool CanCallOffPaidShift
        {
            get
            {
                return !ConfigurationService.CallOffAppliesForFixedShiftsOnly || HasMatchingFixedShift;
            }
            set
            {
                RaisePropertyChanged(() => HasMatchingFixedShift);
            }
        }
        
        public bool CanCallOffUnpaidShift
        {
            get
            {
                return !ConfigurationService.CallOffUnpaidAppliesForFixedShiftsOnly || HasMatchingFixedShift;
            }
            set
            {
                RaisePropertyChanged(() => HasMatchingFixedShift);
            }
        }

        public bool ShowShiftToolbar 
        {
            get 
            {
                if (ConfigurationService.CanEditSchedule)
                    return ConfigurationService.ShowUnfillOptions && Editable;
                else
                    return ConfigurationService.ShowUnfillOptions;
            }
            set 
            {
                RaisePropertyChanged (() => ShowShiftToolbar);
            }
        }

        public ShiftViewModel (IAlertBox alert, IMvxMessenger messenger, ScheduleService service, SessionData sessionData, SpareShiftService spareShiftService, IToastService toastService)
        {
            m_alert = alert;
            m_messenger = messenger;
            m_service = service;
            m_sessionData = sessionData;
            m_spareShiftService = spareShiftService;
            m_toastService = toastService;
            IsShowing = false;

        }

        public void Init(int employeeId, int shiftId)
        {
            Analytics.TrackAction ("View Shift","Show");

            if (m_service.Schedule == null)
            {
                m_alert.ShowOK (Localiser.Get("shift_view_error_title"), string.Format (Localiser.Get("shift_view_error_not_found"), shiftId), x => {OnCancel();});
                return;
            }

            m_employee = m_service.Schedule.GetEmployee(employeeId);
            if (m_employee == null)
            {
                m_employee = m_spareShiftService.GetEmployee (employeeId);
            }
            m_dayIndex = m_service.DayIndex;

            if (m_employee == null)
            {
                if (m_service.Schedule == null)
                {
                    m_alert.ShowOK (Localiser.Get("shift_view_error_title"), string.Format (Localiser.Get("shift_view_error_employee_not_found"), shiftId), x => {OnCancel();});
                    return;
                }
            }

            m_mainShift = m_employee.GetShift(shiftId);

            if (m_mainShift == null)
            {
                m_alert.ShowOK (Localiser.Get("shift_view_error_title"), string.Format (Localiser.Get("shift_view_error_not_found"), shiftId), x => {OnCancel();});
                return;
            }

            Shift = m_mainShift.Copy (); 

            //Tesco requirement: Call Off only available for Fixed shifts with Shift matching start and end time
            UpdateCallOffAvailability();
                   
            Editable = m_mainShift.CanEdit() && ConfigurationService.CanEditSchedule && !ConfigurationService.IsScheduleEditorReadOnlyForUserRole;

            UpdateShift ();

            ShowCallOff = CanCallOffShift();

            IsShowing = true;
        }

        private void UpdateCallOffAvailability ()
        {
            if(m_employee.DayIndex.HasValue)
            {
                var fixedShifts = m_employee.Days[m_employee.DayIndex.Value].FixedShifts;
                HasMatchingFixedShift = fixedShifts.Any(a => a.Start >= Shift.Start && a.End <= Shift.End);
            }
            else
            {
                var dayIndex = (int)Shift.Start.DayOfWeek - 1 >= 0 ? (int)Shift.Start.DayOfWeek - 1 : (int)Shift.Start.DayOfWeek + 6;
                var fixedShifts = m_employee.Days[dayIndex].FixedShifts;
                HasMatchingFixedShift = fixedShifts.Any(a => a.Start >= Shift.Start && a.End <= Shift.End);
            }
        }
        
        private bool CanCallOffShift()
        {
            var shiftDate = this.m_shift.Start.Date;
            var currentDate = DateTime.Now.Date;
            
            if(shiftDate < currentDate)
            {
                return ConfigurationService.ShowCallOffPast;
            }
            else if(shiftDate == currentDate)
            {
                return ConfigurationService.ShowCallOffCurrentDay;
            }
            else //shiftDate > currentDate
            {
                return ConfigurationService.ShowCallOffFuture;
            }
        }

        private void UpdateShift()
        {
            // Employee Name
            var firstName = (string.IsNullOrEmpty(m_employee.Info.NickName) ? m_employee.Info.FirstName : m_employee.Info.NickName);
            FullName = string.Format("{0}, {1}", m_employee.Info.LastName, firstName);

            // Shift description
            var dateAsString = Shift.Start.ToString(Localiser.Get("dd/MM/yyyy"));
            // Remove any breaks we aren't paid for
            var shiftLength = TimeSpan.Zero;
            foreach (var job in Shift.Jobs)
            {
                JDAScheduleRule rule = m_employee.FindMatchingScheduleRule(m_service.Schedule.ScheduleRules);
                bool mealPaidBU = m_service.Schedule.BUSettings.IsMealPaid;
                bool breakPaidBU = m_service.Schedule.BUSettings.IsBreakPaid;
                int mealMinutesPaid = 0;
                int breakMinutesPaid = 0;

                if(rule != null)
                {
                    var allocation = Shift.FindBestMatchAllocationFromRule(rule);
                    if(allocation != null)
                    {
                        mealMinutesPaid = allocation.MealDurationPaid;
                        breakMinutesPaid = allocation.BreakDurationPaid;
                    }
                    else
                    {
                        rule = null;
                    }
                }


                var duration = m_employee.CalculateJobDuration (Shift, job, rule != null, breakMinutesPaid, mealMinutesPaid, breakPaidBU, mealPaidBU);
                shiftLength += duration;
            }

            ShiftDescription = string.Format(@"{0} - {1}:{2:00} Hours", 
                dateAsString, (int)shiftLength.TotalHours, shiftLength.Minutes);

            List<ShiftSection> items;

            if (ConfigurationService.ShowScheduleDetails)
            {
                items = new List<ShiftSection> () {
                    new ShiftSection() { Title = "Jobs", Items = new List<ShiftItem>() },
                    new ShiftSection() { Title = "Breaks & Meals", Items = new List<ShiftItem>() }
                };
            }
            else
            {
                items = new List<ShiftSection> () {
                    new ShiftSection() { Title = "Jobs", Items = new List<ShiftItem>() }
                };
            }

            // Add Jobs

            bool firstJob = true;

            foreach (var j in Shift.Jobs)
            {
                var itemVm = new ShiftItem () {
                    ScheduledJobID = j.ScheduledJobID,
                    Title = j.JobName,
                    Start = j.Start,
                    End = j.End,
                    Handler = this,
                    CanEdit = Editable,
                    CanDelete = Editable && !firstJob
                };

                items [0].Items.Add (itemVm);

                firstJob = false;
            }

            // Add Breaks & Meals
            if (ConfigurationService.ShowScheduleDetails)
            {
                foreach (var d in Shift.Details)
                {
                    if(!d.IsRole)
                    {
                        var itemVm = new ShiftItem ()
                        {
                            ScheduledJobID = d.ScheduledJobID,
                            DetailID = d.ShiftDetailID,
                            Title = d.IsBreak ? "Break" : "Meal",
                            Start = d.Start.Value,
                            End = d.End.Value,
                            Handler = this,
                            CanEdit = Editable, 
                            CanDelete = Editable
                        };

                        items[1].Items.Add (itemVm);
                    }
                }
            }

            ShiftItems = items;
            RaisePropertyChanged (() => Shift);
        }

        public void OnReturn(ShiftItem item, JDANameAndID selected)
        {
            if (item.DetailID != 0)
            {
                string punchCode = selected.ID == 0 ? ShiftDetailData.c_breakID : ShiftDetailData.c_mealID;
                var detail = Shift.GetDetail (item.DetailID);

                // Will match mean
                if (detail.PunchCode != punchCode)
                {
                    var mod = ShiftDetailData.Create (ShiftData.GenerateUniqueID());
                    mod.Start = detail.Start;
                    mod.End = detail.End;
                    mod.PunchCode = punchCode;

                    Shift.DeleteDetail (item.DetailID);
                    Shift.AddDetail (mod);
                }
            }
            else
            {
                var data = m_employee.AllowableJobs.Where (x => x.JobID == selected.ID).FirstOrDefault ();
                var job = Shift.GetJob (item.ScheduledJobID);

                job.Edit = Shift.Edit = EditState.Modified;
                job.JobID = data.JobID;
                job.JobColor = data.JobColor;
                job.JobName = data.JobName;
                job.PunchCode = data.PunchCode;
            }

            UpdateShift ();
        }

        public void OnReturn(ShiftItem item, DateTime start, DateTime end)
        {
            var originalShiftLength = (Shift.End - Shift.Start);
            var originalItemLength = (item.End - item.Start);
            var newItemLength = (end - start);
            var newShiftLength = ((originalShiftLength - originalItemLength) + newItemLength).TotalHours;
            if(newShiftLength >= ConfigurationService.MaxShiftLengthHours)
            {
                // Shift Length will be over the maximum;
                m_alert.ShowOK(Localiser.Get("shift_view_error_max_shift_header"), string.Format(Localiser.Get("shift_view_error_shift_length_body"), ConfigurationService.MaxShiftLengthHours));
            }
            else
            {
                if (item.DetailID != 0)
                {
                    var detail = Shift.GetDetail (item.DetailID);
                    detail.Resize (start, end);
                }
                else
                {
                    var job = Shift.GetJob (item.ScheduledJobID);

                    //We can't make a shift shorter if its the primary shift. 
                    if (start > Shift.Start || item.ScheduledJobID == Shift.ShiftID)
                    {
                        job.Resize (start, end);
                    }
                }

                Shift.UpdateAndValidate ();
                UpdateCallOffAvailability();
            }
            UpdateShift ();
        }

        public void OnShiftItemJob(ShiftItem item)
        {
            if (item.DetailID != 0)
            {
                var typeList = new JDANameAndID[] {
                    new JDANameAndID() { Name = "Break", ID = 0 },
                    new JDANameAndID() { Name = "Meal", ID = 1 }
                };

                ShowViewModel<ShiftTypeViewModel> (new { item = item, types = typeList });
            }
            else
            {
                var typeList = m_employee.AllowableJobs.Select (x => new JDANameAndID () { Name = x.JobName, ID = x.JobID }).ToArray ();
                ShowViewModel<ShiftTypeViewModel> (new { item = item, types = typeList });
            }
        }

		public void OnShiftItemTime(ShiftItem item)
        {
			ShowViewModel<ShiftTimeViewModel> (new{item = item, ShiftStart = Shift.Start, ShiftEnd = Shift.End});
        }

        public void OnShiftItemDelete(ShiftItem item)
        {
            Analytics.TrackAction ("View Shift","Delete Detail");

            if (item.DetailID != 0)
            {
                m_alert.ShowYesNo("Delete Break Or Meal", "Are you sure?", (option) => {
                    if (option == 1)
                    {
                        Shift.DeleteDetail (item.DetailID);
                        UpdateShift ();
                    }
                });
            }
            else
            {
                // Handle last shift deletion
                if (Shift.Jobs.Count == 1)
                {
                    DeleteShift(false);
                }
                else
                {
                    Shift.DeleteJob (item.ScheduledJobID);
                }
            }

            UpdateShift ();
        }
        
        public void OnAutoCallOffPaidShift()
        {
            if(CanCallOffPaidShift)
            {
                AutoCallOffShift(true);
            }
            else
            {
                m_toastService.Show(Localiser.Get("calloff_disabled_message"));
            }
        }
        
        public void OnAutoCallOffUnpaidShift()
        {
            if(CanCallOffUnpaidShift)
            {
                AutoCallOffShift(false);
            }
            else
            {
                m_toastService.Show(Localiser.Get("calloff_disabled_message"));
            }
        }

        public void OnCallOffShift()
        {
            CallOffShift();
        }
        
        private void AutoCallOffShift(bool paid)
        {
            Analytics.TrackAction ("View Shift","Call Off");
            
            if(m_service.State != ScheduleService.UpdateState.View)
            {   
                if(paid)
                    m_alert.ShowOK(Localiser.Get ("calloff_denypendingchanges_title"), Localiser.Get ("calloff_denypendingchanges_body"));
                else
                    m_alert.ShowOK(Localiser.Get("calloff_denypendingunpaidchanges_title"),Localiser.Get("calloff_denypendingunpaidchanges_body"));
            }
            else
            {
                string callOffTimeOffType;
                
                if(paid)
                {
                    callOffTimeOffType = ConfigurationService.CallOffShiftTimeOffType;
                }
                else
                {
                    callOffTimeOffType = ConfigurationService.CallOffShiftUnpaidTimeOffType;
                }
            
                //Check user
                bool canApproveOwnTimeOff = ConfigurationService.CanApproveOwnTimeOff;
                if (!canApproveOwnTimeOff &&
                    m_sessionData.CurrentUsername.Equals (m_employee.Info.LoginName, StringComparison.CurrentCultureIgnoreCase))
                {
                    m_alert.ShowOK (Localiser.Get ("calloff_denysameuser_title"), Localiser.Get ("calloff_denysameuser_body"));
                    
                }
                else
                {
                    CloseWindow(new {
                        employeeId = m_shift.EmployeeID.GetValueOrDefault (-1),
                        startTime = m_shift.Start,
                        endTime = m_shift.End,
                        autoBookTimeOff = true,
                        timeOffType = callOffTimeOffType,
                        paidAbsence = paid
                    });
                }
            }
        }

        private void CallOffShift()
        {
            Analytics.TrackAction ("View Shift","Call Off");
            
            if(m_service.State != ScheduleService.UpdateState.View)
            {
                m_alert.ShowOK(Localiser.Get ("calloff_denypendingchanges_title"), Localiser.Get ("calloff_denypendingchanges_body"));
            }
            else
            {
                //Check user
                bool canApproveOwnTimeOff = ConfigurationService.CanApproveOwnTimeOff;
                if (!canApproveOwnTimeOff &&
                    m_sessionData.CurrentUsername.Equals (m_employee.Info.LoginName, StringComparison.CurrentCultureIgnoreCase))
                {
                    m_alert.ShowOK (Localiser.Get ("calloff_denysameuser_title"), Localiser.Get ("calloff_denysameuser_body"));
                    
                }
                else
                {
                    CloseWindow(new {
                        employeeId = m_shift.EmployeeID.GetValueOrDefault (-1),
                        startTime = m_shift.Start,
                        endTime = m_shift.End,
                        autoBookTimeOff = false,
                        timeOffType = string.Empty,
                        paidAbsence = false
                    });
                }
            }
        }

        public void OnDeleteShift()
        {
            DeleteShift(true);
        }

        public void OnQueueDeleteShiftOperation()
        {
            DeleteShift(false);
        }

        private void DeleteShift(bool actionImmediately)
        {
            m_alert.ShowYesNo("Delete Shift", "Are you sure?", (option) => {
                if (option == 1)
                {
                    //Message flows into the schedule service?
                    ShiftEditMessage deleteShift = new ShiftEditMessage (this) {
                        Op = m_shift.Edit == EditState.Added ? ShiftEditMessage.Operation.DeleteUnsavedShift : ShiftEditMessage.Operation.DeleteShift,
                        ShiftID = m_shift.ShiftID,
                        EmployeeID = m_shift.EmployeeID.HasValue ? m_shift.EmployeeID.Value : 0,
                        Start = m_shift.Start,
                        End = m_shift.End,
                        StoreInUndoStack = true,
                        AllowAutoSubmission = actionImmediately,
                    };

                    m_messenger.Publish<ShiftEditMessage>(deleteShift);

                    CloseWindow();
                }
            });
        }

        public void OnUnfillShift()
        {
            UnfillShift();
        }

        private void UnfillShift()
        {
            m_alert.ShowYesNo("Unfill Shift", "Are you sure?", (option) => {
                if (option == 1)
                {
                    Analytics.TrackAction ("View Shift","Unfill");

                    //Message flows into the schedule service?
                    ShiftEditMessage unfillShift = new ShiftEditMessage (this) {
                        Op = ShiftEditMessage.Operation.UnfillFilledShift,
                        ShiftID = m_shift.ShiftID,
                        EmployeeID = m_shift.EmployeeID.HasValue ? m_shift.EmployeeID.Value : 0,
                        StoreInUndoStack = true,
                        AllowAutoSubmission = true,
                    };

                    m_messenger.Publish<ShiftEditMessage>(unfillShift);

                    CloseWindow();
                }
            });
        }

        public void OnAddRole()
        {
            Analytics.TrackAction ("View Shift","Add Role");

            Shift.AddDefaultRole();

            UpdateShift();
        }
        

        public void OnAddJob()
        {
            Analytics.TrackAction ("View Shift","Add Job");

            var shiftLength = (Shift.End - Shift.Start).TotalHours;
            if((shiftLength + c_defaultJobDuration) >= (double)ConfigurationService.MaxShiftLengthHours)
            {
                // Shift Length will be over the maximum;
                m_alert.ShowOK(Localiser.Get("shift_view_error_max_shift_header"), string.Format(Localiser.Get("shift_view_add_job_max_shift_body"), ConfigurationService.MaxShiftLengthHours));
            }
            else
            {
                Shift.AddDefaultJob (c_defaultJobDuration);
            }

            UpdateShift ();
        }

        public void OnAddMeal()
        {
            Analytics.TrackAction ("View Shift","Add Meal");

            var detail = Shift.AddDefaultDetail ();
            detail.SetMeal ();
            UpdateShift ();
        }

        public void OnAddBreak()
        {
            Analytics.TrackAction ("View Shift","Add Break");

            var detail = Shift.AddDefaultDetail (isBreak: true);
            detail.SetBreak ();
            UpdateShift ();
        }

        public void OnCancel()
        {
            IsShowing = false;
            CloseWindow();
        }

        public void OnSave()
        {
            if(!Shift.UpdateAndValidate())
            {
                UpdateShift();
                return;
            }

            CloseWindow();

            // Save the shift
            var request = new ShiftEditMessage (this) {
                Op = ShiftEditMessage.Operation.ReplaceShift,
                PreviousShiftState = m_mainShift,
                UpdateShift = Shift,
                ShiftID = m_shift.ShiftID,
                EmployeeID = m_shift.EmployeeID.HasValue ? m_shift.EmployeeID.Value : 0,
                RecalculateDetails = false,
                StoreInUndoStack = true
            };

            m_messenger.Publish<ShiftEditMessage>(request);
        }

        private void CloseWindow()
        {
            IsClosing = true;
            InvokeOnMainThread(
                new Action(() => Close())
            );
        }

        private void CloseWindow(object returnParams)
        {
            IsClosing = true;
            InvokeOnMainThread (
                new Action (() => Close (returnParams))
            );
        }

        public override void OnViewClosed ()
        {
            if(!IsClosing)
                Close ();
            
            base.OnViewClosed ();
        }
    }
}