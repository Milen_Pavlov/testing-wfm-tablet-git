﻿using System;
using System.Linq;
using Consortium.Client.Core;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ShiftTypeViewModel : ConsortiumViewModel
    {
        private ShiftItem m_item;

        private JDANameAndID[] m_types;
        public JDANameAndID[] Types
        { 
            get { return m_types; }
            set { m_types = value; RaisePropertyChanged(() => Types); }
        }

        public ShiftTypeViewModel()
        {
        }

        public void Init(ShiftItem item, JDANameAndID[] types)
        {
            m_item = item;
            Types = types;
        }

        public void OnSelected(JDANameAndID selected)
        {
            Close (ViewCloseType.CurrentView, new { item = m_item, selected = selected });
        }

        public void OnCancel()
        {
            Close (ViewCloseType.CurrentView);
        }
    }
}
