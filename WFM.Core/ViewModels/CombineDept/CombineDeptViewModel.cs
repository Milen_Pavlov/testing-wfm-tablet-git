﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using WFM.Core.MyHours;
using System.Diagnostics;
using WFM.Shared.Constants;

namespace WFM.Core
{
    public enum CombinedRegion
    {
        Unknown,
        Uncombined,
        Lead,
        Combined,
    }

    public class ChangeCommit
    {
        public LabourDemandDepartment Data { get; set; }
        public string Method { get; set; } 
    }

    public class CombineDeptViewModel : WFMViewModel
    {
        private readonly IMvxMessenger m_messenger;
        private readonly SettingsService m_settings;
        private readonly IAlertBox m_alertService;
        private readonly IToastService m_toastService;
        private readonly CombinedDeptService m_combinedDeptService;
        private readonly SessionData m_sessionData;
        private readonly IESSConfigurationService m_essConfig;

        private MvxSubscriptionToken m_signOutToken;
        private MvxSubscriptionToken m_orgEndToken;

        public override string AnalyticsScreenName
        {
            get
            {
                return "CombineDept";
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private bool m_noData;
        public bool NoData
        {
            get { return  m_noData; }
            set { m_noData = value; RaisePropertyChanged (() => NoData); }
        }

        private string m_errorMessage;
        public string ErrorMessage
        {
            get { return m_errorMessage; }
            set { m_errorMessage = value; RaisePropertyChanged(() => ErrorMessage); }
        }

        private StoreCombinedDeptSetup m_data;
        public StoreCombinedDeptSetup Data
        {
            get { return m_data; }
            set { m_data = value; RaisePropertyChanged (() => Data); }
        }

        private List<LabourDemandDepartment> m_uncombined;
        public List<LabourDemandDepartment> Uncombined
        {
            get { return m_uncombined; }
            set { m_uncombined = value; RaisePropertyChanged (() => Uncombined); RaisePropertyChanged (() => EmptyUncombined); }
        }

        private List<LabourDemandDepartment> m_leads;
        public List<LabourDemandDepartment> Leads
        {
            get { return m_leads; }
            set { m_leads = value; RaisePropertyChanged (() => Leads); RaisePropertyChanged (() => EmptyLeads); }
        }

        private LabourDemandDepartment m_selectedLead;
        public LabourDemandDepartment SelectedLead
        {
            get { return m_selectedLead; }
            set { m_selectedLead = value; RaisePropertyChanged (() => SelectedLead); }
        }

        private List<LabourDemandDepartment> m_currentCombined;
        public List<LabourDemandDepartment> CurrentCombined
        {
            get { return m_currentCombined; }
            set { m_currentCombined = value; RaisePropertyChanged (() => CurrentCombined); RaisePropertyChanged (() => EmptyCombined); RaisePropertyChanged (() => CombinedEmptyText);  }
        }

        public bool EmptyUncombined
        {
            get { return Uncombined == null || Uncombined.Count == 0; }
        }

        public bool EmptyCombined
        {
            get { return CurrentCombined == null || CurrentCombined.Count == 0; }
        }

        public bool EmptyLeads
        {
            get { return Leads == null || Leads.Count == 0; }
        }

        public string CombinedEmptyText
        {
            get { return (SelectedLead == null) ? Localiser.Get ("combined_dept_combined_empty_no_lead") : Localiser.Get ("combined_dept_combined_empty"); }
        }

        private List<ChangeCommit> m_pendingChanges = new List<ChangeCommit>();
        public List<ChangeCommit> PendingChanges
        {
            get { return m_pendingChanges; }
            set { m_pendingChanges = value; RaisePropertyChanged (() => CanSave); }
        }

        public bool CanSave
        {
            get { return  m_pendingChanges.Count > 0; }
        }

        private JDASite CurrentSite
        {
            get;
            set;
        }

        public CombineDeptViewModel (
            SettingsService settings,
            IAlertBox alertService,
            IToastService toastService,
            IMvxMessenger messenger,
            CombinedDeptService combinedDeptService,
            SessionData sessionData,
            IESSConfigurationService essConfig)
        {
            m_messenger = messenger;
            m_settings = settings;
            m_alertService = alertService;
            m_toastService = toastService;
            m_combinedDeptService = combinedDeptService;
            m_sessionData = sessionData;
            m_essConfig = essConfig;
        }

        public void Init()
        {
            if(m_orgEndToken == null)
                m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            
            if(m_signOutToken == null)
                m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage> (OnSignOut);

            Update();
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            if(m_signOutToken != null)
                m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);

            if(m_orgEndToken != null)
                m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);

            m_signOutToken = null;
            m_orgEndToken = null;
        }

        private void CheckModified(Action<int> saveCallback)
        {
            if (CanSave)
            {
                m_alertService.ShowOptions (Localiser.Get("combined_dept_modified"), Localiser.Get("combined_dept_changes_message"), 
                    new List<string> () { Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                    Localiser.Get("continue_editing_message"), saveCallback );
            }
            else
            {
                saveCallback(2);
            }
        }

        public void OnRefresh()
        {
            CheckModified (async (i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if (shouldSave)
                        await SubmitAndUpdate();
                    else
                        Update();
                }
            });
        }

        private async void Update(bool fetch = true)
        {
            if (fetch)
            {
                Loading = true;
                LoadingMessage = Localiser.Get("combined_dept_waiting_wheel_loading");

                PendingChanges = new List<ChangeCommit> ();
                CurrentSite = m_sessionData.Site;

                Data = await m_combinedDeptService.GetCombinedDepartmentsForStore (CurrentSite.Name);

                if (Data == null)
                    ErrorMessage = "Failed to fetch combined department data";

                Loading = false;
                LoadingMessage = string.Empty;
            }

            UpdateData ();
        }

        private void UpdateData()
        {
            //Are we "empty"
            NoData = Data == null || ( Data.Combined.Count == 0 && Data.Uncombined.Count == 0 );

            if (!NoData)
            {
                Uncombined = Data.Uncombined.OrderBy(x => x.Name).ToList();
                Leads = Data.Combined.OrderBy(x => x.Name).ToList();
                if (SelectedLead != null)
                {
                    //See if its still a lead
                    var newLead = Leads.FirstOrDefault(x => x.ID == SelectedLead.ID);
                    OnLeadSelected (newLead);
                }
            }
            else
            {
                Uncombined = new List<LabourDemandDepartment> ();
                Leads = new List<LabourDemandDepartment> ();
                CurrentCombined = new List<LabourDemandDepartment> ();
                SelectedLead = null;
            }
        }

        public void OnSubmit()
        {
            if (CanSave)
            {
                m_alertService.ShowYesNo (Localiser.Get("combined_dept_submit_hours_header"), Localiser.Get("combined_dept_submit_hours_body"), async (result) =>
                    {
                        if (result == 1)
                            await SubmitAndUpdate();
                    });
            }
        }

        private async Task SubmitAndUpdate()
        {
            Loading = true;
            LoadingMessage = Localiser.Get("combined_dept_waiting_wheel_submitting");

            bool success = await m_combinedDeptService.SubmitChanges(CurrentSite.Name, PendingChanges);

            if(success)
                m_toastService.Show("Saved");

            //Refresh
            Update();
        }

        public void OnLeadSelected(LabourDemandDepartment lead)
        {
            if (SelectedLead != null)
                SelectedLead.IsSelected = false;
            
            SelectedLead = lead;

            if (SelectedLead != null)
            {
                CurrentCombined = lead.Children.OrderBy(x => x.Name).ToList();;
                SelectedLead.IsSelected = true;
            }
            else
            {
                CurrentCombined = null;
            }
        }

        private void OnSignOut(SignOutMessage message)
        {
            // Unsubscribe from messages
            if(m_signOutToken != null)
                m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);

            if(m_orgEndToken != null)
                m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);

            m_signOutToken = null;
            m_orgEndToken = null;
        }
            
        private void OnOrgEnd(SetOrgEndMessage message)
        {
            if (!message.SessionTimeout)
            {
                OnRefresh ();
            }
        }


        //THese events fire from the drag and drop system for shifting departments around.
        //Note - the LabourDemandDepartment being passed in is NOT the actual one stored in
        //      various lists, its a clone/copy and doesn't contain CHILD data. Use the ID
        //      LEADID and ISLEAD values to determine which list to look for the actual obj.
        public void DroppedUncombined(LabourDemandDepartment dept)
        {
            if (!dept.IsLead && string.IsNullOrEmpty (dept.LeadID))
                return;
            
            ReassignDept (dept, GetDeptRegion(dept), CombinedRegion.Uncombined);
        }

        public void DroppedLead(LabourDemandDepartment dept)
        {
            if (dept.IsLead)
                return;
            
            ReassignDept (dept, GetDeptRegion(dept), CombinedRegion.Lead);
        }

        public void DroppedCombined(LabourDemandDepartment dept)
        {
            if (SelectedLead == null || !string.IsNullOrEmpty (dept.LeadID) || SelectedLead.ID == dept.ID)
                return;

            //Don't allow leads to be dropped into other leads (WFM-2048)
            if(dept.IsLead)
            {
                m_alertService.ShowOK(Localiser.Get("combined_dept_no_lead_into_lead_title"), 
                                      string.Format(Localiser.Get("combined_dept_no_lead_into_lead_body"), m_essConfig.CombineDepartmentsMaxChildDepartments) ); 
                return;
            }

            //Don't allow more than 7 children (WFM-2047)
            if(SelectedLead.Children != null && m_essConfig.CombineDepartmentsMaxChildDepartments >= 0 && SelectedLead.Children.Count >= m_essConfig.CombineDepartmentsMaxChildDepartments)
            {
                m_alertService.ShowOK(Localiser.Get("combined_dept_max_departments_header"), 
                                      string.Format(Localiser.Get("combined_dept_max_departments_body"), m_essConfig.CombineDepartmentsMaxChildDepartments) ); 
                return;
            }
            
            ReassignDept (dept, GetDeptRegion(dept), CombinedRegion.Combined);
        }

        private CombinedRegion GetDeptRegion(LabourDemandDepartment dept)
        {
            if (dept.IsLead)
                return CombinedRegion.Lead;
            if (!string.IsNullOrEmpty (dept.LeadID))
                return CombinedRegion.Combined;

            return CombinedRegion.Uncombined;
        }

        private void ReassignDept(LabourDemandDepartment deptCopy, CombinedRegion fromSection, CombinedRegion toSection)
        {
            //Do removals first
            switch (fromSection)
            {
                case CombinedRegion.Combined:
                    RemoveDeptFromCurrentCombined (deptCopy);
                    break;
                case CombinedRegion.Uncombined:
                    RemoveDeptFromUncombined (deptCopy);
                    break;
                case CombinedRegion.Lead:
                    RemoveDeptFromLeads (deptCopy);
                    break;
            }

            //Add to new location
            switch (toSection)
            {
                case CombinedRegion.Combined:
                    AddDeptToCurrentCombined (deptCopy);
                    break;
                case CombinedRegion.Uncombined:
                    AddDeptToUncombined (deptCopy);
                    break;
                case CombinedRegion.Lead:
                    AddDeptToLeads (deptCopy);
                    break;
            }

            //Reorder
            ReorderAll();
        }

        private void ReorderAll()
        {
            Uncombined = Uncombined.OrderBy (x => x.Name).ToList ();
            Leads = Leads.OrderBy (x => x.Name).ToList ();

            if(CurrentCombined!=null)
                CurrentCombined = CurrentCombined.OrderBy (x => x.Name).ToList ();
        }

        private void RemoveDeptFromLeads(LabourDemandDepartment deptCopy)
        {
            if (!deptCopy.IsLead)
                return;

            var actualLead = Leads.FirstOrDefault(x => x.ID == deptCopy.ID);
            if (actualLead != null)
            {
                //Remove all children
                foreach (var child in actualLead.Children)
                {
                    child.LeadID = string.Empty;
                    Uncombined.Add (child);
                }
                actualLead.Children.Clear ();

                //Deselect if selected
                if (actualLead == SelectedLead)
                    OnLeadSelected (null);

                //Remove from leads
                Leads = Leads.Where (x => x.ID != deptCopy.ID).ToList();

                //Create New Commit to server - Clear children first, then delete TODO - Check if delete just uncombines children on server anyway
                LabourDemandDepartment serverCommit = new LabourDemandDepartment ()
                    {
                        ID = deptCopy.ID,
                        Name = deptCopy.Name,
                        IsLead = true,
                        LeadID = string.Empty,
                    };
                PendingChanges.Add (new ChangeCommit ()
                    { 
                        Data = serverCommit,
                        Method = HttpVerb.Delete 
                    });

                RaisePropertyChanged (() => CanSave);
            }
        }

        private void RemoveDeptFromCurrentCombined(LabourDemandDepartment deptCopy)
        {
            if (string.IsNullOrEmpty (deptCopy.LeadID))
                return;
            
            var currentCombined = Leads.FirstOrDefault (x => x.ID == deptCopy.LeadID);
            if (currentCombined != null)
            {
                currentCombined.Children = currentCombined.Children.Where (x => x.ID != deptCopy.ID).ToList ();

                //Reselect to rebuild CurrentCombined list
                OnLeadSelected (currentCombined);

                //Create New Commit to server
                LabourDemandDepartment serverCommit = new LabourDemandDepartment ()
                    {
                        ID = currentCombined.ID,
                        Name = currentCombined.Name,
                        IsLead = true,
                        LeadID = string.Empty,
                        Children = currentCombined.Children,
                    };
                PendingChanges.Add (new ChangeCommit ()
                    { 
                        Data = serverCommit,
                        Method = HttpVerb.Post 
                    });

                RaisePropertyChanged (() => CanSave);
            }

        }

        private void RemoveDeptFromUncombined(LabourDemandDepartment deptCopy)
        {
            if (deptCopy.IsLead || !string.IsNullOrEmpty (deptCopy.LeadID))
                return;
            
            Uncombined = Uncombined.Where (x => x.ID != deptCopy.ID).ToList();

            //Nothing to commit to server
        }

        private void AddDeptToLeads(LabourDemandDepartment deptCopy)
        {
            Leads.Add (new LabourDemandDepartment ()
                {
                    ID = deptCopy.ID,
                    Name = deptCopy.Name,
                    LeadID = string.Empty,
                    IsLead = true,
                });

            //Create New Commit to server
            LabourDemandDepartment serverCommit = new LabourDemandDepartment ()
                {
                    ID = deptCopy.ID,
                    Name = deptCopy.Name,
                    IsLead = true,
                    LeadID = string.Empty,
                };
            PendingChanges.Add (new ChangeCommit ()
                { 
                    Data = serverCommit,
                    Method = HttpVerb.Post 
                });

            RaisePropertyChanged (() => CanSave);
        }

        private void AddDeptToCurrentCombined(LabourDemandDepartment deptCopy)
        {
            if (SelectedLead == null)
                return;

            //Get real current combined
            var currentCombined = Leads.FirstOrDefault(x => SelectedLead.ID == x.ID);
            if (currentCombined != null)
            {
                currentCombined.Children.Add (new LabourDemandDepartment ()
                    {
                        ID = deptCopy.ID,
                        Name = deptCopy.Name,
                        IsLead = false,
                        LeadID = currentCombined.ID,
                    });

                //Reselect
                OnLeadSelected(currentCombined);

                //Create New Commit to server
                LabourDemandDepartment serverCommit = new LabourDemandDepartment ()
                    {
                        ID = currentCombined.ID,
                        Name = currentCombined.Name,
                        IsLead = true,
                        LeadID = string.Empty,
                        Children = currentCombined.Children,
                    };
                PendingChanges.Add (new ChangeCommit ()
                    { 
                        Data = serverCommit,
                        Method = HttpVerb.Post 
                    });

                RaisePropertyChanged (() => CanSave);
            }
        }

        private void AddDeptToUncombined(LabourDemandDepartment deptCopy)
        {
            Uncombined.Add (new LabourDemandDepartment ()
                {
                    ID = deptCopy.ID,
                    Name = deptCopy.Name,
                    LeadID = string.Empty,
                    IsLead = false,
                });

            //Nothing to commit to server
        }
    }
}
