﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WFM.Core
{
    public class ScheduleSummaryViewModel : WFMViewModel
    {
        private readonly ScheduleService m_scheduleService;
        private readonly ScheduleSummaryService m_summaryService;

        private List<JDANameAndID> m_groups;
        public List<JDANameAndID> Groups
        {
            get { return m_groups; }
            set 
            { 
                m_groups = value; 
                RaisePropertyChanged(() => Groups);
                RaisePropertyChanged (() => ReceivedGroupData);
            }
        }
            
        private bool m_IsLoading;
        public bool IsLoading
        {
            get
            {
                return m_IsLoading;
            }
            set
            {
                m_IsLoading = value;
                RaisePropertyChanged (() => IsLoading);
                RaisePropertyChanged (() => IsEmpty);
            }
        }

        private List<JDANameAndID>  m_selectedGroups;
        public List<JDANameAndID>  SelectedGroups
        {
            get { return m_selectedGroups; }
            set { m_selectedGroups = value; RaisePropertyChanged(() => SelectedGroups); 
                if (m_selectedGroups != null)
                {
                    if (m_selectedGroups.Count == 0)
                        SelectionTitle = "No Workgroups";
                    else if (m_selectedGroups.Count == 1)
                        SelectionTitle = m_selectedGroups[0].Name;
                    else if (m_selectedGroups.Count == Groups.Count)
                        SelectionTitle = "All Workgroups";
                    else
                    {
                        SelectionTitle = String.Join (", ", m_selectedGroups.Select (group => group.Name));
                    }
                }
            }
        }

        private string m_selectionName;
        public string SelectionTitle
        {
            get { return m_selectionName; }
            set { m_selectionName = value; RaisePropertyChanged(() => SelectionTitle); }
        }

        private DateTime m_startDate;
        public DateTime StartDate
        {
            get { return m_startDate; }
            set 
            { 
                m_startDate = value; 
                RaisePropertyChanged(() => StartDate);
            }
        }

        public bool IsEmpty
        {
            get { return (Data == null || Data.Count == 0) && !IsLoading; }
        }

        public bool ReceivedGroupData
        {
            get
            {
                return Groups != null && Groups.Count > 0;
            }

            set
            {
                RaisePropertyChanged (() => ReceivedGroupData);
            }
        }

        private List<JDAScheduleSummaryGroup> m_data;
        public List<JDAScheduleSummaryGroup> Data
        {
            get { return m_data; }
            set
            {
                m_data = value;
                RaisePropertyChanged (() => Data);
                RaisePropertyChanged (() => IsEmpty);

                float demandCover = 0;
                float demandOver = 0;
                float demandUnder = 0;
                TitleTotals = new JDAScheduleSummaryGroupDayEntry ();
                if (Data != null && Data.Count > 0)
                {
                    for (int i = 0; i < Data.Count; i++)
                    {
                        //Add totals
                        TitleTotals.LabourHours += Data[i].LabourHours;
                        TitleTotals.ScheduledHours += Data[i].ScheduledHours;
                        TitleTotals.LabourCost += Data[i].LabourCost;
                        TitleTotals.ScheduledCost += Data[i].ScheduledCost;

                        //Add Day/Night totals to demand
                        foreach (var entry in Data[i].Entries)
                        {
                            JDAScheduleSummaryGroupDayEntry day = entry as JDAScheduleSummaryGroupDayEntry;
                            if (day != null && (day.SummaryType == JDADepartmentSummaryType.WeeklyDaysTotal || day.SummaryType == JDADepartmentSummaryType.WeeklyNightsTotal))
                            {
                                demandCover += day.DemandCovered;
                                demandOver += day.DemandOver;
                                demandUnder += day.DemandUnder;
                            }
                        }
                    }

                    //Coverage
                    float divideCo = TitleTotals.LabourHours;
                    float coverage = (divideCo > 0.001) ? (TitleTotals.ScheduledHours / divideCo)  * 100.0f : 0;
                    TitleTotals.Coverage = coverage;

                    // Cost Variance
                    float divideCv = TitleTotals.LabourHours;
                    float diff = TitleTotals.ScheduledCost - TitleTotals.LabourCost;
                    float costVariance = divideCv <= 0.001 ? 0 : TitleTotals.LabourCost != 0 ? (diff / TitleTotals.LabourCost) * 100.0f : 0;
                    TitleTotals.CostVariance = costVariance;

                    // Accuracy
                    float divideBy = demandCover + demandOver + demandUnder;
                    float accuracy = divideBy > 0.001 ? (demandCover / divideBy) * 100.0f : demandCover == 0 ? 100.0f : 0;
                    TitleTotals.Accuracy = accuracy;


                    //C# float string format handles percentages
                    TitleTotals.Coverage /= 100;
                    TitleTotals.CostVariance /= 100;
                    TitleTotals.Accuracy /= 100;

                }
                RaisePropertyChanged (() => TitleTotals);
            }
        }

        private JDAScheduleSummaryGroupDayEntry m_titleTotals = new JDAScheduleSummaryGroupDayEntry();
        public JDAScheduleSummaryGroupDayEntry TitleTotals
        {
            get { return m_titleTotals; }
            set { m_titleTotals = value; }
        }

        public ScheduleSummaryViewModel (ScheduleService schedule, ScheduleSummaryService summaryService)
        {
            m_scheduleService = schedule;
            m_summaryService = summaryService;
        }

        public async void Init()
        {
            StartDate = m_scheduleService.StartDate;

            IsLoading = true;
            await UpdateWorkgroups ();
            LoadPlanningSummary ();
        }

        public void OnReturn(List<JDANameAndID> items)
        {
            SelectedGroups = items;
            OnRefresh ();
        }

        private async Task UpdateWorkgroups()
        {
            this.Groups = await m_summaryService.GetSummaryWorkgroups() ?? new List<JDANameAndID>();

            if (this.Groups != null && this.Groups.Count > 0)
            {
                SelectedGroups = Groups;
            }
            else
            {
                Groups = new List<JDANameAndID> ();
                SelectionTitle = "Error Loading Workgroups" ;
            }
        }

        public void ShowGroupList()
        {
            List<SelectableWorkgroup> groups = Groups.Select (group => new SelectableWorkgroup ()
                { 
                    ID = group.ID, 
                    Name = group.Name, 
                    Selected = SelectedGroups.Any (item => item.ID == group.ID)
                }).ToList ();

            groups.Sort ((x, y) => x.Name.CompareTo (y.Name));
            
            ShowViewModel<WorkgroupViewModel> (
                new 
                { 
                    groups = groups
                });
        }

        public void OnMoveNext()
        {
            StartDate = StartDate.AddDays (7);

            m_summaryService.Cancel ();
            LoadPlanningSummary ();
        }

        public void OnMovePrev()
        {
            StartDate = StartDate.AddDays (-7);

            m_summaryService.Cancel ();
            LoadPlanningSummary ();
        }

        public async void LoadPlanningSummary()
        {
            IsLoading = true;
            this.Data = await m_summaryService.GetDepartmentSummaries (this.StartDate, SelectedGroups);
            IsLoading = false;
        }

        public void OnRefresh()
        {
            m_summaryService.Cancel ();
            LoadPlanningSummary ();
        }
    }
}
