﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class WorkgroupViewModel : WFMViewModel
    {
        private List<SelectableWorkgroup> m_groups;
        public List<SelectableWorkgroup> Groups
        {
            get { return m_groups; }
            set { m_groups = value; RaisePropertyChanged(() => Groups); }
        }

        public WorkgroupViewModel ()
        {            
        }

        public void SelectAll()
        {
            foreach (var group in Groups)
            {
                group.Selected = true;
            }

            RaisePropertyChanged (() => Groups);
        }

        public void ClearSelection()
        {
            foreach (var group in Groups)
            {
                group.Selected = false;
            }

            RaisePropertyChanged (() => Groups);
        }

        public void Init(List<SelectableWorkgroup> groups)
        {
            Groups = groups;
        }

        public void OnSelectGroup(JDANameAndID group)
        {
            var selected = Groups.FirstOrDefault(item => item.ID == group.ID);

            if (selected != null)
            {
                selected.Selected = !selected.Selected;
            }

            RaisePropertyChanged (() => Groups);
        }

        public void Apply()
        {
            Close (Groups.Where(item => item.Selected));
        }
    }
}