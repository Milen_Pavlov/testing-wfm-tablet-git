﻿using System;
using System.Collections.Generic;
using WFM.Core.MyHours;
using System.Linq;

namespace WFM.Core
{
    public class DepartmentSelection
    {
        public Department Dept { get; set; }
        public bool Selected { get; set; }
    }

    public class MyHoursDepartmentSelectViewModel : WFMViewModel
    {
        private readonly MyHoursDataService m_myHoursDataService;

        private List<DepartmentSelection> m_departments;
        public List<DepartmentSelection> Departments
        {
            get { return m_departments; }
            set 
            {
                m_departments = value;
                RaisePropertyChanged(() => Departments);
            }
        }

        public MyHoursDepartmentSelectViewModel(MyHoursDataService dataService)
        {
            m_myHoursDataService = dataService;
        }

        public void Init(Department dept)
        {
            var depts = m_myHoursDataService.CurrentWeekData.Departments.Select(x => new DepartmentSelection(){Dept = x, Selected = x.DepartmentCode == dept.DepartmentCode}).ToList();
            depts.Sort ((x, y) => x.Dept.DepartmentName.CompareTo (y.Dept.DepartmentName));
            Departments = depts;
        }

        public void OnDepartmentSelected(DepartmentSelection selected)
        {
            Close (selected.Dept);
        }
    }
}

