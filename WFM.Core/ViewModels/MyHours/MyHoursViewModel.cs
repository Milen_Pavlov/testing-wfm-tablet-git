﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Linq;
using WFM.Core.MyHours;

namespace WFM.Core
{
    public class MyHoursViewModel : WFMViewModel
    {
        public const int c_sortDept = -1;
        public const int c_unsorted = -2;

        public enum MyHoursMode
        {
            Week,
            Department,
        }

        public override string AnalyticsScreenName
        {
            get
            {
                return "My Hours";
            }
        }

        public const int WEEKS_TO_START_AHEAD = 4;

        public const DayOfWeek CUTOFF_DAY = DayOfWeek.Thursday;
        public const int CUTOFF_MINUTES_IN_DAY = (12 * 60);

        private readonly IMvxMessenger m_messenger;

        private readonly IUndoService m_undoService;
        private readonly MyHoursDataService m_myHoursDataService;
        private readonly IAlertBox m_alertService;
        private readonly IToastService m_toastService;
        private readonly ScheduleService m_schedule;

        private MvxSubscriptionToken m_signOutToken;
        private MvxSubscriptionToken m_orgEndToken;

        //Week and Day Selection
        private bool m_weekNumberDisplayed;

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        public string Date
        {
            get 
            { 
                int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (WeekStart);

                return m_weekNumberDisplayed ?
                    string.Format (Localiser.Get("week_starting_label"), weekNumber, WeekStart.ToString (Localiser.Get("dd/MM/yyyy"))) :
                    string.Format (Localiser.Get("starting_label"),  WeekStart.ToString (Localiser.Get("dd/MM/yyyy"))) ;
            }
        }

        public bool CanUndo
        {
            get { return m_undoService.CanUndo; }
        }
            
        public bool CanMovePrev
        {
            get
            { 
                return true;
            }
        }

        public bool CanMoveNext
        {
            get
            { 
                return true;
            }
        }

        public bool CanSubmit
        {
            get
            {
                //DateTime startOfWeek = TescoAccountingHelper.GetStartOfWeekForDate (WeekStart, ScheduleConfig);
               
                return CanModifyWeekStarting (WeekStart);
            }
        }

        bool CanModifyWeekStarting (DateTime weekStart)
        {
            var dayOffset = (CUTOFF_DAY - m_schedule.AccountingConfig.StartDay);

            if (dayOffset < 0)
                dayOffset += 7;
            
            DateTime cutOffTime = weekStart.AddDays (-7).AddDays(dayOffset).Date.AddMinutes(CUTOFF_MINUTES_IN_DAY);

            return DateTime.Now < cutOffTime;
        }

        private int SortDeptIndex { get; set; }
        private bool SortDeptAscending { get; set; }
        private bool[] WeekHeaderSorts;

        private SortState m_deptSort;
        public SortState DeptSort
        {
            get { return m_deptSort; }
            set { m_deptSort = value; RaisePropertyChanged(() => DeptSort); }
        }

        private List<ColumnHeader> m_headers;
        public List<ColumnHeader> Headers
        {
            get { return m_headers; }
            set { m_headers = value; RaisePropertyChanged(() => Headers); }
        }

        private List<MyHoursRowData> m_data;
        public List<MyHoursRowData> Data
        {
            get { return m_data; }
            set { m_data = value; RaisePropertyChanged(() => Data); }
        }

        private bool m_noData;
        public bool NoData
        {
            get { return  m_noData; }
            set { m_noData = value; RaisePropertyChanged (() => NoData); }
        }

        private string m_totalSales;
        public string TotalSales
        {
            get { return m_totalSales; }
            set { m_totalSales = value; RaisePropertyChanged (() => TotalSales);}
        }

        private DateTime m_weekStart;
        public DateTime WeekStart
        {
            get { return m_weekStart; }
            set { m_weekStart = value; RaisePropertyChanged (() => WeekStart);}
        }

        private MyHoursMode m_currentMode = MyHoursMode.Week;
        public MyHoursMode CurrentMode
        {
            get { return m_currentMode; }
            set 
            { 
                m_currentMode = value; 
                RaisePropertyChanged (() => CurrentMode); 
                RaisePropertyChanged (() => IsWeekMode); 
                RaisePropertyChanged (() => ColumnHeaderTitle);
            }
        }

        private string m_selectedDepartmentCode;
        public string SelectedDepartmentCode
        {
            get { return m_selectedDepartmentCode; }
            set { m_selectedDepartmentCode = value; }
        }

        private Department m_selectedDepartment;
        public Department SelectedDepartment 
        {
            get { return m_selectedDepartment; }
            set
            {
                m_selectedDepartment = value;
                RaisePropertyChanged (() => SelectedDepartment);
                RaisePropertyChanged (() => SelectedDepartmentName);
            }
        }

        private MyHoursRowData m_scrollToRow;
        public MyHoursRowData ScrollToRow
        {
            get { return m_scrollToRow; }
            set { m_scrollToRow = value; RaisePropertyChanged (() => ScrollToRow); }
        }

        public bool IsWeekMode
        {
            get { return CurrentMode == MyHoursMode.Week; }
        }

        public string SelectedDepartmentName
        {
            get 
            { 
                string name = string.Empty;

                if (SelectedDepartment != null)
                {
                    if (!string.IsNullOrEmpty (SelectedDepartment.DepartmentName))
                        name = SelectedDepartment.DepartmentName;
                    else
                        name = SelectedDepartment.DepartmentCode;
                }

                return name;
            }
        }

        public string ColumnHeaderTitle
        {
            get
            {
                switch (CurrentMode)
                {
                    case MyHoursMode.Department:
                        return Localiser.Get ("my_hours_leftcolumn_dayofweek");
                    case MyHoursMode.Week:
                        return Localiser.Get ("my_hours_leftcolumn_department");
                }

                return string.Empty;
            }
        }

        public MyHoursViewModel (
            MyHoursDataService myHoursDataService,
            ScheduleService schedule,
            IUndoService undoService,
            IAlertBox alertService,
            IToastService toastService,
            IMvxMessenger messenger)
        {
            m_schedule = schedule;
            m_messenger = messenger;
            m_undoService = undoService;
            m_myHoursDataService = myHoursDataService;
            m_alertService = alertService;
            m_toastService = toastService;
            m_weekNumberDisplayed = ConfigurationService.WeekNumberDisplayed;
            m_deptSort = new SortState ();

            WeekHeaderSorts = new bool[4];

            SortDeptAscending = true;
            SortDeptIndex = c_sortDept;

            WeekStart = GetStartDate ();

            CurrentMode = MyHoursMode.Week;
            SelectedDepartment = null;
        }

        DateTime GetStartDate ()
        {
            //Get the date of the next week
            DateTime weekToDisplay = m_schedule.AccountingHelper.GetStartOfWeekForDate (DateTime.Now.Date);

            if (!CanModifyWeekStarting (weekToDisplay.AddDays(7)))
            {
                weekToDisplay = weekToDisplay.AddDays (7 * (WEEKS_TO_START_AHEAD + 1));
            }
            else
            {
                weekToDisplay = weekToDisplay.AddDays (7 * WEEKS_TO_START_AHEAD);
            }

            //Jump forward 4 weeks
            return weekToDisplay;
        }

        public void Init(bool refresh)
        {
            Update(true);

            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage> (OnSignOut);
        }

        public void OnReturn(Department selectedDept)
        {
            if (selectedDept != null)
            {
                SelectedDepartment = selectedDept;
                SelectedDepartmentCode = selectedDept.DepartmentCode;
                UpdateData ();
            }
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            m_myHoursDataService.OnDataUpdated += OnWeekDataUpdated;
            m_myHoursDataService.OnBeforeUndo += OnBeforeUndo;
            m_myHoursDataService.OnAfterUndo += OnAfterUndo;
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            m_myHoursDataService.OnDataUpdated -= OnWeekDataUpdated;
            m_myHoursDataService.OnBeforeUndo -= OnBeforeUndo;
            m_myHoursDataService.OnAfterUndo -= OnAfterUndo;

            m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
        }

        private void CheckModified(Action<int> saveCallback)
        {
            if (CanUndo)
            {
                m_alertService.ShowOptions (Localiser.Get("my_hours_modified"), Localiser.Get("my_hours_changes_message"), 
                    new List<string> () { Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                    Localiser.Get("continue_editing_message"), saveCallback );
            }
            else
            {
                saveCallback(2);
            }
        }

        public void SetDate(DateTime date)
        {
            //Get start of week
            DateTime start = m_schedule.AccountingHelper.GetStartOfWeekForDate(date);
            if (WeekStart == start)
                return;
            
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        m_myHoursDataService.SubmitData();

                    if(CurrentMode == MyHoursMode.Department)
                    {
                        CurrentMode = MyHoursMode.Week;
                        SelectedDepartment = null;
                        SelectedDepartmentCode = string.Empty;
                        
                        if (SortDeptIndex == c_unsorted)
                            SortDeptIndex = c_sortDept;
                    }
                    
                    WeekStart = start;
                    Update(true);
                }
            });
        }

        public void OnMoveNext()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        m_myHoursDataService.SubmitData();
                    
                    WeekStart = WeekStart.AddDays(7);
                    Update(true);
                }
            });
        }

        public void OnMovePrev()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        m_myHoursDataService.SubmitData();
                    
                    WeekStart = WeekStart.AddDays(-7);
                    Update(true);
                }
            });
        }

        public void OnRefresh()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;
                    if(shouldSave)
                        m_myHoursDataService.SubmitData();
                    
                    Update(true);
                }
            });
        }

        public void Undo()
        {
            if(m_undoService.CanUndo)
                m_undoService.Undo();
        }

        public void OnBeforeUndo(object sender, EventArgs e)
        {
            MyHoursUndoStackOperation op = sender as MyHoursUndoStackOperation;
            if (op != null)
            {
                //Trigger UI event to show change animations etc
                CurrentMode = op.Day == null ? MyHoursMode.Week : MyHoursMode.Department;
                if (op.Day != null && m_myHoursDataService.CurrentWeekData != null && m_myHoursDataService.CurrentWeekData.Departments != null)
                {
                    SelectedDepartment = m_myHoursDataService.CurrentWeekData.Departments.FirstOrDefault (x => x.DepartmentCode == op.DeptCode);
                    SelectedDepartmentCode = op.DeptCode;
                }
                else
                {
                    SelectedDepartment = null;
                    SelectedDepartmentCode = string.Empty;
                }

                UpdateDate ();
                UpdateSorting ();
                UpdateHeaders ();
            }
        }

        public void OnAfterUndo(object sender, EventArgs e)
        {
            MyHoursUndoStackOperation op = sender as MyHoursUndoStackOperation;
            if (op != null)
            {
                MyHoursRowData data = null;

                if (CurrentMode == MyHoursMode.Week)
                    data = Data.FirstOrDefault (x => x.ID == op.DeptCode);
                else
                    data = Data.FirstOrDefault (x => x.ID == op.Day.Value.ToString ());

                if (data != null)
                {
                    //Flag cells for animation in UI
                    foreach(var column in data.Columns)
                        column.DidUndo = true;
                    
                    ScrollToRow = data;
                    RaisePropertyChanged (() => Data);

                    if(CurrentMode == MyHoursMode.Week)
                        m_toastService.Show (string.Format(Localiser.Get("my_hours_undo_week_hours_toast"), data.Columns[3].RowTitle, data.Columns[3].Text));
                    else
                        m_toastService.Show (string.Format(Localiser.Get("my_hours_undo_day_percentage_toast"), SelectedDepartmentName, GetDayString(op.Day.Value), data.Columns[0].Text));
                }
            }
        }

        private void Update(bool fetch = false)
        {
            UpdateSorting ();
            UpdateDate ();

            if (fetch)
            {
                Loading = true;
                LoadingMessage = Localiser.Get("my_hours_waiting_wheel");
                m_myHoursDataService.FetchData (WeekStart);
            }

            UpdateData ();
            UpdateHeaders();
        }

        private void UpdateData()
        {
            List<MyHoursRowData> data = new List<MyHoursRowData> ();

            var weekData = m_myHoursDataService.CurrentWeekData;
            if (weekData != null)
            {
                if (CurrentMode == MyHoursMode.Week)
                {
                    TotalSales = weekData.Sales.ToString(MyHoursDataService.IntegerSpecifier);

                    //Add row for every dept
                    if (weekData.Departments != null)
                    {
                        foreach (Department dept in weekData.Departments)
                        {
                            //Build column data, Week is Target, Allowed, Sales, Planned....>Day is Percent, Sales, Hours
                            
                            //Dept name
                            string name = string.Empty;
                            if (!string.IsNullOrEmpty (dept.DepartmentName))
                                name = dept.DepartmentName;
                            else
                                name = dept.DepartmentCode;
                            
                            List<MyHoursCellData> columns = new List<MyHoursCellData>();
                            columns.AddRange(GenerateWeekViewColumns (dept));
                            data.Add (new MyHoursRowData ()
                                {
                                    CanClickTitle = true,
                                    ID = dept.DepartmentCode,
                                    Title = name,
                                    Columns = columns,
                                });
                        }
                    }
                }
                else
                {
                    //Reget the selected department as it might have changed
                    if (weekData.Departments == null)
                        SelectedDepartment = null;
                    else
                        SelectedDepartment = weekData.Departments.FirstOrDefault(x => x.DepartmentCode == SelectedDepartmentCode);

                    if (SelectedDepartment != null)
                    {
                        TotalSales = SelectedDepartment.Sales.ToString (MyHoursDataService.IntegerSpecifier);
                        
                        for (int i = 0; i < 7; i++)
                        {
                            DayOfWeek day = WeekStart.AddDays (i).DayOfWeek;
                            string title = GetDayString (day);
                            
                            List<MyHoursCellData> columns = new List<MyHoursCellData> ();
                            columns.AddRange (GenerateDeptViewColumns (SelectedDepartment, day));
                            data.Add (new MyHoursRowData ()
                                {
                                    ID = day.ToString (),
                                    Title = title,
                                    Columns = columns,
                                });
                        }
                    }
                    else
                    {
                        TotalSales = "0";
                        NoData = true;
                    }
                }
            }

            //Sort
            if (CurrentMode == MyHoursMode.Week)
            {
                if (DeptSort.IsSorting)
                {
                    data.Sort ((x, y) => (DeptSort.IsAscending) ? x.Title.CompareTo (y.Title) : y.Title.CompareTo (x.Title));
                }
                else if(SortDeptIndex != -1)
                {
                    data.Sort ((x, y) => (WeekHeaderSorts[SortDeptIndex]) ? x.Columns[SortDeptIndex].Value.CompareTo (y.Columns[SortDeptIndex].Value) : y.Columns[SortDeptIndex].Value.CompareTo (x.Columns[SortDeptIndex].Value));
                }
            }

            Data = data;

            //Are we "empty"
            NoData = weekData == null || weekData.IsEmpty;
        }

        private void UpdateSorting()
        {
            DeptSort = new SortState () {
                IsSorting = SortDeptIndex == c_sortDept,
                IsAscending = SortDeptAscending
            };
        }

        private void UpdateHeaders()
        {

            var headers = new List<ColumnHeader> ();
            bool[] ascending = WeekHeaderSorts;
            string[] titles = new string[0];
            string[] subtitles = new string[0];
            if (CurrentMode == MyHoursMode.Department)
            {
                titles = new string[]
                    {
                        Localiser.Get("my_hours_header_day_percent"),
                        Localiser.Get("my_hours_header_day_sales"), 
                        Localiser.Get("my_hours_header_day_hours")
                    };

                subtitles = new string[]
                    {
                        string.Empty,
                        string.Format ("{0}", (SelectedDepartment!=null) ? SelectedDepartment.Sales.ToString (MyHoursDataService.IntegerSpecifier) : string.Empty),
                        string.Format ("{0}", (SelectedDepartment!=null) ? RoundToNearestQuarter (SelectedDepartment.PlannedHours).ToString (MyHoursDataService.DecimalSpecifier) : string.Empty),
                    };
            }
            else
            {
                titles = new string[]
                    {
                        Localiser.Get("my_hours_header_week_target_hours"),
                        Localiser.Get("my_hours_header_week_allowed_hours"), 
                        Localiser.Get("my_hours_header_week_sales"),
                        Localiser.Get("my_hours_header_week_planned_hours")
                    };

                var weekData = m_myHoursDataService.CurrentWeekData;
                if (weekData != null)
                {
                    subtitles = new string[]
                    {
                        string.Format ("{0}", RoundToNearestQuarter (weekData.TargetHours).ToString (MyHoursDataService.DecimalSpecifier)),
                        string.Format ("{0}", RoundToNearestQuarter (weekData.AllowedHours).ToString (MyHoursDataService.DecimalSpecifier)),
                        string.Format ("{0}", weekData.ForecastedSales.ToString (MyHoursDataService.IntegerSpecifier)),
                        string.Format ("{0}", RoundToNearestQuarter (weekData.PlannedHours).ToString (MyHoursDataService.DecimalSpecifier)),
                    };
                }
                else
                {
                    subtitles = new string[4];
                }
            }

            for (int i = 0; i < titles.Length; i++)
                headers.Add (new ColumnHeader ()
                    {
                        Index = i,
                        Title = titles[i],
                        Title2 = subtitles[i],
                        Sort = new SortState()
                            {
                                IsSorting = SortDeptIndex == i && CurrentMode == MyHoursMode.Week,
                                IsAscending = ascending[i],
                            }
                    });

            Headers = headers;
        }

        private void UpdateDate()
        {
            RaisePropertyChanged (() => Date);
            RaisePropertyChanged (() => CanMovePrev);
            RaisePropertyChanged (() => CanMoveNext);
        }


        List<MyHoursCellData> GenerateWeekViewColumns (Department dept)
        {
            return new List<MyHoursCellData> () {
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_week_target_hours"),
                    Value = dept.TargetHours,
                    Text = string.Format ("{0}", RoundToNearestQuarter (dept.TargetHours).ToString (MyHoursDataService.DecimalSpecifier)),
                    Dept = dept,
                },
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_week_allowed_hours"),
                    Value = dept.AllowedHours,
                    Text = string.Format ("{0}", RoundToNearestQuarter (dept.AllowedHours).ToString (MyHoursDataService.DecimalSpecifier)),
                    Dept = dept,
                },
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_week_sales"),
                    Value = dept.Sales,
                    Text = string.Format ("{0}", dept.Sales.ToString (MyHoursDataService.IntegerSpecifier)),
                    Dept = dept,
                },
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_week_planned_hours"),
                    Value = RoundToNearestQuarter (dept.PlannedHours),
                    Text = string.Format ("{0}", RoundToNearestQuarter (dept.PlannedHours).ToString (MyHoursDataService.DecimalSpecifier)),
                    EditableType = EditableCellType.WeekPlannedHours,
                    IsModified = dept.IsModified,
                    CanModify = CanSubmit && dept.UserCanModify,
                    IsLocked = dept.IsProtected,
                    Dept = dept,
                    ChangedBy = dept.WeekChange,
                }
            };
        }

        List<MyHoursCellData> GenerateDeptViewColumns (Department dept, DayOfWeek day)
        {
            Day deptDay = dept.Days.First(x => x.DayOfWeek == day.ToString());
            return new List<MyHoursCellData> () {
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_day_percent"),
                    Value = deptDay.Percentage,
                    Text = string.Format ("{0}", deptDay.Percentage.ToString (MyHoursDataService.PercentageSpecifier)),
                    EditableType = EditableCellType.DayPercentage,
                    IsLocked = deptDay.IsProtected,
                    IsModified = deptDay.DayPerChangeIndicator != 0,
                    CanModify = CanSubmit,
                    Dept = dept,
                    Day = day,
                    ChangedBy = deptDay.GetDayChange(),
                },
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_day_sales"),
                    Value = deptDay.Sales,
                    Text = string.Format ("{0}", deptDay.Sales.ToString (MyHoursDataService.IntegerSpecifier)),
                    Dept = dept,
                    Day = day,
                },
                new MyHoursCellData () {
                    RowTitle = dept.DepartmentName,
                    ColumnTitle = Localiser.Get ("my_hours_header_day_hours"),
                    Value = RoundToNearestQuarter (deptDay.PlannedHours),
                    Text = string.Format ("{0}", RoundToNearestQuarter (deptDay.PlannedHours).ToString (MyHoursDataService.DecimalSpecifier)),
                    Dept = dept,
                    Day = day,

                }
            };
        }

        private void OnWeekDataUpdated(object sender, EventArgs e)
        {
            Loading = false;
            LoadingMessage = string.Empty;

            UpdateData ();
            UpdateHeaders ();

            RaisePropertyChanged (() => CanUndo);
            RaisePropertyChanged (() => CanSubmit);
        }

        public void OnDeptHeaderSelect()
        {
            if (CurrentMode != MyHoursMode.Week)
                return;

            if(SortDeptIndex != c_sortDept)
                SortDeptIndex = c_sortDept;
            else
                SortDeptAscending = !SortDeptAscending;

            UpdateSorting ();
            UpdateData ();
            UpdateHeaders ();
        }

        public void OnHeaderSelect(ColumnHeader header)
        {
            if (CurrentMode != MyHoursMode.Week)
                return;
            
            // Sort by column header.Index
            if (SortDeptIndex != header.Index)
                SortDeptIndex = header.Index;
            else
                WeekHeaderSorts[header.Index] = !WeekHeaderSorts[header.Index];

            UpdateSorting ();
            UpdateData ();
            UpdateHeaders ();
        }

        public void OnRowSelected(MyHoursRowData data)
        {
            if (m_myHoursDataService.CurrentWeekData == null || m_myHoursDataService.CurrentWeekData.Departments == null)
                return;
            
            var dept = m_myHoursDataService.CurrentWeekData.Departments.FirstOrDefault (x => x.DepartmentCode == data.ID);

            if (dept != null)
            {
                SelectedDepartment = dept;
                SelectedDepartmentCode = dept.DepartmentCode;
                CurrentMode = MyHoursMode.Department;
                SortDeptIndex = c_unsorted;
                UpdateSorting ();
                UpdateData ();
                UpdateHeaders ();
            }
        }

        public void OnCellSelected(MyHoursCellData data)
        {
            m_myHoursDataService.ShowEditDialogueForCell (data);
        }

        public void OnDepartmentDropDownSelect()
        {
            ShowViewModel<MyHoursDepartmentSelectViewModel>(SelectedDepartment);
        }

        public void OnUndoConfirmed(MyHoursCellData data)
        {
            data.DidUndo = false;
            RaisePropertyChanged (() => Data);
        }

        public void OnReturnToWeek()
        {
            SelectedDepartment = null;
            SelectedDepartmentCode = string.Empty;
            CurrentMode = MyHoursMode.Week;

            if (SortDeptIndex == c_unsorted)
                SortDeptIndex = c_sortDept;

            UpdateSorting ();
            UpdateData ();
            UpdateHeaders ();
        }

        public void OnSubmit()
        {
            //Make sure we update the view if we now know we cant submit.
            RaisePropertyChanged (() => CanSubmit);

            if (CanUndo)
            {
                if (CanSubmit)
                {
                    m_alertService.ShowYesNo (Localiser.Get("my_hours_submit_hours_header"), Localiser.Get("my_hours_submit_hours_body"), (result) =>
                        {
                            if (result == 1)
                            {
                                Loading = true;

                                m_myHoursDataService.SubmitData ();
                            }
                        });
                }
                else
                {
                    m_alertService.ShowOK (Localiser.Get("my_hours_unable_hours_header"), String.Format (Localiser.Get("my_hours_unable_hours_body"), TimeSpan.FromMinutes (CUTOFF_MINUTES_IN_DAY).ToString (), CUTOFF_DAY.ToString ()), 
                        (i) => Update(true));
                }
            }
        }

        public void OnReset()
        {
            if (CanSubmit)
            {
                m_alertService.ShowYesNo (Localiser.Get("my_hours_reset_hours_header"), Localiser.Get("my_hours_reset_hours_body"), (result) =>
                    {
                        if (result == 1)
                        {
                            Loading = true;

                            m_myHoursDataService.ResetData ();
                        }
                    });
            }
            else
            {
                m_alertService.ShowOK (Localiser.Get("my_hours_unable_hours_header"), String.Format (Localiser.Get("my_hours_unable_hours_body"), TimeSpan.FromMinutes (CUTOFF_MINUTES_IN_DAY).ToString (), CUTOFF_DAY.ToString ()),
                    (i) => Update(true));
            }
        }

        private string GetDayString(DayOfWeek day)
        {
            //Bleurgh
            string title = string.Empty;
            switch(day)
            {
                case DayOfWeek.Sunday: title = Localiser.Get("sunday_short"); break;
                case DayOfWeek.Monday: title = Localiser.Get("monday_short"); break;
                case DayOfWeek.Tuesday: title = Localiser.Get("tuesday_short"); break;
                case DayOfWeek.Wednesday: title = Localiser.Get("wednesday_short"); break;
                case DayOfWeek.Thursday: title = Localiser.Get("thursday_short"); break;
                case DayOfWeek.Friday: title = Localiser.Get("friday_short"); break;
                case DayOfWeek.Saturday: title = Localiser.Get("saturday_short"); break;
            }

            return title;
        }


        private decimal RoundToNearestQuarter(decimal val)
        {
            return Math.Round (val * 4, MidpointRounding.ToEven) / 4;//Multiply by 4, round, divide by 4
        }

        private void OnSignOut(SignOutMessage message)
        {
            // Unsubscribe from messages
            m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
        }
            
        private void OnOrgEnd(SetOrgEndMessage message)
        {
            if (!message.SessionTimeout)
            {
                OnRefresh ();
            }
        }
    }
}
