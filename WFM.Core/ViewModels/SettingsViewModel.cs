﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using Cirrious.CrossCore;
using System.Linq;

namespace WFM.Core
{
    public class SettingsViewModel : WFMViewModel
    {
        private bool m_closing;

        private List<Environment> m_environments;
        public List<Environment> Environments
        {
            get { return m_environments; }
            set { m_environments = value; RaisePropertyChanged(() => Environments); }
        }

        private int m_envIndex;
        public int EnvIndex
        {
            get { return m_envIndex; }
            set { m_envIndex = value; RaisePropertyChanged(() => EnvIndex); }
        }

        private bool m_editing;
        public bool Editing
        {
            get { return m_editing; }
            set 
            { 
                m_editing = value; 
                RaisePropertyChanged(() => Editing); 

                EditingTitle = (Editing) ? "Save" : "Edit";
            }
        }

        private int m_scrollToRow;
        public int ScrollToRow
        {
            get { return m_scrollToRow; }
            set { m_scrollToRow = value; RaisePropertyChanged(() => ScrollToRow); }
        }

        private string m_editingTitle;
        public string EditingTitle
        {
            get { return m_editingTitle; }
            set { m_editingTitle = value; RaisePropertyChanged(() => EditingTitle); }
        }

        private bool m_canEdit;
        public bool CanEdit
        {
            get { return m_canEdit; }
            set { m_canEdit = value; RaisePropertyChanged(() => CanEdit); } 
        }

        private int EditingIndex { get; set; }

        public SettingsViewModel()
        {
        }

        public void Init()
        {
            Environments = Settings.Environments;
            EnvIndex = Settings.GetEnvironmentIndex();

            Editing = false;
            CanEdit = ConfigurationService.CanEditEnvironments;

            UpdateEditing();
        }

        public void OnReturn(Environment environment)
        {
            if (environment != null)
            {
                //Load in the config edits for the incomming environment...
                environment.Editable = true;
                environment.InEditMode = Editing;

                if (EditingIndex == -1)
                    Environments.Add(environment);
                else
                    Environments[EditingIndex] = environment;

                //Check for AddRow and shift to bottom
                int i = Environments.RemoveAll(x => x.IsAddRow);
                if(i > 0)
                    Environments.Add(new Environment(){ IsAddRow = true, Name = "Add new environment..." });

                RaisePropertyChanged(() => Environments);
                ScrollToRow = Environments.Count - 1;
            }
        }

        public void OnSetEnvironment(Environment env)
        {
            // Avoid re-entry
            if (m_closing)
                return;

            if (Editing)
            {
                ShowEditView(env);
                return;
            }

            // Update current environment
            var client = Mvx.Resolve<RestClient> ();
            client.BaseUri = new Uri(env.Address);

            // Save Environment
            Settings.SaveEnvironment (env);
            EnvIndex = Settings.GetEnvironmentIndex ();

            // Delay by short period to show selection
            Editing = false;
            UpdateEditing();
            m_closing = true;
            ScheduledTimer.CreateOneShot(TimeSpan.FromSeconds(0.2f), () => Close(ViewCloseType.CurrentView, env));
        }

        public void OnDeleteEnvironment(Environment env)
        {
            if (env.IsAddRow)
            {
                ShowEditView(env);
                return;
            }

            int index = Environments.IndexOf(env);

            //Check if this is the currently selected one - Just reselect the first if it is
            if (index == EnvIndex)
            {
                // Save Environment
                Settings.SaveEnvironment (Environments[0]);
                EnvIndex = Settings.GetEnvironmentIndex ();
            }

            Environments.Remove(env);
            RaisePropertyChanged(() => Environments);//'Remove' doesn't trigger a set

            if (index <= EnvIndex)
                EnvIndex--;
        }

        private void ShowEditView(Environment env)
        {
            if (env.IsAddRow)
            {
                EditingIndex = -1;
                ShowViewModel<EditEnvironmentViewModel>();
            }
            else if (env.Editable)
            {
                EditingIndex = Environments.IndexOf(env);
                ShowViewModel<EditEnvironmentViewModel>(new { environment = env, index = EditingIndex });
            }

            RaisePropertyChanged(() => EnvIndex);
        }

        public void OnToggleEdit()
        {
            if (!CanEdit)
                return;

            Editing = !Editing;

            if (!Editing)
                Save();

            UpdateEditing();
        }

        private void UpdateEditing()
        {
            if (!CanEdit)
                return;

            foreach (var env in Environments)
                env.InEditMode = Editing;

            //Add "Add" row if needed
            if (Editing)
                Environments.Add(new Environment(){ IsAddRow = true, Name = "Add new environment..." });
            else
                Environments = Environments.Where(x => !x.IsAddRow).ToList();

            RaisePropertyChanged(() => Environments);
            RaisePropertyChanged(() => EnvIndex);

            if (Editing)
                ScrollToRow = Environments.Count - 1;
            else
                ScrollToRow = EnvIndex;
        }

        public void Save()
        {
            Environments = Environments.Where(x => !x.IsAddRow).ToList();

            Settings.SaveCustomEnvironments(Environments);
            Environments = Settings.Environments;
            EnvIndex = Settings.GetEnvironmentIndex();

            //Reselect env in case we edited it and need to reapply the new config values or settings
            Settings.SaveEnvironment (Environments[EnvIndex]);
        }

        //TODO - Can't figure out how to get Popover View in consortium to trigger this when dismissing on tapping outside.
        public void OnCloseRequested()
        {
            if (Editing)
            {
                IAlertBox alertService = Mvx.Resolve<IAlertBox> ();
                alertService.ShowOptions("Unsaved Changes", "You have unsaved changes to Environments, how would you like to continue?", new List<string>(){ "Save", "Discard" }, "Cancel", (int i) =>
                    {
                        //1 = save, 2 = discard, 0 == cancel
                        switch (i)
                        {
                            case 0:
                                return;
                            case 1:
                                Save();
                                Close();
                                break;
                            case 2:
                                //Just close, this will discard pending changes
                                Close();
                                break;
                        }
                    });
            }
            else
                Close(ViewCloseType.CurrentView, Settings.Current.Env);
        }
    }
}
