﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;

namespace WFM.Core
{
    public class ShiftSwapApprovalDetailsViewModel : WFMViewModel
    {
        private readonly ApprovalsService m_approvalService;
        private readonly IAlertBox m_alertService;
        private readonly IToastService m_toastService;
        private readonly ISpinnerService m_spinnerService;
        private readonly SessionData m_sessionData;

        private ShiftSwapDetails m_details;
        public ShiftSwapDetails Details
        {
            get { return m_details; }
            set
            {
                m_details = value;
                RaisePropertyChanged (() => Details);
                RaisePropertyChanged (() => SwapperName);
                RaisePropertyChanged (() => CanApprove);
                RaisePropertyChanged (() => DateLabel);
            }
        }

        public string SwapperName
        {
            get { return Details.Swapper.EmployeeName; }
        }

        public bool CanApprove
        {
            get { return Details.IsPending; }
        }

        public string DateLabel
        {
            get { return string.Format (Localiser.Get ("swapshift_weekstarting_title"), Details.Swapper.StartOfWeek.ToString (Localiser.Get("dd MMMM yyyy"))); }
        }
        
        public ShiftSwapApprovalDetailsViewModel (ApprovalsService approvals, IAlertBox alert, IToastService toast, ISpinnerService spinner, SessionData session)
        {
            m_approvalService = approvals;
            m_alertService = alert;
            m_toastService = toast;
            m_spinnerService = spinner;
            m_sessionData = session;
        }

        public void Init(ShiftSwapDetails details)
        {
            Details = details;
        }

        public void Approve()
        {
            //Ask for confirmation
            m_alertService.ShowYesNo (Localiser.Get("swapshift_approve_title"),
                Localiser.Get("swapshift_approve_body"),
                (i) =>
                {
                    if(i==1)
                        SetApproval (true);
                });
        }

        public void Deny()
        {
            //Ask for confirmation
            m_alertService.ShowYesNo (Localiser.Get("swapshift_deny_title"),
                Localiser.Get("swapshift_deny_body"),
                (i) =>
                {
                    if(i==1)
                        SetApproval(false);
                });
        }

        private async Task SetApproval(bool approve)
        {
            SpinnerToken spinner = m_spinnerService.Show ((approve) ? Localiser.Get("swapshift_approve_spinner") : Localiser.Get("swapshift_deny_spinner"));

            var request = m_approvalService.UpdateSwapShiftStatus (m_sessionData.Site.SiteID, Details, approve);

            //Ensure spinner
            await Task.Delay(500);
            await request;

            if (request.Result.Success)
            {
                //Success
                m_toastService.Show((approve) ? Localiser.Get("swapshift_approval_success") : Localiser.Get("swapshift_deny_success"));
                Close (new { refresh = true });
            }
            else
            {
                //Fail
                string body = request.Result.ErrorMessage;
                if (string.IsNullOrEmpty (body))
                    body = (approve) ? Localiser.Get ("swapshift_approval_failed_body") : Localiser.Get ("swapshift_deny_failed_body");

                m_alertService.ShowOK ((approve) ? Localiser.Get ("swapshift_approval_failed_title") : Localiser.Get ("swapshift_deny_failed_title"), body);
            }

            m_spinnerService.Dismiss (spinner);
        }

    }
}

