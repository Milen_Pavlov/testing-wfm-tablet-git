﻿using System;
using System.Threading.Tasks;
using Consortium.Client.Core;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class ApprovalsViewModel : WFMViewModel
    {
        public enum ApprovalTab
        {
            Pending = 0,
            History = 1,
        }

        private readonly ApprovalsService m_approvalService;
        private readonly SessionData m_sessionData;
        private readonly IAlertBox m_alertService;
        private readonly ISpinnerService m_spinnerService;
        private readonly IMvxMessenger m_messenger;
        private readonly SettingsService m_settings;
        private readonly ScheduleService m_schedule;

        private JDAApprovalRequestsBody m_pendingData;
        private JDAApprovalHistoryBody m_historyData;

        private ApprovalTab m_currentTab = ApprovalTab.Pending;

        private MvxSubscriptionToken m_orgEndToken;
        private MvxSubscriptionToken m_signOutToken;

        private bool m_loadingHistory;
        public bool LoadingHistory
        {
            get { return m_loadingHistory; }
            set 
            {
                m_loadingHistory = value;
                RaisePropertyChanged (() => LoadingHistory);
                RaisePropertyChanged (() => Loading);
                RaisePropertyChanged (() => CanRefresh);
            }
        }

        private bool m_loadingPending;
        public bool LoadingPending
        {
            get { return m_loadingPending; }
            set 
            {
                m_loadingPending = value;
                RaisePropertyChanged (() => LoadingPending);
                RaisePropertyChanged (() => Loading);
                RaisePropertyChanged (() => CanRefresh);
            }
        }

        public bool Loading
        {
            get { return LoadingHistory || LoadingPending; }
        }

        private List<IApprovalData> m_currentData;
        public List<IApprovalData> CurrentData
        {
            get { return m_currentData; }
            set
            {
                m_currentData = value;
                RaisePropertyChanged(() => CurrentData);
                RaisePropertyChanged (() => IsEmpty);
            }
        }

        public bool IsEmpty
        {
            get { return CurrentData == null || CurrentData.Count == 0; }
        }


        public bool CanRefresh
        {
            get { return !Loading; }
        }

        private string m_emptyText;
        public string EmptyText
        {
            get { return m_emptyText; }
            set
            {
                m_emptyText = value;
                RaisePropertyChanged (() => EmptyText);
            }
        }

        public string DateString
        {
            get
            { 
                int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (StartDate);

                if (m_weekNumberDisplayed)
                    return string.Format (Localiser.Get ("week_starting_label"), weekNumber, StartDate.ToString(Localiser.Get ("dd/MM/yyyy"))); 
                else
                    return string.Format (Localiser.Get ("starting_label"), StartDate.ToString(Localiser.Get ("dd/MM/yyyy"))); 
            }
        }

        private DateTime m_startDate;
        public DateTime StartDate
        {
            get { return m_startDate; }
            set 
            { 
                m_startDate = value; 
                RaisePropertyChanged(() => StartDate);
                RaisePropertyChanged (() => DateString);
            }
        }

        private bool m_showDatePicker = false;
        public bool ShowDatePicker
        {
            get { return m_showDatePicker; } 
            set
            {
                m_showDatePicker = value;
                RaisePropertyChanged (() => ShowDatePicker);
            }
        }

        private bool m_weekNumberDisplayed;

        public ApprovalsViewModel (ApprovalsService approvalService, 
            SessionData sessionData,
            IAlertBox alertService,
            ISpinnerService spinnerService,
            IMvxMessenger messenger,
            SettingsService settingsService,
            ScheduleService scheduleService)
        {
            m_approvalService = approvalService;
            m_sessionData = sessionData;
            m_alertService = alertService;
            m_spinnerService = spinnerService;
            m_messenger = messenger;
            m_settings = settingsService;
            m_schedule = scheduleService;
            m_weekNumberDisplayed = ConfigurationService.WeekNumberDisplayed;
        }

        public async Task Init ()
        {            
            StartDate = m_sessionData.Site.EffectiveStartOfWeek.Value;

            //Monitor store change to refresh
            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);

            await RefreshPending ();
            await RefreshHistory ();
        }

        private void OnOrgEnd(SetOrgEndMessage message)
        {
            if (!message.SessionTimeout)
                RefreshAll ();
        }

        public async Task OnReturn(bool refresh)
        {
            if (refresh)
                await RefreshAll ();
        }

        public async Task Refresh()
        {
            switch (m_currentTab)
            {
                case ApprovalTab.History:
                    await RefreshHistory ();
                    break;
                case ApprovalTab.Pending:
                    await RefreshPending ();
                    break;
            }
        }

        public async Task RefreshAll()
        {
            await RefreshPending ();
            await RefreshHistory ();
        }

        private async Task RefreshHistory()
        {
            LoadingHistory = true;

            try
            {
                var history = m_approvalService.GetHistoryApprovals (m_sessionData.Site.SiteID, StartDate);

                //Now await the original task which may have already finished
                await history;

                if (history.Result != null)
                    m_historyData = history.Result;

                BuildData ();
            }
            catch(TaskCanceledException)
            {
            }

            LoadingHistory = false;
        }

        private async Task RefreshPending()
        {           
            LoadingPending = true;

            var pending = m_approvalService.GetAllPendingApprovals (m_sessionData.Site.SiteID); 
            await Task.Delay(500);//Make sure the spinner shows for long enough time, don't want it blinking in and out
            await pending;

            if (pending.Result != null)
                m_pendingData = pending.Result;
            
            BuildData ();

            LoadingPending = false;
        }

        public void OnTabSelect(ApprovalTab tab)
        {
            if (tab != m_currentTab)
            {
                m_currentTab = tab;
                ShowDatePicker = tab == ApprovalTab.History;
                BuildData ();
            }
        }

        private void BuildData()
        {
            List<IApprovalData> data = new List<IApprovalData> ();

            switch (m_currentTab)
            {
                case ApprovalTab.Pending:
                    if (m_pendingData != null)
                    {
                        foreach (JDASwapShiftApproval shiftswap in m_pendingData.SwapShiftApprovals)
                            data.Add (new ShiftSwapApprovalData (shiftswap, true));

                        //foreach (JDAAvailabilityApproval availability in m_pendingData.AvailabilityApprovals)
                        //    data.Add (new AvailabilityApprovalData (availability));
                    }

                    data.Sort ((x, y) => x.StartDate.CompareTo (y.StartDate));

                    EmptyText = Localiser.Get ("approvals_pending_empty");
                    break;
                case ApprovalTab.History:
                    if (m_historyData != null)
                    {
                        foreach (JDASwapShiftApproval shiftswap in m_historyData.SwapShiftApprovals)
                            data.Add (new ShiftSwapApprovalData (shiftswap, m_pendingData != null && m_pendingData.SwapShiftApprovals.Any (x => x.ID == shiftswap.ID)));

                        //foreach (JDAAvailabilityApproval availability in m_historyData.AvailabilityApprovals)
                        //    data.Add (new AvailabilityApprovalData (availability));
                    }

                    data.Sort ((x, y) => -x.StartDate.CompareTo (y.StartDate));

                    EmptyText = Localiser.Get ("approvals_history_empty");
                    break;
            }


            CurrentData = data;
        }

        public async Task ApprovalSelected(IApprovalData data)
        {
            ShiftSwapApprovalData shiftSwap = data as ShiftSwapApprovalData;
            if (shiftSwap != null)
            {
                if (shiftSwap.Status == ApprovalStatus.Cancelled)
                {
                    m_alertService.ShowOK (Localiser.Get ("approval_swapshift_invalid_title"), Localiser.Get ("approval_swapshift_invalid_body"));
                }
                else
                {
                    //Load details
                    SpinnerToken spinner = m_spinnerService.Show(Localiser.Get("approval_swapshift_loading"));

                    var request = m_approvalService.GetSwapApprovalDetail (m_sessionData.Site.SiteID, shiftSwap.SwapID, shiftSwap.SwappeeShiftID);

                    await Task.Delay (500);//Ensure spinner doesn't just appear momentarily
                    await request;

                    if (request.Result != null)
                    {
                        ShiftSwapDetails details = request.Result;
                        details.IsPending = shiftSwap.IsPending;
                        details.SwapID = int.Parse (shiftSwap.SwapID);
                        details.SwappeeShiftID = int.Parse (shiftSwap.SwappeeShiftID);

                        ShowViewModel<ShiftSwapApprovalDetailsViewModel> (new { details = details });
                    }
                    else
                    {
                        m_alertService.ShowOK (Localiser.Get ("approval_swapshift_error_title"), Localiser.Get ("approval_swapshift_error_body"));
                    }

                    m_spinnerService.Dismiss (spinner);
                }
            }
        }

        public void OnMoveNext()
        {
            StartDate = StartDate.AddDays (7);
            m_approvalService.CancelGetHistory ();
            RefreshHistory ();
        }

        public void OnMovePrev()
        {
            StartDate = StartDate.AddDays (-7);
            m_approvalService.CancelGetHistory ();
            RefreshHistory ();
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
        }

        private void OnSignOut(SignOutMessage message)
        {
            // Unsubscribe from messages
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
            m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
        }
    }
}

