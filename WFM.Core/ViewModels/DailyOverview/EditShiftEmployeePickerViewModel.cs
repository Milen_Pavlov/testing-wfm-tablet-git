﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WFM.Core
{
    public class EditShiftEmployeePickerViewModel : WFMViewModel
    {
        private readonly EmployeeService m_employeeService;
        private readonly SessionData m_sessionData;

        private bool m_loading = true;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged (() => Loading); }
        }

        private List<JDANameAndID> m_employees;
        public List<JDANameAndID> Employees
        {
            get { return m_employees; }
            set { m_employees = value; RaisePropertyChanged (() => Employees); }
        }


        public EditShiftEmployeePickerViewModel (EmployeeService employeeService, SessionData sessionData)
        {
            m_employeeService = employeeService;
            m_sessionData = sessionData;
        }

        public async void Init ()
        {
            Loading = true;

            var employees = m_employeeService.GetAllEmployees (m_sessionData.Site.SiteID);
            await Task.Delay (500);
            await employees;

            Employees = GenerateEmployeeData(employees.Result);

            Loading = false;
        }

        private List<JDANameAndID> GenerateEmployeeData(JDAEmployeeInfo[] employees)
        {
            bool skipLoggedInEmployee = ConfigurationService.CanEditOwnPunches == false;
            List<JDANameAndID> result = new List<JDANameAndID> ();
            foreach (var employee in employees)
            {
                if (skipLoggedInEmployee && m_sessionData.CurrentUsername.Equals (employee.LoginName, StringComparison.CurrentCultureIgnoreCase))
                    continue;
                var firstName = (string.IsNullOrEmpty(employee.NickName) ? employee.FirstName : employee.NickName);
                string employeeName = string.Format("{0}, {1}", employee.LastName, firstName);

                result.Add (new JDANameAndID ()
                    {
                        Name = employeeName,
                        ID = employee.EmployeeID,
                    });
            }

            return result;
        }

        public void OnSelected(JDANameAndID selected)
        {
            Close (ViewCloseType.CurrentView, new { name = selected.Name, id = selected.ID });
        }

        public void OnCancel()
        {
            Close (ViewCloseType.CurrentView);
        }
    }
}

