﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class DailyOverviewViewModel : WFMViewModel
    {
        public static readonly int BlankCountValue = -1;

        private readonly ScheduleService m_scheduleService;//Only used for Week Starting
        private readonly SessionData m_sessionData;//Used to get start of week too
        private readonly DailyOverviewService m_dailyOverviewService;
        private readonly IDailyOverviewFilterService m_dailyOverviewFilterService;
        private readonly IMvxMessenger m_messenger;
        private bool m_weekNumberDisplayed;

        public override string AnalyticsScreenName
        {
            get
            {
                return "DailyOverview";
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set 
            { 
                m_loading = value; 
                RaisePropertyChanged(() => Loading);
            }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private string m_emptyMessage;
        public string EmptyMessage
        {
            get { return m_emptyMessage; }
            set { m_emptyMessage = value; RaisePropertyChanged(() => EmptyMessage); }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        private int m_weekSelect;
        public int WeekSelect
        {
            get { return m_weekSelect; }
            set { m_weekSelect = value; RaisePropertyChanged(() => WeekSelect); }
        }

        private List<string> m_segments;
        public List<string> Segments
        {
            get { return m_segments; }
            set { m_segments = value; RaisePropertyChanged(() => Segments); }
        }

        private List<Tuple<int,int>> m_segmentCounts;
        public List<Tuple<int,int>> SegmentCounts
        {
            get { return m_segmentCounts; }
            set { m_segmentCounts = value; RaisePropertyChanged(() => SegmentCounts); }
        }

        private DateTime m_currentStartOfWeek;
        public DateTime CurrentStartOfWeek
        {
            get { return m_currentStartOfWeek; }
            set { m_currentStartOfWeek = value; }
        }

        private List<SiteTimecardEntry> m_timecards;
        public List<SiteTimecardEntry> Timecards
        {
            get { return m_timecards; }
            set { m_timecards = value; RaisePropertyChanged (() => Timecards); }
        }

        private List<SiteTimecardEntry> m_filteredTimecards;
        public List<SiteTimecardEntry> FilteredTimecards
        {
            get { return m_filteredTimecards; }
            set { m_filteredTimecards = value; RaisePropertyChanged (() => FilteredTimecards); }
        }

        private bool m_majorFilter;
        public bool MajorFilter
        {
            get { return m_majorFilter; }
            set { m_majorFilter = value; RaisePropertyChanged(() => MajorFilter); }
        }

        private List<FilterGroup> m_filters;
        public List<FilterGroup> Filters
        {
            get { return m_filters; }
            set { m_filters = value; RaisePropertyChanged(() => Filters); }
        }

        private FilterGroup m_currentFilter;
        public FilterGroup CurrentFilter
        {
            get { return m_currentFilter; }
            set { m_currentFilter = value; RaisePropertyChanged(() => CurrentFilter); }
        }

        private string m_title;
        public string Title
        {
            get { return m_title; }
            set 
            {
                m_title = value;
                RaisePropertyChanged (() => Title);
            }
        }

        private string m_employeeNameTitle;
        public string EmployeeNameTitle
        {
            get { return m_employeeNameTitle; }
            set
            {
                m_employeeNameTitle = value;
                RaisePropertyChanged (() => EmployeeNameTitle);
            }
        }

        private bool m_canAddShift;
        public bool CanAddShift
        {
            get { return m_canAddShift; }
            set
            {
                m_canAddShift = value;
                RaisePropertyChanged(() => CanAddShift);
            }
        }


        private CancellationTokenSource m_timecardRequestTokenSource = new CancellationTokenSource();

        public DailyOverviewViewModel (
            IAlertBox alertService,
            ScheduleService scheduleService,
            SessionData sessionData,
            DailyOverviewService dailyOverviewService,
            IDailyOverviewFilterService dailyOverviewFilterService,
            IMvxMessenger messenger
        )
        {
            m_scheduleService = scheduleService;
            m_sessionData = sessionData;
            m_dailyOverviewService = dailyOverviewService;
            m_dailyOverviewFilterService = dailyOverviewFilterService;
            m_weekNumberDisplayed = ConfigurationService.WeekNumberDisplayed;
            m_messenger = messenger;
        }

        public async Task Init ()
        {
            Title = Localiser.Get ("daily_overview");
            EmployeeNameTitle = Localiser.Get ("employee_name_label");

            m_orgEndToken = m_messenger.Subscribe<SetOrgEndMessage>(OnOrgEnd);
            SetWeekStartAndDay(m_sessionData.Site.EffectiveStartOfWeek.Value, DateTime.Now.DayOfWeek);

            m_dailyOverviewFilterService.CurrentView = Title;

            var loading = Load ();
            await loading;
        }

        private void OnFiltersChanged()
        {
            Filters = m_dailyOverviewFilterService.CurrentFilters;
            CurrentFilter = null;
        }

        public async Task Load()
        {
            //Cancel any previous loads, if they are done no bother, if not this will speed things up a touch
            m_timecardRequestTokenSource.Cancel();

            Loading = true;
            LoadingMessage = "Loading Daily Overview";

            UpdateDate (CurrentStartOfWeek, WeekSelect);

            //Wipe Counts
            SegmentCounts = null;

            m_timecardRequestTokenSource = new CancellationTokenSource();
            var timecardsRequest = m_dailyOverviewService.GetSiteTimecardsForDate (m_sessionData.Site.SiteID, CurrentStartOfWeek.AddDays (WeekSelect), m_timecardRequestTokenSource.Token);
            var unpairedRequest = m_dailyOverviewService.GetUnpairedPunchesForWeekStarting (m_sessionData.Site.SiteID, CurrentStartOfWeek, m_timecardRequestTokenSource.Token);
            var punchExceptionsRequest = m_dailyOverviewService.GetPunchExceptionsForWeekStarting (m_sessionData.Site.SiteID, CurrentStartOfWeek, m_timecardRequestTokenSource.Token);
            await Task.Delay (500);//Min spinner length
            var timecardsResult = await timecardsRequest;
            var unpairedResult = await unpairedRequest;
            var punchExceptionResult = await punchExceptionsRequest;
            await m_dailyOverviewFilterService.GenerateFilterGroups(m_sessionData.Site.SiteID);

            CurrentFilter = null;

            if (timecardsResult.WasCancelled)
                return;

            Timecards = timecardsResult.Timecards;
            SetMajorFilter (false);
            UpdateFilteredTimecards();

            BuildTupleCounts (unpairedResult, punchExceptionResult);

            if (!timecardsResult.Success)
                EmptyMessage = "Failed to load Daily Overview";
            else
                SetEmptyLabel ();

            OnFiltersChanged();

            Loading = false;
            LoadingMessage = string.Empty;
        }

        public void BuildTupleCounts(List<JDAUnpairedPunch> unpairedPunches, List<JDAPunchException> punchExceptions)
        {
            List<Tuple<int, int>> counts = new List<Tuple<int, int>> ();

            //Tuples are read only, build separate first then
            int[] minors = new int[7];
            int[] majors = new int[7];

            if (unpairedPunches != null)
            {
                foreach (var punch in unpairedPunches)
                {
                    if (!punch.Start.HasValue)
                        continue;
                    
                    //Get Date
                    DateTime date = punch.Start.Value.Date;
                    //Get offset in days from current start
                    int offset = (date - CurrentStartOfWeek.Date).Days;
                    if (offset >= 0 && offset < 7)
                        majors[offset]++;
                }
            }

            if (punchExceptions != null)
            {
                foreach (var punch in punchExceptions)
                {
                    if (!punch.BusinessDate.HasValue)
                        continue;
                    
                    //Get Date
                    DateTime date = punch.BusinessDate.Value.Date;
                    //Get offset in days from current start
                    int offset = (date - CurrentStartOfWeek.Date).Days;
                    if (offset >= 0 && offset < 7)
                        minors[offset]++;
                }
            }

            //Blank out any tuples for future days
            for (int i = 0; i < 7; i++)
            {
                //Get Date
                DateTime date = CurrentStartOfWeek.Date.AddDays(i);
                if (date.Date > DateTime.Now)
                {
                    majors[i] = BlankCountValue;
                    minors[i] = BlankCountValue;
                }

            }

            for (int i = 0; i < 7; i++)
                counts.Add (new Tuple<int, int> (minors[i],majors[i]));

            SegmentCounts = counts;
        }

        public async Task OnWeekSelect(int index)
        {
            WeekSelect = index;
            await Load();
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            m_dailyOverviewFilterService.OnFiltersChanged += OnFiltersChanged;
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            if(m_orgEndToken != null)
            {
                if(m_messenger != null)
                {
                    m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
                    m_orgEndToken = null;
                }
            }

            m_dailyOverviewFilterService.OnFiltersChanged -= OnFiltersChanged;
        }

        protected override async void OnOrgEnd (SetOrgEndMessage message)
        {
            base.OnOrgEnd (message);
            await OnRefresh();
        }

        private void UpdateDate(DateTime start, int startDay)
        {
            var day = start.AddDays (startDay);
            int weekNumber = m_scheduleService.AccountingHelper.GetWeekNumberForDate (start);
            Date = m_weekNumberDisplayed ?
                string.Format ("{0} {1}, {2}", Localiser.Get("week_label"), weekNumber, day.ToString(Localiser.Get("dddd, dd/MM/yyyy"))) :
                string.Format (day.ToString(Localiser.Get("dddd, dd/MM/yyyy")));

            // Update days in segments
            List<string> daySegments = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                int dayIndex = (int)start.AddDays(i).DayOfWeek;

                switch(dayIndex)
                {
                    case 0: daySegments.Add(Localiser.Get("sunday_short")); break;
                    case 1: daySegments.Add(Localiser.Get("monday_short")); break;
                    case 2: daySegments.Add(Localiser.Get("tuesday_short")); break;
                    case 3: daySegments.Add(Localiser.Get("wednesday_short")); break;
                    case 4: daySegments.Add(Localiser.Get("thursday_short")); break;
                    case 5: daySegments.Add(Localiser.Get("friday_short")); break;
                    case 6: daySegments.Add(Localiser.Get("saturday_short")); break;
                }
            }

            if (Segments == null || Segments.Count == 0 || Segments[1] != daySegments[1])
                Segments = daySegments;

            CanAddShift = ConfigurationService.CanModifyPunchesInFuture == true || day <= DateTime.Now.Date;
        }

        public async Task OnRefresh()
        {
            await Load();
        }

        public async Task OnMoveNext()
        {
            CurrentStartOfWeek = CurrentStartOfWeek.AddDays(7);
            await Load();
        }

        public async Task OnMovePrev()
        {
            CurrentStartOfWeek = CurrentStartOfWeek.AddDays(-7);
            await Load();
        }

        public void OnDateSelect()
        {
            ShowViewModel<DatePickerViewModel>(new{ currentWeek = CurrentStartOfWeek, numberOfWeeks = 2000, isWeekSelected = false, startingWeekNumber = m_scheduleService.AccountingHelper.GetWeekNumberForDate (CurrentStartOfWeek), currentDay = CurrentStartOfWeek.AddDays(WeekSelect) });
        }

        public void SetEmptyLabel()
        {
            if (FilteredTimecards == null || FilteredTimecards.Count == 0)
                EmptyMessage = (MajorFilter) ? "No Unpaired Punch Exceptions" : "No Daily Overview";
            else
                EmptyMessage = string.Empty;
        }

        public void OnApplyFilter(FilterGroup filter)
        {
            if(filter == null)
                m_dailyOverviewFilterService.ClearFilter();
            else
                m_dailyOverviewFilterService.ApplyFilter(filter);
           
            UpdateFilteredTimecards();
        }

        public void OnMajorFilter()
        {
            if (MajorFilter)
            {
                // End Edit Mode
                SetMajorFilter (false);
            }
            else
            {
                // Start Edit mode
                SetMajorFilter (true);
            }
        }

        //Used if we need filter unpaired punches in DailyOverview too
        public void SetMajorFilter(bool setFilter)
        {
            MajorFilter = setFilter;

            if (Timecards != null)
            {
                foreach (var timecard in Timecards)
                {
                    timecard.SetUnpairedPunchFilter(setFilter);
                }

                UpdateFilteredTimecards();
            }

            SetEmptyLabel ();
        }

        private void UpdateFilteredTimecards()
        {
            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);

            // Now filter using the Major/Minor toggle
            if(MajorFilter)
                FilteredTimecards = filteredTimecards.Where (x => x.Exceptions != null && DoesContainUnpairedPunch (x.Exceptions)).ToList ();
            else
                FilteredTimecards = filteredTimecards;

            SetEmptyLabel();
        }

        private bool DoesContainUnpairedPunch(List<TimecardException> exceptions)
        {
            var unpairedPunches = exceptions.Where (x => x.ExceptionType == TimecardExceptionType.Unpaired);
            if (unpairedPunches.Count() > 0)
                return true;

            return false;
        }

        public void OnTimecardSelected(SiteTimecardEntry timecard)
        {
            ShowViewModel<DailyOverviewEditShiftViewModel> ( new { timecard = timecard, date = CurrentStartOfWeek.AddDays(WeekSelect)  } );
        }

        public void OnAddShift()
        {
            ShowViewModel<DailyOverviewEditShiftViewModel> ( new { timecard = (null as SiteTimecardEntry), date = CurrentStartOfWeek.AddDays(WeekSelect) } );
        }

        public void OnReturn(bool shiftChanged)
        {
            if(shiftChanged)
                OnRefresh ().ConfigureAwait(false);
        }

        public async Task OnReturn(SelectedPickerItem item)
        {
            SetWeekStartAndDay(m_scheduleService.AccountingHelper.GetStartOfWeekForDate (item.Date), item.Date.DayOfWeek);
            await Load ();
        }

        private void SetWeekStartAndDay(DateTime date, DayOfWeek day)
        {
            CurrentStartOfWeek = date;

            DayOfWeek start = CurrentStartOfWeek.DayOfWeek;
            int offset = day - start;
            if (offset < 0)
                offset += 7;

            WeekSelect = offset;
        }
    }
}

