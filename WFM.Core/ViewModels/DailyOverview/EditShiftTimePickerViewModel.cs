﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class EditShiftTimePickerViewModel : WFMViewModel
    {
        private EditShiftPunchData m_item;
        private DateTime m_Time;

        private DateTime m_time;
        public DateTime Time
        { 
            get { return m_time; }
            set { m_time = value; RaisePropertyChanged(() => Time); }
        }
            
        public EditShiftTimePickerViewModel()
        {
        }

        public void Init(EditShiftPunchData punch, DateTime defaultTime)
        {
            m_item = punch;
            Time = defaultTime;
        }

        public void OnApply()
        {
            Close (ViewCloseType.CurrentView, new { punch = m_item, selectedTime = m_time.ToLocalTime() });
        }

        public void OnCancel()
        {
            Close (ViewCloseType.CurrentView);
        }
    }
}

