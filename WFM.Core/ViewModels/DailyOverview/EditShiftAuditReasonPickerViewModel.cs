﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class EditShiftAuditReasonPickerViewModel : WFMViewModel
    {
        private JDANameAndID[] m_types;
        public JDANameAndID[] Types
        {
            get { return m_types; }
            set { m_types = value; RaisePropertyChanged (() => Types); }
        }

        private EditShiftPunchData m_punch;

        public EditShiftAuditReasonPickerViewModel ()
        {
        }

        public void Init ()
        {
        }

        public void Init(JDANameAndID[] types)
        {
            Types = types;
        }

        public void OnSelected(JDANameAndID selected)
        {
            Close (ViewCloseType.CurrentView, new { selectedID = selected.ID });
        }

        public void OnCancel()
        {
            Close (ViewCloseType.CurrentView);
        }
    }
}

