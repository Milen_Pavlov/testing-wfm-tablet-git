using System;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WFM.Shared.Extensions;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class DailyOverviewEditShiftViewModel : WFMViewModel
    {
        private readonly ScheduleService m_schedule;
        private readonly SessionData m_sessionData;
        private readonly DailyOverviewEditShiftService m_editShiftService;
        private readonly IAlertBox m_alertBox;

        private CancellationTokenSource m_cancellationToken;

        private EditShiftData m_shiftData;

        private bool IsClosing { get; set; }
        private bool ShiftChanged { get; set; }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged (() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged (() => LoadingMessage); }
        }

        private bool m_createShift;
        public bool CreateShift
        {
            get { return m_createShift; }
            set { m_createShift = value; RaisePropertyChanged (() => CreateShift); }
        }

        private int m_employeeID;
        public int EmployeeID
        {
            get { return m_employeeID; }
            set 
            {
                m_employeeID = value; 
                RaisePropertyChanged (() => HasEmployee); 
                RaisePropertyChanged (() => EmployeeID);  
                RaisePropertyChanged(() => CanEditShift);
                RaisePropertyChanged(() => CanEditJob);
            }
        }

        private string m_employeeName;
        public string EmployeeName 
        {
            get { return m_employeeName; }
            set { m_employeeName = value; RaisePropertyChanged (() => EmployeeName); }
        }

        public bool HasEmployee
        {
            get { return m_employeeID > 0; }
            set { RaisePropertyChanged (() => HasEmployee); }
        }

        private DateTime? m_date;
        public DateTime? Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        public int? JobID { get; set; }

        private string m_jobName;
        public string JobName
        {
            get { return m_jobName; }
            set { m_jobName = value; HasJob = true; RaisePropertyChanged (() => JobName); }
        }

        private bool m_hasJob;
        public bool HasJob
        {
            get { return !String.IsNullOrEmpty (m_jobName); }
            set { RaisePropertyChanged (() => HasJob); }
        }
            
        public int PunchID { get; set; }

        private List<EditShiftPunchData> m_punches;
        public List<EditShiftPunchData> Punches
        {
            get { return m_punches; }
            set { m_punches = value; RaisePropertyChanged (() => Punches); }
        }

		private String m_titleText;
        public String TitleText
		{
			get { return m_titleText; }
            set { m_titleText = value; RaisePropertyChanged (() => TitleText); }
		}

        private bool m_allowPunchesForMeal;
        public bool AllowPunchesForMeal
        {
            get { return m_allowPunchesForMeal && ConfigurationService.AllowPunchesForMeals && CanEditShift; }
            set { m_allowPunchesForMeal = value; RaisePropertyChanged (() => AllowPunchesForMeal); }
            
        }

        private bool m_allowPunchesForBreak;
        public bool AllowPunchesForBreak
        {
            get { return m_allowPunchesForBreak && ConfigurationService.AllowPunchesForBreaks && CanEditShift; }
            set { m_allowPunchesForBreak = value; RaisePropertyChanged (() => AllowPunchesForBreak); }
        }

        public bool AllowPunchesForJob
        {
            get { return ConfigurationService.AllowPunchesForJobs && CanEditShift; }
            set { RaisePropertyChanged (() => AllowPunchesForJob); }
        }

        private bool m_editingLoggedInEmployee;
        public bool EditingLoggedInEmployee
        {
            get { return m_editingLoggedInEmployee; }
            set { m_editingLoggedInEmployee = value; RaisePropertyChanged (() => EditingLoggedInEmployee); }
        }

        public bool CanEditShift
        {
            get { return (EditingLoggedInEmployee == false || ConfigurationService.CanEditOwnPunches) && m_shiftData.IsReadOnly == false; }
            set 
            { 
                RaisePropertyChanged (() => CanEditShift); 
                RaisePropertyChanged (() => CanEditJob); 
                RaisePropertyChanged (() => CanMatchAllPunches);
                RaisePropertyChanged (() => AllowPunchesForJob);
            } 
        }

        public bool CanMatchAllPunches
        {
            get { return CreateShift == false && CanEditShift; }
            set {  RaisePropertyChanged (() => CanMatchAllPunches); }
        }

        public bool CanEditJob
        {
            get { return HasEmployee == true && CanEditShift == true; }
            set { RaisePropertyChanged (() => CanEditJob); }
        }

        public DailyOverviewEditShiftViewModel (ScheduleService schedule, SessionData session, DailyOverviewEditShiftService editShiftService, IAlertBox alertBox)
        {
            m_schedule = schedule;
            m_sessionData = session;
            m_editShiftService = editShiftService;
            m_alertBox = alertBox;
        }

        public async void Init(SiteTimecardEntry timecard, DateTime date)
        {
            m_cancellationToken = new CancellationTokenSource ();

            Date = date;

            LoadingMessage = String.Empty;

            if (timecard == null)
            {
                var auditRequest = m_editShiftService.GetAuditReasons (m_sessionData.Site.SiteID, m_cancellationToken.Token);
                await Task.Delay (500);
                var auditResponse = await auditRequest;

                m_shiftData = new EditShiftData () {
                    EmployeeName = String.Empty,
                    EmployeeID = -1,
                    Jobs = new List<JDACanWorkJobs>(),
                    Details = new List<EditShiftPunchDetails>(),
                    AuditReasons = auditResponse,
                };
                    
                int newPunchID = m_editShiftService.GetNewPunchID ();
                EditShiftPunchDetails shiftDetail = new EditShiftPunchDetails ()
                {
                        PunchCode = PunchCodes.Shift,
                        ScheduleID = newPunchID,
                        ShiftID =  newPunchID,
                };
                m_shiftData.Details.Add (shiftDetail);

                AllowPunchesForMeal = m_shiftData.AllowMealPunches;
                AllowPunchesForBreak = m_shiftData.AllowBreakPunches;
                EditingLoggedInEmployee = false;
                CanEditShift = true;
                CreateShift = true;
                TitleText = Localiser.Get ("edit_shift_title_new_shift");
                EmployeeName = Localiser.Get("edit_shift_select_employee");
                EmployeeID = -1;
                Loading = false;
                JobName = String.Empty;
                JobID = null;
                Punches = GeneratePunches (m_shiftData.Details);
                return;
            }

            Loading = true;

            var editShiftRequest = m_editShiftService.GetEditShiftData (m_sessionData.Site.SiteID, timecard, date, m_cancellationToken.Token);

            await Task.Delay (500);
            m_shiftData = await editShiftRequest;

            EditingLoggedInEmployee = m_sessionData.CurrentUsername.Equals(m_shiftData.LoginName, StringComparison.CurrentCultureIgnoreCase);
            
            RaisePropertyChanged(()=> CanEditShift);

            CreateShift = false;
            if(EditingLoggedInEmployee == false)
                TitleText = Localiser.Get ("edit_shift_title_edit_shift");
            else
                TitleText = Localiser.Get ("edit_shift_title_your_shift");
            
            if (m_shiftData != null)
            {
                EmployeeName = m_shiftData.EmployeeName;
                EmployeeID = m_shiftData.EmployeeID.GetValueOrDefault (-1);
                JobName = m_shiftData.Jobs.FirstOrDefault (x => x.IsPrimary).Name;
                JobID = m_shiftData.Jobs.FirstOrDefault (x => x.IsPrimary).ID;
                AllowPunchesForMeal = m_shiftData.AllowMealPunches;
                AllowPunchesForBreak = m_shiftData.AllowBreakPunches;
                Punches = GeneratePunches (m_shiftData.Details);
            }
            else
            {
                LoadingMessage = Localiser.Get("edit_shift_error_loading");
            }


            Loading = false;
        }
            
        public void OnCancel()
        {
            if (ShiftChanged && CanEditShift == true)
            {
                m_alertBox.ShowYesNo (
                    Localiser.Get ("edit_shift_discard_changes_title"),
                    Localiser.Get ("edit_shift_discard_changes_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    ShiftChanged = false;
                                    CloseWindow ();
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                CloseWindow();
            }
        }

        public async void OnSave()
        {
            if (CanEditShift == false)
            {
                m_alertBox.ShowOK(Localiser.Get("edit_shift_edit_own_shift_title"), Localiser.Get("edit_shift_edit_own_shift_message"));
                return;
            }

            if (m_shiftData.AuditReasons != null && m_shiftData.AuditReasons.Any ())
            {
                ShowViewModel<EditShiftAuditReasonPickerViewModel>(new { types = m_shiftData.AuditReasons });
            }
            else
            {
                DoSave ();
            }
        }

        public void OnReturn(int selectedID)
        {
            //if (selected != null)
            {
                m_shiftData.AuditReason = selectedID;
                DoSave ();
            }
        }

        private async void DoSave()
        {
            bool dataIsValid = m_editShiftService.ValidateEditShiftData (m_shiftData);
            if (dataIsValid)
            {
                Loading = true;
                var result = await m_editShiftService.WriteEditShiftData (m_shiftData, EmployeeID);
                if (result)
                {
                    ShiftChanged = true;
                    CloseWindow ();
                }
                else
                {
                    Loading = false;
                }
            }
        }

        private void CloseWindow()
        {
            IsClosing = true;
            InvokeOnMainThread(
                new Action(() => Close(new { shiftChanged = ShiftChanged }))
            );
        }

        public override void OnViewClosed ()
        {
            if(!IsClosing)
                Close (new { shiftChanged = ShiftChanged });

            base.OnViewClosed ();
        }

        private EditShiftPunchDetails CreateNewDetail(PunchCodes punchCode)
        {
            var startShift = Punches.First (x => x.PunchCode == PunchCodes.Shift);
            int newPunchDetail = m_editShiftService.GetNewPunchID ();
            EditShiftPunchDetails newDetail = new EditShiftPunchDetails ();
            newDetail.PunchCode = punchCode;
            newDetail.StartPunchDetailID = newPunchDetail;
            newDetail.EndPunchDetailID = newPunchDetail;
            newDetail.StartPunchID = startShift.PunchID;
            newDetail.EndPunchID = startShift.PunchID;
            newDetail.ScheduleID = startShift.PunchID;
            newDetail.ShiftID = startShift.ShiftID;

            return newDetail;
        }

        public void OnAddMeal()
        {
            EditShiftPunchDetails mealDetail = CreateNewDetail(PunchCodes.Meal);
            m_shiftData.Details.Add (mealDetail);
            Punches = GeneratePunches(m_shiftData.Details);
            ShiftChanged = true;
        }

        public void OnAddBreak()
        {
            EditShiftPunchDetails breakDetail = CreateNewDetail (PunchCodes.Break);
            m_shiftData.Details.Add (breakDetail);
            Punches = GeneratePunches(m_shiftData.Details);
            ShiftChanged = true;
        }

        public void OnAddJob()
        {
            EditShiftPunchDetails jobDetail = CreateNewDetail (PunchCodes.Job);
            jobDetail.JobName = JobName;
            jobDetail.JobID = JobID;
            m_shiftData.Details.Add (jobDetail);
            Punches = GeneratePunches(m_shiftData.Details);
            ShiftChanged = true;
        }

        public void OnMatchPunches()
        {
            foreach (var punch in Punches)
            {
                DoMatchPunch (punch);
            }

            Punches = GeneratePunches (m_shiftData.Details);
            ShiftChanged = true;
        }

        public void OnPickEmployee()
        {
            ShowViewModel<EditShiftEmployeePickerViewModel> ();
        }

        public void OnReturn(string name, int id)
        {
            EmployeeName = name;
            EmployeeID = id;
            UpdateJobsForEmployee (id); 
        }

        public void OnPickDate()
        {
            ShowViewModel<DatePickerViewModel> (new {currentWeek = m_schedule.StartDate, numberOfWeeks = 2000, isWeekSelected = false, startingWeekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (m_schedule.StartDate), currentDay = m_schedule.DayIndex});
        }
            
        public void OnReturn(SelectedPickerItem item)
        {
            if (item == null)
                return;

            if(ConfigurationService.CanModifyPunchesInFuture == false && item.Date > DateTime.Now.Date)
            {
                m_alertBox.ShowOK(Localiser.Get("edit_shift_in_future_error_title"), Localiser.Get("edit_shift_in_future_error_message"));
              return;
            }

            Date = item.Date;
            ShiftChanged = true;
        }

        public void OnPickJob()
        {
            if (EmployeeID <= 0)
            {
                m_alertBox.ShowOK (Localiser.Get("edit_shift_no_employee_title"), Localiser.Get("edit_shift_no_employee_message"));
                return;
            }
            List<JDANameAndID> jobData = GenerateJobData ();
            if (jobData == null || jobData.Count () <= 0)
            {
                m_alertBox.ShowOK (Localiser.Get("edit_shift_no_jobs_title"), Localiser.Get("edit_shift_no_jobs_message"));
                return;
            }
            ShowViewModel<EditShiftJobPickerViewModel> (new { punch = (null as EditShiftPunchData), types = jobData.ToArray() });
        }

        public void OnPickPunchJob(EditShiftPunchData punchData)
        {
            if (EmployeeID <= 0)
            {
                m_alertBox.ShowOK (Localiser.Get("edit_shift_no_employee_title"), Localiser.Get("edit_shift_no_employee_message"));
                return;
            }
            List<JDANameAndID> jobData = GenerateJobData ();
            if (jobData == null || jobData.Count () <= 0)
            {
                m_alertBox.ShowOK (Localiser.Get("edit_shift_no_jobs_title"), Localiser.Get("edit_shift_no_jobs_message"));
                return;
            }
            ShowViewModel<EditShiftJobPickerViewModel> (new { punch = punchData, types = jobData.ToArray() });
        }

        public void OnReturn(EditShiftPunchData punch, JDANameAndID selected)
        {
            if (punch == null)
            {
                JobName = selected.Name;
                JobID = selected.ID;
                return;
            }
                
            int shiftDetailIndex = punch.DetailIndex;
            m_shiftData.Details[shiftDetailIndex].JobName = selected.Name;
            m_shiftData.Details[shiftDetailIndex].JobID = selected.ID;

            Punches = GeneratePunches (m_shiftData.Details);
            ShiftChanged = true;
        }

        public void OnPickPunchActual(EditShiftPunchData punchData)
        {
            DateTime time = Date.GetValueOrDefault(DateTime.Now);
            if (punchData.Actual.HasValue)
            {
                time = punchData.Actual.Value;
            }
            else if (punchData.Scheduled.HasValue)
            {
                time = punchData.Scheduled.Value;
            }

            ShowViewModel<EditShiftTimePickerViewModel> (new { punch = punchData, defaultTime = time });
        }

        public void OnReturn(EditShiftPunchData punch, DateTime selectedTime)
        {
            if (punch == null)
            {
                return;
            }

            if(ConfigurationService.CanModifyPunchesInFuture == false && selectedTime > DateTime.Now)
            {
                m_alertBox.ShowOK(Localiser.Get("edit_shift_in_future_error_title"), Localiser.Get("edit_shift_in_future_error_message"));
                return;
            }
                
            int shiftDetailIndex = punch.DetailIndex;
            if(punch.IsStart)
                m_shiftData.Details[shiftDetailIndex].ActualStart = DateTime.SpecifyKind(selectedTime, DateTimeKind.Unspecified);
            else
                m_shiftData.Details[shiftDetailIndex].ActualEnd = DateTime.SpecifyKind(selectedTime, DateTimeKind.Unspecified);

            Punches = GeneratePunches (m_shiftData.Details);
            ShiftChanged = true;
        }

        public void OnMatchPunch(EditShiftPunchData punchData)
        {
            if (punchData.CanDelete)
            {
                DeletePunch (punchData);
            }
            else
            {
                DoMatchPunch (punchData);
            }
            Punches = GeneratePunches (m_shiftData.Details);
            ShiftChanged = true;
        }

        private void DoMatchPunch(EditShiftPunchData punchData)
        {
            if (punchData.Scheduled.HasValue && punchData.Actual.HasValue == false && punchData.CanMatch)
            {
                if (punchData.IsStart)
                    m_shiftData.Details[punchData.DetailIndex].ActualStart = m_shiftData.Details[punchData.DetailIndex].ScheduledStart;
                else
                    m_shiftData.Details[punchData.DetailIndex].ActualEnd = m_shiftData.Details[punchData.DetailIndex].ScheduledEnd;
            }
        }

        private void DeletePunch(EditShiftPunchData punchData)
        {
            if (punchData.IsStart)
                m_shiftData.Details[punchData.DetailIndex].ActualStart = null;
            else
                m_shiftData.Details[punchData.DetailIndex].ActualEnd = null;
        }

        private List<JDANameAndID> GenerateJobData()
        {
            List<JDANameAndID> result = new List<JDANameAndID> ();
            foreach (var job in m_shiftData.Jobs)
            {
                result.Add (new JDANameAndID ()
                    {
                        ID = job.ID,
                        Name = job.Name,
                    });
            }

            return result;
        }

        private void UpdateJobsForEmployee (int employeeID)
        {
            EmployeeData employee = m_schedule.Schedule.GetEmployee (employeeID);

            m_shiftData.Jobs.Clear ();
            if (employee == null)
                return;
            
            foreach (var job in employee.CanWorkJobs.Values)
            {
                m_shiftData.Jobs.Add (new JDACanWorkJobs ()
                    { 
                        Name = m_schedule.JobNameLookup[job.JobID],
                        ID = job.JobID,
                        IsPrimary = job.IsPrimary,
                    });
            }

            if (m_shiftData != null && m_shiftData.Jobs != null)
            {
                var primaryJob = m_shiftData.Jobs.First (x => x.IsPrimary);
                JobID = primaryJob.ID;
                JobName = primaryJob.Name;

                foreach (var detail in m_shiftData.Details)
                {
                    if (detail.PunchCode == PunchCodes.Shift)
                    {
                        detail.JobID = JobID;
                        detail.JobName = JobName;
                    }
                }

                Punches = GeneratePunches (m_shiftData.Details);
            }
        }

        private List<EditShiftPunchData> GeneratePunches(List<EditShiftPunchDetails> details)
        {
            List<EditShiftPunchData> result = new List<EditShiftPunchData> ();
            for (int i = 0; i < details.Count(); ++i)
            {
                EditShiftPunchData startPunch = new EditShiftPunchData ()
                {
                        PunchCode = details[i].PunchCode,
                        Description = details[i].JobName,
                        Scheduled = details[i].ScheduledStart,
                        Actual = details[i].ActualStart,
                        ShiftID = details[i].StartPunchID,
                        ScheduleID = details[i].ScheduleID,
                        PunchID = details[i].StartPunchID,
                        PunchDetailID = details[i].StartPunchDetailID,
                        JobID = details[i].JobID,
                        IsStart = true,
                        DetailIndex = i,
                };

                switch(details[i].PunchCode)
                {
                    case PunchCodes.Shift:
                        startPunch.CanEdit = CanEditShift;
                        result.Insert (0, startPunch);
                        break;
                    case PunchCodes.Break:
                        startPunch.CanEdit = AllowPunchesForBreak;
                        result.Insert (result.Count - 1, startPunch);
                        break;
                    case PunchCodes.Meal:
                        startPunch.CanEdit = AllowPunchesForMeal;
                        result.Insert (result.Count - 1, startPunch);
                        break;
                    case PunchCodes.Job:
                        startPunch.CanEdit = CanEditShift;
                        result.Insert (result.Count - 1, startPunch);
                        break;
                    default:
                        break;
                }

                if(ConfigurationService.CanModifyPunchesInFuture == false && startPunch.CanEdit == true && startPunch.Scheduled.HasValue)
                {
                    startPunch.CanEdit = startPunch.Scheduled.Value <= DateTime.Now;
                }

                // Only show one punch for a job transfer.
                if (details[i].PunchCode == PunchCodes.Job)
                    continue;
                
                EditShiftPunchData endPunch = new EditShiftPunchData ()
                {
                    PunchCode = details[i].PunchCode,
                    Description = details[i].JobName,
                    Scheduled = details[i].ScheduledEnd,
                    Actual = details[i].ActualEnd,
                    ShiftID = details[i].StartPunchID,
                    ScheduleID = details[i].ScheduleID,
                    PunchID = details[i].EndPunchID,
                    PunchDetailID = details[i].EndPunchDetailID,
                    JobID = details[i].JobID,
                    IsStart = false,
                    DetailIndex = i,
                };

                switch(details[i].PunchCode)
                {
                    case PunchCodes.Shift:
                        endPunch.CanEdit = CanEditShift;
                        result.Add (endPunch);
                        break;
                    case PunchCodes.Break:
                        endPunch.CanEdit = AllowPunchesForBreak;
                        result.Insert (result.Count - 1, endPunch);
                        break;
                    case PunchCodes.Meal:
                        endPunch.CanEdit = AllowPunchesForMeal;
                        result.Insert (result.Count - 1, endPunch);
                        break;
                    case PunchCodes.Job:
                        endPunch.CanEdit = CanEditShift;
                        result.Insert (result.Count - 1, endPunch);
                        break;
                    default:
                        break;
                }

                if(ConfigurationService.CanModifyPunchesInFuture == false && endPunch.CanEdit == true && endPunch.Scheduled.HasValue)
                {
                    endPunch.CanEdit = endPunch.Scheduled.Value <= DateTime.Now;
                }
            }
            return result;
        }
    }
}

