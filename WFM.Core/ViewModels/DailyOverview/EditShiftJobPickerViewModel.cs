﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class EditShiftJobPickerViewModel : WFMViewModel
    {
        private JDANameAndID[] m_types;
        public JDANameAndID[] Types
        {
            get { return m_types; }
            set { m_types = value; RaisePropertyChanged (() => Types); }
        }

        private EditShiftPunchData m_punch;

        public EditShiftJobPickerViewModel ()
        {
        }

        public void Init ()
        {
        }

        public void Init(EditShiftPunchData punch, JDANameAndID[] types)
        {
            Types = types;
            m_punch = punch;
        }

        public void OnSelected(JDANameAndID selected)
        {
            Close (ViewCloseType.CurrentView, new { punch = m_punch, selected = selected });
        }

        public void OnCancel()
        {
            Close (ViewCloseType.CurrentView);
        }
    }
}

