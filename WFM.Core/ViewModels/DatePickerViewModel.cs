﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class DatePickerViewModel : WFMViewModel
    {
        public string FirstDay { get; set; }
        public string SecondDay { get; set; }
        public string ThirdDay { get; set; }
        public string FourthDay { get; set; }
        public string FifthDay { get; set; }
        public string SixthDay { get; set; }
        public string SeventhDay { get; set; }

        private List<DatePickerModel> m_data; 
        public List<DatePickerModel> Data
        {
            get
            {
                return m_data;
            }
            set
            {
                m_data = value;
                RaisePropertyChanged (() => Data);
            }
        }

        private int m_scrollPosition;
        public int ScrollPosition
        {
            get
            {
                return m_scrollPosition;
            }
            set
            {
                m_scrollPosition = value;
                RaisePropertyChanged (() => ScrollPosition);
            }
        }

        public DateTime CurrentWeek { get; set; }
        public int ForwardAndPresentNumber { get; set; }
        public int? StartingWeekNumber { get; set; }

        private readonly ScheduleService m_schedule;

        public DatePickerViewModel (ScheduleService scheduleService)
        {
            m_schedule = scheduleService;
        }

        public void Init(DateTime currentWeek, int numberOfWeeks, bool isWeekSelected, int? startingWeekNumber, DateTime? currentDay)
        {
            CurrentWeek = currentWeek;
            ForwardAndPresentNumber = numberOfWeeks;
            StartingWeekNumber = startingWeekNumber;

            FirstDay = currentWeek.ToString ("dddd");
            SecondDay = currentWeek.AddDays(1).ToString ("dddd");
            ThirdDay = currentWeek.AddDays(2).ToString ("dddd");
            FourthDay = currentWeek.AddDays(3).ToString ("dddd");
            FifthDay = currentWeek.AddDays(4).ToString ("dddd");
            SixthDay = currentWeek.AddDays(5).ToString ("dddd");
            SeventhDay = currentWeek.AddDays(6).ToString ("dddd");

            m_data =  m_data ?? new List<DatePickerModel> ();
            for (int i = 0 - ForwardAndPresentNumber; i < ForwardAndPresentNumber; i++)
            {
                DateTime date = CurrentWeek.AddDays (i * 7);
                m_data.Add(new DatePickerModel()
                    { 
                        Date = date,
                        IsCurrentWeek = date.Equals(currentWeek),
                        WeekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate(date), 
                        SelectedDate = currentDay,
                        IsWeekSelected = isWeekSelected
                    }
                );
            }
            ScrollPosition = m_data.Count / 2;
        }

        public void OnSetDate(SelectedPickerItem item)
        {
            Close (new { item = item } );
        }
    }
}