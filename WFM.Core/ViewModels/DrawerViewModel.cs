using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.CrossCore;
using System.Threading.Tasks;
using System.Linq;

namespace WFM.Core
{
    public class DrawerViewModel : WFMViewModel
    {
        private readonly SessionService m_session;
        private readonly SessionData m_sessionData;
        private readonly ISpinnerService m_spinner;
        private readonly IMvxMessenger m_messenger;
        private readonly ApprovalsService m_approvalService;
        private readonly TimeOffService m_timeOffService;
        private readonly IRaygunService m_raygunService;
        private readonly FilteredSiteService m_filteredSiteService;
        private readonly ITermsOfUseService m_termsOfUseService;

        protected MvxSubscriptionToken m_timeOffNotificationToken;
        protected MvxSubscriptionToken m_approvalsNotificationToken;
		protected SpinnerToken m_spinnerToken;

        private IToastService m_toastService;

        public string Logo { get; set; }

        private List<DrawerItem> m_menuItems;
        public List<DrawerItem> MenuItems 
        { 
            get { return m_menuItems; }
            set
            {
                m_menuItems = value;
                RaisePropertyChanged (() => MenuItems);
                RaisePropertyChanged(() => CurrentDrawerIndex); 
            }
        }

        private string m_store;
        public string Store
        {
            get { return m_store; }
            set 
            {
                m_store = value; 
                RaisePropertyChanged(() => Store); 
            }
        }

        private List<JDAOrgUnit> m_orgUnits;
        public List<JDAOrgUnit> OrgUnits
        {
            get { return m_orgUnits; }
            set 
            {
                m_orgUnits = value; 
                RaisePropertyChanged(() => OrgUnits); 
            }
        }

        private bool m_drawerOpen;
        public bool DrawerOpen
        {
            get { return m_drawerOpen; }
            set 
            {
                m_drawerOpen = value; 
                RaisePropertyChanged(() => DrawerOpen);
            }
        }

        private bool m_showOrgUnits;
        public bool ShowOrgUnits
        {
            get { return m_showOrgUnits; }
            set 
            {
                m_showOrgUnits = value; 
                RaisePropertyChanged(() => ShowOrgUnits); 

                if (!ShowOrgUnits)
                    RaisePropertyChanged (() => CurrentDrawerIndex);
            }
        }

        private int m_currentOrgIndex = 0;
        public int CurrentOrgIndex 
        {
            get { return m_currentOrgIndex; }
            set
            {
                m_currentOrgIndex = value;
                RaisePropertyChanged (() => CurrentOrgIndex);
            }
        }

        private int m_currentDrawerItem;
        public int CurrentDrawerIndex
        {
            get { return m_currentDrawerItem; }
            set 
            { 
                m_currentDrawerItem = value; 
                RaisePropertyChanged(() => CurrentDrawerIndex); 
            }
        }

        public int DrawerIconNotificationCount
        {
            get
            {
                int total = 0;

                if (DisplayUnreadTimeOffNotificationCount)
                    total += UnreadTimeOffNotificationCount;

                if (DisplayPendingApprovalsNotificationCount)
                    total += PendingApprovalsNotificationCount;

                return total;
            }
        }

        private int m_prevpendingApprovalsNotificationCount = 0;
        private int m_pendingApprovalsNotificationCount;
        public int PendingApprovalsNotificationCount
        {
            get
            {
                if (!ConfigurationService.ShowApprovals)
                    return 0;

                return m_pendingApprovalsNotificationCount;
            }
            set
            {
                m_pendingApprovalsNotificationCount = value;
                RaisePropertyChanged(() => PendingApprovalsNotificationCount);
                RaisePropertyChanged(() => DrawerIconNotificationCount);
            }
        }

        private bool m_displayPendingApprovalsNotificationCount = true;
        public bool DisplayPendingApprovalsNotificationCount
        {
            get { return m_displayPendingApprovalsNotificationCount; }
            set
            {
                m_displayPendingApprovalsNotificationCount = value;
                RaisePropertyChanged(() => DisplayPendingApprovalsNotificationCount);
                RaisePropertyChanged(() => DrawerIconNotificationCount);
            }
        }

        private int m_prevUnreadTimeOffNotificationCount = 0;
        private int m_unreadTimeOffNotificationCount;
        public int UnreadTimeOffNotificationCount
        {
            get
            {
                if (!ConfigurationService.ShowTimeOffMenu)
                    return 0;

                return m_unreadTimeOffNotificationCount;
            }
            set
            {
                m_unreadTimeOffNotificationCount = value;
                RaisePropertyChanged(() => UnreadTimeOffNotificationCount);
                RaisePropertyChanged(() => DrawerIconNotificationCount);
            }
        }

        private bool m_displayUnreadTimeOffNotificationCount = true;
        public bool DisplayUnreadTimeOffNotificationCount
        {
            get { return m_displayUnreadTimeOffNotificationCount; }
            set
            {
                m_displayUnreadTimeOffNotificationCount = value;
                RaisePropertyChanged(() => DisplayUnreadTimeOffNotificationCount);
                RaisePropertyChanged(() => DrawerIconNotificationCount);
            }
        }

        private String m_searchPlaceholderText;
        public String SearchPlaceholderText
        {
            get { return m_searchPlaceholderText; }
            set 
            {
                m_searchPlaceholderText = value;
                RaisePropertyChanged (() => SearchPlaceholderText);
            }
        }

        private String m_searchText;
        public String SearchText
        {
            get { return m_searchText; }
            set 
            {
                m_searchText = value; 
                m_filteredSiteService.SetFilter (m_searchText); 
                RaisePropertyChanged (() => SearchText); 
            }
        }

        public DrawerViewModel(SessionService session, 
            SessionData sessionData, 
            ISpinnerService spinner, 
            IMvxMessenger messenger, 
            TimeOffService timeOffService,
            IToastService toastService, 
            ApprovalsService approvals,
            IRaygunService raygun,
            FilteredSiteService filteredSiteService,
            ITermsOfUseService termsOfUseService)
        {
            m_session = session;
            m_sessionData = sessionData;
            m_spinner = spinner;
            m_messenger = messenger;
            m_toastService = toastService;
            m_approvalService = approvals;
            m_timeOffService = timeOffService;
            m_raygunService = raygun;
            m_termsOfUseService = termsOfUseService;

            m_filteredSiteService = filteredSiteService;
            m_filteredSiteService.Subscribe (OnSitesUpdated);
            SearchPlaceholderText = Localiser.Get ("change_store_search");

            m_timeOffNotificationToken = messenger.SubscribeOnMainThread<PendingTORUpdatedMessage> (OnTimeOffNotificationCountUpdate);
            m_approvalsNotificationToken = messenger.SubscribeOnMainThread<PendingApprovalsUpdatedMessage> (OnPendingApprovalsNotificationCountUpdate);
        }

        public void OnSitesUpdated(List<JDAOrgUnit> sites)
        {
            OrgUnits = sites;    
        }

        public void Init()
        {
            //We've been getting crashes in here, so lets just check this...
            if (m_sessionData.Site == null)
            {
                LogRaygunData ("DrawerViewModel.Init - Site Null");
                return;
            }

            UnreadTimeOffNotificationCount = m_timeOffService.GetPreviousNotificationCount (m_sessionData.Site.SiteID);
            PendingApprovalsNotificationCount = m_approvalService.GetPreviousNotificationCount (m_sessionData.Site.SiteID);
            UpdateDrawers ();

            if (MenuItems == null)
            {
                LogRaygunData ("DrawerViewModel.Init - MenuItems Null");
                return;
            }

            //TODO FIrst view model?
            //Select first(after store) if no Schedule
            Type firstViewModel = typeof(ScheduleViewModel);
            int index = 1;
            for (int i = 0; i < MenuItems.Count; i++)
                if (MenuItems[i].ModelType == firstViewModel)
                    index = i;

            OnSelectMenu (MenuItems[index]);

            //Terms of Service Modal TODO - Check this works after the ShowViewModel above...:/
            m_termsOfUseService.LoadState();
            bool showToS = m_termsOfUseService.ShouldShowToS(m_sessionData.CurrentUserID);
            if(showToS)
                ShowViewModel<TermsOfServiceViewModel>(null, ViewShowType.Modal);
        }

        private void LogRaygunData(string exception)
        {
            //Log Raygun
            Dictionary<string, object> data = new Dictionary<string, object>();
            try
            {
                data.Add("Environment", Settings.Current.Env);
                data.Add("User", m_sessionData.CurrentUsername);
            }
            catch { }//We just don't want the custom logging to actually crash!

            m_raygunService.LogCustomError(new Exception(exception), data);

            if (m_toastService != null)
                m_toastService.Show ("An error occured connecting to JDA Site");

            //Return out to stop it crashing
            Close();
        }

        private DrawerItem m_changeStoreDrawerItem;
        private DrawerItem m_changePasswordDrawerItem;

        private void UpdateDrawers()
        {
            Logo = "assets://Theme/Logo.png";

            IMobileDevice device = Mvx.Resolve<IMobileDevice> ();

            m_changeStoreDrawerItem = new DrawerItem (Localiser.Get ("change_store"), "assets://Icons/Icon_Home.png", true, null, null);
            m_changePasswordDrawerItem = new DrawerItem (Localiser.Get ("changepassword_title"), "assets://Icons/Icon_Password.png", false, typeof (ChangePasswordViewModel), null);

            List<DrawerItem> items = new List<DrawerItem> ();

            if (ConfigurationService.ShowMyHours && device.Platform == Platform.Android)
                items.Add (new DrawerItem(Localiser.Get("my_hours"), "assets://Icons/Icon_MyHours.png", false, typeof(MyHoursViewModel), new { refresh = false }));

            if (ConfigurationService.ShowScheduleMenu)
                items.Add (new DrawerItem(Localiser.Get("store_schedule"), "assets://Icons/Icon_Schedule.png", false, typeof(ScheduleViewModel), new { refresh = false }));

            if (ConfigurationService.ShowTillAllocationMenu && device.Platform == Platform.Android)
                items.Add (new DrawerItem(Localiser.Get("till_allocation"), "assets://Icons/Icon_Till.png", false, typeof(RootViewModel), null));

            if (ConfigurationService.ShowNightAislePlannerMenu && device.Platform == Platform.Android)
                items.Add (new DrawerItem(Localiser.Get("night_aisle_planner"), "assets://Icons/Icon_NightAisle.png", false, typeof(NightAislePlannerViewModel), null));

            if (ConfigurationService.ShowCombineDepartments && device.Platform == Platform.Android)
                items.Add (new DrawerItem(Localiser.Get("combined_dept"), "assets://Icons/Icon_CombineDept.png", false, typeof(CombineDeptViewModel), null));

            if (ConfigurationService.ShowTimeOffMenu)
                items.Add (new DrawerItem(Localiser.Get("time_off"), "assets://Icons/Icon_Timeoff.png", false, typeof(TimeOffViewModel), new { refresh = false }, UnreadTimeOffNotificationCount > 0 ? UnreadTimeOffNotificationCount.ToString() : string.Empty));

            if (ConfigurationService.ShowApprovals && device.Platform == Platform.IOS)
                items.Add (new DrawerItem(Localiser.Get("approvals"), "assets://Icons/Icon_Approval.png", false, typeof(ApprovalsViewModel), null, PendingApprovalsNotificationCount > 0 ? PendingApprovalsNotificationCount.ToString() : string.Empty));

            if (ConfigurationService.ShowPlannedScheduleSummary && device.Platform == Platform.IOS)
                items.Add (new DrawerItem(Localiser.Get("plannedschedulesummary_drawername"), "assets://Icons/Icon_Approval.png", false, typeof(ScheduleSummaryViewModel), null));

            if (ConfigurationService.ShowDailyOverviewMenu && device.Platform == Platform.IOS)
                items.Add (new DrawerItem(Localiser.Get("daily_overview"), "assets://Icons/Icon_Schedule.png", false, typeof(DailyOverviewViewModel), null));

            if (ConfigurationService.ShowDailyOverviewMenu && device.Platform == Platform.IOS)
                items.Add (new DrawerItem(Localiser.Get("weekly_overview"), "assets://Icons/Icon_Timeoff.png", false, typeof(WeeklyOverviewViewModel), null));

            if(ConfigurationService.ShowSalesForecast && device.Platform == Platform.IOS)
                items.Add (new DrawerItem(Localiser.Get("sales_forecasting"), "assets://Icons/Icon_Sales_Forecast.png", false, typeof(SalesForecastViewModel), null));

            if(ConfigurationService.ShowOvertimeTracker && device.Platform == Platform.Android)
                items.Add(new DrawerItem(Localiser.Get("overtime_tracker"), "assets://Icons/Icon_Overtime.png", false, typeof(OvertimeTrackerViewModel), null));

            items.Add (m_changeStoreDrawerItem);

            if (ConfigurationService.ShowChangePassword)
                items.Add (m_changePasswordDrawerItem);

            MenuItems = items;

            UpdateSite();
        }

        private void OnTimeOffNotificationCountUpdate(PendingTORUpdatedMessage message)
        {
            UnreadTimeOffNotificationCount = message.Total;

            if (UnreadTimeOffNotificationCount != m_prevUnreadTimeOffNotificationCount)
            {
                m_prevUnreadTimeOffNotificationCount = UnreadTimeOffNotificationCount;
                UpdateDrawers();
            }
        }

        private void OnPendingApprovalsNotificationCountUpdate(PendingApprovalsUpdatedMessage message)
        {
            PendingApprovalsNotificationCount = message.Total;

            if (PendingApprovalsNotificationCount != m_prevpendingApprovalsNotificationCount)
            {
                m_prevpendingApprovalsNotificationCount = PendingApprovalsNotificationCount;
                UpdateDrawers();
            }
        }

        public override void OnViewClosed()
        {
            m_messenger.Unsubscribe<SetOrgBeginMessage>(m_orgBeginToken);
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);

            // Cancel any pending service operations
            //Mvx.Resolve<ScheduleService> ().Cancel (); //TODO: Investigate this`
   
            // Force sign out if we are still signed in and closing this view
            if (m_session.IsSignedIn)
                m_session.SignOut ();
        }

        public void OnSelectMenu(DrawerItem item)
        {
            if (item != m_changeStoreDrawerItem && item != m_changePasswordDrawerItem)
                CurrentDrawerIndex = MenuItems.IndexOf(item);

            if (item.ModelType != null)
            {
                ShowViewModel (item.ModelType, item.Parameters);
            }
            else
            {
                if (item == m_changeStoreDrawerItem)
                {
                    // Show Store Change
                    ShowOrgUnits = true;
                }
            }
        }

        public void OnReturn(string newPassword)
        {
            DrawerOpen = false;
            RaisePropertyChanged (() => CurrentDrawerIndex);

            if (!String.IsNullOrEmpty (newPassword))
            {
                if (m_toastService != null)
                {
                    m_toastService.Show ("Password changed successfully");
                }
            }
        }

        public void OnSelectOrg(JDAOrgUnit org)
        {
            m_sessionData.SetOrgUnitAsync(org);
            CurrentOrgIndex = OrgUnits.IndexOf (org);
        }

        public void OnCancelOrg()
        {
            ShowOrgUnits = false;
        }

        public void OnSignOut()
        {
            m_session.SignOut();
        }

        private void UpdateSite()
        {
            if (ConfigurationService.ShowSiteNumber)
            {
                Store = string.Format ("{0}, {1}", m_sessionData.Site.Name, m_sessionData.Site.LongName);
            }
            else
            {
                Store = m_sessionData.Site.LongName;
            }

            m_filteredSiteService.UpdateSites ();

            OrgUnits = new List<JDAOrgUnit>(m_sessionData.OrgUnits);
            var orgUnit = OrgUnits.FirstOrDefault(x => x.Name == m_sessionData.Site.Name);
            CurrentOrgIndex = OrgUnits.IndexOf (orgUnit);

            //Request Pending Notifications...
            m_approvalService.UpdatePendingApprovalsCount(m_sessionData.Site.SiteID);
            m_timeOffService.UpdatePendingTORCount (m_sessionData.Site.SiteID);
        }

        protected override void OnOrgBegin(SetOrgBeginMessage message)
        {
            base.OnOrgBegin (message);

			if (m_spinnerToken != null) 
				m_spinner.Dismiss(m_spinnerToken);

            m_spinnerToken = m_spinner.Show(Localiser.Get("changing_site"));
            DrawerOpen = false;
            RaisePropertyChanged (() => CurrentDrawerIndex);
        }

        protected override async void OnOrgEnd (SetOrgEndMessage message)
        {
            base.OnOrgEnd (message);

            UpdateSite();

			if (m_spinnerToken != null) 
			{
				m_spinner.Dismiss(m_spinnerToken);
				m_spinnerToken = null;
			}

            ShowOrgUnits = false;
        }
    }
}
