﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class TimeOffEmployeeHistoryViewModel : WFMViewModel
    {
        private List<TimeOffHistory> m_history;
        public List<TimeOffHistory> History
        {
            get { return m_history; }
            set
            {
                m_history = value;

                if (m_history.Count == 0)
                    NoDataLabel = Localiser.Get ("timeoff_details_tab_nodata");
                else
                    NoDataLabel = string.Empty;

                RaisePropertyChanged (() => History);
            }
        }

        private string m_noDataLabel;
        public string NoDataLabel
        {
            get { return m_noDataLabel; }
            set
            {
                m_noDataLabel = value;
                RaisePropertyChanged (() => NoDataLabel);
            }
        }

        public TimeOffEmployeeHistoryViewModel ()
        {
        }

        public void Init(List<TimeOffHistory> history, bool upcoming)
        {
            if (upcoming)
            {
                DateTime now = DateTime.Now.Date;
                History = history.Where (t => (t.From >= now || t.To >= now) && t.Status != TimeOffStatus.Deleted).ToList ();
            }
            else
            {
                History = history;
            }
        }
    }
}