﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class TimeOffPeerRequestsViewModel : WFMViewModel
    {
        private readonly IMvxMessenger m_messenger;
        private MvxSubscriptionToken m_createTimeOffPeers;

        private List<PeerRequest> m_peers;
        public List<PeerRequest> Peers
        {
            get { return m_peers; }
            set
            {
                m_peers = value;

                if (m_peers.Count == 0)
                    NoDataLabel = Localiser.Get ("timeoff_details_tab_nodata");
                else
                    NoDataLabel = string.Empty;

                RaisePropertyChanged (() => Peers);
            }
        }

        private string m_noDataLabel;
        public string NoDataLabel
        {
            get { return m_noDataLabel; }
            set
            {
                m_noDataLabel = value;
                RaisePropertyChanged (() => NoDataLabel);
            }
        }

        private string m_employeeText;
        public string EmployeeText 
        {
            get { return m_employeeText; }
            set 
            {
                m_employeeText = value;
                RaisePropertyChanged (() => EmployeeText);
            }
        }

        public TimeOffPeerRequestsViewModel (IMvxMessenger messenger)
        {
            m_messenger = messenger;
            EmployeeText = Localiser.Get ("colleague_label");
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            m_createTimeOffPeers = m_messenger.Subscribe<CreateTimeOffPeersMessage> (OnCreatePeers);
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<CreateTimeOffPeersMessage> (m_createTimeOffPeers);
        }

        public void OnCreatePeers(CreateTimeOffPeersMessage message)
        {
            Init (message.Changes);
        }

        public void Init(List<PeerRequest> peers)
        {
            Peers = peers;
        }
    }
}