﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;

namespace WFM.Core
{
    public class TimeOffDetailsViewModel : TimeOffBaseDetailViewModel
    {
        private class ConfirmOperationMessage : MvxMessage
        {
            public ConfirmOperationMessage(object sender) : base(sender) {}
        }

        public enum OperationOnClose
        {
            None,
            ForceRefresh
        }

        private enum OperationToPerform
        {
            None,
            Reject,
            Approve,
            Cancel
        }

        private readonly TimeOffService m_timeOffService;
        private readonly SessionData m_sessionData;
        private readonly IAlertBox m_alertBox;
        private readonly ISpinnerService m_spinner;
        private readonly ScheduleService m_schedule;

        private OperationToPerform m_pendingOperation = OperationToPerform.None;

        private bool m_CanApproveTimeOff;

        public bool CanApproveTimeOff
        {
            get
            {
                return m_CanApproveTimeOff;
            }
            set
            {
                m_CanApproveTimeOff = value;
                RaisePropertyChanged (() => CanApproveTimeOff);
            }
        }

        private bool m_CanApproveOwnTimeOff;
        public bool CanApproveOwnTimeOff
        {
            get
            {
                return m_CanApproveOwnTimeOff;
            }
            set
            {
                m_CanApproveOwnTimeOff = value;
                RaisePropertyChanged(() => CanApproveOwnTimeOff);
            }
        }

        private JDAEmployeeInfo m_EmployeeInfo;
        public JDAEmployeeInfo EmployeeInfo
        {
            get
            {
                return m_EmployeeInfo;
            }
            set
            {
                m_EmployeeInfo = value;
                RaisePropertyChanged(() => EmployeeInfo);
            }
        }
            
        public TimeOffDetailsViewModel (TimeOffService timeOffService, SessionData sessionData, IAlertBox alertBox, ISpinnerService spinner, ScheduleService schedule, IMvxMessenger messenger) : base (messenger)
        {
            m_timeOffService = timeOffService;
            m_sessionData = sessionData;
            m_alertBox = alertBox;
            m_spinner = spinner;
            m_schedule = schedule;

            CanApproveTimeOff = !ConfigurationService.TimeOffReadOnly;
            CanApproveOwnTimeOff = ConfigurationService.CanApproveOwnTimeOff;
        }

        public async void Init(DetailedTimeOffModel detail, JDAEmployeeInfo employee)
        {
            PendingAllocationChanges.Clear ();

            EmployeeInfo = employee;

            if (detail != null)
            {
                TimeOffModel = detail;
            }
            else
            {
                var timeOff = await m_timeOffService.GetAllTimeOffRequests(m_sessionData.Site.SiteID, DateTime.Now.Date.AddDays(-30).Date, DateTime.Now.AddDays(30).Date);

                if (timeOff != null && timeOff.Length > 0)
                {
                    TimeOffModel = await m_timeOffService.GetDetailedTimeOffRequest(m_sessionData.Site.SiteID, timeOff[0]);
                }
            }

            if (TimeOffModel == null)
            {
                Close ();
            }
            else
            {
                BuildTabTitles (TimeOffModel.PeerRequests.Count);
            }
        }
            
        public void ConfirmDeleteTimeOffRequest()
        {   
            HandleChangeTimeOffRequest (OperationToPerform.Cancel,Localiser.Get("approvals_details_canceltitle"));
        }

        public void ConfirmApproveTimeOffRequest()
        {
            HandleChangeTimeOffRequest (OperationToPerform.Approve,Localiser.Get("approvals_details_approvetitle"));
        }

        public void ConfirmRejectTimeOffRequest()
        {
            HandleChangeTimeOffRequest (OperationToPerform.Reject,Localiser.Get("approvals_details_denytitle"));
        }

        private void HandleChangeTimeOffRequest(OperationToPerform operation, string message)
        {
            DateTime? startTime = TimeOffModel.StartDate;
            if(m_schedule.IsWithinPreventChangesPeriod(startTime))
            {
                m_alertBox.ShowOK (Localiser.Get ("prevent_changes_title"), String.Format(Localiser.Get ("prevent_changes_message"), ConfigurationService.PreventChangesHours));
            }
            else if (!this.CanApproveOwnTimeOff && m_sessionData.CurrentUsername.Equals(EmployeeInfo.LoginName,StringComparison.CurrentCultureIgnoreCase) )
            {
                m_alertBox.ShowOK (Localiser.Get ("approvals_details_denysameuser_title"), Localiser.Get ("approvals_details_denysameuser_body"));
            }
            else
            {
                m_pendingOperation = operation;
                ShowViewModel<ModalTextInputViewModel> (message);
            }
        }

        public void OnReturn(ModalTextInputViewModel.ReturnParameters parameters)
        {
            if (parameters.IsSubmitted)
            {
                if (m_pendingOperation == OperationToPerform.Cancel)
                {
                    OnConfirmCancel (parameters.Text);
                }
                else if (m_pendingOperation == OperationToPerform.Approve)
                {
                    OnConfirmApprove (parameters.Text);
                }
                else if (m_pendingOperation == OperationToPerform.Reject)
                {
                    OnConfirmReject (parameters.Text);
                }
            }
        }
            
        public async void OnConfirmCancel(string comment)
        {
            var token = m_spinner.Show ();

            var result = await m_timeOffService.CancelHolidayRequest (this.TimeOffModel.TimeOffRequest, comment);

            m_spinner.Dismiss (token);

            if(result.Success)
            {
                Close (OperationOnClose.ForceRefresh);
            }
            else
            {
                m_alertBox.ShowOK(Localiser.Get("timeoff_approval_error_title"),result.Message, (option) => Close (OperationOnClose.ForceRefresh));
            }
        }

        public async void OnConfirmApprove(string comment)
        {
            var token = m_spinner.Show ();

            var result = await m_timeOffService.ApproveHolidayRequest (this.TimeOffModel.TimeOffRequest, comment, PendingAllocationChanges);

            m_spinner.Dismiss (token);

            if(result.Success)
            {
                Close (OperationOnClose.ForceRefresh);
            }
            else
            {
                m_alertBox.ShowOK(Localiser.Get("timeoff_approval_error_title"),result.Message, (option) => Close (OperationOnClose.ForceRefresh));
            }
        }

        public async void OnConfirmReject(string comment)
        {
            var token = m_spinner.Show ();

            var result = await m_timeOffService.DenyHolidayRequest (this.TimeOffModel.TimeOffRequest, comment);
            
            m_spinner.Dismiss (token);
                
            if(result.Success)
            {
                Close (OperationOnClose.ForceRefresh);
            }
            else
            {
                m_alertBox.ShowOK(Localiser.Get("timeoff_approval_error_title"),result.Message, (option) => Close (OperationOnClose.ForceRefresh));
            }
        }
    }
}