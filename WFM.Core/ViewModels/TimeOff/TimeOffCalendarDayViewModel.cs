﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class TimeOffCalendarDayViewModel : WFMViewModel
    {
        private List<ISection> m_requests;
        public List<ISection> Requests
        {
            get { return m_requests; }
            set
            {
                m_requests = value;
                RaisePropertyChanged (() => Requests);
            }
        }

        private string m_title;
        public string Title
        {
            get { return m_title; }
            set {
                m_title = value;
                RaisePropertyChanged (() => Title);
            }
        }

        public TimeOffCalendarDayViewModel()
        {
        }

        public void Init(TimeOffDayData data)
        {
            Title = data.Date.ToString (Localiser.Get("dd MMMM yyyy"));

            JDATimeOffRequestSection approved = new JDATimeOffRequestSection () { Date = data.Date, Status = TimeOffStatus.Approved };
            JDATimeOffRequestSection pending = new JDATimeOffRequestSection () { Date = data.Date, Status = TimeOffStatus.Pending };
            JDATimeOffRequestSection denied = new JDATimeOffRequestSection () { Date = data.Date, Status = TimeOffStatus.Denied };

            foreach (JDATimeOffRequest request in data.Data)
            {
                DateTime start = request.Start.Date;
                DateTime end = request.End.Date;

                if ((start < data.Date && end < data.Date) ||
                    (start > data.Date && end > data.Date))
                    continue;

                switch (request.Status)
                {
                    case TimeOffStatus.Approved:
                        approved.Items.Add (request);
                        break;
                    case TimeOffStatus.Denied:
                        denied.Items.Add (request);
                        break;
                    case TimeOffStatus.Pending:
                        pending.Items.Add (request);
                        break;
                }
            }

            List<ISection> items = new List<ISection> ();
            if (approved.Items.Count > 0)
                items.Add (approved);
            if (pending.Items.Count > 0)
                items.Add (pending);
            if (denied.Items.Count > 0)
                items.Add (denied);

            Requests = items;
        }

        public void OnSelect(JDATimeOffRequest request)
        {
            Close (new { returnRequest = request });
        }
    }
}

