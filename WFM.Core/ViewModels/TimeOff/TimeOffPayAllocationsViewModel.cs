﻿using System;
using System.Collections.Generic;
using Consortium.Client.Core;
using System.Collections;
using System.Linq;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class AllocationsUpdatedMessage : MvxMessage
    {
        public List<PayAllocation> Changes { get; set; }

        public AllocationsUpdatedMessage (object sender) : base (sender)
        {
        }
    }

    public class PayAllocationSection : ISection
    {
        private IList m_items;
        public IList Items
        {
            get { return m_items; }
            set { m_items = value; }
        }

        private double m_totalPaidHours;
        public double TotalPaidHours
        {
            get { return m_totalPaidHours; }
            set { m_totalPaidHours = value; }
        }

        private double m_totalUnpaidHours;
        public double TotalUnpaidHours
        {
            get { return m_totalUnpaidHours; }
            set { m_totalUnpaidHours = value; }
        }

        private DateTime m_startOfWeek;
        public DateTime StartOfWeek
        {
            get { return m_startOfWeek; }
            set { m_startOfWeek = value; }
        }

        public PayAllocationSection(DateTime startOfWeek)
        {
            StartOfWeek = startOfWeek;
            Items = new List<PayAllocationItem> ();
        }

        public void SortAndCompute()
        {
            //Annoying IList
            List<PayAllocationItem> list = Items as List<PayAllocationItem>;

            list.OrderBy (x => x.Allocation.Date);

            double totalPaid = 0;
            double totalUnpaid = 0;
            foreach (PayAllocationItem item in list)
            {
                if (item.Allocation.IsPaid)
                    totalPaid += item.Allocation.PaidHours;
                else
                    totalUnpaid += item.Allocation.PaidHours;
            }

            if (list.Count > 0)
            {
                list[list.Count - 1].TotalHours = totalPaid + totalUnpaid;
                list[list.Count - 1].DisplayTotal = true;
            }

            Items = list;
            TotalPaidHours = totalPaid;
            TotalUnpaidHours = totalUnpaid;
        }       
    }

    public class PayAllocationItem : MvxNotifyPropertyChanged
    {
        private PayAllocation m_allocation = new PayAllocation();
        public PayAllocation Allocation
        {
            get { return m_allocation; } 
            set { m_allocation = value; }
        }

        private double m_TotalHours;

        public double TotalHours
        {
            get
            {
                return m_TotalHours;
            }
            set
            {
                m_TotalHours = value;
                RaisePropertyChanged (() => TotalHours);
            }
        }

        private bool m_displayTotal;
        public bool DisplayTotal
        {
            get { return m_displayTotal; }
            set { m_displayTotal = value; }
        }

        public bool CanModify
        {
            get;
            set; 
        }

        public Action OnHoursChanged { get ; set; }

        public PayAllocationItem() { }
    }

    public class TimeOffPayAllocationsViewModel : WFMViewModel
    {
        private PayAllocationSection[] m_sections;
        public PayAllocationSection[] Sections
        {
            get { return m_sections; } 
            set
            {
                m_sections = value;

                if (m_sections == null || m_sections.Length == 0)
                    NoDataLabel = Localiser.Get ("timeoff_details_tab_nodata");
                else
                    NoDataLabel = string.Empty;

                SectionsUpdated = true;

                RaisePropertyChanged (() => Sections);
            }
        }

        private bool m_sectionsUpdated;
        public bool SectionsUpdated
        {
            get { return m_sectionsUpdated; }
            set
            {
                m_sectionsUpdated = value;
                RaisePropertyChanged (() => SectionsUpdated);
            }
        }

        private double m_totalUnpaidHours;
        public double TotalUnpaidHours
        {
            get { return m_totalUnpaidHours; }
            set
            {
                m_totalUnpaidHours = value;
                RaisePropertyChanged (() => TotalUnpaidHours);
            }
        }

        private double m_totalPaidHours;
        public double TotalPaidHours
        {
            get { return m_totalPaidHours; }
            set
            {
                m_totalPaidHours = value;
                RaisePropertyChanged (() => TotalPaidHours);
            }
        }

        private string m_noDataLabel;
        public string NoDataLabel
        {
            get { return m_noDataLabel; }
            set
            {
                m_noDataLabel = value;
                RaisePropertyChanged (() => NoDataLabel);
            }
        }

        private bool m_CanModify;
        public bool CanModify
        {
            get
            {
                return m_CanModify;
            }
            set
            {
                m_CanModify = value;
                RaisePropertyChanged (() => CanModify);
            }
        }

        private readonly IMvxMessenger m_messenger;
        private MvxSubscriptionToken m_createTimeOffAllocations;

        public TimeOffPayAllocationsViewModel (IMvxMessenger messenger)
        {
            m_messenger = messenger;
        }

        public override void OnViewOpened ()
        {
            base.OnViewOpened ();

            m_createTimeOffAllocations = m_messenger.Subscribe<CreateTimeOffAllocationsMessage> (OnCreateAllocations);
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<CreateTimeOffAllocationsMessage> (m_createTimeOffAllocations);
        }

        public void OnCreateAllocations(CreateTimeOffAllocationsMessage message)
        {
            //Clear pending as new allocations have arrived (Create time off does this)

            Init (message.Changes);
            SectionsUpdated = true;
        }

        public void Init(List<PayAllocation> allocations)
        {
            CanModify = !ConfigurationService.TimeOffReadOnly;

            if (allocations != null)
            {
                //Build Sectioned lists and custom items to aid with the view
                Dictionary<DateTime, PayAllocationSection> weeks = new Dictionary<DateTime, PayAllocationSection>();
                
                foreach (PayAllocation allocation in allocations)
                {
                    if (!weeks.ContainsKey (allocation.StartOfWeek))
                        weeks.Add (allocation.StartOfWeek, new PayAllocationSection (allocation.StartOfWeek));

                    var section = weeks[allocation.StartOfWeek];
                    var item = new PayAllocationItem () { Allocation = allocation, CanModify = CanModify };
                    item.OnHoursChanged += CalculateTotals;
                    section.Items.Add (item);
                }

                Sections = weeks.Values.ToArray ();

                CalculateTotals (false);
            }
        }

        public void CalculateTotals()
        {
            CalculateTotals (true);
        }

        public void CalculateTotals(bool sendUpdateMessage)
        {
            if (Sections != null)
            {
                double totalPaid = 0, totalUnpaid = 0;

                foreach (PayAllocationSection section in Sections)
                {
                    section.SortAndCompute ();
                    totalPaid += section.TotalPaidHours;
                    totalUnpaid += section.TotalUnpaidHours;
                }

                TotalPaidHours = totalPaid;
                TotalUnpaidHours = totalUnpaid;
            }

            if (sendUpdateMessage)
            {
                var message = new AllocationsUpdatedMessage(this)
                {
                    Changes = Sections.SelectMany(section => section.Items as List<PayAllocationItem>).Select(item => item.Allocation).Where(item => item.IsDirty).ToList()
                };

                m_messenger.Publish<AllocationsUpdatedMessage>(message);
            }
        }
    }
}