﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class TimeOffAccrualsViewModel : WFMViewModel
    {
        private List<Accrual> m_accruals;
        public List<Accrual> Accruals
        {
            get { return m_accruals; }
            set
            {
                m_accruals = value;

                if (m_accruals.Count == 0)
                    NoDataLabel = Localiser.Get ("timeoff_details_tab_nodata");
                else
                    NoDataLabel = string.Empty;

                RaisePropertyChanged (() => Accruals);
            }
        }

        private string m_noDataLabel;
        public string NoDataLabel
        {
            get { return m_noDataLabel; }
            set
            {
                m_noDataLabel = value;
                RaisePropertyChanged (() => NoDataLabel);
            }
        }

        public TimeOffAccrualsViewModel ()
        {
        }

        public void Init(List<Accrual> accruals)
        {
            Accruals = accruals;
        }
    }
}