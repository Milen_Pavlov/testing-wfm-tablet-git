﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public abstract class TimeOffBaseDetailViewModel : WFMViewModel
    {            
        public string EmployeeName
        {
            get
            {
                if (m_timeOffModel != null)
                    return m_timeOffModel.EmployeeName;
                else
                    return string.Empty;
            }
            set
            {
                RaisePropertyChanged (() => EmployeeName);
            }
        }

        public bool IncludePayAllocations
        {
            get
            {
                if (m_timeOffModel != null)
                    return !m_timeOffModel.EmployeeViewOnly;
                else
                    return false;
            }
            set
            {
                RaisePropertyChanged (() => IncludePayAllocations);
            }
        }

        private DetailedTimeOffModel m_timeOffModel;
        public DetailedTimeOffModel TimeOffModel
        {
            get
            {
                return m_timeOffModel;
            }

            set
            {
                m_timeOffModel = value;
                RaisePropertyChanged(() => TimeOffModel);
                RaisePropertyChanged (() => EmployeeName);
                RaisePropertyChanged (() => IncludePayAllocations);
            }
        }

        private string[] m_tabTitles;
        public string[] TabTitles
        {
            get { return m_tabTitles; }
            set
            {                
                m_tabTitles = value;
                RaisePropertyChanged (() => TabTitles);
            }
        }

        private List<PayAllocation> m_pendingAllocationChanges = null;
        public List<PayAllocation> PendingAllocationChanges
        {
            get
            {
                return m_pendingAllocationChanges;
            }
        }

        public List<PayAllocation> CurrentAllocations
        {
            get
            {
                //Add all current allocations not in pending
                List<PayAllocation> allocations = TimeOffModel.PayAllocations.Where(x => !m_pendingAllocationChanges.Any(y => y.Date == x.Date)).ToList();
                //Add Pending
                allocations.AddRange(m_pendingAllocationChanges);
                //Sort
                allocations.Sort((x, y) => x.Date.CompareTo(y.Date));

                return allocations;
            }
        }

        private int m_currentTab;
        public int CurrentTab
        {
            get { return m_currentTab; }
            set { m_currentTab = value; }
        }

        protected readonly IMvxMessenger m_messenger;

        private MvxSubscriptionToken m_allocationsChangedToken;

        public TimeOffBaseDetailViewModel(IMvxMessenger messenger)
        {
            m_messenger = messenger;

            m_pendingAllocationChanges = new List<PayAllocation> ();

            m_allocationsChangedToken = m_messenger.Subscribe<AllocationsUpdatedMessage> (OnAllocationsUpdated);
        }

        private void OnAllocationsUpdated(AllocationsUpdatedMessage allocationsChanged)
        {
            m_pendingAllocationChanges = allocationsChanged.Changes;
        }

        public void OnTabSelect(int index)
        {
            CurrentTab = index;
            switch (CurrentTab)
            {
                case 0:
                    if (!IncludePayAllocations)
                        ShowViewModel<TimeOffAccrualsViewModel> (new { accruals = TimeOffModel.Accruals });
                    else
                        ShowViewModel<TimeOffPayAllocationsViewModel> (new { allocations = CurrentAllocations });
                    break;
                case 1:
                    if (!IncludePayAllocations)
                        ShowViewModel<TimeOffEmployeeHistoryViewModel> (new { history = TimeOffModel.History, upcoming = false });
                    else
                        ShowViewModel<TimeOffAccrualsViewModel> (new { accruals = TimeOffModel.Accruals });
                    break;
                case 2:
                    if (!IncludePayAllocations)
                        ShowViewModel<TimeOffEmployeeHistoryViewModel> (new { history = TimeOffModel.History, upcoming = true });
                    else
                        ShowViewModel<TimeOffPeerRequestsViewModel> (new { peers = TimeOffModel.PeerRequests });
                    break;
                case 3:
                    ShowViewModel<TimeOffEmployeeHistoryViewModel> (new { history = TimeOffModel.History, upcoming = false });
                    break;
            }
        }    

        //TimeOffModel.EmployeeViewOnly = !includePayAllocations
        protected void BuildTabTitles (int peerRequestsCount, bool selectFirst = true)
        {
            //TabTitles
            string[] titles = new string[4];
            if (!IncludePayAllocations)
            {
                titles = new string[3];
                titles [0] = Localiser.Get ("timeoff_details_tab_accruals");
                titles [1] = Localiser.Get ("timeoff_details_tab_employeehistory");
                titles [2] = Localiser.Get ("timeoff_details_tab_upcoming");
            }
            else
            {
                titles [0] = Localiser.Get ("timeoff_details_tab_payallocations");
                titles [1] = Localiser.Get ("timeoff_details_tab_accruals");
                titles [3] = Localiser.Get ("timeoff_details_tab_employeehistory");
                if (peerRequestsCount > 0)
                    titles [2] = string.Format (Localiser.Get ("timeoff_details_tab_peerrequests_count"), peerRequestsCount);
                else
                    titles [2] = Localiser.Get ("timeoff_details_tab_peerrequests_nocount");
            }

            TabTitles = titles;

            if(selectFirst)
                OnTabSelect (0);
        }

    }
}