﻿//using System;
//using System.Collections.Generic;
//using Consortium.Client.Core;
//using Cirrious.MvvmCross.Plugins.Messenger;
//using System.Threading.Tasks;
//using Cirrious.CrossCore;
//using System.Linq;
//using Newtonsoft.Json;
//
//namespace WFM.Core
//{
//    public class TimeOffViewModel : WFMViewModel
//    {
//        private readonly ScheduleService m_schedule;
//        private readonly IMvxMessenger m_messenger;
//        private readonly TimeOffService m_timeoff;
//        private readonly EmployeeService m_employee;
//        private readonly SessionData m_session;
//        private readonly IAlertBox m_alert;
//        private readonly TimeOffNotificationService m_backgroundTimeOffService;
//
//        protected MvxSubscriptionToken m_scheduleReadBeginToken;
//        protected MvxSubscriptionToken m_scheduleReadEndToken;
//        protected MvxSubscriptionToken m_signOutToken;
//        protected MvxSubscriptionToken m_timeOffNotificationToken;
//        protected MvxSubscriptionToken m_orgEndToken;
//
//        private int m_currentStartDay;
//
//        private TimeOffStatus m_filter = TimeOffStatus.Approved | TimeOffStatus.Denied | TimeOffStatus.Pending;
//        public TimeOffStatus Filter
//        {
//            get { return m_filter; }
//            set {
//                m_filter = value;
//                RaisePropertyChanged (() => Filter);
//                FilterResults();
//            }
//        }
//
//        private List<string> m_segments;
//        public List<string> Segments
//        {
//            get { return m_segments; }
//            set {
//                m_segments = value;
//                RaisePropertyChanged (() => Segments);
//            }
//        }
//
//        private DateTime m_firstOfMonth;
//        public DateTime FirstOfMonth
//        {
//            get { return m_firstOfMonth; }
//            set 
//            { 
//                m_firstOfMonth = value;
//                RaisePropertyChanged(() => FirstOfMonth);
//            }
//        }
//
//        private int m_loadingCount = 0;
//        public int LoadingCount
//        {
//            get { return m_loadingCount; }
//            set
//            {
//                m_loadingCount = value;
//                Loading = m_loadingCount > 0;
//            }
//        }
//
//        private bool m_loading;
//        public bool Loading
//        {
//            get { return m_loading; }
//            set 
//            {
//                m_loading = value;
//                RaisePropertyChanged (() => Loading);
//            }
//        }
//
//        private string m_loadingMessage;
//        public string LoadingMessage
//        {
//            get { return m_loadingMessage; }
//            set {
//                m_loadingMessage = value;
//                RaisePropertyChanged (() => LoadingMessage);
//            }
//        }
//
//        private ICalendarDayData[] m_allRequests;
//        public ICalendarDayData[] AllRequests
//        {
//            get { return m_allRequests; }
//            set {
//                m_allRequests = value;
//                RaisePropertyChanged (() => AllRequests);
//            }
//        }
//
//        private ICalendarDayData[] m_visibleRequests;
//        public ICalendarDayData[] VisibleRequests
//        {
//            get { return m_visibleRequests; }
//            set {
//                m_visibleRequests = value;
//                RaisePropertyChanged (() => VisibleRequests);
//            }
//        }
//
//        private string m_title;
//        public string Title
//        {
//            get { return m_title; }
//            set 
//            {
//                m_title = value;
//                RaisePropertyChanged (() => Title);
//            }
//        }
//
//        private string m_filterTitle;
//        public string FilterTitle
//        {
//            get { return m_filterTitle; }
//            set 
//            {
//                m_filterTitle = value;
//                RaisePropertyChanged (() => FilterTitle);
//                FilterResults ();
//            }
//        }
//
//        private string m_checkboxAllTitle;
//        public string CheckboxAllTitle
//        {
//            get { return m_checkboxAllTitle; }
//            set 
//            {
//                m_checkboxAllTitle = value;
//                RaisePropertyChanged (() => CheckboxAllTitle);
//            }
//        }
//
//        private string m_checkboxPendingTitle;
//        public string CheckboxPendingTitle
//        {
//            get { return m_checkboxPendingTitle; }
//            set 
//            {
//                m_checkboxPendingTitle = value;
//                RaisePropertyChanged (() => CheckboxPendingTitle);
//            }
//        }
//
//        private string m_checkboxApprovedTitle;
//        public string CheckboxApprovedTitle
//        {
//            get { return m_checkboxApprovedTitle; }
//            set 
//            {
//                m_checkboxApprovedTitle = value;
//                RaisePropertyChanged (() => CheckboxApprovedTitle);
//            }
//        }
//
//        private string m_checkboxDeniedTitle;
//        public string CheckboxDeniedTitle
//        {
//            get { return m_checkboxDeniedTitle; }
//            set 
//            {
//                m_checkboxDeniedTitle = value;
//                RaisePropertyChanged (() => CheckboxDeniedTitle);
//            }
//        }
//
//        private DetailedTimeOffModel m_detailToShow;
//        public DetailedTimeOffModel DetailToShow
//        {
//            get
//            {
//                return m_detailToShow;
//            }
//
//            set
//            {
//                m_detailToShow = value;
//                RaisePropertyChanged(() => DetailToShow);
//            }
//        }
//
//        private int m_unreadNotificationCount;
//        public int UnreadNotificationCount
//        {
//            get { return m_unreadNotificationCount; }
//            set
//            {
//                m_unreadNotificationCount = value;
//                RaisePropertyChanged (() => UnreadNotificationCount);
//            }
//        }
//
//        private List<CachedTimeOffRequest> m_notificationItems;
//        public List<CachedTimeOffRequest> NotificationItems
//        {
//            get { return m_notificationItems; }
//            set
//            {
//                m_notificationItems = value;
//                RaisePropertyChanged (() => NotificationItems);
//            }
//        }
//
//        private bool m_notificationsLoading;
//        public bool NotificationsLoading
//        {
//            get { return m_notificationsLoading; }
//            set
//            {
//                m_notificationsLoading = value;
//                RaisePropertyChanged (() => NotificationsLoading);
//            }
//        }
//
//        private JDAEmployeeInfo[] m_allEmployees;
//        public JDAEmployeeInfo[] AllEmployees
//        {
//            get { return m_allEmployees; }
//            set
//            {
//                m_allEmployees = value;
//                RaisePropertyChanged (() => AllEmployees);
//            }
//        } 
//
//        private string m_employeesTitle;
//        public string EmployeesTitle
//        {
//            get { return m_employeesTitle; }
//            set 
//            {
//                m_employeesTitle = value;
//                RaisePropertyChanged (() => EmployeesTitle);
//            }
//        }
//
//        private bool m_employeesLoading;
//        public bool LoadingEmployees
//        {
//            get { return m_employeesLoading; }
//            set
//            {
//                m_employeesLoading = value;
//                RaisePropertyChanged (() => LoadingEmployees);
//            }
//        }
//
//        private bool m_loadingEmployeeUpcomingRequests;
//        public bool LoadingEmployeeUpcomingRequests
//        {
//            get { return m_loadingEmployeeUpcomingRequests; }
//            set
//            {
//                m_loadingEmployeeUpcomingRequests = value;
//                RaisePropertyChanged (() => LoadingEmployeeUpcomingRequests);
//            }
//        }
//
//        private JDAEmployeeInfo m_SelectedEmployeeInfo;
//        public JDAEmployeeInfo SelectedEmployeeInfo
//        {
//            get
//            {
//                return m_SelectedEmployeeInfo;
//            }
//            set
//            {
//                m_SelectedEmployeeInfo = value;
//                RaisePropertyChanged(() => SelectedEmployeeInfo);
//            }
//        }
//
//        private List<JDATimeOffRequest> m_employeeUpcomingRequests;
//        public List<JDATimeOffRequest> EmployeeUpcomingRequests
//        {
//            get { return m_employeeUpcomingRequests; }
//            set
//            {
//                m_employeeUpcomingRequests = value;
//                RaisePropertyChanged (() => EmployeeUpcomingRequests);
//            }
//        }
//
//        public bool ShowAllHistoryInPopover
//        {
//            get { return Settings.AppConfig.ShowAllEmployeeHistoryInPopover; }
//        }
//
//        public TimeOffViewModel (SessionData session, 
//            TimeOffService timeOff,
//            EmployeeService employee,
//            ScheduleService schedule,
//            IMvxMessenger messenger,
//            IAlertBox alertService, 
//            TimeOffNotificationService backgroundService)
//        {
//            m_schedule = schedule;
//            m_messenger = messenger;
//            m_timeoff = timeOff;
//            m_employee = employee;
//            m_session = session;
//            m_alert = alertService;
//            m_backgroundTimeOffService = backgroundService;
//        }
//
//        public async Task Init (bool refresh)
//        {
//            m_scheduleReadBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage> (OnScheduleReadBegin);
//            m_scheduleReadEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleReadEnd);
//            m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
//            m_timeOffNotificationToken = m_messenger.SubscribeOnMainThread<TimeOffStatusUpdatedMessage> (OnTimeOffNotificationCountUpdate);
//            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
//
//            FirstOfMonth = new DateTime (DateTime.Now.Year, DateTime.Now.Month, 1);
//            LoadingMessage = Localiser.Get("loading_ellipsis_label");
//            Title = Localiser.Get ("timeoffview_title_label");
//            FilterTitle = Localiser.Get ("timeoffview_filtertitle_label");
//            EmployeesTitle = Localiser.Get ("timeoffview_employeestitle_label");
//
//            CheckboxAllTitle = Localiser.Get ("timeoffview_checkbox_all_label");
//            CheckboxPendingTitle = Localiser.Get ("timeoffview_checkbox_pending_label");
//            CheckboxApprovedTitle = Localiser.Get ("timeoffview_checkbox_approved_label");
//            CheckboxDeniedTitle = Localiser.Get ("timeoffview_checkbox_denied_label");
//
//            if(m_schedule.StartDay != null)
//            {
//                UpdateDate (m_schedule.StartDay);
//            }
//
//            UnreadNotificationCount = m_backgroundTimeOffService.GetPreviousNotificationCount (m_session.CurrentUsername,m_session.Site.SiteID);
//
//            RaisePropertyChanged (() => ShowAllHistoryInPopover);
//
//            await Refresh (true);
//        }
//
//        public async Task LoadNotificationItems()
//        {
//            NotificationsLoading = true;
//
//            Task<BackgroundTimeOffResponse> task = m_backgroundTimeOffService.RetrieveSplitTimeOffRequests (m_session.CurrentUsername, m_session.Site.SiteID, true);
//
//            await Task.Delay (500);
//            await task;
//
//            BackgroundTimeOffResponse response = task.Result;
//
//            if (response != null)
//            {
//                List<CachedTimeOffRequest> seen = response.SeenRequests;
//                List<CachedTimeOffRequest> unseen = response.UnseenRequests;
//                List<CachedTimeOffRequest> merge = new List<CachedTimeOffRequest> ();
//                merge.AddRange (unseen);
//                merge.AddRange (seen);
//
//                NotificationItems = merge;
//            }
//
//            NotificationsLoading = false;
//        }
//
//        public async Task Refresh(bool loadEmployees = false)
//        {
//            if(loadEmployees)
//                LoadingEmployees = true;
//
//            LoadingCount++;
//            LoadingMessage = Localiser.Get("loading_ellipsis_label");
//
//            DateTime firstDay = FirstOfMonth;
//            DateTime startDay = firstDay.AddDays (-6);
//            DateTime endDay = firstDay.AddMonths (1).AddDays (6);
//
//            var response = m_timeoff.GetAllTimeOffRequests (m_session.Site.SiteID, startDay, endDay); 
//            //Make sure the spinner shows for long enough time, don't want it blinking in and out
//            await Task.Delay(500);
//            //Now await the original task which may have already finished
//            await response;
//
//            if (loadEmployees)
//            {
//                var employees = m_employee.GetAllEmployees (m_session.Site.SiteID);
//                await employees;
//                LoadingEmployees = false;
//                AllEmployees = employees.Result;
//            }
//
//            JDATimeOffRequest[] requests = response.Result; 
//
//            if (firstDay != FirstOfMonth)
//            {
//                Mvx.Trace ("Got results for a month we don't care about anymore!");
//            }
//            else
//            {
//                Dictionary<DateTime, TimeOffDayData> days = new Dictionary<DateTime, TimeOffDayData> ();
//                foreach (JDATimeOffRequest request in requests)
//                {
//                    //Zero at the start of the day (some people have holidays that start at different times due to shift work
//                    DateTime start = request.Start.Date;
//                    DateTime end = request.End.Date;
//
//                    if (start < startDay && end < startDay)
//                        continue;
//
//                    if (start > endDay && end > endDay)
//                        continue;
//
//                    //True start and end - The request could start or end outside of this month so trim it off
//                    DateTime trueStart = (start < startDay) ? startDay : start;
//                    DateTime trueEnd = (end > endDay) ? endDay : end;
//
//                    //For each day of the request, add it to the relevant TimeOffDayData, making a new one if it doesn't exist
//                    TimeSpan dur = trueEnd - trueStart;
//                    int totalDays = dur.Days;//0 for 1 day request, 1 for 2, 2 for 3 ect
//
//                    for (int i = 0; i <= totalDays; i++)
//                    {
//                        DateTime startOfDay = trueStart.AddDays (i);
//
//                        if (days.ContainsKey (startOfDay))
//                            days[startOfDay].Data.Add (request);
//                        else
//                            days.Add (startOfDay, new TimeOffDayData (){ Date = startOfDay, Data = new List<JDATimeOffRequest> (){ request } });
//
//                    }
//                }
//
//                AllRequests = days.Values.ToArray ();
//                FilterResults ();
//            }
//
//            LoadingCount--;
//        }
//
//        private void Clear()
//        {
//            AllRequests = null;
//            VisibleRequests = null;
//        }
//
//        private void FilterResults()
//        {
//            if (AllRequests != null)
//            {
//                TimeOffDayData[] visible = new TimeOffDayData[AllRequests.Length];
//                for (int i = 0; i < AllRequests.Length; i++)
//                {
//                    TimeOffDayData day = new TimeOffDayData ();
//                    day.Date = AllRequests[i].Date;
//                    day.Data = ((TimeOffDayData)AllRequests[i]).Data.Where (x => Filter.HasFlag (x.Status)).ToList ();
//                    visible[i] = day;
//                }
//                VisibleRequests = visible;
//            }
//            else
//                VisibleRequests = null;
//        }
//
//        public async Task OnEmployeeSelected(JDAEmployeeInfo info)
//        {
//            if (Loading || LoadingEmployeeUpcomingRequests)
//                return;
//
//            if (Settings.AppConfig.ShowPopoverEmployeeTimeOffView)
//            {
//                LoadingEmployeeUpcomingRequests = true;
//
//                var response = m_timeoff.GetTimeOffHistoryRequest (m_session.Site.SiteID, info);
//                //Make sure the spinner shows for long enough time, don't want it blinking in and out
//                await Task.Delay(500);
//                //Now await the original task which may have already finished
//                await response;
//
//                DateTime now = DateTime.Now.Date;
//                List<JDATimeOffRequest> requests = response.Result;
//                LoadingEmployeeUpcomingRequests = false;
//
//                if(Settings.AppConfig.ShowAllEmployeeHistoryInPopover)
//                    EmployeeUpcomingRequests = requests;
//                else
//                    EmployeeUpcomingRequests = requests.Where (t => (t.Start >= now || t.End >= now)).ToList ();
//
//                SelectedEmployeeInfo = info;
//            }
//            else
//            {
//                Loading = true;
//                LoadingMessage = Localiser.Get("timeoffview_loading_employee_ellipsis_label");
//                DetailToShow = await m_timeoff.GetDetailedEmployeeRequest(m_session.Site.SiteID, info);
//
//                Loading = false;
//                if (DetailToShow == null)
//                {
//                    //Show error
//                    string title = Localiser.Get ("timeoffview_loading_employee_failed_title");
//                    string body = Localiser.Get ("timeoffview_loading_employee_failed_body");
//                    m_alert.ShowOK (title, body);
//                }
//                else
//                {
//                    ShowViewModel<TimeOffDetailsViewModel> (new { detail = DetailToShow });
//                }
//            }
//        }
//
//        public void OnUpcomingRequestTapped(int index)
//        {
//            if (index < 0 || index >= EmployeeUpcomingRequests.Count)
//            {
//                Mvx.Error ("Trying to display upcoming request index out of bounds {0}", index);
//            }
//
//            JDATimeOffRequest request = EmployeeUpcomingRequests[index];
//
//            if(request!=null)
//                ShowTimeOffRequest (request);
//        }
//
//        public void OnNotificationTapped(int index)
//        {
//            if (index < 0 || index >= NotificationItems.Count)
//            {
//                Mvx.Error ("Trying to display notification index out of bounds {0}", index);
//            }
//
//            CachedTimeOffRequest request = NotificationItems[index];
//
//            if(request!=null)
//                ShowTimeOffRequest (request.Request);
//        }
//
//        public async Task OnReturn(string from, JDATimeOffRequest returnRequest)
//        {
//            if (returnRequest != null)
//            {
//                await ShowTimeOffRequest (returnRequest);
//            }
//        }
//
//        public void OnCreateHolidayRequestSelected(JDAEmployeeInfo info)
//        {
//            ShowViewModel<TimeOffCreateViewModel> (new { info = info });
//        }
//
//        public async Task ShowTimeOffRequest(JDATimeOffRequest request)
//        {
//            if (Loading)
//                return;
//
//            Loading = true;
//            LoadingMessage = Localiser.Get("timeoffview_loading_request_ellipsis_label");
//            DetailToShow = await m_timeoff.GetDetailedTimeOffRequest(m_session.Site.SiteID, request);
//
//            Loading = false;
//            if (DetailToShow == null)
//            {
//                //Show error
//                string title = Localiser.Get ("timeoffview_loading_request_failed_title");
//                string body = Localiser.Get ("timeoffview_loading_request_failed_body");
//                m_alert.ShowOK (title, body);
//            }
//            else
//            {
//                ShowViewModel<TimeOffDetailsViewModel> (new { detail = DetailToShow });
//            }
//        }
//
//        public async Task MoveForward()
//        {
//            Clear ();
//            FirstOfMonth = FirstOfMonth.AddMonths (1);
//
//            await Refresh();
//        }
//
//        public async Task MoveBack()
//        {
//            Clear ();
//            FirstOfMonth = FirstOfMonth.AddMonths (-1);
//
//            await Refresh();
//        }
//
//        private void OnScheduleReadBegin(ScheduleReadBeginMessage message)
//        {
//            if(m_schedule.StartDay != null)
//            {
//                UpdateDate (m_schedule.StartDay);
//            }
//        }
//
//        private void OnScheduleReadEnd(ScheduleReadEndMessage message)
//        {
//            if (m_schedule.StartDay != null)
//            {
//                UpdateDate(m_schedule.StartDay);
//            }
//        }
//
//        private void OnOrgEnd(SetOrgEndMessage message)
//        {
//            if (!message.SessionTimeout)
//            {
//                Refresh (true);
//            }
//        }
//
//        private void OnSignOut(SignOutMessage message)
//        {
//            // Unsubscribe from messages
//            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleReadBeginToken);
//            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleReadEndToken);
//            m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
//            m_messenger.Unsubscribe<TimeOffStatusUpdatedMessage> (m_timeOffNotificationToken);
//            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
//        }
//
//        public override void OnViewClosed()
//        {
//            base.OnViewClosed ();
//
//            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleReadBeginToken);
//            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleReadEndToken);
//            m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
//            m_messenger.Unsubscribe<TimeOffStatusUpdatedMessage> (m_timeOffNotificationToken);
//            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
//        }
//
//        private void UpdateDate(int? startDay)
//        {
//            // Update days in segments
//            if(startDay != null && startDay != m_currentStartDay)
//            {
//                startDay %= 7;
//                m_currentStartDay = (int) startDay;
//                List<string> daySegments = new List<string>();
//                for(int i = 0; i < 7; i++)
//                {
//                    switch(startDay)
//                    {
//                        case 0:
//                            daySegments.Add(Localiser.Get("saturday_short"));
//                            break;
//                        case 1:
//                            daySegments.Add(Localiser.Get("sunday_short"));
//                            break;
//                        case 2:
//                            daySegments.Add(Localiser.Get("monday_short"));
//                            break;
//                        case 3:
//                            daySegments.Add(Localiser.Get("tuesday_short"));
//                            break;
//                        case 4:
//                            daySegments.Add(Localiser.Get("wednesday_short"));
//                            break;
//                        case 5:
//                            daySegments.Add(Localiser.Get("thursday_short"));
//                            break;
//                        case 6:
//                            daySegments.Add(Localiser.Get("friday_short"));
//                            break;
//                    }
//                    startDay++;
//                    startDay %= 7;
//                }
//                Segments = daySegments;
//            }
//        }
//
//        private void OnTimeOffNotificationCountUpdate(TimeOffStatusUpdatedMessage message)
//        {
//            UnreadNotificationCount = message.UnseenTimeOffNotifications;
//        }
//
//        public void OnDaySelected(CalendarDay dayData)
//        {
//            if (dayData != null && dayData.DataContext != null && (dayData.DataContext as TimeOffDayData) != null)
//            {
//                ShowViewModel<TimeOffCalendarDayViewModel> (new { data = dayData.DataContext as TimeOffDayData });
//            }
//            else
//            {
//                Mvx.Error ("Tapped on day with no data");
//            }
//        }
//
//
//        public void OnReturn(TimeOffDetailsViewModel.OperationOnClose operation)
//        {
//            if (operation == TimeOffDetailsViewModel.OperationOnClose.ForceRefresh)
//            {
//                this.Refresh (false);
//            }
//        }
//    }
//}
//
