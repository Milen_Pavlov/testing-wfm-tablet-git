﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace WFM.Core
{
    //Update MEssages for VM -> VM communication (TabView doesn't seem happy if you reshow the current tab)
    public class CreateTimeOffAllocationsMessage : MvxMessage
    {
        public List<PayAllocation> Changes { get; set; }

        public CreateTimeOffAllocationsMessage (object sender) : base (sender)
        {
        }
    }

    public class CreateTimeOffPeersMessage : MvxMessage
    {
        public List<PeerRequest> Changes { get; set; }

        public CreateTimeOffPeersMessage (object sender) : base (sender)
        {
        }
    }

    public class TimeOffCreateViewModel : TimeOffBaseDetailViewModel
    {
        public enum DateTimePopoverMode
        {
            StartDate,
            EndDate,
            StartTime,
            EndTime,
        }

        private readonly TimeOffService m_timeOffService;
        private readonly SessionData m_sessionData;
        private readonly IAlertBox m_alertBox;
        private readonly ISpinnerService m_spinner;
        private readonly IToastService m_toastService;
        private readonly EmployeeService m_employeeService;
        private readonly ScheduleService m_schedule;
        private readonly ITimeOffRequestBlackoutOverrideService m_blackoutOverrideService;

        private int EmployeeID
        {
            get
            {
                return TimeOffModel.EmployeeInfo.EmployeeID;
            }
        }

        private JDATimeOffType[] m_TimeOffTypes;
        public JDATimeOffType[] TimeOffTypes
        {
            get
            {
                return m_TimeOffTypes;
            }
            set
            {
                m_TimeOffTypes = value;
                RaisePropertyChanged (() => TimeOffTypes);
            }
        }

        private bool m_isAllDay = true;
        public bool IsAllDay
        {
            get { return m_isAllDay; }
            set
            {
                m_isAllDay = value;
                RaisePropertyChanged (() => IsAllDay);
            }
        }

        private string m_comment;
        public string Comment
        {
            get { return m_comment; }
            set
            {
                m_comment = value;
                RaisePropertyChanged (() => Comment);
            }
        }

        private JDATimeOffType m_currentType;
        public JDATimeOffType CurrentType
        {
            get { return m_currentType; }
            set
            {
                m_currentType = value;
                RaisePropertyChanged (() => CurrentType);
            }
        }

        private DateTime m_startTime;
        public DateTime StartTime
        {
            get { return m_startTime; }
            set
            {
                m_startTime = value;
                RaisePropertyChanged (() => StartTime);
            }
        }

        private DateTime m_endTime;
        public DateTime EndTime
        {
            get { return m_endTime; }
            set
            {
                m_endTime = value;
                RaisePropertyChanged (() => EndTime);
            }
        }

        private bool m_updating;
        public bool Updating
        {
            get { return m_updating; }
            set
            {
                m_updating = value;
                RaisePropertyChanged (() => Updating);
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set
            {
                m_loading = value;
                RaisePropertyChanged (() => Loading);
            }
        }

        private DateTimePopoverMode m_popoverMode = DateTimePopoverMode.StartDate;
        public DateTimePopoverMode PopoverMode
        {
            get { return m_popoverMode; }
            set { m_popoverMode = value; }
        }

        private bool AllowBlackoutOverride
        {
            get; 
            set;
        }

        public TimeOffCreateViewModel (
            EmployeeService employeeService, 
            TimeOffService timeOffService, 
            SessionData sessionData, 
            IAlertBox alertBox, 
            ISpinnerService spinner, 
            IToastService toast, 
            IMvxMessenger messenger, 
            ScheduleService schedule,
            IESSConfigurationService configurationService,
            ITimeOffRequestBlackoutOverrideService blackoutOverrideService
            ) : base(messenger)
        {
            m_timeOffService = timeOffService;
            m_sessionData = sessionData;
            m_alertBox = alertBox;
            m_spinner = spinner;
            m_toastService = toast;
            m_employeeService = employeeService;
            m_schedule = schedule;
            m_blackoutOverrideService = blackoutOverrideService;
        }

        public async void Init(int employeeId, DateTime startTime, DateTime endTime)
        {
            Loading = true;

            //Thanks JDA. Shifts only have employeeIds, I need the whole employee, and you can only ask for all.
            var employees = m_employeeService.GetAllEmployees (m_sessionData.Site.SiteID);
            await employees;

            if (employees.Result == null || employees.Result.Length == 0)
            {
                m_alertBox.ShowOK ("Error", "Something went wrong attempting to get employee information");
            }
            else
            {
                JDAEmployeeInfo info = null;
                foreach (JDAEmployeeInfo emp in employees.Result)
                {
                    if (emp.EmployeeID == employeeId)
                        info = emp;
                }

                if (info == null)
                {
                    m_alertBox.ShowOK ("Error", "Something went wrong attempting to get employee information");
                }
                else
                {
                    IsAllDay = false;
                    Init (info, startTime, endTime);
                }
            }
        }
        public async void Init(JDAEmployeeInfo info)
        {
            Init (info, DateTime.Now.Date, DateTime.Now.Date.Add (new TimeSpan (23, 59, 00)));
        }

        public async void Init(JDAEmployeeInfo info, DateTime startTime, DateTime endTime)
        {
            PendingAllocationChanges.Clear ();

            Loading = true;

            //Set initial holiday times (this can also then be used to request holiday information)
            StartTime = startTime;
            EndTime = endTime;
            Comment = string.Empty;

            TimeOffModel = new DetailedTimeOffModel (null, null, null, null, null, null, info) { EmployeeViewOnly = false };

            BuildTabTitles (0);

            //Get Employee History and Accruals, this only needs to happen once as they won't change while editing
            var accruals = await m_timeOffService.GetAccruals(DateTime.Now, EmployeeID);
            TimeOffModel.Accruals.Clear ();
            foreach (var accrual in accruals)
                TimeOffModel.Accruals.Add (new Accrual (accrual));

            var history = await m_timeOffService.GetHistory (m_sessionData.Site.SiteID, EmployeeID, DateTime.Now);
            TimeOffModel.History.Clear ();
            foreach (var request in history)
            {
                if(request!=null && request.EmployeeID == EmployeeID && request.Status != TimeOffStatus.Deleted)
                    TimeOffModel.History.Add(new TimeOffHistory(request));
            }
                
            //Get Request Types
            var typeResult = await m_timeOffService.GetTimeOffTypesForEmployee (EmployeeID);
            if (typeResult.Success)
            {
                TimeOffTypes = typeResult.Object.ToArray();

                if (TimeOffTypes.Length > 0)
                    CurrentType = TimeOffTypes[0];
            }

            //Update the views that will change based on Start/End and Type
            await UpdateAllocationsAndPeers ();

            BuildTabTitles (0, false);

            Loading = false;
        }

        private async Task UpdateAllocationsAndPeers()
        {
            if (CurrentType == null || Updating)
                return;

            Updating = true;

            SpinnerToken spinner = null;
            if (!Loading)
            {
                spinner = m_spinner.Show ("Updating...");
            }

            var allocationsTask = m_timeOffService.GetNewTimeOffDetails(m_sessionData.Site.SiteID, EmployeeID, StartTime, EndTime, CurrentType.TypeID);
            var peersTask = m_timeOffService.GetPeerRequests(m_sessionData.Site.SiteID, EmployeeID, StartTime, EndTime);
            var minDelay = Task.Delay (500);

            //Get pay allocations
            await allocationsTask;
            await peersTask;
            await minDelay;

            List<JDATimeOffDetail> details = allocationsTask.Result;
            TimeOffModel.PayAllocations.Clear ();
            foreach (JDATimeOffDetail detail in details)
            {
                TimeOffModel.PayAllocations.Add (new PayAllocation (detail));
            }

            TimeOffModel.PeerRequests = peersTask.Result;

            //Notify things to update
            m_messenger.Publish<AllocationsUpdatedMessage>( new AllocationsUpdatedMessage(this) { Changes = new List<PayAllocation>() });
            m_messenger.Publish<CreateTimeOffAllocationsMessage> (new CreateTimeOffAllocationsMessage (this) { Changes = TimeOffModel.PayAllocations } );
            m_messenger.Publish<CreateTimeOffPeersMessage> (new CreateTimeOffPeersMessage (this) { Changes = TimeOffModel.PeerRequests } );

            //Refresh TabTitles (Peer Requests change this)
            BuildTabTitles (TimeOffModel.PeerRequests.Count, false);

            if(spinner!=null)
                m_spinner.Dismiss (spinner);

            Updating = false;
        }

        public void OnAllDaySelected()
        {
            IsAllDay = true;
        }

        public void OnPartialDaySelected()
        {
            IsAllDay = false;
        }

        public void OnTimeOffTypeSelected(int index)
        {
            if (CurrentType != TimeOffTypes[index])
            {
                CurrentType = TimeOffTypes[index];
                UpdateAllocationsAndPeers ();
            }
        }

        public void OnDateTimeSelected(DateTime dateTime)
        {
            switch (PopoverMode)
            {
                case DateTimePopoverMode.StartDate:
                    StartTime = dateTime.Date + StartTime.TimeOfDay;
                    break;
                case DateTimePopoverMode.EndDate:
                    EndTime = dateTime.Date + EndTime.TimeOfDay;
                    break;
                case DateTimePopoverMode.StartTime:
                    StartTime = StartTime.Date + dateTime.TimeOfDay;
                    break;
                case DateTimePopoverMode.EndTime:
                    EndTime = EndTime.Date + dateTime.TimeOfDay;
                    break;
            }

            UpdateAllocationsAndPeers ();
        }

        public async void SubmitRequest()
        {
            //Sanity check some obvious errors
            if (CurrentType == null)
            {
                m_alertBox.ShowOK ("Failed", "You must select a valid request type for this time off request");
                return;
            }

            if (StartTime == EndTime)
            {
                m_alertBox.ShowOK ("Failed", "The start and end date of the request must be different");
                return;
            }

            if (EndTime < EndTime)
            {
                m_alertBox.ShowOK ("Failed", "The end date cannot occur before the start date");
                return;
            }

            if (m_schedule.IsWithinPreventChangesPeriod (StartTime))
            {
                m_alertBox.ShowOK (Localiser.Get("prevent_changes_title"), String.Format(Localiser.Get("prevent_changes_message"), ConfigurationService.PreventChangesHours));
                return;
            }

            SpinnerToken spin = m_spinner.Show ("Submitting...");

            var submitTask = m_timeOffService.CreateAndApproveHoliday (m_sessionData.Site.SiteID, EmployeeID, StartTime, EndTime, CurrentType.TypeID, Comment, PendingAllocationChanges, AllowBlackoutOverride);

            await Task.Delay (500);
            await submitTask;

            //Reset temporary settings
            AllowBlackoutOverride = false;

            //Check success (TODO Handle Error Codes Nicely!!!!)
            bool errorParseSuccess = true;
            m_spinner.Dismiss (spin);

            if(CheckForErrorCodesAgainstTimeOffTypeNames(submitTask.Result))
            {
                return;
            }

            if (submitTask.Result.Success)
            {
                m_toastService.Show ("Request Approved");
                //Tell schedule (if needed) to refresh
                m_messenger.Publish<ScheduleRefreshMessage> (new ScheduleRefreshMessage (this));
                m_messenger.Publish<TimeOffRefreshMessage> (new TimeOffRefreshMessage (this));
                //Close
                Close ();
            }
            else if (submitTask.Result.Code == 6)// "TimeOffRequestCannotOverlapExistingTimeOffRequestForSingleDay")
            {
                m_alertBox.ShowOK ("Conflict", Localiser.Get("timeoff_request_overlaps_with_blackout"));
            }
            else if (submitTask.Result.Code == 8 || submitTask.Result.Code == 9)// "TimeOffRequestCannotOverlapExistingTimeOffRequestForSingleDay" or "TimeOffRequestCannotOverlapExistingTimeOffRequestForMultipleDay"  
            {
                m_alertBox.ShowOK ("Conflict", Localiser.Get("timeoff_request_overlaps_with_blackout"));
            }
            else if (submitTask.Result.Code == 19)// "TimeOffRequestDetailHoursCannotBeGreaterThanTwentyFour")
            {
                m_alertBox.ShowOK ("Failed", Localiser.Get("timeoff_request_pay_allocation"));
            }
            else if (submitTask.Result.Code == 17)// "MinTimeInAdvanceForSelf")
            {
                //Get min time
                string args = submitTask.Result.Object.Errors[0].Arguments.ToString ();
                string requiredDays = GetArgValueClass<string> (args, "RequiredDays", ref errorParseSuccess);

                string errorBody = "Time off requests for yourself cannot be made too close to current date.";
                if (errorParseSuccess)
                    errorBody = string.Format (Localiser.Get("timeoff_request_occurence"), requiredDays);

                m_alertBox.ShowOK ("Failed", errorBody);
            }
            else if (submitTask.Result.Code == 10)// "TimeOffBlackoutPeriodOverlap"
            {
                ShowBlackoutPeriodErrorPopup(submitTask.Result.Object.Errors[0]);
            }
            else
            {
                m_alertBox.ShowOK ("Failed", string.Format ("Something went wrong submitting the time off request. {0}", submitTask.Result.Message));
            }
        }

        private bool CheckForErrorCodesAgainstTimeOffTypeNames (Result<JDAProcessingResult> result)
        {
            if(result.Success)
            {
                return false;
            }

            if(result.Object.Errors.Any())
            {
                bool errorParseSuccess = true;
                var error = result.Object.Errors.First();

                if(string.Equals(error.Type, "TimeOffRequestCannotOverlapExistingTimeOffRequestForSingleDay", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_alertBox.ShowOK ("Conflict", Localiser.Get("timeoff_request_overlaps_with_blackout"));

                    return true;
                }
                else if(string.Equals(error.Type, "TimeOffRequestCannotOverlapExistingTimeOffRequestForSingleDay", StringComparison.CurrentCultureIgnoreCase) || 
                        string.Equals(error.Type, "TimeOffRequestCannotOverlapExistingTimeOffRequestForMultipleDay", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_alertBox.ShowOK ("Conflict", Localiser.Get("timeoff_request_overlaps_with_blackout"));

                    return true;
                }
                else if (string.Equals(error.Type, "TimeOffRequestDetailHoursCannotBeGreaterThanTwentyFour", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_alertBox.ShowOK ("Failed", Localiser.Get("timeoff_request_pay_allocation"));

                    return true;
                }
                else if (string.Equals(error.Type, "MinTimeInAdvanceForSelf", StringComparison.CurrentCultureIgnoreCase))
                {
                    //Get min time
                    string args = result.Object.Errors[0].Arguments.ToString ();
                    string requiredDays = GetArgValueClass<string> (args, "RequiredDays", ref errorParseSuccess);

                    string errorBody = "Time off requests for yourself cannot be made too close to current date.";
                    if (errorParseSuccess)
                        errorBody = string.Format (Localiser.Get("timeoff_request_occurence"), requiredDays);

                    m_alertBox.ShowOK ("Failed", errorBody);

                    return true;
                }
                else if( string.Equals(error.Type, "TimeOffBlackoutPeriodOverlap", StringComparison.CurrentCultureIgnoreCase) || 
                        string.Equals(error.Type, "TimeOffBlackoutPeriodOverlapForSelf", StringComparison.CurrentCultureIgnoreCase))
                {
                    ShowBlackoutPeriodErrorPopup(result.Object.Errors[0]);
                    return true;
                }
            }

            return false;
        }

        private void ShowBlackoutPeriodErrorPopup(JDAProcessingResult.ErrorOrWarning error)
        {
            bool errorParseSuccess = true;

            string args = error.Arguments.ToString ();
            string blackoutName = GetArgValueClass<string> (args, "Name", ref errorParseSuccess);
            DateTime blackoutStart = GetArgValueStruct<DateTime> (args, "Start", ref errorParseSuccess);
            DateTime blackoutEnd = GetArgValueStruct<DateTime> (args, "End", ref errorParseSuccess);

            if(m_blackoutOverrideService.CanOverrideBlackoutPeriod(ConfigurationService.CanOverrideBlackoutPeriod, ConfigurationService.TimeOffTypesOverridingBlackoutPeriod, CurrentType.TypeName))
            {
                string errorBody = "The time off request overlaps a blackout period. Allow Time Off Request anyway?";
                if (errorParseSuccess)
                    errorBody = string.Format (Localiser.Get("timeoff_request_overlaps_with_blackout_confirmation"), blackoutName, blackoutStart.ToString(Localiser.Get("dd/MM/yyyy")), blackoutEnd.ToString(Localiser.Get("dd/MM/yyyy")));
                ShowOverrideBlackoutPeriodDialog(errorBody);
            }
            else
            {
                string errorBody = "The time off request overlaps a blackout period.";
                if (errorParseSuccess)
                    errorBody = string.Format (Localiser.Get("timeoff_request_overlaps_with_blackout_noconfirmation"), blackoutName, blackoutStart.ToString(Localiser.Get("dd/MM/yyyy")), blackoutEnd.ToString(Localiser.Get("dd/MM/yyyy")));
                m_alertBox.ShowOK(Localiser.Get("timeoff_request_conflict"), errorBody);                 
            }
        }

        private void ShowOverrideBlackoutPeriodDialog(string errorBody)
        {
            m_alertBox.ShowYesNo (Localiser.Get("timeoff_request_conflict"), errorBody, (i) =>
            {
                if(i == 1)
                {
                    //Resubmit with override
                    AllowBlackoutOverride = true;
                    SubmitRequest();
                }
            });
        }

        private T GetArgValueStruct<T>(string args, string name, ref bool success) where T : struct
        {
            //Json serialize
            JContainer argsObj = JsonConvert.DeserializeObject<JContainer> (args);
            T obj = new T();
            try
            {
                foreach (JToken token in argsObj)
                {
                    if (token.Path == name)
                    {
                        var valToken = token.Value<JProperty> ();
                        obj = valToken.Value.ToObject<T>();
                    }
                }

            }
            catch (Exception e)
            {
                Mvx.Trace ("Error parsing JSON {0}", e.Message);
                success = false;
            }

            return obj;
        }

        private T GetArgValueClass<T>(string args, string name, ref bool success) where T : class
        {
            //Json serialize
            JContainer argsObj = JsonConvert.DeserializeObject<JContainer> (args);
            T obj = null;
            try
            {
                foreach (JToken token in argsObj)
                {
                    if (token.Path == name)
                    {
                        var valToken = token.Value<JProperty> ();
                        obj = valToken.Value.ToObject<T>();
                    }
                }

            }
            catch (Exception e)
            {
                Mvx.Trace ("Error parsing JSON {0}", e.Message);
                success = false;
            }

            return obj;
        }
    }
}