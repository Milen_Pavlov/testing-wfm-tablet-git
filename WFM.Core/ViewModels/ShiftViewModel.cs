﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Consortium.Client.Core.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Core.ViewModels
{
    public class ShiftViewModel : WFMViewModel
    {
        public class Parameters
        {
            public int EmployeeId { get; set; }
            public int ShiftId { get; set; }
        }

        private ScheduleService m_service;
        private EmployeeData m_employee;

        private ShiftData m_shift;
        private ShiftData m_previousShiftState;

        public DateTime ShiftDate { get; set; }
        public string FullName { get; set; }
        public IList<ShiftItemViewModel> ShiftItems { get; set; }
        public IList<JobViewModel> AllowableJobs { get; set; }
        public bool Editable { get; set; }

        public string ShiftDescription
        {
            get
            {
                var dateAsString = ShiftDate.ToString("dd/MM/yyyy");
                var shiftLength = (ShiftEnd - ShiftStart);
                var shiftLengthHours = (int)shiftLength.TotalHours;
                var shiftLengthMinutes = shiftLength.Minutes.ToString("00");
                return string.Format(@"{0} - {1}:{2} {3}", dateAsString, shiftLengthHours, shiftLengthMinutes, Localiser.Get("hours_label"));
            }
        }

        public DateTime ShiftStart
        {
            get
            {
                if (ShiftItems == null)
                {
                    return ShiftDate;
                }

                var jobItems = ShiftItems.Where(i => i.JobOrBreakOrMeal == JobOrBreakOrMeal.Job).ToList();
                if (!jobItems.Any())
                {
                    return ShiftDate;
                }

                return jobItems.Min(i => i.Start);
            }
        }

        public DateTime ShiftEnd
        {
            get
            {
                if (ShiftItems == null)
                {
                    return ShiftDate;
                }

                var jobItems = ShiftItems.Where(i => i.JobOrBreakOrMeal == JobOrBreakOrMeal.Job).ToList();
                if (!jobItems.Any())
                {
                    return ShiftDate;
                }

                return jobItems.Max(i => i.End);
            }
        }

        public ShiftViewModel(ScheduleService service)
        {
            m_service = service;
        }

        public void Init(Parameters parameters)
        {
            m_employee = m_service.Schedule.Employees.Where(x => x.EmployeeID == parameters.EmployeeId).FirstOrDefault();
            m_shift = m_employee.GetShift(parameters.ShiftId);

            m_previousShiftState = m_shift.Copy();

            Editable = m_employee.CanEdit;

            // Set properties
            AllowableJobs = m_employee.AllowableJobs.Select(j => new JobViewModel(j)).ToList();
            ShiftDate = m_shift.Start.Date;
            var firstName = (string.IsNullOrEmpty(m_employee.Info.NickName) ? m_employee.Info.FirstName : m_employee.Info.NickName);
            FullName = string.Format("{0}, {1}", m_employee.Info.LastName, firstName);

            // Set shift items
            var jobs = m_shift.Jobs.Select(j => new ShiftItemViewModel(this, j)).ToList();
            var breaks = m_shift.Jobs.SelectMany(j => j.Breaks).Select(b => new ShiftItemViewModel(this, b)).ToList();
            var meals = m_shift.Jobs.SelectMany(j => j.Meals).Select(m => new ShiftItemViewModel(this, m)).ToList();
            ShiftItems = jobs.Union(breaks).Union(meals).ToList();
        }

        public ShiftData ToShift()
        {
            // Note: Important that jobs are added before breaks and meals
            var orderedShiftItems = ShiftItems.OrderBy(i => (i.JobOrBreakOrMeal == JobOrBreakOrMeal.Job ? 0 : 1)).ToList();

            var shift = new ShiftData()
            {
                Start = ShiftStart,
                End = ShiftEnd,
            };

            foreach (var shiftItem in orderedShiftItems)
            {
                var start = shiftItem.Start;
                var end = shiftItem.End;

                if (shiftItem.JobOrBreakOrMeal == JobOrBreakOrMeal.Job)
                {
                    var job = JobData.Create();
                    job.Edit = EditState.None; // Prevent red border on shift control
                    job.JobID = int.Parse(shiftItem.TypeId);
                    job.Start = start;
                    job.End = end;
                    job.JobColor = shiftItem.Colour;
                    job.JobName = shiftItem.Name;

                    shift.Jobs.Add(job);
                }
                else if (shiftItem.JobOrBreakOrMeal == JobOrBreakOrMeal.BreakOrMeal && shiftItem.TypeId == ShiftData.c_breakID)
                {
                    var shiftDetail = new ShiftDetailData()
                    {
                        Start = start,
                        End = end,
                    };

                    shift.AddBreak(shiftDetail);
                }
                else if (shiftItem.JobOrBreakOrMeal == JobOrBreakOrMeal.BreakOrMeal && shiftItem.TypeId == ShiftData.c_mealID)
                {
                    var shiftDetail = new ShiftDetailData()
                    {
                        Start = start,
                        End = end,
                    };

                    shift.AddMeal(shiftDetail);
                }
                else
                {
                    throw new NotSupportedException();
                }
            }

            return shift;
        }

        public ShiftItemViewModel CreateShiftItem(JobOrBreakOrMeal jobOrBreakOrMeal)
        {
            var shiftItemId = new ShiftItemId(0, Guid.NewGuid().ToString(), 0, jobOrBreakOrMeal);
            var shiftItem = new ShiftViewModel.ShiftItemViewModel(this, shiftItemId);
            ShiftItems.Add(shiftItem);
            return shiftItem;
        }

        public void DeleteShiftItem(ShiftItemId shiftItemId)
        {
            var shift = ShiftItems.SingleOrDefault(s => s.ShiftItemId == shiftItemId);
            if (shift != null)
            {
                ShiftItems.Remove(shift);
            }
        }

        public void Cancel()
        {
            // Nothing to do
        }

        public void Save()
        {
            // Always delete all breaks and meals for the shift
            m_shift.DeleteAllBreaks();
            m_shift.DeleteAllMeals();

            // Update all jobs
            List<int> activeJobIds = new List<int>();
            foreach (var shiftItem in ShiftItems)
            {
                if (shiftItem.JobOrBreakOrMeal == JobOrBreakOrMeal.Job)
                {
                    activeJobIds.Add(shiftItem.ShiftItemId.Id);

                    var job = m_shift.Jobs.Find(j => j.ScheduledJobID == shiftItem.ShiftItemId.ScheduledId);
                    if (job != null)
                    {
                        job.Resize(shiftItem.Start, shiftItem.End);
                    }
                    else
                    {
                        var newJob = JobData.Create();
                        newJob.Resize(shiftItem.Start, shiftItem.End);
                        newJob.JobID = int.Parse(shiftItem.TypeId);
                        newJob.JobColor = shiftItem.Colour;
                        newJob.JobName = shiftItem.Name;
                        m_shift.Jobs.Add(newJob);
                    }
                }
            }

            // Delete required jobs
            List<int> jobsToDelete = new List<int>();
            foreach (var job in m_shift.Jobs)
            {
                if (!activeJobIds.Contains(job.JobID))
                {
                    jobsToDelete.Add(job.JobID);
                }
            }

            foreach (int jobId in jobsToDelete)
            {
                m_shift.DeleteJob(jobId);
            }

            // Shrink to fit
            m_shift.ShrinkToFit();

            foreach (var shiftItem in ShiftItems)
            {
                if (shiftItem.JobOrBreakOrMeal == JobOrBreakOrMeal.BreakOrMeal)
                {
                    var detail = ShiftDetailData.Create();
                    detail.Start = shiftItem.Start;
                    detail.End = shiftItem.End;

                    if (shiftItem.TypeId == ShiftData.c_breakID)
                    {
                        detail.PunchCode = ShiftData.c_breakID;
                        m_shift.AddBreak(detail);
                    }
                    else if (shiftItem.TypeId == ShiftData.c_mealID)
                    {
                        detail.PunchCode = ShiftData.c_mealID;
                        m_shift.AddMeal(detail);
                    }
                    else
                        throw new NotImplementedException();
                }
            }

            var message = new ShiftEditMessage(this)
            {
                UpdateShift = m_shift,
                Op = ShiftEditMessage.Operation.UpdateShift,
                EmployeeID = m_employee.EmployeeID,
                RecalculateDetails = false,
                PreviousShiftState = m_previousShiftState
            };

            Mvx.Resolve<IMvxMessenger>().Publish(message);
        }

        public class ShiftItemViewModel
        {
            public ShiftViewModel Shift { get; set; }
            public ShiftItemId ShiftItemId { get; set; }
            public string TypeId { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public string Colour { get; set; }
            public string Name { get; set; }

            private ShiftItemViewModel(ShiftViewModel shift)
            {
                Shift = shift;
                Start = End = shift.ShiftStart;
            }

            public ShiftItemViewModel(ShiftViewModel shift, ShiftItemId shiftItemId)
                : this(shift)
            {
                ShiftItemId = shiftItemId;

                if (JobOrBreakOrMeal == JobOrBreakOrMeal.Job && Shift.AllowableJobs != null && Shift.AllowableJobs.Any())
                {
                    TypeId = Shift.AllowableJobs.First().JobId.ToString();
                }
                else if (JobOrBreakOrMeal == JobOrBreakOrMeal.BreakOrMeal)
                {
                    TypeId = ShiftData.c_breakID;
                }

                Normalise();
            }

            public ShiftItemViewModel(ShiftViewModel shift, JobData job)
                : this(shift)
            {
                ShiftItemId = new ShiftItemId(job.JobID, job.JobName, job.ScheduledJobID, JobOrBreakOrMeal.Job);
                TypeId = job.JobID.ToString();
                Start = job.Start;
                End = job.End;

                Normalise();
            }

            public ShiftItemViewModel(ShiftViewModel shift, ShiftDetailData shiftDetail)
                : this(shift)
            {
                ShiftItemId = new ShiftItemId(shiftDetail.ShiftDetailID, null, 0, JobOrBreakOrMeal.BreakOrMeal); // Is this unique?
                TypeId = shiftDetail.PunchCode;
                
                if (shiftDetail.Start != null)
                {
                    Start = shiftDetail.Start.Value;
                }
                if (shiftDetail.End != null)
                {
                    End = shiftDetail.End.Value;
                }

                Normalise();
            }

            public JobOrBreakOrMeal JobOrBreakOrMeal
            {
                get
                {
                    return ShiftItemId.JobOrBreakOrMeal;
                }
            }

            public void TypeChanged(string typeId)
            {
                TypeId = typeId;
                Normalise();
            }

            public void StartChanged(TimeSpan start)
            {
                Start = Shift.ShiftDate.Add(start);
                Normalise();

            }

            public void EndChanged(TimeSpan end)
            {
                End = Shift.ShiftDate.Add(end);
                Normalise();
            }

            public void Normalise()
            {
                Start = Shift.ShiftDate.Add(Start.TimeOfDay);
                End = Shift.ShiftDate.Add(End.TimeOfDay);

                if (Start > End)
                {
                    End = End.AddDays(1);
                }

                if (JobOrBreakOrMeal == JobOrBreakOrMeal.Job && Shift.AllowableJobs != null && Shift.AllowableJobs.Any())
                {
                    var allowableJob = Shift.AllowableJobs.SingleOrDefault(j => j.JobId == int.Parse(TypeId));
                    if (allowableJob != null)
                    {
                        Name = allowableJob.JobName;
                        Colour = allowableJob.JobColour;
                    }
                }
            }
        }

        public class ShiftItemId
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int ScheduledId { get; set; }
            public JobOrBreakOrMeal JobOrBreakOrMeal { get; set; }

            public ShiftItemId(int id, string name, int scheduledId, JobOrBreakOrMeal jobOrBreakOrMeal)
            {
                Id = id;
                Name = name;
                ScheduledId = scheduledId;
                JobOrBreakOrMeal = jobOrBreakOrMeal;
            }

            public static bool operator ==(ShiftItemId value1, ShiftItemId value2)
            {
                return (value1.Id == value2.Id && value1.Name == value2.Name && value1.ScheduledId == value2.ScheduledId && value1.JobOrBreakOrMeal == value2.JobOrBreakOrMeal);
            }

            public static bool operator !=(ShiftItemId value1, ShiftItemId value2)
            {
                return !(value1 == value2);
            }
        }

        public enum JobOrBreakOrMeal
        {
            Job,
            BreakOrMeal,
        }

        public class JobViewModel
        {
            public int JobId { get; set; }
            public string JobName { get; set; }
            public string JobColour { get; set; }

            public JobViewModel(JobData job)
            {
                JobId = job.JobID;
                JobName = job.JobName;
                JobColour = job.JobColor;
            }
        }
    }
}