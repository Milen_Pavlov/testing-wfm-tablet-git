using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Newtonsoft.Json;
using System.Threading.Tasks;
using WFM.Core.Schedule;

namespace WFM.Core
{
    public class ScheduleGridViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Schedule";
            }
        }

        private readonly ISessionData m_session;
        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;
        private readonly IAlertBox m_alertBox;
        private readonly SpareShiftService m_spareShiftService;
        private readonly ICallOffService m_callOffService;
        private readonly SessionData m_sessionData;
        private readonly IToastService m_toastService;
        private readonly ISpinnerService m_spinner;
        private readonly IESSConfigurationService m_essConfig;


        private bool m_viewModelShown;

        protected MvxSubscriptionToken m_scheduleBeginToken;
        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;
        protected MvxSubscriptionToken m_signOutToken;
        protected MvxSubscriptionToken m_headerEditUpdateToken;

        private SortState m_nameSort;
        public SortState NameSort
        {
            get { return m_nameSort; }
            set { m_nameSort = value; RaisePropertyChanged(() => NameSort); }
        }

        private SortState m_hourSort;
        public SortState HourSort
        {
            get { return m_hourSort; }
            set { m_hourSort = value; RaisePropertyChanged(() => HourSort); }
        }

        private ScheduleData m_schedule;
        public ScheduleData Schedule
        {
            get { return m_schedule; }
            set 
            { 
                if(value != null)
                {
                    m_schedule = value; 
                    RaisePropertyChanged(() => Schedule); 
                    RaisePropertyChanged(() => Employees); 
                }
            }
        }

        public List<EmployeeData> Employees
        {
            get 
            { 
                if (Schedule == null)
                    return null;
              
                bool showUnfilledShifts = false;
                List<EmployeeData> employees = Schedule.FilteredEmployees;
                if (Schedule.CurrentFilter != null)
                {
                    var filteredItems = Schedule.CurrentFilter.Items.Where (x => x.Selected == true).ToList();

                    if(filteredItems != null && filteredItems.Count == 1)
                    {
                        List<EmployeeData> unfilledShifts = m_spareShiftService.GetSpareShifts(filteredItems[0].ID, filteredItems[0].Name, Schedule.StartDate, Schedule.DayIndex);
                        employees.AddRange(unfilledShifts);
                        showUnfilledShifts = true;

                    }
                }
                ShowSpareShifts = showUnfilledShifts;
                return employees;
            }

            set 
            {
                RaisePropertyChanged(() => Employees); 
            }
        }

        private List<ColumnHeader> m_headers;
        public List<ColumnHeader> Headers
        {
            get { return m_headers; }
            set { m_headers = value; RaisePropertyChanged(() => Headers); }
        }

        private string m_totalHours;
        public string TotalHours
        {
            get { return m_totalHours; }
            set { m_totalHours = value; RaisePropertyChanged(() => TotalHours); }
        }

        private string m_totalDemand;
        public string TotalDemand
        {
            get { return m_totalDemand; }
            set { m_totalDemand = value; RaisePropertyChanged(() => TotalDemand); }
        }

        public string TotalHoursHeader
        {
            get { return Localiser.Get("hours_label"); }
            set { RaisePropertyChanged(() => TotalHoursHeader); }
        }

        public string TotalDemandHeader
        {
            get { return Localiser.Get("demand_label"); }
            set { RaisePropertyChanged(() => TotalDemandHeader); }
        }

        private int m_scrollToEmployeeAtIndex = 0;
        public int ScrollToEmployeeAtIndex
        {
            get { return m_scrollToEmployeeAtIndex; }
            set
            {
                m_scrollToEmployeeAtIndex = value; RaisePropertyChanged(() => ScrollToEmployeeAtIndex);
            }
        }

        private bool m_isWeekView = true;
        public bool IsWeekView
        {
            get { return m_isWeekView; }
            set
            {
                m_isWeekView = value;
                RaisePropertyChanged (() => IsWeekView);
            }
        }

        private bool m_showSpareShifts = false;
        public bool ShowSpareShifts
        {
            get { return m_showSpareShifts; }
            set
            {
                m_showSpareShifts = value;
                if (m_showSpareShifts == true)
                {
                    m_showSpareShifts = ConfigurationService.ShowSpareShifts;
                }
                RaisePropertyChanged (() => ShowSpareShifts);
            }
        }

        private string m_employeeNameTitle;
        public string EmployeeNameTitle
        {
            get { return m_employeeNameTitle; }
            set { m_employeeNameTitle = value; RaisePropertyChanged(() => EmployeeNameTitle); }
        }

        public ScheduleGridViewModel (ISessionData session,
                                      ScheduleService schedule,
                                      IMvxMessenger messenger,
                                      IAlertBox alertBox,
                                      SpareShiftService spareShiftService,
                                      SessionData sessionData,
                                      ICallOffService callOffService,
                                      IToastService toastService,
                                      ISpinnerService spinner,
                                      IESSConfigurationService essConfig)
        {
            m_session = session;
            m_scheduleService = schedule;
            m_messenger = messenger;
            m_spareShiftService = spareShiftService;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_nameSort = new SortState ();
            m_hourSort = new SortState ();
            m_alertBox = alertBox;
            m_viewModelShown = false;
            m_sessionData = sessionData;
            m_callOffService = callOffService;
            m_toastService = toastService;
            m_spinner = spinner;
            m_essConfig = essConfig;
        }

        public async Task OnReturn(int employeeId, DateTime startTime, DateTime endTime, bool autoBookTimeOff, string timeOffType, bool paidAbsence)
        {
            m_viewModelShown = false;
            
            if(!autoBookTimeOff)
            {
                await ShowCreateTimeOff(employeeId, startTime, endTime);   
            }
            else 
            {
                CallOffShiftAutomatically (employeeId, startTime, endTime, timeOffType, paidAbsence);
            }
        }

        public async Task OnReturn()
        {
            m_viewModelShown = false;
        }

        public async Task OnReturn(bool needsRefresh)
        {
            m_viewModelShown = false;
            
            if (needsRefresh)
            {
                m_scheduleService.Refresh (false);
                m_spareShiftService.Refresh (false, m_schedule.CurrentFilter);
            }
        }

        public async Task ShowCreateTimeOff(int employeeId, DateTime startTime, DateTime endTime)
        {
            await Task.Delay(100);

            ShowViewModel<TimeOffCreateViewModel> (new {
                employeeId = employeeId,
                startTime = startTime,
                endTime = endTime,
            });
        }

        public void Init ()
        {
            m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage> (OnScheduleBegin);
            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage> (OnScheduleUpdate);
            m_headerEditUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleRulerMessageUpdate> (OnRulerMessageUpdate);

            EmployeeNameTitle = Localiser.Get ("employee_name_label");
            Update();
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
            m_messenger.Unsubscribe<ScheduleRulerMessageUpdate>(m_headerEditUpdateToken);
        }

        public void OnNameHeaderSelect()
        {
            // Sort by name
            m_scheduleService.SortName ();
        }

        public void OnHourHeaderSelect()
        {
            // Sort by hour
            m_scheduleService.SortHour ();
        }

        public void OnHeaderSelect(ColumnHeader header)
        {
            // Sort by start time
            m_scheduleService.SortEarliestStart(header.Index);
        }

        public void OnEmployeeNameSelected(JDAEmployeeInfo employeeInfo)
        {
            var data = JsonConvert.SerializeObject (employeeInfo);
            ShowViewModel<EmployeeInfoViewModel> (new { info = data });
        }

        public void OnShiftSelected(ShiftData shift)
        {
            if(!m_viewModelShown)
            {
                string shiftErrors = GetShowShiftErrors(shift);
                if(string.IsNullOrEmpty(shiftErrors) == false)
                {
                    m_alertBox.ShowOK (Localiser.Get("shift_view_error_title"), shiftErrors);
                    return;
                }

                m_viewModelShown = true;
                ShowViewModel<ShiftViewModel>(new { employeeId = shift.EmployeeID, shiftId = shift.ShiftID });
            }
        }

        private string GetShowShiftErrors(ShiftData shift)
        {
            if (m_scheduleService.Schedule == null)
            {
                return string.Format (Localiser.Get("shift_view_error_not_found"), shift.ShiftID);
            }

            var employee = m_scheduleService.Schedule.GetEmployee(shift.EmployeeID.GetValueOrDefault());
            if (employee == null)
            {
                employee = m_spareShiftService.GetEmployee (shift.EmployeeID.GetValueOrDefault());
            }

            if (employee == null)
            {
                return string.Format (Localiser.Get("shift_view_error_employee_not_found"), shift.ShiftID);
            }

            var dayIndex = m_scheduleService.DayIndex;
            if(employee.CheckIfShiftOnPreviousWeek(shift.ShiftID, dayIndex))
            {
                return Localiser.Get("shift_view_error_previous_day");
            }

            var mainShift = employee.GetShift(shift.ShiftID);
            if (mainShift == null)
            {
                return string.Format (Localiser.Get("shift_view_error_not_found"), shift.ShiftID);
            }

            return string.Empty;
        }

        public void OnWarningSelected(List<string> warnings)
        {
            var data = JsonConvert.SerializeObject(warnings);
            ShowViewModel<EmployeeWarningsViewModel>(new { info = data });
        }

        public void OnTimeOffSelected(TimeOffData timeOffData)
        {
            if(m_viewModelShown == true)
                return;
            
            //Show Time Off ViewModel here.
            m_viewModelShown = true;
            ShowViewModel<ScheduleTimeOffViewModel>( new { employeeId = timeOffData.Employee.EmployeeID, dayStart = timeOffData.Day } );
        }

        public void OnAddShiftToWeek(WeekItemData week)
        {
            //get the last possible shift time, add after that
            DateTime centre;

            var startNew = week.Day.Shifts.OrderBy(p => p.End).ToList().LastOrDefault();

            if (startNew != null)
            {
                centre = startNew.End.AddHours (ScheduleService.c_defaultShiftLength/2);
            }
            else
            {
                centre = week.Day.DayStart.AddHours(12);
            }

            // check this shift will not go over to the next day, make number of hours less magic
            DateTime added = centre.AddHours(ScheduleService.c_defaultShiftLength / 2);
            bool overlapsToNextDay = !week.Day.DayStart.Date.Equals(added.Date);

            if (!overlapsToNextDay)
            {
                var request = new ShiftEditMessage (this)
                    {
                        Op = ShiftEditMessage.Operation.CreateDefaultShift,
                        EmployeeID = week.EmployeeId,
                        Start = centre,
                        End = centre
                    };

                m_messenger.Publish(request);
            }
            else
            {
                m_alertBox.ShowOK("Add Shift Failed", "No room to create shift at this location");
            }
        }

        private void Update()
        {
            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            UpdateSchedule();
            UpdateTotals();
            UpdateHeaders();
        }

        private void UpdateSchedule(int? scrollToEmployeeId = null)
        {
            Schedule = m_scheduleService.Schedule;

            if (scrollToEmployeeId != null)
            {
                EmployeeData targetEmployee = this.Employees.FirstOrDefault(employee => employee.EmployeeID == scrollToEmployeeId);

                if (targetEmployee != null)
                    ScrollToEmployeeAtIndex = this.Employees.IndexOf(targetEmployee);
            }
        }

        private void UpdateTotals()
        {
            TotalDemand = m_scheduleService.Accuracy != null ? (m_scheduleService.Accuracy.WeekDemand / m_essConfig.TotalHoursPerDayDivisor).ToString("n2") : string.Empty;
            TotalHours = m_scheduleService.Accuracy != null ? (m_scheduleService.Schedule.WeekScheduled / m_essConfig.TotalHoursPerDayDivisor).ToString("n2") : string.Empty;
        }

        private void UpdateHeaders()
        {
            NameSort = new SortState () {
                IsSorting = m_scheduleService.SortIndex == ScheduleData.c_sortName,
                IsAscending = m_scheduleService.SortNameAscending
            };

            HourSort = new SortState () {
                IsSorting = m_scheduleService.SortIndex == ScheduleData.c_sortHour,
                IsAscending = m_scheduleService.SortHourAscending
            };
                    
            var headers = new List<ColumnHeader> ();

            if (Schedule.DayIndex != null)
            {
                int index = (int) m_scheduleService.DayIndex;
                var date = m_scheduleService.StartDate.AddDays (index).ToString (Localiser.Get("dddd dd/MM"));

                var dayDemand = m_scheduleService.Accuracy == null ? 0 : m_scheduleService.Accuracy.DayDemand[m_scheduleService.DayIndex.Value];

                var header = new ColumnHeader () {
                    Index = index,
                    StartHour = m_scheduleService.StartDate.Hour,
                    Title = string.Format("{0}, {3} {1:0.00}, {4} {2:0.00}", 
                        date, 
                        Schedule.DayScheduled[m_scheduleService.DayIndex.Value] / m_essConfig.TotalHoursPerDayDivisor, 
                        dayDemand / m_essConfig.TotalHoursPerDayDivisor,
                        Localiser.Get("hours_label"),
                        Localiser.Get("demand_label")),
                    Sort = new SortState () {
                        IsSorting = index == m_scheduleService.SortIndex,
                        IsAscending = m_scheduleService.SortEarliestStartAscending
                    }
                };

                headers.Add (header);
            }
            else
            {
                for (int i = 0; i < 7; ++i)
                {
                    var dayDemand = m_scheduleService.Accuracy == null ? 0 : m_scheduleService.Accuracy.DayDemand[i];
                    var header = new ColumnHeader () {
                        Index = i,
                        Title = m_scheduleService.StartDate.AddDays (i).ToString (Localiser.Get("ddd dd/MM")),
                        Title1 = Localiser.Get("hours_label"),
                        Value1 = (Schedule.DayScheduled[i] / m_essConfig.TotalHoursPerDayDivisor).ToString ("n2"),
                        Title2 = Localiser.Get("demand_label"),
                        Value2 = (dayDemand / m_essConfig.TotalHoursPerDayDivisor).ToString ("n2"),
                        Sort = new SortState () {
                            IsSorting = i == m_scheduleService.SortIndex,
                            IsAscending = m_scheduleService.SortEarliestStartAscending
                        }
                    };

                    headers.Add (header);
                }
            }

            Headers = headers;
        }

        private void UpdateWarnings ()
        {
            RaisePropertyChanged(() => Employees);
        }

        private void OnScheduleBegin(ScheduleReadBeginMessage message)
        {
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            Update();
        }

        private void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            if (message.UpdateSchedule)
                UpdateSchedule (message.ScrollToEmployeeId);

            if (message.UpdateTotals)
                UpdateTotals ();

            if (message.UpdateHeaders)
                UpdateHeaders ();

            if(message.UpdateWarnings)
                UpdateWarnings ();
        }

        private void OnSignOut(SignOutMessage message)
        {
            Schedule = null;
        }

        private void OnRulerMessageUpdate(ScheduleRulerMessageUpdate message)
        {
            if(Headers != null && Headers.Count > 0)
            {
                var header = Headers.FirstOrDefault ();
                header.Title = string.Format("Editing Shift: Start: {0}, End {1}", message.AdjustedStart.ToString(Localiser.Get("dd/MM/yyyy HH:mm")), message.AdjustedEnd.ToString(Localiser.Get("dd/MM/yyyy HH:mm")));
            }
        }

        public bool CanShowSpareShifts()
        {
            return ConfigurationService.ShowSpareShifts;
        }

        void CallOffShiftAutomatically (int employeeId, DateTime startTime, DateTime endTime, string timeOffTypeName, bool paidAbsence)
        {
            //Attached to the end of every message to distinguish between paid and unpaid
            string paidStatusMessageSuffix = (paidAbsence) ? "" : "_unpaid" ;
                        
            var employee = m_schedule.GetEmployee(employeeId);
            string employeeName = (employee == null || String.IsNullOrEmpty(employee.Name)) ? Localiser.Get("calloff_confirm_message_employee") : employee.Name;
            var token = m_alertBox.ShowYesNo(Localiser.Get ("calloff_confirm_title"+paidStatusMessageSuffix),string.Format(Localiser.Get ("calloff_confirm_message"+paidStatusMessageSuffix),startTime,employeeName),async (selection) => 
            {
                if(selection == 1)
                {
                    var spinnerToken = m_spinner.Show(Localiser.Get("calloff_spinner_message" + paidStatusMessageSuffix));
                
                    //WFM-1955, shifts that finish at midnight shouldn't raise holiday for the next day - setting daterange includeend to false
                    //discards the day if ends at midnight, but includes if goes over midnight. Handy!
                    //var callOffPeriod = new DateRange (startTime.Date, endTime, TimeSpan.FromDays (1), true, false);
        
                    var result = await m_callOffService.CallOffShift (m_sessionData.Site.SiteID, employeeId, startTime,endTime, timeOffTypeName).ConfigureAwait (true);
        
                    m_spinner.Dismiss (spinnerToken);
        
                    if (result.Success) 
                    {
                        m_toastService.Show (Localiser.Get ("calloff_success_toast"+paidStatusMessageSuffix));
        
                        m_scheduleService.Refresh (false);
                        m_spareShiftService.Refresh (false, m_schedule.CurrentFilter);
                    } 
                    else 
                    {
                        m_alertBox.ShowOK (Localiser.Get ("calloff_failed_title"+paidStatusMessageSuffix), result.Message);
                    }
                }
            });
        }
    }
}

