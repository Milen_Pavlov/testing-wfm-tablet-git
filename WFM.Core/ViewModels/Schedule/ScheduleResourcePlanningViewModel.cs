﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;
using AutoMapper;
using System.ComponentModel;
using Cirrious.CrossCore;
using Newtonsoft.Json;
using System.Globalization;

namespace WFM.Core
{
    public class ScheduleResourcePlanningViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Resources";
            }
        }

        private static readonly string [] EXTERNALLY_FUNDED_DEPARTMENT_NAMES = new string [] { "Cleaning & Maintenance", "Phone Shop", "Security" };

        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;
        private readonly ScheduleOvertimeService m_overtimeService;
        private readonly ResourceDataService m_resourceDataService;
        private readonly IAlertBox m_alert;
        private readonly ILabourDemandService m_labourDemandService;
        private readonly SessionData m_sessionData;
        private readonly ITimeOffService m_timeOffService;
        private readonly IESSConfigurationService m_settingsService;


        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;

        private SortState m_currentSortState = null;

        private ResourcePlanningData m_lockedData = null;

        private ResourcePlanningData DataAtLockedState
        {
            get
            {
                return m_lockedData;
            }

            set
            {
                m_lockedData = value;
                RaisePropertyChanged(() => CanLock);
                RaisePropertyChanged(() => ShowLockDataButton);
            }
        }

        private DateTime m_LockedDate;

        public DateTime LockedDate
        {
            get
            {
                return m_LockedDate;
            }

            set
            {
                m_LockedDate = value;
                RaisePropertyChanged(() => LockedDate);
                RaisePropertyChanged(() => LockedDateText);
            }
        }

        public string LockedDateText
        {
            get
            {
                return string.Format(Localiser.Get("planning_header_lockmessage"));
            }
            set
            {
                RaisePropertyChanged(() => LockedDateText);
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get{ return m_loading; }
            set
            {
                m_loading = value;
                RaisePropertyChanged(() => Loading);
                RaisePropertyChanged(() => NoData);
            }
        } 

        private string m_loadingMessage;
        public string NoDataMessage
        {
            get{ return m_loadingMessage; }
            set
            {
                m_loadingMessage = value;
                RaisePropertyChanged(() => NoDataMessage);
            }
        } 

        public bool ShowLockDataButton
        {
            get
            {
                if (ConfigurationService.CanLockResources == false)
                    return false;
                return DataAtLockedState == null;
            }
            set
            {
                RaisePropertyChanged(() => ShowLockDataButton);
            }
        }

        public bool CanLock
        {
            get
            {
                return DataAtLockedState == null;
            }
            set
            {
                RaisePropertyChanged(() => CanLock);
            }
        }
            

        private bool m_noData;
        public bool NoData
        {
            get{ return m_noData || Loading; }
            set
            {
                m_noData = value;
                RaisePropertyChanged(() => NoData);
                RaisePropertyChanged(() => Loading);
            }
        }

        private SortState m_DepartmentNameSortState = new SortState();

        public SortState DepartmentNameSortState
        {
            get
            {
                return m_DepartmentNameSortState;
            }
            set
            {
                m_DepartmentNameSortState = value;
                RaisePropertyChanged(() => DepartmentNameSortState);
            }
        }

		private SortState m_TimeOffSortState = new SortState();

		public SortState TimeOffSortState
		{
			get
			{
				return m_TimeOffSortState;
			}
			set
			{
				m_TimeOffSortState = value;
				RaisePropertyChanged(() => TimeOffSortState);
			}
		}
            
        private SortState m_PlannedHoursSortState = new SortState();

        public SortState PlannedHoursSortState
        {
            get
            {
                return m_PlannedHoursSortState;
            }
            set
            {
                m_PlannedHoursSortState = value;
                RaisePropertyChanged(() => PlannedHoursSortState);
            }
        }

        private SortState m_ScheduledHoursSortState = new SortState();

        public SortState ScheduledHoursSortState
        {
            get
            {
                return m_ScheduledHoursSortState;
            }
            set
            {
                m_ScheduledHoursSortState = value;
                RaisePropertyChanged(() => ScheduledHoursSortState);
            }
        }
            
        private SortState m_OvertimeRequiredSortState = new SortState();

        public SortState OvertimeRequiredSortState
        {
            get
            {
                return m_OvertimeRequiredSortState;
            }
            set
            {
                m_OvertimeRequiredSortState = value;
                RaisePropertyChanged(() => OvertimeRequiredSortState);
            }
        }

        private SortState m_OvertimeAgreedSortState = new SortState();

        public SortState OvertimeAgreedSortState
        {
            get
            {
                return m_OvertimeAgreedSortState;
            }
            set
            {
                m_OvertimeAgreedSortState = value;
                RaisePropertyChanged(() => OvertimeAgreedSortState);
            }
        }

		private SortState m_ContractedSortState = new SortState();

		public SortState ContractedSortState
		{
			get
			{
				return m_ContractedSortState;
			}
			set
			{
				m_ContractedSortState = value;
				RaisePropertyChanged(() => ContractedSortState);
			}
		}

        private bool m_showSetOvertimeAgreed;

        public bool ShowOvertimeAgreed
        {
            get
            {
                return m_showSetOvertimeAgreed;
            }
            set
            {
                m_showSetOvertimeAgreed = value;
                RaisePropertyChanged(() => ShowOvertimeAgreed);
            }
        }

        private string m_OvertimeAgreed;

        public string OvertimeAgreedForSite
        {
            get
            {
                return m_OvertimeAgreed;
            }
            set
            {
                m_OvertimeAgreed = value;
                RaisePropertyChanged(() => OvertimeAgreedForSite);
            }
        }

        public bool HasResourceRows
        {
            get
            {
                return m_rows.Count > 0;
            }
            set
            {
                RaisePropertyChanged(() => HasResourceRows);
            }
        }

        private ResourcePlanningItem m_TotalRow;

        public ResourcePlanningItem TotalRow
        {
            get
            {
                return m_TotalRow;
            }
            set
            {
                m_TotalRow = value;
                RaisePropertyChanged(() => TotalRow);
            }
        }

        private IList<OvertimeRecord> m_records = new List<OvertimeRecord>();
        private List<ResourcePlanningItem> m_rows = new List<ResourcePlanningItem>();
        private JDATimeOffRequest[] m_timeOffData = new JDATimeOffRequest[0];

        public List<ResourcePlanningItem> Rows
        {
            get { return m_rows; }
            set 
            { 
                m_rows = value; 
                RaisePropertyChanged(() => Rows); 
                RaisePropertyChanged(() => HasResourceRows);
            }
        }

        private string m_totalHours;
        public string TotalHours
        {
            get { return m_totalHours; }
            set
            {
                m_totalHours = String.Format(Localiser.Get("planning_affordablehours"), value);
                RaisePropertyChanged (() => TotalHours);
            }
        }

        private string m_adjustedDemandHours;
        public string AdjustedDemandHours
        {
            get { return m_adjustedDemandHours; }
            set
            {
                m_adjustedDemandHours = string.Format(Localiser.Get("adjusted_demandhours"), value);
                RaisePropertyChanged (() => AdjustedDemandHours);
            }
        }

        private decimal m_absenceTargetFactor;
        public decimal AbsenceTargetFactor
        {
            get
            {
                return m_absenceTargetFactor;
            }
            set
            {
                m_absenceTargetFactor = value;
                RaisePropertyChanged(() => AbsenceTargetFactor);
            }
        }

        public double ContractedHours { get; private set; }
        public double PlannedTimeOff { get; private set; }
        public double UnplannedTimeOff { get; private set; }
        public double DemandHours { get; private set; }

        public ScheduleResourcePlanningViewModel (
            ScheduleService schedule,
            IMvxMessenger messenger, 
            ScheduleOvertimeService overtimeService,
            ResourceDataService resourceDataService,
            IAlertBox alert,
            ILabourDemandService labourDemandService,
            SessionData sessionData,
            ITimeOffService timeOffService, 
            IESSConfigurationService settingsService)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;
            m_overtimeService = overtimeService;

            DepartmentNameSortState.IsSorting = true;
            DepartmentNameSortState.IsAscending = true;
            m_currentSortState = DepartmentNameSortState;
            m_resourceDataService = resourceDataService;
            m_alert = alert;
            m_labourDemandService = labourDemandService;
            m_sessionData = sessionData;
            m_timeOffService = timeOffService;
            m_settingsService = settingsService;
        }

        public void Init()
        {
            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage> (OnScheduleUpdate);
            LoadData(false);
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            m_overtimeService.Cancel();

            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
        }

        public async Task ReadAbsenceTargetPercentageAsync()
        {
            int year = m_scheduleService.AccountingHelper.GetStartOfAccountingYear(m_scheduleService.StartDate.Date).Year;
            int week = m_scheduleService.AccountingHelper.GetWeekNumberForDate(m_scheduleService.StartDate.Date);
            var labourDemandResult = await m_labourDemandService.GetAbsenceTargetAsPercentage(m_sessionData.Site.Name, year.ToString() + week.ToString());

            if(labourDemandResult.Success)
            {
                if(labourDemandResult.Object != null &&
                   labourDemandResult.Object.StoreAbsenceTargets != null &&
                   labourDemandResult.Object.StoreAbsenceTargets.AbsenceTargets != null)
                {
                    AbsenceTargetFactor = labourDemandResult.Object.StoreAbsenceTargets.AbsenceTargets.AbsenceTargetAsFactor;
                }
                else
                {
                    AbsenceTargetFactor =  default(decimal);
                }
            }
            else
            {
                AbsenceTargetFactor = default(decimal);
            }
        }

        /// <summary>
        /// Loads the data by retrieving it from the server
        /// </summary>
        /// <param name="forceUpdate">If set to <c>true</c> force the service to retrieve new data, othewise just return the current state.</param>
        private async void LoadData(bool forceUpdate)
        {
            NoDataMessage = "Loading overtime records...";
            Loading = true;

            DataAtLockedState = null;

            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            var readDepartmentoperation = m_overtimeService.ReadDepartmentOvertime(m_scheduleService.StartDate.Date, forceUpdate);
            var lockCheckOperation = m_resourceDataService.GetLockedData(m_scheduleService.StartDate.Date);
            var readOvertimeOperation = m_resourceDataService.ReadOvertimeAgreedValue(m_scheduleService.StartDate.Date);
            var holidayReadOperation = m_timeOffService.GetAllTimeOffRequests(m_sessionData.Site.SiteID,m_scheduleService.StartDate.Date,m_scheduleService.StartDate.AddDays(7));
            
            //Comfort break
            await Task.Delay(ConstantValues.c_comfortBreakMillis);

            m_records = await readDepartmentoperation;
            m_timeOffData = await holidayReadOperation;
            
            var lockCheckResult = await lockCheckOperation;
            var readOvertimeResult = await readOvertimeOperation;

            if(!lockCheckResult.Success || m_records == null || !readOvertimeResult.Success || m_timeOffData == null)
            {
                NoData = true;
                NoDataMessage = "Data transfer failed. Please try again.";
                return;
            }
            else
            {
                NoData = false;

                if (lockCheckResult.Object != null)
                {
                    DataAtLockedState = lockCheckResult.Object;
                    LockedDate = lockCheckResult.Object.LockedDate;
                }

                OvertimeAgreedForSite = readOvertimeResult.Object == null ? string.Empty : readOvertimeResult.Object.Value.ToString();

                await ReadAbsenceTargetPercentageAsync();
            }

            UpdateDisplay();

            Loading = false;
        }

        private void UpdateDisplay()
        {
            if (m_scheduleService.Schedule == null)
                Rows = new List<ResourcePlanningItem> ();
            else
                Rows = GeneratePlanningRows(m_scheduleService.Schedule.DayIndex, m_records);

            TotalHours = CalculateTotalPlannedHours (m_records).ToString();

            AdjustedDemandHours = CalculateAdjustedDemandHours();

            if (m_scheduleService.Schedule != null && m_scheduleService.Schedule.DayIndex == null)
            {
                ShowOvertimeAgreed = true;
            }
            else
            {
                ShowOvertimeAgreed = false;
            }

            CalculateTotalPlannedHours (m_records);
            ApplyCurrentSortState();
        }

        private string CalculateAdjustedDemandHours ()
        {
            var storeAbsenceTarget = ContractedHours * (double)AbsenceTargetFactor;
            
            if(storeAbsenceTarget >= UnplannedTimeOff)
            {
                return DemandHours.ToString();
            }
            else
            {
                var result =  NearestQuarterConverter.RoundToQuarter ((DemandHours - (UnplannedTimeOff - storeAbsenceTarget)));

                return result.ToString();
            }
        }

        private double CalculateTotalPlannedHours(IList<OvertimeRecord> overtimeRecords)
        {
            if (overtimeRecords == null)
                return 0;

            List<ResourcePlanningItem> rows = GeneratePlanningRows (null, overtimeRecords);

            ContractedHours = rows.Sum (row => row.ContractedHours);
            PlannedTimeOff = rows.Sum (row => row.PlannedTimeOff);
            UnplannedTimeOff = rows.Sum(row => row.UnplannedTimeOff);
            DemandHours = rows.Sum(row => row.PlannedHours);
            double overtimeAgreedDay = rows.Sum (row => row.OvertimeAgreedDay.GetValueOrDefault ());
            double overtimeAgreedNight = rows.Sum(row => row.OvertimeAgreedNight.GetValueOrDefault());

            double val = (ContractedHours - PlannedTimeOff + overtimeAgreedDay + overtimeAgreedNight);
            return NearestQuarterConverter.RoundToQuarter (val);
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            //Schedule end means the schedule
            LoadData(true);
        }

        private void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            //Revert changes is passed in as a flag to tell the vm to wipe its current state and refresh data. For us this just means we need to perform a fresh update from the service
            //And overwrite whatever changes the user has made up to this point.
            if (message.RevertChanges)
                LoadData(true);
            else
                UpdateDisplay();
        }

        public async void LockData()
        {
            if (m_scheduleService.CurrentFilter != null)
            {
                m_alert.ShowOK(Localiser.Get("planning_confirmlock_errortitle"), Localiser.Get("planning_confirmlock_errormessage"));
            }
            else
            {
                m_alert.ShowYesNo(Localiser.Get("planning_confirmlock_title"), Localiser.Get("planning_confirmlock_message"), (int action) =>
                    {
                        if (action == 1)
                            SetLockedState();
                    });
            }
        }

        public async void UnlockData()
        {
            if (m_scheduleService.CurrentFilter != null)
            {
                m_alert.ShowOK(Localiser.Get("planning_confirmlock_errortitle"), Localiser.Get("planning_confirmlock_errormessage"));
            }
            else
            {
                m_alert.ShowYesNo(Localiser.Get("planning_confirmlock_title"), Localiser.Get("planning_confirmlock_message"), async (int action) =>
                    {
                        if (action == 1)
                        {
                            var lockResult = await m_resourceDataService.UnsetLockedData(m_scheduleService.StartDate.Date);
                            m_alert.ShowOK(lockResult.Success.ToString(),"Happy");
                        }
                    });
            }
        }

        public async void SetLockedState()
        {
            Loading = true;

            //Check for filter -> error if filtered, check if not already locked

            ResourcePlanningData data = new ResourcePlanningData();

            PrimaryJobRoleFilterGroup roleFilter = m_scheduleService.Filters.Groups.Find(group => group.Type == PrimaryJobRoleFilterGroup.PRIMARYJOBROLE_FILTER_CODE) as PrimaryJobRoleFilterGroup;

            if (roleFilter != null && m_scheduleService.Accuracy != null)
            {
                foreach (PlanningData planningData in m_scheduleService.Accuracy.Planning)
                {       
                    int jobType = roleFilter.GetJobTypeForWorkgroup(planningData.ID);

                    for(int i = 0; i< 7; i++)
                    {
                        var rowEntry = BuildRowEntry(planningData,i, null, jobType);

                        if (!data.Days[i].ContainsKey(jobType))
                            data.Days[i].Add(jobType, rowEntry);    
                    }
                }
            }

            var result = await m_resourceDataService.SetLockedData(m_scheduleService.StartDate.Date, data);

            if (result.Success)
            {
                DataAtLockedState = data;
            }

            Loading = false;
        }

        public void ShowAudit()
        {
            ShowViewModel<OvertimeAuditViewModel>();
        }

        /// <summary>
        /// From the records available to us, build the list of row items we are going to display to the user
        /// </summary>
        /// <returns>The rows that form the contents of the collection control.</returns>
        /// <param name="dayIndex">Index of the day to build the data for.</param>
        /// <param name="overtime">The list of overtime data to merge with the data available from the schedule service.</param>
        private List<ResourcePlanningItem> GeneratePlanningRows(int? dayIndex, IList<OvertimeRecord> overtimeRecords)
        {
            List<ResourcePlanningItem> rows = CreatePlanningRows (dayIndex, overtimeRecords);

            TotalRow = new ResourcePlanningItem()
                {
                    Department = "Total",
                    ForDate = m_scheduleService.StartDate,
                    ContractedHours = Rows.Sum(row => row.ContractedHours),
                    PlannedTimeOff = Rows.Sum(row => row.PlannedTimeOff),
                    UnplannedTimeOff = Rows.Sum(row => row.UnplannedTimeOff),
                    PlannedHours = Rows.Sum(row => row.PlannedHours),
                    ScheduledHours = Rows.Sum(row => row.ScheduledHours),
                    UnfilledHours = Rows.Sum(row => row.UnfilledHours),
                    OvertimeAgreedDay = Rows.Sum(row => row.OvertimeAgreedDay.GetValueOrDefault()),
                    OvertimeAgreedNight = Rows.Sum(row => row.OvertimeAgreedNight.GetValueOrDefault()),
                    OvertimeRequestedDay = Rows.Sum(row => row.OvertimeRequestedDay.GetValueOrDefault()) ,
                    OvertimeRequestedNight = Rows.Sum(row => row.OvertimeRequestedNight.GetValueOrDefault()),
                    IsWeekCell = dayIndex == null
                };

            return rows;
        }

        private List<ResourcePlanningItem> CreatePlanningRows(int? dayIndex, IList<OvertimeRecord> overtimeRecords)
        {
            List<ResourcePlanningItem> rows = new List<ResourcePlanningItem>();

            PrimaryJobRoleFilterGroup roleFilter = null;

            if(m_scheduleService.Filters!=null)
                roleFilter = m_scheduleService.Filters.Groups.Find(group => group.Type == PrimaryJobRoleFilterGroup.PRIMARYJOBROLE_FILTER_CODE) as PrimaryJobRoleFilterGroup;

            IEnumerable<PlanningData> internallyFundedDepartments = m_scheduleService.Accuracy.Planning.Where(IsDepartmentInternallyFunded);

            if (roleFilter != null && m_scheduleService.Accuracy != null)
            {
                foreach (PlanningData planningData in internallyFundedDepartments)
                {       
                    int jobType = roleFilter.GetJobTypeForWorkgroup(planningData.ID);

                    var rowEntry = BuildRowEntry(planningData, dayIndex, overtimeRecords, jobType, DataAtLockedState);

                    if (dayIndex == null && overtimeRecords != null)
                    {
                        var overtimeRecord = overtimeRecords.FirstOrDefault(record => record.JDADepartmentId == planningData.ID);

                        if (overtimeRecord != null)
                        {
                            rowEntry.OvertimeAgreedDay = overtimeRecord.AllocatedHours;
                            rowEntry.OvertimeAgreedNight = overtimeRecord.ResourceAgreedNight;
                            rowEntry.OvertimeRequestedDay = overtimeRecord.ResourceRequestDay;
                            rowEntry.OvertimeRequestedNight = overtimeRecord.ResourceRequestNight;
                        }

                        rowEntry.DirtyFields = m_overtimeService.GetDirtyFieldFlags(planningData.ID);
                    }

                    rowEntry.OnUpdated = OnHoursEdited;

                    rows.Add(rowEntry);
                }
            }

            return rows;
        }

        ResourcePlanningItem BuildRowEntry(PlanningData planningData, int? dayIndex, IList<OvertimeRecord> overtime, int jobType, ResourcePlanningData m_loadFromData = null)
        {
            ResourcePlanningItem rowEntry;

            if (dayIndex == null)
            {
                rowEntry = new ResourcePlanningItem();

                for (int i = 0; i < 7; i++)
                {
                    ResourcePlanningItem incrementalItem;

                    if (m_loadFromData != null)
                    {
                        incrementalItem = BuildRowFromData(i, jobType, m_loadFromData);
                    }
                    else
                    {
                        incrementalItem = CreateRowItemForDay(planningData, i, jobType);
                    }

                    rowEntry.ContractedHours += incrementalItem.ContractedHours;
                    rowEntry.PlannedTimeOff += incrementalItem.PlannedTimeOff;
                    rowEntry.UnplannedTimeOff += incrementalItem.UnplannedTimeOff;
                    rowEntry.PlannedHours += incrementalItem.PlannedHours;
                    rowEntry.ScheduledHours += incrementalItem.ScheduledHours;
                    rowEntry.UnfilledHours += incrementalItem.UnfilledHours;
                }

                rowEntry.IsWeekCell = true;

                bool exceedsRange = planningData.IsThereAnOverOfLength(dayIndex, 8);
                rowEntry.ShouldFlag = exceedsRange && ((rowEntry.UnfilledHours) < 0);
            }
            else
            {
                if (m_loadFromData != null)
                {
                    rowEntry = BuildRowFromData(dayIndex.Value, jobType, m_loadFromData);
                }
                else
                {
                    rowEntry = CreateRowItemForDay(planningData, dayIndex.Value, jobType);
                }

                rowEntry.IsWeekCell = false;
            }

            rowEntry.DepartmentId = planningData.ID;
            rowEntry.Department = planningData.Name;
            rowEntry.ForDate = m_scheduleService.StartDate;

            return rowEntry;
        }

        ResourcePlanningItem CreateRowItemForDay(PlanningData planningData, int dayIndex, int jobType)
        {
            ResourcePlanningItem rowEntry = new ResourcePlanningItem();
            
            double planned = (planningData.DayDemand[dayIndex] / 4);
            double scheduled = m_scheduleService.Schedule.GetHoursForJobType(dayIndex, jobType);
             
            
            double contractedHours = m_scheduleService.Schedule.GetContractedHoursForJobType(dayIndex, jobType);
            double plannedTimeOff = m_scheduleService.Schedule.GetContractedTimeOffHoursForJobType(dayIndex, jobType);
            
            var unplannedTimeOffRequests = m_timeOffData.Where(timeOff => timeOff.Status == TimeOffStatus.Approved && timeOff.PayAdjustmentCategoryName != m_settingsService.ExcludedUnplannedTimeOffType);
            
            double unplannedTimeOff = m_scheduleService.Schedule.GetContractedTimeOffHoursForJobType(dayIndex,jobType, unplannedTimeOffRequests);
            
            double plannedHours = planned;
            double scheduledHours = scheduled;
            double unfilledHours = planned - scheduled;

            bool exceedsRange = planningData.IsThereAnUnderOfLength(dayIndex, 8);

            rowEntry.ContractedHours = contractedHours;
            rowEntry.PlannedTimeOff = plannedTimeOff;
            rowEntry.UnplannedTimeOff = unplannedTimeOff;
            rowEntry.PlannedHours = plannedHours;
            rowEntry.ScheduledHours = scheduledHours;
            rowEntry.UnfilledHours = unfilledHours;
            rowEntry.ShouldFlag = exceedsRange && ((plannedHours - scheduledHours) < 0);

            return rowEntry;
        }
            
        ResourcePlanningItem BuildRowFromData(int dayIndex, int jobType, ResourcePlanningData m_loadFromData)
        {
            if (m_loadFromData.Days[dayIndex].ContainsKey(jobType))
                return new ResourcePlanningItem(m_loadFromData.Days[dayIndex][jobType]);
            else
                return new ResourcePlanningItem();
        }

        /// <summary>
        /// When the user has edited the hours in a row, handle the change with this callback.
        /// </summary>
        /// <param name="updated">The row that has been updated.</param>
        public void OnHoursEdited(ResourcePlanningItem updated, ResourcePlanningItem.OvertimeField fieldUpdated, float valueUpdated)
        {
            OvertimeRecord record = m_records.FirstOrDefault(overtimeRecord => overtimeRecord.JDADepartmentId == updated.DepartmentId);

            if(record == null)
            {
                record = new OvertimeRecord()
                {
                    DepartmentName = updated.Department,
                    JDADepartmentId = updated.DepartmentId
                };
                
                m_records.Add(record);
            }  

            switch(fieldUpdated)
            {
                case ResourcePlanningItem.OvertimeField.OvertimeAgreedDay:
                    updated.OvertimeAgreedDay = valueUpdated;
                    record.AllocatedHours = valueUpdated;
                    break;
                case ResourcePlanningItem.OvertimeField.OvertimeAgreedNight:
                    updated.OvertimeAgreedNight = valueUpdated;
                    record.ResourceAgreedNight = valueUpdated;
                    break;
                case ResourcePlanningItem.OvertimeField.OvertimeRequestedDay:
                    record.ResourceRequestDay = valueUpdated;
                    updated.OvertimeRequestedDay = valueUpdated;
                    break;
                case ResourcePlanningItem.OvertimeField.OvertimeRequestedNight:
                    record.ResourceRequestNight = valueUpdated;
                    updated.OvertimeRequestedNight = valueUpdated;
                    break;
                default:
                    break;
            }

            m_overtimeService.RecordChange(updated.DepartmentId,updated.Department, new KeyValuePair<ResourcePlanningItem.OvertimeField, float>(fieldUpdated,valueUpdated));

            RaisePropertyChanged(() => Rows);

            TotalRow.OvertimeAgreedDay = Rows.Sum(row => row.OvertimeAgreedDay);
			TotalRow.OvertimeAgreedNight = Rows.Sum(row => row.OvertimeAgreedNight);
            TotalRow.OvertimeRequestedDay = Rows.Sum(row => row.OvertimeRequestedDay);
			TotalRow.OvertimeRequestedNight = Rows.Sum(row => row.OvertimeRequestedNight);

			RaisePropertyChanged (() => TotalRow);
        }

        /// <summary>
        /// If department is funded by the store, return true, else return false.
        /// </summary>
        /// <returns><c>true</c>, if department is funded by the store, <c>false</c> otherwise.</returns>
        /// <param name="department">department to check.</param>
        bool IsDepartmentInternallyFunded (PlanningData department)
        {
            if (EXTERNALLY_FUNDED_DEPARTMENT_NAMES.Any (externallyFundedName => String.Equals (externallyFundedName, department.Name, StringComparison.CurrentCultureIgnoreCase))) {
                return false;
            } else {
                return true;
            }
        }

        #region Sorting Functions
        void ApplyCurrentSortState()
        {
            if (DepartmentNameSortState.IsSorting)
            {
                PerformSortByDepartmentName();
                RaisePropertyChanged(() => DepartmentNameSortState);
            }
            else if (OvertimeRequiredSortState.IsSorting)
            {
                PerformSortByOvertimeRequired();
                RaisePropertyChanged(() => OvertimeRequiredSortState);
            }
            else if (PlannedHoursSortState.IsSorting)
            {
                PerformSortByPlannedHours();
                RaisePropertyChanged(() => PlannedHoursSortState);
            }
            else if (ScheduledHoursSortState.IsSorting)
            {
                PerformSortByPlannedHours();
                RaisePropertyChanged(() => ScheduledHoursSortState);
			}
			else if(ContractedSortState.IsSorting)
			{
				PerformSortByContracted();
				RaisePropertyChanged(() => ContractedSortState);
			}
			else if(TimeOffSortState.IsSorting)
			{
				PerformSortByTimeOff();
				RaisePropertyChanged(() => TimeOffSortState);
			}
        }

        void ToggleSortState(SortState state, bool startInverted)
        {
            if (m_currentSortState != null && state != m_currentSortState)
                m_currentSortState.IsSorting = false;

            if (!state.IsSorting)
            {
                state.IsSorting = true;
                state.IsAscending = !startInverted;
            }
            else
            {
                state.IsAscending = !state.IsAscending;
            }

            m_currentSortState = state;
        }

        public void SortByDepartmentName()
        {
            ToggleSortState(DepartmentNameSortState,false);

            PerformSortByDepartmentName();

            RaisePropertyChanged(() => DepartmentNameSortState);
			RaisePropertyChanged(() => Rows);
        }

		public void SortByContracted()
		{
			ToggleSortState(ContractedSortState,true);

			PerformSortByContracted();

			RaisePropertyChanged(() => ContractedSortState);
			RaisePropertyChanged(() => Rows);
		}
            
        public void SortByPlannedHours()
        {
            ToggleSortState(PlannedHoursSortState,true);

            PerformSortByPlannedHours();

            PlannedHoursSortState = PlannedHoursSortState;
			RaisePropertyChanged(() => Rows);
        }

        public void SortByScheduledHours()
        {
            ToggleSortState(ScheduledHoursSortState,true);

            PerformSortByScheduledHours();

            ScheduledHoursSortState = ScheduledHoursSortState;
			RaisePropertyChanged(() => Rows);
        }

        public void SortByOvertimeRequired()
        {
            ToggleSortState(OvertimeRequiredSortState,true);

            PerformSortByOvertimeRequired();

            OvertimeRequiredSortState = OvertimeRequiredSortState;
			RaisePropertyChanged(() => Rows);
        }

		public void SortByTimeOff()
		{
			ToggleSortState(TimeOffSortState,true);

			PerformSortByTimeOff();

			TimeOffSortState = TimeOffSortState;
			RaisePropertyChanged(() => Rows);
		}

        void PerformSortByOvertimeRequired()
        {
            if (OvertimeRequiredSortState.IsAscending)
            {
                Rows.Sort((x, y) => x.UnfilledHours.CompareTo(y.UnfilledHours));
            }
            else
            {
                Rows.Sort((x, y) => y.UnfilledHours.CompareTo(x.UnfilledHours));
            }
        }

		void PerformSortByContracted()
		{
			if (ContractedSortState.IsAscending)
			{
				Rows.Sort((x, y) => x.ContractedHours.CompareTo(y.ContractedHours));
			}
			else
			{
				Rows.Sort((x, y) => y.ContractedHours.CompareTo(x.ContractedHours));
			}
		}

        void PerformSortByScheduledHours()
        {
            if (ScheduledHoursSortState.IsAscending)
            {
                Rows.Sort((x, y) => x.ScheduledHours.CompareTo(y.ScheduledHours));
            }
            else
            {
                Rows.Sort((x, y) => y.ScheduledHours.CompareTo(x.ScheduledHours));
            }
        }

        void PerformSortByPlannedHours()
        {
            if (PlannedHoursSortState.IsAscending)
            {
                Rows.Sort((x, y) => x.PlannedHours.CompareTo(y.PlannedHours));
            }
            else
            {
                Rows.Sort((x, y) => y.PlannedHours.CompareTo(x.PlannedHours));
            }
        }

        void PerformSortByDepartmentName()
        {
            if (DepartmentNameSortState.IsAscending)
            {
                Rows.Sort((x, y) => x.Department.CompareTo(y.Department));
            }
            else
            {
                Rows.Sort((x, y) => y.Department.CompareTo(x.Department));
            }
        }

		void PerformSortByTimeOff()
		{
			if (TimeOffSortState.IsAscending)
			{
				Rows.Sort((x, y) => x.PlannedTimeOff.CompareTo(y.PlannedTimeOff));
			}
			else
			{
				Rows.Sort((x, y) => y.PlannedTimeOff.CompareTo(x.PlannedTimeOff));
			}
		}

        #endregion
    }
}
