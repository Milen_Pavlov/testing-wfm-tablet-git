﻿using System;
using System.Linq;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using System.Runtime.CompilerServices;
using WFM.Shared.DataTransferObjects.Breaks;
using System.Resources;
using System.Reflection;
using Cirrious.CrossCore;
using System.ComponentModel;

namespace WFM.Core
{
    public class ScheduleBreaksViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Breaks";
            }
        }
        
        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;
        private readonly ScheduleBreaksService m_breaksService;

        protected MvxSubscriptionToken m_scheduleBeginToken;
        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;

        private const int c_daysInWeek = 7;

        private bool m_loading;
        public bool Loading
        {
            get{ return m_loading; }
            set
            {
                m_loading = value;
                RaisePropertyChanged(() => Loading);
            }
        } 

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get{ return m_loadingMessage; }
            set
            {
                m_loadingMessage = value;
                RaisePropertyChanged(() => LoadingMessage);
            }
        } 

        private bool m_noData;
        public bool NoData
        {
            get{ return m_noData; }
            set
            {
                m_noData = value;
                RaisePropertyChanged(() => NoData);
            }
        }

        private List<BreakItem> m_data;
        public List<BreakItem> Data
        {
            get { return m_data; }
            set
            {
                m_data = value;
                RaisePropertyChanged(() => Data);
            }
        }

        private IList<BreakRecord> m_records = new List<BreakRecord>();

        private List<BreakItem> m_rows;
        public List<BreakItem> Rows
        {
            get { return m_rows; }
            set
            {
                m_rows = value;
                RaisePropertyChanged(() => Rows);
            }
        }

        private String m_colleagueText;
        public String ColleagueText
        {
            get { return m_colleagueText; }
            set
            {
                m_colleagueText = value;
                RaisePropertyChanged (() => ColleagueText);
            }
        }

        public ScheduleBreaksViewModel(
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            ScheduleBreaksService breaksService)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;
            m_breaksService = breaksService;
        }

        public void Init()
        {
            m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage>(OnScheduleBegin);
            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage>(OnScheduleEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage>(OnScheduleUpdate);

            LoadData(false);

            ColleagueText = Localiser.Get ("colleague_label");
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed();

            // Stop breaks service request
            m_breaksService.Cancel();

            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
        }

        private async void LoadData(bool forceUpdate)
        {
            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            Loading = true;

            var employees = m_scheduleService.Schedule.Employees;
            var breakShiftIDs = GenerateBreakShiftIDs(employees);
            m_records = await LoadBreaks(forceUpdate, breakShiftIDs);

            if (m_records == null)
                m_records = new List<BreakRecord> ();

            UpdateDisplay();

            Loading = false;
        }

        List<BreakItem> BuildBreakObjects(List<EmployeeData> employees)
        {
            var breaks = new List<BreakItem>();

            if (employees != null)
            {
                foreach (var e in employees)
                {                
                    foreach (var d in e.Days)
                    {
                        foreach (var s in d.Shifts)
                        {
                            foreach (var detail in s.Details)
                            {
                                BreakRecord matchingRecord = m_records.Where(m => m.BreakID == detail.ShiftDetailID).FirstOrDefault();
                                bool breakTaken = matchingRecord != null ? matchingRecord.BreakTaken : false;
                                breaks.Add(CreateBreakItem(e, s, detail, breakTaken));
                            }
                        }
                    }
                }
                
                foreach (var employee in employees)
                {
                    if (employee.LastDayPreviousWeek != null)// Add breaks from the last day of the previous week in
                    {
                        foreach (var s in employee.LastDayPreviousWeek.Shifts)
                        {
                            foreach (var d in s.Details)
                            {
                                BreakRecord matchingRecord = m_records.Where(m => m.BreakID == d.ShiftDetailID).FirstOrDefault();
                                bool breakTaken = matchingRecord != null ? matchingRecord.BreakTaken : false;
                                var job = s.GetJob(d.ScheduledJobID);
                                if (job != null)
                                {
                                    breaks.Add(CreateBreakItem(employee, s, d, breakTaken));
                                }
                            }
                        }
                    }
                }
            }

            return breaks;
        }

        List<int> GenerateBreakShiftIDs(List<EmployeeData> employees)
        {
            // Retrieve all break taken data
            List<int> breakShiftIDs = new List<int>();

            foreach (var e in employees)
            {
                foreach (var d in e.Days)
                {
                    foreach (var s in d.Shifts)
                    {
                        breakShiftIDs.Add(s.ShiftID);
                    }
                }

                if (e.LastDayPreviousWeek != null)// Add breaks from the last day of the previous week in
                {
                    foreach (var s in e.LastDayPreviousWeek.Shifts)
                    {
                        breakShiftIDs.Add(s.ShiftID);
                    }
                }
            }

            return breakShiftIDs;
        }

        async Task<IList<BreakRecord>> LoadBreaks(bool forceUpdate, List<int> breakShiftIDs)
        {
            var readOperation = m_breaksService.ReadBreaks(breakShiftIDs, forceUpdate);

            await Task.Delay(ConstantValues.c_comfortBreakMillis);

            var result = await readOperation;

            return result;
        }

        private void UpdateDisplay()
        {
            //Rebuild data from break records
            List<EmployeeData> employees = new List<EmployeeData>();

            if(m_scheduleService.Schedule!=null)
                employees = m_scheduleService.Schedule.FilteredEmployees;
            
            Data = BuildBreakObjects(employees);

            if(Data != null)
            {
                var filteredList = new List<BreakItem>();

                if(m_scheduleService.DayIndex != null && m_scheduleService.StartDay != null)
                {
                    var currentDate = ((DateTime) m_scheduleService.StartDate).AddDays((int) m_scheduleService.DayIndex);

                    foreach(var detail in Data)
                    {
                        if(TimePeriodHelper.DoesOverlap(currentDate, currentDate.AddDays(1), detail.StartTime, detail.EndTime))
                        {
                            filteredList.Add(detail);
                        }
                    }
                }
                else
                {
                    filteredList = Data.ToList();
                }

                Rows = filteredList.OrderBy(b => b.StartTime).ThenBy(b => b.EmployeeName).ToList();
            }

            NoData = (Data == null || Data.Count == 0);
        }

        public void OnBreaksTakenEdited(BreakItem updated)
        {
            var record = m_records.FirstOrDefault(breakItem => breakItem.BreakID == updated.BreakDetailID);

            if(record != null)
            {
                record.BreakTaken = !updated.BreakTaken;
            }
            else
            {
                record = new BreakRecord()
                    {
                        BreakID = updated.BreakDetailID,
                        BreakTaken = !updated.BreakTaken,
                        ShiftID = updated.ShiftID
                    };

                m_records.Add(record);
            }

            //Refresh VM display of data
            UpdateDisplay();

            m_breaksService.RecordChange(record.BreakID, record.ShiftID, record.BreakTaken);
        }

        private void OnScheduleBegin(ScheduleReadBeginMessage message)
        {
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            //Schedule end means the schedule
            LoadData(true);
        }

        private void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            if (message.RevertChanges)
                LoadData (true);
            else
                UpdateDisplay();
        }

        private BreakItem CreateBreakItem(EmployeeData e, ShiftData s, ShiftDetailData d, bool breakTaken)
        {
            
        
            return new BreakItem()
            {
                StartTime = d.Start.Value,
                Duration = d.Duration,
                EmployeeName = e.Name,
                ShiftStartTime = s.Start,
                ShiftEndTime = s.End,
                Department = s.GetJob(d.ScheduledJobID).JobName,
                BreakDetailID = d.ShiftDetailID,
                BreakTaken = breakTaken,
                StartTimeFormatted = string.Format("{0} {1} - {2}", d.Start.Value.ToString("ddd"), TimeFormatter.FormatTime(d.Start.Value), TimeFormatter.FormatTime(d.End.Value)),
                ShiftID = s.ShiftID,
            };
        }
    }
}

