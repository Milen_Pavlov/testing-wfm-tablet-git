﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ScheduleDataViewModel : WFMViewModel
    {
        private readonly ScheduleService m_schedule;
        //private readonly IMvxMessenger m_messenger;

        //protected MvxSubscriptionToken m_scheduleBeginToken;
        //protected MvxSubscriptionToken m_scheduleEndToken;

        private List<JDAShift> m_unfilledShifts;
        public List<JDAShift> UnfilledShifts
        {
            get { return m_unfilledShifts; }
            private set 
            {
                m_unfilledShifts = value; RaisePropertyChanged(() => UnfilledShifts); 
            }
        }

        // For handling the selection
        public void OnShiftSelect( JDAShift shift )
        {

        }

        public ScheduleDataViewModel (ScheduleService schedule, IMvxMessenger messenger)
        {
            m_schedule = schedule;
            //m_messenger = messenger;
        }

        public void Init()
        {
            //m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleBeginMessage> (OnScheduleBegin);
            //m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleEndMessage> (OnScheduleEnd);
        
            UnfilledShifts = m_schedule.Schedule.UnfilledShifts;
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            //m_messenger.Unsubscribe<ScheduleBeginMessage>(m_scheduleBeginToken);
            //m_messenger.Unsubscribe<ScheduleEndMessage>(m_scheduleEndToken);
        }

        private void OnScheduleBegin(ScheduleReadBeginMessage message)
        {
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
        }
    }
}

