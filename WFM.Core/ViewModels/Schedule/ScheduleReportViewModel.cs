﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ScheduleReportViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Report";
            }
        }

        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;

        protected MvxSubscriptionToken m_scheduleBeginToken;
        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;

        private ReportItem[] m_rows;
        public ReportItem[] Rows
        {
            get { return m_rows; }
            set { m_rows = value; RaisePropertyChanged(() => Rows); }
        }

        public ScheduleReportViewModel (ScheduleService schedule, IMvxMessenger messenger)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;
        }

        public void Init()
        {
            m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage> (OnScheduleBegin);
            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage> (OnScheduleUpdate);

            Update ();
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
        }

        private void Update()
        {
            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            var index = m_scheduleService.Schedule.DayIndex;
            int start = index == null ? 0 : index.Value * ScheduleTime.DayIntervals;
            int length = index == null ? ScheduleTime.WeekIntervals : ScheduleTime.DayIntervals;

            bool hideZeroDemand = ConfigurationService.HideDefaultVarianceValues;

            List<ReportItem> rows;

            if (hideZeroDemand)
            {
                rows = BuildFilteredReportList (length, start);
            }
            else
            {
                rows = BuildDefaultReportList (length, start);
            }

            Rows = rows.ToArray();
        }

        List<ReportItem> BuildDefaultReportList(int length, int start)
        {
            var rows = new List<ReportItem>();

            for(int i=0; i<length; ++i)
            {
                int interval = start + i;

                var timeStart = m_scheduleService.StartDate.AddMinutes (interval * ScheduleTime.IntervalMinutes);
                var timeEnd = timeStart.AddMinutes (ScheduleTime.IntervalMinutes);

                rows.Add (new ReportItem ()
                    {
                        Time = string.Format ("{0} {1} - {2}", timeStart.ToString ("ddd"), TimeFormatter.FormatTime(timeStart), TimeFormatter.FormatTime(timeEnd)),
                        Scheduled = m_scheduleService.Accuracy != null ? m_scheduleService.Accuracy.Scheduled[interval] : 0,
                        Demand = m_scheduleService.Accuracy != null ? m_scheduleService.Accuracy.Demand[interval] : 0,
                        Variance = m_scheduleService.Accuracy != null ? m_scheduleService.Accuracy.Scheduled[interval] - m_scheduleService.Accuracy.Demand[interval] : 0
                    });
            }

            return rows;
        }


        List<ReportItem> BuildFilteredReportList(int length, int start)
        {
            var rows = new List<ReportItem>();

            for(int i=0; i<length; ++i)
            {
                int interval = start + i;

                var timeStart = m_scheduleService.StartDate.AddMinutes (interval * ScheduleTime.IntervalMinutes);
                var timeEnd = timeStart.AddMinutes (ScheduleTime.IntervalMinutes);
                
                var scheduled = m_scheduleService.Accuracy != null ? m_scheduleService.Accuracy.Scheduled[interval] : 0;
                var demand = m_scheduleService.Accuracy != null ? m_scheduleService.Accuracy.Demand[interval] : 0;
                var variance = m_scheduleService.Accuracy != null ? m_scheduleService.Accuracy.Scheduled[interval] - m_scheduleService.Accuracy.Demand[interval] : 0;
                
                bool isDefault = scheduled == 0 && demand == 0;

                if(isDefault)
                {
                    continue;
                }

                rows.Add(new ReportItem ()
                    {
                        Time = string.Format ("{0} {1} - {2}", timeStart.ToString ("ddd"), TimeFormatter.FormatTime(timeStart), TimeFormatter.FormatTime(timeEnd)),
                        Scheduled = scheduled,
                        Demand = demand,
                        Variance = variance
                    });
            }

            return rows;
        }


        private void OnScheduleBegin(ScheduleReadBeginMessage message)
        {
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            Update();
        }

        private void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            Update();
        }
    }
}

