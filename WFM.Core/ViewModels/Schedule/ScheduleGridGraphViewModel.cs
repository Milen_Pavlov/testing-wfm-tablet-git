﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class ScheduleGridGraphViewModel : WFMViewModel
    {
        public enum ScreenSizeState
        {
            TopMax,
            Split,
            BottomMax
        }

        public bool GraphMode
        {
            get
            {
                return m_persistentState.GraphMode;
            }

            set
            {
                m_persistentState.GraphMode = value;
                RaisePropertyChanged (() => GraphMode);
            }
        }

        public bool GraphShown
        {
            get
            {
                return GraphMode && ScreenState != ScreenSizeState.TopMax;
            }
            set
            {
                RaisePropertyChanged (() => GraphShown);
            }
        }
            
        public bool TableShown
        {
            get
            {
                return !GraphMode && ScreenState != ScreenSizeState.TopMax;
            }
            set
            {
                RaisePropertyChanged (() => TableShown);
            }
        }
            
        public ScreenSizeState ScreenState
        {
            get
            {
                return m_persistentState.ScreenState;
            }

            set
            {
                m_persistentState.ScreenState = value;
                RaisePropertyChanged (() => ScreenState);

                RaisePropertyChanged (() => CanIncreaseSize);
                RaisePropertyChanged (() => CanDecreaseSize);
                RaisePropertyChanged (() => GraphShown);
                RaisePropertyChanged (() => TableShown);
            }
        }

        public bool CanIncreaseSize
        {
            get
            {
                return ScreenState != ScreenSizeState.BottomMax;
            }
            set
            {
                RaisePropertyChanged (() => CanIncreaseSize);
            }
        }

        public bool CanDecreaseSize
        {
            get
            {
                return ScreenState != ScreenSizeState.TopMax;
            }
            set
            {
                RaisePropertyChanged (() => CanDecreaseSize);
            }
        }

        private readonly ScheduleGridGraphPersistentStateService m_persistentState;

        public ScheduleGridGraphViewModel (ScheduleGridGraphPersistentStateService persistentState)
        {
            m_persistentState = persistentState;
        }

        public void Init()
        {
            ShowViewModel<ScheduleGridViewModel> ();

            if (m_persistentState.GraphMode)
            {
                ShowViewModel<ScheduleGraphViewModel> (true);
            }
            else
            {
                ShowViewModel<ScheduleReportViewModel> ();
            }

            GraphMode = m_persistentState.GraphMode;
        }

        public void SwitchToTable()
        {
            ShowViewModel<ScheduleReportViewModel> ();
            GraphMode = false;

            if (ScreenState == ScreenSizeState.TopMax)
            {
                ScreenState = ScreenSizeState.Split;
            }
            else
            {
                ScreenState = ScreenState;
            }
        }

        public void SwitchToGraph()
        {
            ShowViewModel<ScheduleGraphViewModel> (true);
            GraphMode = true;

            if(ScreenState == ScreenSizeState.TopMax)
            {
                ScreenState = ScreenSizeState.Split;
            }
            else
            {
                ScreenState = ScreenState;
            }
        }

        public void IncreasePanel()
        {
            if (ScreenState == ScreenSizeState.TopMax)
            {
                ScreenState = ScreenSizeState.Split;
            }
            else if (ScreenState == ScreenSizeState.Split)
            {
                ScreenState = ScreenSizeState.BottomMax;
            }
        }

        public void DecreasePanel()
        {
            if (ScreenState == ScreenSizeState.Split)
            {
                ScreenState = ScreenSizeState.TopMax;
            }
            else if (ScreenState == ScreenSizeState.BottomMax)
            {
                ScreenState = ScreenSizeState.Split;
            }
        }
    }
}