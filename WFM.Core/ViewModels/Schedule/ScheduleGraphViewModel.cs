﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public enum ScheduleGraphLegendType
    {
        Under,
        Correct,
        Over,
        Demand
    }

    public class ScheduleGraphLegendItem
    {
        public string Title { get; set; }
        public ScheduleGraphLegendType LegendType { get; set; }
    }

    public class ScheduleGraphViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Workload";
            }
        }

        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;

        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;

        private AccuracyData m_accuracy;
        public AccuracyData Accuracy
        {
            get { return m_accuracy; }
            set { m_accuracy = value; RaisePropertyChanged(() => Accuracy); }
        }

        private int? m_dayIndex;
        public int? DayIndex
        {
            get { return m_dayIndex; }
            set { m_dayIndex = value; RaisePropertyChanged(() => DayIndex); }
        }

        private bool m_isInline;

        public bool IsInline
        {
            get
            {
                return m_isInline;
            }

            set
            {
                m_isInline = value;
                RaisePropertyChanged (() => IsInline);
            }
        }

        private ScheduleGraphLegendItem[] m_legend;
        public ScheduleGraphLegendItem[] Legend
        {
            get { return m_legend; }
            set { m_legend = value; RaisePropertyChanged (() => Legend); }
        }

        public ScheduleGraphViewModel (ScheduleService schedule, IMvxMessenger messenger)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;
        }

        public void Init(bool displayedInline)
        {
            IsInline = displayedInline;

            Legend = new ScheduleGraphLegendItem[]
            {
                new ScheduleGraphLegendItem (){ Title = Localiser.Get ("under_covered"), LegendType = ScheduleGraphLegendType.Under },
                new ScheduleGraphLegendItem (){ Title = Localiser.Get ("correct_covered"), LegendType = ScheduleGraphLegendType.Correct },
                new ScheduleGraphLegendItem (){ Title = Localiser.Get ("over_covered"), LegendType = ScheduleGraphLegendType.Over },
                new ScheduleGraphLegendItem (){ Title = Localiser.Get ("demand_label"), LegendType = ScheduleGraphLegendType.Demand },
            };

            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<GraphUpdateMessage> (OnGraphUpdate);

            Update ();
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
            m_messenger.Unsubscribe<GraphUpdateMessage>(m_scheduleUpdateToken);
        }

        private void Update()
        {
            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            DayIndex = m_scheduleService.Schedule.DayIndex;
            Accuracy = m_scheduleService.Accuracy;
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            Update();
        }

        private void OnGraphUpdate(GraphUpdateMessage message)
        {
            Update();
        }
    }
}

