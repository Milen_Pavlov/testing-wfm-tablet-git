using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class ScheduleTimeOffViewModel : WFMViewModel
    {
        private readonly ScheduleService m_scheduleService;
        private readonly IAlertBox m_alert;
        private readonly TimeOffService m_timeOffService;
        private readonly SessionData m_sessionData;
        private readonly IEditModeLink m_overtimeEditMode;
        private readonly IEditModeLink m_breaksEditMode;
        private readonly IMvxMessenger m_messenger;

        private bool IsClosing { get; set; }
        private DateTime m_day;
        private int m_employeeID;
        private EmployeeData m_employeeData;
        private JDATimeOffRequest m_request;

        private string m_fullName;
        public string FullName
        { 
            get { return m_fullName; }
            set { m_fullName = value; RaisePropertyChanged(() => FullName); }
        }

        private String m_date;
        public String Date
        {
            get { return m_date; }
            set
            {
                m_date = value;
                RaisePropertyChanged (() => Date);
            }
        }

        private bool m_loading;
        public bool Loading 
        {
            get { return m_loading; }
            set
            {
                m_loading = value;
                RaisePropertyChanged (() => Loading);
            }
        }

        private bool m_allowTimeOffRemoval;
        public bool AllowTimeOffRemoval
        {
            get { return m_allowTimeOffRemoval; }
            set
            {
                m_allowTimeOffRemoval = value;
                RaisePropertyChanged (() => AllowTimeOffRemoval);
            }
        }

        private IList<JDAEmployeeAssignedJob> m_fixedShifts;
        public IList<JDAEmployeeAssignedJob> FixedShifts
        {
            get { return m_fixedShifts; }
            set
            {
                m_fixedShifts = value;
                RaisePropertyChanged (() => FixedShifts);
            }
        }

        public ScheduleTimeOffViewModel (ScheduleService schedule,
            IAlertBox alert,
            TimeOffService timeOff, 
            SessionData session, 
            EmployeeService employeeService, 
            ScheduleOvertimeService overtimeService, 
            ScheduleBreaksService breaksService,
            IMvxMessenger messenger)
        {
            m_scheduleService = schedule;
            m_alert = alert;
            m_timeOffService = timeOff;
            m_sessionData = session;
            m_overtimeEditMode = overtimeService;
            m_breaksEditMode = breaksService;
            m_messenger = messenger;
        }

        public async void Init(int employeeId, DateTime dayStart)
        {
            Analytics.TrackAction ("View Shift","Show");

            m_employeeID = employeeId;
            m_day = dayStart;

            AllowTimeOffRemoval = IsTimeOffRemovalAllowed ();

            Loading = true;
            var request = GetTimeOffRequest (m_employeeID, m_day);
            //Make sure the spinner shows for long enough time, don't want it blinking in and out
            await Task.Delay(500);
            await request;
            
            if(request != null)
                m_request = request.Result;

			m_employeeData = GetEmployeeInfo (m_employeeID);
            Loading = false;

            FixedShifts = GetFixedShift ();

            if (m_request == null)
            {
                // Request not found show an error.
                m_alert.ShowOK (Localiser.Get ("timeoffview_loading_request_failed_title"), Localiser.Get ("timeoffview_loading_request_failed_body"), ((i) => { CloseWindow(false); }) );
                return;
            }

            UpdateEmployee ();
            UpdateDate ();
        }

        private void UpdateEmployee()
        {
            string requestTitleFormatString = Localiser.Get("shift_view_timeoff_titleformat") ?? "{0}, {1} - {2}";
            
            string title = string.Format(requestTitleFormatString, m_request.EmployeeLastName, m_request.EmployeeFirstName, m_request.PayAdjustmentCategoryName);
            
            FullName = title;
        }

        private void UpdateDate()
        {
            Date = m_request.SubTitle;
        }

        public void OnCancel()
        {
            if(IsClosing == false)
                CloseWindow (false);
        }

        public void OnCancelTimeOff()
        {
            CheckModified(() => { 
                m_alert.ShowYesNo (Localiser.Get ("schedule_time_off_confirm_title"), Localiser.Get ("schedule_time_off_confirm_body"),
                    ((i) =>
                        {
                            if(i == 1)
                            {
                                CancelTimeOff ("");
                            }
                        })
                );
            });
        }

        public async void CancelTimeOff(string comment)
        {
            Loading = true;
            var request = m_timeOffService.GetDetailedTimeOffRequest (m_sessionData.Site.SiteID, m_request);
            await request;
            if (request == null || request.Result == null)
            {
                Loading = false;
                m_alert.ShowOK (Localiser.Get ("schedule_time_off_remove_failed_title"), Localiser.Get ("schedule_time_off_remove_failed_body"));
                return;
            }
            
            var result = await m_timeOffService.CancelHolidayRequest (request.Result.TimeOffRequest, comment);
            
            bool requiresRefresh = false;
            
            if (result.Success == true)
            {
                m_scheduleService.Schedule.ClearTimeOff(m_employeeID, m_day);
                if (FixedShifts != null)
                {
                    foreach (var shift in FixedShifts)
                    {
                        var newShiftRequest = new ShiftEditMessage (this)
                        {
                            Op = ShiftEditMessage.Operation.CreateShift,
                            EmployeeID = m_employeeID,
                            Start = shift.Start,
                            End = shift.End,
                            JobID = shift.JobID,
                            StoreInUndoStack = false,
                        };

                        m_messenger.Publish<ShiftEditMessage> (newShiftRequest);

                        await m_scheduleService.SafeWriteReadAsync(true, m_scheduleService.StartDate, m_scheduleService.CurrentFilter, 500);
                    }
                }
                requiresRefresh = true;
            }
            else
            {
                m_alert.ShowOK (result.ErrorSource, result.Message);
                requiresRefresh = false;
            }

            Loading = false;

            CloseWindow (requiresRefresh);
        }

        public override void OnViewClosed ()
        {
            if(!IsClosing)
                Close ();

            base.OnViewClosed ();
        }

        private void CloseWindow(bool requiresRefresh)
        {
            IsClosing = true;
            InvokeOnMainThread(
                new Action(() => Close(requiresRefresh))
            );
        }

        private async Task<JDATimeOffRequest> GetTimeOffRequest(int employeeID, DateTime day)
        {
            DateTime startDay = day;
            DateTime endDay = day.AddDays (1);

            m_request = null;

            var requests = await m_timeOffService.GetAllTimeOffRequests (m_sessionData.Site.SiteID, startDay, endDay); 
            if (requests == null)
                return null;

            foreach (JDATimeOffRequest request in requests)
            {
                if (request.Status != TimeOffStatus.Approved)
                    continue;
                
                if (request.EmployeeID.HasValue == false || request.EmployeeID.Value != employeeID)
                    continue;
                    
                if (request.End.Date < startDay.Date || request.Start.Date > endDay.Date)
                    continue;

                return request;
            }
            return null;
        }

        private EmployeeData GetEmployeeInfo(int employeeID)
        {
            var employee = m_scheduleService.Schedule.GetEmployee (employeeID);
            return employee;
        }

        private bool IsTimeOffRemovalAllowed()
        {
            // Don't allow time off removal for more than a week in advance
            if (DateTime.Now.AddDays (7) < m_day)
                return false;

            // Don't allow time off removal in the past
            if (DateTime.Now.Date > m_day)
                return false;

            return true;
        }

        private List<JDAEmployeeAssignedJob> GetFixedShift()
        {
            var currentDay = m_employeeData.Days.First (x => x.DayStart.Date.Equals (m_day.Date));
            if (currentDay == null)
                return null;
            
            var fixedShift = currentDay.FixedShifts;

            if (fixedShift == null || fixedShift.Count <= 0)
                return null;
            
            return fixedShift;
        }

        private void CheckModified(Action unmodifiedCallback)
        {
            if ((m_scheduleService.Schedule != null && m_scheduleService.Schedule.IsModified) || m_overtimeEditMode.IsEdited || m_breaksEditMode.IsEdited)
            {
                m_alert.ShowOK (Localiser.Get ("schedule_timeoff_commit_changes_title"), Localiser.Get ("schedule_timeoff_commit_changes_body"));
            }
            else
            {
                unmodifiedCallback();
            }
        }
    }
}

