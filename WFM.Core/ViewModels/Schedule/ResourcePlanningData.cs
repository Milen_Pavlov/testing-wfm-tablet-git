﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ResourcePlanningData
    {
        public DateTime LockedDate
        {
            get;
            set;
        }

        public Dictionary<int, ResourcePlanningItem>[] Days
        {
            get;
            set;
        }

        public ResourcePlanningData()
        {
            Days = new Dictionary<int, ResourcePlanningItem>[7];

            Days[0] = new Dictionary<int, ResourcePlanningItem>();
            Days[1] = new Dictionary<int, ResourcePlanningItem>();
            Days[2] = new Dictionary<int, ResourcePlanningItem>();
            Days[3] = new Dictionary<int, ResourcePlanningItem>();
            Days[4] = new Dictionary<int, ResourcePlanningItem>();
            Days[5] = new Dictionary<int, ResourcePlanningItem>();
            Days[6] = new Dictionary<int, ResourcePlanningItem>();
        }
    }
}

