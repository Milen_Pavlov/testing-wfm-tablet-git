﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Consortium.Client.Core;
using Consortium.Client.Core.Extensions;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.CrossCore.UI;
using WFM.Shared.DataTransferObjects.Audits;

namespace WFM.Core
{
    public class OvertimeAuditViewModel : WFMViewModel
    {
        private ScheduleService m_scheduleService;
        private OvertimeService m_overtimeService;

        private bool m_Loading;

        public bool Loading
        {
            get
            {
                return m_Loading;
            }
            set
            {
                m_Loading = value;

                if (value)
                    NoDataMessage = "Loading";
                
                RaisePropertyChanged(() => Loading);
                RaisePropertyChanged(() => NoData);
            }
        }

        public bool NoData
        {
            get
            {
                return m_Loading || Records == null || Records.Count == 0;
            }
            set
            {
                RaisePropertyChanged(() => NoData);
            }
        }

        private string m_NoDataMessage;

        public string NoDataMessage
        {
            get
            {
                return m_NoDataMessage;
            }
            set
            {
                m_NoDataMessage = value;
                RaisePropertyChanged(() => NoDataMessage);
            }
        }

        private string m_ViewTitle;

        public string ViewTitle
        {
            get
            {
                return "Audit Log for week starting " + m_scheduleService.StartDate.Date.ToString("D");
            }
            set
            {
                m_ViewTitle = value;
                RaisePropertyChanged(() => ViewTitle);
            }
        }

        private bool m_isShowing;
        public bool IsShowing
        { 
            get { return m_isShowing; }
            set { m_isShowing = value; RaisePropertyChanged(() => IsShowing); }
        }

        private List<AuditRecord> m_Records = new List<AuditRecord>();

        public List<AuditRecord> Records
        {
            get
            {
                return m_Records;
            }
            set
            {
                m_Records = value;
                RaisePropertyChanged(() => Records);
                RaisePropertyChanged(() => NoData);
            }
        }

        public OvertimeAuditViewModel(ScheduleService scheduleService, OvertimeService overtimeService)
        {
            this.m_scheduleService = scheduleService;
            this.m_overtimeService = overtimeService;

            IsShowing = false;
        }

        public async void Init()
        {
            Records.Clear();

            IsShowing = true;
            Loading = true;

            var response = await m_overtimeService.GetAuditRecordsForDate(m_scheduleService.StartDate.Date);

            if (response == null)
            {
                NoDataMessage = "Error retrieving audit log";
            }
            else
            {
                if (response.Count == 0)
                {
                    NoDataMessage = "No audit log found.";
                }
                else
                {
                    Records = new List<AuditRecord>(response);
                    NoDataMessage = string.Empty;
                }
            }


            RaisePropertyChanged(() => Records);
            Loading = false;
        }

        public void OnClose()
        {
            IsShowing = false;
            InvokeOnMainThread(
                new Action(() => Close())
            );
        }
    }
}