﻿using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class ScheduleDailyPlanningViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Planning";
            }
        }

        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;

        protected MvxSubscriptionToken m_scheduleBeginToken;
        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;

        private bool m_sortedAscending = true;
        public bool SortedAscending
        {
            get { return m_sortedAscending; }
            set { m_sortedAscending = value; RaisePropertyChanged(() => SortedAscending); }
        }

        private bool m_isSorted;
        public bool IsSorted
        {
            get { return m_isSorted; }
            set { m_isSorted = value; RaisePropertyChanged(() => IsSorted); }
        }

        private List<ColumnHeader> m_headers;
        public List<ColumnHeader> Headers
        {
            get { return m_headers; }
            set { m_headers = value; RaisePropertyChanged(() => Headers); }
        }

        private int? m_dayIndex;
        public int? DayIndex
        {
            get { return m_dayIndex; }
            set { m_dayIndex = value; RaisePropertyChanged(() => DayIndex); }
        }

        private List<PlanningData> m_planning;
        public List<PlanningData> Planning
        {
            get { return m_planning; }
            set { m_planning = value; RaisePropertyChanged(() => Planning); }
        }

        public ScheduleDailyPlanningViewModel (ScheduleService schedule, IMvxMessenger messenger)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;
        }

        public void Init()
        {
            m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage> (OnScheduleBegin);
            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage> (OnScheduleUpdate);

            IsSorted = false;
            SortedAscending = false;

            Update();
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
        }

        private void Update()
        {
            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            UpdateSchedule();
            UpdateHeaders();
        }

        private void UpdateSchedule()
        {   
            DayIndex = m_scheduleService.DayIndex;
            if (m_scheduleService.Accuracy != null &&
               m_scheduleService.Accuracy.Planning != null)
                Planning = m_scheduleService.Accuracy.Planning.ToList ();
            else
                Planning = new List<PlanningData> ();
        }

        private void UpdateHeaders()
        {   
            if (m_scheduleService.DayIndex.HasValue)
            {
                int index = m_scheduleService.DayIndex.Value;
                var date = m_scheduleService.StartDate.AddDays (index).ToString (Localiser.Get("dddd dd/MM"));

                var header = new ColumnHeader () {
                    Index = index,
                    StartHour = m_scheduleService.StartDate.Hour,
                    Title = string.Format("{0}, Under {1:0.00}, Over {2:0.00}", date, 
                        m_scheduleService.Accuracy.PlanningUnder [index], 
                        m_scheduleService.Accuracy.PlanningOver [index]),
                    Sort = new SortState ()
                };

                Headers = new List<ColumnHeader> () { header };
            }
            else
            {
                var headers = new List<ColumnHeader> ();

                for (int i = 0; i < 7; ++i)
                {
                    var header = new ColumnHeader () {
                        Index = i,
                        Title = m_scheduleService.StartDate.AddDays (i).ToString (Localiser.Get("ddd dd/MM")),
                        Title1 = "Under",
                        Value1 = m_scheduleService.Accuracy.PlanningUnder[i].ToString ("n2"),
                        Title2 = "Over",
                        Value2 = m_scheduleService.Accuracy.PlanningOver[i].ToString ("n2"),
                        Sort = new SortState () { IsSorting = false }
                    };

                    headers.Add (header);
                }

                Headers = headers;
            }
        }

        private void OnScheduleBegin(ScheduleReadBeginMessage message)
        {
        }

        private void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            Update ();
        }

        private void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            if (message.UpdateSchedule)
                UpdateSchedule ();

            if (message.UpdateHeaders)
                UpdateHeaders ();
        }
            
        public void OnSortDepartment()
        {
                if (SortedAscending)
                {
                    Planning = Planning
                        .OrderBy (x => x.Name)
                        .ToList ();
                }
                else
                {
                    Planning = Planning
                        .OrderByDescending (x => x.Name)
                        .ToList ();
                }
                
                IsSorted = true;
                SortedAscending = !SortedAscending;
        }
    }
}

