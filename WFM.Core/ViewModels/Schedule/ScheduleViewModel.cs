using System;
using System.Collections.Generic;
using System.Globalization;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using System.Diagnostics;
using System.Text;
using System.Linq;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleViewModel : WFMViewModel
    {
        private enum TabTypes
        {
            Schedule,
            ScheduleAlternative,
            Workload,
            Planning,
            Report,
            Breaks,
            Resources,
        }

        private readonly ScheduleService m_schedule;
        private readonly IMvxMessenger m_messenger;
        private readonly IESSConfigurationService m_essConfigurationService;
        private readonly IAlertBox m_alertBox;
        private readonly SessionService m_session;
        private readonly ScheduleUndoService m_undoService;
        private readonly SpareShiftService m_spareShiftsService;

        private readonly IEditModeLink m_overtimeEditMode;
        private readonly IEditModeLink m_breaksEditMode;

        //private Task m_overtimeSaveOperation;
        //private Task m_breaksSaveOperation;

        protected MvxSubscriptionToken m_scheduleWriteBeginToken;
        protected MvxSubscriptionToken m_scheduleWriteEndToken;
        protected MvxSubscriptionToken m_scheduleReadBeginToken;
        protected MvxSubscriptionToken m_scheduleReadEndToken;
        protected MvxSubscriptionToken m_orgEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;
        protected MvxSubscriptionToken m_startTimeChangedToken;
        protected MvxSubscriptionToken m_editModeChangedToken;
        protected MvxSubscriptionToken m_signOutToken;
        protected MvxSubscriptionToken m_scheduleRefreshToken;

        private bool m_weekNumberDisplayed;

        private const int c_weeksInYear = 52;
        private const int c_startWeekOffset = 0;

        private int m_selectedDayIndex;
        public int SelectedDayIndex
        {
            get { return m_selectedDayIndex; }
            set { m_selectedDayIndex = value; RaisePropertyChanged(() => SelectedDayIndex); }
        }

        private List<string> m_segments;
        public List<string> Segments
        {
            get { return m_segments; }
            set { m_segments = value; RaisePropertyChanged(() => Segments); }
        }

        private int m_weekSelect;
        public int WeekSelect
        {
            get { return m_weekSelect; }
            set { m_weekSelect = value; RaisePropertyChanged(() => WeekSelect); }
        }

        private bool m_noData;
        public bool NoData
        {
            get { return m_noData; }
            set { m_noData = value; RaisePropertyChanged(() => NoData); }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set 
            { 
                m_loading = value; 
                RaisePropertyChanged(() => Loading);
                RaisePropertyChanged(() => CanEdit);
                RaisePropertyChanged (() => UnfilledShiftsEnabled);
            }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set { m_editMode = value; RaisePropertyChanged(() => EditMode); }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        private string m_totals;
        public string Totals
        {
            get { return m_totals; }
            set { m_totals = value; RaisePropertyChanged(() => Totals); }
        }

        private int m_startHour;
        public int StartHour
        {
            get { return CanChangeStartTime ? m_startHour : 0; }
            set 
            {
                if(CanChangeStartTime == true)
                    m_startHour = value; 
                RaisePropertyChanged(() => StartHour); 
            }
        }

        public bool CanChangeStartTime
        {
            get { return m_schedule.DayIndex.HasValue; }
        }

        private List<FilterGroup> m_filters;
        public List<FilterGroup> Filters
        {
            get { return m_filters; }
            set { m_filters = value; RaisePropertyChanged(() => Filters); }
        }

        private FilterGroup m_currentFilter;
        public FilterGroup CurrentFilter
        {
            get { return m_currentFilter; }
            set { m_currentFilter = value; RaisePropertyChanged(() => CurrentFilter); }
        }

        private string m_filterText;
        public string FilterText
        {
            get { return m_filterText; }
            set { m_filterText = value; RaisePropertyChanged(() => FilterText); }
        }

        public bool CanUndo
        {
            get { return m_undoService.CanUndo; }
        }

        public bool CanEdit
        {
            get 
            { 
                return !m_loading && m_essConfigurationService.CanEditSchedule && !ConfigurationService.IsScheduleEditorReadOnlyForUserRole;
            }
        }

        public bool ReadOnlySchedule
        {
            get 
            { 
                return !m_essConfigurationService.CanEditSchedule;
            }
        }

        private bool _scheduleHasUnfilledShifts;
        public bool ScheduleHasUnfilledShifts 
        {
            get 
            {
                return _scheduleHasUnfilledShifts;
            }
            set 
            {
                _scheduleHasUnfilledShifts = value;
                RaisePropertyChanged (() => ScheduleHasUnfilledShifts);
                RaisePropertyChanged (() => UnfilledShiftsEnabled);
            }
        }

        private int _unfilledShiftCount;
        public int UnfilledShiftsCount {
            get 
            {
                return _unfilledShiftCount;
            }
            set 
            {
                _unfilledShiftCount = value;
                RaisePropertyChanged (() => UnfilledShiftsCount);
            }
        }

        public bool UnfilledShiftsEnabled 
        {
            get 
            {
                return !m_loading && ScheduleHasUnfilledShifts;
            }
        }

        public bool ShowFillUnfilledShifts 
        {
            get 
            {
                return m_essConfigurationService.ShowFillUnfilledShifts;
            }
        }

        public bool CanFilterByTime
        {
            get
            {
                return !m_essConfigurationService.UseCustomScheduleRequest;
            }
        }

        public ScheduleViewModel (
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            IAlertBox alertBox, 
            SessionService session,
            ScheduleUndoService undoService,
            ScheduleOvertimeService overtimeService,
            ScheduleBreaksService breaksService,
            IESSConfigurationService essConfigurationService,
            SpareShiftService spareShiftService)
        {
            m_schedule = schedule;
            m_messenger = messenger;
            m_alertBox = alertBox;
            m_session = session;
            m_undoService = undoService;
            m_overtimeEditMode = overtimeService;
            m_breaksEditMode = breaksService;
            m_essConfigurationService = essConfigurationService;
            m_weekNumberDisplayed = essConfigurationService.WeekNumberDisplayed;
            m_spareShiftsService = spareShiftService;
        }

        public void Init ()
        {
            m_scheduleReadBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage> (OnScheduleReadBegin);
            m_scheduleReadEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleReadEnd);
            m_scheduleWriteBeginToken = m_messenger.SubscribeOnMainThread<ScheduleWriteBeginMessage> (OnScheduleWriteBegin);
            m_scheduleWriteEndToken = m_messenger.SubscribeOnMainThread<ScheduleWriteEndMessage> (OnScheduleWriteEnd);
            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage> (OnScheduleUpdate);
            m_startTimeChangedToken = m_messenger.SubscribeOnMainThread<StartTimeChangedMessage> (OnStartTimeChanged);
            m_editModeChangedToken = m_messenger.SubscribeOnMainThread<OnEditModeMessage>(OnEditModeChanged);
            m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleRefreshMessage> (OnScheduleRefresh);

            if (m_schedule.FilterNeedsResetOnRefresh)
            {
                m_schedule.Cancel ();
                m_schedule.ClearData ();
            }

            UpdateAll ();

            if(m_schedule.Schedule == null)
            {
                m_schedule.Refresh (false);
            }

            if (m_overtimeEditMode.IsEdited || m_breaksEditMode.IsEdited || (m_schedule.Schedule != null && m_schedule.Schedule.IsModified))
            {
                m_schedule.Revert();
                m_spareShiftsService.Reset ();
                m_overtimeEditMode.Reset();
                m_breaksEditMode.Reset();
            }

            SetEdit(false);

            //if (m_settings.AppConfig.AlternativeScheduleView)
            if (m_essConfigurationService.AlternativeScheduleView)
                ShowViewModel<ScheduleGridGraphViewModel> ();
            else
                ShowViewModel<ScheduleGridViewModel> ();

            SelectedDayIndex = (m_schedule.DayIndex == null) ? 0 : m_schedule.DayIndex.Value + 1;
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();

            //Called on view closed to ensure that we clear the state when going to the till allocation view 
            m_breaksEditMode.Reset();
            m_overtimeEditMode.Reset();

            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleReadBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleReadEndToken);
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
            m_messenger.Unsubscribe<OnEditModeMessage>(m_editModeChangedToken);
        }

        public void ShowUnfilledShifts ()
        {
            ShowViewModel<UnfilledShiftsViewModel> ();
        }

        List<TabTypes> Tabs
        {
            get
            {
                List<TabTypes> tabs = new List<TabTypes> ();

                //if (m_settings.AppConfig.AlternativeScheduleView)
                if (m_essConfigurationService.AlternativeScheduleView)    
                {
                    tabs.Add (TabTypes.ScheduleAlternative);
                }
                else
                {
                    tabs.Add (TabTypes.Schedule);
                    tabs.Add (TabTypes.Workload);
                }

                if (m_essConfigurationService.ShowPlanningTab)
                {
                    tabs.Add (TabTypes.Planning);
                }

                if (!m_essConfigurationService.AlternativeScheduleView)
                {
                    tabs.Add (TabTypes.Report);
                }

                if (m_essConfigurationService.ShowBreaksTab)
                {
                    tabs.Add (TabTypes.Breaks);
                }

                if (m_essConfigurationService.ShowResourcesTab)
                {
                    tabs.Add (TabTypes.Resources);
                }

                return tabs;
            }
        }

        public List<string> TabNames
        {
            get
            {
                List<string> tabNames = new List<string> ();
                foreach(TabTypes tab in this.Tabs)
                {
                    switch(tab)
                    {
                        case TabTypes.Schedule:
                        case TabTypes.ScheduleAlternative:
                            tabNames.Add(Localiser.Get("schedule"));
                            break;
                        case TabTypes.Workload:
                            tabNames.Add(Localiser.Get("workload"));
                            break;
                        case TabTypes.Report:
                            tabNames.Add(Localiser.Get("report"));
                            break;
                        case TabTypes.Planning:
                            tabNames.Add(Localiser.Get("planning"));
                            break;
                        case TabTypes.Breaks:
                            tabNames.Add(Localiser.Get("breaks"));
                            break;
                        case TabTypes.Resources:
                            tabNames.Add(Localiser.Get("resources"));
                            break;
                        default:
                            break;
                    }
                }

                return tabNames;
            }
        }

        public void OnTabSelect(int index)
        {
            var tabs = Tabs;

            switch (tabs [index])
            {
                case TabTypes.Schedule:
                    ShowViewModel<ScheduleGridViewModel> ();
                    break;
                case TabTypes.ScheduleAlternative:
                    ShowViewModel<ScheduleGridGraphViewModel> ();
                    break;
                case TabTypes.Workload:
                    ShowViewModel<ScheduleGraphViewModel> (false);
                    break;
                case TabTypes.Planning:
                    ShowViewModel<ScheduleDailyPlanningViewModel> ();
                    break;
                case TabTypes.Report:
                    ShowViewModel<ScheduleReportViewModel> ();
                    break;
                case TabTypes.Breaks:
                    ShowViewModel<ScheduleBreaksViewModel> ();
                    break;
                case TabTypes.Resources:
                    ShowViewModel<ScheduleResourcePlanningViewModel> ();
                    break;
                default:
                    throw new ArgumentOutOfRangeException ();
            }
        }

        public void OnWeekSelect(int index)
        {
            bool needsUpdate = false;
            if (index == 0)
            {
                needsUpdate = m_schedule.DayIndex != null;
                m_schedule.SetWeek ();
                UpdateTotals ();

            }
            else if (index >= 1 && index <= 8)
            {
                needsUpdate = m_schedule.DayIndex == null;
                m_schedule.SetDay (index-1);
                m_spareShiftsService.SetDay (index - 1);
                UpdateTotals ();
            }

            if(needsUpdate)
            {
                OnSetTime(StartHour);
                RaisePropertyChanged(() => StartHour);
                RaisePropertyChanged(() => CanChangeStartTime);
            }
            UpdateDate (m_schedule.StartDate, m_schedule.StartDay);
        }

        public void OnViewAuditSelected()
        {
            ShowViewModel<OvertimeAuditViewModel>();
        }

        public void OnStartTimeSelected()
        {
            ShowViewModel<StartTimeViewModel>(StartHour);
        }

        public void OnClickUnfilledShifts()
        {
            if (m_schedule.Schedule != null)
            {
                //var data = JsonConvert.SerializeObject(m_schedule.Schedule.UnfilledShifts);
                //ShowViewModel<UnfilledShiftsViewModel>(new { info = data });
                ShowViewModel<UnfilledShiftsViewModel>();
            }
        }

        private void CheckModified(Action<int> saveCallback)
        {
            if ((m_schedule.Schedule != null && m_schedule.Schedule.IsModified) || m_overtimeEditMode.IsEdited || m_breaksEditMode.IsEdited)
            {
                m_alertBox.ShowOptions (Localiser.Get("schedule_modified"), Localiser.Get("schedule_changes_message"), 
                    new List<string> () { Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                    Localiser.Get("continue_editing_message"), saveCallback );
            }
            else
            {
                saveCallback(2);
            }
        }

        public void OnMoveNext()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;

                    ClearUnfilledShiftsCount();
                    m_schedule.MoveNext (shouldSave);
                    m_spareShiftsService.Refresh(shouldSave, m_schedule.CurrentFilter);

                    if(shouldSave)
                    {
                        if(m_overtimeEditMode.IsEdited)
                            m_overtimeEditMode.Save();

                        if(m_breaksEditMode.IsEdited)
                            m_breaksEditMode.Save();
                    }
                }
            });
        }

        public void OnMovePrev()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;

                    ClearUnfilledShiftsCount();
                    m_schedule.MovePrev (shouldSave);
                    m_spareShiftsService.Refresh(shouldSave, m_schedule.CurrentFilter);

                    if(shouldSave)
                    {
                        if(m_overtimeEditMode.IsEdited)
                            m_overtimeEditMode.Save();

                        if(m_breaksEditMode.IsEdited)
                            m_breaksEditMode.Save();
                    }
                }
            });
        }

        public void OnRefresh()
        {
            CheckModified ((i) => {
                if (i > 0) 
                { 
                    bool shouldSave = i == 1;

                    ClearUnfilledShiftsCount();
                    m_schedule.Refresh (shouldSave);
                    m_spareShiftsService.Refresh(shouldSave, m_schedule.CurrentFilter);

                    if(shouldSave)
                    {
                        if(m_overtimeEditMode.IsEdited)
                            m_overtimeEditMode.Save();

                        if(m_breaksEditMode.IsEdited)
                            m_breaksEditMode.Save();
                    }
                }
            });
        }

        public void OnSetTime(int hour)
        {
            CheckModified (async (i) => {
                if (i > 0) { 
                    var time = m_schedule.StartDate;
                    var newStart = new DateTime(time.Year, time.Month, time.Day, hour, 0, 0);
                    StartHour = hour;
                    await m_schedule.SafeWriteAsync(i == 1);
                    await m_schedule.SafeReadAsync (newStart, m_schedule.CurrentFilter);
                }
            });
        }

        public void OnApplyFilter(FilterGroup filter)
        {
            //Check for unselecting everything
            if(filter != null && filter.Items.All(p => !p.Selected))
                filter = null;

            if ((filter == null && m_schedule.CurrentFilter != null) ||
                (filter != null && filter.Items.Any(p => p.Selected == true)))
            {
                //Clear all filters
                CheckModified (async (i) => {
                    if (i > 0) 
                    { 
                        await m_schedule.SafeWriteAsync(i == 1);
                        await m_spareShiftsService.Refresh(i == 1, filter);
                        await m_schedule.SafeReadAsync (m_schedule.StartDate, filter); 
                    }
                });
            }
        }

        public void OnEdit()
        {
            if (EditMode)
            {
                if ((m_schedule.Schedule != null && m_schedule.Schedule.IsModified) || m_overtimeEditMode.IsEdited || m_breaksEditMode.IsEdited )
                {
                    m_alertBox.ShowOptions(
                        Localiser.Get("save_schedule"), 
                        Localiser.Get("save_schedule_ellipsis_message"), 
                        new List<string>() {  Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                        Localiser.Get("continue_editing_message"),
                        (i) =>
                        {
                            switch (i)
                            {
                                case 1:
                                    Save();
                                    break;
                                case 2:
                                    Revert();
                                    break;
                            }
                        });
                }
                else
                {
                    // End Edit Mode
                    SetEdit (false);
                }
            }
            else
            {
                // Start Edit mode
                SetEdit (true);
            }
        }

        private void ClearUnfilledShiftsCount ()
        {
            UnfilledShiftsCount = 0;
        }

        void Revert()
        {
            Loading = true;

            m_schedule.Revert();
            SetEdit(false);
            m_schedule.Refresh (false);
            m_spareShiftsService.Refresh (false, m_schedule.CurrentFilter);
        }

        async void Save()
        {
            Loading = true;

            await m_overtimeEditMode.Save();
            await m_breaksEditMode.Save();

            m_spareShiftsService.Refresh(true, m_schedule.CurrentFilter);
            m_schedule.Refresh(true); 
        }

        private void SetEdit(bool canEdit)
        {
            m_schedule.SetEdit (canEdit);

            EditMode = canEdit;

            if(!canEdit)
                m_undoService.ClearHistory();
        }

        private void OnScheduleWriteBegin(ScheduleWriteBeginMessage message)
        {
            Loading = true;
            LoadingMessage = m_schedule.State == ScheduleService.UpdateState.Saving ? " "+Localiser.Get("saving_ellipsis") : Localiser.Get("loading_ellipsis_label");
        }

        private void OnScheduleWriteEnd(ScheduleWriteEndMessage message)
        {
            Loading = false;

            UpdateScheduleUnfilledShifts ();
        }

        private void UpdateScheduleUnfilledShifts ()
        {
            if (m_schedule.DayIndex == null) {
                ScheduleHasUnfilledShifts = m_schedule.Schedule == null ?
                             false :
                             m_schedule.Schedule.UnfilledShifts == null ?
                               false :
                               m_schedule.Schedule.UnfilledShifts.Any ();



                UnfilledShiftsCount = m_schedule.Schedule == null ?
                                                0 :
                                                m_schedule.Schedule.UnfilledShifts == null ?
                                                0 :
                                                m_schedule.Schedule.UnfilledShifts
                                                .Where (shift => !IsWithinPreventChangesTime (shift.Start))
                                                .ToList ()
                                                .Count;
            } 
            else 
            {
                ScheduleHasUnfilledShifts = m_schedule.Schedule == null ?
                           false :
                           m_schedule.Schedule.UnfilledShifts == null ?
                             false :
                              m_schedule.Schedule.UnfilledShifts
                              .Where(shift => IsForCurrentDay(shift.Start))
                              .ToList()
                              .Any ();

                UnfilledShiftsCount = m_schedule.Schedule == null ?
                                                0 :
                                                m_schedule.Schedule.UnfilledShifts == null ?
                                                0 :
                                                m_schedule.Schedule.UnfilledShifts
                                                .Where (shift => IsForCurrentDay(shift.Start))
                                                .Where (shift => !IsWithinPreventChangesTime (shift.Start))
                                                .ToList ()
                                                .Count;
            }
        }

        private bool IsForCurrentDay (DateTime start)
        {
            if (start == DateTime.MinValue || start == DateTime.MaxValue) 
            {
                return false;
            }

            var dayIndex = start.GetJDADayIndexForDateTime(m_schedule.StartDay.GetValueOrDefault(0));

            return dayIndex == m_schedule.DayIndex;
        }

        private bool IsWithinPreventChangesTime (DateTime testTime)
        {
            int hours = m_essConfigurationService.PreventChangesHours;
            if (hours <= 0)
                return false;

            if (testTime < DateTime.Now.AddHours (hours))
                return true;

            return false;
        }

        private void OnScheduleRefresh(ScheduleRefreshMessage message)
        {
            OnRefresh ();
        }

        private void OnScheduleReadBegin(ScheduleReadBeginMessage message)
        {
            NoData = false;
            Loading = true;
            ClearUnfilledShiftsCount();
            WeekSelect = m_schedule.DayIndex.HasValue ? m_schedule.DayIndex.Value + 1 : 0; 
            LoadingMessage = m_schedule.State == ScheduleService.UpdateState.Saving ? " "+Localiser.Get("saving_ellipsis") : Localiser.Get("loading_ellipsis_label");
            Totals = string.Empty;

            UpdateDate (m_schedule.StartDate, m_schedule.StartDay);
        }

        private void OnScheduleReadEnd(ScheduleReadEndMessage message)
        {
            //Wait until the operations are complete if they have been run
            //if(m_overtimeSaveOperation != null)
            //    await m_overtimeSaveOperation;
            //
            //await m_breaksSaveOperation;
            Loading = false;

            if (message.SessionTimeout)
            {
                m_session.SignOut ();
            }
            else if(message.NoData)
            {
                Loading = true;
                NoData = true;
                LoadingMessage = Localiser.Get("no_data_loaded");
                return;
            }
            else if (!message.Success)
            {
                //m_alertBox.ShowOK (Localiser.Get("schedule_error"), message.Message);
                return;
            }

            UpdateScheduleUnfilledShifts ();

            NoData = false;
            UpdateAll ();
        }

        private void OnOrgEnd(SetOrgEndMessage message)
        {
            if (message.SessionTimeout)
            {
                m_session.SignOut ();
            }
            else
            {
                m_schedule.Refresh (false);
                m_spareShiftsService.Refresh(false, m_schedule.CurrentFilter);
            }
        }

        private void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            if (message.UpdateTotals)
                UpdateTotals ();

            EditMode = m_schedule.State == ScheduleService.UpdateState.Edit;
            RaisePropertyChanged(() => CanUndo);
            UpdateScheduleUnfilledShifts ();
        }

        private void OnStartTimeChanged(StartTimeChangedMessage message)
        {
            OnSetTime(message.StartTime);
        }

        private void UpdateAll()
        {
            // Update view
            Loading = m_schedule.IsBusy;

            LoadingMessage = m_schedule.State == ScheduleService.UpdateState.Saving ? " "+Localiser.Get("saving_ellipsis") : Localiser.Get("loading_ellipsis_label");
            WeekSelect = m_schedule.DayIndex.HasValue ? m_schedule.DayIndex.Value + 1 : 0; 
            StartHour = m_schedule.StartDate.Hour;
            Filters = m_schedule.Filters == null ? new List<FilterGroup>() : m_schedule.Filters.Groups;
            CurrentFilter = m_schedule.CurrentFilter;
            FilterText = m_schedule.CurrentFilter == null ? string.Empty : m_schedule.CurrentFilter.GetDescription ();
            EditMode = m_schedule.State == ScheduleService.UpdateState.Edit;
            m_spareShiftsService.SetEdit (EditMode);
            UpdateDate(m_schedule.StartDate, m_schedule.StartDay);
            UpdateTotals ();
            Debug.WriteLine("Data Updated: "+DateTime.Now.ToString("HH:mm:ss.f"));
        }

        private void UpdateTotals()
        {
            if (m_schedule.Schedule == null || m_schedule.Accuracy == null)
                return;

            StringBuilder sb = new StringBuilder();

            if (m_schedule.Schedule.DayIndex.HasValue)
            {
                sb.AppendFormat("Schedule Accuracy: {0}%", m_schedule.Accuracy.DayAccuracy[m_schedule.Schedule.DayIndex.Value].ToString ("n2"));

                if (m_essConfigurationService.ShowScheduleCost)
                {
                    sb.AppendFormat("\t\tSchedule Cost: {0}{1}", CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, m_schedule.Schedule.DayCost[m_schedule.Schedule.DayIndex.Value].ToString ("n2"));
                }
            }
            else
            {
                sb.AppendFormat("Schedule Accuracy: {0}%", m_schedule.Accuracy.WeekAccuracy.ToString ("n2"));

                if (m_essConfigurationService.ShowScheduleCost)
                {
                    sb.AppendFormat("\t\tSchedule Cost: {0}{1}", CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, m_schedule.Schedule.WeekCost.ToString ("n2"));
                }
            }

            Totals = sb.ToString();
        }

        private void UpdateDate(DateTime start, int? startDay)
        {
            if (m_schedule.DayIndex.HasValue)
            {
                var day = start.AddDays (m_schedule.DayIndex.Value);
                int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (start);
                Date = m_weekNumberDisplayed ?
                    string.Format (Localiser.Get("week_starting_label"), weekNumber, day.ToString(Localiser.Get("dddd, dd/MM/yyyy"))) :
                    string.Format (day.ToString(Localiser.Get("dddd, dd/MM/yyyy")));
            }
            else
            {
                int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (start);
                Date = m_weekNumberDisplayed ?
                    string.Format (Localiser.Get("week_starting_label"), weekNumber, start.ToString (Localiser.Get("dd/MM/yyyy"))) :
                    string.Format (Localiser.Get("starting_label"),  start.ToString (Localiser.Get("dd/MM/yyyy"))) ;
            }

            // Update days in segments
            List<string> daySegments = new List<string>();
            daySegments.Add(Localiser.Get("week_label"));

            for (int i = 0; i < 7; i++)
            {
                int dayIndex = (int)start.AddDays(i).DayOfWeek;

                switch(dayIndex)
                {
                    case 0: daySegments.Add(Localiser.Get("sunday_short")); break;
                    case 1: daySegments.Add(Localiser.Get("monday_short")); break;
                    case 2: daySegments.Add(Localiser.Get("tuesday_short")); break;
                    case 3: daySegments.Add(Localiser.Get("wednesday_short")); break;
                    case 4: daySegments.Add(Localiser.Get("thursday_short")); break;
                    case 5: daySegments.Add(Localiser.Get("friday_short")); break;
                    case 6: daySegments.Add(Localiser.Get("saturday_short")); break;
                }
            }

            if(Segments == null || Segments.Count == 0 || Segments[1] != daySegments[1])
                Segments = daySegments;
        }

        public void OnUndo()
        {
            if(m_undoService.CanUndo)
                m_undoService.Undo();
        }

        void OnEditModeChanged(OnEditModeMessage obj)
        {
            SetEdit(true);
        }

        private void OnSignOut(SignOutMessage message)
        {
            // Unsubscribe from messages
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
            m_messenger.Unsubscribe<OnEditModeMessage>(m_editModeChangedToken);
            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleReadBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleReadEndToken);
            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
            m_messenger.Unsubscribe<StartTimeChangedMessage>(m_startTimeChangedToken);
            m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
            m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleRefreshMessage> (OnScheduleRefresh);
        }

        public void OnDateButtonPressed()
        {
            DateTime selectedDate = (m_schedule.DayIndex == null) ? m_schedule.StartDate.AddDays(0) : m_schedule.StartDate.AddDays(m_schedule.DayIndex.Value);
            bool isWeekSelectedBool = !m_schedule.DayIndex.HasValue;
            ShowViewModel<DatePickerViewModel>(new{ currentWeek = m_schedule.StartDate, numberOfWeeks = 2000, isWeekSelected = isWeekSelectedBool, startingWeekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (m_schedule.StartDate), currentDay = selectedDate});
        }

        public void OnReturn(SelectedPickerItem item)
        {
            if (item == null)
                return;
            
            if (item.IsWeek)
            {
                m_schedule.SetWeek ();
                UpdateTotals ();
            }
            else
            {
                // sunday in enum is 0, put it to correct number for method
                int dayOfWeek = (int)item.Date.DayOfWeek;// == DayOfWeek.Sunday) ? 6 : (int)item.Date.DayOfWeek - 1;

                // set the day index
                if (dayOfWeek >= 0 && dayOfWeek <= 6)
                {
                    m_schedule.SetDay (dayOfWeek);
                    m_spareShiftsService.SetDay (dayOfWeek);
                    UpdateTotals ();
                }
            }
                
            // get start of week for date, then set schedule
            DateTime startOfWeek = item.Date.StartOfWeek(m_schedule.StartDate.DayOfWeek);

            CheckModified (async (i) => {
                if (i > 0) { 
                    await m_schedule.SafeWriteAsync(i == 1);
                    await m_schedule.SafeReadAsync (startOfWeek, m_schedule.CurrentFilter);
                }
            });
        }
    }
}
