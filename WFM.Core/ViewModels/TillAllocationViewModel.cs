﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class TillAllocationViewModel : ConsortiumViewModel
    {
        private int m_editMode;
        public int EditMode
        {
            get { return m_editMode; }
            set { m_editMode = value; RaisePropertyChanged(() => EditMode); }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }


        public TillAllocationViewModel() : base()
        {
        }

        public void Init()
        {
            OnTabSelect(0); // Initially we can programmatically 'click' the first tab

            UpdateAll();
        }

        public void OnTabSelect(int index)
        {
            switch(index)
            {
                case 0:
                    ShowViewModel<TillAllocationTillsViewModel> ();
                    break;
                case 1:
                    ShowViewModel<TillAllocationAllocationsViewModel> ();
                    break;
            }
        }

        private void SetEdit(bool canEdit)
        {
            EditMode = canEdit ? 1 : 0;
        }

        public void OnEdit()
        {
            if (EditMode == 1)
            {
                // End Edit Mode
                SetEdit (false);
            }
            else
            {
                // Start Edit mode
                SetEdit (true);
            }
        }

        private void BeginLoad()
        {
            Loading = true;
        }

        private void EndLoad()
        {
            Loading = false;

            UpdateAll ();
        }

        private void UpdateAll()
        {
            EditMode = 0;
        }

    }
}

