﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using Newtonsoft.Json;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class StartTimeViewModel : WFMViewModel
    {
        public class TimeOffset
        {
            private int m_offset;
            public int Offset
            {
                get { return m_offset; }
                set { m_offset = value; }
            }

            private string m_offsetString;
            public string OffsetString
            {
                get { return m_offsetString; }
                set { m_offsetString = value; }
            }

            private bool m_selected;
            public bool Selected
            {
                get { return m_selected; }
                set { m_selected = value; }
            }

            public TimeOffset(int offset, string formattedOffset, bool selected = false)
            {
                Offset = offset;
                OffsetString = formattedOffset;
                Selected = selected;
            }
        }

        private readonly IMvxMessenger m_messenger;

        private List<TimeOffset> m_offsets;
        public List<TimeOffset> Offsets
        {
            get { return m_offsets; }
            set 
            {
                m_offsets = value;
                RaisePropertyChanged(() => Offsets);
            }
        }

        public StartTimeViewModel(IMvxMessenger messenger)
        {
            m_messenger = messenger;
        }

        public void Init(int info)
        {
            Analytics.TrackAction ("Start Time","Show");

            int startHour = info;

            List<TimeOffset> offsets = new List<TimeOffset>();
            for (int i = 0; i < 24; i++)
            {
                DateTime offsetTime = DateTime.MinValue.AddHours(i);
                
                offsets.Add(new TimeOffset(i, TimeFormatter.FormatTime(offsetTime), startHour == i));
            }

            Offsets = offsets;
        }

        public void OnOffsetSelected(TimeOffset selected)
        {
            Analytics.TrackAction ("Start Time","Select", selected.ToString());

            // Send a message instead of call close with the data as
            // it won't currently get passed back to the presenting viewmodel.
            m_messenger.Publish (new StartTimeChangedMessage (this) {
                StartTime = selected.Offset
            });

            Close();
        }
    }
}

