﻿using System;
using System.Collections.Generic;

namespace WFM.Core
{
    public class EmployeePickerViewModel : WFMViewModel
    {
        private List<EmployeePickerData> m_employeeList;
        public List<EmployeePickerData> EmployeeList
        {
            get { return m_employeeList; }
            set
            {
                m_employeeList = value;
                RaisePropertyChanged (() => EmployeeList);
            }
        }

        public string Subtitle { get; set; }

        private bool m_closing = false;

        public EmployeePickerViewModel ()
        {
            
        }

        public void Init(List<EmployeePickerData> employeeList, string subtitle)
        {
            EmployeeList = employeeList;
            Subtitle = subtitle;
        }

        public void OnEmployeeToggle(EmployeePickerData employee)
        {
            employee.Picked = !employee.Picked;
        }

        public void OnClearAll()
        {
            SetAll (false);
        }

        public void OnSelectAll()
        {
            SetAll (true);
        }

        private void SetAll(bool picked)
        {
            foreach (var employee in EmployeeList)
            {
                employee.Picked = picked;
            }   
        }

        public void OnDone()
        {
            if(m_closing)
            {
                return;
            }
            m_closing = true;
            Close (EmployeeList);
        }

        public override void OnViewClosed ()
        {
            if(m_closing)
            {
                return;
            }
            m_closing = true;
            Close ();
        }
    }
}

