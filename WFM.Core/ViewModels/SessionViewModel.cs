﻿using System.Diagnostics;
using System;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class SessionViewModel : WFMViewModel
    {
        private readonly SessionService m_session;
        private readonly SettingsService m_settings;
        private readonly IUserDetailsService m_userDetailsService;
        private readonly SessionData m_sessionData;

        protected MvxSubscriptionToken m_signOutToken;
        protected bool m_retry;

        public string Logo { get; set; }
        public string Background { get; set; }

        private string m_username;
        public string UserName
        { 
            get { return m_username; }
            set { m_username = value; RaisePropertyChanged(() => UserName); }
        }

        private string m_password;
        public string Password
        { 
            get { return m_password; }
            set { m_password = value; RaisePropertyChanged(() => Password); }
        }

        private bool m_loading;
        public bool Loading
        { 
            get { return m_loading; }
            set 
            { 
                m_loading = value; 
                RaisePropertyChanged(() => Loading); 
            }
        }

        private string m_loadingText;
        public string LoadingText {
            get { return m_loadingText; }
            set { m_loadingText = value; RaisePropertyChanged (() => LoadingText); }
        }

        private string m_message;
        public string Message
        { 
            get { return m_message; }
            set { m_message = value; RaisePropertyChanged(() => Message); }
        }

        private string m_version;
        public string Version
        { 
            get { return m_version; }
            set { m_version = value; RaisePropertyChanged(() => Version); }
        }

        public bool IsLoggedIn
        {
            get { return m_session.IsSignedIn; }
        }

        public bool HideSettingsButton
        {
            get
            {
                return m_settings == null || m_settings.Environments == null || 
                       (m_settings.Environments.Count <= 1 && !ConfigurationService.CanEditEnvironments && !ConfigurationService.ShowSettingsButton);
            }
        }

        public bool ShowResetPassword
        {
            get { return  m_sessionData.CanResetPassword; }
            set { RaisePropertyChanged(() => ShowResetPassword); }
        }

        private bool m_showCredentials;
        public bool ShowCredentials 
        {
            get { return m_showCredentials; }
            set { m_showCredentials = value; RaisePropertyChanged (() => ShowCredentials); }
        }

        private bool m_showSSOLogin;
        public bool ShowSSOLogin 
        {
            get { return m_showSSOLogin; }
            set { m_showSSOLogin = value; RaisePropertyChanged (() => ShowSSOLogin); }
        }

        private string m_ssoAddress;
        public string SSOAddress 
        {
            get { return m_ssoAddress; }
            set { m_ssoAddress = value; RaisePropertyChanged (() => SSOAddress); }
        }

        private bool m_ssoHasToken;
        public bool SSOHasToken 
        {
            get { return m_ssoHasToken; }
            set { m_ssoHasToken = value; RaisePropertyChanged (() => SSOHasToken); }
        }

        private string m_wfmAddress;
        public string WFMAddress 
        {
            get { return m_wfmAddress; }
            set { m_wfmAddress = value; RaisePropertyChanged (() => WFMAddress); }
        }

        private bool m_clearCookies;
        public bool ClearCookies 
        {
            get { return m_clearCookies; }
            set { m_clearCookies = value; RaisePropertyChanged (() => ClearCookies); }
        }

        public SessionViewModel(SessionService session,
            IMvxMessenger messenger,
            IMobileApp app, 
            SettingsService settings, 
            IUserDetailsService userDetailsService, 
            SessionData sessionData)
        {
            Debug.WriteLine("Login Screen Loaded: "+DateTime.Now.ToString("HH:mm:ss.f"));

            m_session = session;
            m_settings = settings;
            m_userDetailsService = userDetailsService;
            m_sessionData = sessionData;
            m_signOutToken = messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_retry = true;

            string format = "v{0}.{1}.{2}";
        #if DEBUG
            format = "v{0}.{1}.{2}.{3}";
        #endif
            string[] versions = app.Version.Split(new char[] { '.' });

            Version = string.Format (format, versions[0], versions[1], versions[2], versions[3]);
        }

        public void OnReturn()
        {
            RaisePropertyChanged(() => ShowResetPassword);
        }

        public void OnReturn(ResetPasswordViewModel.ReturnState state)
        {
            RaisePropertyChanged(() => ShowResetPassword);
        }

        public void OnReturn (Environment env)
        {
            RaisePropertyChanged (() => ShowResetPassword);

            if (env != null) 
            {
                UpdateViewForEnv (env);
            }
        }

        public void OnReturn(string newPassword)
        {
            RaisePropertyChanged(() => ShowResetPassword);
            Password = newPassword;
            OnSignIn();
        }

        public void Init()
        {
            Message = string.Empty;
            LoadingText = Localiser.Get ("signing_in_ellipsis");
            Logo = "assets://Theme/Logo.png";
            Background = "assets://Theme/Background.jpg";

            //Autofill if we can
            UserName = m_userDetailsService.GetUsername();

            // Update view for any env specific settings
            UpdateViewForEnv (Settings.Current.Env);

            if (SSOAddress != null) {
                OnSignIn ();
                return;
            }

#if DEBUG
            if (String.IsNullOrEmpty(UserName))
            {
                m_userDetailsService.ClearPassword();
            }
            else
            {
                Password = m_userDetailsService.GetPassword();
            }
#endif
        }

        public Task OnSignIn ()
        {
            Task signInTask;

            if (SSOAddress != null) 
            {
                ShowSSOLogin = true;
                Loading = true;
                ShowCredentials = false;
                 
                signInTask = Task.FromResult (true);
            }
            else 
            {
                signInTask = OnSignIn (UserName, Password);
            }

            return signInTask;
        }

        public async Task OnSignIn (string username, string password)
        {
            Message = string.Empty;
            Loading = true;
            ShowCredentials = false;

            try
            {
                Debug.WriteLine("Login Attempt Begun: "+  DateTime.Now.ToString("HH:mm:ss.f"));

                var result = await m_session.SignIn(username, password);
                Debug.WriteLine("Login Attempt Completed: "+ DateTime.Now.ToString("HH:mm:ss.f"));

                if (result.Success)
                {
                    m_userDetailsService.SetUsername( username );

#if DEBUG
                    m_userDetailsService.SetPassword( password );
#endif

                    OnSignInSuccessful();
                }
                else
                {
                    switch(result.FailReason)
                    {
                        case SignInFailReason.AlreadySignedIn:
                            OnSignInFailed(Localiser.Get("already_sign_in"));
                            break;
                        case SignInFailReason.InvalidPassword:
                        case SignInFailReason.InvalidUsername:
                            OnSignInFailed(Localiser.Get("enter_valid_username_password"));
                            break;
                        case SignInFailReason.PasswordExpired:
                            OnChangePassword();
                            break;
                        case SignInFailReason.NetworkError:
                            OnSignInFailed(Localiser.Get("network_connection_error"));
                            break;
                        case SignInFailReason.DataError:
                            OnSignInFailed(Localiser.Get("invalid_server_response"));
                            break;
                        case SignInFailReason.Timeout:
                            OnSignInFailed(Localiser.Get("connection_timed_out_message"));
                            break;
                        default:
                            if(result.Message != null)
                            {
                                OnSignInFailed(result.Message);
                            }
                            else
                            {
                                OnSignInFailed("Sign in failed");
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                OnSignInFailed(ex.Message);
            }
        }

        public virtual void OnSignInSuccessful()
        {
            ShowViewModel<DrawerViewModel>();
            UserName = string.Empty;
            Password = string.Empty;
        }

        public void OnChangePassword()
        {
            Loading = false;
            ShowCredentials = string.IsNullOrWhiteSpace (SSOAddress);
            ShowViewModel<ChangePasswordViewModel>();
        }

        public virtual void OnSignInFailed(string message)
        {
            ShowSSOLogin = false;

            if (m_retry && message == SessionData.SAML_ERROR) 
            {
                m_retry = false;
                OnSignIn ();
            } 
            else 
            {
                m_retry = true;
                Loading = false;
                ShowCredentials = string.IsNullOrWhiteSpace (SSOAddress);
                SSOHasToken = false;
                ClearCookies = true;
                m_session.SignOut ();
                Message = message;
            }
        }

        public virtual void OnShowSettings()
        {
            ShowViewModel(typeof(SettingsViewModel));
        }

        private void OnSignOut(SignOutMessage message)
        {
            Loading = false;
            ShowCredentials = string.IsNullOrWhiteSpace (SSOAddress);
            m_retry = true;
            Message = message.SessionTimedOut ? Localiser.Get("session_timed_out") : Message;
            Password = string.Empty;
            ClearCookies = true;
            SSOHasToken = false;
            Close( ViewCloseType.ToCurrentView );
        }

        public void OnResetPassword()
        {
            ShowViewModel<ResetPasswordViewModel>(null);
        }

        public void OnSSOClose ()
        {
            OnSignInFailed ("Sign in cancelled");
        }

        public virtual bool CheckForToken (string body)
        {
            if (body.StartsWith (SessionData.SAML_TOKEN + "=", StringComparison.CurrentCultureIgnoreCase)) {
                var token = body.Substring (SessionData.SAML_TOKEN.Length + 1);
                TokenLogin (token);
                return true;
            }

            return false;
        }

        private Task TokenLogin (string token)
        {
            SSOHasToken = true;
            ShowSSOLogin = false;
            return OnSignIn(SessionData.SAML_TOKEN, token);
        }

        private void UpdateViewForEnv (Environment env)
        {
            ShowCredentials = string.IsNullOrWhiteSpace (env.SSOStart);
            SSOAddress = env.SSOStart;
            WFMAddress = env.Address;
        }
    }
}
