﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;
using WFM.Core.WFMPlus;

namespace WFM.Core
{
    public class ResetPasswordViewModel : WFMViewModel
    {        
        public enum ReturnState
        {
            ResetSent,
            ResetFailed,
            UserCancelled
        }

        private string m_errorMessage;
        public string ErrorMessage
        {
            get
            {
                return m_errorMessage;
            }

            set
            {
                m_errorMessage = value;
                RaisePropertyChanged(() => ErrorMessage);
            }
        }

        private string m_username;
        public string Username
        {
            get
            {
                return m_username;
            }
            set
            {
                m_username = value;
                RaisePropertyChanged(() => Username);
                RaisePropertyChanged(() => ShowSubmit);
            }
        }

        private bool m_showSubmit;
        public bool ShowSubmit
        { 
            get { return !string.IsNullOrWhiteSpace(Username); }
            set 
            { 
                RaisePropertyChanged(() => ShowSubmit); 
            }
        }

        public string Logo { get; set; }
        public string Background { get; set; }

        private bool IsClosing { get; set; }

        private readonly SessionData m_session;
        private readonly ISpinnerService m_spinner;
        private readonly IAlertBox m_alertBox;
        private readonly IStringService m_localiser;

        public ResetPasswordViewModel(SessionData session, ISpinnerService spinner, IAlertBox alertBox, IStringService localiser,IMobileApp mobileApp)
        {
            m_session = session;
            m_spinner = spinner;
            m_alertBox = alertBox;
            m_localiser = localiser;
        }

        public void Init ()
        {
            IsClosing = false;

            Logo = "assets://Theme/Logo.png";
            Background = "assets://Theme/Background.jpg";
        }

        public async void OnSubmitButton()
        {
            SpinnerToken token = m_spinner.Show("Sending");

            Task<ResetPasswordResult> changePasswordTask = m_session.ResetPassword(Username);

            await Task.Delay(500);
            ResetPasswordResult response = await changePasswordTask;

            m_spinner.Dismiss(token);

            if (response.Success)
            {
                m_alertBox.ShowOK(m_localiser.Get("reset_password_successtitle"), m_localiser.Get("reset_password_successmessage"),
                    (x) =>
                    {
                        Close(ReturnState.ResetSent);
                    });
            }
            else
            {
                m_alertBox.ShowOK (m_localiser.Get ("reset_password_failedtitle"),
                    m_localiser.Get (response.Message),
                    (x) =>
                    {

                    });
            }
        }

        public void OnCancelButton()
        {    
            IsClosing = true;

            Close (ReturnState.UserCancelled);
        }            

        public override void OnViewClosed ()
        {
            if(!IsClosing)
                Close (ReturnState.UserCancelled);

            base.OnViewClosed ();
        }
    }
}