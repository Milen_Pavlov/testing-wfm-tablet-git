using System;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Plugins.Messenger;
using WFM.Core.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using WFM.Core.Model;

namespace WFM.Core
{
    //note, this is intended to be the newer version of the date view model until breaks can be refactored
    public abstract class AllocationDateSelectionViewModel : WFMViewModel
    {
		protected readonly IMvxMessenger m_messenger;

        protected readonly ScheduleService m_schedule;
        protected readonly SessionService m_session;

        protected readonly TillAllocationService m_allocations;

        protected readonly IAlertBox m_alertBox;

        protected readonly SpareShiftService m_spareShiftsService;

        protected MvxSubscriptionToken m_scheduleBeginToken;
        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_orgEndToken;
        protected MvxSubscriptionToken m_scheduleUpdateToken;
        protected MvxSubscriptionToken m_allocationRetrievedToken;

        protected bool m_scheduleWorking;

        private bool m_weekNumberDisplayed = true;

        private DateTime? allocationsRetrievedFor = null;

        private int m_selectedDayIndex;
        public int SelectedDayIndex
        {
            get { return m_selectedDayIndex; }
            set { m_selectedDayIndex = value; RaisePropertyChanged(() => SelectedDayIndex); }
        }

        private int m_currentStartDay;

        private List<string> m_segments;
        public List<string> Segments
        {
            get { return m_segments; }
            set { m_segments = value; RaisePropertyChanged(() => Segments); }
        }

        public bool Loading
        {
			get 
			{ 
				return m_loadingSchedule || m_loadingService || m_loadingSubmitChanges; 
			}
            set {}
        }

		private string m_LoadingMessage = "Loading";
		public string LoadingMessage
		{
			get
			{
				return m_LoadingMessage;
			}
			set
			{
				m_LoadingMessage = value;
				RaisePropertyChanged(() => LoadingMessage);
			}
		}

        private IList<TillModel> m_tills;
        public IList<TillModel> Tills
        {
            get { return m_tills; }
            set
            {
                m_tills = value;
                RaisePropertyChanged(() => Tills);
            }
        }

        private bool m_loadingSchedule;
        public bool LoadingSchedule
        {
            get { return m_loadingSchedule; }
            set 
            {
                    m_loadingSchedule = value; 
                    RaisePropertyChanged(() => LoadingSchedule);
                    RaisePropertyChanged(() => Loading);
            }
        }

        private bool m_loadingService;
        public bool LoadingService
        {
            get { return m_loadingService; }
            set 
            { 
                m_loadingService = value; 
                RaisePropertyChanged(() => LoadingService); 
                RaisePropertyChanged(() => Loading);
            }
        }

		private bool m_loadingSubmitChanges;
		public bool LoadingSubmitChanges
		{
			get { return m_loadingSubmitChanges; }
			set 
			{ 
                m_loadingSubmitChanges = value; 
				RaisePropertyChanged(() => LoadingSubmitChanges); 
				RaisePropertyChanged(() => Loading);
			}
		}

        private DateTime m_startDate;
        public DateTime StartDate
        {
            get { return m_startDate; }
            set 
            { 
                m_startDate = value; 
                RaisePropertyChanged(() => StartDate);
            }
        }

        private int m_dayIndex;
        public int DayIndex
        {
            get { return m_dayIndex; }
            set { m_dayIndex = value; RaisePropertyChanged(() => DayIndex); }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        public DateTime SelectedDay
        {
            get
            {
                return StartDate.AddDays(DayIndex);
            }
        }

        public AllocationDateSelectionViewModel(
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            SessionService session, 
            IAlertBox alertBox,
            TillAllocationService allocationService,
            SpareShiftService spareShiftsService)
        {
            m_schedule = schedule;
            m_messenger = messenger;
            m_session = session;
            m_alertBox = alertBox;
            m_allocations = allocationService;
            m_spareShiftsService = spareShiftsService;

            m_scheduleWorking = false;
        }

        //public abstract void ReloadData();
        public abstract void UpdateView();

        public virtual async void Init()
        {  
            if (!m_allocations.CheckoutFilterApplied && !m_schedule.IsBusy)
            {
                LoadingSchedule = true;
                
                //Reset filter
                m_schedule.Cancel ();
                m_schedule.ClearData ();
                
                await m_schedule.Refresh (false);
            }

            AttachEvents ();

            StartDate = m_schedule.StartDate;

            if (m_schedule.DayIndex == null)
            {
                m_schedule.SetDay(0);
            }

            DayIndex = (int)m_schedule.DayIndex;

            UpdateDate(StartDate, m_schedule.StartDay);

            SelectedDayIndex = (m_schedule.DayIndex == null) ? 0 : m_schedule.DayIndex.Value;

            if (!m_schedule.IsBusy)
            {
                LoadAllocationsForPeriod();
            }
            else
            {
                LoadingSchedule = true;
            }
        }

        public virtual void AttachEvents()
        {
            if(m_scheduleBeginToken == null)
                m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage>(OnScheduleBegin);
            
            if(m_scheduleEndToken == null)
                m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage>(OnScheduleEnd);
            
            if(m_orgEndToken == null)
                m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            
            if(m_scheduleUpdateToken == null)
                m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage>(OnScheduleUpdate);
            
            if(m_allocationRetrievedToken == null)
                m_allocationRetrievedToken = m_messenger.SubscribeOnMainThread<OnTillAllocationDataRetrievedMessage>(OnAllocationsRetrieved);
        }

        public virtual void DetachEvents()
        {
            if(m_scheduleBeginToken != null)
                m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleBeginToken);

            if(m_scheduleEndToken != null)
                m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);

            if(m_orgEndToken != null)
                m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);

            if(m_scheduleUpdateToken != null)
                m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);

            if(m_allocationRetrievedToken != null)
                m_messenger.Unsubscribe<OnTillAllocationDataRetrievedMessage>(m_allocationRetrievedToken);
            
            m_scheduleBeginToken = null;
            m_scheduleEndToken = null;
            m_orgEndToken = null;
            m_scheduleUpdateToken = null;
            m_allocationRetrievedToken = null;
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            DetachEvents ();
        }
            
        public void OnSelectDay(int index)
        {
			if (index == DayIndex)
				return;
			
			if (!CanChangeDate ()) 
			{
				RaisePropertyChanged (() => SelectedDayIndex);//Tell the UI to go back to what it previously had selected
				return;
			}

            DayIndex = index;

            m_schedule.SetDay(index);

            UpdateDate(StartDate, m_schedule.StartDay);

            UpdateView();
        }

        public void OnMoveNext()
        {
			if (!CanChangeDate ())
				return;

            StartDate = StartDate.AddDays(7);
			UpdateDate(StartDate, m_schedule.StartDay);

            m_schedule.Cancel();

            if(m_schedule.StartDate != StartDate)
            {
                m_schedule.MoveNext(false);
            }
        }

        public void OnMovePrev()
        {
			if (!CanChangeDate ())
				return;

            StartDate = StartDate.AddDays(-7);
			UpdateDate(StartDate, m_schedule.StartDay);

            m_schedule.Cancel();

            if(m_schedule.StartDate != StartDate)
            {
                m_schedule.MovePrev(false);
            }
        }

        public virtual void OnRefresh()
        {
			if (!CanChangeDate ())
				return;

			LoadAllocationsForPeriod (true);
        }

        protected void UpdateDate(DateTime start, int? startDay)
        {
            if (m_schedule.DayIndex.HasValue)
            {
                var day = start.AddDays(DayIndex);
                int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (StartDate);
                Date = m_weekNumberDisplayed ?
                    string.Format (Localiser.Get("week_starting_label"), weekNumber, day.ToString(Localiser.Get("dddd, dd/MM/yyyy"))) :
                    string.Format (day.ToString(Localiser.Get("dddd, dd/MM/yyyy")));
            }
            else
            {
                int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (start);
                Date = m_weekNumberDisplayed ?
                    string.Format (Localiser.Get("week_starting_label"), weekNumber, start.ToString (Localiser.Get("dd/MM/yyyy"))) :
                    string.Format (Localiser.Get("starting_label"),  start.ToString (Localiser.Get("dd/MM/yyyy")));
            }

			// this may be the first time initialising the control, so set a default
			if(startDay == null)
			{
				startDay = 2;
			}

            // Update days in segments
            if(startDay != null && startDay != m_currentStartDay)
            {
                startDay %= 7;
                m_currentStartDay = (int) startDay;
                List<string> daySegments = new List<string>();
                for(int i = 0; i < 7; i++)
                {
                    //daySegments.Add(((DayOfWeek) startDay).ToString().Substring(0, 3)); // This gives three character days and not as previously set with variable length

                    switch(startDay)
                    {
                        case 0:
                            daySegments.Add(Localiser.Get("saturday_short"));
                            break;
                        case 1:
                            daySegments.Add(Localiser.Get("sunday_short"));
                            break;
                        case 2:
                            daySegments.Add(Localiser.Get("monday_short"));
                            break;
                        case 3:
                            daySegments.Add(Localiser.Get("tuesday_short"));
                            break;
                        case 4:
                            daySegments.Add(Localiser.Get("wednesday_short"));
                            break;
                        case 5:
                            daySegments.Add(Localiser.Get("thursday_short"));
                            break;
                        case 6:
                            daySegments.Add(Localiser.Get("friday_short"));
                            break;
                    }

                    startDay++;
                    startDay %= 7;
                }
                Segments = daySegments;
            }
        }

        protected virtual void OnScheduleBegin(ScheduleReadBeginMessage message)
        {
            LoadingSchedule = true;
        }

        protected void OnScheduleEnd(ScheduleReadEndMessage message)
        {
            if (message.SessionTimeout)
            {
                m_session.SignOut ();
            }
            else if (!message.Success || m_schedule.IsBusy)
            {
                //m_alertBox.ShowOK (Localiser.Get("schedule_error"), message.Message);
                return;
            }

            LoadingSchedule = m_allocations.ReloadScheduleIfNotCorrect ();
            if (!LoadingSchedule)
            {
                //Get the data from the allocator
                LoadAllocationsForPeriod();
                
                UpdateDate(StartDate, m_schedule.StartDay);
            }
        }

        protected void OnAllocationsRetrieved(OnTillAllocationDataRetrievedMessage message)
        {
            LoadingSchedule = false;

            if (message.Success)
            {
                UpdateView();

                allocationsRetrievedFor = StartDate;
            }
        }

        protected virtual void LoadAllocationsForPeriod(bool refresh = false, bool updateMessage = true, bool doScheduleRefresh = true)
		{
			if (!allocationsRetrievedFor.HasValue || refresh || (DateTime.Compare (allocationsRetrievedFor.Value, StartDate) != 0)) 
			{
				LoadingSchedule = true;

				if (updateMessage)
					LoadingMessage = "Loading Allocations";

				m_allocations.ReadAsync (StartDate, StartDate.AddDays (7));

                if (doScheduleRefresh)
                {
                    m_schedule.Refresh (false);
                }
			} 
			else
			{
				LoadingSchedule = false;

				UpdateView ();
			}
		}
        
        private void OnOrgEnd(SetOrgEndMessage message)
        {
            if (message.SessionTimeout)
            {
                m_session.SignOut();
            }
            else
            {
                m_schedule.Refresh(false);
                m_spareShiftsService.Refresh (false, m_schedule.CurrentFilter);
            }
        }

        protected void OnScheduleUpdate(ScheduleUpdateMessage message)
        {
            
        }

		protected virtual bool CanChangeDate()
		{
			return true;
		}
    }
}