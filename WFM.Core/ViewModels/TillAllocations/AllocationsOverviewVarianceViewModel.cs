﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class VarianceOverviewModel
    {
        public List<String> Data {get; set;}
    }

    public class AllocationsOverviewVarianceViewModel : WFMViewModel
    {
        private readonly ScheduleService m_scheduleService;

        private readonly IMvxMessenger m_messenger;

        private MvxSubscriptionToken m_scheduleRefreshToken;

        public override string AnalyticsScreenName
        {
            get
            {
                return "AllocationOverviewVariance";
            }
        }
            
        private List<VarianceOverviewModel> m_rows;
        public List<VarianceOverviewModel> Rows
        {
            get { return m_rows; }
            set { m_rows = value; RaisePropertyChanged(() => Rows); }
        }

        public AllocationsOverviewVarianceViewModel (ScheduleService schedule, IMvxMessenger messenger)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;

            Refresh ();
        }

        public void ScheduleReadEndMessage(ScheduleReadEndMessage ScheduleReadEndMessage)
        {
            Refresh ();
        }

        public void Init ()
        {
            m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (ScheduleReadEndMessage);
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            m_messenger.Unsubscribe<ScheduleReadEndMessage> (m_scheduleRefreshToken);
        }

        public void Refresh()
        {
            if (m_scheduleService == null || m_scheduleService.Schedule == null)
                return;
            
            var index = m_scheduleService.Schedule.DayIndex;
            int start = index == null ? 0 : index.Value * ScheduleTime.DayIntervals;
            int length = index == null ? ScheduleTime.WeekIntervals : ScheduleTime.DayIntervals;

            Rows = BuildReportList (length, start);
        }

        List<VarianceOverviewModel> BuildReportList(int length, int start)
        {
            var time = new VarianceOverviewModel();
            time.Data = new List<String>();
            var demand = new VarianceOverviewModel();
            demand.Data = new List<String>();
            var scheduled = new VarianceOverviewModel();
            scheduled.Data = new List<String>();
            var variance = new VarianceOverviewModel();
            variance.Data = new List<String>();
                
            for(int i=0; i<length; ++i)
            {
                int interval = start + i;

                var timeStart = m_scheduleService.StartDate.AddMinutes (interval * ScheduleTime.IntervalMinutes);
                    
                if(time != null && time.Data != null)
                    time.Data.Add(TimeFormatter.FormatTime(timeStart));

                double demandVal = 0;
                double scheduledVal = 0;
                double varianceVal = 0;

                if (m_scheduleService.Accuracy != null)
                {
                    demandVal = m_scheduleService.Accuracy.Demand[interval];
                    scheduledVal = m_scheduleService.Accuracy.Scheduled[interval];
                    varianceVal = m_scheduleService.Accuracy.Scheduled[interval] - m_scheduleService.Accuracy.Demand[interval];
                }

                demand.Data.Add(demandVal.ToString());
                scheduled.Data.Add (scheduledVal.ToString());
                variance.Data.Add (varianceVal.ToString());        
            }

            var rows = new List<VarianceOverviewModel> ();
            rows.Add (time);
            rows.Add (demand);
            rows.Add (scheduled);
            rows.Add (variance);
            return rows;
        }
    }
}

