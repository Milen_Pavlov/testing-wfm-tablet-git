using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using WFM.Core.Model;
using WFM.Core.Services;
using WFM.Shared.DataTransferObjects.Breaks;

namespace WFM.Core
{
    //TODO: I'm pretty sure this is broken with the allocations summary view
    public class TillsBreaksViewModel : AllocationDateSelectionViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Till Breaks";
            }
        }

        private readonly ScheduleBreaksService m_breaksService;

        private bool c_viewDetailOfOverrunningNextDayShift = true;
        private bool c_viewDetailOfOverrunningPreviousDayShift = true;
        private const int c_daysInWeek = 7;

        private DateTime? m_loadedDataStartDate = null;

        private bool m_noData;
        public bool NoData
        {
            get { return m_noData; }
            set { m_noData = value; RaisePropertyChanged(() => NoData); }
        }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set 
            { 
                m_editMode = value; 
                RaisePropertyChanged(() => EditMode);
            }
        }

        private List<BreakItem> m_data;
        public List<BreakItem> Data
        {
            get { return m_data; }
            set
            {
                m_data = value;
                RaisePropertyChanged(() => Data);
            }
        }

        private IList<BreakRecord> m_records = new List<BreakRecord>();

        private IList<BreakItem> m_rows;
        public IList<BreakItem> Rows
        {
            get { return m_rows; }
            set 
            {
                m_rows = value;
                RaisePropertyChanged(() => Rows);
            }
        }

        private IList<TillAllocation> m_supportAllocations;
        public IList<TillAllocation> SupportAllocations
        {
            get { return m_supportAllocations; }
            set
            {
                m_supportAllocations = value;
                RaisePropertyChanged(() => SupportAllocations);
            }
        }

        private string m_breakMessage;
        public string BreakMessage
        {
            get { return m_breakMessage; }
            set
            {
                m_breakMessage = value;
                RaisePropertyChanged(() => BreakMessage);
            }
        }

        private string m_coverMessage;
        public string CoverMessage
        {
            get { return m_coverMessage; }
            set
            {
                m_coverMessage = value;
                RaisePropertyChanged(() => CoverMessage);
            }
        }

		private int m_BreakIndex;
		public int BreakIndex
		{
			get { return m_BreakIndex; }
			set { m_BreakIndex = value; RaisePropertyChanged(() => BreakIndex); }
		}

        public TillsBreaksViewModel(
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            SessionService session, 
            TillAllocationService allocations, 
            IAlertBox alertBox,
            ScheduleBreaksService breaksService,
            SpareShiftService spareShifts)
            : base(schedule, messenger, session, alertBox, allocations, spareShifts)
        {
            m_breaksService = breaksService;
        }

        public override void Init()
        {
            base.Init();

            if (!Loading)
            {
                if (m_loadedDataStartDate != StartDate)
                {
                    DoRefresh();
                }
                else
                {
                    UpdateView();
                }
            }
        }

        public override void UpdateView()
        {
            UpdateView(false);
        }

        private void UpdateView(bool forceUpdate)
        {            
            if (forceUpdate || m_loadedDataStartDate != StartDate)
            {
                SupportAllocations = new List<TillAllocation>();

                ReloadData(forceUpdate);
            }
            else
            {
                UpdateDisplay();
            }
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();
            m_breaksService.Cancel();
        }

        public async void ReloadData(bool forceUpdate = false)
        {
            m_breaksService.Cancel();

            if(!LoadingSchedule && m_schedule.Schedule != null && m_schedule.Schedule.Employees != null)
            {
                LoadingService = true;
                LoadingMessage = "Loading";

                var employees = m_schedule.Schedule.Employees;

                var breakShiftIDs = employees.SelectMany(e => e.Days).SelectMany(d => d.Shifts).Select(shift => shift.ShiftID);

                // Retrieve all break taken data
                m_records = await LoadBreaks(forceUpdate,breakShiftIDs);

                if (m_records != null)
                {
                    var breaks = new List<BreakItem>();

                    foreach (var e in employees.Where(m_allocations.IsEmployeeInRelevantTillJob))
                    {
                        foreach (var d in e.Days)
                        {
                            breaks.AddRange(BuildBreakRecords(e, d.Shifts));
                        }

                        if (e.LastDayPreviousWeek != null) // Add breaks from the last day of the previous week in
                        {
                            breaks.AddRange(BuildBreakRecords(e, e.LastDayPreviousWeek.Shifts));
                        }
                    }
                
                    Data = breaks;

                    CoverMessage = Localiser.Get("cover_message_select_break");

                    UpdateDisplay();

                    EditMode = m_breaksService.IsEdited;

                    m_loadedDataStartDate = StartDate;
                }
            }

            LoadingService = false;
        }

        async Task<IList<BreakRecord>> LoadBreaks(bool forceUpdate, IEnumerable<int> breakShiftIDs)
        {
            var result = await m_breaksService.ReadBreaks(breakShiftIDs.ToList(), forceUpdate);

            if (result != null)
                return result;
            else
                return new List<BreakRecord>();
        }

        private List<BreakItem> BuildBreakRecords(EmployeeData e, IEnumerable<ShiftData> shifts)
        {
            var breaks = new List<BreakItem>();

            foreach (var s in shifts)
            {
                foreach (var d in s.Details)
                {
                    BreakRecord matchingRecord = m_records.Where(m => m.BreakID == d.ShiftDetailID).FirstOrDefault();

                    bool breakTaken = matchingRecord != null ? matchingRecord.BreakTaken : false;

                    var job = s.GetJob (d.ScheduledJobID);

                    if (job != null && m_allocations.JobIsRelevant(job.JobName))
                    {
                        breaks.Add(CreateBreakItem(e, s, d, breakTaken));
                    }
                }
            }

            return breaks;
        }

        private void UpdateDisplay()
        {
            if (m_breaksService.IsEdited)
                EditMode = true;
            
            if(Data != null)
            {
                var filteredList = new List<BreakItem>();

                if(m_schedule.DayIndex != null && m_schedule.StartDay != null)
                {
                    var currentDate = ((DateTime) m_schedule.StartDate).AddDays((int) m_schedule.DayIndex);

                    foreach(var detail in Data)
                    {
                        bool addDetail = false;

                        if(detail.StartTime.Date == currentDate) // Check if break on same day as day index [fast check]
                        {
                            addDetail = true;
                        }
                        else
                        {
                            if(c_viewDetailOfOverrunningPreviousDayShift && detail.ShiftEndTime.Date == currentDate)  // Check if the shift overruns into the current day [slower check]
                            {
                                addDetail = true;
                            }

                            if(c_viewDetailOfOverrunningNextDayShift && detail.ShiftStartTime.Date == currentDate)  // Check if the shift overruns from same day as day index [slower check]
                            {
                                addDetail = true;
                            }

                            if (detail.StartTime.Date < currentDate.Date && detail.StartTime.AddMinutes(detail.Duration).Date == currentDate.Date)
                            {
                                addDetail = true;
                            }
                            else
                            {
                                addDetail = false;
                            }
                        }

                        if(addDetail)
                        {
                            filteredList.Add(detail);
                        }
                    }
                }
                else
                {
                    filteredList = Data.ToList();
                }

                foreach (var entry in filteredList) 
                {
                    if (m_records == null)
                        break;
                    var breakRecord = m_records.FirstOrDefault (x => x.BreakID == entry.BreakDetailID);
                    if(breakRecord != null)  
                        entry.BreakTaken = breakRecord.BreakTaken;

                }
                Rows = filteredList.OrderBy(b => b.StartTime).ThenBy(b => b.EmployeeName).ToList();
            }

            if(Rows != null)
            {
                BreakMessage = Rows.Count > 0 ? string.Empty : Localiser.Get("no_breaks_message");
            }
        }
            
        public void OnBreaksTakenEdited(BreakItem updated)
        {
            var record = m_records.FirstOrDefault(breakItem => breakItem.BreakID == updated.BreakDetailID);

            if(record != null)
            {
                record.BreakTaken = !updated.BreakTaken;
            }
            else
            {
                record = new BreakRecord()
                    {
                        BreakID = updated.BreakDetailID,
                        BreakTaken = !updated.BreakTaken,
                        ShiftID = updated.ShiftID
                    };

                m_records.Add(record);
            }

            EditMode = true;
            m_breaksService.RecordChange(record.BreakID, record.ShiftID, record.BreakTaken);
        }

        public void OnBreakSelected(BreakItem item)
        {
            DateTime startTime = item.StartTime;
            DateTime endTime = startTime.AddMinutes(item.Duration);

            if (m_allocations.TillAllocations != null)
            {
                SupportAllocations = m_allocations.TillAllocations.Where(
                    a => a.CheckoutSupport &&
                    a.StartTime <= startTime &&
                    a.EndTime >= endTime
					&& a.EmployeeId != item.EmployeeId).ToList();
            }

            if (SupportAllocations.Count > 0)
            {
                CoverMessage = string.Empty;
            }
            else
            {
                CoverMessage = Localiser.Get("cover_message_no_cover_available");
            }

			m_BreakIndex = Rows.IndexOf (item);
        }

        public override void OnRefresh()
        {
            LoadingMessage = "Loading";

            if (EditMode)
            {
                m_alertBox.ShowYesNo
                (
                    Localiser.Get("confirm_discard_breaks_title"),
                    Localiser.Get("confirm_discard_breaks_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    DoRefresh();
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                DoRefresh();
            }
        }

        private void DoRefresh()
        {
            if (!Loading)
            {
                //This is so we trigger a clean refresh of the data, clearing breaks service wipes the current changes (so we trigger a fresh get)
                m_breaksService.Reset();
                //Resetting the date makes sure that we go down the path of getting fresh data.
                m_loadedDataStartDate = null;
                //schedule change will automatically update break data
                m_schedule.Refresh(false);
                EditMode = false;
            }
        }

        public async void OnEdit()
        {
            if(EditMode)
            {
                // Submit changes
                LoadingService = true;

                LoadingMessage = "Uploading changes...";

                await m_breaksService.Save();

                EditMode = false;

                ReloadData(true);
            }
            else
            {
                EditMode = true;
            }
        }

        private BreakItem CreateBreakItem(EmployeeData e, ShiftData s, ShiftDetailData d, bool breakTaken)
        {
                return new BreakItem()
                {
                    StartTime = d.Start.Value,
                    Duration = d.Duration,
                    EmployeeName = e.Name,
                    ShiftStartTime = s.Start,
                    ShiftEndTime = s.End,
                    Department = s.GetJob(d.ScheduledJobID).JobName,
                    BreakDetailID = d.ShiftDetailID,
                    BreakTaken = breakTaken,
                    StartTimeFormatted = string.Format("{0} {1} - {2}", d.Start.Value.ToString("ddd"), TimeFormatter.FormatTime(d.Start.Value), TimeFormatter.FormatTime(d.End.Value)),
                    ShiftID = s.ShiftID,
					EmployeeId = e.EmployeeID
                };
        }
    }
}