using System;
using System.Linq;
using Consortium.Client.Core;
using Consortium.Client.Core.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using WFM.Core.Services;
using WFM.Core.Model;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System.Threading.Tasks;
using WFM.Shared.Extensions;

namespace WFM.Core
{
    public class TillsViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Tills";
            }
        }

        private StaticData m_staticData;
        private IList<Till> m_tillsData;

        private readonly TillService m_tillService;
        private readonly SessionData m_sessionData;
        private readonly IAlertBox m_alertBox;

        private bool m_noData;
        public bool NoData
        {
            get { return m_noData; }
            set { m_noData = value; RaisePropertyChanged(() => NoData); }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        public List<TillModel> m_tills;
        public List<TillModel> Tills
        {
            get { return m_tills; }
            private set 
            {
                m_tills = value; 
                RaisePropertyChanged(() => Tills); 
                RaisePropertyChanged(() => UndeletedTills);
            }
        }

        // Convenience property for all current tills
        public IList<TillModel> UndeletedTills
        {
            get
            {
				if (Tills != null) {
					return Tills.Where (t => !t.State.DeletedInMemory).ToList ();
				} else
					return new List<TillModel>();
            }
        }

        public TillModel m_selectedTill;
        public TillModel SelectedTill
        {
            get { return m_selectedTill; }
            private set 
            {
                m_selectedTill = value; RaisePropertyChanged(() => SelectedTill); 
            }
        }

        private IList<TillType> m_tillTypes;
        public IList<TillType> TillTypes 
        { 
            get { return m_tillTypes; }
            set
            {
                m_tillTypes = value;
                RaisePropertyChanged(() => TillTypes); 
            }
        }

        private IList<TillStatus> m_tillStatuses;
        public IList<TillStatus> TillStatuses 
        { 
            get { return m_tillStatuses; }
            set
            {
                m_tillStatuses = value;
                RaisePropertyChanged(() => TillStatuses);
            }
        }

        public int BasketTypeId { get; set; }
        public int OpenStatusId { get; set; }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set 
            { 
                m_editMode = value; 
                m_tillService.IsEdited = value;
                RaisePropertyChanged(() => EditMode);
                if (Tills != null)
                {
                    foreach (var till in Tills)
                    {
                        till.IsDraggable = m_editMode;
                    }
                }
            }
        }

        private bool m_isSaving;
        public bool IsSaving
        {
            get { return m_isSaving; }
            set
            {
                m_isSaving = value;
                m_tillService.IsSaving = value;
            }
        }

        public TillsViewModel(TillService tillService, SessionData sessionData, IAlertBox alertBox)
        {
            m_tillService = tillService;
            m_sessionData = sessionData;
            m_alertBox = alertBox;
        }
            
        public override void OnViewClosed()
        {
            base.OnViewClosed();

            EditMode = false;
            m_tillService.IsEdited = false;
            m_tillService.IsSaving = false;
            m_tillService.Cancel ();
            m_sessionData.CancelStaticDataLoad ();
        }

        public void Init()
        {
            Refresh();
        }

        private void Refresh()
        {
            if (EditMode)
            {
                m_alertBox.ShowYesNo
                (
                    Localiser.Get("discard_changes_message"),
                    Localiser.Get("confirm_discard_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    DoRefresh();
                                    break;
                                }
                        }
                    }
                );
            }
            else
            {
                DoRefresh();
            }
        }

        private async void DoRefresh()
        {
            LoadingMessage = "Loading tills data...";
            Loading = true;

            //The tasks are split into separate requests because the managed service base can't tae an arbitrary number of operations
            //Splitting them up allows us to show the correct number of dialog boxes at the correct time.
            Task<StaticData> staticDataTask = m_sessionData.GetStaticDataAsync();

            m_staticData = null;
            m_tillsData = null;
                       
            m_staticData = await staticDataTask;

            if(m_staticData == null)
            {
                NoData = true;
                LoadingMessage = "Data transfer failed. Please try again.";
                return;
            }

            //And THEN get the tills
			IList<Till> data = await m_tillService.SafeReadAsync(m_sessionData.Site.SiteID);
            
			SetTillData (data);

            EditMode = false;
            Loading = false;
        }

		private void SetTillData(IList<Till> data)
		{
			m_tillsData = data;

			if(m_tillsData == null)
			{
				NoData = true;
				LoadingMessage = "Data transfer failed. Please try again.";
				return;
			}
			else
			{
				NoData = false;
			}

			BasketTypeId = m_staticData.TillTypes.Single(t => t.Key == TillTypeKey.Basket).TillTypeId;
			OpenStatusId = m_staticData.TillStatuses.Single(t => t.Key == TillStatusKey.Open).TillStatusId;
			TillTypes = m_staticData.TillTypes;
			TillStatuses = m_staticData.TillStatuses;
			Tills = m_tillsData.Select(t => new TillModel(t, BasketTypeId, OpenStatusId)).ToList();

            if (SelectedTill == null)
                SelectedTill = m_tills.FirstOrDefault ();
            else
                SelectedTill = m_tills.FirstOrDefault(x => x.TillId == SelectedTill.TillId);
		}

        private void SwitchEditMode()
        {
            if (EditMode)
            {
                m_alertBox.ShowOptions
                (
                    Localiser.Get("save_tills_message"),
                    Localiser.Get("confirm_save_message"),
                    new List<string>() { Localiser.Get("save_changes_message"),  Localiser.Get("discard_changes_message") },  
                    Localiser.Get("continue_editing_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Save
                            {
                                TrySave();
                                break;
                            }
                            case 2: // Revert
                            {
                                DoRefresh();
                                EditMode = false;
                                break;
                            }
                        }
                    }
                );
            }
            else
            {
                EditMode = true;
            }
        }

        private void TrySave()
        {
            if (!IsValid)
            {
                m_alertBox.ShowOK(Localiser.Get("validation_errors_message"), Localiser.Get("tills_cannot_be_saved_message"));
            }
            else
            {
                DoSave();
            }
        }

        private async void DoSave()
        {
            IsSaving = true;
            Loading = true;
            LoadingMessage = Localiser.Get("saving_ellipsis");

            var tills = Tills.Select(t => t.ToTill()).ToList();
            var saveTillsTask = await m_tillService.SafeWriteReadAsync(tills, m_sessionData.Site.SiteID);

            IsSaving = false;
            EditMode = false;
			DoRefresh ();//TODO Techincally only need to refresh when creating new tills so we get the ID?
        }

        private void DoCreateTill()
        {
			Tills = Tills._NewIfNull();

			var newTill = new TillModel(m_sessionData.Site.SiteID, Tills._NewId(t => t.TillId), BasketTypeId, OpenStatusId);
            Tills.Add(newTill);
            var temp = Tills;
            Tills = null;
            Tills = temp;
            SelectedTill = newTill;
        }

        private void DeleteTill(TillModel till)
        {
            m_alertBox.ShowYesNo(
                Localiser.Get("delete_till_message"),
                Localiser.Get("confirm_delete_till_message"),
                (option) =>
                {
                    switch (option)
                    {
                        case 1: // Yes
                            {
                                DoDeleteTill(till);
                                break;
                            }
                    }
                });
        }

        private void DoDeleteTill(TillModel till)
        {
            if (till != null)
            {
                till.State.DeleteInMemory();
                var temp = Tills;
                Tills = null;
                Tills = temp;
                SelectedTill = null;
            }
        }

        public void OnTillSelected(TillModel till)
        {
            SelectedTill = till;
        }

        public void OnTillOrderChanged(List<int> tillIdsOrderedByOpeningSequence)
        {
            Tills = Tills.OrderBy(till => tillIdsOrderedByOpeningSequence.IndexOf(till.TillId)).ToList();
        }

        public void OnRefresh()
        {
            Refresh();
        }

        public void OnEdit()
        {
            SwitchEditMode();
        }

        public void OnCreateTill()
        {
            DoCreateTill();
        }

        public void OnDeleteTill(TillModel till)
        {
            DeleteTill(till);
        }

        public bool IsValid
        {
            get
            {
                return Tills.TrueForAll(t => t.IsValid);
            }
        }
    }
}