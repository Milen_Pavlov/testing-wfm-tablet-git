﻿using System;
using System.Linq;
using System.Collections.Generic;
using WFM.Core.Model;
using Cirrious.MvvmCross.ViewModels;
using Consortium.Client.Core.Models;
using WFM.Core.Validators;
using TimePeriod = WFM.Core.Validators.OverlappingTimePeriodsValidator.TimePeriod;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class TillModel : MvxNotifyPropertyChanged
    {
        public int TillId { get; set; }
        private IStringService  m_localiser;

        private string m_tillNumber;
        public string TillNumber
        { 
            get { return m_tillNumber; }
            set
            {
                m_tillNumber = value;
                RaisePropertyChanged(() => TillNumber);
            }
        }

        public RequiredValidator TillNumberRequiredValidator { get; set; }

        private int m_typeId;
        public int TypeId 
        { 
            get{ return m_typeId; } 
            set
            {
                m_typeId = value;
                RaisePropertyChanged(() => TypeId);
                RaisePropertyChanged(() => IsBasket);
            }
        }

        private int m_statusId;
        public int StatusId 
        { 
            get { return m_statusId; }
            set
            {
                m_statusId = value;
                RaisePropertyChanged(() => StatusId);
                RaisePropertyChanged(() => IsClosed);
            }
        }

        public int SiteId { get; set; }
        public int BasketTypeId { get; set; }
        public int OpenStatusId { get; set; }
        public ModelState State { get; set; }

        public List<TillWindowModel> Windows { get; set; }

        public bool IsBasket
        {
            get
            {
                return (TypeId == BasketTypeId);
            }
        }

        public bool IsClosed
        {
            get
            {
                return (StatusId != OpenStatusId);
            }
        }

        private bool m_isDraggable;
        public bool IsDraggable
        {
            get { return m_isDraggable; }
            set
            {
                m_isDraggable = value;
                RaisePropertyChanged(() => IsDraggable);
            }
        }

        private TillModel()
        {
            m_localiser = Mvx.Resolve<IStringService>();
            TillNumberRequiredValidator = new RequiredValidator(m_localiser.Get("till_number"), () => TillNumber);
        }

        public TillModel(int siteId, int tillId, int basketTypeId, int openStatusId)
            : this()
        {
            SiteId = siteId;
            TillId = tillId;
            BasketTypeId = basketTypeId;
            OpenStatusId = openStatusId;
            TillNumber = string.Format(" {0}", Math.Abs(tillId));
            TypeId = basketTypeId;
            StatusId = openStatusId;
            State = ModelState.DoesNotExistInDatabase;

            Windows = new List<TillWindowModel>() // Note: There must be exactly 3 till windows
            {
                new TillWindowModel(0, new TimeSpan(8, 00, 00), new TimeSpan(20, 00, 00)),
                new TillWindowModel(1),
                new TillWindowModel(2),
            };

            InitialiseTillWindows();
        }

        public TillModel(Till till, int basketTypeId, int openStatusId)
            : this()
        {
            SiteId = till.SiteId;
            TillId = till.TillId;
            BasketTypeId = basketTypeId;
            OpenStatusId = openStatusId;
            TillNumber = till.TillNumber;
            TypeId = till.TypeId;
            StatusId = till.StatusId;
            State = ModelState.ExistsInDatabase;

            Windows = new List<TillWindowModel>() // Note: There must be exactly 3 till windows
            {
                new TillWindowModel(0, till.Windows == null || till.Windows.Count < 1 ? null : till.Windows[0]),
                new TillWindowModel(1, till.Windows == null || till.Windows.Count < 2 ? null : till.Windows[1]),
                new TillWindowModel(2, till.Windows == null || till.Windows.Count < 3 ? null : till.Windows[2]),
            };

            InitialiseTillWindows();
        }

        public void InitialiseTillWindows()
        {
            foreach (var window in Windows)
            {
                var timePeriod = new Func<TimePeriod>(() => new TimePeriod(window.StartTime, window.EndTime));
                
                var timePeriodsToCompare = new Func<IList<TimePeriod>>(() => Windows.
                    Where(w => w != window).
                    Select(w => new TimePeriod(w.StartTime, w.EndTime)).
                    ToList());
                
                window.OverlappingWindowsValidator = new OverlappingTimePeriodsValidator(m_localiser.Get("till_window"), timePeriod, timePeriodsToCompare);

                window.TillWindowChanged += () =>
                {
                    State.UpdateInMemory();
                    Validate();
                };
            }
        }

        public Till ToTill()
        {
            var till = new Till()
                {
                    TillId = TillId,
                    TillNumber = TillNumber,
                    TypeId = TypeId,
                    StatusId = StatusId,
                    SiteId = SiteId,
                    State = State,
                };

            foreach (var windowViewModel in Windows)
            {
                var tillWindow = windowViewModel.ToTillWindow();
                if (tillWindow != null)
                {
                    till.Add(tillWindow);
                }
            }

            return till;
        }

        public void TillNumberChanged(string value)
        {
            TillNumber = value;
            State.UpdateInMemory();
            Validate();
        }

        public void TypeIdChanged(int value)
        {
            TypeId = value;
            State.UpdateInMemory();
            Validate();
        }

        public void StatusIdChanged(int value)
        {
            StatusId = value;
            State.UpdateInMemory();
            Validate();
        }

        public void Validate()
        {
            TillNumberRequiredValidator.Validate();
            foreach (var window in Windows)
            {
                window.Validate(IsBasket);
            }

            RaisePropertyChanged(() => ErrorMessage);
        }

        public bool IsValid
        {
            get
            {
                return (TillNumberRequiredValidator.IsValid && Windows.TrueForAll(w => w.IsValid));
            }
        }

        public string ErrorMessage
        {
            get
            {
                return (IsValid ? null : m_localiser.Get("till_invalid_message"));
            }
        }
    }
}
