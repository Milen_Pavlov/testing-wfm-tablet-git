﻿using System;
using System.Linq;
using Consortium.Client.Core;
using System.Collections.Generic;
using WFM.Core.Services;

namespace WFM.Core
{
    public class RootViewModel : WFMViewModel
    {
        private readonly IAlertBox m_alertBox;
        private readonly TillService m_tillService;
        private readonly ScheduleBreaksService m_breaksService;

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private int m_tabIndex = -1;
        public int TabIndex
        {
            get { return m_tabIndex; }
            set { m_tabIndex = value; RaisePropertyChanged (() => TabIndex); }
        }

        public RootViewModel(IAlertBox alertBox, TillService tillService, ScheduleBreaksService breaksService)
        {
            m_alertBox = alertBox;
            m_tillService = tillService;
            m_breaksService = breaksService;
        }

        public void Init()
        {
            OnTabSelect(0); // Initially we can programmatically 'click' the first tab

//            UpdateAll();
        }

        public void OnTabSelect(int index)
        {
            if (TabIndex == index)
                return;

            if (m_tillService.IsSaving)
            {
                m_alertBox.ShowOK
                (
                    Localiser.Get ("allocations_saving_title"), 
                    Localiser.Get ("allocations_saving_message"),
                    (option) =>
                    {  
                        TabIndex = TabIndex;
                    }
                );
            }
            else if (m_tillService.IsEdited == false && m_breaksService.IsEdited == false)
            {
                ChangeTab (index);
            }
            else
            {
                m_alertBox.ShowYesNo
                (
                    Localiser.Get ("discard_changes_message"),
                    Localiser.Get ("allocations_confirm_discard_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    ChangeTab (index);
                                    return;
                                }
                            case -1:
                            case 0: //no
                                {
                                    TabIndex = TabIndex;
                                    return;
                                }
                        }
                    }
                );
            }

        }

        public void ChangeTab(int index)
        {
            switch(index)
            {
                case 0:
                    ShowViewModel<AllocationsOverviewViewModel> ();
                    break;
                case 1:
                    ShowViewModel<AllocationsViewModel>();
                    break;
                case 2:
                    ShowViewModel<TillsBreaksViewModel> ();
                    break;
			    case 3:
                    ShowViewModel<TillsViewModel> ();
					break;
                case 4:
                    ShowViewModel<AllocationSettingsViewModel> ();
                    break;
                default:
                    throw new NotImplementedException();
            }

            TabIndex = index;
        }

        private void BeginLoad()
        {
            Loading = true;
        }

        private void EndLoad()
        {
            Loading = false;

//            UpdateAll ();
        }

//        private void UpdateAll()
//        {
//            EditMode = 0;
//        }

    }
}

