﻿using System;
using WFM.Shared.DataTransferObjects.TillAllocation.Model;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using WFM.Core.Services;
using WFM.Shared.DataTransferObjects.Breaks;
using System.Threading.Tasks;
using System.Linq;

namespace WFM.Core
{
    public class AllocationsOverviewBreaksViewModel : AllocationDateSelectionViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "AllocationOverviewBreaks";
            }
        }

        private readonly ScheduleBreaksService m_breaksService;

        protected readonly IMvxMessenger m_messenger;


        private MvxSubscriptionToken m_scheduleRefreshToken;

        private const int c_daysInWeek = 7;

        private DateTime? m_loadedDataStartDate = null;
        private bool m_noData;
        public bool NoData
        {
            get { return m_noData; }
            set { m_noData = value; RaisePropertyChanged(() => NoData); }
        }

        private List<BreakItem> m_data;
        public List<BreakItem> Data
        {
            get { return m_data; }
            set
            {
                m_data = value;
                RaisePropertyChanged(() => Data);
            }
        }

        private bool m_isListEmpty;
        public bool IsListEmpty
        {
            get{ return m_isListEmpty; }
            set
            {
                m_isListEmpty = value;
                RaisePropertyChanged (() => IsListEmpty);
            }
        }

        private IList<BreakRecord> m_records = new List<BreakRecord>();

        private IList<BreakItem> m_rows;
        public IList<BreakItem> Rows
        {
            get { return m_rows; }
            set 
            {
                m_rows = value;
                RaisePropertyChanged(() => Rows);
                IsListEmpty = m_rows.Count < 1;
            }
        }

        private IList<TillAllocation> m_supportAllocations;
        public IList<TillAllocation> SupportAllocations
        {
            get { return m_supportAllocations; }
            set
            {
                m_supportAllocations = value;
                RaisePropertyChanged(() => SupportAllocations);
            }
        }

        private string m_breakMessage;
        public string BreakMessage
        {
            get { return m_breakMessage; }
            set
            {
                m_breakMessage = value;
                RaisePropertyChanged(() => BreakMessage);
            }
        }

        private string m_coverMessage;
        public string CoverMessage
        {
            get { return m_coverMessage; }
            set
            {
                m_coverMessage = value;
                RaisePropertyChanged(() => CoverMessage);
            }
        }

        private int m_BreakIndex;
        public int BreakIndex
        {
            get { return m_BreakIndex; }
            set { m_BreakIndex = value; RaisePropertyChanged(() => BreakIndex); }
        }

        private DateTime m_endDate;
        public DateTime EndDate
        {
            get { return m_endDate; }
            set
            { 
                m_endDate = value; 
                RaisePropertyChanged (() => StartDate);
            }
        }

        public override void Init()
        {
            StartDate = DateTime.Now.AddHours (-2);

            EndDate = DateTime.Now.AddHours(2);

            m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleFinishedRead);

            if (!Loading)
            {
                if (m_loadedDataStartDate != StartDate)
                {
                    DoRefresh();
                }
                else
                {
                    UpdateView();
                }
            }
        }

        public override void UpdateView()
        {
            UpdateView(false);
        }

        public void UpdateView(bool forceUpdate)
        {            
            if (forceUpdate || m_loadedDataStartDate != StartDate)
            {
                SupportAllocations = new List<TillAllocation>();

                ReloadData(forceUpdate);
            }
            else
            {
                UpdateDisplay();
            }
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed ();
            m_breaksService.Cancel();
            m_messenger.Unsubscribe<ScheduleReadEndMessage> (m_scheduleRefreshToken);
        }

        public async void ReloadData(bool forceUpdate = false)
        {
            m_breaksService.Cancel();

            if(!LoadingService && m_schedule.Schedule != null && m_schedule.Schedule.Employees != null)
            {
                LoadingService = true;
                LoadingMessage = "Loading";

                var employees = m_schedule.Schedule.Employees;

                var breakShiftIDs = employees.SelectMany(e => e.Days).SelectMany(d => d.Shifts).Select(shift => shift.ShiftID);

                // Retrieve all break taken data
                m_records = await LoadBreaks(forceUpdate,breakShiftIDs);

                if (m_records != null)
                {
                    var breaks = new List<BreakItem>();

                    foreach (var e in employees.Where(m_allocations.IsEmployeeInRelevantTillJob))
                    {
                        foreach (var d in e.Days)
                        {
                            breaks.AddRange(BuildBreakRecords(e, d.Shifts));
                        }

                        if (e.LastDayPreviousWeek != null) // Add breaks from the last day of the previous week in
                        {
                            breaks.AddRange(BuildBreakRecords(e, e.LastDayPreviousWeek.Shifts));
                        }
                    }

                    Data = breaks;

                    CoverMessage = Localiser.Get("cover_message_select_break");

                    UpdateDisplay();

                    m_loadedDataStartDate = StartDate;
                }
            }

            LoadingService = false;
        }

        async Task<IList<BreakRecord>> LoadBreaks(bool forceUpdate, IEnumerable<int> breakShiftIDs)
        {
            var result = await m_breaksService.ReadBreaks(breakShiftIDs.ToList(), forceUpdate);

            if (result != null)
                return result;
            else
                return new List<BreakRecord>();
        }

        private List<BreakItem> BuildBreakRecords(EmployeeData e, IEnumerable<ShiftData> shifts)
        {
            var breaks = new List<BreakItem>();

            foreach (var s in shifts)
            {
                foreach (var d in s.Details)
                {
                    BreakRecord matchingRecord = m_records.Where(m => m.BreakID == d.ShiftDetailID).FirstOrDefault();

                    bool breakTaken = matchingRecord != null ? matchingRecord.BreakTaken : false;

                    var job = s.GetJob (d.ScheduledJobID);

                    if (job != null && m_allocations.JobIsRelevant(job.JobName))
                    {
                        breaks.Add(CreateBreakItem(e, s, d, breakTaken));
                    }
                }
            }

            return breaks;
        }

        private void UpdateDisplay()
        {
            if(Data != null)
            {
                Rows = Data.Where (d => d.StartTime.CompareTo (StartDate) >= 0 && d.EndTime.CompareTo (EndDate) <= 0)
                            .OrderBy (b => b.BreakTaken)
                            .ThenBy (b => b.StartTime).ToList();
            }

            if(Rows != null)
            {
                BreakMessage = Rows.Count > 0 ? string.Empty : Localiser.Get("no_breaks_message");
            }
        }

        public async void OnBreaksTakenEdited(BreakItem updated)
        {
            var record = m_records.FirstOrDefault(breakItem => breakItem.BreakID == updated.BreakDetailID);

            if(record != null)
            {
                record.BreakTaken = !updated.BreakTaken;
            }
            else
            {
                record = new BreakRecord()
                    {
                        BreakID = updated.BreakDetailID,
                        BreakTaken = !updated.BreakTaken,
                        ShiftID = updated.ShiftID
                    };

                m_records.Add(record);
            }
                
            m_breaksService.RecordChange(record.BreakID, record.ShiftID, record.BreakTaken);
            await m_breaksService.Save();
        }

        public void OnScheduleFinishedRead(ScheduleReadEndMessage scheduleReadEndMessage)
        {
            UpdateView (true);
        }

        public override void OnRefresh()
        {
            LoadingMessage = "Loading";

            DoRefresh();
        }

        private async void DoRefresh()
        {
            if (!Loading)
            {
                //This is so we trigger a clean refresh of the data, clearing breaks service wipes the current changes (so we trigger a fresh get)
                m_breaksService.Reset();
                //Resetting the date makes sure that we go down the path of getting fresh data.
                m_loadedDataStartDate = null;
                //schedule change will automatically update break data
                await m_schedule.Refresh(false);
            }
        }

        private BreakItem CreateBreakItem(EmployeeData e, ShiftData s, ShiftDetailData d, bool breakTaken)
        {
            return new BreakItem()
            {
                StartTime = d.Start.Value,
                Duration = d.Duration,
                EmployeeName = e.Name,
                ShiftStartTime = s.Start,
                ShiftEndTime = s.End,
                Department = s.GetJob(d.ScheduledJobID).JobName,
                BreakDetailID = d.ShiftDetailID,
                BreakTaken = breakTaken,
                StartTimeFormatted = string.Format("{0} {1} - {2}", d.Start.Value.ToString("ddd"), TimeFormatter.FormatTime(d.Start.Value), TimeFormatter.FormatTime(d.End.Value)),
                ShiftID = s.ShiftID,
                EmployeeId = e.EmployeeID
            };
        }

        public AllocationsOverviewBreaksViewModel (
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            SessionService session, 
            TillAllocationService allocations, 
            IAlertBox alertBox,
            ScheduleBreaksService breaksService,
            SpareShiftService spareShifts)
            : base(schedule, messenger, session, alertBox, allocations, spareShifts)
        {
            m_breaksService = breaksService;
            m_messenger = messenger;
        }
    }
}

