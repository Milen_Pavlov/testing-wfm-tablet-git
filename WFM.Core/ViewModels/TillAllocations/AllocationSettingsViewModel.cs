﻿using System;
using Consortium.Client.Core.ViewModels;
using System.Collections.Generic;
using WFM.Core.Services;
using System.Threading;
using System.Threading.Tasks;

namespace WFM.Core
{
	public class AllocationSettingsViewModel : WFMViewModel
	{       
        public override string AnalyticsScreenName
        {
            get
            {
                return "Till Settings";
            }
        }

        private const int C_DEFAULT_HOURS = 4;

        private const int C_DEFAULT_WAIT_MS = 500;

		private readonly TillAllocationService m_tillAllocationService;

        private bool m_Loading;

        public bool Loading
        {
            get
            {
                return m_Loading;
            }
            set
            {
                m_Loading = value;
                RaisePropertyChanged(() => Loading);
                RaisePropertyChanged(() => CanSave);
            }
        }

        public bool CanSave
        {
            get
            {
                return !m_Loading;
            }
            
            set
            {                
                RaisePropertyChanged(() => CanSave);
            }
        }

        public List<SpinnerItem> SpinnerOptions
        {
            get
            {
                return new List<SpinnerItem> () 
                {
                    new SpinnerItem ("2", "2"),
                    new SpinnerItem ("3", "3"),
                    new SpinnerItem ("4", "4"),
                };
            }
            set{ }
        }
        
        private int m_selectedValue = C_DEFAULT_HOURS;
        public int SelectedValue 
        { 
            get
            {
                return m_selectedValue;
            }

            set
            { 
                m_selectedValue = value;
                RaisePropertyChanged (() => SelectedValue);
            }
        }
        

        public AllocationSettingsViewModel (TillAllocationService tillAllocationService)
        {
            m_tillAllocationService = tillAllocationService;
        }

            
		public AllocationSettingsViewModel ()
		{
		}

		public async void Init()
		{
            GetData();
		}

		public void SelectedValueChanged(SpinnerItem item)
		{
			int outValue = C_DEFAULT_HOURS;

			bool success = int.TryParse (item.Key, out outValue);

			if(success)
			{ 
				this.SelectedValue = outValue; 
			}
		}

		public async void OnSubmit()
		{
            Loading = true;

            var operation = m_tillAllocationService.UpdateMaxHoursAsync (SelectedValue);

            await Task.Delay (C_DEFAULT_WAIT_MS);

            await operation;

            Loading = false;
		}

        public void OnRefresh()
        {
            GetData();
        }

        public async void GetData()
        {
            Loading = true;

            var operation = m_tillAllocationService.GetMaxHoursAsync();
            await Task.Delay (C_DEFAULT_WAIT_MS);
            int? value = await operation;

            SelectedValue = value.GetValueOrDefault (C_DEFAULT_HOURS);

            Loading = false;
        }
	}
}