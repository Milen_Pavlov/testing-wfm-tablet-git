//using System;
//using Consortium.Client.Core;
//using Cirrious.MvvmCross.Plugins.Messenger;
//using WFM.Core.Services;
//using System.Collections.Generic;
//
//namespace WFM.Core
//{
//    public abstract class DateSelectionViewModel : WFMViewModel
//    {
//        private readonly IMvxMessenger m_messenger;
//
//        protected readonly ScheduleService m_schedule;
//        protected readonly SessionService m_session;
//        protected readonly TillAllocationService m_allocationsService;
//        protected readonly SettingsService m_settings;
//
//        protected readonly IAlertBox m_alertBox;
//
//        protected MvxSubscriptionToken m_scheduleBeginToken;
//        protected MvxSubscriptionToken m_scheduleEndToken;
//        protected MvxSubscriptionToken m_orgEndToken;
//        protected MvxSubscriptionToken m_scheduleUpdateToken;
//
//        protected MvxSubscriptionToken m_allocationBeginToken;
//        protected MvxSubscriptionToken m_allocationEndToken;
//        protected MvxSubscriptionToken m_allocationRunBeginToken;
//        protected MvxSubscriptionToken m_allocationRunEndToken;
//
//        protected bool m_scheduleWorking;
//        private bool m_allocationWorking;
//        private bool m_allocationRunWorking;
//        private bool m_weekNumberDisplayed;
//
//        public abstract bool NoData {get; set;}
//
//        private int m_selectedDayIndex;
//        public int SelectedDayIndex
//        {
//            get { return m_selectedDayIndex; }
//            set { m_selectedDayIndex = value; RaisePropertyChanged(() => SelectedDayIndex); }
//        }
//
//        private int m_currentStartDay;
//
//        private List<string> m_segments;
//        public List<string> Segments
//        {
//            get { return m_segments; }
//            set { m_segments = value; RaisePropertyChanged(() => Segments); }
//        }
//
//        private bool m_loading;
//        public bool Loading
//        {
//            get { return m_loading; }
//            set { m_loading = value; RaisePropertyChanged(() => Loading); }
//        }
//
//        private string m_loadingMessage;
//        public string LoadingMessage
//        {
//            get { return m_loadingMessage; }
//            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
//        }
//
//        private DateTime m_startDate;
//        public DateTime StartDate
//        {
//            get { return m_startDate; }
//            set 
//            { 
//                m_startDate = value; 
//                RaisePropertyChanged(() => StartDate);
//            }
//        }
//
//        private int m_dayIndex;
//        public int DayIndex
//        {
//            get { return m_dayIndex; }
//            set { m_dayIndex = value; RaisePropertyChanged(() => DayIndex); }
//        }
//
//        private string m_date;
//        public string Date
//        {
//            get { return m_date; }
//            set { m_date = value; RaisePropertyChanged(() => Date); }
//        }
//
//        public DateTime SelectedDay
//        {
//            get
//            {
//                return StartDate.AddDays(DayIndex);
//            }
//        }
//
//        public DateSelectionViewModel(
//            ScheduleService schedule, 
//            IMvxMessenger messenger, 
//            SessionService session, 
//            TillAllocationService allocations, 
//            IAlertBox alertBox,
//            SettingsService settings)
//        {
//            m_schedule = schedule;
//            m_messenger = messenger;
//            m_session = session;
//            m_allocationsService = allocations;
//            m_alertBox = alertBox;
//            m_settings = settings;
//
//            m_weekNumberDisplayed = m_settings.AppConfig.WeekNumberDisplayed;
//            m_scheduleWorking = false;
//            m_allocationWorking = false;
//        }
//
//        public async virtual void Init()
//        {
//            m_scheduleBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage>(OnScheduleBegin);
//            m_scheduleEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage>(OnScheduleEnd);
//            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
//            m_scheduleUpdateToken = m_messenger.SubscribeOnMainThread<ScheduleUpdateMessage>(OnScheduleUpdate);
//
//            StartDate = m_schedule.StartDate;
//            if (m_schedule.DayIndex == null)
//            {
//                m_schedule.SetDay(0);
//            }
//            DayIndex = (int)m_schedule.DayIndex;
//
//            // Sync up the allocation service with the schedule service
//            if (m_allocationsService.StartDate != StartDate)
//            {
//                LoadAllocations();
//            }
//
//            Update();
//            UpdateDate(StartDate, m_schedule.StartDay);
//
//            SelectedDayIndex = (m_schedule.DayIndex == null) ? 0 : m_schedule.DayIndex.Value;
//        }
//
//        public override void OnViewClosed()
//        {
//            base.OnViewClosed();
//
//            m_allocationsService.Cancel();
//
//            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleBeginToken);
//            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleEndToken);
//            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
//            m_messenger.Unsubscribe<ScheduleUpdateMessage>(m_scheduleUpdateToken);
//        }
//
//        private async void LoadAllocations()
//        {
//            Loading = true;
//
//            await m_allocationsService.ReadAsync(StartDate, StartDate.AddDays(7));
//
//            if(m_allocationsService.TillAllocations != null)
//            {
//                Loading = false;
//            }
//        }
//
//        public void OnSelectDay(int index)
//        {
//            DayIndex = index;
//
//            m_schedule.SetDay(index);
//
//            UpdateDate(StartDate, m_schedule.StartDay);
//            Update();
//        }
//
//        public async void OnMoveNext()
//        {
//            StartDate = StartDate.AddDays(7);
//            m_schedule.MoveNext(false);
//            LoadAllocations();
//
//            UpdateDate(StartDate, m_schedule.StartDay);
//        }
//
//        public async void OnMovePrev()
//        {
//            StartDate = StartDate.AddDays(-7);
//            m_schedule.MovePrev(false);
//            LoadAllocations();
//
//            UpdateDate(StartDate, m_schedule.StartDay);
//        }
//
//        public async virtual void OnRefresh()
//        {
//            m_schedule.Refresh(false);
//            LoadAllocations();
//
//            UpdateDate(StartDate, m_schedule.StartDay);
//        }
//
//        public virtual void Update()
//        {
//
//        }
//
//        protected void UpdateDate(DateTime start, int? startDay)
//        {
//
//            if (m_schedule.DayIndex.HasValue)
//            {
//                var day = start.AddDays(DayIndex);
//                int weekNumber = TescoAccountingHelper.GetWeekNumberForDate (StartDate, m_schedule.AccountingConfig);
//                Date = m_weekNumberDisplayed ?
//                    string.Format ("{0} {1}, {2}", Localiser.Get("week_label"), weekNumber, day.ToString("dddd, dd/MM/yyyy")) :
//                    string.Format ("{0}", day.ToString("dddd, dd/MM/yyyy"));
//            }
//            else
//            {
//                int weekNumber = TescoAccountingHelper.GetWeekNumberForDate (start, m_schedule.AccountingConfig);
//                Date = m_weekNumberDisplayed ?
//                    string.Format ("{0} {1}, {2} {3}", Localiser.Get("week_label") , weekNumber, Localiser.Get("starting_label"),  start.ToString ("dd/MM/yyyy")) :
//                    string.Format ("{0} {1}", Localiser.Get("starting_label"),  start.ToString ("dd/MM/yyyy")) ;
//            }
//
//            // Update days in segments
//            if(startDay != null && startDay != m_currentStartDay)
//            {
//                m_currentStartDay = (int) startDay;
//                List<string> daySegments = new List<string>();
//                for(int i = 0; i < 7; i++)
//                {
//                    //daySegments.Add(((DayOfWeek) startDay).ToString().Substring(0, 3)); // This gives three character days and not as previously set with variable length
//
//                    switch(startDay)
//                    {
//                        case 0:
//                            daySegments.Add(Localiser.Get("saturday_short"));
//                            break;
//                        case 1:
//                            daySegments.Add(Localiser.Get("sunday_short"));
//                            break;
//                        case 2:
//                            daySegments.Add(Localiser.Get("monday_short"));
//                            break;
//                        case 3:
//                            daySegments.Add(Localiser.Get("tuesday_short"));
//                            break;
//                        case 4:
//                            daySegments.Add(Localiser.Get("wednesday_short"));
//                            break;
//                        case 5:
//                            daySegments.Add(Localiser.Get("thursday_short"));
//                            break;
//                        case 6:
//                            daySegments.Add(Localiser.Get("friday_short"));
//                            break;
//                    }
//
//                    startDay++;
//                    startDay %= 7;
//                }
//                Segments = daySegments;
//            }
//        }
//
//        protected void UpdateWorking(bool noData)
//        {
//            Loading = m_scheduleWorking || m_allocationWorking || m_allocationRunWorking || noData;
//
//            if (m_allocationRunWorking)
//            {
//                LoadingMessage = Localiser.Get("running_allocation");
//            }
//            else if(noData)
//            {
//                NoData = noData;
//                LoadingMessage = Localiser.Get("no_data_loaded");
//            }
//            else
//            {
//                LoadingMessage = Loading ? Localiser.Get("loading_ellipsis_label") : string.Empty;
//            }
//
//            if (!Loading)
//            {
//                Update();
//            }
//        }
//
//        protected virtual void OnScheduleBegin(ScheduleReadBeginMessage message)
//        {
//            m_scheduleWorking = true;
//            UpdateWorking(false);
//        }
//
//        protected virtual void OnScheduleEnd(ScheduleReadEndMessage message)
//        {
//            m_scheduleWorking = false;
//            UpdateWorking(false);
//
//            if (message.SessionTimeout)
//            {
//                m_session.SignOut ();
//            }
//            else if (!message.Success)
//            {
//                //m_alertBox.ShowOK (Localiser.Get("schedule_error"), message.Message);
//                return;
//            }
//
//            Update();
//            UpdateDate(StartDate, m_schedule.StartDay);
//        }
//
//        private void OnOrgEnd(SetOrgEndMessage message)
//        {
//            if (message.SessionTimeout)
//            {
//                m_session.SignOut();
//            }
//            else
//            {
//                m_schedule.Refresh(false);
//            }
//        }
//
//        protected virtual void OnScheduleUpdate(ScheduleUpdateMessage message)
//        {
//            Update();
//        }
//    }
//}
//
