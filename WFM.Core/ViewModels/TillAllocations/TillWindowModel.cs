﻿using System;
using System.Collections.Generic;
using WFM.Core.Model;
using WFM.Core.Validators;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class TillWindowModel
    {
        public int Index { get; set; }
        public event Action TillWindowChanged;
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public ComparisonValidator StartLessThanEndValidator { get; set; }
        public OverlappingTimePeriodsValidator OverlappingWindowsValidator { get; set; }
        private IStringService m_localiser;

        public TillWindowModel(int index)
        {
            Index = index;
            m_localiser = Mvx.Resolve<IStringService>();
            StartLessThanEndValidator = new ComparisonValidator(m_localiser.Get("start_time"), () => StartTime, m_localiser.Get("end_time"), () => EndTime, ComparisonValidator.ComparisonOperator.LessThan);
        }

        public TillWindowModel(int index, TimeSpan? startTime, TimeSpan? endTime)
            : this(index)
        {
            StartTime = startTime;
            EndTime = endTime;
        }

        public TillWindowModel(int index, TillWindow tillWindow)
            : this(index)
        {
            if (tillWindow == null)
            {
                return;
            }

            StartTime = tillWindow.StartTime.TimeOfDay;
            EndTime = tillWindow.EndTime.TimeOfDay;
        }

        public TillWindow ToTillWindow()
        {
            if (StartTime == null || EndTime == null)
            {
                return null;
            }

            var tillWindow = new TillWindow()
                {
                    StartTime = DateTime.Today.Add(StartTime.Value),
                    EndTime = DateTime.Today.Add(EndTime.Value),
                };

            return tillWindow;
        }

        public void StartTimeChanged(TimeSpan? value)
        {
            StartTime = value;
            OnTillWindowChanged();
        }

        public void EndTimeChanged(TimeSpan? value)
        {
            EndTime = value;
            OnTillWindowChanged();
        }

        public void Clear()
        {
            StartTime = null;
            EndTime = null;
            OnTillWindowChanged();
        }

        protected virtual void OnTillWindowChanged()
        {
            if (TillWindowChanged != null)
            {
                TillWindowChanged();
            }
        }

        public void Validate(bool isBasketTill)
        {
            if (isBasketTill)
            {
                StartLessThanEndValidator.Validate();
                OverlappingWindowsValidator.Validate();
            }
            else
            {
                StartLessThanEndValidator.IsValid = true;
                OverlappingWindowsValidator.IsValid = true;
            }
        }

        public bool IsValid
        {
            get
            {
                return (StartLessThanEndValidator.IsValid && OverlappingWindowsValidator.IsValid);
            }
        }

        public bool Optional
        {
            get
            {
                return (Index > 0);
            }
        }
    }
}
