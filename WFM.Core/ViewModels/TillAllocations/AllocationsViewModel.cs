using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Consortium.Client.Core;
using WFM.Core.Model;
using WFM.Core.Services;
using WFM.Shared.Extensions;
using Constants = WFM.Shared.Constants;

namespace WFM.Core
{
    public class SSCSAYSTillShiftModel
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }

        public int JobID { get; set; }
        public string JobName { get; set; }
        public string JobColor { get; set; }

        public bool EndsAtMidnight { get { return End.HasValue && End.Value.TimeOfDay.Hours == 0 && End.Value.TimeOfDay.Minutes == 0 && End.Value.TimeOfDay.Seconds == 0; } }
    }

    public class AllocationsViewModel : AllocationDateSelectionViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "Allocation";
            }
        }

		protected MvxSubscriptionToken m_signOutToken;
		protected MvxSubscriptionToken m_editModeChangedToken;
		protected MvxSubscriptionToken m_tillAllocationEditToken;
        protected MvxSubscriptionToken m_orgEndToken;

		private IDictionary<string, ItemColour> m_ColourMappings = new Dictionary<string, ItemColour>();

        private bool m_noData;
        public bool NoData
        {
            get { return m_noData; }
            set { m_noData = value; RaisePropertyChanged(() => NoData); }
        }

		private string m_noDataText;
		public string NoDataText
		{
			get { return m_noDataText; }
			set { m_noDataText = value; RaisePropertyChanged(() => NoDataText); }
		}

		//Buffer Till
		private IList<TillModel> m_bufferTills;
		public IList<TillModel> BufferTills
		{
			get { return m_bufferTills; }
			set
			{
				m_bufferTills = value;
				RaisePropertyChanged(() => BufferTills);
			}
		}

        private IList<TillModel> m_tills;
        public IList<TillModel> Tills
        {
            get { return m_tills; }
            set
            {
                m_tills = value;
                RaisePropertyChanged(() => Tills);
            }
        }

        protected Tuple<float, float> m_hours = new Tuple<float, float>(0,0);
        public virtual Tuple<float, float> Hours
        {
            get
            {
                return m_hours;
            }
            set
            {
                m_hours = value;

                if (Tills != null)
                {
                    foreach (TillModel till in Tills)
                    {
                        till.Hours = value;
                    }
                }

				if (BufferTills != null) 
				{
					foreach (TillModel till in BufferTills) 
					{
						till.Hours = value;
					}
				}
            }
        }

		private bool m_editMode;
		public bool EditMode
		{
			get { return m_editMode; }
			set 
			{ 
				m_editMode = value; 
				RaisePropertyChanged(() => EditMode); 
			}
		}

		public bool CanUndo
		{
			get { return m_allocations.IsEdited; }
			set { RaisePropertyChanged(() => CanUndo); }
		}

        public virtual bool ShowDateOptions
        {
            get { return true; }
        }

        public AllocationsViewModel(
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            SessionService session, 
            IAlertBox alertBox,
            TillAllocationService allocations,
            SpareShiftService spareShifts)
            : base(schedule, messenger, session, alertBox, allocations, spareShifts)
        {
        }

        public override void Init()
        {
            base.Init();

			NoDataText = Localiser.Get ("no_data");

			if (m_allocations.IsEdited)
			{
				Revert ();
			}
			else
			{
				//SetEdit(true);
			}

            UpdateView();
        }

        public override void AttachEvents()
		{
            base.AttachEvents ();

            if(m_editModeChangedToken==null)
			    m_editModeChangedToken = m_messenger.SubscribeOnMainThread<OnEditModeMessage>(OnEditModeChanged);
            
            if(m_signOutToken==null)
			    m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            
            if(m_tillAllocationEditToken==null)
			    m_tillAllocationEditToken = m_messenger.SubscribeOnMainThread<TillAllocationEditMessage> (OnTillAllocation);

            if(m_orgEndToken == null)
                m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
		}

        public override void DetachEvents()
		{
            base.DetachEvents();

			if(m_editModeChangedToken!=null)
				m_messenger.Unsubscribe<OnEditModeMessage> (m_editModeChangedToken);

			if(m_signOutToken!=null)
				m_messenger.Unsubscribe<SignOutMessage> (m_signOutToken);

			if(m_tillAllocationEditToken!=null)
				m_messenger.Unsubscribe<TillAllocationEditMessage> (m_tillAllocationEditToken);

            if(m_orgEndToken != null)
                m_messenger.Unsubscribe<SetOrgEndMessage> (m_orgEndToken);

			m_editModeChangedToken = null;
			m_signOutToken = null;
			m_tillAllocationEditToken = null;
		}

		private void UpdateAllocations()
		{
			List<List<TillAllocation>> groupedAllocations = m_allocations.TillAllocations.
				_NewIfNull ().
				Where (a =>
					!a.CheckoutSupport &&
	                   a.StartTime < SelectedDay.Date.AddDays (1) &&
	                   a.EndTime > SelectedDay.Date).// Assumption that StartTime > EndTime
				GroupBy (a => a.StampedTillId).
				Select (a => a.ToList ()).ToList ();

			
			foreach (TillModel till in Tills) 
			{
                till.CanEdit = EditMode && !till.SSCSAYS;

                if (till.SSCSAYS)
                    continue;

				till.TillAllocations.Clear();

				var grouped = groupedAllocations.FirstOrDefault (a => a.First ().StampedTillId == till.TillId);
				//Reassign
				till.TillAllocations = (grouped!=null) ? grouped.Select (x => new TillAllocationModel (x)).ToList () : new List<TillAllocationModel>();

				//Rebuild data
				foreach(TillAllocationModel alloc in till.TillAllocations)
				{
                    alloc.Hours = till.Hours;
                    till.CanEdit = EditMode && !till.SSCSAYS;
                    

					var key = (alloc.EmployeeFullName ?? string.Empty);

					if (m_ColourMappings.ContainsKey(key))
					{
						alloc.Colour = m_ColourMappings[key];
					}

                    alloc.CanEdit = EditMode && !alloc.IsSSCSAYS;
				}
			}

			//Clear any buffer tills (but leave 1)
			for (int i = 0; i < BufferTills.Count; i++) 
			{
				if (BufferTills [i].TillAllocations.Count == 0 && BufferTills.Count != 1) 
				{
					//Remove
					Tills.Remove(BufferTills[i]);
					BufferTills.RemoveAt(i);
					i--;
				}
			}

			RaisePropertyChanged (() => Tills);
		}
            
        public override void UpdateView()
        {
            if (m_allocations != null)
            {
                var sscTills = BuildSSCSAYS();
                var orderededAllocations = m_allocations.TillAllocations._NewIfNull().OrderBy(a => a.EmployeeFullName ?? string.Empty).ToList();

                //Add SSC tills into ordering so they coloured consistently with other till allocations too :)
                foreach (var ssc in sscTills)
                {
                    if (ssc.TillAllocations == null)
                        continue;
                    
                    foreach (var alloc in ssc.TillAllocations)
                    {
                        orderededAllocations.Add (new TillAllocation ()
                            {
                                EmployeeFullName = alloc.EmployeeFullName,
                            });
                    }
                }
                orderededAllocations = orderededAllocations.OrderBy (a => a.EmployeeFullName ?? string.Empty).ToList ();

                BuildColourMappingsForAllocations(orderededAllocations);

				//Generate Buffer Till Data
				BufferTills = new List<TillModel> ();
				AddBufferTill();

                var tills = m_allocations.TillAllocations
                ._NewIfNull()
                .Where(a =>
                    !a.CheckoutSupport &&
                    a.StartTime >= SelectedDay.Date &&
                    a.StartTime < SelectedDay.Date.AddDays(1) )
                .GroupBy(a => a.StampedTillId)
                .Select(a => new
                {
                    RootTillAllocation = a.First(),
                    TillAllocations = a.ToList(),
                })
                .Select(t => new TillModel(t.RootTillAllocation, t.TillAllocations, SelectedDay.Date, m_ColourMappings))
                .OrderBy(t => t.DisplaySequence)
                .ToList();

                //Update colours of SSC tills and prepend to list
                for (int i = sscTills.Count - 1; i >= 0; i--)
                {
                    sscTills[i].SetupColours ();
                    tills.Insert (0, sscTills[i]);
                }

                Tills = tills;

				NoData = Tills.Count == 0;

				SetEdit (EditMode);

                Hours = new Tuple<float, float>(0.0f, 24.0f);
            }
        }

        private List<TillModel> BuildSSCSAYS()
        {
            //If we have 1 grouping, then our new tills will be combined names, otherwise JOBNAME is the name of these custom tills
            string combinedName = m_allocations.CombinedSSCSAYSTillName;
            List<TillModel> extraTills = new List<TillModel> ();

            //Get all employees with shifts in the jobs configured
            if (m_schedule.Schedule != null && m_schedule.Schedule.Shifts != null && m_schedule.Schedule.Employees != null)
            {
                //First get all the shifts that contains jobs we are interested in (SSC and SAYS only, checkout we don't care)
                var relevantShifts = m_schedule.Schedule.Shifts.Where (x => 
                    x.Jobs.Any(y => m_allocations.JobIsRelevant (y.JobName, false) &&
                        (y.Start.Date == SelectedDay.Date || ( y.End.Date == SelectedDay.Date && !y.EndsAtMidnight)))).ToList();

                //Now, get all the job assignments specifically, some shifts might contain >1 job, and we only want the jobs we care about.
                //Get the EmployeeID while we're at it too
                var jobs = relevantShifts.SelectMany (x => x.Jobs.Where(y => m_allocations.JobIsRelevant(y.JobName, false)), 
                               (parent, child) => new SSCSAYSTillShiftModel ()
                    { 
                        EmployeeID = parent.EmployeeID.GetValueOrDefault(), 
                        JobID = child.JobID,
                        JobName = child.JobName, 
                        JobColor = child.JobColor,
                        Start = child.Start, 
                        End = child.End 
                    }).ToList();

                //Get Names for these employee IDs
                foreach (var shift in jobs)
                {
                    var employee = m_schedule.Schedule.Employees.FirstOrDefault (x => x.EmployeeID == shift.EmployeeID);
                    if(employee!=null)
                        shift.EmployeeName = employee.Name;
                }

                //Group by job, now we have a list of ALL jobs that are each SSC/SAYS specific so we can build tills...
                var groupings = jobs.GroupBy(x => x.JobName).ToList();
                foreach (var g in groupings)
                {
                    //Lets build some SSC AND SAYS tills finally.
                    //For each job, start from the first till and add if no overlap, otherwise move onto the next till, if no till then add a new one
                    string name = (groupings.Count > 1 || !ConfigurationService.ShowCombinedSSCSAYSTill) ? g.Key : combinedName;
                    string displayName = m_allocations.GetSSCSAYSDisplayName (name);
                    foreach (var job in g)
                    {
                        //Loop through tills looking for the first that can "fit" the job (no overlap). If none, make new and append.
                        bool added = false;
                        TillModel till = null;
                        foreach (var t in extraTills)
                        {
                            if (t.TillNumber != displayName)
                                continue;

                            if (job.Start.GetValueOrDefault ().Date == SelectedDay.Date ||
                                (job.End.GetValueOrDefault ().Date == SelectedDay.Date && !job.EndsAtMidnight))
                            {
                                bool overlap = t.TillAllocations.Any (x => x.StartTime < job.End && job.Start < x.EndTime);
                                if (!overlap)
                                {
                                    added = true;
                                    till = t;
                                    break;
                                }
                            }
                        }

                        if (!added)
                        {
                            till = MakeSSCSAYSTill (displayName);
                            extraTills.Add (till);
                        }

                        //Add job to till
                        till.TillAllocations.Add (new TillAllocationModel ()
                            {
                                IsSSCSAYS = true,
                                TillAllocationId = till.TillId,
                                TillId = till.TillId,
                                EmployeeFullName = job.EmployeeName,
                                StartTime = job.Start.GetValueOrDefault(),//TODO Ensure jobs starting on previous day start 00:00???
                                EndTime = job.End.GetValueOrDefault(), //TODO Ensure jobs ending on next day end 23:59???
                            });
                    }
                }

            }

            //Add Empty extra till
            if (extraTills.Count == 0 && !ConfigurationService.HideEmptySSCSAYSTill)
                extraTills.Add (MakeSSCSAYSTill (combinedName));


            //Order tills - SAYS -> SSC
            if (extraTills.Count > 1)
                extraTills = extraTills.OrderBy (x => x.TillNumber).ToList();//bit sneaky

            return extraTills;
        }

        void BuildColourMappingsForAllocations(List<TillAllocation> orderededAllocations)
        {
            for (int i = 0; i < orderededAllocations.Count; i++)
            {
                var key = (orderededAllocations[i].EmployeeFullName ?? string.Empty);

                if (!m_ColourMappings.ContainsKey(key))
                {
                    var colourIndex = (m_ColourMappings.Count % ConstantValues.ItemBackgrounds.Length);
                    var itemColours = new ItemColour(ConstantValues.ItemBackgrounds[colourIndex], ConstantValues.ItemBorders[colourIndex]);
                    m_ColourMappings.Add(key, itemColours);
                }
            }
        }

		private TillModel AddBufferTill(int id = -1, bool force = false)
		{
			//Look for lowest id needed
			if (!force) 
			{
				foreach (TillModel model in BufferTills) 
				{
					if (model.TillId <= id)
						id = model.TillId - 1;
				}
			}

			//Generate Buffer Till Data
			var bufferAllocation = new TillAllocation ();
			bufferAllocation.TillAllocationId = id;
			bufferAllocation.TillId = id;
			bufferAllocation.StampedTillId = id;
			bufferAllocation.TillTypeKey = TillAllocation.BufferTillType;
			bufferAllocation.TillStatusKey = "Open";
			bufferAllocation.TillNumber = "Unassigned Allocations";
			bufferAllocation.TillOpeningSequence = id;
            bufferAllocation.TillDisplaySequence = id;

            var bufferTill = new TillModel (bufferAllocation, new List<TillAllocation> (), SelectedDay.Date, m_ColourMappings){ Hours = Hours };

			BufferTills.Insert(0, bufferTill);

			return bufferTill;
		}

        private TillModel MakeSSCSAYSTill(string jobName)
        {
            int id = int.MinValue;

            //Generate SSC SAYS Till Data
            var bufferAllocation = new TillAllocation ();
            bufferAllocation.TillAllocationId = id;
            bufferAllocation.TillId = id;
            bufferAllocation.StampedTillId = id;
            bufferAllocation.TillTypeKey = TillAllocation.SSCSAYSTillType;
            bufferAllocation.TillStatusKey = "Open";
            bufferAllocation.TillNumber = jobName;
            bufferAllocation.TillOpeningSequence = id;
            bufferAllocation.TillDisplaySequence = id;

            return new TillModel (bufferAllocation, new List<TillAllocation> (), SelectedDay.Date, m_ColourMappings){ Hours = Hours, SSCSAYS = true };
        }

        public void OnRunAllocation()
        {
            if (!Loading)
            {
                RunAllocationViewModel.Parameters parameters = new RunAllocationViewModel.Parameters()
                {
                    StartDate = StartDate,
                    Offset = DayIndex,
                    EndDayOffset = 6
                };

                ShowViewModel<RunAllocationViewModel>(parameters);
            }
        }

        public void OnReturn(Tuple<DateTime,DateTime> allocationPeriod)
        {
			if(allocationPeriod != null)
            {
				PerformAllocation(allocationPeriod.Item1,allocationPeriod.Item2);
			}
        }

        public void OnReturn()
        {

        }

        private async void PerformAllocation(DateTime start, DateTime end)
        {
            LoadingService = true;

            LoadingMessage = "Running allocation";

            await m_allocations.CreateAsync(start, end);

            LoadingService = false;

			LoadAllocationsForPeriod (true);
        }

		protected override bool CanChangeDate ()
		{
			if (EditMode && m_allocations.IsEdited) 
			{
				OnEdit ();
				return false;
			}

			return base.CanChangeDate ();
		}

		public void OnEdit()
		{
			if (EditMode)
			{
				if (m_allocations.IsEdited)
				{
					//Have we got unassigned tills?
					bool buffers = false;
					if (BufferTills.Count > 0) 
					{
						foreach (TillModel till in BufferTills)
							buffers |= till.TillAllocations.Count > 0;
					}

					if (buffers) 
					{
						m_alertBox.Show(Localiser.Get ("save_tills_message_unassigned"),
							Localiser.Get ("save_tills_ellipsis_message_unassigned"),
							Localiser.Get ("discard_changes_message"),
							Localiser.Get ("continue_editing_message"),
							(i) => {
								switch (i) {
								case 1:
									Revert ();
									break;
								}
							});
					} 
					else 
					{
						m_alertBox.ShowOptions(
							Localiser.Get("save_tills_message"), 
							Localiser.Get("save_tills_ellipsis_message"), 
							new List<string>() {  Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
							Localiser.Get("continue_editing_message"),
							(i) =>
							{
								switch (i)
								{
								case 1:
									Save();
									break;
								case 2:
									Revert();
									break;
								}
							});
					}

				}
				else
				{
					// End Edit Mode
					SetEdit (false);
				}
			}
			else
			{
				// Start Edit mode
				SetEdit (true);
			}
		}

		void Revert()
		{
			SetEdit(false);
			m_allocations.UndoAll ();
			//Refresh
			UpdateAllocations();
		}

		async void Save()
		{
			LoadingSubmitChanges = true;

			LoadingMessage = "Uploading changes...";

			await m_allocations.Save(Tills);

			SetEdit (false);

			LoadAllocationsForPeriod (true, false);

			LoadingSubmitChanges = false;
		}

		protected virtual void SetEdit(bool canEdit)
		{
			EditMode = canEdit;

			if (ConfigurationService.ShowTillAllocationBuffer)
			{
				if (BufferTills != null)
				{
					foreach (TillModel bufferTill in BufferTills) 
					{
                        bufferTill.TillAllocations.Clear ();

                        if (!m_editMode)
                        {
                            if (Tills.Contains (bufferTill))
                            {
                                Tills.Remove (bufferTill);
                            }
                        }
                        else
                        {
                            if (!Tills.Contains (bufferTill))
                            {
                                Tills.Insert (0, bufferTill);
                            }
                        }
					}
				}
			}

			foreach (TillModel till in Tills) 
			{
                till.CanEdit = canEdit && !till.SSCSAYS;
				foreach (TillAllocationModel alloc in till.TillAllocations) 
				{
                    alloc.CanEdit = canEdit && !alloc.IsSSCSAYS;
				}
			}

			if (!canEdit) 
			{
				m_allocations.UndoAll ();
			}

			//Trigger cell refresh
			RaisePropertyChanged (() => Tills);
			RaisePropertyChanged (() => CanUndo);

		}

		public void OnUndo()
		{
			if (CanUndo) 
			{
				if (m_allocations.Undo ()) 
				{
					//Recreate any buffer tills we might need
					if (ConfigurationService.ShowMultipleTillAllocationBuffers) 
					{
						foreach (TillAllocation alloc in m_allocations.TillAllocations) 
						{
							if (alloc.StampedTillId == null || alloc.StampedTillId >= 0)
								continue;

							//Look for a Buffer
							bool found = false;
							foreach(TillModel till in BufferTills)
							{
								if (till.TillId == alloc.StampedTillId) 
								{
									found = true;
									break;
								}
							}

							if (!found) 
							{
								TillModel buffer = AddBufferTill ((int)alloc.StampedTillId, true);
								Tills.Insert (0, buffer);
							}
						}
					}

					UpdateAllocations ();//Call this directly, or invalidate or what?
					RaisePropertyChanged(() => CanUndo);
				}
			}
		}

		void OnEditModeChanged(OnEditModeMessage obj)
		{
			SetEdit(true);
		}

		private void OnTillAllocation(TillAllocationEditMessage msg)
		{
			bool refresh = false;

			//Check if we need to make a new Buffer Till and remove any empty ones
			if (msg.TillID == TillAllocation.NewBufferTillId) 
			{
				//Make new
				var bufferTill = AddBufferTill();

				Tills.Insert (0, bufferTill);

				//Remap new ID
				msg.TillID = bufferTill.TillId;

				//Make sure we refresh to see the new buffer till
				refresh = true;
			}


			refresh |= m_allocations.UpdateTillAllocation (msg.TillAllocationID, msg.TillID);

			if (refresh) 
			{
				UpdateAllocations ();
				RaisePropertyChanged (() => CanUndo);
			}
		}

		private void OnSignOut(SignOutMessage message)
		{
			// Unsubscribe from messages
			m_messenger.Unsubscribe<OnEditModeMessage>(m_editModeChangedToken);
			m_messenger.Unsubscribe<SignOutMessage>(m_signOutToken);
		}

        protected virtual void OnOrgEnd(SetOrgEndMessage message)
        {
            LoadAllocationsForPeriod (true);
        }
           
		public class TillModel : MvxNotifyPropertyChanged
        {
			public enum HighlightState
			{
				Normal = 0,
				Disabled,
				Blocked,
			}

            public int TillId { get; set; }
            public string TillNumber { get; set; }
            public string TillTypeKey { get; set; }
            public string TillStatusKey { get; set; }
            public int? OpeningSequence { get; set; }
            public int? DisplaySequence { get; set; }
			public bool CheckoutSupport { get; set; }
            public bool SSCSAYS { get; set; }
            public IList<TillAllocationModel> TillAllocations { get; set; }
            public IList<TillWindowModel> TillWindows { get; set; }
            public DateTime StartDate { get; set; }
            public bool CanEdit { get; set; }
            public IDictionary<string, ItemColour> Colors { get; set; }

			private HighlightState m_highlightState = HighlightState.Normal;
			public HighlightState Highlight
			{
				get { return m_highlightState; }
				set 
				{
					m_highlightState = value;
					RaisePropertyChanged (() => Highlight);
				}
			}

            Tuple<float, float> m_hours = new Tuple<float, float>(0,0);
            public Tuple<float, float> Hours
            {
                get
                {
                    return m_hours;
                }
                set
                {
                    m_hours = value;

					if(HoursChanged != null)
					{
						HoursChanged();
					}

                    foreach (TillAllocationModel tillAllocation in TillAllocations)
                    {
                        tillAllocation.Hours = value;
                    }
                    // Propagate to all TillWindowModel
                    foreach (TillWindowModel tillWindow in TillWindows)
                    {
                        tillWindow.Hours = value;
                    }
                }
            }

			public TillModel(TillAllocation rootTillAllocation, IList<TillAllocation> tillAllocations, DateTime startDate, IDictionary<string, ItemColour> colours)
            {
                TillId = rootTillAllocation.StampedTillId.Value; // Assumption that not null
                TillNumber = rootTillAllocation.TillNumber;
                TillTypeKey = rootTillAllocation.TillTypeKey;
                TillStatusKey = rootTillAllocation.TillStatusKey;
                OpeningSequence = rootTillAllocation.TillOpeningSequence;
                DisplaySequence = rootTillAllocation.TillDisplaySequence;
				CheckoutSupport = rootTillAllocation.CheckoutSupport;
                TillAllocations = tillAllocations.Select(a => new TillAllocationModel(a)).ToList();
                TillWindows = rootTillAllocation.TillWindows._NewIfNull().Select(w => new TillWindowModel(w)).ToList();
                StartDate = startDate;
				Colors = colours;
                SSCSAYS = false;

                SetupColours();    
            }
            
            public void SetupColours()
            {
                foreach(TillAllocationModel alloc in TillAllocations)
                {
                    alloc.Colour = GetColourForAllocation(alloc);
                }
            }

            public ItemColour GetColourForAllocation(TillAllocationModel model)
            {
                var colour = Colors.Values.FirstOrDefault();

                Colors.TryGetValue(model.EmployeeFullName, out colour);

                //TryGet will null this so do it again
                if(colour == null)
                    colour = Colors.Values.FirstOrDefault();
                
                return colour;
            }

			public Action HoursChanged
			{
				get;
				set;
			}

            public bool IsBasket
            {
                get
                {
                    return (TillTypeKey == Constants.TillTypeKey.Basket);
                }
            }

            public bool IsOutOfOrder
            {
                get
                {
                    return (TillStatusKey == Constants.TillStatusKey.OutOfOrder);
                }
            }

            public string Order
            {
                get
                {
                    return DisplaySequence.ToString();
                }
            }
        }

        public class TillAllocationModel
        {
            public int TillAllocationId { get; set; }
			public int TillId { get; set; }
            public string EmployeeFullName { get; set; }
			public DateTime StartTime { get; set; }
			public DateTime EndTime { get; set; }
            public bool IsSSCSAYS { get; set; }
            private bool m_canEdit;
            public bool CanEdit { 
                get { return m_canEdit; } 
                set
                {
                    if (StartTime == EndTime || IsSSCSAYS)
                        m_canEdit = false;
                    else
                        m_canEdit = value;
                }
            }

            public AllocationsViewModel.ItemColour Colour { get; set; }


            Tuple<float, float> m_hours = new Tuple<float, float>(0,0);
            public Tuple<float, float> Hours
            {
                get
                {
                    return m_hours;
                }

                set
                {
                    m_hours = value;

                    if (HoursChanged != null)
                        HoursChanged();
                }
            }

            public Action HoursChanged
            {
                get;
                set;
            }

			public TillAllocationModel()
			{
			}

            public TillAllocationModel(TillAllocation tillAllocation)
            {
                TillAllocationId = tillAllocation.TillAllocationId;
				TillId = (int)tillAllocation.StampedTillId;
                EmployeeFullName = tillAllocation.EmployeeFullName;
                StartTime = tillAllocation.StartTime; // ToDo: Ensure allocations that start on the previous day are set to 00:00
                EndTime = tillAllocation.EndTime; // ToDo: Ensure allocations that end on the next day are set to 24:00
            }
        }

        public class TillWindowModel
        {
            public int TillAllocationTillWindowId { get; set; }
            public TimeSpan StartTime { get; set; }
            public TimeSpan EndTime { get; set; }

            public Action HoursChanged
            {
                get;
                set;
            }

            Tuple<float, float> m_hours = new Tuple<float, float>(0,0);
            public Tuple<float, float> Hours
            {
                get
                {
                    return m_hours;
                }

                set
                {
                    m_hours = value;

                    if (HoursChanged != null)
                        HoursChanged();
                }
            }

            public TillWindowModel(TillAllocationTillWindow tillWindow)
            {
                TillAllocationTillWindowId = tillWindow.TillAllocationTillWindowId;
                StartTime = tillWindow.StartTime;
                EndTime = tillWindow.EndTime;
            }
        }

		public class ItemColour
		{
			public string BackgroundColour { get; set; }
			public string BorderColour { get; set; }

			public ItemColour(string backgroundColour, string borderColour)
			{
				BackgroundColour = backgroundColour;
				BorderColour = borderColour;
			}
		}
    }
}

