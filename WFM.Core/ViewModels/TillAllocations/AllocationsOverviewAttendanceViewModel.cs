﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Linq;
using Cirrious.MvvmCross.ViewModels;
using System.Threading.Tasks;
using WFM.Shared.Extensions;
using WFM.Core.Services;

namespace WFM.Core
{
    public class AllocationsOverviewAttendanceViewModel: WFMViewModel
    {
        private readonly AttendanceService m_attendanceService;
        private readonly ScheduleService m_scheduleService;
        private readonly IMvxMessenger m_messenger;
        private readonly IAlertBox m_alertService;
        private readonly TillAllocationService m_allocations;

        protected MvxSubscriptionToken m_scheduleEndToken;
        protected MvxSubscriptionToken m_signOutToken;
        private MvxSubscriptionToken m_scheduleRefreshToken;
        private MvxSubscriptionToken m_allocationRetrievedToken;

        private bool m_isListEmpty;
        public bool IsListEmpty
        {
            get{ return m_isListEmpty; }
            set
            {
                m_isListEmpty = value;
                RaisePropertyChanged (() => IsListEmpty);
            }
        }

        public override string AnalyticsScreenName
        {
            get
            {
                return "AllocationOverviewAttendance";
            }
        }

        private bool m_loading = false;
        public bool Loading
        {
            get { return m_loading; }
            set
            {
                m_loading = value;
                RaisePropertyChanged (() => Loading);
            }
        }

        private List<AttendanceEntry> m_attendance;
        public List<AttendanceEntry> Attendance
        {
            get { return m_attendance; }
            set 
            {
                m_attendance = value; RaisePropertyChanged (() => Attendance);
                IsListEmpty = m_attendance.Count < 1;
            }
        }

        private int m_firstInIndex = 0;
        public int FirstInIndex
        {
            get { return m_firstInIndex; }
            set { m_firstInIndex = value; RaisePropertyChanged (() => FirstInIndex); }
        }

        public AllocationsOverviewAttendanceViewModel (
            AttendanceService attendance, 
            ScheduleService schedule,
            IMvxMessenger messenger,
            IAlertBox alert,
            TillAllocationService allocation)
        {
            m_attendanceService = attendance;
            m_scheduleService = schedule;
            m_messenger = messenger;
            m_alertService = alert;
            m_allocations = allocation;
        }

        public async Task Init()
        {
            AttachEvents ();
            await Update();
        }

        public async Task Refresh()
        {
            await Update();
        }

        private async Task Update()
        {
            if (Loading || m_scheduleService == null || m_scheduleService.Schedule == null)
                return;

            Loading = true;

            //Get current shifts in from attendance
            List<JDAShift> shifts = new List<JDAShift>();
            List<AttendanceEntry> attendance = new List<AttendanceEntry> ();
            List<JDAShift> unTakenShifts = new List<JDAShift> ();

            if (m_scheduleService.Schedule.Shifts != null && m_scheduleService.Schedule.Employees != null)
            {
                //First get all the shifts that contains jobs we are interested in
                shifts = m_scheduleService.Schedule.Shifts.Where (x => 
                    x.Jobs.Any(y => m_allocations.JobIsRelevant (y.JobName) && y.Start.Date == DateTime.Now.Date)).ToList();

                Result<List<int>> shiftsIn = await m_attendanceService.GetCurrentShiftsIn (shifts.Select (x => x.ShiftID).ToList ());

                //Check things are still okay to load, schedule might have nulled by now
                if (m_scheduleService.Schedule == null || m_scheduleService.Schedule.Employees == null)
                {
                    Loading = false;
                    Refresh ();
                    return;
                }

                if(shiftsIn.Success && shiftsIn.Object != null)
                {
                    unTakenShifts = shifts.Where (shift => !shiftsIn.Object.Contains (shift.ShiftID)).ToList();
                }

                //Build Data
                attendance = unTakenShifts.Select (x => new AttendanceEntry ()
                    {
                        ShiftID = x.ShiftID, 
                        Jobs = x.Jobs,
                        EmployeeID = x.EmployeeID.GetValueOrDefault(),
                        EmployeeName = m_scheduleService.Schedule.Employees.FirstOrDefault (y => y.EmployeeID == x.EmployeeID)._NewIfNull().Name,
                        IsIn = false,
                        Start = x.Start,
                        End = x.End,
                        FirstTillName = string.Empty,
                        CurrentShift = x.Start <= DateTime.Now && x.End >= DateTime.Now,
                    }).ToList ();
                
                attendance.Sort ((x, y) => x.Start.CompareTo (y.Start));
            }

            Attendance = attendance;
            
            //Get FIrst in
            int index = -1;
            for (int i = 0; i < attendance.Count; i++)
            {
                if (attendance[i].CurrentShift)
                {
                    index = i;
                    break;
                }
            }
            
            if(index != -1)
                FirstInIndex = index;

            //Get Till Allocations if they are there
            BuildFirstTillData();

            Loading = false;
        }

        private void BuildFirstTillData()
        {
            //Attempt to match till data up to attendance data (just don't bother if the data is never going to match...)
            if (Attendance == null || m_allocations.TillAllocations == null || m_allocations.IsLoadingAllocations || 
                m_allocations.StartDate > DateTime.Now.Date || m_allocations.EndDate < DateTime.Now.Date)
                return;

            foreach (var attendance in Attendance)
            {
                //Look for till with this person + time
                foreach (var till in m_allocations.TillAllocations)
                {
                    if (till.EmployeeId == attendance.EmployeeID &&
                        till.StartTime == attendance.Start)
                        attendance.FirstTillName = string.Format(Localiser.Get("attendance_cell_first_till"), till.TillNumber);
                }

                //Check if we are SSC or SAYS
                if (string.IsNullOrEmpty (attendance.FirstTillName))
                {
                    foreach (var custom in m_allocations.SSCSAYSConfigurations)
                    {
                        foreach (var job in attendance.Jobs)
                        {
                            if(job.Start == attendance.Start &&
                                custom.JobName == job.JobName)
                                attendance.FirstTillName = string.Format(Localiser.Get("attendance_cell_first_till"), custom.DisplayName);
                        }
                    }
                }
            }

            //Update View to new changes
            RaisePropertyChanged (() => Attendance);
        }

        public void OnAllocationsRetrieved(OnTillAllocationDataRetrievedMessage allocationReadMessage)
        {
            BuildFirstTillData ();
        }

        public void OnScheduleFinishedRead(ScheduleReadEndMessage scheduleReadEndMessage)
        {
            Refresh ();
        }

        public virtual void AttachEvents()
        {
            if(m_scheduleRefreshToken == null)
                m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage>(OnScheduleFinishedRead);

            if(m_allocationRetrievedToken == null)
                m_allocationRetrievedToken = m_messenger.SubscribeOnMainThread<OnTillAllocationDataRetrievedMessage>(OnAllocationsRetrieved);
        }

        public virtual void DetachEvents()
        {
            if(m_scheduleRefreshToken != null)
                m_messenger.Unsubscribe<ScheduleReadEndMessage> (m_scheduleRefreshToken);

            if(m_allocationRetrievedToken != null)
                m_messenger.Unsubscribe<OnTillAllocationDataRetrievedMessage> (m_allocationRetrievedToken);
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            DetachEvents ();
        }

        public async void OnAttendanceToggle(AttendanceEntry entry)
        {
            if (Loading)
                return;
            
            entry.IsIn = !entry.IsIn;

            //TODO Figure out how we sync nicely and keep UI updating
            List<int> shiftsIn = Attendance.Where(x => x.IsIn).Select(x => x.ShiftID).ToList();
            List<int> shiftsOut = Attendance.Where (x => !x.IsIn).Select (x => x.ShiftID).ToList ();

            //Submit
            Result result = await m_attendanceService.SetCurrentAttendance(shiftsIn, shiftsOut);

            if (!result.Success)
            {
                //TODO Error message and reload to get back in sync
                await Update ();
            }
            else
            {
                Attendance.Remove (entry);
                RaisePropertyChanged (() => Attendance);
            }
        }
    }
}

