﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using WFM.Core.Services;
using WFM.Shared.Extensions;
using System.Linq;
using System.Collections.Generic;

namespace WFM.Core
{
    public class AllocationsOverviewAllocationViewModel: AllocationsViewModel
    {

        private readonly IMvxMessenger m_messenger;

        private MvxSubscriptionToken m_scheduleRefreshToken;

        public override string AnalyticsScreenName
        {
            get
            {
                return "AllocationOverviewAllocation";
            }
        }

        public override Tuple<float, float> Hours
        {
            get 
            { 
                return m_hours;
            }

            set
            {
                float currentTime = (float)DateTime.Now.Hour + ((float)(DateTime.Now.Minute) / 60.0f);
                float startHours = currentTime - 2.0f;
                float endHours = currentTime + 2.0f;
                m_hours = new Tuple<float, float> (startHours, endHours);

                if (Tills != null)
                {
                    foreach (TillModel till in Tills)
                    {
                        till.Hours = m_hours;
                    }
                }

                if (BufferTills != null) 
                {
                    foreach (TillModel till in BufferTills) 
                    {
                        till.Hours = m_hours;
                    }
                }
            }
        }

        public override bool ShowDateOptions
        {
            get { return false; }
        }

        public AllocationsOverviewAllocationViewModel (
            ScheduleService schedule, 
            IMvxMessenger messenger, 
            SessionService session, 
            IAlertBox alertBox,
            TillAllocationService allocations,
            SpareShiftService spareShift)
            : base(schedule, messenger, session, alertBox, allocations, spareShift)
        {
            m_messenger = messenger;
        }

        public void ScheduleReadEndMessage(ScheduleReadEndMessage ScheduleReadEndMessage)
        {
            int dayOfWeek = (DateTime.Now.Date.DayOfWeek == DayOfWeek.Sunday) ? 6 : (int)DateTime.Now.Date.DayOfWeek - 1;
            m_schedule.SetDay (dayOfWeek);

            //Update the current date
            if (StartDate != m_schedule.StartDate || DayIndex != (int)m_schedule.DayIndex)
            {
                StartDate = m_schedule.StartDate;
                DayIndex = (int)m_schedule.DayIndex;
                UpdateDate(StartDate, m_schedule.StartDay);
                SelectedDayIndex = m_schedule.DayIndex.Value;
            }

            OnRefresh ();
        }

        public override void AttachEvents()
        {
            base.AttachEvents ();

            if(m_scheduleRefreshToken==null)
                m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (ScheduleReadEndMessage);
        }

        public override void DetachEvents()
        {
            base.DetachEvents();

            if(m_scheduleRefreshToken!=null)
                m_messenger.Unsubscribe<ScheduleReadEndMessage> (m_scheduleRefreshToken);

            m_scheduleRefreshToken = null;
        }

        protected override void SetEdit(bool canEdit)
        {
        }

        protected override void LoadAllocationsForPeriod(bool refresh = false, bool updateMessage = true, bool doScheduleRefresh = false)
        {
            base.LoadAllocationsForPeriod (refresh, updateMessage, false);
        }
    }
}

