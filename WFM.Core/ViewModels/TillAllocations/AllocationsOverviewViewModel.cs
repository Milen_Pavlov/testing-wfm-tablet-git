using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Constants = WFM.Shared.Constants;
using System.Threading;
using WFM.Core.Services;

namespace WFM.Core
{
    public class AllocationsOverviewViewModel : WFMViewModel
    {
        private readonly ScheduleService m_scheduleService;
        private readonly SessionData m_sessionData;
        private readonly IMvxMessenger m_messenger;
        private readonly TillAllocationService m_allocationService;

        private MvxSubscriptionToken m_scheduleRefreshToken;
        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();
        private CancellationTokenSource m_attachViewsTokenSource = new CancellationTokenSource();

        public override string AnalyticsScreenName
        {
            get
            {
                return "AllocationOverview";
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged (() => Loading); }
        }

        private bool m_viewsShown = false;

        public AllocationsOverviewViewModel(ScheduleService scheduleService, SessionData sessionData, IMvxMessenger messenger, TillAllocationService allocationService)
        {
            m_scheduleService = scheduleService;
            m_sessionData = sessionData;
            m_messenger = messenger;
            m_allocationService = allocationService;
            m_viewsShown = false;
        }

        public async void Refresh(bool forced = false)
        {
            Loading = forced;
            await m_scheduleService.Refresh (forced);
        }

        public async void Init ()
        {
            Loading = true;
            m_tokenSource = new CancellationTokenSource();

            //Reset schedule to this week so overview is showing correct data
            m_scheduleService.Cancel ();
            m_scheduleService.ClearData ();

            await m_scheduleService.Refresh (false);

            if (m_tokenSource.IsCancellationRequested)
                return;
            
            AttachEvents ();
        }

        public void OnScheduleFinishedRead(ScheduleReadEndMessage scheduleReadEndMessage)
        {
            Loading = m_allocationService.ReloadScheduleIfNotCorrect ();
        }

        public void SetupViewModels()
        {
            if (m_viewsShown == false)
            {
                m_viewsShown = true;
                //TODO Do we have these broadcast when they are "done" to set "Loading" to false?
                ShowViewModel<AllocationsOverviewAllocationViewModel> (null, ViewShowType.Local);
                ShowViewModel<AllocationsOverviewBreaksViewModel> (null, ViewShowType.Local);
                ShowViewModel<AllocationsOverviewVarianceViewModel> (null, ViewShowType.Local);
                ShowViewModel<AllocationsOverviewAttendanceViewModel> (null, ViewShowType.Local);
            }
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();

            DetachEvents ();
        }

        public void AttachEvents()
        {
            if(m_scheduleRefreshToken == null)
                m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleFinishedRead);
        }

        public void DetachEvents()
        {
            m_tokenSource.Cancel ();

            if(m_scheduleRefreshToken != null)
                m_messenger.Unsubscribe<ScheduleReadEndMessage> (m_scheduleRefreshToken);

            m_scheduleRefreshToken = null;
        }
    }
}

