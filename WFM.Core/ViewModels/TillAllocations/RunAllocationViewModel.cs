﻿using System;
using Consortium.Client.Core;
using Newtonsoft.Json;
using System.Collections.Generic;
using WFM.Core.Services;
using WFM.Core.Validators;
using System.Linq;
using Consortium.Client.Core.ViewModels;

namespace WFM.Core
{
    public class RunAllocationViewModel : WFMViewModel
    {
        private bool m_weekNumberDisplayed;

        public class Parameters
        {
            public DateTime StartDate { get; set; }
            public int Offset { get; set; }
			public int EndDayOffset { get; set; }
        }

        public class DaySelectionItem
        {
            public string Name { get; set; }
            public int Offset { get; set; }
        }

        private string m_summary;
        public string Summary
        {
            get { return m_summary; }
            set { m_summary = value; RaisePropertyChanged(() => Summary); }
        }

        private string m_message;
        public string Message
        {
            get { return m_message; }
            set { m_message = value; RaisePropertyChanged(() => Message); }
        }

        private List<DaySelectionItem> m_days;
        public List<DaySelectionItem> Days
        {
            get { return m_days; }
            set { m_days = value; }
        }


		public List<SpinnerItem> SpinnerDays 
		{
			get{ return Days.Select (d => new SpinnerItem (d.Offset, d.Name)).ToList (); }
			set{ }
		}

        public DateTime StartDate { get; set; }
        public int StartOffset { get; set; }
        public int EndOffset { get; set; }
        public ComparisonValidator StartLessThanEndValidator { get; set; }

        private readonly ScheduleService m_schedule;

        public RunAllocationViewModel(
            IStringService localiser, 
            ScheduleService schedule,
            IESSConfigurationService settings)
        {
            Localiser = localiser;

            StartLessThanEndValidator = new ComparisonValidator(
                Localiser.Get("start_day"), 
                () => StartOffset,
                Localiser.Get("end_day"),
                () => EndOffset,
                ComparisonValidator.ComparisonOperator.LessThanEquals);

            m_schedule = schedule;
            m_weekNumberDisplayed = ConfigurationService.WeekNumberDisplayed;
        }

        public void Init(Parameters parameters)
        {
            StartDate = parameters.StartDate;
            StartOffset = parameters.Offset;
			EndOffset = parameters.EndDayOffset;

            Summary = m_weekNumberDisplayed ? 
                Localiser.Get("week_number") + string.Format(" {0}", m_schedule.AccountingHelper.GetWeekNumberForDate(StartDate)) :
                string.Empty;

            Days = new List<DaySelectionItem>()
            { 
                new DaySelectionItem() { Name=Localiser.Get("sunday"), Offset=(int)DayOfWeek.Sunday },
                new DaySelectionItem() { Name=Localiser.Get("monday"), Offset=(int)DayOfWeek.Monday },
                new DaySelectionItem() { Name=Localiser.Get("tuesday"), Offset=(int)DayOfWeek.Tuesday },
                new DaySelectionItem() { Name=Localiser.Get("wednesday"), Offset=(int)DayOfWeek.Wednesday },
                new DaySelectionItem() { Name=Localiser.Get("thursday"), Offset=(int)DayOfWeek.Thursday },
                new DaySelectionItem() { Name=Localiser.Get("friday"), Offset=(int)DayOfWeek.Friday },
                new DaySelectionItem() { Name=Localiser.Get("saturday"), Offset=(int)DayOfWeek.Saturday }          
            };
                    
            //Proper wraparound mod to adjust the offset correctly so that the start date is always 0 and days prior (sunday) come at the end
            for (int i = 0; i < Days.Count; i++)
            {
                int x = Days[i].Offset - (int)StartDate.DayOfWeek;
                int m = 7;
                Days[i].Offset = (x % m + m) % m;
            }
                
            Days = new List<DaySelectionItem>(Days.OrderBy(item => item.Offset));
        }

        public void OnRunAllocation()
        {
            StartLessThanEndValidator.Validate();
            Message = StartLessThanEndValidator.ErrorMessage;

            if (StartLessThanEndValidator.IsValid)
            {

                DateTime start = StartDate.AddDays(StartOffset);
                DateTime end = StartDate.AddDays(EndOffset + 1);

                Analytics.TrackAction ("Till Allocator","Run Allocation", string.Format("{0} - {1}",start.DayOfWeek,end.DayOfWeek));

                Close(new Tuple<DateTime,DateTime>(start,end));
            }
        }

		public void StartOffsetChanged(SpinnerItem item)
		{
			int outValue;
			bool success = int.TryParse (item.Key, out outValue);
			if(success){this.StartOffset = outValue;}
		}

		public void EndOffsetChanged(SpinnerItem item)
		{
			int outValue;
			bool success = int.TryParse (item.Key, out outValue);
			if(success){this.EndOffset = outValue;}
		}

        public void OnCancel()
        {
            Close();
        }
    }
}