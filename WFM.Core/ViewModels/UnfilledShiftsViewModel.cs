﻿using System;
using Consortium.Client.Core;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Linq;

namespace WFM.Core
{
    public class UnfilledShiftsViewModel : WFMViewModel
    {
        private const int c_selectionIndexNone = -1; // This is fine at least for Android

        private readonly ScheduleService m_scheduleService;
        private JDAShift m_selectedJDAShift = null;
        private readonly IMvxMessenger m_messenger;
        private readonly IAlertBox m_alert;
        private readonly IToastService m_toastService;

        private List<JDAShift> m_unfilledShifts;
        public List<JDAShift> UnfilledShifts
        {
            get { return m_unfilledShifts; }
            private set 
            {
                m_unfilledShifts = value; 
                RaisePropertyChanged(() => UnfilledShifts);

                if (m_unfilledShifts.Count == 0)
                {
                    Message1 = Localiser.Get("all_shifts_filled_message");
                }
                else
                {
                    Message1 = string.Empty;
                }
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            private set 
            {
                m_loading = value; RaisePropertyChanged(() => Loading);
            }
        }


        private List<EmployeeData> m_availableEmployees;
        public List<EmployeeData> AvailableEmployees
        {
            get { return m_availableEmployees; }
            private set 
            {
                m_availableEmployees = value; RaisePropertyChanged(() => AvailableEmployees);
            }
        }

        private string m_availableEmployeeLabel;
        public string AvailableEmployeeLabel
        {
            get
            {
                return m_availableEmployeeLabel;
            }
            set
            {
                m_availableEmployeeLabel = value;
                RaisePropertyChanged( () => AvailableEmployeeLabel);
            }
        }

        private string m_message1;
        public string Message1
        {
            get { return m_message1; }
            set
            {
                m_message1 = value;
                RaisePropertyChanged(() => Message1);
            }
        }

        private string m_message2;
        public string Message2
        {
            get { return m_message2; }
            set
            {
                m_message2 = value;
                RaisePropertyChanged(() => Message2);
            }
        }

        private int m_unfilledShiftIndex;
        public int UnfilledShiftIndex
        {
			get { return m_unfilledShiftIndex; }
            set 
			{ 
				m_unfilledShiftIndex = value;
				RaisePropertyChanged(() => UnfilledShiftIndex); 
			}
        }

        public UnfilledShiftsViewModel(
            ScheduleService schedule, 
            IMvxMessenger messenger,
            IAlertBox alert,
            IToastService toastService)
        {
            m_scheduleService = schedule;
            m_messenger = messenger;
            m_alert = alert;
            m_toastService  = toastService;
        }

        public void Init()
        {
            Analytics.TrackAction ("Unfilled Shifts","Show");

            Message2 = Localiser.Get("unfilled_shift_message");
            AvailableEmployeeLabel = Localiser.Get("available_employees_label");

            UnfilledShifts = FilterUnfilledShiftByDayIndex(m_scheduleService.DayIndex , m_scheduleService.Schedule.UnfilledShifts);

            AvailableEmployees = new List<EmployeeData>();

            DeselectUnfilledShifts();

            if(UnfilledShifts.Count > 0)
            {
                OnUnfilledShiftSelected(UnfilledShifts[0]);
            }

			UnfilledShiftIndex = 0;
            Loading = false;
        }

        private void RefreshUnfilledShiftsWindow()
        {
            // Assigning the same list doesn't correctly propagate the Property to the View. 
            // So we assign to empty first, and then back. Ideally this should be fixed in CollectionControl Items
            UnfilledShifts = new List<JDAShift>();
            UnfilledShifts = FilterUnfilledShiftByDayIndex(m_scheduleService.DayIndex , m_scheduleService.Schedule.UnfilledShifts);
        }

        private List<JDAShift> FilterUnfilledShiftByDayIndex(int? dayIndex, List<JDAShift> rawUnfilledShifts)
        {
            if(dayIndex == null) // Week view
            {
                return rawUnfilledShifts.Where(r => !IsWithinPreventChangesTime(r.Start)).ToList();
            }
            else // Day view
            {
                var filteredUnfilledShifts = new List<JDAShift>();

                filteredUnfilledShifts.AddRange(rawUnfilledShifts
                                                        .Where (shift => shift.Start.DayOfWeek == (DayOfWeek)((dayIndex + m_scheduleService.StartDay - 1) % 7))
                                                        .Where (shift => !IsWithinPreventChangesTime (shift.Start))
                                                        .ToList());

                return filteredUnfilledShifts;
            }
        }

        public bool IsWithinPreventChangesTime (DateTime testTime)
        {
            int hours = ConfigurationService.PreventChangesHours;
            if (hours <= 0)
                return false;

            if (testTime < DateTime.Now.AddHours (hours))
                return true;

            return false;
        }

        private void DeselectUnfilledShifts()
        {
			m_unfilledShiftIndex = c_selectionIndexNone;
        }

        private void ClearEmployeesWindow()
        {
            AvailableEmployees = null;
        }

        public void OnDelete(JDAShift shift)
        {
            m_alert.ShowYesNo("Delete Unfilled Shift", "Are you sure?", (option) => {
                if (option == 1)
                {

                    Analytics.TrackAction ("Unfilled Shifts","Delete Unfilled");
                    // If the currently-selected UnfilledShift is the same as the one that we're deleting...
					if (UnfilledShifts.IndexOf(shift) == m_unfilledShiftIndex)
                    {
                        // ...then deselect
                        DeselectUnfilledShifts();

                        ClearEmployeesWindow();
                    }

                    m_scheduleService.Schedule.DeleteUnfilledShift(shift);

                    m_scheduleService.SetEdit(true);

                    RefreshUnfilledShiftsWindow();
                }
            });
        }

        public async void OnUnfilledShiftSelected(JDAShift shift)
        {
            Analytics.TrackAction ("Unfilled Shifts","Selected");

            m_selectedJDAShift = shift;

            Message2 = string.Empty;
            if (AvailableEmployees != null) 
            {
                AvailableEmployees.Clear ();
                RaisePropertyChanged (() => AvailableEmployees);
            }

            Loading = true;
            AvailableEmployees = await m_scheduleService.GetAvailableEmployees(shift);
            Loading = false;

            if (AvailableEmployees.Count > 0)
            {
                Message2 = "";
            }
            else
            {
                Message2 = Localiser.Get("no_employees_found");
            }

			UnfilledShiftIndex = UnfilledShifts.IndexOf(shift);
        }

        public void OnFillUnfilledShift(EmployeeData employee)
        {
            if (m_selectedJDAShift != null)
            {
                Analytics.TrackAction ("Unfilled Shifts","Fill Unfilled");

                ShiftEditMessage editOperation = new ShiftEditMessage(this) { UnfilledShiftToFill = m_selectedJDAShift, EmployeeID = employee.EmployeeID, Op = ShiftEditMessage.Operation.AssignUnfilledShift };

                m_messenger.Publish<ShiftEditMessage>(editOperation);
    
                RefreshUnfilledShiftsWindow();
                DeselectUnfilledShifts();

                ClearEmployeesWindow();

                Message2 = Localiser.Get("unfilled_shift_message");

                m_scheduleService.SetEdit(true);

                var shiftInfo = string.Format("{0} {1} to {2}, {3}", 
                                              m_selectedJDAShift.Start.ToString ("ddd d/M"), 
                                              TimeFormatter.FormatTime(m_selectedJDAShift.Start), 
                                              TimeFormatter.FormatTime(m_selectedJDAShift.End), 
                                              m_selectedJDAShift.Jobs.FirstOrDefault() == null ? string.Empty : m_selectedJDAShift.Jobs.FirstOrDefault().JobName);
                m_toastService.Show(string.Format(Localiser.Get("my_hours_shift_assigned_info"), shiftInfo, employee.Name));
            }
        }

        public override void OnViewClosed()
        {
            base.OnViewClosed();
        }

    }
}

