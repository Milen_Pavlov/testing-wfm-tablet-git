﻿using System;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class TermsOfServiceViewModel : WFMViewModel
    {
        private readonly SessionService m_session;
        private readonly ITermsOfUseService m_tosService;
        private readonly SessionData m_sessionData;
        private readonly IFileSystem m_fileSystem;

        private string m_termsOfServiceFileLocation;
        public string TermsOfServiceFileLocation
        {
            get { return m_termsOfServiceFileLocation; }
            set { m_termsOfServiceFileLocation = value; RaisePropertyChanged(() => TermsOfServiceFileLocation); }
        }

        public TermsOfServiceViewModel (SessionService session, 
                                        ITermsOfUseService tosService, 
                                        SessionData sessionData,
                                        IFileSystem fileSystem)
        {
            m_session = session;
            m_tosService = tosService;
            m_sessionData = sessionData;
            m_fileSystem = fileSystem;
        }

        public void Init()
        {
            if(m_tosService.CurrentToS != null)
                TermsOfServiceFileLocation = m_fileSystem.GetPath(m_tosService.CurrentToS.TextFileLocation);
        }

        public void OnAccept()
        {
            m_tosService.MarkUserAsAccepted(m_sessionData.CurrentUserID);
            Close();
        }

        public void OnCancel()
        {
            Close();
            m_session.SignOut();
        }
    }
}

