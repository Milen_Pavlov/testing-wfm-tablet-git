﻿using System;
using Consortium.Client.Core;
using System.Threading.Tasks;

namespace WFM.Core
{
    public class ChangePasswordViewModel : WFMViewModel
    {
        private string m_errorMessage;
        public string ErrorMessage
        {
            get
            {
                return m_errorMessage;
            }

            set
            {
                m_errorMessage = value;
                RaisePropertyChanged(() => ErrorMessage);
            }
        }

        private string m_oldPassword;
        public string OldPassword
        {
            get
            {
                return m_oldPassword;
            }
            set
            {
                m_oldPassword = value;
                RaisePropertyChanged(() => OldPassword);
            }
        }

        private string m_newPassword;
        public string NewPassword
        {
            get
            {
                return m_newPassword;
            }
            set
            {
                m_newPassword = value;
                RaisePropertyChanged(() => NewPassword);
            }
        }

        private string m_confirmPassword;
        public string ConfirmPassword
        {
            get
            {
                return m_confirmPassword;
            }
            set
            {
                m_confirmPassword = value;
                RaisePropertyChanged(() => ConfirmPassword);
            }
        }

        private readonly SessionData m_session;
        private ISpinnerService m_spinner;

        public ChangePasswordViewModel(SessionData session, ISpinnerService spinner)
        {
            m_session = session;
            m_spinner = spinner;
        }

        public void Init()
        {

        }

        public async void OnSubmitButton()
        {
            var validationResult = ValidateNewPassword();
            if(!validationResult.IsValid)
            {
                ErrorMessage = validationResult.ValidationMessage;
                return;
            }

            SpinnerToken token = m_spinner.Show("Sending");

            Task<ChangePasswordResponse> changePasswordTask = m_session.ChangePassword(OldPassword, NewPassword, ConfirmPassword);

            await Task.Delay(500);
            ChangePasswordResponse response = await changePasswordTask;

            m_spinner.Dismiss(token);

            if (response.Success)
            {
                Close(NewPassword);
            }
            else
            {
                ErrorMessage = response.Message;
            }
        }

        public void OnCancelButton()
        {
            Close(string.Empty);
        }

        private PasswordValidationResult ValidateNewPassword ()
        {
            var result = new PasswordValidationResult();

            if(string.IsNullOrEmpty(NewPassword))
            {
                result.IsValid = false;
                result.ValidationMessage = "New password cannot be empty";

                return result;
            }

            else if(string.IsNullOrEmpty(ConfirmPassword))
            {
                result.IsValid = false;
                result.ValidationMessage = "New password and Confirm password do not match";

                return result;
            }

            else if(!string.Equals(NewPassword, ConfirmPassword, StringComparison.CurrentCultureIgnoreCase))
            {
                result.IsValid = false;
                result.ValidationMessage = "New password and Confirm password do not match";

                return result;
            }

            result.IsValid = true;
            return result;
        }
    }
}