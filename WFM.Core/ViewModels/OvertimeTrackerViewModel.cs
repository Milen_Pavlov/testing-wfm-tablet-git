﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.OvertimePlanning;
using WFM.Shared.DataTransferObjects.Overtime;

namespace WFM.Core
{
    public class OvertimeTrackerViewModel : WFMViewModel
    {
        public override string AnalyticsScreenName
        {
            get
            {
                return "OvertimeTracker";
            }
        }

        private readonly IMvxMessenger m_messenger;
        private readonly ScheduleService m_schedule;
        private readonly SessionService m_session;
        private readonly PlannedOvertimeService m_plannedOvertimeService;
        private readonly ScheduleOvertimeService m_scheduleOvertimeService;
        private readonly IAlertBox m_alertBox;
        private readonly ICustomAlertService m_customAlertBox;

        protected MvxSubscriptionToken m_scheduleReadBeginToken;
        protected MvxSubscriptionToken m_scheduleReadEndToken;
        protected MvxSubscriptionToken m_orgEndToken;
        protected MvxSubscriptionToken m_scheduleRefreshToken;

        public DailyOvertimeModel EditingCell { get ; set; }
        public DayOfWeek EditingDay { get; set; }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set { m_loading = value; RaisePropertyChanged(() => Loading); }
        }

        private string m_loadingMessage;
        public string LoadingMessage
        {
            get { return m_loadingMessage; }
            set { m_loadingMessage = value; RaisePropertyChanged(() => LoadingMessage); }
        }

        private string m_date;
        public string Date
        {
            get { return m_date; }
            set { m_date = value; RaisePropertyChanged(() => Date); }
        }

        private DateTime m_weekStart;
        public DateTime WeekStart
        {
            get { return m_weekStart; }
            set { m_weekStart = value; RaisePropertyChanged (() => WeekStart);}
        }

        private List<ColumnHeader> m_headers;
        public List<ColumnHeader> Headers
        {
            get { return m_headers; }
            set { m_headers = value; RaisePropertyChanged(() => Headers); }
        }

        private List<DailyOvertimeModel> m_rows;
        public List<DailyOvertimeModel> Rows
        {
            get { return m_rows; }
            set { m_rows = value; RaisePropertyChanged(() => Rows); }
        }

        private IList<ReadPlannedOvertimeResponse.Department> m_plannedOvertime;
        private IList<OvertimeRecord> m_scheduleOvertime;

        private int m_selectedRowIndex = 0;
        public int SelectedRowIndex
        {
            get { return m_selectedRowIndex; }
            set { m_selectedRowIndex = value; RaisePropertyChanged (() => SelectedRowIndex); }
        }

        private bool m_isModified;
        public bool IsModified
        {
            get { return m_isModified; }
            set
            {
                m_isModified = value;
                RaisePropertyChanged (() => IsModified);
            }
        }

        private DateTime m_saveDate;

        public OvertimeTrackerViewModel (IMvxMessenger messenger,
            ScheduleService schedule, 
            SessionService session,
            PlannedOvertimeService plannedOvertimeService,
            ScheduleOvertimeService scheduleOvertimeService,
            IAlertBox alertBox,
            ICustomAlertService customAlertBox)
        {
            m_messenger = messenger;
            m_schedule = schedule;
            m_session = session;
            m_plannedOvertimeService = plannedOvertimeService;
            m_scheduleOvertimeService = scheduleOvertimeService;
            m_alertBox = alertBox;
            m_customAlertBox = customAlertBox;
        }

        public void Init()
        {
            m_scheduleReadBeginToken = m_messenger.SubscribeOnMainThread<ScheduleReadBeginMessage> (OnScheduleReadBegin);
            m_scheduleReadEndToken = m_messenger.SubscribeOnMainThread<ScheduleReadEndMessage> (OnScheduleReadEnd);
            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage>(OnOrgEnd);
            m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleRefreshMessage> (OnScheduleRefresh);

            Loading = true;
            m_schedule.SafeReadAsync (m_schedule.StartDate, null);
            UpdateDate (m_schedule.StartDate, m_schedule.StartDay);
        }

        public void OnRefresh()
        {
            CheckModified ((i) =>
                {
                    if (i > 0)
                    { 
                        bool shouldSave = i == 1;

                        Refresh();

                        if (shouldSave)
                        {
                            TrySave ();
                        }
                    }
                });
        }
            
        private void Refresh()
        {
            
            DoRefresh();
        }

        private async void DoRefresh()
        {
            LoadingMessage = "Loading data...";
            Loading = true;

            UpdateDate (m_schedule.StartDate, m_schedule.StartDay);

            // Load overtime data here.
            var readPlannedOvertimeOperation = m_plannedOvertimeService.ReadPlannedOvertime(m_schedule.StartDate.Date);
            var readScheduleOvertimeOperation = m_scheduleOvertimeService.ReadDepartmentOvertime(m_schedule.StartDate.Date, true);

            //Comfort break
            await Task.Delay(ConstantValues.c_comfortBreakMillis);

            var overtimeResult = await readPlannedOvertimeOperation;
            m_scheduleOvertime = await readScheduleOvertimeOperation;

            if (overtimeResult.Success == true && overtimeResult.Object != null)
                m_plannedOvertime = overtimeResult.Object.ToList ();
            else
                m_plannedOvertime = new List<ReadPlannedOvertimeResponse.Department> ();

            // Create the table of data to show.
            BuildData();

            Loading = false;
        }

        //Date navigation
        public void OnMoveNext()
        {
            CheckModified ((i) =>
                {
                    if (i > 0)
                    { 
                        bool shouldSave = i == 1;
                        m_saveDate = m_schedule.StartDate.Date;

                        m_schedule.MoveNext (shouldSave);
                        UpdateDate (m_saveDate.AddDays(7), m_schedule.StartDay);
                        if (shouldSave)
                        {
                            TrySave ();
                        }
                    }
                });
        }

        public void OnMovePrev()
        {
            CheckModified ((i) =>
                {
                    if (i > 0)
                    { 
                        bool shouldSave = i == 1;
                        m_saveDate = m_schedule.StartDate.Date;

                        m_schedule.MovePrev (shouldSave);
                        UpdateDate (m_saveDate.AddDays(-7), m_schedule.StartDay);
                        if (shouldSave)
                        {
                            TrySave ();
                        }
                    }
                });
        }

        public void SetDate(DateTime date)
        {
            //Get start of week
            DateTime start = m_schedule.AccountingHelper.GetStartOfWeekForDate(date);
            if (WeekStart == start)
                return;

            CheckModified ((i) =>
                {
                    if (i > 0)
                    { 
                        bool shouldSave = i == 1;
                        m_saveDate = m_schedule.StartDate.Date;
                        m_schedule.MoveToWeek(false, start.Date);
                        UpdateDate (date, m_schedule.StartDay);
                        UpdateDate (m_schedule.StartDate, m_schedule.StartDay);
                        if (shouldSave)
                        {
                            TrySave ();
                        }
                    }
                });
        }

        private void TrySave()
        {
            DoSave ();
        }

        private async void DoSave()
        {
            Loading = true;
            LoadingMessage = Localiser.Get("saving_ellipsis");

            // Save the overtime here
            var save = await m_plannedOvertimeService.WritePlannedOvertime(m_saveDate, Rows);

            bool needsRefresh = save.Success;
            IsModified = false;

            if (needsRefresh)
            {
                DoRefresh ();
            } 
            else 
            {
                Loading = false;
                LoadingMessage = string.Empty;
            }
        }

        private void UpdateDate(DateTime start, int? startDay)
        {
            WeekStart = start;
            int weekNumber = m_schedule.AccountingHelper.GetWeekNumberForDate (start);
            Date = string.Format (Localiser.Get ("week_starting_label"), weekNumber, start.ToString (Localiser.Get("dd/MM/yyyy")));

            UpdateHeaders ();
        }

        private void UpdateHeaders()
        {
            var headers = new List<ColumnHeader> ();

            for (int i = 0; i < 7; ++i)
            {
                var header = new ColumnHeader ()
                    {
                        Index = i,
                        Title = m_schedule.StartDate.AddDays (i).ToString (Localiser.Get("ddd dd/MM")),
                    };

                headers.Add (header);
            }

            var totalHeader = new ColumnHeader ()
                {
                    Index = 7,
                    Title = Localiser.Get("my_hours_total"),
                };

            var totalPlannedHeader = new ColumnHeader ()
                {
                    Index = 8,
                    Title = Localiser.Get("planning_overtimeagreed"),
                };
            
            headers.Add (totalHeader);
            headers.Add (totalPlannedHeader);

            Headers = headers;
        }

        //Schedule events
        private void OnScheduleReadBegin(ScheduleReadBeginMessage message)
        {
            Loading = true;
            LoadingMessage = m_schedule.State == ScheduleService.UpdateState.Saving ? " "+Localiser.Get("saving_ellipsis") : Localiser.Get("loading_ellipsis_label");
        }

        private void OnScheduleReadEnd(ScheduleReadEndMessage message)
        {
            Loading = false;
            if (message.SessionTimeout)
                m_session.SignOut ();
            else if (message.Success)
            {
                DoRefresh ();
            }

            if(!message.Success)
            {
                LoadingMessage = Localiser.Get("no_data_loaded");
            }
        }

        private void OnScheduleRefresh(ScheduleRefreshMessage message)
        {
            OnRefresh ();
        }

        private void OnOrgEnd(SetOrgEndMessage message)
        {
            if (message.SessionTimeout)
            {
                m_session.SignOut ();
            }
            else
            {
                //TODO Full refresh
                m_schedule.Refresh (false);
            }
        }

        public override void OnViewClosed ()
        {
            base.OnViewClosed ();
        
            // Unsubscribe from messages
            m_messenger.Unsubscribe<SetOrgEndMessage>(m_orgEndToken);
            m_messenger.Unsubscribe<ScheduleReadBeginMessage>(m_scheduleReadBeginToken);
            m_messenger.Unsubscribe<ScheduleReadEndMessage>(m_scheduleReadEndToken);
            m_scheduleRefreshToken = m_messenger.SubscribeOnMainThread<ScheduleRefreshMessage> (OnScheduleRefresh);
        }

        private void BuildData()
        {
            List<DailyOvertimeModel> rows = CreatePlanningRows (m_plannedOvertime, m_scheduleOvertime);

            if (rows != null)
            {
                LoadingMessage = string.Empty;
                DailyOvertimeModel total = CreateTotalRow (rows);
                rows.Insert (0, total);
            }
            else
            {
                LoadingMessage = Localiser.Get("no_data_loaded");
            }

            Rows = rows;
        }

        private List<DailyOvertimeModel> CreatePlanningRows(IList<ReadPlannedOvertimeResponse.Department> plannedOvertime, IList<OvertimeRecord> scheduledOvertime)
        {
            List<DailyOvertimeModel> rows = new List<DailyOvertimeModel>();

            if (m_schedule == null || m_schedule.Filters == null || m_schedule.Filters.Groups == null)
            {
                return null;
            }

            if(plannedOvertime == null || !plannedOvertime.Any())
            {
                return null;
            }

            if (m_schedule.Accuracy != null)
            {
                foreach (PlanningData planningData in m_schedule.Accuracy.Planning)
                {       

                    var rowEntry = BuildRowEntry (planningData, plannedOvertime);

                    if (scheduledOvertime != null)
                    {
                        var overtimeRecord = scheduledOvertime.FirstOrDefault (record => record.JDADepartmentId == planningData.ID);

                        if (overtimeRecord != null)
                        {
                            rowEntry.AdditionalResource = overtimeRecord.AllocatedHours.GetValueOrDefault() + overtimeRecord.ResourceAgreedNight.GetValueOrDefault();
                        }
                    }

                    rowEntry.Total = rowEntry.Hours.Sum (x => x.Value);
                    rowEntry.OnTotalUpdated += OnHoursEdited;

                    rows.Add (rowEntry);
                }
            }

            rows.Sort((x, y) => x.DepartmentName.CompareTo(y.DepartmentName));
            return rows;
        }

        private DailyOvertimeModel BuildRowEntry(PlanningData planningData, IList<ReadPlannedOvertimeResponse.Department> plannedOvertime)
        {
            DailyOvertimeModel rowEntry = new DailyOvertimeModel ();
            rowEntry.Editable = true;
            rowEntry.Hours = new Dictionary<DayOfWeek, double> ();
            rowEntry.HourStrings = new Dictionary<DayOfWeek, String> ();
            rowEntry.DayEdited = new Dictionary<DayOfWeek, bool> ();

            ReadPlannedOvertimeResponse.Department department = null;
            if(plannedOvertime != null)
                department = plannedOvertime.Where (x => x.DepartmentId == planningData.ID).FirstOrDefault();
            
            if (department != null)
            {
                for (int i = 0; i < 7; i++)
                {
                    double hours = 0.0;
                    if(department.Hours.ContainsKey((DayOfWeek)i))
                        hours = department.Hours[(DayOfWeek)i];
                    rowEntry.Hours.Add ((DayOfWeek)i, hours);
                    if(hours == 0)
                        rowEntry.HourStrings.Add ((DayOfWeek)i, "");
                    else
                        rowEntry.HourStrings.Add ((DayOfWeek)i, hours.ToString("0.##"));

                    rowEntry.DayEdited.Add ((DayOfWeek)i, false);
                }
            }
            else
            {
                for (int i = 0; i < 7; i++)
                {
                    rowEntry.Hours.Add ((DayOfWeek)i, 0.0);
                    rowEntry.HourStrings.Add ((DayOfWeek)i, "");
                    rowEntry.DayEdited.Add ((DayOfWeek)i, false);
                }    
            }
                
            rowEntry.DepartmentID = planningData.ID;
            rowEntry.DepartmentName = planningData.Name;

            return rowEntry;
        }

        private DailyOvertimeModel CreateTotalRow(List<DailyOvertimeModel> rows)
        {
            DailyOvertimeModel total = new DailyOvertimeModel ()
            {
                DepartmentID = -1,
                DepartmentName = "Total",
                Hours = new Dictionary<DayOfWeek, double>(),
                HourStrings = new Dictionary<DayOfWeek, String>(),
                Editable = false,
            };

            if (rows != null)
            {
                var nonNullRows = rows.Where (row => row != null).ToList ();
                for (int i = 0; i < 7; ++i)
                {
                    var rowsWithHours = nonNullRows.Where (row => row.Hours != null && row.Hours.ContainsKey ((DayOfWeek)i)).ToList();
                    double dayTotal = 0;

                    if (rowsWithHours != null && rowsWithHours.Count > 0)
                        dayTotal = rowsWithHours.Sum (row => row.Hours[(DayOfWeek)i]);
                    
                    total.Hours.Add ((DayOfWeek)i, dayTotal);
                    total.HourStrings.Add ((DayOfWeek)i, dayTotal.ToString("0.##"));
                }
                total.Total = total.Hours.Sum (row => row.Value);
                total.AdditionalResource = nonNullRows.Sum (row => row.AdditionalResource);
            }

            return total;
        }

        private void OnHoursEdited(DailyOvertimeModel overtime, double newValue)
        {
            List<DailyOvertimeModel> rows = Rows;
            rows.RemoveAt (0);
            DailyOvertimeModel total = CreateTotalRow (rows);
            rows.Insert (0, total);
            Rows = rows;
            IsModified = true;
        }

        private void CheckModified(Action<int> saveCallback)
        {
            if ((IsModified))
            {
                m_alertBox.ShowOptions (Localiser.Get("schedule_modified"), Localiser.Get("schedule_changes_message"), 
                    new List<string> () { Localiser.Get("save_changes_message"), Localiser.Get("discard_changes_message") }, 
                    Localiser.Get("continue_editing_message"), saveCallback );
            }
            else
            {
                saveCallback(2);
            }
        }

        public void OnEdit()
        {
            SwitchEditMode();
        }

        private void SwitchEditMode()
        {
            if (IsModified)
            {
                CheckModified ((i) =>
                    {
                        if (i > 0)
                        { 
                            bool shouldSave = i == 1;
                            m_saveDate = m_schedule.StartDate.Date;

                            if (shouldSave)
                            {
                                TrySave ();
                            }
                            else
                            {
                                DoRefresh();
                            }
                        }
                    });
            }
            else
            {
                IsModified = true;
            }
        }
            
        public void OnItemSelected(DailyOvertimeModel item)
        {
            int index = Rows.IndexOf (item);
            SelectedRowIndex = index;

            ShowEditDialogueForCell (item);
        }

        public void ShowEditDialogueForCell(DailyOvertimeModel data)
        {
            //Crappy hack - Take the text value (as we're secretly round) but this means remove the %
            EditingCell = data;
            EditingDay = data.SelectedDay;
            m_customAlertBox.ShowMyHoursInput
            (
                data.DepartmentName + " Hours",
                data.SelectedDay.ToString(), 
                data.Hours[EditingDay].ToString(), 
                (x, s) => HandleDataValueChanged (data, EditingDay, x, s)
            );
        }

        public void HandleDataValueChanged(DailyOvertimeModel data, DayOfWeek day, AlertReturn buttonSelected, string valueReturned)
        {
            EditingCell = null;

            if (buttonSelected == AlertReturn.Positive)
            {
                //Validate the input
                string error = string.Empty;

                double val = 0;
                if (string.IsNullOrEmpty (valueReturned))
                {
                    error = Localiser.Get("my_hours_input_invalid");
                }
                else if (!double.TryParse (valueReturned, out val))
                {
                    error = string.Format (Localiser.Get("my_hours_input_invalid_value"), valueReturned);
                }

                if (!string.IsNullOrEmpty (error))
                {
                    m_alertBox.Show (Localiser.Get("my_hours_input_invalid_header"), error, Localiser.Get("my_hours_input_invalid_retry"), Localiser.Get("my_hours_input_invalid_cancel"), (int i) =>
                        {
                            if (i == 1)
                            {
                                ShowEditDialogueForCell (data);
                            }
                        });
                }
                else
                {
                    var converter = new NearestQuarterConverter ();
                    var convertedValue = converter.Convert(val, null, null, System.Globalization.CultureInfo.CurrentCulture);
                    data.EditedDay(data.SelectedDay, convertedValue.ToString ());
                    data.UpdateHours ();
                    OnHoursEdited (data, val);
                }
            }
        }
    }
}