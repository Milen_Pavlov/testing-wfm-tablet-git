﻿Hi,

There's a post build step in iOS and Android which converts the strings xml (in android's case it just copies it) into the correct format and puts it in the correct folder.

You only need to modify this file in order to make changes to the WFM localisation.