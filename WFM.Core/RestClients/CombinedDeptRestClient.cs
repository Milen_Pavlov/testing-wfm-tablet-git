using System;
using Consortium.Client.Core;
using System.Diagnostics;

namespace WFM.Core.RestClients
{
    [ConsortiumService]
    public class CombinedDeptRestClient : RestClient
    {
        private readonly SettingsService m_settings;

        Environment configuredForEnvironment = null;

        public override Uri BaseUri
        {
            get
            {
                Environment currentEnvironment = m_settings.Current.Env;

                if (configuredForEnvironment != currentEnvironment)
                {
                    configuredForEnvironment = currentEnvironment;

                    // CHECK environment
                    if (String.IsNullOrWhiteSpace (currentEnvironment.CombinedDeptAddress))
                    {
                        Debug.WriteLine ("### FATAL ERROR: Missing labour demand service address, Labour Demand systems will not work!! Please try clearing the App cache by uninstalling ###");
                    }
                    else
                    {
                        base.BaseUri = new Uri (currentEnvironment.CombinedDeptAddress);
                        SetAuthentication (currentEnvironment.CombinedDeptUsername, currentEnvironment.CombinedDeptPassword);
                    }
                }


                return base.BaseUri;
            }
            set
            {
                //nop
            }
        }

        public CombinedDeptRestClient(SettingsService settings, IESSConfigurationService configurationService)
        {
            m_settings = settings;

            TimeoutMs = configurationService.RestClientTimeoutSecs * 1000;
        }
    }
}
