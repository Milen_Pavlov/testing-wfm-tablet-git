using System;
using Consortium.Client.Core;
using System.Diagnostics;

namespace WFM.Core.RestClients
{
    [ConsortiumService]
    public class MyHoursRestClient : RestClient
    {
        private readonly SettingsService m_settings;

        Environment configuredForEnvironment = null;

        public override Uri BaseUri
        {
            get
            {
                Environment currentEnvironment = m_settings.Current.Env;

                if (configuredForEnvironment != currentEnvironment)
                {
                    configuredForEnvironment = currentEnvironment;

                    // CHECK environment
                    if (String.IsNullOrWhiteSpace (currentEnvironment.MyHoursAddress))
                    {
                        Debug.WriteLine ("### FATAL ERROR: Missing consortium service address, My Hours system will not work!! Please try clearing the App cache by uninstalling ###");
                    }
                    else
                    {
                        base.BaseUri = new Uri (currentEnvironment.MyHoursAddress);
                        SetAuthentication (currentEnvironment.MyHoursUsername, currentEnvironment.MyHoursPassword);
                    }
                }


                return base.BaseUri;
            }
            set
            {
                //nop
            }
        }

        public MyHoursRestClient(SettingsService settings, IESSConfigurationService configurationService)
        {
            m_settings = settings;
            TimeoutMs = configurationService.RestClientTimeoutSecs * 1000;
        }
    }
}
