﻿using System;
using System.Diagnostics;
using Consortium.Client.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class LabourDemandRestClient : RestClient
    {
        private readonly SettingsService m_settings;

        private  Environment configuredForEnvironment = null;

        public LabourDemandRestClient (SettingsService settings, IESSConfigurationService configurationService)
        {
            m_settings = settings;

            TimeoutMs = configurationService.RestClientTimeoutSecs * 1000;
        }

        public override Uri BaseUri {
            get 
            {
                var currentEnvironment = m_settings.Current.Env;

                if(configuredForEnvironment != currentEnvironment)
                {
                    configuredForEnvironment = currentEnvironment;

                    if(string.IsNullOrEmpty(currentEnvironment.LabourDemandAddress.Trim()))
                    {
                        Debug.WriteLine ("### FATAL ERROR: Missing labour demand service address, Labour Demand systems will not work!! Please try clearing the App cache by uninstalling ###");
                    }
                    else
                    {
                        base.BaseUri = new Uri(currentEnvironment.LabourDemandAddress);
                        SetAuthentication(currentEnvironment.CombinedDeptUsername, currentEnvironment.CombinedDeptPassword);
                    }
                }

                return base.BaseUri;
            }
        }
    }
}

