using System;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Core.RestClients
{
    [ConsortiumService]
    public class ConsortiumRestClient : RestClient
    {
        private readonly RestClient m_jdaRestClient;

        public string JdaSessionId 
        { 
            get
            {
				try
				{
					// Remove port
					Uri uri =  null;


					if(m_jdaRestClient.BaseUri.IsDefaultPort)
					{
						uri = m_jdaRestClient.BaseUri;
					}
					else
					{
						uri = new Uri(m_jdaRestClient.BaseUri.AbsoluteUri.Replace(":" + m_jdaRestClient.BaseUri.Port, ""));
					}

                    string environmentUri = uri.ToString();

                    //Retrieve the cookie from the correct path
                    if(!environmentUri.EndsWith("/"))
                    {
                        environmentUri += "/";
                    }

                    var cookie = m_jdaRestClient.Cookies.GetCookies(new Uri(environmentUri))[SessionData.REFS_SESSION_COOKIE];

					return cookie == null ? null : cookie.Value;
				}
				catch(Exception ex)
				{
					Mvx.Exception(ex, "Failed to get cookie");
				}
				return null;
            }
        }

        public ConsortiumRestClient(RestClient client, IESSConfigurationService essConfigurationService)
        {
            client.TimeoutMs = essConfigurationService.RestClientTimeoutSecs * 1000;
            this.TimeoutMs = essConfigurationService.RestClientTimeoutSecs * 1000;
            m_jdaRestClient = client;

            DeserialiseOnError = true;
        }
    }
}
