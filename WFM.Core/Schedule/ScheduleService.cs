using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Siesta;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;

namespace WFM.Core
{
    [ConsortiumService]
    public class ScheduleService : ManagedRestService
    {
        public enum UpdateState
        {
            View,
            Edit,
            Loading,
            Saving
        }

        private readonly SessionData m_data;
        private readonly RestClient m_client;
        private readonly IMvxMessenger m_messenger;
        protected MvxSubscriptionToken m_signInToken;
        protected MvxSubscriptionToken m_signOutToken;
        protected MvxSubscriptionToken m_shiftRequestToken;
        protected MvxSubscriptionToken m_orgEndToken;
        private readonly IAlertBox m_alert;
        private readonly IESSConfigurationService m_settings;
        private readonly ScheduleUndoService m_undoService;
        private readonly IStringService m_localiser;
        private readonly IRaygunService m_raygunService;

        private Dictionary<int, JDAEmployeeInfo> m_employeeInfoLookup;
        private JDAScheduleData m_rawSchedule;
        private JDAScheduleAccuracyData m_rawAccuracy;

        //Filter lists
        private List<JDANameAndID> m_departments;
        private List<JDANameAndID> m_workgroups;
        private List<JDAEmployeeAttribute> m_attributes;
        //Added with Walmart custom service calls in JDA
        private List<JDACustomJob> m_customJobs;
        private List<JDACustomRole> m_customRoles;

        public Dictionary<int, string> JobNameLookup;
        public UpdateState State { get; private set; }
        public int? DayIndex { get; private set; }
        public int? StartDay { get; private set; }
        public DateTime StartDate { get; private set; }
        public FilterGroup CurrentFilter { get; set; }
        public bool FilterNeedsResetOnRefresh { get; set; }
        public AccuracyData Accuracy { get; private set; }
        public ScheduleData Schedule { get; private set; }
        public FilterData Filters { get; private set; }
        public int SortIndex { get; private set; }
        public bool SortNameAscending { get; private set; }
        public bool SortHourAscending { get; private set; }
        public bool SortEarliestStartAscending { get; private set; }
        public decimal SalesForecast { get; private set; }
        public const int c_defaultShiftLength = 4;

        private bool m_isSigningOut = false;

        // Original state variables
        public bool FirstSuccessfulLoad;
        private DateTime? m_origStartDate;
        private FilterGroup m_origCurrentFilter;

        private CancellationTokenSource m_tokenSource = new CancellationTokenSource();

        private IAccountingHelper m_accountingHelper;
        public IAccountingHelper AccountingHelper
        {
            get { return m_accountingHelper; }
            private set { m_accountingHelper = value; }
        }

        public IAccountingConfiguration AccountingConfig
        {
            get
            {
                return new AccountingConfiguration()
                {
                    PeriodFirstDay = m_settings.PeriodFirstDay,
                    PeriodFirstMonth = m_settings.PeriodFirstMonth,
                    StartDay =  (DayOfWeek)m_settings.StartDay,
                };
            }
        }

        public bool IsBusy
        { 
            get { return State == UpdateState.Loading || State == UpdateState.Saving; }
        }

        public ScheduleService(
            SessionData data, 
            RestClient client, 
            IMvxMessenger messenger, 
            IAlertBox alert, 
            IESSConfigurationService settings, 
            ScheduleUndoService undoService,
            IMobileDevice mobileDevice,
            IStringService localiser,
            IRaygunService raygunService)
        {
            m_messenger = messenger;
            m_localiser = localiser;
            m_raygunService = raygunService;
            m_signInToken = m_messenger.SubscribeOnMainThread<SignInMessage>(OnSignIn);

            m_signOutToken = m_messenger.SubscribeOnMainThread<SignOutMessage>(OnSignOut);
            m_shiftRequestToken = m_messenger.SubscribeOnMainThread<ShiftEditMessage>(OnShiftRequest);
            m_orgEndToken = m_messenger.SubscribeOnMainThread<SetOrgEndMessage> (OnSiteChanged);

            m_data = data;
            m_client = client;
            m_alert = alert;
            m_employeeInfoLookup = new Dictionary<int, JDAEmployeeInfo>();
            JobNameLookup = new Dictionary<int, string>();
            m_rawSchedule = null;
            m_settings = settings;
            m_undoService = undoService;

            AccountingHelper = new AccountingHelper (AccountingConfig);

            State = UpdateState.View;
            DayIndex = null;
            StartDay = null;
            StartDate = m_data.Site.EffectiveStartOfWeek.Value;
            SortIndex = ScheduleData.c_sortName;
            SortNameAscending = true;
            SortHourAscending = true;
            FirstSuccessfulLoad = false;
            m_isSigningOut = false;
        }

        public void ClearData()
        {            
            State = UpdateState.View;
            m_departments = null;
            m_workgroups = null;
            m_rawSchedule = null;
            m_rawAccuracy = null;
            m_attributes = null;
            m_customJobs = null;
            m_customRoles = null;

            Accuracy = null;
            SalesForecast = 0;

            m_employeeInfoLookup.Clear();
            JobNameLookup.Clear();    

            // Reset Schedule
            StartDay = null;
            SetEdit(false);
            m_undoService.ClearHistory();
            Schedule = null;
            ResetSortIndex();

            // Create filters
            Filters = null;
            FilterNeedsResetOnRefresh = false;

            // Reset successful load flag
            FirstSuccessfulLoad = false;
			CurrentFilter = null;
			StartDate = m_data.Site.EffectiveStartOfWeek.Value;
        }

        private void OnSignIn(SignInMessage message)
        {            
            m_tokenSource.Cancel();

            m_isSigningOut = false;
            ClearData();
        }

        private void OnSignOut(SignOutMessage message)
        {
            m_isSigningOut = true;

            m_tokenSource.Cancel();

            //m_alert.Dismiss(m_currentAlertToken, 0, false);

            ClearData();
            DayIndex = null;
        }

        public void SetDay(int index)
        {
            if (index >= 0 && index <= 7)
            {
                if (!DayIndex.HasValue || DayIndex.Value != index)
                {
                    DayIndex = index;

                    if (Schedule != null)
                    {
                        Schedule.SetDay(DayIndex);

                        if(Accuracy!=null)
                            Accuracy.SetDay(DayIndex);

                        m_messenger.Publish(new ScheduleUpdateMessage(this)
                            {
                                UpdateSchedule = true,
                                UpdateTotals = true,
                                UpdateHeaders = true
                            });

                        m_messenger.Publish (new GraphUpdateMessage (this));
                    }
                }
                ResetSortIndex();
            }
        }

        public void SetWeek()
        {
            if (DayIndex.HasValue)
            {
                DayIndex = null;

                if (Schedule != null)
                {
                    Schedule.SetDay(DayIndex);
                    Accuracy.SetDay(DayIndex);

                    m_messenger.Publish(new ScheduleUpdateMessage(this)
                        {
                            UpdateSchedule = true,
                            UpdateTotals = true,
                            UpdateHeaders = true
                        });

                    m_messenger.Publish (new GraphUpdateMessage (this));
                }
            }
        }

        public void SetEdit(bool canEdit)
        {
            if (Schedule != null)
            {
                State = canEdit ? UpdateState.Edit : UpdateState.View;

                Schedule.SetEdit(canEdit);
                Mvx.Resolve<SpareShiftService> ().SetEdit (canEdit);

                m_messenger.Publish(new ScheduleUpdateMessage(this)
                    {
                        UpdateSchedule = true
                    });
            }
        }

        public void SortEarliestStart(int? headerIndex)
        {
            SortEarliestStart(ScheduleData.c_sortEarliestStart, headerIndex);
        }

        public void SortEarliestStart(int index, int? headerIndex)
        {        
            if (Schedule != null)
            {
                if (headerIndex != null)
                {
                    SortEarliestStartAscending = SortIndex == headerIndex ? !SortEarliestStartAscending : SortEarliestStartAscending;
                    Schedule.SortEarliestStart(headerIndex, SortEarliestStartAscending);
                    SortIndex = (int)headerIndex;
                }
                else
                {
                    SortEarliestStartAscending = SortIndex == index ? !SortEarliestStartAscending : SortEarliestStartAscending;
                    Schedule.SortEarliestStart(DayIndex, SortEarliestStartAscending);
                    SortIndex = index;
                }

                m_messenger.Publish(new ScheduleUpdateMessage(this)
                    {
                        UpdateSchedule = true,
                        UpdateHeaders = true
                    });

                m_messenger.Publish (new GraphUpdateMessage (this));
            }
        }

        public void SortName()
        {
            SortName(ScheduleData.c_sortName);
        }

        public void SortName(int index)
        {        
            if (Schedule != null)
            {
                SortNameAscending = SortIndex == index ? !SortNameAscending : SortNameAscending;
                SortIndex = index;
                Schedule.SortNames(DayIndex, SortNameAscending);

                m_messenger.Publish(new ScheduleUpdateMessage(this)
                    {
                        UpdateSchedule = true,
                        UpdateHeaders = true
                    });
            }
        }

        public void SortHour()
        {
            SortHour(ScheduleData.c_sortHour);
        }

        public void SortHour(int index)
        {        
            if (Schedule != null)
            {
                SortHourAscending = SortIndex == ScheduleData.c_sortHour ? !SortHourAscending : SortHourAscending;
                SortIndex = ScheduleData.c_sortHour;
                Schedule.SortHours(DayIndex, SortHourAscending);

                m_messenger.Publish(new ScheduleUpdateMessage(this)
                    {
                        UpdateSchedule = true,
                        UpdateHeaders = true
                    });
            }
        }

        public void ResetSortIndex()
        {
            SortIndex = ScheduleData.c_unsorted;
        }

        public async Task MoveToWeek(bool trySave, DateTime startDay)
        {
            m_tokenSource.Cancel();
            FirstSuccessfulLoad = false;
            await SafeWriteReadAsync(trySave, startDay, CurrentFilter, 500);
        }

        public async Task MoveNext(bool trySave)
        {
            m_tokenSource.Cancel();
            FirstSuccessfulLoad = false;
            await SafeWriteReadAsync(trySave, StartDate.AddDays(7), CurrentFilter, 500);
        }

        public async Task MovePrev(bool trySave)
        {
            m_tokenSource.Cancel();
            FirstSuccessfulLoad = false;
            await SafeWriteReadAsync(trySave, StartDate.AddDays(-7), CurrentFilter, 500);
        }

        public async Task Refresh(bool trySave)
        {
            m_tokenSource.Cancel();
            await SafeWriteReadAsync(trySave, StartDate, CurrentFilter, 500);
        }

        #region Read Operations

        private void StorePreviousStateBeforeRead()
        {
            m_origStartDate = StartDate;
			m_origCurrentFilter = CurrentFilter != null ? CurrentFilter.Clone() : null;
        }

        private void RevertReadToOriginalState()
        {
            if(m_origStartDate != null) 
            {
                StartDate = (DateTime) m_origStartDate;
            }

            CurrentFilter = m_origCurrentFilter;

            m_origStartDate = null;
            m_origCurrentFilter = null;
        }

        public async Task SafeReadAsync(DateTime time, FilterGroup filter, int delayMs = 0)
        {
            if (m_isSigningOut)
                return;
            
            State = UpdateState.Loading;

            StartDate = time;

            //CUstom services don't support custom timespans for now so lets just make sure they never happen
            //the UI should stop it anyway
            if (m_settings.UseCustomScheduleRequest) 
                StartDate = StartDate.Date;

            m_messenger.Publish(new ScheduleReadBeginMessage(this));

            int timeoutMillis = m_settings.ScheduleTimeoutSecs * 1000;

            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<ScheduleReadEndMessage>(() =>
                {
                    return ReadAsync(time, filter,token, delayMs).Result;
                });
            };

            StorePreviousStateBeforeRead();

            m_tokenSource = new CancellationTokenSource();
            ManagedRestResponse<ScheduleReadEndMessage> response = await PerformOperation<ScheduleReadEndMessage>(timeoutMillis, taskGenerator,m_tokenSource.Token);

            if (m_isSigningOut)
                return;

            State = UpdateState.View;
            if (!response.TimeOut && !response.Cancelled)
            {
                m_messenger.Publish(response.ResponseObject);
            }
            else
            {
                RevertReadToOriginalState();

                response.ResponseObject = new ScheduleReadEndMessage(this, false, "Failed");
                response.ResponseObject.NoData = !FirstSuccessfulLoad;

                m_messenger.Publish(response.ResponseObject);
            }
        }

        public async Task<ScheduleReadEndMessage> ReadAsync(DateTime time, FilterGroup filter, CancellationToken token, int msDelay)
        {
            ScheduleReadEndMessage message = new ScheduleReadEndMessage(this, false, null);

            var deptByAuthRequest = new DeptByAuthJobsRequest()
                {
                    siteID = m_data.Site.SiteID
                };

            var workgroupRequest = new WorkgroupBySiteRequest()
                {
                    siteID = m_data.Site.SiteID
                };

            var employeeAttrRequest = new EmployeeAttributesRequest()
                {
                    siteID = m_data.Site.SiteID
                };

            var customJobsRequest = new GetAllJobsForSiteCustomRequest ()
                {
                    siteID = m_data.Site.SiteID
                };

            var customRolesRequest = new GetRolesForSiteCustomRequest ()
                {
                    siteID = m_data.Site.SiteID
                };

            var scheduleRequest = new ScheduleDataRequest()
                {
                    siteID = m_data.Site.SiteID,
                    startDay = StartDate,
                    filter = filter != null ? filter.Type : string.Empty,
                    filterIDs = filter != null ? filter.GetSelectedList() : new int[] { },
                    budgetID = 1000012,
                    metricID = 1000047
                };

            var scheduleCustomRequest = new ScheduleDataCustomRequest()
                {
                    siteID = m_data.Site.SiteID,
                    startDay = StartDate,
                    filter = filter != null ? filter.Type : string.Empty,
                    filterIDs = filter != null ? filter.GetSelectedList() : new int[] { },
                    budgetID = 1000012,
                    metricID = 1000047
                };

            var accuracyRequest = new ScheduleAccuracyRequest()
                {
                    buID = m_data.Site.SiteID,
                    start = StartDate,
                    end = StartDate.AddDays(7),
                    filters = filter != null ? new JDAScheduleFilter() { FilterType = filter.Type, FilterIDs = filter.GetSelectedList() } : null
                };

            var accuracyCustomRequest = new ScheduleAccuracyCustomRequest () {
                buID = m_data.Site.SiteID,
                start = StartDate,
                end = StartDate.AddDays (7),
                filters = filter != null ? new JDAScheduleFilter () { FilterType = filter.Type, FilterIDs = filter.GetSelectedList () } : null
                };


            var salesForecastRequest = new SalesForecastRequest()
                {
                    siteID = m_data.Site.SiteID,
                    forecastGroupID = 1000101,
                    metricID = 1000104,
                    start = StartDate.Date,
                    end = StartDate.AddDays(6).Date, // Was set to 6 in ObjC client
                };
                    
            //This acts as a buffer, allowing us to cancel the operations quickly before transmission. This means JDA doesn't get overwhelmed by multiple requests that don't expect a response.
            //Typical use case is when cycling through the weeks.
            await Task.Delay(msDelay);

            Task<RestReply<ScheduleDataReply>> schedule;
            Task<RestReply<ScheduleAccuracyReply>> accuracy;

            if (!m_settings.UseCustomScheduleRequest) 
            {
                schedule = m_client.Post<ScheduleDataReply> (scheduleRequest, token);
                accuracy = m_client.Post<ScheduleAccuracyReply> (accuracyRequest, token);
            }
            else 
            {
                schedule = m_client.Post<ScheduleDataReply> (scheduleCustomRequest, token);
                accuracy = m_client.Post<ScheduleAccuracyReply> (accuracyCustomRequest, token);
            }

            var salesForecast = m_client.Post<SalesForecastReply>(salesForecastRequest, token);

            //Annoyingly, these can't just all be put in a single list, no shared base unless we go right down to just Task, but then we can't get the errors out
            //RestReply can't be a base
            Task<RestReply<DeptByAuthJobsReply>> deptByAuth = null;
            Task<RestReply<WorkgroupBySiteReply>> workgroup = null;
            Task<RestReply<EmployeeAttributesReply>> employeeAttr = null;
            Task<RestReply<GetAllJobsForSiteCustomResponse>> customJobs = null;
            Task<RestReply<GetRolesForSiteCustomResponse>> customRoles = null;

            if(!m_settings.CanFilterByAttribute.HasValue || m_settings.CanFilterByAttribute.Value)
                employeeAttr = m_client.Post<EmployeeAttributesReply>(employeeAttrRequest, token);
            if(!m_settings.CanFilterByDepartment.HasValue || m_settings.CanFilterByDepartment.Value)
                deptByAuth = m_client.Post<DeptByAuthJobsReply>(deptByAuthRequest, token);
            if(!m_settings.CanFilterByWorkgroup.HasValue || m_settings.CanFilterByWorkgroup.Value)
                workgroup = m_client.Post<WorkgroupBySiteReply>(workgroupRequest, token);
            if(!m_settings.CanFilterByJobCustom.HasValue || m_settings.CanFilterByJobCustom.Value)
                customJobs = m_client.Post<GetAllJobsForSiteCustomResponse> (customJobsRequest, token);
            if(!m_settings.CanFilterByRoleCustom.HasValue || m_settings.CanFilterByRoleCustom.Value)
                customRoles = m_client.Post<GetRolesForSiteCustomResponse> (customRolesRequest, token);

            await schedule.ConfigureAwait(false);
            await accuracy.ConfigureAwait(false);
            await salesForecast.ConfigureAwait(false);

            if(employeeAttr != null)
                await employeeAttr.ConfigureAwait (false);
            if(deptByAuth != null)
                await deptByAuth.ConfigureAwait (false);
            if(workgroup != null)
                await workgroup.ConfigureAwait (false);
            if(customJobs != null)
                await customJobs.ConfigureAwait (false);
            if(customRoles != null)
                await customRoles.ConfigureAwait (false);

            token.ThrowIfCancellationRequested();
           
            // Check for errors (argh, can't seem to build a single list of RestReplys etc due to the generics
            List<string> messages = new List<string> ();
            List<bool> errors = new List<bool> ();
            List<bool> cancelled = new List<bool> ();
            List<int> codes = new List<int> ();

            errors.Add (!schedule.Result.Success);
            cancelled.Add (schedule.Result.IsCancelled);
            messages.Add (schedule.Result.Message);
            codes.Add (schedule.Result.Code);

            errors.Add (!salesForecast.Result.Success);
            cancelled.Add (salesForecast.Result.IsCancelled);
            messages.Add (salesForecast.Result.Message);
            codes.Add (salesForecast.Result.Code);

            errors.Add (!accuracy.Result.Success);
            cancelled.Add (accuracy.Result.IsCancelled);
            messages.Add (accuracy.Result.Message);
            codes.Add (accuracy.Result.Code);

            // Check for errors
            if (errors.Any(x => x))
            {
                // If any of the operations were cancelled then return without warning
                if (cancelled.Any(x => x))
                {
                    //CancelledBySiesta = true;
                    message.Success = false;
                    message.Message = "Cancelled";
                }
                else
                {
                    // Assuming unhandled content is a session timeout but may need to refine this later
                    if (codes.Any(x => x < 0))
                    {
                        if (codes.All(x => x != (int)RestError.Cancelled))
                            CancelledBySiesta = true;
                        else
                        {
                            message.SessionTimeout = true;
                        }

                        message.NoData = true;

                        message.Message = "Session Timeout";
                    }
                    else
                    {
                        // Pick out the 1st error
                        for (int i = 0; i < errors.Count; i++)
                        {
                            if (errors[i])
                            {
                                message.Message = messages[i];
                                break;
                            }
                        }

                        Mvx.Trace("Schedule Load: Error {0}", time.ToString("G"));
                        message.Success = false;
                    }
                }
            }
            else
            {
                Mvx.Trace("Schedule Load: Complete {0}", time.ToString("G"));
                message.Success = true;

                //Filter stuff
                m_departments = (deptByAuth != null && deptByAuth.Result.Success && deptByAuth.Result.Object != null) ? deptByAuth.Result.Object.data : null;
                m_workgroups = (workgroup != null && workgroup.Result.Success && workgroup.Result.Object != null) ? workgroup.Result.Object.data : null;
                m_attributes = (employeeAttr != null && employeeAttr.Result.Success && employeeAttr.Result.Object != null) ? employeeAttr.Result.Object.data : null;
                m_customJobs = (customJobs != null && customJobs.Result.Success && customJobs.Result.Object != null) ? customJobs.Result.Object.data : null;
                m_customRoles = (customRoles != null && customRoles.Result.Success && customRoles.Result.Object != null) ? customRoles.Result.Object.data : null;

                m_rawSchedule = schedule.Result.Object.data;
                m_rawAccuracy = accuracy.Result.Object.data;

				CurrentFilter = filter;

                Accuracy = new AccuracyData(m_rawAccuracy, StartDate, CurrentFilter);
                Accuracy.SetDay(DayIndex);

                SalesForecast = salesForecast.Result.Object.data;
                StartDate = schedule.Result.Object.data.DefaultStartDate;

                m_employeeInfoLookup.Clear();
                JobNameLookup.Clear();

                if (m_rawSchedule.EmployeeInfo != null)
                {
                    foreach (var e in m_rawSchedule.EmployeeInfo)
                        m_employeeInfoLookup.Add(e.EmployeeID, e);
                }

                if (m_rawSchedule.JobRoles != null)
                {
                    foreach (var j in m_rawSchedule.JobRoles)
                    {
                        JobNameLookup.Add(j.JobID, j.JobName);
                    }
                }
                // Create Schedule
                StartDay = m_rawSchedule.BUSettings.FirstDayOfWeekID;

                Schedule = new ScheduleData(m_rawSchedule, m_employeeInfoLookup, CurrentFilter);
                Schedule.SortNames(DayIndex, SortNameAscending);
                Schedule.SetDay(DayIndex);
                ResetSortIndex();
                // Create filters
                Filters = new FilterData(m_rawSchedule, m_workgroups, m_departments, m_attributes, m_customJobs, m_customRoles, m_data, m_settings);
                FirstSuccessfulLoad = true;
            }

            return message;
        }

        #endregion

        #region Write Operations

        public void Revert()
        {
            if (State != UpdateState.Edit)
                return;

            Schedule = new ScheduleData(m_rawSchedule, m_employeeInfoLookup, CurrentFilter);
            Schedule.SortNames(DayIndex, SortNameAscending);
            Schedule.SetDay(DayIndex);

            Accuracy = new AccuracyData(m_rawAccuracy, StartDate, CurrentFilter);
            Accuracy.SetDay(DayIndex);

            m_messenger.Publish(new ScheduleUpdateMessage(this)
                {
                    UpdateSchedule = true,
                    UpdateTotals = true,
                    UpdateHeaders = true, 
                    RevertChanges = true
                });

            m_messenger.Publish (new GraphUpdateMessage (this));
        }

        public async Task SafeWriteReadAsync(bool trySave, DateTime time, FilterGroup filter, int msBufferBeforeOperation = 0)
        {
            await SafeWriteAsync(trySave);

            await SafeReadAsync(time, filter,msBufferBeforeOperation);
        }

        public async Task SafeWriteAsync(bool trySave)
        {
            if (m_isSigningOut)
                return;

            if (!trySave)
                return;

            State = UpdateState.Saving;

            m_messenger.Publish(new ScheduleWriteBeginMessage(this));

            TaskGenerator taskGenerator = (token) =>
            {
                return new Task<ScheduleWriteEndMessage>(() =>
                {
                    return WriteAsync(token).Result;
                });
            };

            m_tokenSource = new CancellationTokenSource();
            var response = await PerformOperation<ScheduleWriteEndMessage>(m_client.TimeoutMs, taskGenerator, m_tokenSource.Token);

            if (m_isSigningOut)
                return;

            State = UpdateState.View;

            if (response.Cancelled || response.TimeOut)
            {
                m_undoService.ClearHistory();
                ClearData();

                ScheduleWriteEndMessage message = new ScheduleWriteEndMessage(this, false, "Failed");

                message.ResponseReceived = false;

                m_messenger.Publish(message);
            }
            else if (response.ResponseObject.Success)
            {
                m_undoService.ClearHistory();
                ClearData();

                m_messenger.Publish(response.ResponseObject);
            }
            else
            {   
                //Got a response, but is a rejection, so revert
                Revert();

                m_messenger.Publish(response.ResponseObject);
            }
        }
            
        private bool TryGenerateWriteRequest(ref ProcessChangeCommandsRequest request)
        {
            ProcessChangeCommandsRequest generated;

            bool saving = Schedule != null && Schedule.IsModified;

            if (saving && Schedule != null)
            {
                generated = new ScheduleChange().Create(Schedule, m_data.Site.SiteID);

                if (generated.jsChangeCommands.Length > 0)
                {
                    request = generated;
                    return true;
                }
            }

            return false;
        }

        public async Task<ScheduleWriteEndMessage> WriteAsync(CancellationToken token)
        {
            ScheduleWriteEndMessage message = new ScheduleWriteEndMessage(this, false, null);

            ProcessChangeCommandsRequest request = null;

            // Do we need to save first ?
            if (TryGenerateWriteRequest(ref request))
            {
                var response = await m_client.Post<ProcessChangeCommandsReply>(request,token);
                token.ThrowIfCancellationRequested();

                if (response.Success)
                {
                    if (response.Object != null && response.Object.data != null)
                    {
                        message.ResponseReceived = true;

                        if (response.Object.data.ResultCode == JDAProcessingResult.Codes.OK)
                        {
                            //Server accepted change
                            message.Success = true;
                        }
                        else
                        {
                            // Server rejected change
                            // Display error and log to raygun
                            message.Success = false;
                            m_alert.ShowOK(m_localiser.Get("schedule_write_error_header"), m_localiser.Get("schedule_write_error_body"));

                            //Create error for raygun
                            Dictionary<string, object> data = new Dictionary<string, object>();
                            data.Add ("Content", (response.Content != null) ? Encoding.UTF8.GetString(response.Content, 0, response.Content.Length) : null);
                            data.Add ("ChangeCommands", request.jsChangeCommands);
                            data.Add ("Object", response.Object);
                            data.Add ("Message", response.Message);

                            m_raygunService.LogCustomError(new Exception("ScheduleWriteAsyncFailed"), data);
                        }

                        return message;
                    }
                }
            }

            message.Success = false;
            message.ResponseReceived = false;           

            return message;
        }

        #endregion

        private void OnSiteChanged (SetOrgEndMessage message)
        {
            if(m_data.Site!=null && m_data.Site.EffectiveStartOfWeek.HasValue)
            {
                StartDate = m_data.Site.EffectiveStartOfWeek.Value;
                Schedule = null;
            }
                
        }

        private void OnShiftRequest(ShiftEditMessage message)
        {
            if (Schedule == null)
            {
                Mvx.Error("Cannot perform shift request, no schedule data");
                return;
            }

            DateTime? shiftStart = message.Start;

            //Create Default shift is a bit weird, it sets End to the Middle(click) and Start to the start of the day and then recalcs it
            if (message.Op == ShiftEditMessage.Operation.CreateDefaultShift) 
            {
                //This is because the shift start for a default shift is actually the midpoint between the start and end of the shift.
                shiftStart = shiftStart.Value.AddHours (-(c_defaultShiftLength / 2));
            }

            //TODO: Move these out into the validation phase of the schedule change operations
            if (m_settings.BlockEditingInPast && EditedShiftIsInThePast (shiftStart)) 
            {
                m_alert.ShowOK (
                    m_localiser.Get ("prevent_changes_title"),
                    m_localiser.Get ("prevent_changes_message_for_past_date"));
                return;
            }

            if (IsWithinPreventChangesPeriod (shiftStart))
            {
                m_alert.ShowOK (
                    m_localiser.Get ("prevent_changes_title"),
                    String.Format(m_localiser.Get ("prevent_changes_message"), m_settings.PreventChangesHours));
                return;
            }

            var employee = Schedule.GetEmployee(message.EmployeeID);
            if (employee == null)
            {
                employee = Mvx.Resolve<SpareShiftService> ().GetEmployee (message.EmployeeID);
            }

            if (employee == null)
                return;

            switch (message.Op)
            {
                case ShiftEditMessage.Operation.UnfillFilledShift:
                    PerformUnfillShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.AssignUnfilledShift:
                    PerformFillShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.CreateShift:
                    PerformCreateShiftOperation (message, employee);
                    break;
                case ShiftEditMessage.Operation.CreateDefaultShift:
                    PerformCreateDefaultShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.UpdateShift:
                    PerformUpdateShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.ReplaceShift:
                    PerformReplaceShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.DeleteShift:
                    PerformDeleteShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.DeleteUnsavedShift:
                PerformDeleteUnsavedShiftOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.CreateJob:
                    PerformCreateJobOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.DeleteJob:
                    PerformDeleteJobOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.Undo:
                    PerformUndoOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.ReassignShift:
                    PerformReassignOperation(message, employee);
                    break;
                case ShiftEditMessage.Operation.UndoFilledShift:
                    PerformReverseFillShiftOperation(message, employee);
                    break;
            }

            //Can we edit the schedule, if so just continue and allow further edits
            //otherwise just submit if allowed
            if (!m_settings.CanEditSchedule && message.AllowAutoSubmission)
            {
                //Automatically just trigger the submission of this edit and refresh all
                SafeWriteReadAsync(true, StartDate, CurrentFilter, 500);
            }
        }

        private bool EditedShiftIsInThePast (DateTime? timeToTest)
        {
            if (timeToTest.HasValue == false)
                return false;

            return timeToTest.Value < DateTime.Now;
        }

        private void PerformDeleteUnsavedShiftOperation (ShiftEditMessage message, EmployeeData employee)
        {
            var shiftState = employee.GetShift(message.ShiftID);

            if (message.StoreInUndoStack)
                m_undoService.Store(new ScheduleUndoStackOperation(shiftState.Copy(), message.Op, message.EmployeeID));

            employee.DeleteShift(message.ShiftID, true);

            EmployeeUpdate(employee, !message.StoreInUndoStack);

            SetEdit(true);
        }

        void PerformUpdateShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            var shift = message.UpdateShift;

            bool rollBack = false;

            ShiftData oldShift;

            //The caller can set the previous shift state.
            if (message.PreviousShiftState != null)
            {
                oldShift = message.PreviousShiftState;
            }
            else
            {
                //Or we can get the current state from the employee
                if (message.ShiftID != 0)
                {
                    oldShift = employee.GetShift (message.ShiftID).Copy ();
                }
                else
                {
                    oldShift = employee.GetShift(message.UpdateShift.ShiftID).Copy();
                }
            }

            if (shift == null)
            {
                shift = employee.GetShift(message.ShiftID);
            }

            var originalShiftState = shift.Copy();

            if (message.Start != null && message.End != null)
            {
                ConstrainedResize(shift, message.Start.Value, message.End.Value);
            }
                
            rollBack = !ValidateShift (employee, shift, message.Start, message.End, originalShiftState.Start, originalShiftState.End, message.RecalculateDetails);

            if (rollBack)
            {
                ConstrainedResize(shift, originalShiftState.Start, originalShiftState.End);

                //TODO: Check this doesn't make things go weird
                employee.UpdateShift(shift);

                m_messenger.Publish(new ScheduleUpdateMessage(this)
                    {
                        UpdateSchedule = true
                    });            
            }
            else
            {
                if (message.StoreInUndoStack)
                    m_undoService.Store(new ScheduleUndoStackOperation(oldShift, message.Op, message.EmployeeID));
            }

            EmployeeUpdate(employee, !message.StoreInUndoStack);
        }

        bool ValidateShift(EmployeeData employee, ShiftData shift, DateTime? start, DateTime? end, DateTime originalStart, DateTime originalEnd, bool recalculateDetails)
        {
            bool passValidation = true;

            //string validationMessage = "Shift cannot be placed at this location";

            TimeSpan hoursIncrease = (end.Value - start.Value) - (originalEnd - originalStart);

            var validationResponse = ScheduleChangeValidator.ValidateUpdateShift(employee, start.Value, end.Value, hoursIncrease);

            if(validationResponse.PassesValidation)
            {
                validationResponse = ScheduleChangeValidator.ValidateShiftLength(employee, start.Value, end.Value, shift.ShiftID);
            }

            if (!validationResponse.PassesValidation || !employee.UpdateShift(shift))
            {
                if(validationResponse.PassesValidation)
                    m_currentAlertToken = m_alert.ShowOK("Shift Placement Failed", "Shift cannot be placed at this location");
                else
                    m_currentAlertToken = m_alert.ShowOK("Shift Placement Failed", validationResponse.DisplayMessage);

                //This rolls back the shift to the previous size because constrined resize would have modified it to fit in with the original space
                passValidation = false;
            }

            //Things get a bit weird here because of how the minor rules get enforced. After the shift is created and the breaks regenerated

            if (validationResponse.PassesValidation && passValidation)
            {
                //Recalc happens before second pass validation as minor rules are based on working hours (after breaks)
                if (recalculateDetails)
                {
                    employee.CalculateDetails(shift.ShiftID, m_rawSchedule.ScheduleRules);
                }

                employee.CalculateMetrics(m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                var secondPassValidation = ScheduleChangeValidator.ValidateMinor(employee, shift.Start);

                if (!secondPassValidation.PassesValidation)
                {
                    m_currentAlertToken = m_alert.ShowOK("Shift Placement Failed", secondPassValidation.DisplayMessage);

                    passValidation = false;
                }
            }    

            return passValidation;
        }

        void PerformReplaceShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            var shift = message.UpdateShift;

            shift.RecalculateDetailsForRoles();

            ShiftData originalShift = message.PreviousShiftState != null ? message.PreviousShiftState : employee.GetShift(shift.ShiftID);

            if (!employee.UpdateShift (shift))
            {
                m_currentAlertToken = m_alert.ShowOK ("Shift Placement Failed", "Shift cannot be placed at this location");

                m_messenger.Publish (new ScheduleUpdateMessage (this)
                    {
                        UpdateSchedule = true
                    });

                return;
            }
            else if (ValidateShift (employee, shift, shift.Start, shift.End, originalShift.Start, originalShift.End, message.RecalculateDetails) == false)
            {
                employee.UpdateShift(message.PreviousShiftState);

                m_messenger.Publish (new ScheduleUpdateMessage (this)
                    {
                        UpdateSchedule = true
                    });

                return;
            }
            else
            {
                if (message.StoreInUndoStack)
                {
                    m_undoService.Store(new ScheduleUndoStackOperation(originalShift.Copy(), message.Op, message.EmployeeID));
                }

                SetEdit(true); // Successful update, set edit mode to true
            }

            if (message.RecalculateDetails)
                employee.CalculateDetails(shift.ShiftID, m_rawSchedule.ScheduleRules);

            EmployeeUpdate(employee, !message.StoreInUndoStack);
        }

        void PerformCreateShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            ShiftData shift = null;

            if (message.UpdateShift != null)
            {
                employee.AssignShift(message.UpdateShift);

                shift = message.UpdateShift;
            }
            else
            {
                if (message.Start != null && message.End != null)
                {
                    DateTime shiftStart = (DateTime)message.Start;
                    DateTime shiftEnd = (DateTime)message.End;

                    var validationResponse = ScheduleChangeValidator.ValidateCreateShift(employee, shiftStart, shiftEnd);

                    if (validationResponse.PassesValidation)
                    {
                        shift = employee.AddShift(shiftStart, shiftEnd, m_rawSchedule.JobRoles);

                        if (shift == null)
                        {
                            m_currentAlertToken = m_alert.ShowOK("Add Shift Failed", "No room to create shift at this location");
                        }
                    }
                    else
                    {
                        m_currentAlertToken = m_alert.ShowOK("Add Shift Failed", validationResponse.DisplayMessage);
                    }
                }
                else
                {
                    m_currentAlertToken = m_alert.ShowOK("Add Shift Failed", "Provide valid start and end times for shift.");
                }
            }

            RefreshAfterCreateShift (message, employee, shift);
        }

        public void PerformCreateDefaultShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            int halfDefaultShiftLength = (c_defaultShiftLength / 2);
            ShiftData shift = null;

            if (message.UpdateShift != null)
            {
                employee.AssignShift(message.UpdateShift);

                shift = message.UpdateShift;
            }
            else
            {
                //In this grubby instance start doesn't actually refer to the start of the shift, but the midpoint of the shift
                if (message.Start != null) 
                {
                    var start = message.Start.Value.AddHours (-halfDefaultShiftLength);
                    var end = message.Start.Value.AddHours (halfDefaultShiftLength);
   
                    var validationResponse = ScheduleChangeValidator.ValidateCreateShift(employee, start, end);

                    if (validationResponse.PassesValidation)
                    {
                        if (m_rawSchedule.JobRoles.Count == 0)
                        {
                            m_currentAlertToken = m_alert.ShowOK ("Add Shift Failed", "No job roles for shift");
                        }
                        else
                        {
                            shift = employee.AddDefaultShift (c_defaultShiftLength, start.Date, message.Start.Value, m_rawSchedule.JobRoles);

                            if (shift == null)
                            {
                                m_currentAlertToken = m_alert.ShowOK ("Add Shift Failed", "No room to create shift at this location");
                            }
                        }
                    }
                    else
                    {
                        m_currentAlertToken = m_alert.ShowOK("Add Shift Failed", validationResponse.DisplayMessage);
                    }
                }
                else
                {
                    m_currentAlertToken = m_alert.ShowOK("Add Shift Failed", "Provide valid start and end times for shift.");
                }
            }

            RefreshAfterCreateShift (message, employee, shift);
        }

        void RefreshAfterCreateShift(ShiftEditMessage message, EmployeeData employee, ShiftData shift)
        {
            if (shift == null)
                return;
            
            //Recalc happens before second pass validation as minor rules are based on working hours (after breaks)
            if (message.RecalculateDetails)
            {
                employee.CalculateDetails(shift.ShiftID, m_rawSchedule.ScheduleRules);
            }

            employee.CalculateMetrics(m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

            var secondPassValidation = ScheduleChangeValidator.ValidateMinor(employee, shift.Start);

            if (!secondPassValidation.PassesValidation)
            {
                m_currentAlertToken = m_alert.ShowOK("Shift Placement Failed", secondPassValidation.DisplayMessage);

                employee.DeleteShift(shift.ShiftID);

                employee.CalculateMetrics(m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                return;        
            }

            if (message.StoreInUndoStack)
                m_undoService.Store(new ScheduleUndoStackOperation(shift.Copy(), message.Op, message.EmployeeID));

            if (message.RecalculateDetails)
                employee.CalculateDetails(shift.ShiftID, m_rawSchedule.ScheduleRules);

            EmployeeUpdate(employee, !message.StoreInUndoStack);
        }

        void PerformDeleteShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            var shiftState = employee.GetShift(message.ShiftID);

            if (message.StoreInUndoStack)
                m_undoService.Store(new ScheduleUndoStackOperation(shiftState.Copy(), message.Op, message.EmployeeID, message.Start.GetValueOrDefault(), message.End.GetValueOrDefault()));

            employee.DeleteShift(message.ShiftID);

            EmployeeUpdate(employee, !message.StoreInUndoStack);

            SetEdit(true);
        }

        void PerformCreateJobOperation(ShiftEditMessage message, EmployeeData employee)
        {
            var shift = employee.GetShift(message.ShiftID);

            var job = shift.AddDefaultJob(4);

            if (job == null)
            {
                m_currentAlertToken = m_alert.ShowOK("Add Job Failed", "No room to create job at this location");
                return;
            }

            if (message.StoreInUndoStack)
                m_undoService.Store(new ScheduleUndoStackOperation(shift.ShiftID, message.Op, message.EmployeeID));

            employee.CalculateDetails(shift.ShiftID, m_rawSchedule.ScheduleRules);

            EmployeeUpdate(employee, !message.StoreInUndoStack);
        }

        void PerformDeleteJobOperation(ShiftEditMessage message, EmployeeData employee)
        {
            var shift = employee.GetShift(message.ShiftID);

            if (shift != null)
            {
                m_undoService.Store(new ScheduleUndoStackOperation(shift.Copy(), message.Op, message.EmployeeID));

                if (shift.Jobs.Count <= 1)
                {
                    employee.DeleteShift(message.ShiftID);
                }
                else
                {
                    shift.DeleteJob(message.JobID);
                }

                EmployeeUpdate(employee, false);
            }
        }

        void PerformUndoOperation(ShiftEditMessage message, EmployeeData employee)
        {
            // Find the shift in the raw schedule
            // TODO: Switch to using a saved schedule rather than raw schedule
            var shift = m_rawSchedule.GetShift(message.EmployeeID, message.ShiftID);

            if (shift != null)
            {
                var shiftData = Mapper.Map<JDAShift, ShiftData>(shift);

                employee.UpdateShift(shiftData);
                EmployeeUpdate(employee, false);
            }
        }

        void PerformReassignOperation(ShiftEditMessage message, EmployeeData employee)
        {
            if(IsNewlyCreatedShift(message))
            {
                m_currentAlertToken = m_alert.ShowOK (m_localiser.Get("swap_shift_failed"), m_localiser.Get("cannot_swap_newly_created_shift"));
                return;    
            }

            var secondEmployee = Schedule.Employees.FirstOrDefault (x => x.Info.EmployeeID == message.NewEmployeeID);

            if (secondEmployee == null)
                secondEmployee = Mvx.Resolve<SpareShiftService> ().GetEmployee (message.NewEmployeeID);

            if (secondEmployee == null)
                return;

            //This controls whether to set the ID on the shift data object, needs to be controlled in order for the schedule change message to be generated correctly.
            int? originalEmployeeId = null;
            if (message.StoreInUndoStack)
                originalEmployeeId = employee.EmployeeID;

            var shift = employee.GetShift (message.ShiftID);
            var originalShiftState = shift.Copy ();

            //New Functionality - First check to see if this is a straight swap, if it fails we just continue as normal unless a message was returned
            if (m_settings.ShiftSwapOnDragAndDropEnabled && message.Start.HasValue && message.End.HasValue) 
            {
                var swapValidationResponse = ScheduleChangeValidator.ValidateSwapShift (message, employee, secondEmployee, shift);
                if (swapValidationResponse.PassesValidation) 
                {
                    PerformSwapReassignOperation (message, employee, secondEmployee, shift, originalShiftState);
                    return;
                } 
                else if (!string.IsNullOrEmpty (swapValidationResponse.DisplayMessage)) 
                {
                    m_currentAlertToken = m_alert.ShowOK (m_localiser.Get("swap_shift_failed"), swapValidationResponse.DisplayMessage);
                    return;
                }
            }

            PerformNormalReassignOperation (message, employee, secondEmployee, shift, originalShiftState);
        }

        private bool IsNewlyCreatedShift (ShiftEditMessage message)
        {
            return message.ShiftID < 0;
        }

        private void PerformSwapReassignOperation (ShiftEditMessage message, EmployeeData sourceEmployee, EmployeeData targetEmployee, ShiftData sourceShift, ShiftData originalSourceShiftState)
        {
            //We are assuming this swap will "work" as the validation should have caught any issues
            //Get the "target" shift we are swapping with
            var overlaps = targetEmployee.GetOverlapShifts (message.Start.Value, message.End.Value);

            if(overlaps == null || overlaps.Count() != 1)
            {
                m_currentAlertToken = m_alert.ShowOK (m_localiser.Get("swap_shift_failed"), "Something went wrong swapping shifts");
                return;
            }

            var targetShift = overlaps.First ();
            var originalTargetShiftState = targetShift.Copy ();

            //Check resulting shift length for both employees
            var shiftLengthValidationResponse = ScheduleChangeValidator.ValidateShiftLength(targetEmployee, sourceShift);
            if(shiftLengthValidationResponse.PassesValidation)
            {
                shiftLengthValidationResponse = ScheduleChangeValidator.ValidateShiftLength(sourceEmployee, targetShift);
            }

            if(shiftLengthValidationResponse.PassesValidation == false)
            {
                if(string.IsNullOrEmpty(shiftLengthValidationResponse.DisplayMessage) == false)
                {
                    m_currentAlertToken = m_alert.ShowOK(m_localiser.Get("swap_shift_failed"), shiftLengthValidationResponse.DisplayMessage);
                }
                return;
            }

            //Reassign source shift to target
            sourceEmployee.SwapShiftWithEmployee (targetShift, sourceShift, targetEmployee);

            if (message.RecalculateDetails)
            {
                targetEmployee.CalculateDetails (sourceShift.ShiftID, m_rawSchedule.ScheduleRules);
            }

            targetEmployee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

            var secondPassValidationSource = ScheduleChangeValidator.ValidateMinor (sourceEmployee, targetShift.Start);
            var secondPassValidationTarget = ScheduleChangeValidator.ValidateMinor (targetEmployee, sourceShift.Start);

            if (!secondPassValidationSource.PassesValidation || !secondPassValidationTarget.PassesValidation) 
            {
                if(!secondPassValidationSource.PassesValidation)
                {
                    m_currentAlertToken = m_alert.ShowOK ("Shift Placement Failed", secondPassValidationSource.DisplayMessage);
                }
                else
                {
                    m_currentAlertToken = m_alert.ShowOK ("Shift Placement Failed", secondPassValidationTarget.DisplayMessage);
                }

                sourceEmployee.SwapShiftWithEmployee (originalSourceShiftState, originalTargetShiftState, targetEmployee);

                sourceEmployee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                targetEmployee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                return;
            }

            if (message.StoreInUndoStack) 
            {
                m_undoService.Store (new ScheduleUndoStackOperation (originalSourceShiftState, originalTargetShiftState, sourceEmployee.EmployeeID, targetEmployee.EmployeeID, message.Start.Value, message.End.Value, ShiftEditMessage.Operation.SwapShift));
            }

            EmployeeUpdate (sourceEmployee, !message.StoreInUndoStack, targetEmployee);
        }

        private void PerformNormalReassignOperation (ShiftEditMessage message, EmployeeData employee, EmployeeData secondEmployee, ShiftData shift, ShiftData originalShiftState)
        {
            if (message.Start != null && message.End != null)
                ConstrainedResize (shift, message.Start.Value, message.End.Value);

            var validationResponse = ScheduleChangeValidator.ValidateReassignShift (secondEmployee, shift, message.ShiftID);
            if(validationResponse.PassesValidation)
            {
                validationResponse =  ScheduleChangeValidator.ValidateShiftLength(secondEmployee, shift);
            }

            var shiftToBeReassigned = shift.Copy();
            if (validationResponse.PassesValidation && secondEmployee.ReassignShift (shiftToBeReassigned, employee)) 
            {
                //Recalc happens before second pass validation as minor rules are based on working hours (after breaks)
                if (message.RecalculateDetails)
                {
                    secondEmployee.CalculateDetails (shiftToBeReassigned.ShiftID, m_rawSchedule.ScheduleRules);
                }

                secondEmployee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                var secondPassValidation = ScheduleChangeValidator.ValidateMinor (secondEmployee, shiftToBeReassigned.Start);

                if (!secondPassValidation.PassesValidation)
                {
                    m_currentAlertToken = m_alert.ShowOK ("Shift Placement Failed", secondPassValidation.DisplayMessage);

                    var revertedToShift = shift.Copy ();

                    employee.ReassignShift (revertedToShift, secondEmployee);

                    ConstrainedResize (revertedToShift, originalShiftState.Start, originalShiftState.End);

                    employee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                    secondEmployee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

                    return;
                }

                if (message.StoreInUndoStack)
                {
                    m_undoService.Store (new ScheduleUndoStackOperation (originalShiftState, message.Op, message.EmployeeID, message.NewEmployeeID));
                }

                EmployeeUpdate (employee, !message.StoreInUndoStack, secondEmployee);
            } 
            else
            {
                if (validationResponse.PassesValidation)
                    m_currentAlertToken = m_alert.ShowOK ("Reassign Job Failed", "Operation Failed");
                else
                    m_currentAlertToken = m_alert.ShowOK ("Reassign Job Failed", validationResponse.DisplayMessage);

                //This rolls back the shift to the previous size because constrined resize would have modified it to fit in with the original space
                ConstrainedResize (shift, originalShiftState.Start, originalShiftState.End);
            }
        }

        void PerformFillShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            Schedule.AssignUnfilledShift(message.UnfilledShiftToFill, employee);

            if (message.StoreInUndoStack)
            {
                m_undoService.Store(new ScheduleUndoStackOperation(message.UnfilledShiftToFill.ShiftID, message.Op, message.EmployeeID));
            }

            if (message.RecalculateDetails)
                employee.CalculateDetails(message.UnfilledShiftToFill.ShiftID, m_rawSchedule.ScheduleRules);

            EmployeeUpdate(employee, !message.StoreInUndoStack);
        }

        void PerformUnfillShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            if (message.StoreInUndoStack)
            {
                m_undoService.Store(new ScheduleUndoStackOperation(message.ShiftID, message.Op, message.EmployeeID));
            }

            var shift = m_rawSchedule.GetShift(employee.EmployeeID, message.ShiftID);
            var shiftData = employee.GetShift(message.ShiftID);

            if(shift != null)
            {            
                Schedule.UnfillShift(shiftData, shift, employee);
            }
            else
            {
                //In case the shift we've filled is not sent to server yet and is not contained in m_rawService.GetShift(..) so shift is null
                Schedule.UnfillUnsavedShift(shiftData, employee);
            }

            EmployeeUpdate(employee, !message.StoreInUndoStack);

            SetEdit(true); // Successful update, set edit mode to true
        }

        void PerformReverseFillShiftOperation(ShiftEditMessage message, EmployeeData employee)
        {
            JDAShift shift = Schedule.PendingFilledShifts.FirstOrDefault(pending => pending.ShiftID == message.ShiftID);

            var shiftData = employee.GetShift(message.ShiftID);
           
            Schedule.UndoFillShift(shiftData, shift, employee);

            employee.DeleteShift(message.ShiftID);

            EmployeeUpdate(employee, !message.StoreInUndoStack);

            SetEdit(true); 
        }

        /// <summary>
        /// Constrains the resize of a shift to maintain logic consistency with the website:
        /// start time must remain with the bounds of the current date (00:00-23:45);
        /// shift length must be maintained if the start time is pushed up to the left bound;
        /// shift length must be maintained if the start time is pushed up to the right bound;
        /// </summary>
        /// <param name="shift">Shift.</param>
        /// <param name="start">New Start Time</param>
        /// <param name="end">New End Time</param> 
        private void ConstrainedResize(ShiftData shift, DateTime start, DateTime end)
        {
            DateTime currentDateStart = start.Date;//Schedule.StartDate.AddDays(Schedule.DayIndex.Value);
            if (start < currentDateStart)
            {
                end += currentDateStart - start;
                start = currentDateStart;
            }

            DateTime currentDateEnd = currentDateStart.AddHours(23).AddMinutes(45);
            if (start > currentDateEnd)
            {
                start = currentDateEnd;
            }

            shift.Resize(start, end);
        }

        /// <summary>
        /// Sends a notificaton that the schedule for an employee has been updated
        /// </summary>
        /// <param name="employee">Employee data to notify.</param>
        /// <param name="showAffectedEmployee">If set to <c>true</c> scroll the gridview to the affected employee. Used in undo mode this will allow the caller to perform an undo
        /// operation and show the user at the same time. Useful if the user has scrolled away from the affected employee</param>
        public void EmployeeUpdate(EmployeeData employee, bool showAffectedEmployee)
        {
            // Set Entire schedule as modified
            Schedule.IsModified = true;

            // Update the all metrics
            if (!m_settings.DontGenerateWarnings)
            {
                employee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);
                employee.CalculateWarnings ();
            }

            employee.CalculateMetrics(m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

            Schedule.CalculateMetrics();

            ScheduleUpdateMessage updateMessage = new ScheduleUpdateMessage(this)
                {
                    UpdateSchedule = true,
                    UpdateTotals = true,
                    UpdateHeaders = true,
                    UpdateWarnings = employee.WarningCount > 0,
                };

            if (showAffectedEmployee)
                updateMessage.ScrollToEmployeeId = employee.EmployeeID;

            // Inform listeners of the update
            m_messenger.Publish(updateMessage);
        }

        /// <summary>
        /// Sends a notificaton that the schedule for an employee has been updated
        /// </summary>
        /// <param name="employee">Employee data to notify.</param>
        /// <param name="showAffectedEmployee">If set to <c>true</c> scroll the gridview to the affected employee. Used in undo mode this will allow the caller to perform an undo
        /// operation and show the user at the same time. Useful if the user has scrolled away from the affected employee</param>
        private void EmployeeUpdate(EmployeeData employee, bool showAffectedEmployee, EmployeeData otherEmployee = null)
        {
            // Set Entire schedule as modified
            Schedule.IsModified = true;

            // Update the all metrics
            if (!m_settings.DontGenerateWarnings)
            {
                employee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);
                employee.CalculateWarnings ();
            }

            employee.CalculateMetrics(m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);

            if (otherEmployee != null)
            {
                if (!m_settings.DontGenerateWarnings)
                {
                    otherEmployee.CalculateMetrics (m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);
                    otherEmployee.CalculateWarnings ();
                }

                otherEmployee.CalculateMetrics(m_rawSchedule.BUSettings, m_rawSchedule.ScheduleRules);
            }

            Schedule.CalculateMetrics();

            ScheduleUpdateMessage updateMessage = new ScheduleUpdateMessage(this)
                {
                    UpdateSchedule = true,
                    UpdateTotals = true,
                    UpdateHeaders = true,
                    UpdateWarnings = employee.WarningCount > 0,
                };

            if (showAffectedEmployee)
                updateMessage.ScrollToEmployeeId = employee.EmployeeID;

            // Inform listeners of the update
            m_messenger.Publish(updateMessage);
        }

        //TODO: Refactor

        public async Task<List<EmployeeData>> GetAvailableEmployees(JDAShift shift)
        {
            List<EmployeeData> availableEmployees = new List<EmployeeData>();

            var availableEmployeesRequest = new AvailableEmployeesRequest()
                {
                    siteID = m_data.Site.SiteID,
                    shiftStartTime = shift.Start,
                    shiftEndTime = shift.End,
                    jobID = shift.Jobs.FirstOrDefault().JobID
                };
                        
            TaskGenerator taskGenerator = (token) => 
            {
                return new Task<RestReply<AvailableEmployeesReply>>(() =>
                    {
                        return m_client.Post<AvailableEmployeesReply>(availableEmployeesRequest, token).Result;
                    });
            };

            m_tokenSource = new CancellationTokenSource();
            var result = await PerformOperation<RestReply<AvailableEmployeesReply>>(m_client.TimeoutMs, taskGenerator, m_tokenSource.Token);

            if (result.ResponseObject != null && result.ResponseObject.Success)
            {
                foreach (var employee in result.ResponseObject.Object.data)
                {
					var scheduleEmployee = Schedule.GetEmployee (employee.EmployeeID);
					if(scheduleEmployee != null)
					{
                        bool overlappingShift = false;
                        var shifts = scheduleEmployee.GetShiftsOnDay(shift.Start);
                        if (shifts != null && shifts.Count() > 0)
                        {
                            foreach(var existingshift in shifts)
                            {
                                if (existingshift.Start < shift.End && existingshift.End > shift.Start)
                                {
                                    overlappingShift = true;
                                }
                            }
                        }
                        if (overlappingShift == false)
                        {
                            availableEmployees.Add (scheduleEmployee);
                        }
					}
                }
            }

            return availableEmployees;
        }

        public List<JDAJob> GetJobRolesForDepartment(int departmentId)
        {
            return m_rawSchedule.JobRoles.Where (x => x.WorkgroupID == departmentId).ToList();
        }

        public int GetSiteId()
        {
            return m_data.Site.SiteID;
        }

        public void Cancel()
        {
            m_tokenSource.Cancel();
        }

        public bool IsWithinPreventChangesPeriod(DateTime? timeToTest)
        {
            if (timeToTest.HasValue == false)
                return false;
            
            int hours = m_settings.PreventChangesHours;
            if (hours <= 0)
            {
                return false;
            }

            DateTime cutoffLimit = DateTime.Now.AddHours (24);
            if(timeToTest.Value > cutoffLimit)
            {
                return false;
            }

            return true;
        }
    }
}
