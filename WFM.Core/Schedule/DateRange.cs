﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace WFM.Core.Schedule
{
    public class DateRange: IEnumerable<DateTime>
    {
        public static TimeSpan DEFAULT_STEP_SIZE = new TimeSpan(1, 0, 0, 0);

        public DateTime StartDate
        {
            get;
            private set;
        }

        public DateTime EndDate
        {
            get;
            private set;
        }

        public TimeSpan StepSize
        {
            get;
            private set;
        }

        public TimeSpan Length
        {
            get
            {
                return EndDate.Subtract(StartDate);
            }
        }

        public bool IncludeStart
        {
            get;
            private set;
        }

        public bool IncludeEnd
        {
            get;
            private set;
        }

        public DateRange(DateTime from, DateTime to) : this(from,to,DEFAULT_STEP_SIZE)
        {
            
        }

        public DateRange(DateTime from, DateTime to, TimeSpan stepSize) : this(from,to,stepSize,true,false)
        {
            
        }

        public DateRange(DateTime from, DateTime to, TimeSpan stepSize, bool includeStart, bool includeEnd)
        {
            if (to < from)
                throw new ArgumentException("End date must be equal to or after start date");

            EndDate = to;
            StartDate = from;

            StepSize = stepSize;

            IncludeStart = includeStart;
            IncludeEnd = includeEnd;
        }

        private DateTime Step(DateTime date)
        {
            return date.Add(StepSize);
        }

        /// <summary>
        /// Returns an IEnumerator{T} running over the range.
        /// </summary>
        public IEnumerator<DateTime> GetEnumerator()
        {
            DateTime start = StartDate;
            DateTime end = EndDate;

            IComparer<DateTime> comparer = Comparer<DateTime>.Default;

            DateTime value = start;

            if(IncludeStart)
                yield return value;

            value = Step(value);

            while (comparer.Compare(value, end) < 0)
            {
                yield return value;
                value = Step(value);
            }
                
            if (IncludeEnd && comparer.Compare(value, end) == 0)
            {
                yield return value;
            }
        }

        /// <summary>
        /// Returns an IEnumerator running over the range.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

