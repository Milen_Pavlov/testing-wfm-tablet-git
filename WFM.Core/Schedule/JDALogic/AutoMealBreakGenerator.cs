﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    /// <summary>
    /// Auto meal break generator.
    /// This pretty closely matches the behaviour of the site manager Javascript functionality. A few .net specific 
    /// refactorings have taken place but it's pretty much the same logic.
    /// </summary>
    public class AutoMealBreakGenerator
    {
        ShiftData m_shiftObject;
        Tuple<DateTime,DateTime> m_shift;
        JDAScheduleAllocation m_allocation; 
        Queue<char> m_mealBreakOrder;
        List<ShiftDetailData> m_details;
        bool m_isRegeneration;
        
        JDAScheduleRule m_rule;
        
        public AutoMealBreakGenerator (JDAScheduleRule rule)
        {
            m_rule = rule;
        }
        
        public List<ShiftDetailData> RegenerateMealsAndBreaks(ShiftData shift)
        {
            return CreateListOfGeneratedDetails(shift,new Tuple<DateTime, DateTime>(shift.Start,shift.End),(JDAScheduleAllocation)null,false);
        }
        
        List<ShiftDetailData> CreateListOfGeneratedDetails(ShiftData shift, Tuple<DateTime, DateTime> shiftLength, JDAScheduleAllocation allocation, bool isRegeneration) 
        {
            m_shiftObject = shift;
            m_shift = shiftLength;
            m_details = new List<ShiftDetailData>();

            var shiftDuration = m_shift.GetDuration();
            
            if(allocation == null)
            {                
                allocation = new RuleAllocationFilterService().GetAllocationForShiftLengthAndStartTime(shiftDuration,m_shift.GetStart(),m_rule);
            }

            if(allocation != null)
            {
                m_allocation = allocation;
                m_isRegeneration = isRegeneration;
        
                m_mealBreakOrder = new Queue<char>(generateSequenceOfDetails(allocation));
        
                createAndPlaceDetailsBasedOnAllocation();
                doFinalAdjustmentForSpaceBetweenDetails();
            }
    
            return m_details;
        }
        
        IList<char> generateSequenceOfDetails(JDAScheduleAllocation alloc) 
        {
            var mb = new List<char>();
            
            var breaksLeft = alloc.NumberOfBreaks;
            var mealsLeft = alloc.NumberOfMeals;
            var total = breaksLeft + mealsLeft;
            
            var lastPunchAdded = getStartingPunchCode();
            
            for (var i = 0; i < total; i++) 
            {
                if ((lastPunchAdded == 'm' && breaksLeft > 0) || onlyBreaksLeft(breaksLeft, mealsLeft)) 
                {
                    mb.Add('b');
                    breaksLeft--;
                    lastPunchAdded = 'b';
                } 
                else if (mealsLeft > 0) 
                {
                    mb.Add('m');
                    mealsLeft--;
                    lastPunchAdded = 'm';
                }
            }
            
            return mb;
        }
        
        void createAndPlaceDetailsBasedOnAllocation() 
        {
            Tuple<DateTime, DateTime> shiftWorkingCopy = new Tuple<DateTime, DateTime> (m_shift.Item1,m_shift.Item2);

            m_details = new List<ShiftDetailData>();
    
            while (m_mealBreakOrder.Count != 0) 
            {
                var detail = createDetail(shiftWorkingCopy, m_mealBreakOrder.Peek());
    
                m_details.Add(detail);
    
                shiftWorkingCopy = new Tuple<DateTime, DateTime>(detail.End.GetValueOrDefault(), m_shift.GetEnd());
                
                m_mealBreakOrder.Dequeue();
            }
        }
        
        ShiftDetailData createDetail(Tuple<DateTime, DateTime> shiftWorkingCopy, char type) 
        {
            var allocWorkingCopy = getWorkingCopyAllocation(false, shiftWorkingCopy);
            
            var detailPositioner = new DetailPositioner(type, allocWorkingCopy, m_shift, shiftWorkingCopy);
            
            if (detailPositioner.NaturalPlacementWasCompromised) 
            {
                var newPlacementDetailList = resetPlacementOfPriorDetails(detailPositioner);
                m_details = newPlacementDetailList;
            }

            ShiftDetailData detail = ShiftDetailData.Create(ShiftData.GenerateUniqueID());
            
            detail.Start = detailPositioner.Start;
            detail.End = detailPositioner.End;
            detail.PunchCode = type.ToString();
            
            detail.SetOriginalValues();
            
            return detail;
        }
        
        List<ShiftDetailData> resetPlacementOfPriorDetails (DetailPositioner detailPositioner) 
        {
            var segmentOfShiftToRegenerate = new Tuple<DateTime,DateTime>(m_shift.GetStart(),m_shift.GetStart().Add(detailPositioner.DistanceToDetail));            
            
            var allocBasedOnShiftSegment = getWorkingCopyAllocation(true, segmentOfShiftToRegenerate);
            var mbGen = new AutoMealBreakGenerator(m_rule);
            var newDetailList = mbGen.CreateListOfGeneratedDetails(m_shiftObject, segmentOfShiftToRegenerate, allocBasedOnShiftSegment, true);
            return newDetailList;
        }
        
        JDAScheduleAllocation getWorkingCopyAllocation(bool isRegeneration, Tuple<DateTime, DateTime> shiftWorkingCopy) 
        {
            var minMealFromEnd = m_allocation.MealMinTimeBeforeShiftEnd;
            var minBreakFromEnd = m_allocation.BreakMinTimeBeforeShiftEnd;
            var shiftDuration = m_shift.GetDuration();
            
            var shiftWorkingCopyDuration = shiftWorkingCopy.GetDuration();
            
            if (isRegeneration) 
            {
                minMealFromEnd = (int)(TimeSpan.FromMinutes(minMealFromEnd) - (shiftDuration - shiftWorkingCopyDuration)).TotalMinutes;
                minBreakFromEnd = (int)(TimeSpan.FromMinutes(minBreakFromEnd) - (shiftDuration - shiftWorkingCopyDuration)).TotalMinutes;
                minMealFromEnd = Math.Max(0, minMealFromEnd);
                minBreakFromEnd = Math.Max(0, minBreakFromEnd);
            }
            
            var shiftAllocation = new JDAScheduleAllocation();
            
            shiftAllocation.EndTime = m_allocation.EndTime;
            shiftAllocation.StartTime = shiftAllocation.StartTime;
            shiftAllocation.BreakDuration = m_allocation.BreakDuration;
            shiftAllocation.BreakDurationPaid = m_allocation.BreakDurationPaid;
            shiftAllocation.BreakMinTimeAfterShiftStart = m_allocation.BreakMinTimeAfterShiftStart;
            shiftAllocation.MealDuration = m_allocation.MealDuration;
            shiftAllocation.MealDurationPaid = m_allocation.MealDurationPaid;
            shiftAllocation.MealMinTimeAfterShiftStart = m_allocation.MealMinTimeAfterShiftStart;
            shiftAllocation.MinTimeAfterShiftStart = m_allocation.MinTimeAfterShiftStart;
            shiftAllocation.MinTimeBeforeShiftEnd = m_allocation.MinTimeBeforeShiftEnd;
            shiftAllocation.TimeBetweenBreaks = m_allocation.TimeBetweenBreaks;
            shiftAllocation.ShiftDuration = m_allocation.ShiftDuration;
            shiftAllocation.NumberOfBreaks = getNumberOfBreaksForWorkingCopyAllocation(isRegeneration);
            shiftAllocation.NumberOfMeals = getNumberOfMealsForWorkingCopyAllocation(isRegeneration);
            shiftAllocation.MealMinTimeBeforeShiftEnd = minMealFromEnd;
            shiftAllocation.BreakMinTimeBeforeShiftEnd = minBreakFromEnd;
            
            return shiftAllocation;
        }
        
        int getNumberOfBreaksForWorkingCopyAllocation(bool isRegeneration) 
        {
            if(isRegeneration)
                return m_details.Count(d => d.IsBreak);
            else
                return m_mealBreakOrder.Count(d => d == 'b');
        }
        
        int getNumberOfMealsForWorkingCopyAllocation(bool isRegeneration) 
        {
            if(isRegeneration)
                return m_details.Count(d => d.IsMeal);
            else
                return m_mealBreakOrder.Count(d => d == 'm');
        }
        

        void doFinalAdjustmentForSpaceBetweenDetails() 
        {
            if (m_isRegeneration) 
            {
                return;
            }
            
            for (var i = m_details.Count - 1; i > 0; i--) 
            {
                var currentDetail = m_details[i];
                var previousDetail = m_details[i - 1];
                var spacingIsGood = isSpacingGoodBetweenDetails(currentDetail, previousDetail);
                
                if (!spacingIsGood) 
                {
                    adjustPreviousDetailForCorrectSpacing(previousDetail, currentDetail);
                }
            }
        }
        
        bool isSpacingGoodBetweenDetails(ShiftDetailData currentDetail, ShiftDetailData previousDetail) 
        {
            var spaceBetweenCurrentAndPreviousDetail = currentDetail.Start.Value.Subtract(previousDetail.End.Value);
            var minsBetweenBreaks = TimeSpan.FromMinutes(m_allocation.TimeBetweenBreaks);
            var spacingIsGood = spaceBetweenCurrentAndPreviousDetail >= minsBetweenBreaks;
            return spacingIsGood;
        }
        
        void adjustPreviousDetailForCorrectSpacing(ShiftDetailData previousDetail, ShiftDetailData currentDetail) 
        {
            var newDistanceToDetail = calculateNewDistanceForPreviousDetail(currentDetail, previousDetail);
            var previousDetailDuration = previousDetail.End.Value.Subtract(previousDetail.Start.Value).SnapTo15Minutes();
            previousDetail.Start = m_shift.GetStart().Add(newDistanceToDetail);
            previousDetail.End = previousDetail.Start.Value.Add(previousDetailDuration);
        }
        
        TimeSpan calculateNewDistanceForPreviousDetail(ShiftDetailData currentDetail, ShiftDetailData previousDetail) 
        {
            var previousDetailDuration = previousDetail.DurationSpan;
            var minsBetweenBreaks = m_allocation.TimeBetweenBreaks;
            var distanceToCurrentDetail = currentDetail.Start.Value.Subtract(m_shift.GetStart());
            var newDistanceToPreviousDetail = distanceToCurrentDetail - (TimeSpan.FromMinutes(minsBetweenBreaks) + previousDetailDuration);
            
            return newDistanceToPreviousDetail;
        }
        
        bool onlyBreaksLeft(int numBreaks, int numMeals) 
        {
            return numBreaks > 0 && numMeals == 0;
        }
        
        char getStartingPunchCode() 
        {
            char lastPunchAdded = 'm';
            
            if (leapFrogIsNeeded()) 
            {
                lastPunchAdded = 'b';
            }
            
            return lastPunchAdded;
        }
        
        bool leapFrogIsNeeded() 
        {
            var shiftDuration = m_shift.GetDuration();
            
            var mealDuration = m_allocation.MealDuration;
            var mealStartMinTimeBeforeShiftEnd = m_allocation.MealMinTimeBeforeShiftEnd + mealDuration;
            var maxDistanceToFirstMeal = shiftDuration.Subtract(TimeSpan.FromMinutes(mealStartMinTimeBeforeShiftEnd));
            var minDistanceToFirstBreak = m_allocation.BreakMinTimeAfterShiftStart;
            
            minDistanceToFirstBreak += m_allocation.TimeBetweenBreaks;
            
            var noRoomToPlaceFirstBreak = maxDistanceToFirstMeal <= TimeSpan.FromMinutes(minDistanceToFirstBreak);
            
            return noRoomToPlaceFirstBreak;
       }
    }
}