﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Core
{
    public class RuleAllocationFilterService
    {
        public JDAScheduleAllocation GetAllocationForShiftLengthAndStartTime (TimeSpan duration, DateTime startTime, JDAScheduleRule rule)
        {
            if(rule == null)
            {
                return null;
            }

            var remainingAllocations = FilterAllocationsByTime(rule.ScheduleAllocations, startTime);

            return GetAllocationForShiftLength(remainingAllocations, duration);
        }

        List<JDAScheduleAllocation> FilterAllocationsByTime (List<JDAScheduleAllocation> scheduleAllocations, DateTime startDateTime)
        {
            var remainingAllocations = new List<JDAScheduleAllocation>();
            // if allocation start time is range of schedule allocations add it to the list
            foreach (var scheduleAllocation in scheduleAllocations) 
            {
                var allocationStartTime = scheduleAllocation.StartTime.HasValue ? CreateDateTime(startDateTime, scheduleAllocation.StartTime.Value) : startDateTime;
                var allocationEndTime = scheduleAllocation.EndTime.HasValue ? CreateDateTime(startDateTime, scheduleAllocation.EndTime.Value) : startDateTime;
                if(allocationStartTime <= startDateTime && allocationEndTime >= startDateTime)
                {
                    remainingAllocations.Add(scheduleAllocation);
                }
            }

            return remainingAllocations ?? new List<JDAScheduleAllocation>();
        }

        DateTime CreateDateTime (DateTime startDateTime, DateTime timeToBeAdded)
        {
            //start with date only and add hours and minutes from JDAScheduleAllocation
            return startDateTime.Date.AddHours(timeToBeAdded.Hour).AddMinutes(timeToBeAdded.Minute);
        }

        JDAScheduleAllocation GetAllocationForShiftLength (List<JDAScheduleAllocation> remainingAllocations, TimeSpan shiftDuration)
        {        
            JDAScheduleAllocation allocationToUse = null;

            foreach (var allocationToCheck in remainingAllocations) 
            {
                var allocationIsLessThanDuration = TimeSpan.FromHours((double)allocationToCheck.ShiftDuration) <= shiftDuration;
                if(allocationIsLessThanDuration)
                {
                    var allocationNullOrMoreThanPrevious = allocationToUse == null || allocationToUse.ShiftDuration < allocationToCheck.ShiftDuration;
                    if(allocationNullOrMoreThanPrevious)
                    {
                        allocationToUse = allocationToCheck;
                    }
                }
            }

            return allocationToUse;
        }
    }
}