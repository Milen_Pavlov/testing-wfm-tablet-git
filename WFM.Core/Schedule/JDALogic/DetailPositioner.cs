﻿using System;
namespace WFM.Core
{
    public class DetailPositioner
    {
        const int THRESHOLD_TO_CENTRE_AT_MIDDLE_MINUTES = 30; 
        
        public bool NaturalPlacementWasCompromised { get; private set; }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        public TimeSpan DistanceToDetail { get; private set; }
        
        private char m_type = ' ';
        private Tuple<DateTime, DateTime> m_shift;
        private JDAScheduleAllocation m_allocation;
        
        TimeSpan DetailBeforeShiftStart
        {
            get
            {
                return m_type == 'b' ? TimeSpan.FromMinutes(m_allocation.BreakMinTimeAfterShiftStart) : TimeSpan.FromMinutes(m_allocation.MealMinTimeAfterShiftStart);
            }
        }
        
        TimeSpan DetailBeforeShiftEnd
        {
            get
            {
                return m_type == 'b' ? TimeSpan.FromMinutes(m_allocation.BreakMinTimeBeforeShiftEnd): TimeSpan.FromMinutes(m_allocation.MealMinTimeBeforeShiftEnd);
            }
        }
        
        TimeSpan DetailDuration
        {
            get
            {
                return m_type == 'b' ? TimeSpan.FromMinutes(m_allocation.BreakDuration) : TimeSpan.FromMinutes(m_allocation.MealDuration);
            }
        }
        
        public DetailPositioner (char type, JDAScheduleAllocation allocation, Tuple<DateTime, DateTime> shift, Tuple<DateTime, DateTime> shiftWorkingCopy) 
        {
            m_type = type;
            m_shift = shift;
            m_allocation = allocation;
            
            DistanceToDetail = calculateDistanceToDetail(shiftWorkingCopy, DetailDuration);
            
            Start = m_shift.GetStart().Add(DistanceToDetail);
            End = Start.Add(DetailDuration);
        }
    
        private TimeSpan calculateDistanceToDetail (Tuple<DateTime, DateTime> shiftWorkingCopy, TimeSpan detailDuration) 
        {    
            TimeSpan distanceToDetail = calculateDistanceBetweenDetails(shiftWorkingCopy);
            
            TimeSpan distanceFromOriginalStart = getDistanceFromOriginalStartToCurrentPosition(m_shift.GetStart(), shiftWorkingCopy.Item1);
            
            distanceToDetail += distanceFromOriginalStart;
            
            distanceToDetail = adjustForCenteringDetailFromTheMiddle(detailDuration, distanceToDetail);
            distanceToDetail = adjustForSpaceBetweenDetails(shiftWorkingCopy, distanceToDetail);
            distanceToDetail = adjustForMinutesAfterShiftStart(distanceToDetail);
            distanceToDetail = adjustForMinutesBeforeShiftEnd(detailDuration, distanceToDetail);
            
            return distanceToDetail.SnapTo15Minutes();
        }
        
        private TimeSpan getDistanceFromOriginalStartToCurrentPosition(DateTime start, DateTime shiftOriginalStart) 
        {
            return shiftOriginalStart.Subtract(start);
        } 
        
        private TimeSpan calculateDistanceBetweenDetails (Tuple<DateTime, DateTime> shiftWorkingCopy) 
        {
            var shiftDuration = shiftWorkingCopy.GetDuration();
            var numberOfDetails = m_allocation.NumberOfBreaks + m_allocation.NumberOfMeals;
            
            return TimeSpan.FromMinutes(shiftDuration.TotalMinutes / (numberOfDetails + 1)).SnapTo15Minutes();
        }
    
        private TimeSpan adjustForCenteringDetailFromTheMiddle (TimeSpan detailDuration, TimeSpan distanceToDetail) 
        {
            var ifDetailIsLargeEnoughToCenterFromTheMiddle = detailDuration.TotalMinutes >= THRESHOLD_TO_CENTRE_AT_MIDDLE_MINUTES;
            
            if (ifDetailIsLargeEnoughToCenterFromTheMiddle) 
            {
                distanceToDetail = distanceToDetail - TimeSpan.FromMinutes((detailDuration.TotalMinutes / 2));
                NaturalPlacementWasCompromised = true;
            }
            
            return distanceToDetail;
        }
    
        private TimeSpan adjustForSpaceBetweenDetails (Tuple<DateTime, DateTime> shiftWorkingCopy, TimeSpan distanceToDetail) 
        {
            var distanceFromOriginalStart = getDistanceFromOriginalStartToCurrentPosition(m_shift.GetStart(), shiftWorkingCopy.GetStart());
            var shiftDuration = m_shift.GetDuration();
            var shiftWorkingCopyDuration = shiftWorkingCopy.GetDuration();
            var placingFirstDetail = shiftDuration == shiftWorkingCopyDuration;
            
            if (!placingFirstDetail) 
            {
                if (m_allocation.TimeBetweenBreaks > (distanceToDetail - distanceFromOriginalStart).TotalMinutes) 
                {
                    distanceToDetail = distanceFromOriginalStart.Add(TimeSpan.FromMinutes(m_allocation.TimeBetweenBreaks));
                }
            }
            
            return distanceToDetail;
        }
    
        private TimeSpan adjustForMinutesAfterShiftStart(TimeSpan distanceToDetail) 
        {
            var minDistanceFromShiftStartIsViolated = distanceToDetail < DetailBeforeShiftStart;
            
            if (minDistanceFromShiftStartIsViolated)
            {
                distanceToDetail = DetailBeforeShiftStart;
                NaturalPlacementWasCompromised = true;
            }
            
            return distanceToDetail;
        }
    
        private TimeSpan adjustForMinutesBeforeShiftEnd (TimeSpan detailDuration,TimeSpan distanceToDetail) 
        {
            if (minDistanceToShiftEndIsViolated(distanceToDetail, DetailBeforeShiftEnd, detailDuration)) 
            {
                distanceToDetail = m_shift.GetDuration() - DetailBeforeShiftEnd - detailDuration;
                NaturalPlacementWasCompromised = true;
            }
            
            return distanceToDetail;
        }
        
        bool minDistanceToShiftEndIsViolated(TimeSpan distanceToDetail, TimeSpan timeBeforeEnd, TimeSpan detailDuration) 
        {
            var distanceToDetailEnd = distanceToDetail + detailDuration;
            var distanceFromShiftEnd = m_shift.GetDuration() - distanceToDetailEnd;
            var distanceToShiftEndIsViolated = distanceFromShiftEnd < timeBeforeEnd;
            
            return distanceToShiftEndIsViolated;
        }
    }
}