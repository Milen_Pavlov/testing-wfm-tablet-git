﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using System.Diagnostics;

namespace WFM.Core
{
    public class ScheduleChange
    {
        private readonly IESSConfigurationService m_settings;

        public class ChangeCommand : JDAChangeCommand
        {
            public int Priority { get; set; }
        }

        private int m_siteId;
        private EmployeeData m_employee;
        private ShiftData m_shift;

        public ScheduleChange()
        {
            m_settings = Mvx.Resolve<IESSConfigurationService> ();
        }

        public ProcessChangeCommandsRequest Create(ScheduleData data, int siteID)
        {
            m_siteId = siteID;
            var commands = GenerateCommand (data);

            var cmd = new ProcessChangeCommandsRequest ();
            cmd.jsChangeCommands = commands.OrderBy (x => x.Priority).Select (x => x as JDAChangeCommand).ToArray();

            return cmd;
        }

        private void ProcessShift(ShiftData shift, List<ChangeCommand> cmdList, DateTimeOffset offset)
        {
            if (shift.Edit == EditState.None)
                return;

            m_shift = shift;

            var deleted = shift.GetDeletedWarnings ();

            foreach (var warnID in deleted)
                cmdList.Add(DeleteShiftWarningCommand (shift, warnID));

            var added = shift.GetAddedWarnings ();

            foreach (var warnID in added)
                cmdList.Add(AddShiftWarningCommand (shift, warnID));

            // Deleted Jobs
            foreach (var job in shift.DeletedJobs)
            {
                if (job.ScheduledJobID >= 0)
                {
                    cmdList.Add (DeleteJobCommand (job));
                }
            }

            // Added & Modified Jobs
            var jobs = shift.Jobs;
            var details = shift.DeletedDetails;
            if (jobs != null && jobs.Count > 1 && jobs[0].OriginalStart < jobs[0].Start)
            {
                jobs.Sort((x, y) => y.Start.CompareTo(x.Start));
                details.Sort ((x, y) => y.Start.GetValueOrDefault().CompareTo (x.Start.GetValueOrDefault()));
            }
            foreach (var job in jobs)
            {
                if (job.Edit == EditState.Added || job.AddedWithoutSaving)
                {
                    cmdList.Add(AddJobCommand (job, offset));

                    if(job.AddedWithoutSaving){
                        job.AddedWithoutSaving = false;
                    }
                }
                else if (job.Edit == EditState.Modified)
                {
                    cmdList.Add(ModifyJobCommand (shift, job, offset, shift.OriginalEmployeeId));
                }
            }

            // Deleted Details
            foreach (var detail in details)
                cmdList.Add(DeleteDetailCommand (detail));

            // Added & Modified Details
            foreach (var detail in shift.Details)
            {
                if (detail.Edit == EditState.Added)
                {
                    cmdList.Add(AddDetailCommand (detail));
                }
                else if (detail.Edit == EditState.Modified)
                {
                    cmdList.Add(ModifyDetailCommand (detail));
                }
            }

            m_shift = null;
        }

        private ChangeCommand CreateAuditForChangedShift(ShiftData shift, DateTimeOffset offset)
        {
            if (shift.Edit == EditState.None)
                return null;

            if (m_settings.SendScheduleAudit == false)
                return null;

            m_shift = shift;

            int firstJobID = shift.Jobs.First ().JobID;
            JDAUserIntent? userIntent = null;
            List<JDAScheduleAuditDetail> auditDetails = new List<JDAScheduleAuditDetail> ();

            // Added & Modified Jobs
            foreach (var job in shift.Jobs)
            {
                if (job.Edit == EditState.Added || job.AddedWithoutSaving)
                {
                    JDAScheduleAuditDetail addedDetail = new JDAScheduleAuditDetail () {
                        AuditDetailType = JDAAuditDetailType.Job.ToString (),
                        primaryEditColumn = true,
                        ChangedStart = job.Start.ToString("s"), 
                        ChangedEnd = job.End.ToString("s"),
                        ChangedJob = job.JobID,
                        AuditChangeType = JDAAuditChangeType.Added.ToString (),
                    };
                    auditDetails.Add (addedDetail);
                    if(userIntent == null)
                        userIntent = JDAUserIntent.ShiftAdded;
                }
                else if (job.Edit == EditState.Modified)
                {
                    JDAScheduleAuditDetail modifiedDetail = new JDAScheduleAuditDetail () {
                        AuditDetailType = JDAAuditDetailType.Job.ToString (),
                        primaryEditColumn = true,
                        ChangedStart = job.Start.ToString("s"), 
                        ChangedEnd = job.End.ToString("s"),
                        ChangedJob = job.JobID,
                        OriginalStart = job.OriginalStart.ToString ("s"),
                        OriginalEnd = job.OriginalEnd.ToString("s"),
                        OriginalJob = job.OriginalJobID,
                        AuditChangeType = JDAAuditChangeType.Changed.ToString (),
                    };
                    auditDetails.Add (modifiedDetail);
                    if(userIntent == null)
                        userIntent = JDAUserIntent.ShiftChanged;
                }
            }

            // Deleted Details
            foreach (var detail in shift.DeletedDetails)
            {
                JDAScheduleAuditDetail deletedDetail = new JDAScheduleAuditDetail ()
                {
                    AuditDetailType = JDAAuditDetailType.Job.ToString (),
                    primaryEditColumn = true,
                    ChangedStart = detail.Start.Value.ToString ("s"), 
                    ChangedEnd = detail.End.Value.ToString ("s"),
                    ChangedJob = firstJobID,
                    AuditChangeType = JDAAuditChangeType.Deleted.ToString (),
                };
                if (detail.IsMeal)
                {
                    if(userIntent == null)
                        userIntent = JDAUserIntent.MealDeleted;
                    deletedDetail.AuditDetailType = JDAAuditDetailType.Meal.ToString ();
                }
                else if (detail.IsBreak)
                {
                    if(userIntent == null)
                        userIntent = JDAUserIntent.BreakDeleted;
                    deletedDetail.AuditDetailType = JDAAuditDetailType.Break.ToString ();
                }
                else if(detail.IsRole)
                {
                    if(userIntent == null)
                        userIntent = JDAUserIntent.RoleDeleted;
                    deletedDetail.AuditDetailType = JDAAuditDetailType.Role.ToString ();
                }

                if(userIntent == null)
                    userIntent = JDAUserIntent.ShiftDeleted;
                auditDetails.Add (deletedDetail);
            }

            // Added & Modified Details
            foreach (var detail in shift.Details)
            {
                if (detail.Edit == EditState.Added)
                {
                    JDAScheduleAuditDetail addedDetail = new JDAScheduleAuditDetail () {
                        AuditDetailType = JDAAuditDetailType.Job.ToString (),
                        ChangedStart = detail.Start.Value.ToString("s"), 
                        ChangedEnd = detail.End.Value.ToString("s"),
                        ChangedJob = firstJobID,
                        AuditChangeType = JDAAuditChangeType.Added.ToString (),
                    }; 
                    if (detail.IsMeal)
                    {
                        addedDetail.AuditDetailType = JDAAuditDetailType.Meal.ToString ();
                        if(userIntent == null)
                            userIntent = JDAUserIntent.MealAdded;
                    }
                    else if (detail.IsBreak)
                    {
                        addedDetail.AuditDetailType = JDAAuditDetailType.Break.ToString ();
                        if(userIntent == null)
                            userIntent = JDAUserIntent.BreakAdded;
                    }
                    else if (detail.IsRole)
                    {
                        addedDetail.AuditDetailType = JDAAuditDetailType.Role.ToString ();
                        if(userIntent == null)
                            userIntent = JDAUserIntent.RoleAdded;
                    }
                    auditDetails.Add (addedDetail);
                    if(userIntent == null)
                        userIntent = JDAUserIntent.ShiftAdded;
                }
                else if (detail.Edit == EditState.Modified)
                {
                    JDAScheduleAuditDetail modifiedDetail = new JDAScheduleAuditDetail () {
                        AuditDetailType = JDAAuditDetailType.Job.ToString (),
                        ChangedStart = detail.Start.Value.ToString("s"), 
                        ChangedEnd = detail.End.Value.ToString("s"),
                        ChangedJob = firstJobID,
                        OriginalStart = detail.OriginalStart.HasValue ? detail.OriginalStart.Value.ToString("s") : detail.Start.Value.ToString("s"), 
                        OriginalEnd = detail.OriginalEnd.HasValue ? detail.OriginalEnd.Value.ToString("s") : detail.End.Value.ToString("s"),
                        OriginalJob = firstJobID,
                        AuditChangeType = JDAAuditChangeType.Changed.ToString (),
                    };
                    if (detail.IsMeal)
                    {
                        modifiedDetail.AuditDetailType = JDAAuditDetailType.Meal.ToString ();
                        if(userIntent == null)
                            userIntent = JDAUserIntent.MealChanged;
                    }
                    else if (detail.IsBreak)
                    {
                        modifiedDetail.AuditDetailType = JDAAuditDetailType.Break.ToString ();
                        if(userIntent == null)
                            userIntent = JDAUserIntent.BreakChanged;
                    }
                    else if(detail.IsRole)
                    {
                        modifiedDetail.AuditDetailType = JDAAuditDetailType.Role.ToString ();
                        if(userIntent == null)
                            userIntent = JDAUserIntent.RoleChanged;
                    }
                    auditDetails.Add (modifiedDetail);
                    if(userIntent == null)
                        userIntent = JDAUserIntent.BreakDeleted;
                }
            }
                


            if (auditDetails.Count () <= 0)
                return null;

            if(userIntent == null)
                userIntent = JDAUserIntent.ShiftChanged;
            JDAAddScheduleAuditCommand auditResult = new JDAAddScheduleAuditCommand () {
                UserIntent = userIntent.ToString(),
                EmployeeID = shift.EmployeeID,
                ScheduleDate = shift.Start.ToString("s"),
                ScheduleType = "Manual",
                FirstJobID = shift.Jobs.First().JobID,
                ScheduleAuditDetails = auditDetails.ToArray(),
                Priority = 100
            };

            auditResult.ShiftID = m_shift.ShiftID;

            m_shift = null;

            return auditResult;
        }

        private ChangeCommand CreateAuditForDeletedShift(ShiftData shift, DateTimeOffset offset)
        {
            if (shift.Edit == EditState.None)
                return null;

            if (m_settings.SendScheduleAudit == false)
                return null;
            
            m_shift = shift;

            int? firstJob = null;
            List<JDAScheduleAuditDetail> deletedJobs = new List<JDAScheduleAuditDetail> ();
            foreach(var job in shift.DeletedJobs)
            {
                JDAScheduleAuditDetail deletedDetail = new JDAScheduleAuditDetail () {
                    AuditDetailType = JDAAuditDetailType.Job.ToString (),
                    primaryEditColumn = true,
                    OriginalStart = job.OriginalStart.ToString("s"), 
                    OriginalEnd = job.OriginalEnd.ToString("s"),
                    OriginalJob = job.OriginalJobID,
                    AuditChangeType = JDAAuditChangeType.Changed.ToString (),
                };
                deletedJobs.Add (deletedDetail);

                if (firstJob.HasValue == false)
                {
                    firstJob = job.JobID;
                }
            };

            JDAAddScheduleAuditCommand auditResult = new JDAAddScheduleAuditCommand () {
                UserIntent = JDAUserIntent.ShiftDeleted.ToString(),
                EmployeeID = shift.EmployeeID,
                ScheduleDate = shift.Start.ToString("s"),
                FirstJobID = firstJob.GetValueOrDefault(0),
                ScheduleAuditDetails = deletedJobs.ToArray(),

                Priority = 100,
            };

            return auditResult;
        }

        private void ProcessDeletedUnfilledShift(JDAShift shift, List<ChangeCommand> cmdList)
        {
            cmdList.Add(DeleteUnfilledShiftCommand(shift));
        }

        private List<ChangeCommand> GenerateCommand(ScheduleData data)
        {
            var cmdList = new List<ChangeCommand> ();
            var auditList = new List<ChangeCommand> ();

            foreach (var employee in data.Employees)
            {
                m_employee = employee; 

                foreach (var day in employee.Days)
                {
                    foreach (var shift in day.Shifts)
                    {
                        ProcessShift (shift, cmdList, data.BUSettings.BusinessUnitOffset);
                        auditList.Add(CreateAuditForChangedShift (shift, data.BUSettings.BusinessUnitOffset));
                    }    
                }

                foreach (var shift in employee.DeletedShifts)
                {
                    ProcessShift (shift, cmdList, data.BUSettings.BusinessUnitOffset);
                    auditList.Add(CreateAuditForDeletedShift (shift, data.BUSettings.BusinessUnitOffset));
                }

                if (m_settings.SendWeeklyWarningsUpdates)
                {
                    foreach (var weekly in employee.WeeklyWarnings)
                    {
                        if (weekly.Edit == EditState.Added)
                        {
                            cmdList.Add (AddWeeklyWarningCommand (weekly));
                        }
                    }

                    foreach (var daily in employee.DailyWarnings)
                    {
                        if (daily.Edit == EditState.Added)
                        {
                            cmdList.Add (AddDailyWarningCommand (daily));
                        }
                    }

                    foreach (var weeklyDelete in employee.DeletedWeeklyWarnings.ToList())
                    {
                        cmdList.Add (DeleteWeeklyWarningCommand (weeklyDelete));
                        employee.DeletedWeeklyWarnings.Remove (weeklyDelete);
                    }

                    foreach (var dailyDelete in employee.DeletedDailyWarnings.ToList())
                    {
                        cmdList.Add (DeleteDailyWarningCommand (dailyDelete));
                        employee.DeletedDailyWarnings.Remove (dailyDelete);
                    }
                }
            }

            m_employee = null;

            foreach (var shift in data.DeletedUnfilledShifts)
            {
                ProcessDeletedUnfilledShift(shift, cmdList);
            }

            foreach (var shift in data.PendingUnfilledShifts)
            {
                foreach(var job in shift.Jobs)
                {
                    // Delete all details in job
                    foreach(var breakDetail in job.Breaks)
                    {
                        cmdList.Add(DeleteJDADetailCommand(breakDetail, shift, true));
                    }
                    foreach(var mealDetail in job.Meals)
                    {
                        cmdList.Add(DeleteJDADetailCommand(mealDetail, shift, false));
                    }

                    // Add pending unfilled jobs to unfilled list
                    cmdList.Add(UnfillFilledJobCommand(shift, job));
                }
            }
            // Reset pending filled shifts
            data.ResetPendingUnfilledShifts();
            cmdList.AddRange (auditList.Where(x => x != null));
            return cmdList;
        }

        private ChangeCommand CreateCommand(string type, int priority)
        {
            int? employeeId = null;

            if (m_employee != null)
                employeeId = m_employee.EmployeeID;
                
            if(m_shift != null)
            {
                return new ChangeCommand () {
                    __TYPE = type,
                    SiteID = m_siteId,
                    EmployeeToID = employeeId,
                    ShiftID = m_shift.ShiftID,
                    Priority = priority
                };
            }
            else
            {
                return new ChangeCommand () {
                    __TYPE = type,
                    SiteID = m_siteId,
                    EmployeeToID = employeeId,
                    Priority = priority
                };
            }
        }

        private ChangeCommand AddJobCommand(JobData job, DateTimeOffset offset)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.AddScheduledJob", 4);
            cmd.JobID = job.JobID;
            cmd.JobName = job.JobName;
            cmd.ScheduledJobID = job.ScheduledJobID;

            // calculate offset for current device, if its the same, do not convert
            cmd.ResultingStart = ConvertToSiteTimezone(job.Start, offset);
            cmd.ResultingEnd = ConvertToSiteTimezone (job.End, offset);

            return cmd;
        }

        private ChangeCommand ModifyJobCommand(ShiftData shift, JobData job, DateTimeOffset offset, int? originalEmployeeId = null)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.ModifyScheduledJob", 3);
            cmd.JobID = job.JobID;
            cmd.JobName = job.JobName;
            cmd.ScheduledJobID = job.ScheduledJobID;
            // we do this because the business unit may have a different
            // timezone then our local time. To counter this we store an offset
            // of the time we get down and add this to dates that need to be sent
            // straight to jda
            cmd.ResultingStart = ConvertToSiteTimezone(job.Start, offset);
            cmd.ResultingEnd = ConvertToSiteTimezone(job.End, offset);

            if (originalEmployeeId != null)
            {
                cmd.EmployeeFromID = originalEmployeeId.Value;
                cmd.Priority = 1;
            }

            cmd.Position = CalculateJobPosition (shift, job.ScheduledJobID);

            return cmd;
        }

        private ChangeCommand DeleteJobCommand(JobData job)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.DeleteScheduledJob", 2);
            cmd.JobID = job.JobID;
            cmd.JobName = job.JobName;
            cmd.ScheduledJobID = job.ScheduledJobID;
            return cmd;
        }

        private ChangeCommand AddDetailCommand(ShiftDetailData detail)
        {
            ChangeCommand cmd;

            if(detail.IsMeal)
            {
                cmd = CreateCommand("SiteManager.Classes.Commands.Scheduling.AddMeal", 6);
            }

            else if (detail.IsBreak)
            {
                cmd = CreateCommand("SiteManager.Classes.Commands.Scheduling.AddBreak", 6);
            }
            else
            {
                cmd = CreateCommand("SiteManager.Classes.Commands.Scheduling.AddRole", 6);
                cmd.RoleID = detail.RoleId;
            }

            cmd.ScheduledJobID = detail.ScheduledJobID;
            cmd.ShiftDetailID = detail.ShiftDetailID;
            cmd.ResultingStart = detail.Start;
            cmd.ResultingEnd = detail.End;
            return cmd;
        }

        private ChangeCommand ModifyDetailCommand(ShiftDetailData detail)
        {
            ChangeCommand cmd;

            if(detail.IsMeal)
            {
                cmd = CreateCommand("SiteManager.Classes.Commands.Scheduling.ModifyMeal", 6);
            }

            else if (detail.IsBreak)
            {
                cmd = CreateCommand("SiteManager.Classes.Commands.Scheduling.ModifyBreak", 6);
            }
            else
            {
                cmd = CreateCommand("SiteManager.Classes.Commands.Scheduling.ModifyRole", 6);
                cmd.RoleID = detail.RoleId;
            }

            cmd.ScheduledJobID = detail.ScheduledJobID;
            cmd.ShiftDetailID = detail.ShiftDetailID;
            cmd.ResultingStart = detail.Start;
            cmd.ResultingEnd = detail.End;
            return cmd;
        }


        private ChangeCommand ModifyRoleCommand(ShiftDetailData detail)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.ModifyRole" , 5);
            cmd.ScheduledJobID = detail.ScheduledJobID;
            cmd.ShiftDetailID = detail.ShiftDetailID;
            cmd.ResultingStart = detail.Start;
            cmd.ResultingEnd = detail.End;
            return cmd;
        }

        private ChangeCommand AddRoleCommand(JobData job, JDAShiftDetail role)
        {
            
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.AddRole" , 6);
            cmd.ScheduledJobID = job.ScheduledJobID;
            cmd.ResultingStart = role.Start;
            cmd.ResultingEnd = role.End;
            cmd.RoleID = job.PrimaryRoleID;
            cmd.RoleName = job.Roles.FirstOrDefault(r => r.RoleID == job.PrimaryRoleID).RoleName;
            return cmd;
        }

        private ChangeCommand DeleteDetailCommand(ShiftDetailData detail)
        {
            ChangeCommand cmd; 

            if(detail.IsMeal)
            {
                cmd = CreateCommand( "SiteManager.Classes.Commands.Scheduling.DeleteMeal", 1);
            }

            else if(detail.IsBreak)
            {
                cmd = CreateCommand( "SiteManager.Classes.Commands.Scheduling.DeleteBreak", 1);
            }
            else
            {
                cmd = CreateCommand( "SiteManager.Classes.Commands.Scheduling.DeleteRole", 1);
            }

            cmd.ScheduledJobID = detail.ScheduledJobID;
            cmd.ShiftDetailID = detail.ShiftDetailID;
            return cmd;
        }

        private ChangeCommand DeleteJDADetailCommand(JDAShiftDetail detail, JDAShift shift, bool isBreak)
        {
            var cmd = CreateCommand (isBreak ? 
                "SiteManager.Classes.Commands.Scheduling.DeleteBreak" :
                "SiteManager.Classes.Commands.Scheduling.DeleteMeal", 1
            );
            
            cmd.ScheduledJobID = detail.ScheduledJobID;
            cmd.ShiftDetailID = detail.ShiftDetailID;
            cmd.ShiftID = shift.ShiftID;
            return cmd;
        }

        private ChangeCommand AddShiftWarningCommand(ShiftData shift, int warningID)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.AddScheduledShiftWarning", 7);
            cmd.ScheduledJobID = shift.ShiftID; //< Is this right? copied from older code it may be redundant
            cmd.ShiftID = shift.ShiftID;
            cmd.WarningType = warningID;
            return cmd;
        }

        private ChangeCommand DeleteShiftWarningCommand(ShiftData shift, int warningID)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.DeleteScheduledShiftWarning", 1);
            cmd.ScheduledJobID = shift.ShiftID; //< Is this right? copied from older code it may be redundant
            cmd.WarningType = warningID;
            return cmd;
        }

        private ChangeCommand AddWeeklyWarningCommand(WarningData warning)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.AddWeeklyWarning", 10);
            cmd.WarningType = warning.ExceptionDefinitionID;
            cmd.WeekStartDate = warning.WeekStartBusinessDate.Date;
            if(warning.BusinessDate.HasValue ){ cmd.BusinessDate = warning.BusinessDate.Value.Date; }
            return cmd;
        }

        private ChangeCommand DeleteWeeklyWarningCommand(WarningData warning)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.DeleteWeeklyWarning", 9);
            cmd.WarningType = warning.ExceptionDefinitionID;
            cmd.WeekStartDate = warning.WeekStartBusinessDate.Date;
            if(warning.BusinessDate.HasValue ){ cmd.BusinessDate = warning.BusinessDate.Value.Date; }
            return cmd;
        }

        private ChangeCommand AddDailyWarningCommand(WarningData warning)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.AddDailyWarning", 10);
            cmd.WarningType = warning.ExceptionDefinitionID;
            if(warning.BusinessDate.HasValue ){ cmd.BusinessDate = warning.BusinessDate.Value.Date; }
            return cmd;
        }

        private ChangeCommand DeleteDailyWarningCommand(WarningData warning)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.DeleteDailyWarning", 9);
            cmd.WarningType = warning.ExceptionDefinitionID;
            if(warning.BusinessDate.HasValue ){ cmd.BusinessDate = warning.BusinessDate.Value.Date; }
            return cmd;
        }

        private ChangeCommand DeleteUnfilledShiftCommand(JDAShift shift)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.DeleteUnfilledShift", 2);
            cmd.ScheduledJobID = shift.ShiftID;
            cmd.ShiftID = shift.ShiftID;
            return cmd;
        }

        private ChangeCommand UnfillFilledJobCommand(JDAShift shift, JDAJobAssignment job)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.ModifyScheduledJob", 2);
            cmd.ScheduledJobID = job.ScheduledJobID;
            cmd.ResultingEnd = job.End;
            cmd.ResultingStart = job.Start;
            cmd.EmployeeToID=null;
            cmd.JobName= job.JobName;
            cmd.JobID=job.JobID;
            cmd.ShiftID=shift.ShiftID;
            //Without this, refuses to unfill a filled shift on JDA
            cmd.Position = CalculateJobPosition (shift, job.ScheduledJobID);

            return cmd;
        }

        private ChangeCommand AddScheduleAuditCommand(JDAShift shift, JDAJobAssignment job)
        {
            var cmd = CreateCommand ("SiteManager.Classes.Commands.Scheduling.AddScheduleAudit", 2);
            cmd.ScheduledJobID = job.ScheduledJobID;
            cmd.ResultingEnd = job.End;
            cmd.ResultingStart = job.Start;
            cmd.EmployeeToID=null;
            cmd.JobName= job.JobName;
            cmd.JobID=job.JobID;
            cmd.ShiftID=shift.ShiftID;
            //Without this, refuses to unfill a filled shift on JDA
            cmd.Position = CalculateJobPosition (shift, job.ScheduledJobID);

            return cmd;
        }

        private DateTime ConvertToSiteTimezone(DateTime originalTime, DateTimeOffset offset)
        {
            // convert original to utc
            return DateTime.SpecifyKind(originalTime,DateTimeKind.Unspecified);
        }

        //JDA feature where jobs have a position in a shift, first, middle, last or only. JDA sometimes panics if these aren't set. Don't quite know why.
        private static string CalculateJobPosition (ShiftData shift, int scheduledJobID)
        {
            string position = null;

            if (shift.Jobs != null)
            {
                var orderedJobs = shift.Jobs.OrderBy (job => job.Start).ToList();

                var matchingJob = orderedJobs.FirstOrDefault (job => job.ScheduledJobID == scheduledJobID);

                if (matchingJob != null)
                {
                    int jobIndex = orderedJobs.IndexOf (matchingJob);

                    if (shift.Jobs.Count > 1 && jobIndex >= 0)
                    {
                        if (jobIndex == 0)
                        {
                            position = "f";
                        }
                        else
                        {
                            if (jobIndex == shift.Jobs.Count - 1)
                            {
                                position = "l";
                            }
                            else
                            {
                                position = "m";
                            }
                        }
                    }
                    else
                    {
                        position = "o";
                    }
                }
            }

            return position;
        }

        private static string CalculateJobPosition (JDAShift shift, int ScheduledJobID)
        {
            string position = null;

            var matchingJob = shift.Jobs.FirstOrDefault (job => job.ScheduledJobID == ScheduledJobID);

            if (matchingJob != null)
            {
                int jobIndex = shift.Jobs.IndexOf (matchingJob);

                if (shift.Jobs.Count > 1 && jobIndex >= 0)
                {
                    if (jobIndex == 0)
                    {
                        position = "f";
                    }
                    else
                    {
                        if (jobIndex == shift.Jobs.Count - 1)
                        {
                            position = "l";
                        }
                        else
                        {
                            position = "m";
                        }
                    }
                }
                else
                {
                    position = "o";
                }
            }

            return position;
        }
    }
}
