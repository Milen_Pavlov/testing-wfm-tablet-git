using System;
using System.Collections.Generic;
using Cirrious.CrossCore;
using WFM.Shared.DataTransferObjects.TillAllocation;
using Consortium.Client.Core;
using System.Linq;

namespace WFM.Core
{
    public class ScheduleWarnings
    {
        private static IStringService Localiser { get; set; }

        static ScheduleWarnings()
        {
            Localiser = Mvx.Resolve<IStringService> ();
        }

        public static void AddWeeklyWarnings(EmployeeData employee)
        {
            CheckMinMaxHoursWeek(employee);
            CheckMaxDaysWeek(employee);
            CheckMaxConsecutiveDays(employee);
            CheckShiftMinHoursBetweenDays(employee);
        }

        public static void AddDailyWarnings(EmployeeData employee)
        {
            CheckMinMaxHoursDay(employee);
        }

        public static void AddShiftWarnings(EmployeeData employee, ShiftData data)
        {
            CheckUnavailability(employee, data);
        }

        // Warning messages are linked to the Employee and cant really be encapsulated seperately
        static public string GetWarning(int warningID, DateTime? start, double hours, double days, EmployeeData employee)
        {
            DateTime startDate = start == null ? DateTime.MinValue :  (DateTime) start; // Handles null case for start date
            double dayHours = 0;

            switch (warningID)
            {
                case (int)JDAWarningType.GeneralUnavailability:
                    return string.Format(Localiser.Get("jdawarning_generalunavailability"), startDate.ToString("dddd"));
                case (int)JDAWarningType.TimeOffConflict:
                    return string.Format(Localiser.Get("jdawarning_timeoffconflict"), startDate.ToString("dddd"));
                case (int)JDAWarningType.MaxTimeBeforeRest:
                    return string.Format(Localiser.Get("jdawarning_maxtimebeforerest"), startDate.ToString("dddd"));
                case (int)JDAWarningType.UnassignedRole:
                    return string.Format(Localiser.Get("jdawarning_unassignedrole"), startDate.ToString("dddd"));
                case (int)JDAWarningType.MaxConsecutiveDaysCross:
                    return string.Format(Localiser.Get("jdawarning_maxconsecutivedayscross"),
                        employee.Constraints.MaxConsecutiveDaysAcrossWeeks.GetValueOrDefault(0));
                case (int)JDAWarningType.MinHoursBetweenSplit:
                    return BuildMinHoursBetweenSplitWarning(employee, startDate);
                case (int)JDAWarningType.MaxHoursBetweenSplit:
                    return string.Format(Localiser.Get("jdawarning_maxhoursbetweensplit"), startDate.ToString("dddd"),employee.Constraints.MaxHoursBetweenSplits) ;
                case (int)JDAWarningType.MaxHoursWeek:
                    return BuildMaxHoursPerWeekMessage (hours, employee.Constraints);
                case (int)JDAWarningType.MaxDaysWeek:
                    return string.Format(Localiser.Get("jdawarning_maxdaysweek"), 
                            days - (double)employee.Constraints.MaxDaysPerWeek.Value, 
                            employee.Constraints.MaxDaysPerWeek.Value);
                case (int)JDAWarningType.MinHoursBetweenShifts:
                    var hoursDifference = GetHoursDifferenceForEmployeeDataDay(employee, start);
     
                    return hoursDifference > 0 ? string.Format(Localiser.Get("jdawarning_minhoursbetweenshifts"), startDate.ToString("dddd"), hoursDifference,
                                         startDate.AddDays(-1).ToString("dddd"), employee.Constraints.MinHoursBetweenShifts) : string.Empty;
                    
                case (int)JDAWarningType.MaxHoursDay:
                    double? maxDailyHours = FindMinMaxHoursConstraint(employee, startDate, true);
                    dayHours = FindDayHours(employee, startDate);
                    return string.Format(Localiser.Get("jdawarning_maxhoursday"), 
                        dayHours - maxDailyHours, maxDailyHours, startDate.ToString(Localiser.Get("dd/MM/yyyy")));
                case (int)JDAWarningType.MinHoursDay:
                    double? minDailyHours = FindMinMaxHoursConstraint(employee, startDate, false);
                    dayHours = FindDayHours(employee, startDate);
                    return string.Format(Localiser.Get("jdawarning_minhoursday"), 
                        minDailyHours - dayHours, minDailyHours, startDate.ToString(Localiser.Get("dd/MM/yyyy")));
                case (int)JDAWarningType.MinHoursWeek:
                    return BuildMinHoursPerWeekMessage(hours,employee.Constraints);
                case (int)JDAWarningType.MaxConsecutiveDays:
                    int? maxConsecutiveDays = FindMaxConsecutiveDaysConstraint(employee);
                    int longestConsecutiveDays = FindLargestConsecutiveDays(employee);
                    return string.Format(Localiser.Get("jdawarning_maxconsecutivedays"), 
                        longestConsecutiveDays - maxConsecutiveDays, maxConsecutiveDays);
            }

            return string.Format (Localiser.Get("jdawarning_unknown"), warningID);
        }

        static string BuildMinHoursPerWeekMessage(double hours, JDAScheduleConstraints constraints)
        {
            var minHoursPerWeek = constraints.MinHoursPerWeek.GetValueOrDefault(0);
            var timeOffHours = constraints.TimeOffAdjustmentHours.GetValueOrDefault(0);
            
            var hoursUnder = minHoursPerWeek - ((decimal)hours + timeOffHours);
        
        
            string message = string.Format(Localiser.Get("jdawarning_minhoursweek"), hoursUnder, minHoursPerWeek);
                        
            if(timeOffHours > 0)
            {
                string additionalMessage = string.Format (Localiser.Get ("jdawarning_maxhoursweek_timeoff"), timeOffHours);
                message += additionalMessage;
            }
            
            return message;
        }

        static string BuildMaxHoursPerWeekMessage (double hours, JDAScheduleConstraints constraints)
        {
            decimal timeOffHours = constraints.TimeOffAdjustmentHours.GetValueOrDefault(0);
            decimal maxHoursPerWeek = constraints.MaxHoursPerWeek.GetValueOrDefault (0);
            
            var hoursOver = (decimal)hours + timeOffHours - maxHoursPerWeek;

            string message = string.Format (Localiser.Get ("jdawarning_maxhoursweek"),hoursOver, maxHoursPerWeek);

            if (timeOffHours > 0) 
            {
                string additionalMessage = string.Format (Localiser.Get ("jdawarning_maxhoursweek_timeoff"), timeOffHours);
                message += additionalMessage;
            }

            return message;
        }

        private static string BuildMinHoursBetweenSplitWarning(EmployeeData employee, DateTime onDate)
        {
            List<ShiftData> shifts = new List<ShiftData>(employee.GetShiftsOnDay(onDate.Date));

            if (shifts.Count > 1)
            {
                TimeSpan? minTimeBetweenShifts = null;

                for(int i = 0; i < shifts.Count-1; i++)
                {
                    var shiftDiff = shifts[i].End - shifts[i + 1].Start;

                    if (minTimeBetweenShifts == null || shiftDiff < minTimeBetweenShifts)
                    {
                        minTimeBetweenShifts = shiftDiff;
                    }
                }

                if (minTimeBetweenShifts != null )
                {
                    double constraint = (double)employee.Constraints.MinHoursBetweenSplits.GetValueOrDefault(0);

                    var diff = constraint - minTimeBetweenShifts.Value.TotalHours;

                    return string.Format(Localiser.Get("jdawarning_minhoursbetweensplit"),
                        onDate.ToString("dddd"),diff,constraint);
                }
            }
                
            return string.Format (Localiser.Get("jdawarning_unknown"), (int)JDAWarningType.MinHoursBetweenSplit);
        }

        private static int? FindMaxConsecutiveDaysConstraint(EmployeeData employee)
        {
            if(employee.IsMinor)
            {
                return employee.Constraints.MinorMaxConsecDaysInWeek;
            }
            else
            {
                return employee.Constraints.MaxConsecutiveDays;
            }
        }

        private static int FindLargestConsecutiveDays(EmployeeData employee)
        {
            int longestConsecutiveRun = 0;
            for(int i = 0; i < employee.Days.Count; i++)
            {
                int currentConsecutiveRun = 0; 
                for(int j = i; j< employee.Days.Count; j++)
                {
                    if(employee.Days[j].Hours > 0)
                    {
                        currentConsecutiveRun++;
                    }
                    else
                    {
                        break; // if not working on a day, consecutive run count stops
                    }
                }

                longestConsecutiveRun = currentConsecutiveRun > longestConsecutiveRun ? currentConsecutiveRun : longestConsecutiveRun;
            }

            return longestConsecutiveRun;
        }

        private static double? FindMinMaxHoursConstraint(EmployeeData employee, DateTime startDate, bool max)
        {
            if(!employee.IsMinor) 
            {
                foreach(JDAScheduleConstraints.DailyConstraint dailyConstraint in employee.Constraints.MinMaxHoursByDays)
                {
                    if(startDate.Date == dailyConstraint.Date)
                    {
                        if(max)
                        {
                            return (double?)dailyConstraint.MaxHours;
                        }
                        else
                        {
                            return (double?)dailyConstraint.MinHours;
                        }
                    }
                }
            }
            else
            {
                foreach(JDAScheduleConstraints.DayMaxHoursConstraint dailyConstraint in employee.Constraints.MinorDayMaxHours)
                {
                    if(startDate.Date == dailyConstraint.Date)
                    {
                        if(max)
                        {
                            return (double?)dailyConstraint.MaxHours;
                        }
                    }
                }
            }

            return null; // if nothing matches
        }

        private static void CheckUnavailability(EmployeeData employee, ShiftData data)
        {
            if(!employee.AvailableForShiftTimes(data.Start, data.End))
            {
                data.AddWarning((int)JDAWarningType.GeneralUnavailability);
            }
        }

        private static void CheckMinMaxHoursWeek(EmployeeData employee)
        {
            if(employee.Constraints.MaxHoursPerWeek != null)
            {
                if(employee.Hours > (Double) employee.Constraints.MaxHoursPerWeek)
                {
                    employee.AddWeeklyWarning(
                        CreateWarning(JDAWarningType.MaxHoursWeek, employee.WeekStart, null)
                    );
                }
            }

            if(employee.Constraints.MinHoursPerWeek != null)
            {
                if(employee.Hours < (Double) employee.Constraints.MinHoursPerWeek)
                {
                    employee.AddWeeklyWarning(
                        CreateWarning(JDAWarningType.MinHoursWeek, employee.WeekStart, null)
                    );
                }
            }
        }

        private static void CheckMaxDaysWeek(EmployeeData employee)
        {
            if(employee.Constraints.MaxDaysPerWeek != null)
            {
                if(employee.DaysWorked > (Double) employee.Constraints.MaxDaysPerWeek)
                {
                    employee.AddWeeklyWarning(
                        CreateWarning(JDAWarningType.MaxDaysWeek, employee.WeekStart, null)
                    );
                }
            }
        }
            
        private static double FindDayHours(EmployeeData employee, DateTime dayDate)
        {
            foreach(EmployeeDayData day in employee.Days)
            {
                if(day.DayStart.Date == dayDate.Date)
                {
                    return day.Hours;
                }
            }
            return 0;
        }

        private static void CheckMinMaxHoursDay(EmployeeData employee)
        {
            if(employee != null && employee.Constraints != null)
            {
                if(!employee.IsMinor)
                {
                    foreach(JDAScheduleConstraints.DailyConstraint dailyConstraint in employee.Constraints.MinMaxHoursByDays)
                    {
                        foreach(EmployeeDayData day in employee.Days)
                        {                            
                            if(day.DayStart.Date == dailyConstraint.Date)
                            {
                                if(day.Hours > (Double) dailyConstraint.MaxHours.GetValueOrDefault(decimal.MaxValue))
                                {
                                    employee.DailyWarnings.Add(
                                        CreateWarning(JDAWarningType.MaxHoursDay, employee.WeekStart, dailyConstraint.Date)
                                    );
                                }

                                if(day.Hours < (Double) dailyConstraint.MinHours.GetValueOrDefault(decimal.MinValue))
                                {
                                    employee.DailyWarnings.Add(
                                        CreateWarning(JDAWarningType.MinHoursDay, employee.WeekStart, dailyConstraint.Date)
                                    );
                                }

                                break; // Matching date processed by this point, no need to check further
                            }
                        }
                    }
                }
                else // IsMinor
                {
                    foreach(JDAScheduleConstraints.DayMaxHoursConstraint dayConstraint in employee.Constraints.MinorDayMaxHours)
                    {
                        foreach(EmployeeDayData day in employee.Days)
                        {
                            if(day.DayStart.Date == dayConstraint.Date)
                            {
                                if(day.Hours > (Double) dayConstraint.MaxHours)
                                {
                                    employee.DailyWarnings.Add(
                                        CreateWarning(JDAWarningType.MaxHoursDay, employee.WeekStart, dayConstraint.Date)
                                    );
                                }

                                break; // Matching date processed by this point, no need to check further
                            }
                        }
                    }
                }
            }
        }

        private static void CheckMaxConsecutiveDays(EmployeeData employee)
        {
            int? maxConsecutiveDaysConstraint = FindMaxConsecutiveDaysConstraint(employee);
            if(maxConsecutiveDaysConstraint != null)
            {
                int longestConsecutiveDays = FindLargestConsecutiveDays(employee);
                if(longestConsecutiveDays > (int) maxConsecutiveDaysConstraint)
                {
                    employee.AddWeeklyWarning(
                        CreateWarning(JDAWarningType.MaxConsecutiveDays, employee.WeekStart, null)
                    );
                }
            }
        }

        private static double GetHoursDifferenceForEmployeeDataDay (EmployeeData employee, DateTime? start)
        {
            if(employee == null || !start.HasValue)
            {
                return 0;
            }

            //only check for shifts on day of daily warning received from jda 
            foreach (var day in employee.Days.Where(d => d.DayStart == start))
            {
                foreach (var shift in day.Shifts) 
                {
                    var affectedShiftResult = employee.CanWorkShiftMinHoursBetweenDays(shift);
                    if(affectedShiftResult.CreateWarning)
                    {
                        return affectedShiftResult.HoursDifference;
                    }
                }

                return 0;
            }

            return 0;
        }

        private static void CheckShiftMinHoursBetweenDays(EmployeeData employee)
        { 
            foreach (var day in employee.Days)
            {
                bool dayWarning = false;
                double currentResult = 0;
                foreach (var shift in day.Shifts) 
                {
                    var affectedShiftResult = employee.CanWorkShiftMinHoursBetweenDays(shift);
                    if(affectedShiftResult.CreateWarning)
                    {
                        dayWarning = true;
                        currentResult = affectedShiftResult.HoursDifference;
                        break;
                    }
                }

                if(dayWarning)
                {
                    employee.AddDailyWarning(
                        CreateWarning(JDAWarningType.MinHoursBetweenShifts, employee.WeekStart, day.DayStart, currentResult));
                }
            }
        }

        private static WarningData CreateWarning(JDAWarningType type, DateTime? weekStartBusinessDate, DateTime? businessDate, double hoursDifference = 0)
        {
            WarningData warning = new WarningData();
            warning.ExceptionDefinitionID = (int) type;
            if(weekStartBusinessDate != null)
            {
                warning.WeekStartBusinessDate = (DateTime) weekStartBusinessDate;
            }
            if(businessDate != null)
            {
                warning.BusinessDate = (DateTime) businessDate;
            }

            if(hoursDifference > 0)
            {
                warning.HoursDiffrence = hoursDifference;
            }

            warning.Display = true;
            warning.Edit = EditState.Added;
            return warning;
        }
    }
}