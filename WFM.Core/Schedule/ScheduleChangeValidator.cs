﻿using System;
using System.Linq;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using System.Collections.Generic;

namespace WFM.Core
{
    public class ScheduleChangeValidator
    {
        public class ValidationResponse
        {
            public bool PassesValidation { get; set; }
            public string DisplayMessage { get; set; }

            public ValidationResponse()
            {
                PassesValidation = true;
            }
        }

        /// <summary>
        /// Validates an update shift operation
        /// </summary>
        /// <returns>The update shift.</returns>
        /// <param name="employee">Employee.</param>
        /// <param name="startTime">Start time.</param>
        /// <param name="endTime">End time.</param>
        /// <param name="delta">Delta.</param>
        /// <param name="generatesNewShift">If set to <c>true</c> generates new shift.</param>
        public static ValidationResponse ValidateUpdateShift(EmployeeData employee, DateTime startTime, DateTime endTime, TimeSpan delta)
        {
            return ValidateUpdateShift(employee, startTime, endTime, delta, false);
        }

        private static ValidationResponse ValidateUpdateShift(EmployeeData employee, DateTime startTime, DateTime endTime, TimeSpan delta, bool generatesNewShift)
        {
            ValidationResponse response = new ValidationResponse();

            if(response.PassesValidation)
            {
                bool noTimeOffConflictExists = !employee.TimeOffConflictForShiftTimes(startTime, endTime);

                if (!noTimeOffConflictExists)
                {
                    var timeFormatService = Mvx.Resolve<ITimeFormatService>();
                    response.PassesValidation = false;
                    response.DisplayMessage = string.Format("Time off conflict exists at this time ({0}-{1}).", 
                                            timeFormatService.FormatTime(startTime), 
                                            timeFormatService.FormatTime(endTime));
                }
            }

            return response;
        }

        /// <summary>
        /// Validation logic exclusively applied to a minor
        /// </summary>
        /// <returns>The minor.</returns>
        /// <param name="employee">Employee.</param>
        /// <param name="startTime">Start time.</param>
        /// <param name="endTime">End time.</param>
        /// <param name="delta">Delta.</param>
        /// <param name="isCreate">If set to <c>true</c> is create.</param>
        public static ValidationResponse ValidateMinor(EmployeeData employee, DateTime forDay)
        {
            ValidationResponse response = new ValidationResponse();

            if (employee.IsMinor)
            {
                decimal maxHoursPerWeek = employee.Constraints.MinorMaxHoursPerWeek.GetValueOrDefault(24*7);
                decimal maxHoursPerDay = 24;
                double currentHoursPerDay = 0;

                int maxDaysPerWeek = employee.Constraints.MaxDaysPerWeek.GetValueOrDefault(0);
                maxDaysPerWeek = employee.Constraints.MinorMaxDaysPerWeek.GetValueOrDefault(0) > 0 ? employee.Constraints.MinorMaxDaysPerWeek.GetValueOrDefault(0) : maxDaysPerWeek;


                JDAScheduleConstraints.DayMaxHoursConstraint maxHoursPerDayConstraint = employee.Constraints.MinorDayMaxHours.SingleOrDefault(day => day.Date.Date == forDay.Date);
                EmployeeDayData employeeDay = employee.Days.FirstOrDefault(day => day.DayStart.Date == forDay.Date);

                if (maxHoursPerDayConstraint != null)
                    maxHoursPerDay = maxHoursPerDayConstraint.MaxHours;

                if (employeeDay != null)
                    currentHoursPerDay = employeeDay.Hours;

                bool underMinorMaxHours = (decimal)(employee.Hours) <= maxHoursPerWeek;
                bool underMinorDayMaxHours = true;
                bool minorCanWork = true;

                foreach (var day in employee.Days)
                {
                    if (day.Hours > (double)maxHoursPerDay)
                    {
                        underMinorDayMaxHours = false;
                        break;
                    }
                }

                foreach (var shift in employee.Days.SelectMany(day => day.Shifts))
                {
                    minorCanWork = employee.AvailableForShiftTimes(shift.Start, shift.End, new string[]{ JDAInterval.MinorUnavailabilityType });

                    if (!minorCanWork)
                        break;
                }

                if (!minorCanWork)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = string.Format("Minor cannot work at this time.");
                }
                else if (!underMinorMaxHours)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = string.Format("Minor cannot work more than {0} hours per week.", maxHoursPerWeek);
                }
                else if (!underMinorDayMaxHours)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = string.Format("Minor cannot work more than {0} hours per day.", maxHoursPerDay);
                }
                else if (employee.Constraints.MaxDaysPerWeek > 0 && employee.DaysWorked > maxDaysPerWeek)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = string.Format("Minor cannot work more than {0} days per week.", maxDaysPerWeek);
                }
            }

            return response;
        }

        /// <summary>
        /// Validates a create shift operation
        /// </summary>
        /// <returns>The create shift.</returns>
        /// <param name="employee">Employee.</param>
        /// <param name="shiftStart">Shift start.</param>
        /// <param name="shiftEnd">Shift end.</param>
        public static ValidationResponse ValidateCreateShift(EmployeeData employee, DateTime shiftStart, DateTime shiftEnd)
        {
            ValidationResponse response = ValidateUpdateShift(employee, shiftStart, shiftEnd, shiftEnd - shiftStart, true);

            if(response.PassesValidation)
            {
                bool canWorkSplitShift = employee.CanWorkShiftEvenIfSplit(shiftStart.Date);

                if(!canWorkSplitShift)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = Mvx.Resolve<IStringService>().Get ("validation_cannot_work_split_shift");
                }
            }

            return response;
        }

        /// <summary>
        /// Validates the swap shift.
        /// </summary>
        /// <returns>The validation response.</returns>
        /// <param name="sourceEmployee">Source employee.</param>
        /// <param name="targetEmployee">Target employee.</param>
        /// <param name="shift">Shift being moved</param>
        public static ValidationResponse ValidateSwapShift (ShiftEditMessage message, EmployeeData sourceEmployee, EmployeeData targetEmployee, ShiftData sourceShift)
        {
            ValidationResponse response = new ValidationResponse ();

            //Look for a shift on the target employee at that time, if there isn't one then this will just fail, or if there's more than one we fail for simplicity sake as we aren't 100% which one they want to swap with
            var overlaps = targetEmployee.GetOverlapShifts (message.Start.Value, message.End.Value);
            if (overlaps.Count () == 0) 
            {
                response.PassesValidation = false;
                response.DisplayMessage = string.Empty;
            } 
            else if (overlaps.Count () > 1) 
            {
                response.PassesValidation = false;
                response.DisplayMessage = Mvx.Resolve<IStringService> ().Get ("validation_multiple_swap_shifts");
            } 
            else 
            {
                var targetShift = overlaps.First ();
                //Can sourceEmployee work the targetShift job?
                bool sourceCanWork = sourceEmployee.CanWorkShift (targetShift);
                //Can the targetEmployee work the sourceShift job?
                bool targetCanWork = targetEmployee.CanWorkShift (sourceShift);
                //Check both shifts can fit in each others schedules
                bool sourceFits = !sourceEmployee.DoesOverlapIgnoringID (targetShift.ShiftID, targetShift.Start, targetShift.End, sourceShift.ShiftID);
                bool targetFits = !targetEmployee.DoesOverlapIgnoringID (sourceShift.ShiftID, sourceShift.Start, sourceShift.End, targetShift.ShiftID);

                if (!sourceCanWork || !targetCanWork) 
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = Mvx.Resolve<IStringService> ().Get ("validation_swap_failed_job_assign");
                } 
                else if (!sourceFits || !targetFits) 
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = Mvx.Resolve<IStringService> ().Get ("validation_swap_failed_no_space");
                }
            }

            return response;
        }

        /// <summary>
        /// Validates a reassign operation
        /// </summary>
        /// <returns>The reassign shift.</returns>
        /// <param name="employee">Employee.</param>
        /// <param name="shift">Shift.</param>
        /// <param name="shiftId">Shift identifier.</param>
        public static ValidationResponse ValidateReassignShift(EmployeeData employee, ShiftData shift, int shiftId)
        {
            ValidationResponse response = ValidateCreateShift(employee, shift.Start, shift.End);

            if(response.PassesValidation)
            {
                bool doesFit = !employee.DoesOverlap(shiftId, shift.Start, shift.End);
                bool canAssign = employee.CanWorkShift(shift);

                if (!doesFit)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = Mvx.Resolve<IStringService>().Get ("validation_no_room_for_job");
                }
                else if (!canAssign)
                {
                    response.PassesValidation = false;
                    response.DisplayMessage = Mvx.Resolve<IStringService>().Get ("validation_cannot_work_jobs");
                }
            }

            return response;
        }

        public static ValidationResponse ValidateShiftLength(EmployeeData employee, ShiftData shift)
        {
            return ValidateShiftLength(employee, shift.Start, shift.End, shift.ShiftID);
        }

        public static ValidationResponse ValidateShiftLength(EmployeeData employee, DateTime start, DateTime end, int shiftId)
        {
            ValidationResponse response = new ValidationResponse();

            if(!response.PassesValidation)
            {
                return response;
            }

            var shifts = new List<ShiftData>();
            foreach(var day in employee.Days)
            {
                shifts.AddRange(day.Shifts.Where(x=> x.ShiftID != shiftId));
            }
            shifts.AddRange(employee.LastDayPreviousWeek.Shifts);

            var continuousStart = GetContinuousStart(start, shifts);
            var continuousEnd = GetContinuousEnd(end, shifts);
            var essConfig = Mvx.Resolve<IESSConfigurationService>();
            if((continuousEnd - continuousStart).TotalHours >= essConfig.MaxShiftLengthHours)
            {
                response.PassesValidation = false;
                response.DisplayMessage = string.Format(Mvx.Resolve<IStringService>().Get("shift_drop_error_shift_length_body"), essConfig.MaxShiftLengthHours);
            }

            return response;
        }

        static private DateTime GetContinuousStart(DateTime start, List<ShiftData> shifts)
        {
            foreach(var shift in shifts)
            {
                if(shift.End == start)
                {
                    return GetContinuousStart(shift.Start, shifts);
                }
            }

            return start;
        }

        static private DateTime GetContinuousEnd(DateTime end, List<ShiftData> shifts)
        {
            foreach(var shift in shifts)
            {
                if(shift.Start == end)
                {
                    return GetContinuousEnd(shift.End, shifts);
                }
            }

            return end;
        }
    }
}