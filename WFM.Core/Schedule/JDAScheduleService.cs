﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Consortium.Client.Core;
using Siesta;
using System.Threading;

namespace WFM.Core.Schedule
{
    [ConsortiumService]
    public class JDAScheduleService : JDAService
    {
        private readonly IESSConfigurationService m_settings;
        private readonly RestClient m_client;
        private readonly SessionData m_data;

        public const int DEFAULT_SHIFT_LENGTH_HOURS = 4;

        public const int JDA_BUDGET_ID = 1000012;
        public const int JDA_METRIC_ID = 1000047;

        private int Site
        {
            get
            {
                return m_data.Site.SiteID;
            }
        }

        private IAccountingHelper m_accountingHelper;
        public IAccountingHelper AccountingHelper
        {
            get { return m_accountingHelper; }
            private set { m_accountingHelper = value; }
        }

        public IAccountingConfiguration AccountingConfig
        {
            get
            {
                return new AccountingConfiguration()
                {
                    PeriodFirstDay = m_settings.PeriodFirstDay,
                    PeriodFirstMonth = m_settings.PeriodFirstMonth,
                    StartDay =  (DayOfWeek)m_settings.StartDay,
                };
            }
        }

        public JDAScheduleService(
            SessionData data,
            IESSConfigurationService settings, 
            RestClient client)
        {
            m_settings = settings;
            m_data = data;
            m_client = client;

            AccountingHelper = new AccountingHelper (AccountingConfig);
        }

        public async Task<Result<List<ScheduleData>>> GetScheduleDataForPeriod(DateTime startDate, DateTime endDate, CancellationToken token)
        {
            Dictionary<DateTime, Task<RestReply<ScheduleDataReply>>> scheduleRequests = new Dictionary<DateTime,Task<RestReply<ScheduleDataReply>>>();
            
            DateTime first = startDate.Date;
            DateTime last = endDate.Date;

            DateRange range = new DateRange (first, last, TimeSpan.FromDays (7), true, false);

            foreach (DateTime date in range)
            {
                scheduleRequests.Add(date,CreateScheduleRequest(date, token));
            }

            await Task.WhenAll (scheduleRequests.Values);

            //Merge all into a single schedule data object (duplicates might exist)
            Result<List<ScheduleData>> scheduleResult = new Result<List<ScheduleData>>();
            scheduleResult.Object = new List<ScheduleData> ();

            foreach (KeyValuePair<DateTime, Task<RestReply<ScheduleDataReply>>> requestTask in scheduleRequests)
            {
                var result = ConstructResultObject<ScheduleData,ScheduleDataReply>(requestTask.Value);

                if (result.Success)
                {                    
                    scheduleResult.Success = true;

                    JDAScheduleData rawSchedule = requestTask.Value.Result.Object.data;
                    Dictionary<int, JDAEmployeeInfo> employeeInfoLookup = new Dictionary<int, JDAEmployeeInfo> ();
                    if (rawSchedule.EmployeeInfo != null)
                    {
                        foreach (var e in rawSchedule.EmployeeInfo)
                            employeeInfoLookup.Add(e.EmployeeID, e);
                    }

                    scheduleResult.Object.Add(new ScheduleData(rawSchedule, employeeInfoLookup, null));
                }
                else
                {
                    //Populate with the first error and quit
                    scheduleResult.Success = false;
                    scheduleResult.Message = result.Message;
                    scheduleResult.Code = result.Code;
                    break;
                }
            }

            return scheduleResult;
        }

        Task<RestReply<ScheduleDataReply>> CreateScheduleRequest(DateTime forDate, CancellationToken token)
        {
            DateTime fromDate = DateTime.SpecifyKind(forDate.Date, DateTimeKind.Unspecified);
            return m_client.Post<ScheduleDataReply>(new ScheduleDataRequest()
                {
                    siteID = Site,
                    startDay = fromDate,
                    budgetID = JDA_BUDGET_ID,
                    metricID = JDA_METRIC_ID,
                    filterIDs = new int[0],
                    filter = string.Empty
                }, token);
        }
    }
}