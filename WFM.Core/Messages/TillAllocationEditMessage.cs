﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
	public class TillAllocationEditMessage : MvxMessage
	{
		public enum Operation
		{
			ReassignTill,
		};

		public Operation Op { get; set; }

		public int TillAllocationID { get; set; }
		public int TillID { get; set; }

		public TillAllocationEditMessage (object sender) : base(sender)
		{
		}
	}
}

