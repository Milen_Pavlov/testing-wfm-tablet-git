﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class WeeklyOverviewDateMessage : MvxMessage
    {
        public DateTime WeekStarting { get; set; }
        
        public WeeklyOverviewDateMessage (object sender) : base(sender)
        {
        }
    }

    public class WeeklyOverviewTabLoaded : MvxMessage
    {
        public WeeklyOverviewTabLoaded (object sender) : base(sender)
        {
        }
    }
}

