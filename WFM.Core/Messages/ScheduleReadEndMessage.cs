﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.ComponentModel;

namespace WFM.Core
{
    public class ScheduleReadEndMessage : MvxMessage
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public bool IsSaving { get; set; }
        public bool SessionTimeout { get; set; }
        public bool NoData { get; set; }

        public ScheduleReadEndMessage(object sender, bool success, string message) : base(sender)
        {
            Success = success;
            Message = message;
            SessionTimeout = false;
        }
    }
}
