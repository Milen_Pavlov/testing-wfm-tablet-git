﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.ComponentModel;

namespace WFM.Core
{
    public class TillWriteEndMessage : MvxMessage
    {
        public bool Success { get; set; }

        public TillWriteEndMessage(object sender, bool success) : base(sender)
        {
            Success = success;
        }
    }
}
