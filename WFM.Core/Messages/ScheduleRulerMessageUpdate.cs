﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class ScheduleRulerMessageUpdate : MvxMessage
    {
        public DateTime AdjustedStart { get; set; }
        public DateTime AdjustedEnd { get; set; }

        public ScheduleRulerMessageUpdate(object sender) : base(sender)
        {
        }
    }
}

