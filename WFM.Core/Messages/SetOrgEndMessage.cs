﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class SetOrgEndMessage : MvxMessage
    {
        public bool Success { get; set; }
        public bool SessionTimeout { get; set; }

        public SetOrgEndMessage(object sender, bool success) : base(sender)
        {
            Success = success;
            SessionTimeout = false;
        }
    }
}
