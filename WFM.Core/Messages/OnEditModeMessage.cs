﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class OnEditModeMessage : MvxMessage
    {
        public bool Editing { get; set; }

        public OnEditModeMessage(object sender) : base(sender)
        {
        }
    }
}

