﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class PendingApprovalsUpdatedMessage: MvxMessage
    {
        public int Total
        {
            get;
            set;
        }

        public PendingApprovalsUpdatedMessage (object sender) : base(sender)
        {
        }
    }
}