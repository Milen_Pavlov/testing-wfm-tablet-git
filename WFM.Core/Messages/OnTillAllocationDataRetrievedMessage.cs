﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class OnTillAllocationDataRetrievedMessage : MvxMessage
    {
        public bool Success
        {
            get;
            set;
        }

        public OnTillAllocationDataRetrievedMessage(object sender) : base(sender)
        {
        }
    }
}

