﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class ShiftEditMessage : MvxMessage
    {
        public enum Operation
        {
            CreateShift,
            CreateDefaultShift,
            UpdateShift,
            ReplaceShift,
            DeleteShift,
            DeleteUnsavedShift,
            CreateJob,
            DeleteJob,
            Undo,
            ReassignShift,
            UnfillFilledShift,
            AssignUnfilledShift,
            UndoFilledShift, //The undo logic requires significantly different logic to just unfilling a shift
            SwapShift,
        };

        public Operation Op { get; set; }
        public int ShiftID { get; set; }
        public int JobID { get; set; }
        public int EmployeeID { get; set; }

        /// <summary>
        /// Used when the shift is being re-assigned through the Re-Assign shift operation
        /// </summary>
        /// <value>The new employee I.</value>
        public int NewEmployeeID { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public ShiftData UpdateShift { get; set; }
        /// <summary>
        /// If the caller has modified the shift state in the current shift list, they need to set this in order for the undo handler to 
        /// correctly roll back the edit message. If this is not set then the schedule service will use the previous state of the shift to 
        /// </summary>
        /// <value>The state of the previous shift.</value>
        public ShiftData PreviousShiftState { get; set; }

        public bool RecalculateDetails { get; set; }
        public bool StoreInUndoStack { get; set; }

        /// <summary>
        /// Allow this message to be auto submitted even if edit schedule is turned off
        /// If EditSchedule is allowed, it will just queue up like a normal edit.
        /// </summary>
        /// <value>True if this message is auto-submitted if EditSchedule is false</value>
        public bool AllowAutoSubmission { get; set; }

        public JDAShift UnfilledShiftToFill { get; set; }

        public ShiftEditMessage(object sender) : base(sender)
        {
            RecalculateDetails = true;
            StoreInUndoStack = true;
        }
    }
}