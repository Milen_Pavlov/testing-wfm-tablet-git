﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class PendingTORUpdatedMessage: MvxMessage
    {
        public int Total
        {
            get;
            set;
        }

        public PendingTORUpdatedMessage (object sender) : base(sender)
        {
        }
    }
}