﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class StartTimeChangedMessage : MvxMessage
    {
        public int StartTime { get; set; }

        public StartTimeChangedMessage(object sender) : base(sender)
        {

        }
    }
}
