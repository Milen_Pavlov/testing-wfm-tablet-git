﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.ComponentModel;

namespace WFM.Core
{
    public class ScheduleWriteEndMessage : MvxMessage
    {
        public bool ResponseReceived { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        public ScheduleWriteEndMessage(object sender, bool success, string message) : base(sender)
        {
            Success = success;
            Message = message;
        }
    }
}
