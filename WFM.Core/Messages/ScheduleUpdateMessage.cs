﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class ScheduleUpdateMessage : MvxMessage
    {
        public bool UpdateSchedule { get; set; }
        public bool UpdateHeaders { get; set; }
        public bool UpdateTotals { get; set; }
        public bool UpdateWarnings { get; set; }
        public int? ScrollToEmployeeId { get; set; }
        public bool RevertChanges { get; set; }

        public ScheduleUpdateMessage(object sender) : base(sender)
        {
        }
    }
}
