﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Core
{
    public class ScheduleRefreshMessage : MvxMessage
    {
        public ScheduleRefreshMessage(object sender) : base(sender)
        {
        }
    }
}
