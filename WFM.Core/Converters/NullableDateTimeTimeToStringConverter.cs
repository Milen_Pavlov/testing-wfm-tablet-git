﻿using System;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class NullableDateTimeTimeToStringConverter: MvxValueConverter<DateTime?, string>
    {
        private ITimeFormatService m_timeFormatService;
        
        protected override string Convert(DateTime? value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value == null)
            {
                return String.Empty;
            }
            else
            {
                if(parameter != null && parameter is string)
                {
                    return value.Value.ToString((string)parameter);
                }
                else
                {
                    if (m_timeFormatService == null)
                        m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
                        
                    return m_timeFormatService.FormatTime(value.Value);        
                }
            }
        }
    }
}