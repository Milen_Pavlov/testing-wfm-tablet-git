﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class EmptyStringConverter : MvxValueConverter<string, string>
    {
        protected override string Convert(string data, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (string.IsNullOrEmpty ((string)data))
                return "N/A";
            else
                return data;
        }
    }
}

