﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class StringBoolConverter : MvxValueConverter<bool, string>
    {
        protected override string Convert(bool val, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (val)
                return "Yes";
            else
                return "No";
        }
    }
}

