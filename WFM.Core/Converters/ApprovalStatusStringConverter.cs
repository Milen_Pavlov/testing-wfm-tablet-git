﻿using System;
using Cirrious.CrossCore.Converters;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class ApprovalStatusStringConverter : MvxValueConverter<ApprovalStatus, string>
    {
        private IStringService m_localiser;

        protected override string Convert(ApprovalStatus data, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (m_localiser == null)
                m_localiser = Mvx.Resolve<IStringService> ();

            switch (data)
            {
                case ApprovalStatus.None:
                    return m_localiser.Get ("approvalstatus_none");
                case ApprovalStatus.Pending:
                    return m_localiser.Get ("approvalstatus_pending");
                case ApprovalStatus.AwaitingManager:
                    return m_localiser.Get ("approvalstatus_awaitingmanager");
                case ApprovalStatus.ManagerApproved:
                    return m_localiser.Get ("approvalstatus_managerapproved");
                case ApprovalStatus.Denied:
                    return m_localiser.Get ("approvalstatus_denied");
                case ApprovalStatus.SenderCancelled:
                    return m_localiser.Get ("approvalstatus_sendercancelled");
                case ApprovalStatus.Cancelled:
                    return m_localiser.Get ("approvalstatus_cancelled");
                default:
                    return m_localiser.Get ("approvalstatus_none");
            }
        }
    }
}

