﻿using System;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Converters;
using Consortium.Client.Core;

namespace WFM.Core
{
    public class NullableDateTimeDateToStringConverter: MvxValueConverter<DateTime?, string>
    {
        private IStringService m_stringService;
        
        protected override string Convert(DateTime? value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value == null)
            {
                return String.Empty;
            }
            else
            {
                if(parameter != null && parameter is string)
                {
                    return value.Value.ToString((string)parameter);
                }
                else
                {
                    if (m_stringService == null)
                        m_stringService = Mvx.Resolve<IStringService>();
                        
                    return value.Value.ToString(m_stringService.Get("{0:dd/MM/yy"));        
                }
            }
        }
    }
}