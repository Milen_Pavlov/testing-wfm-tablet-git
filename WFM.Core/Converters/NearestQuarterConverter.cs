﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class NearestQuarterConverter : MvxValueConverter<double, double>
    {
        protected override double Convert(double data, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return RoundToQuarter(data);
        }
        
        public static double RoundToQuarter(double data)
        {
            return Math.Round (data * 4) / 4;
        }
    }
}

