﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class ToStringValueConverter<T> : MvxValueConverter<T, string>
    {
        protected override string Convert(T value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Format (parameter as string, value);
        }
    }
}