﻿using System;
using Cirrious.CrossCore.Converters;
using Cirrious.CrossCore;
using System.Globalization;

namespace WFM.Core
{
    public class StringVisibleConverter : MvxValueConverter<string, bool>
    {
        protected override bool Convert (string value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!string.IsNullOrEmpty (value))
                return false;
            else
                return true;
        }
    }
}

