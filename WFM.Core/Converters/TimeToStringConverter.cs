﻿using System;
using System.Diagnostics.Contracts;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class TimeToStringConverter : MvxValueConverter<DateTime, string>
    {        
        private ITimeFormatService m_timeFormatService;
        
        protected override string Convert(DateTime value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (m_timeFormatService == null)
                m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
                
            return m_timeFormatService.FormatTime(value);        
        }
    }
}

