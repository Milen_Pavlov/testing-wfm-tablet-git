﻿using System;
using Cirrious.CrossCore.Converters;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Core
{
    public class TimeOffStatusStringConverter : MvxValueConverter<TimeOffStatus, string>
    {
        private IStringService m_localiser;

        protected override string Convert(TimeOffStatus value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (m_localiser == null)
                m_localiser = Mvx.Resolve<IStringService> ();

            switch (value)
            {
                case TimeOffStatus.None:
                    return m_localiser.Get ("approvalstatus_none");
                case TimeOffStatus.Pending:
                    return m_localiser.Get ("approvalstatus_pending");
                case TimeOffStatus.Approved:
                    return m_localiser.Get ("approvalstatus_managerapproved");
                case TimeOffStatus.Denied:
                    return m_localiser.Get ("approvalstatus_denied");
                case TimeOffStatus.Deleted:
                    return m_localiser.Get ("approvalstatus_deleted");
                default:
                    return m_localiser.Get ("approvalstatus_none");
            }
        }
    }
}

