﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.Constants
{
    public class MessageSeverity
    {
        public const string Success = "Success";
        public const string Info = "Info";
        public const string Warning = "Warning";
        public const string Error = "Error";
    }
}
