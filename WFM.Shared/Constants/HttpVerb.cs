﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.Constants
{
    public class HttpVerb
    {
        public const string Get = "GET";
        public const string Post = "POST";
        public const string Delete = "DELETE";
    }
}
