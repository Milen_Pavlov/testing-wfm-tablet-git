﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.Constants
{
    public static class TillStatusKey
    {
        public const string Open = "Open";
        public const string Unused = "Unused";
        public const string OutOfOrder = "OutOfOrder";
    }
}