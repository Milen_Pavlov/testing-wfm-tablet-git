﻿using Consortium.Model.Engine.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.Extensions
{
    // Use extension methods to keep DTO behavioursless
    public static class RequestExtensions
    {
        public static Pager _CreatePager(this Request source)
        {
            return new Pager(source.Skip, source.Take);
        }
    }
}
