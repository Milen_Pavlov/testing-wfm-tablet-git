﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.Extensions
{
    public static class ObjectExtensions
    {
        public static T _NewIfNull<T>(this T source)
            where T : class, new()
        {
            return (source ?? new T());
        }
    }
}
