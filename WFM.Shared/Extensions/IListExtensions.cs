﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.Extensions
{
    public static class IListExtensions
    {
        public static IList<T> _NewIfNull<T>(this IList<T> source)
        {
            return (source ?? new List<T>());
        }
    }
}
