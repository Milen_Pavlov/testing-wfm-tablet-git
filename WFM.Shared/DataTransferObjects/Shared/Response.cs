﻿using WFM.Shared.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Extensions;

namespace WFM.Shared.DataTransferObjects.Shared
{
    public class Response
    {
        public int? NumOfItemsOverAllPages { get; set; }
        public Message[] Messages { get; set; }
        public bool Success { get; set; }
    }
}
