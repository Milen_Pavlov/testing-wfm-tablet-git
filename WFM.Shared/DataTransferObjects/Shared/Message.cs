﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Shared
{
    public class Message
    {
        public string Severity { get; set; }
        public string FriendlyMessage { get; set; }
        public string TechnicalDetails { get; set; }
    }
}
