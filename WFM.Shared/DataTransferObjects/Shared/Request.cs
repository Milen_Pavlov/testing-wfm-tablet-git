﻿using Consortium.Model.Engine.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Shared
{
    public class Request
    {
        public string JDAUsername { get; set; }
        public string JDAPassword { get; set; }

        public string JdaSessionId { get; set; }

        public int Skip { get; set; }

        int m_take = 1000;
        public int Take
        {
            get
            {
                return m_take;
            }
            set
            {
                m_take = value;
            }
        }
    }
}
