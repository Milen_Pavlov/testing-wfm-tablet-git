﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Resources
{
    [ConsortiumRoute("/resources/get", HttpVerb.Get, "gets the resources for a given site and date")]
    public class GetResourceDataRequest
    {
        public int SiteId { get; set; }
        public DateTime Date { get; set; }
    }
}
