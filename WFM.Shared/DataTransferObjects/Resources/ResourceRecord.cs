﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Resources
{
    public class ResourceRecord
    {
        public int Id { get; set; }
        public int Site { get; set; }
        public string JsonData { get; set; }
        public DateTime Date { get; set; }
    }
}
