﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Resources
{
    [ConsortiumRoute("/resources/set", "POST", "sets the resources for a given site and date")]
    public class SetResourceDataRequest
    {
        public int SiteId { get; set; }
        public string Username { get; set; }
        public DateTime Date { get; set; }
        public string Json { get; set; }
    }
}
