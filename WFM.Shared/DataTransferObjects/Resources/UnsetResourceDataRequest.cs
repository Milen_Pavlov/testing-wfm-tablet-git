﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Resources
{
    [ConsortiumRoute("/resources/unset",HttpVerb.Post,"unsets the resources for a given site and date")]
    public class UnsetResourceDataRequest
    {
        public int SiteId { get; set; }
        public DateTime Date { get; set; }
    }
}
