﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Resources
{
    public class UnsetResourceDataResponse
    {
        public bool Success { get; set; }
    }
}
