﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Attendance
{
    public class ReadAttendanceResponse : Response
    {
        public IEnumerable<int> ShiftsTaken { get; set; }
    }
}