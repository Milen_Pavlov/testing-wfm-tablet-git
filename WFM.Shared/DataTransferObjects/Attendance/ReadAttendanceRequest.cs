﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Attendance
{
    [ConsortiumRoute("/sites/{SiteId}/attendance?SID={SID}", HttpVerb.Get, "Reads attendance items")]
    public class ReadAttendanceRequest
    {
        public int SiteId { get; set; }
        public string SID { get; set; }
    }
}
