﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Attendance
{
    [ConsortiumRoute("/schedule/{SiteId}/attendance/set", HttpVerb.Post, "Writes attendance records to the repository")]
    public class SetAttendanceRequest
    {
        public int SiteId { get; set; }

        public int[] ShiftIdsSet { get; set; }
        public int[] ShiftIdsUnSet { get; set; }
    }
}