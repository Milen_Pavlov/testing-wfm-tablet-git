﻿using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    public class ReadAislesBySiteResponse : Response
    {
        public Aisle[] Aisles { get; set; }

        public class Aisle
        {
            public int AisleId;
            public string AisleName;
            public int Order;
        }
    }
}
