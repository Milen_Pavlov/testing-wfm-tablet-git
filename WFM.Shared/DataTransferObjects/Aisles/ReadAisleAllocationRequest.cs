﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    [ConsortiumRoute("/sites/{SiteId}/aisles/allocations/{dateString}", HttpVerb.Get, "Reads all the allocations for a site over a specific week")]
    public class ReadAisleAllocationRequest : Request
    {
        public int SiteId { get; set; }
        public string dateString { get; set; }
    }
}