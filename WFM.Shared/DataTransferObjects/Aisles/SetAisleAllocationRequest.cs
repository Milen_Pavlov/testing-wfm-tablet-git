﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    [ConsortiumRoute("/sites/{SiteId}/aisles/allocations/{WeekString}", HttpVerb.Post, "Sets the allocations for a site over a specific week")]
    public class SetAisleAllocationRequest : Request
    {
        public int SiteId { get; set; }
        public string WeekString{ get; set; }
        public Aisle[] Aisles { get; set; }

        public class Aisle
        {
            public int AisleId;

            public Dictionary<DayOfWeek, Allocation[]> Allocations;
        }

        public class Allocation
        {
            public int EmployeeId;
            public string EmployeeName;
        }
    }
}