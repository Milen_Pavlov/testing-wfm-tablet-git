﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    public class ReadAisleAllocationResponse : Response
    {
        public DateTime WeekStarting { get; set; }
        public Aisle[] Aisles { get; set; }

        public class Aisle
        {
            public int AisleId;
            public string AisleName;
            public int Order;
            
            public Dictionary<DayOfWeek, Allocation[]> Allocations;
        }

        public class Allocation
        {
            public string EmployeeId;
            public string EmployeeName;
        }
    }
}