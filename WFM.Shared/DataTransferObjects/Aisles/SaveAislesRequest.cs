﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    [ConsortiumRoute("/sites/{SiteId}/aisles", HttpVerb.Post, "Creates or updates the aisles for a site")]
    public class SaveAislesRequest : Request
    {
        public int SiteId { get; set; }
        public Aisle[] Aisles { get; set; }

        public class Aisle
        {
            public int AisleId;
            public string AisleName;
            public int Order;
        }
    }
}
