﻿using System;
using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    // Todo: Replace with call to set replenishment times once they exist
    //[ConsortiumRoute("/sites/{SiteId}/aisles/allocations/{WeekString}", HttpVerb.Post, "Sets the allocations for a site over a specific week")]
    public class SaveReplenishmentTimeRequest : Request
    {
        public int SiteId { get; set; }
        public double Start { get; set; }
        public double Duration { get; set; }
    }
}

