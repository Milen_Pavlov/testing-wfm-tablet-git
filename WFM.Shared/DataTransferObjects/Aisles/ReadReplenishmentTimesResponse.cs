﻿using System;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared
{
    public class ReadReplenishmentTimesResponse : Response
    {
        double StartTime { get; set; }
        double Duration { get; set; }
    }
}

