﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    [ConsortiumRoute("/sites/{SiteId}/aisles", HttpVerb.Get, "Reads all the aisles for a site")]
    public class ReadAislesBySiteRequest : Request
    {
        public int SiteId { get; set; }
    }
}