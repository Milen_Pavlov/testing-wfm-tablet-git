﻿using System;
using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Aisles
{
    // Todo: Replace with call to get replenishment times once they exist
    //[ConsortiumRoute("/sites/{SiteId}/aisles/allocations/{dateString}", HttpVerb.Get, "Reads all the allocations for a site over a specific week")]
    public class ReadReplenishmentTimesRequest : Request
    {
        public int SiteId { get; set; }
    }
}

