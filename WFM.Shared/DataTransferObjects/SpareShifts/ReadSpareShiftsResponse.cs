﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.SpareShifts
{
    public class ReadSpareShiftsResponse : Response
    {
        public DateTime WeekStarting { get; set; }
        public int DepartmentId { get; set; }

        public Dictionary<DayOfWeek, IEnumerable<SpareShift>> SpareShifts { get; set; }

        public class SpareShift
        {
            public TimeSpan ShiftStart { get; set; }
            public TimeSpan ShiftDuration { get; set; }

            public int JobId { get; set; }
            public string JobName { get; set; }
        }
    }
}