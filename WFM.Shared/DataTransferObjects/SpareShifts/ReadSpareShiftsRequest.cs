﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.SpareShifts
{
    [ConsortiumRoute("/sites/{SiteId}/SpareShifts/{JDADepartmentId}/{DateString}", HttpVerb.Get, "Reads spare shifts for a department over a specific week")]
    public class ReadSpareShiftsRequest : Request
    {
        public int SiteId { get; set; }
        public int JDADepartmentId { get; set; }
        public String DateString { get; set; }
    }
}