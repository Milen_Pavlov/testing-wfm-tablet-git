﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.SpareShifts
{
    [ConsortiumRoute("/sites/{SiteId}/SpareShifts/{JDADepartmentId}/{DateString}", HttpVerb.Post, "Sets spare shifts for a department over a specific week")]
    public class SetSpareShiftsRequest : Request
    {
        public int SiteId { get; set; }
        public String DateString { get; set; }
        public int JDADepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public Dictionary<DayOfWeek, IEnumerable<SpareShift>> SpareShifts { get; set; }

        public class SpareShift
        {
            public TimeSpan ShiftStart { get; set; }
            public TimeSpan ShiftDuration { get; set; }

            public int JobId { get; set; }
            public string JobName { get; set; }
        }
    }
}