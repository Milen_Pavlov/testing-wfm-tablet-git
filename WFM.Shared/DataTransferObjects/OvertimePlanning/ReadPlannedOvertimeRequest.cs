﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.OvertimePlanning
{
    [ConsortiumRoute("/sites/{SiteId}/plannedovertime/{DateString}", HttpVerb.Get, "Reads planned overtime site over a specific week")]
    public class ReadPlannedOvertimeRequest : Request
    {
        public int SiteId { get; set; }
        public String DateString { get; set; }
    }
}