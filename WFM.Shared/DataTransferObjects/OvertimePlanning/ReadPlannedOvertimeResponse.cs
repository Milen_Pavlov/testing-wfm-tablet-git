﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.OvertimePlanning
{
    public class ReadPlannedOvertimeResponse : Response
    {
        public DateTime WeekStarting { get; set; }
        public Department[] Departments { get; set; }

        public class Department
        {
            public int DepartmentId;
            public string DepartmentName;

            public Dictionary<DayOfWeek, float> Hours;
        }
    }
}