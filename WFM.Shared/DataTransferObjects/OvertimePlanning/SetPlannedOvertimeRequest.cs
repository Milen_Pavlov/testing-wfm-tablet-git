﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.OvertimePlanning
{
    [ConsortiumRoute("/sites/{SiteId}/plannedovertime/{DateString}", HttpVerb.Post, "Sets the planned overtime for a site over a specific week")]
    public class SetPlannedOvertimeRequest : Request
    {
        public int SiteId { get; set; }
        public String DateString { get; set; }
        public Department[] Departments { get; set; }

        public class Department
        {
            public int DepartmentId;
            public string DepartmentName;

            public Dictionary<DayOfWeek, float> Hours;
        }
    }
}