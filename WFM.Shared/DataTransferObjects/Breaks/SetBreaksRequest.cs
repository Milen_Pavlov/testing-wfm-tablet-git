﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Breaks
{
    [ConsortiumRoute("/schedule/breaks/set", HttpVerb.Post, "Writes break items to the repository")]
    public class SetBreaksRequest
    {
        public List<BreakRecord> Breaks { get; set; }
    }
}
