﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace WFM.Shared.DataTransferObjects.Breaks
{
    public class ReadBreaksResponse
    {
        public IList<BreakRecord> Breaks { get; set; }
    }
}