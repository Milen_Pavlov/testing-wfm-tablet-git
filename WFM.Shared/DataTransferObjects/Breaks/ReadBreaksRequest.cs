﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Breaks
{
    [ConsortiumRoute("/schedule/breaks/get", HttpVerb.Post, "Reads break items")]
    public class ReadBreaksRequest
    {
        public int SiteId { get; set; }
        public List<int> ShiftIDs;
    }
}
