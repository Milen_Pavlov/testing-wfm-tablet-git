﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.OvertimeAgreed
{
    public class OvertimeAgreedRecord
    {
        public double? OvertimeAgreedAmount { get; set; }
    }
}
