﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.OvertimeAgreed
{
    [ConsortiumRoute("/overtimeagreed/get/forsiteandweek", HttpVerb.Get, "Gets the overtime agreed for a specific week and site")]
    public class ReadOvertimeAgreedRequest
    {
        public int SiteId { get; set; }
        public DateTime Week { get; set; }
    }
}
