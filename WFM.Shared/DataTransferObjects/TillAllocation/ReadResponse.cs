﻿using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
    public class ReadResponse : Response
    {
        public Model.TillAllocation[] TillAllocations { get; set; }
    }
}
