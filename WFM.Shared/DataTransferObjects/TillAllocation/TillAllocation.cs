﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.TillAllocation.Model
{
    public class TillAllocation
    {
        public int TillAllocationId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public int? TillId { get; set; }
        public int? StampedTillId { get; set; }
        public string TillNumber { get; set; }
        public bool CheckoutSupport { get; set; }
        public string TillTypeKey { get; set; }
        public string TillStatusKey { get; set; }
        public int? TillOpeningSequence { get; set; }
        public int? TillDisplaySequence { get; set; }
        public TillAllocationTillWindow[] TillWindows { get; set; }
    }

    public class TillAllocationTillWindow
    {
        public int TillAllocationTillWindowId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
}
