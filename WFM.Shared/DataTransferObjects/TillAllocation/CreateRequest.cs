﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
	[ConsortiumRoute("/sites/{SiteId}/allocations/create", HttpVerb.Post, "Creates till allocations for the given site and dates")]
    public class CreateRequest : Request
    {
        public int SiteId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
