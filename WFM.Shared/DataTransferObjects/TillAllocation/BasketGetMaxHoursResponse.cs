﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
    public class BasketGetMaxHoursResponse
    {
        public int? MaxHours { get; set; }
    }
}
