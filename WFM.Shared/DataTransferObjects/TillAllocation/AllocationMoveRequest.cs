﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
    [ConsortiumRoute("/sites/{SiteId}/allocations/move", HttpVerb.Post, "")]
    public class AllocationMoveRequest : Request
    {
        public int SiteId { get; set; }
        public List<Model.TillAllocation> Changes { get; set; }
    }
}
