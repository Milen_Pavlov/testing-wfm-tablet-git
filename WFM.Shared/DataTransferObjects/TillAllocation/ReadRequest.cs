﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
	[ConsortiumRoute("/sites/{SiteId}/allocations", HttpVerb.Post, "Reads the till allocations for the given site and dates")]
    public class ReadRequest : Request
    {
        public int SiteId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ReadRequest()
        {
            Take = 1000;
        }
    }
}
