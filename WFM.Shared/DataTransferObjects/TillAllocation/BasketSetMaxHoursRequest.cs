﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
    [ConsortiumRoute("/sites/{SiteId}/allocations/setmaxhours", HttpVerb.Post, "Sets the maximum hours for a basket till")]
    public class BasketSetMaxHoursRequest
    {
        public int SiteId { get; set; }
        public int? MaximumHours { get; set; }
    }
}
