﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.TillAllocation
{
    [ConsortiumRoute("/sites/{SiteId}/allocations/getmaxhours", HttpVerb.Get, "Gets the maximum hours for a given site")]
    public class BasketGetMaxHoursRequest
    {
        public int SiteId { get; set; }
    }
}
