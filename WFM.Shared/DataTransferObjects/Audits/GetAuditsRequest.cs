﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Audits
{
    [ConsortiumRoute("/audit/get/", HttpVerb.Get, "Gets audit items for a specific date")]
    public class GetAuditsRequest
    {
        public DateTime Date { get; set; }
        public int Site { get; set; }
    }
}
