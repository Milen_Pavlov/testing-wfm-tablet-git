﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Audits
{
    public class GetAuditsResponse
    {
        public List<AuditRecord> Audits { get; set; }
    }
}
