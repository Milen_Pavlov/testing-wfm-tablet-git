﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace WFM.Shared.DataTransferObjects.Audits
{
    public class AuditRecord
    { 
        public const string DATETIME_ENDING = " on ";

        public string Username { get; set; }
        public string Message { get; set; }
        public DateTime DateOfChange { get; set; }

        public int DepartmentId { get; set; }

        //[JsonIgnore]
        //This is sad code, need to remove on the next server update, we need to adjust the date time to compensate for UTC, but the server sends down the formatted message.
        public String AdjustedMessage
        {
            get
            {
                try
                {
                    int dtIndex = Message.IndexOf(DATETIME_ENDING);

                    string DateString = Message.Substring(dtIndex + DATETIME_ENDING.Length, Message.Length - (dtIndex + DATETIME_ENDING.Length));

                    DateTime dt = DateTime.ParseExact(DateString,@"dd\/MM\/yyyy HH:mm:ss",CultureInfo.InvariantCulture);

                    return Message.Substring(0,dtIndex+DATETIME_ENDING.Length) + DateTime.SpecifyKind(dt,DateTimeKind.Utc).ToLocalTime().ToString("g",CultureInfo.CurrentCulture);
                 }
                catch
                {
                    
                }

                return Message;
            }
            set
            { }
        }
    }
}
