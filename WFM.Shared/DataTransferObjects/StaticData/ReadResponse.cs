﻿using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.StaticData
{
    public class ReadResponse : Response
    {
        public TillStatus[] TillStatuses { get; set; }
        public TillType[] TillTypes { get; set; }

        public class TillStatus
        {
            public int TillStatusId { get; set; }
            public string Key { get; set; }
            public string Name { get; set; }
            public int DisplayOrder { get; set; }
        }

        public class TillType
        {
            public int TillTypeId { get; set; }
            public string Key { get; set; }
            public string Name { get; set; }
            public int DisplayOrder { get; set; }
            public int OpeningSequence { get; set; }
        }
    }
}
