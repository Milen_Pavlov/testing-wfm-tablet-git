﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Overtime
{
    [ConsortiumRoute("/schedule/overtime/get", HttpVerb.Post, "Reads agreed overtime hours for all departments for a week")]
    public class ReadOvertimeRequest
    {
        public int SiteId { get; set; }

        public DateTime ForDate { get; set; }
    }
}
