﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace WFM.Shared.DataTransferObjects.Overtime
{
    [ConsortiumRoute("/schedule/overtime/set", HttpVerb.Post, "Writes overtime records for a given date to the repository")]
    public class SetOvertimeRequest
    {
        public int SiteId { get; set; }
        public DateTime ForDate { get; set; }
        public List<OvertimeRecord> Overtime { get; set; }
        public string Username { get; set; }
    }
}
