﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Overtime
{
    public class OvertimeRecord
    {
        public int JDADepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public float? AllocatedHours { get; set; }
        public float? ResourceRequestDay { get; set; }
        public float? ResourceRequestNight { get; set; }
        public float? ResourceAgreedNight { get; set; }
    }
}
