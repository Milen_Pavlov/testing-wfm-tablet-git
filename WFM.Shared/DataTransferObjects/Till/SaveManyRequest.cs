﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/sites/{SiteId}/tills/save", HttpVerb.Post, "Creates or updates the given tills")]
    public class SaveManyRequest : Request
    {
        public int SiteId { get; set; }
        public Till[] Tills { get; set; }

        public class Till
        {
            public int TillId { get; set; }
            public string TillNumber { get; set; }
            public int StatusId { get; set; }
            public int TypeId { get; set; }
            public TillWindow[] Windows { get; set; }
            public int OpeningSequence { get; set; }
        }

        public class TillWindow
        {
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
    }
}
