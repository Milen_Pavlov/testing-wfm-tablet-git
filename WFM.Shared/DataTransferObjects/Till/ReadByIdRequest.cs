﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/tills/{TillId}", HttpVerb.Get, "Reads a till by its ID")]
    public class ReadByIdRequest : Request
    {
        public int TillId { get; set; }
    }
}
