﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/tills/{TillId}/move/{Direction}", HttpVerb.Get, "Moves a till up or down in the opening sequence")]
    public class MoveRequest : Request
    {
        public int TillId { get; set; }
        public int Direction { get; set; }
    }
}
