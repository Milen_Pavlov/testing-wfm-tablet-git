﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/sites/{SiteId}/tills/openingsequences/update", HttpVerb.Post, "Updates the opening sequences of all the tills of the given site according to the order of the given till IDs")]
    public class UpdateOpeningSequencesRequest : Request
    {
        public int SiteId { get; set; }
        public int[] TillIdsOrderedByOpeningSequence { get; set; }
    }
}
