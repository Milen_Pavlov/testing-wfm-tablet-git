﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/sites/{SiteId}/tills/{Skip}/{Take}", HttpVerb.Get, "Reads all the tills by a site")]
    public class ReadBySiteRequest : Request
    {
        public int SiteId { get; set; }
    }
}