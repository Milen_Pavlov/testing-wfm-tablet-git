﻿using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    public class ReadByIdResponse : Response
    {
        public int TillId { get; set; }
        public string TillNumber { get; set; }
        public int OpeningSequence { get; set; }
        public int SiteId { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public TillWindow[] Windows { get; set; }

        public class TillWindow
        {
            public int TillWindowId { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
    }
}
