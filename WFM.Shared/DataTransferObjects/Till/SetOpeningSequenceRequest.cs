﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/tills/{TillId}/openingsequence/{OpeningSequence}", HttpVerb.Get, "Sets the opening sequence of the given till and changes the opening sequence of sibling tills to make way")]
    public class SetOpeningSequenceRequest : Request
    {
        public int TillId { get; set; }
        public int OpeningSequence { get; set; }
    }
}
