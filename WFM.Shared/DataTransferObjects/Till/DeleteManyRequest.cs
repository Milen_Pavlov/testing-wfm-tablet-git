﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.Till
{
    [ConsortiumRoute("/tills/delete", HttpVerb.Delete, "Deletes the given tills")]
    public class DeleteManyRequest : Request
    {
        public int[] TillIds { get; set; }
    }
}
