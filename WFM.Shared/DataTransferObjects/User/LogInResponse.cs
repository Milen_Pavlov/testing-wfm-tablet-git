﻿using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.User
{
    public class LogInResponse : Response
    {
        public string JdaSessionId { get; set; }
    }
}
