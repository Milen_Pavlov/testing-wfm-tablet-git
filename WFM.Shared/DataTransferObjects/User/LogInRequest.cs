﻿using Consortium.Model.Engine;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFM.Shared.DataTransferObjects.User
{
    [ConsortiumRoute("/users/login", HttpVerb.Post, "Logs the given user into JDA and returns a JDA session ID")]
    public class LogInRequest : Request
    {

    }
}
