﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;
using WFM.Shared.DataTransferObjects.Shared;

namespace WFM.Shared
{
    public class MessageList : List<Message>
    {
        protected MessageList AddMessage(string severity, string friendlyMessage, string technicalDetails = null)
        {
            Add(new Message()
            {
                Severity = severity,
                FriendlyMessage = friendlyMessage,
                TechnicalDetails = technicalDetails
            });
            return this;
        }

        public MessageList Add(IList<Message> messages)
        {
            AddRange(messages);
            return this;
        }

        public MessageList AddSuccess(string friendlyMessage, string technicalDetails = null)
        {
            return AddMessage(MessageSeverity.Success, friendlyMessage, technicalDetails);
        }

        public MessageList AddInfo(string friendlyMessage, string technicalDetails = null)
        {
            return AddMessage(MessageSeverity.Info, friendlyMessage, technicalDetails);
        }

        public MessageList AddWarning(string friendlyMessage, string technicalDetails = null)
        {
            return AddMessage(MessageSeverity.Warning, friendlyMessage, technicalDetails);
        }

        public MessageList AddError(string friendlyMessage, string technicalDetails = null)
        {
            return AddMessage(MessageSeverity.Error, friendlyMessage, technicalDetails);
        }

        public MessageList CopyTo(Response response)
        {
            response.Messages = ToArray();
            response.Success = Success;
            return this;
        }

        public bool Success
        {
            get
            {
                return !this.Any(m => m.Severity == MessageSeverity.Error);
            }
        }
    }
}
