﻿Feature: WFM-1793 - Implement drag, auto scroll etc onto Schedule Editor on iPad (from hudl)
	In order to edit shifts on the daily schedule editor
	As a user
	I can drag and drop shifts on the daily schedule
	
@manualtablet @manualios
Scenario: Begin dragging a shift by press and hold
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	When I press and hold on a shift for 0.5 seconds
	And I am not touching the left/right handles
	Then the shift is dettached from the employee
	And the shift becomes slightly transparent
	And the shift can be dragged around
	And employees who cannot work the job in the shift are greyed out

@manualtablet @manualios
Scenario: Begin dragging a shift by dragging
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	When I press and drag a shift
	And I am not touching the left/right handles
	Then the shift is dettached from the employee
	And can be dragged around

@manualtablet @manualios
Scenario: Auto scroll down
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	And I am dragging a shift
	When I move the shift to the top of the list
	Then list will scroll down until the top of the list is reached

@manualtablet @manualios
Scenario: Auto scroll up
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	And I am dragging a shift
	When I move the shift to near the bottom of the list
	Then list will scroll up until the bottom of the list is reached

@manualtablet @manualios
Scenario: Dropping a shift
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	And I am dragging a shift
	When I move the shift to a valid position
	And I drop the shift
	Then the shift will change start and end times to match the new positon
	And assigned to the employee row the shift was dropped on

@manualtablet @manualios
Scenario: Error if shift position is unavailable
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	And I am dragging a shift
	When I move the shift to an invalid position
	And I drop the shift
	Then a popup will display an error detailing why the shift cannot be placed
	And the shift will be returned to its original position

@manualtablet @manualios
Scenario: Moving a shift outside the schedule editor
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	And I am dragging a shift
	When I move the shift outside of the editor section (e.g. to the header)
	Then the shift will continue to appear on top of everything

@manualtablet @manualios
Scenario: Dropping a shift outside the schedule editor
	Given I am on the schedule tab
	And I have selected an individual day
	And all data has been loaded
	And I am dragging a shift
	When I drop the shift outside of the editor section (e.g. to the header)
	And I drop the shift
	Then the shift will be returned to its original position