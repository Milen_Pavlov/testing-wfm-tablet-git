﻿Feature: WFM-1143 - ability to search for available stores when changing store
	In order to find a specific store easily
	As an employee or a manager
	I want to be able to search the available stores when choosing a store to change to
	
@iosmanual @tabletmanual
Scenario: Change store search is visible
	Given I am logged in as either an employee or manager
	And I have more than one store available to me
	When I open the drawer menu
	And press the "Change Store" button
	Then the "Change Store" view will appear
	And I will see a search box at the top of the list of available stores

@iosmanual @tabletmanual
Scenario: Searching for a store
	Given I am on the "Change Store" view
	When I tap the search bar
	And I type something using the keyboard
	Then I will see a list of stores that contain the search term I have typed
	And I will not see any stores that do not contain the search term I have typed

@iosmanual @tabletmanual
Scenario: Searching for a store that contains "21" store
	Given I am on the "Change Store" view
	When I tap the search bar
	And I type in "21" using the keyboard
	Then I will see a list of stores that contain "21" I have typed
	And I will not see any stores that do not contain "21" I have typed	