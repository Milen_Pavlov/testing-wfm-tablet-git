﻿Feature: Selecting night aisle employees
	In order to choose employees for night aisles
	As a manager
	I want to see a list of employees that are available during night aisle times

@androidManual @tabletManual
Scenario: Night Aisle employees with config setting
	Given I am on the night aisle screen
	And I have a config setting for night aisle departments
	When I select an aisle
	Then I only see employees that are assigned to the departments in the config setting
	And I only see employees that start on or after the night aisle start time
	And I only see employees that end on or before the night aisle end time

@androidManual @tabletManual
Scenario: Night Aisle employees without config setting
	Given I am on the night aisle screen
	And I do not have a config setting for night aisle departments
	When I select an aisle
	Then I only see all employees working during the night aisle time
	And I only see employees that start on or after the night aisle start time
	And I only see employees that end on or before the night aisle end time