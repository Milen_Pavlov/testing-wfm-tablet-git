﻿@tesco
Feature: Replenishment Allocator Settings screen
	In order to change settings related to the night aisle allocator
	As a manager
	I want to access a screen that lets me change the start and end time of the night aisle allocator

@androidManual @tabletManual
Scenario: Night aisle settings screen icon
	Given I am on the night aisle allocator screen
	When I look at the tabs in the center of the screen
	Then I see a settings tab

@androidManual @tabletManual
Scenario: Accessing Night aisle settings screen
	Given I am on the night aisle allocator screen
	When I tap the settings tab
	Then I see the settings screen

@androidManual @tabletManual
Scenario: Change start time popup
	Given I am on the night aisle allocator settings screen
	When I tap the start time
	Then I see a time picker popup
	And the time is limited to 15 minute increments

@androidManual @tabletManual
Scenario: Change start time
	Given I am on the night aisle allocator settings screen
	And I tap the start time
	When I select a new time
	And I select Apply
	Then the new time is reflected in the start time box
	And the save button is enabled
	
@androidManual @tabletManual
Scenario: Change start time - Cancel
	Given I am on the night aisle allocator settings screen
	And I tap the start time
	When I select a new time
	And I select Cancel
	Then the original time is shown in the start time box

@androidManual @tabletManual
Scenario: Change end time popup
	Given I am on the night aisle allocator settings screen
	When I tap the end time
	Then I see a time picker popup
	And the time is limited to 15 minute increments

@androidManual @tabletManual
Scenario: Change end time
	Given I am on the night aisle allocator settings screen
	And I tap the end time
	When I select a new time
	And I select Apply
	Then I the new time is reflected in the end time box
	And edit mode is enabled
	
@androidManual @tabletManual
Scenario: Change end time - Cancel
	Given I am on the night aisle allocator settings screen
	And I tap the end time
	When I select a new time
	And I select Cancel
	Then the original time is shown in the end time box

@androidManual @tabletManual
Scenario: Change tab without changes
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	When I tap another tab
	Then I see a message asking if I want to discard changes

@androidManual @tabletManual
Scenario: Change tab discard changes
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	And I tap another tab
	When I select discard
	Then I go the correct tab
	And my changes are discarded

@androidManual @tabletManual
Scenario: Change tab keep changes
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	And I tap another tab
	When I select to keep changes
	Then I stay on the settings tab
	And my changes remain

@andriodManual @tabletManual
Scenario: Save changes
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	When I tap the save button
	Then I see a pop up asking to upload/discard or continue editing

@andriodManual @tabletManual
Scenario: Save changes - discard
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	And  I tap the save button
	When I select discard
	Then my changes are reset

@andriodManual @tabletManual
Scenario: Save changes - save
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	And  I tap the edit button
	When I select save
	Then my changes are uploaded

@andriodManual @tabletManual
Scenario: Save changes - continue editing
	Given I am on the night aisle allocator settings screen
	And I have changes to start and/or end time
	And  I tap the save button
	When I select keep editing
	Then my changes are not uploaded
	And my changes are not reset