﻿@tesco
Feature: Replenishment Allocator Settings screen layout
	In order to change settings related to the night aisle allocator
	As a manager
	I want to access a screen that lets me change the start and end time of the night aisle allocator

@androidManual @tabletManual
Scenario: Night aisle settings screen tab
	Given I am in the night aisle allocator
	When I look at the tabs in the center of the screen
	Then I the third tab is the settings tab

@androidManual @tabletManual
Scenario: Night aisle settings screen nav bar
	Given I am in the night aisle settings screen
	When I look at the right nav bar
	Then I see an icon for refreshing the settings

@androidManual @tabletManual
Scenario: Night aisle settings screen main
	Given I am in the night aisle settings screen
	When I look at the main part of the screen
	Then I see an editable box for the colleague start time
	And I see an editable box for the colleague end time
	
@androidManual @tabletManual
Scenario: Night aisle settings save button
	Given I am in the night aisle settings screen
	When I look at the bottom part of the screen
	Then I see a save button