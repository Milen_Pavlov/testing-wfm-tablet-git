﻿Feature: As a user, I would be able to see a terms of use message everytime I am using a new version of the app.

@manualios @walmart
Scenario: As a user, I would be able to see a terms of use message when I login to the app for the first time
Given I have never used the current instalation of WFM before
And Terms of service is enabled in the current app
When I login successfully
Then I see a terms of use screen

@manualios @walmart
Scenario: As a user, I would be able to see a terms of use message when I login the app for the first time after the Terms of Service have updated
Given The app terms of service have updated
And Terms of service is enabled in the current app
When I login to the app the first time
Then I see a terms of use screen

@manualios @walmart
Scenario: As a user, I would not see a terms of use message once accepted
Given I have accepted the terms of use
And I am using the same app version 
When I login successfully 
Then I do not see a terms of use screen

@manualios @walmart
Scenario: As a user, I would  see a terms of use message until I accept them
Given I haven't accepted the terms of use yet
And Terms of service is enabled in the current app
And I am using the same app version 
When I login successfully
Then I do see a terms of use screen

@manualios @walmart
Scenario: As a user, I would see a button to be able to accept the terms of use 
Given I have logged in succesfully
And Terms of service is enabled in the current app
And I haven't accepted the current terms of use yet
When I am on the terms of use screen
Then I see a button to accept the terms of use

@manualios @walmart
Scenario: As a user, I would see the drawer menu and schedule screen after accepting terms of use 
Given I am on the terms of use screen
When I click on the accept button
Then I am taken to drawer view with the schedule screen displayed

@manualios @walmart
Scenario: As a user, I would not see the terms of use after an app update if the terms of use have not updated
Given The app has updated
And The terms of service have not updated
And Terms of service is enabled
When I login to the app the first time
Then I don't see the terms of service

@manualios @walmart
Scenario: As a user, I can view links in the terms of service in a web browser
Given I am on the terms of service view
And The terms of service contains hyperlinks
When I tap on a hyperlink
Then Link opens up in a seperate Safari view

@manualios @walmart
Scenario: As a user, after viewing a hyperlink in Safari I can return to the app and see the terms of service still
Given I am on the terms of service view
And The terms of service contains hyperlinks
And I have tapped on a hyperlink
And I have viewed the link in Safari
When I go back to the app
Then The terms of service view is still visible

@manualios @walmart
Scenario: As a user, I would see a button to be able to decline the terms of use 
Given I have logged in succesfully
And Terms of service is enabled in the current app
And I haven't accepted the current terms of use yet
When I am on the terms of use screen
Then I see a button to decline the terms of use

@manualios @walmart
Scenario: As a user, I would return to the login screen and be logged out if I decline the terms of service
Given I am on the terms of use screen
When I click on the decline button
Then I am taken back to the login screen and logged out of my current session

@manualios @walmart
Scenario: As a new user, I can accept the terms of service on an app thats previously been used by another user
Given I have not accepted the terms of service
And Terms of service is enabled in the current app
And Another user has previously accepted the terms of service
When I successfully log in
Then I am taken to the terms of service view