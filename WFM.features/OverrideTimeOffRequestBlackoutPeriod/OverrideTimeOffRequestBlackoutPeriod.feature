﻿Feature: Override time off request blackout period
	In order to create time off requiests within blackout period
	As a manager 
	I want to be able to override blackout period restriction for time off requests
	
@manualios
Scenario: Create all day time off request when Configuration setting for allowing blackout period is enabled
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is enabled
	And I select an employee
	And I create request for request type not in the excluded list
	And Select the request
	And I am in All Day tab
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and if I want to override it

@manualios
Scenario: Create all day time off request when Configuration setting for allowing blackout period is disabled
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is disabled
	And Request type in the included list that can override blackout period 
	And I select an employee
	And I create request
	And I am in All Day tab
	And Select the request for request type in the excluded list
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and if I want to override it

@manualios
Scenario: Create all day time off request when Request type not in the included list that can override blackout period
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is disabled
	And  Request type not in the included list that can override blackout period 
	And I select an employee
	And I create request for request type not in the excluded list
	And Select the request
	And I am in All Day tab
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and request cannot be approved

@manualios 
Scenario: Create all day time off request when List of request types that can override blackout period is empty
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is disabled
	And List of request types that can override blackout period is empty
	And I select an employee
	And I create request
	And I am in All Day tab
	And Select the request for request type in the excluded list
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and request cannot be approved



@manualios
Scenario: Create partial day time off request when Configuration setting for allowing blackout period is enabled
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is enabled
	And I select an employee
	And I create request for request type not in the excluded list
	And Select the request
	And I am in Partial Day tab
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and if I want to override it

@manualios
Scenario: Create partial day time off request when Configuration setting for allowing blackout period is disabled
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is disabled
	And Request type in the included list that can override blackout period 
	And I select an employee
	And I create request
	And I am in Partial Day tab
	And Select the request for request type in the excluded list
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and if I want to override it

@manualios
Scenario: Create partial day time off request when Request type not in the included list that can override blackout period
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is disabled
	And  Request type not in the included list that can override blackout period 
	And I select an employee
	And I create request for request type not in the excluded list
	And Select the request
	And I am in Partial Day tab
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and request cannot be approved

@manualios 
Scenario: Create partial day time off request when List of request types that can override blackout period is empty
	Given I am in Time Off Requests 
	Given Configuration setting for allowing blackout period is disabled
	And List of request types that can override blackout period is empty
	And I select an employee
	And I create request
	And I am in Partial Day tab
	And Select the request for request type in the excluded list
	When I press Approve Request
	Then There should be a popup message warning me that this is in blackout period and request cannot be approved
