﻿Feature: Swap Shift in Schedule Editor
	In order to save time
	As a manager
	I want to be able to swap two shifts between employees
	By dragging one shift from one person ontop of the other

@manualios @manualdroid
Scenario: Swap two shifts of the same job
	Given I am on the Schedule Screen
	And The Schedule Screen is in EDIT mode
	And Swap Shift Mode is not disabled in the Theme
	And Two employees have the same primary or secondary job
	And Both employees are free to work both shifts
	When I drag one shift from one employee ontop of another shift on another employee
	Then The shifts are swapped

@manualios @manualdroid
Scenario: Fail to swap two shifts of the same job due to unavailability
	Given I am on the Schedule Screen
	And The Schedule Screen is in EDIT mode
	And Swap Shift Mode is not disabled in the Theme
	And Two employees have the same primary and secondary jobs
	And One of the employees already has a shift at the time of the swap
	When I drag one shift from one employee ontop of another shift on another employee
	Then An error is displayed informing me of the collision of shift times

@manualios @manualdroid
Scenario: Fail to swap two shifts of different jobs
	Given I am on the Schedule Screen
	And The Schedule Screen is in EDIT mode
	And Swap Shift Mode is not disabled in the Theme
	And Two employees have different primary and secondary jobs
	And Both employees are free to work both shifts
	When I drag one shift from one employee ontop of another shift on another employee
	Then An error is displayed informing me that those shifts can't be swapped due to the different job

@manualdroid
Scenario: Undo
	Given I am on the Schedule Screen
	And The Schedule Screen is in EDIT mode
	And Swap Shift Mode is not disabled in the Theme
	And I have swapped two shifts
	When I click Undo
	Then The shifts return to their original setup

@manualios @manualdroid
Scenario: Swap Shift Mode disabled
	Given I am on the Schedule Screen
	And The Schedule Screen is in EDIT mode
	And Swap Shift Mode is disabled in the Theme
	And I drag one shift from one employee ontop of another shift on another employee
	Then The normal error message for invalid shift edit is displayed	

@manualios @manualdroid
Scenario: Fail to swap a shift with a minor outside his availability period or maximum hours
	Given I am on the Schedule Screen
	And The Schedule Screen is in EDIT mode
	And Swap Shift Mode is not disabled in the Theme
	And Two employees have the same primary or secondary job
	And Both employees are free to work both shifts
	And At least one of the employees is a minor
	And Swapping the shift would result in violating the minor rules
	When I drag one shift from one employee ontop of another shift on another employee
	Then An error is displayed informing me that those shifts can't be swapped due to the minor restrictions