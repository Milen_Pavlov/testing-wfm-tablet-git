﻿Feature: Total Hours to be replaced with FTE
	In order to easily see how many people are working on a day
	As a Virgin Venue Manager
	I want to be able to see Full Time Employess instead of total hours on the schedule view, this is assuming all shifts are 8 hours long


@manualios
Scenario: Viewing the schedule as a non Virgin Venue manager
	Given I am using a theme other than Virgin Venue
	And I am logged in as a manager
	And I am on the schedule view
	And TotalHoursPerDayDivisor is set to 1
	When I look at the table headers or the table title
	Then I see "Hours"
	And all the "Hours" values should be normal
	And all the "Demand" values should be normal

@manualios
Scenario: Viewing the schedule as a Virgin Venue manager
	Given I am using a Virgin Venue themed environment
	And I am logged in as a manager
	And I am on the schedule view
	And TotalHoursPerDayDivisor is set to 8
	When I look at the table headers or the table title
	Then I see "FTEs" instead of "Hours"
	And all "FTEs" values shown will be the "Hours" values divided by the TotalHoursPerDayDivisor
	And all the "Demand" values are also divided by TotalHoursPerDayDivisor

