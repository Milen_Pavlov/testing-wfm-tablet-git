﻿Feature: WFM-1669 - Call Off for Android - Allow users to call off shifts from the weekly schedule
	In order to accurately record sickness from the WFM tablet
	As a manager
	I am able to Remove Unpaid Absence (call off) for a shift and record a days absence for an employee instead

@manualtablet @manualandroid
Scenario: Confirmation shown before Remove Unpaid Absence.
	Given I am looking at a the shift details popup
	When I click the Remove Unpaid Absence button
	Then I am presented with a confirmation dialog asking me to confirm or cancel the Remove Unpaid Absence operation

Scenario: Calling off the shift creates a partial day absence record.
	Given The currently selected shift starts and ends on a single day
		I am looking at the Remove Unpaid Absence confirmation dialog
	When I press confirm
	Then the shift view is closed.
		And the schedule is refreshed
		And a time off period of type 'Holiday' is created covering the period the shift was scheduled to run.

Scenario: Calling off a shift that spans two days creates one partial day absence record
	Given The currently selected shift starts before midnight and ends after midnight
		And I am looking at the Remove Unpaid Absence confirmation dialog
	When I press confirm
	Then the shift view is closed.
		And the schedule is refreshed
		And a partial day time off period of type 'Holiday' is created starting at the time the shift started. 
		And the partial day time off period finishes on the time and day the shift ended.

Scenario: Pressing cancel on confirmation dialog does not Remove Unpaid Absence for the shift.
	Given I am looking at the Remove Unpaid Absence confirmation dialog
	When I press 'cancel'
	Then the confirmation dialog is dismissed.
		And the user remains on the shift popup window.
		And the shift is not called off.	

Scenario: Cannot Remove Unpaid Absence for a shift if in edit mode
	Given I have pending edits to the schedule.
		And I am looking at the shift popup
	When I press Remove Unpaid Absence
	Then I see an error message telling me I cannot Remove Unpaid Absence for a shift whilst I have pending changes to schedule
		And I am unable to Remove Unpaid Absence for the shift