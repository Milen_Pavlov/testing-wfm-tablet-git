﻿Feature: WFM-1669 - Call Off for Android - Language Changes
	So I know what effect the delete, unfill and call off shift buttons have on the schedule.
	As a manager
	It is clear from the options on the shift view what operation the buttons perform.
	
@manualtablet @manualandroid
Scenario: Remove Paid Absence button is visible in the shift popup view.
	Given I am on the schedule tab
		And all data has been loaded
	When I click on a shift
	Then I see the shift popup
		And I see an option to 'Remove Paid Absence' for the shift

@manualtablet @manualandroid
Scenario: Remove Overtime button is visible in the shift popup view.
	Given I am on the schedule tab
		And all data has been loaded
	When I click on a shift
	Then I see the shift popup
		And I see an option to 'Remove Overtime' for the shift

@manualtablet @manualandroid
Scenario: Remove Unpaid Absence button is visible in the shift popup view.
	Given I am on the schedule tab
		And all data has been loaded
	When I click on a shift
	Then I see the shift popup
		And I see an option to 'Remove Unpaid Absence' for the shift