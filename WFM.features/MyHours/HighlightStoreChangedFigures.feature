﻿Feature: WFM-1321 - MyHours - visually differentiate figures that have been updated by the store
	In order to easily see any edits I have made to MyHours
	As a user
	I know which values are different to those set by the Back Office
	
@manualtablet @manualandroid
Scenario: Change Planned Hours for a department
	Given I am on the MyHours screen
	And I have valid data
	When I click on an editable field
	And I save a value when prompted
	Then the background of the field I edited will be highlighted in yellow

@manualtablet @manualandroid
Scenario: Submit changes to Planned Hours for a department
	Given I am on the MyHours screen
	And I have valid data
	And I have edited some data locally
	When I click in the "Submit Forecast" button
	And the data has been submitted sucessfully
	Then the background of the field I edited will still be highlighted in yellow

@manualtablet @manualandroid
Scenario: Load edited data for MyHours
	Given I am on the MyHours screen
	And I have valid data
	When the data contains fields edited by the store
	Then the background of the field that has been edited will be highlighted in yellow

@manualtablet @manualandroid
Scenario: Change the percentage value for a day within a department
	Given I am on the MyHours screen
	And I have valid data
	And I have clicked on a department name
	When I edit a "Trading %" field
	And I save a value when prompted
	Then the field will be highlighted in yellow

@manualtablet @manualandroid
Scenario: Submit changes to percentage value for a day within a department
	Given I am on the MyHours screen
	And I have valid data
	And I have locally edited "Trading %" data for a day within a department
	When I click in the "Submit Forecast" button
	And the data has been submitted sucessfully
	Then the background of the field I edited will still be highlighted in yellow

@manualtablet @manualandroid
Scenario: Load edited data for a day within a department
	Given I am on the MyHours screen
	And I have valid data
	And the data contains "Trading %" data edited by the store
	When I click the department name with the edited data
	Then the background of the field that has been edited will be highlighted in yellow