﻿
Feature: Update Forecasted Sales field source in myHours
	In order for myHours to show accurate figures for Total Forecasted Sales
	As a manager
	I want to see a Total Forecasted Sales as the total of only the selling departments
	
@manualtablet @manualandroid
Scenario: Viewing Forecasted Sales Total in MyHours screen
	Given I am logged in as a manager
	When I have click the myHours item in the drawer menu
	And I have valid data
	Forecasted Sales total header will now show the total of only the selling departmenrs
	taken from new version of myHours API
