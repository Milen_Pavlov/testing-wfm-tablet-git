﻿Feature: Highlight changes made by Central
	In order to easily see changes made to MyHours by Central
	As a manager
	I want to see a highlight on any datat that has had changes made by Central
	
@manualtablet @manualandroid
Scenario: Entering MyHours screen with central changes
	Given I am logged in as a manager
	When I have click the myHours item in the drawer menu
	And I have valid data
	And the data contains changes by Central
	Then I get a dialog box telling me that Central have made changes to the data 
	And the background for the cell will be highlighted red

@manualtablet @manualandroid
Scenario: Previous week MyHours screen with central changes
	Given I am on the MyHours screen
	When I click the previous week button
	And I receive valid data
	And I receive data that contains changes made by Central
	Then I get a dialog box telling me that Central have made changes to the data 
	And the background for the cell will be highlighted red

@manualtablet @manualandroid
Scenario: Next day MyHours screen with central changes
	Given I am on the MyHours screen
	When I click the next week button
	And I receive valid data
	And I receive data that contains changes made by Central
	Then I get a dialog box telling me that Central have made changes to the data 
	And the background for the cell will be highlighted red

@manualtablet @manualandroid
Scenario: Refresh day MyHours screen with central changes
	Given I am on the MyHours screen
	When I click the refresh button
	And I receive valid data
	And I receive data that contains changes made by Central
	Then I get a dialog box telling me that Central have made changes to the data 
	And the background for the cell will be highlighted red