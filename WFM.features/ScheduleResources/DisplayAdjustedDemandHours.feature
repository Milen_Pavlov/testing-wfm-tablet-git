﻿
Feature: WFM-1670 - Schedule Resources - display the updated demand from the 'absence target' calculations
	In order to accurately see the updated demand from the 'absence target' calculations
	As a manager
	I need to be able to see the Adjusted Demand Hours value produced as a result of 
	Demand Hours minus difference between Time Off and Store Absence Target
	Store Absebce Target is calculated as percentage for store and week (returned from API) multiplied by the Total of Contracted Hours
	
@manualtablet @manualandroid
Scenario: View Adjusted Demand Hours
	Given I am on the schedule tab
	And all data has been loaded
	When I click on the resources tab
	Then I see Adjusted Demand Hours field calculated using the above variables