﻿Feature: WFM-1669 - Schedule Resources - Filter departments not funded by the store
	In order to accurately see the total store scheduled and planned hours
	As a user
	Only the departments funded by the store are shown in the resources table.
	
@manualtablet @manualandroid
Scenario: Departments only show internally funded departments
	Given I am on the schedule tab
	And all data has been loaded
	When I click on the resources tab
	Then the list of departments does not contain Security
		And the list of departments does not contain Cleaning and Maintenance
		And the list of departments does not contain Phone Shop
