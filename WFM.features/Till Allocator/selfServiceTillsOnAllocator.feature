﻿@tesco
Feature: Self Service Till Allocator Functionality

 Self Service and Scan as you Shop employees should appear in the till allocator.
 This is so that their shifts are visible to the checkouts manager and their breaks
 and attendance can be managed in the same way as other Checkout Operators.


@androidManual @tabletManual
Scenario: Self Service Checkout staff are shown on the till allocator 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Self Service Checkout
	When I am looking a the till allocator screen for a specific day
	Then The first row is for a Self Service Checkout till
	And The employee's shift is detailed

@androidManual @tabletManual
Scenario: Scan as you Shop staff are shown on the till allocator 
	Given The current schedule contains 1 employee
	And The employee has a shift with a job role of Scan as you Shop
	When I am looking a the till allocator screen for a specific day
	Then The first row is for a Scan as you Shop till
	And The employee's shift is detailed

@androidManual @tabletManual
Scenario: Self Service Checkout allocations are read only 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Self Service Checkout
	When I am looking a the till allocator screen for a specific day
	And I am in edit mode
	Then The Self Service Checkout allocation cannot be moved.

@androidManual @tabletManual
Scenario: Scan as you shop allocations are read only 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Scan as you shop
	When I am looking a the till allocator screen for a specific day
	And I am in edit mode
	Then The Scan as you Shop allocation cannot be moved.

@androidManual @tabletManual
Scenario: SAYS and SSC are not always shown on the till allocator 
	Given The current schedule contains no employees
	When I am looking a the till allocator screen for any day
	Then Scan as you Shop and Self Service checkout tills are not shown

@androidManual @tabletManual
Scenario: Multiple self service checkout staff are shown on the till allocator 
	Given The current schedule contains 2 employees
	And The employees both have a shift at the same time 
	And Both shifts have a job role of self service checkout
	When I am looking a the till allocator screen for any day
	Then The first two rows are for self service checkout tills
	And Both employees shifts are shown

@androidManual @tabletManual
Scenario: Multiple scan as you shop staff are shown on the till allocator 
	Given The current schedule contains 2 employees
	And The employees both have a shift at the same time
	And Both shifts have a job role of scan as you shop
	When I am looking a the till allocator screen for any day
	Then The first two rows are for scan as you shop tills
	And Both employees shifts are shown

@androidManual @tabletManual
Scenario: Scan as you shop and Self Service tills are shown in the correct orer 
	Given The current schedule contains 2 employees
	And One employee has a shift with a job role of scan as you shop
	And One employee has a shift with a job role of self service checkout
	When I am looking a the till allocator screen for any day
	Then The first row is for Scan as you Shop
	And The second row is for Self Service 
	And Both employees shifts are shown

@androidManual @tabletManual
Scenario: Self Service Checkout staff are shown on the attendance view 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Self Service Checkout
	When I am looking a the attendance view 
	Then The attendance view contains the employee
	And I am able to change the attendance state of the employee

@androidManual @tabletManual
Scenario: Scan as you shop checkout staff are shown on the attendance view 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Scan as you shop
	When I am looking a the attendance view 
	Then The attendance view contains the employee
	And I am able to change the attendance state of the employee

@androidManual @tabletManual
Scenario: Scan as you shop checkout staff are shown on the break view 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Scan as you shop
	And The shift contains 1 break
	When I am looking a the till allocator breaks view 
	Then The break is detailed
	And I am able to change the taken state of the break

@androidManual @tabletManual
Scenario: Self Service checkout staff are shown on the break view 
	Given The current schedule contains 1 employee
	And The employee has shift with a job role of Self Service Checkout
	And The shift contains 1 break
	When I am looking a the till allocator breaks view 
	Then The break is detailed
	And I am able to change the taken state of the break