﻿
Feature: Ability to fill unfilled shifts 
	In order to fill unfilled shifts with available employees 
	As a manager using an environment that supports this functionality
	I want to be able to view/delete/fill unfilled shifts

@iosmanual @tabletmanual
Scenario: Show fill unfilled shifts popup
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is enabled
	When I press the "Fill Unfilled Shifts" icon
	Then I see Fill Unfilled shifts popup

@iosmanual @tabletmanual
Scenario: Delete unfilled shifts
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is enabled
	And there are unfilled shifts
	And I am on Fill Unfilled Shifts popup
	When I tap on Delete unfilled shifts button from Unfilled Shifts table
	And I confirm that I want to delete the shift
	Then unfilled shift is deleted

@iosmanual @tabletmanual
Scenario: Populate list of available employees for shift
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is enabled
	And I am on Fill Unfilled Shifts popup
	And there are unfilled shifts
	When I select an unfilled shift 
	Then list of available employees is populated to the right of Unfilled Shifts table

@iosmanual @tabletmanual
Scenario: Fill unfilled shift
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is enabled
	And I am on Fill Unfilled Shifts popup
	And there are unfilled shifts
	And Unfilled shift is selected
	And there is at least one available employee to fill the shift
	When I press fill shift for an available employee
	Then unfilled shift is filled with selected employee

@iosmanual @tabletmanual
Scenario: Fill unfilled shift button disabled for customer with readonly access to jda data
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is disabled
	Then Fill Unfilled Shift button will be disabled

@iosmanual @tabletmanual
Scenario: Unfilled Shifts ordering for weekly schedule view starts from earliest date
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is enabled
	And there are unfilled shifts
	And I am in weekly schedule view
	When I press the "Fill Unfilled Shifts" icon
	Then unfilled shifts will be orderd by the shift date starting from earliest date

@iosmanual @tabletmanual
Scenario: Fill unfilled shifts button is disabled when there is no unfilled shifts for week or day
	Given I am on the schedule screen in the app
	And I am on an environment that supports this functionality
	And editing is enabled
	And the is no unfilled shifts for week or day
	Then Fill Unfilled Shift button will be disabled