﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.Android;
using WFM.Core;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.Bindings.Target.Construction;
using Cirrious.MvvmCross.Droid.Views;

namespace WFM.Android
{
	public class PlatformSetup : IPlatformSetup
	{
		public ConsortiumConfig InitialiseFirst()
		{
            RaygunService.Attach ("ocC7dCxTy/N8OPiGn4kbYA==");

			return new ConsortiumConfig() {
				RequiresSignIn = false,
				HomeView = typeof(SessionViewModel)
			};                     
		}

		public void InitialiseLast()
		{
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
		}

		public void RegisterBindings(IMvxBindingNameRegistry registry)
		{
		}

        public void RegisterCustomBindings(IMvxTargetBindingFactoryRegistry registry)
        {

        }            
	}
}
