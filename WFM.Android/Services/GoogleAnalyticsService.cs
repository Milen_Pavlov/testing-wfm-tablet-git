﻿using System;
using WFM.Core;
using Consortium.Client.Core;
using Android.Gms.Analytics;
using Android.App;
using Cirrious.CrossCore;
using System.Collections.Generic;

namespace WFM.Android
{
    [ConsortiumService]
    public class GoogleAnalyticsService : WFM.Core.IAnalyticsService
    {
        private readonly GoogleAnalytics GAInstance;
        private Tracker m_tracker;

        private IDictionary<int,string> Dimensions = new Dictionary<int,string> ();

        private Tracker AnalyticsTracker
        {
            get
            {
                return m_tracker;
            }
        }

        private IMobileApp m_mobileApp;
       
        public string TrackingID
        {
            set
            {
                SetupAnalyticsTracker (value);
            }
        }
            
        public GoogleAnalyticsService(IMobileApp app)
        {
            m_mobileApp = app;
            GAInstance = GoogleAnalytics.GetInstance(Application.Context);

            m_tracker = GAInstance.NewTracker (string.Empty);

            Mvx.Trace("(Analytics) Init");
        }

        void SetupAnalyticsTracker (string trackingCode)
        {            
            Mvx.Trace("(Analytics) Set Tracking Code: {0}",trackingCode);

            m_tracker = GAInstance.NewTracker (trackingCode);

            AnalyticsTracker.EnableExceptionReporting (true);
            AnalyticsTracker.SetAppVersion (m_mobileApp.Version);
            AnalyticsTracker.SetAppName (m_mobileApp.Name);
        }

        public void SetSessionParam(int key, string value)
        {            
            Mvx.Trace("(Analytics) Set Data: {0} = {1}", key, value);
            //AnalyticsService not registered in the app manifest
            if(Dimensions.ContainsKey(key))
                Dimensions[key] = value;
            else
                Dimensions.Add(key,value);
        }

        public void TrackScreenView (string screenName)
        {
            Mvx.Trace("(Analytics) View Screen: {0}",screenName);

            var screenView = new HitBuilders.ScreenViewBuilder ();

            foreach (var kvp in Dimensions)
            {
                screenView.SetCustomDimension (kvp.Key, kvp.Value);
            }
            
            AnalyticsTracker.SetScreenName(screenName);
            AnalyticsTracker.Send(screenView.Build());
        }

        public void TrackAction (string categoryName, string actionName)
        {
            TrackAction (categoryName, actionName, null);
        }

        public void TrackAction (string categoryName, string actionName, string value)
        {
            Mvx.Trace("(Analytics) Perform Action: {0}:{1} - {2}", categoryName,actionName,value);

            var action = new HitBuilders.EventBuilder ()
                .SetCategory (categoryName)
                .SetAction (actionName)
                        .SetLabel (value);

            foreach (var kvp in Dimensions)
            {
                action.SetCustomDimension (kvp.Key, kvp.Value);
            }

            AnalyticsTracker.Send(action.Build());
        }
    }
}