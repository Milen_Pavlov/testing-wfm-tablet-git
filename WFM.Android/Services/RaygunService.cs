﻿using System;
using WFM.Core;
using Consortium.Client.Core;
using System.Collections;
using Android.OS;
using Mindscape.Raygun4Net.Messages;
using Mindscape.Raygun4Net;
using System.Collections.Generic;

namespace WFM.Android
{
    [ConsortiumService]
    public class RaygunService : IRaygunService
    {
        //Can't resolve this in Mvx at the point we need to attach, so do this in this annoying way
        public static void Attach(string key)
        {
            #if !DEBUG
            if(!string.IsNullOrEmpty(key))
                RaygunClient.Attach(key);
            #endif
        }

        public RaygunService ()
        {
        }

        public void LogCustomError (Exception e)
        {
            if(RaygunClient.Current!=null)
                RaygunClient.Current.SendInBackground (e);
        }

        public void LogCustomError (Exception e, IDictionary data)
        {
            if(RaygunClient.Current!=null)
                RaygunClient.Current.SendInBackground (e, null, data);
        }

        public void ClearUserInfo()
        {
            SetCurrentUserInfo(string.Empty, string.Empty, string.Empty, string.Empty);
        }

        public void SetCurrentUserInfo (string loginname, string siteId, string siteName, string environment)
        {
            if(RaygunClient.Current!=null)
                RaygunClient.Current.UserInfo = new RaygunIdentifierMessage(loginname)
                    {
                        IsAnonymous = string.IsNullOrEmpty(loginname),
                        FirstName = loginname,
                        FullName = string.Join(" - ", new List<string>()
                            {
                                loginname,
                                siteId, 
                                siteName,
                                environment
                            }),
                        UUID = Build.Serial,
                    };
        }
    }
}