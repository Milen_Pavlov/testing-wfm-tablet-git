﻿using System;
using WFM.Core;
using Consortium.Client.Core;
using Android.App;
using Consortium.Client.Android;
using Android.Content;
using Android.Views.InputMethods;
using Android.Widget;
using Android.Views;
using Android.Text;
using System.Collections.Generic;
using Cirrious.CrossCore.Core;
using Cirrious.CrossCore;

namespace WFM.Android
{
    [ConsortiumService]
    public class CustomAlertService : MvxMainThreadDispatchingObject, ICustomAlertService
    {
        private readonly IStringService m_localiser;

        public CustomAlertService ()
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public AlertBoxToken ShowMyHoursInput(string columnTitle, string rowTitle, string inputPlaceholder, Action<AlertReturn, string> onSelection)
        {
            AlertBoxToken token = new AlertBoxToken();

            InvokeOnMainThread (() =>
                {
                    EditText text;
                    var inputContent = BuildInputView(string.Format ("{0} : ", columnTitle), Consortium.Client.Core.KeyboardType.Numeric, inputPlaceholder, out text);

                    AlertDialog.Builder adb = new AlertDialog.Builder(ConsortiumApp.CurrentContext);
                    InputMethodManager imm = adb.Context.GetSystemService(global::Android.Content.Context.InputMethodService) as InputMethodManager;
                    adb.SetTitle(string.Format (m_localiser.Get("my_hours_input_title"), columnTitle, rowTitle));
                    adb.SetView(inputContent);
                    adb.SetPositiveButton(m_localiser.Get("my_hours_input_positive"), (sender, e) =>
                        {
                            if (imm != null)
                                imm.HideSoftInputFromWindow ( inputContent.WindowToken, 0);

                            if (onSelection != null)
                                onSelection(AlertReturn.Positive, text.Text);
                        });
                    adb.SetNegativeButton(m_localiser.Get("my_hours_input_negative"), (sender, e) =>
                        {
                            if (imm != null)
                                imm.HideSoftInputFromWindow ( inputContent.WindowToken, 0);

                            if (onSelection != null)
                                onSelection(AlertReturn.Negative, string.Empty);
                        });

                    adb.SetCancelable(false);//Cancelling happens AFTER the window is dismissed, so WindowToken is null and keyboards can't be dismissed :(

                    adb.Show();
                });

            return token;
        }

        class DismissListener : Java.Lang.Object, IDialogInterfaceOnDismissListener
        {
            public void OnDismiss(IDialogInterface dialog)
            {
                var manager = (InputMethodManager)ConsortiumApp.CurrentActivity.GetSystemService(Activity.InputMethodService);

                //Ncessary in case they dismiss the keyboard
                if (KeyboardUtility.IsKeyboardShown && ConsortiumApp.CurrentActivity.Window != null && ConsortiumApp.CurrentActivity.Window.DecorView != null)
                {
                    manager.HideSoftInputFromWindow(ConsortiumApp.CurrentActivity.Window.DecorView.WindowToken, HideSoftInputFlags.None);
                    //manager.ToggleSoftInput(ShowFlags.Implicit, HideSoftInputFlags.ImplicitOnly);
                }
            }
        }

        class CancelListener : Java.Lang.Object, IDialogInterfaceOnCancelListener
        {
            Action m_onCancelAction;

            public CancelListener(Action onCancelAction)
            {
                m_onCancelAction = onCancelAction;
            }

            public void OnCancel (IDialogInterface dialog)
            {
                if (m_onCancelAction != null)
                    m_onCancelAction();
            }
        }

        static View BuildInputView(string message, Consortium.Client.Core.KeyboardType type, string inputPlaceholder, out EditText text, int characterLimit = 0)
        {
            View inputContent = ConsortiumApp.Inflator.Inflate(Resource.Layout.view_custom_input_dialogue, null);

            text = (EditText)inputContent.FindViewById(Resource.Id.dialog_edittext_input);
            text.FocusChange += (object sender, View.FocusChangeEventArgs e) => 
                {

                };

            if (type == Consortium.Client.Core.KeyboardType.Numeric)
            {
                text.InputType = global::Android.Text.InputTypes.ClassNumber | global::Android.Text.InputTypes.NumberFlagDecimal;
            }
            else
            {
                text.InputType = global::Android.Text.InputTypes.ClassText | global::Android.Text.InputTypes.TextFlagMultiLine;
            }

            text.Text = inputPlaceholder;
            if (characterLimit > 0)
            {
                IInputFilter[] filters = { new InputFilterLengthFilter(characterLimit) };
                text.SetFilters(filters);
            }

            TextView messageView = (TextView)inputContent.FindViewById(Resource.Id.dialog_edittext_label);
            if(messageView != null) messageView.Text = message;

            return inputContent;
        }
    }
}

