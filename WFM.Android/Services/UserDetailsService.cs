﻿using System;
using Android.App;
using Android.Content;
using Android.Preferences;
using Consortium.Client.Android;
using Consortium.Client.Core;
using WFM.Core;

namespace WFM.Android
{
    [ConsortiumService]
    public class UserDetailsService : IUserDetailsService
    {
        private ISharedPreferences m_preferences;

        public UserDetailsService()
        {
            m_preferences = PreferenceManager.GetDefaultSharedPreferences( Application.Context );
        }

        #region IUserDetailsService implementation

        public void SetUsername( string username )
        {
            m_preferences.Edit().PutString( UserDetailsUtility.UsernameKey, username ).Commit();
        }

        public string GetUsername()
        {
            return m_preferences.GetString( UserDetailsUtility.UsernameKey, string.Empty );
        }

        public void ClearUsername()
        {
            m_preferences.Edit().Remove( UserDetailsUtility.UsernameKey ).Commit();
        }

        public void SetPassword( string password )
        {
            m_preferences.Edit().PutString( UserDetailsUtility.PasswordKey, password ).Commit();
        }

        public string GetPassword()
        {
            return m_preferences.GetString( UserDetailsUtility.PasswordKey, string.Empty );
        }

        public void ClearPassword()
        {
            m_preferences.Edit().Remove( UserDetailsUtility.PasswordKey ).Commit();
        }

        public void SetKeyValue( string key, string value )
        {
            m_preferences.Edit().PutString( key, value ).Commit();
        }

        public string GetValueForKey( string key )
        {
            return m_preferences.GetString( key, string.Empty );
        }

        public void RemoveValueForKey( string key )
        {
            m_preferences.Edit().Remove( key ).Commit();
        }

        #endregion
    }
}