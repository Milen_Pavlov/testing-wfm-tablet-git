﻿using System;
using Consortium.Client.Core;
using WFM.Core;
using Android.Security;

namespace WFM.Droid
{
    [ConsortiumService]
    public class StoredCredentialsService : IStoredCredentialsService
    {
        private const string c_usernameKey = "wfmcredentials.username";
        private const string c_passwordKey = "wfmcredentials.password";
        private const string c_siteIDKey = "wfmcredentials.siteid";
        private const string c_userIDKey = "wfmcredentials.userid";

     
        #region IStoredCredentialsService implementation
        public void StoreCredentials (string username, string password, string siteID, string userID)
        {
            
        }

        public void ClearCredentials ()
        {
            
        }

        public void UpdateSiteID (string newSiteId)
        {
            
        }

        public string Username
        {
            get
            {
                return null;
            }
        }

        public string Password
        {
            get
            {
                return null;
            }
        }

        public string SiteID
        {
            get
            {
                return null;
            }
        }

        public string UserUID
        {
            get
            {
                return null;
            }
        }

        #endregion
    }
}