﻿using System;
using Consortium.Client.Android;
using Android.Widget;
using WFM.Core;
using Android.Views;
using Android.OS;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Threading.Tasks;
using Android.Views.Animations;
using Android.App;
using Android.Graphics;
using Android.Content;

namespace WFM.Android
{
    public class CombineDeptView : ConsortiumFragment
    {
        private readonly IStringService m_localiser;

        private const int c_topBottomMarginDp = 4;
        private const int c_segmentWidth = 90;

        private AlternateBackgroundControl m_uncombined;
        private AlternateBackgroundControl m_leads;
        private AlternateBackgroundControl m_combined;

        private BarButton m_refreshButton;
        private BarButton m_saveButton;

        public CombineDeptViewModel VM { get { return (CombineDeptViewModel)this.ViewModel; } }

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        public bool Loading
        {
            get { return false; }
            set 
            {
                if(!value)
                {
                    m_refreshButton.Clickable = true;
                    m_refreshButton.Alpha = 1f;
                }
                else
                {
                    m_refreshButton.Clickable = false;
                    m_refreshButton.Alpha = 0.5f;
                }
            }
        }

        public bool CanSave
        {
            get { return false; }
            set 
            {
                if(value)
                {
                    m_saveButton.Clickable = true;
                    m_saveButton.Alpha = 1f;
                }
                else
                {
                    m_saveButton.Clickable = false;
                    m_saveButton.Alpha = 0.5f;
                }
            }
        }

        public CombineDeptView() : base(Resource.Layout.view_combinedept)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            Title = m_localiser.Get ("combined_dept");
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += OnRefresh;

            m_saveButton = new BarButton (ConsortiumApp.Context);
            m_saveButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_saveButton.Text = m_localiser.Get ("combined_dept_save_button");
            m_saveButton.Click += OnSave;

            var controlButtons = new List<BarButton>() { m_saveButton, m_refreshButton };
            NavBar.RightButtons = controlButtons;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_uncombined = (AlternateBackgroundControl)view.FindViewById (Resource.Id.uncombinedList);
            m_uncombined.RegisterViewWithResourceID<CombinedDepartmentCell> (Resource.Layout.cell_combined_dept);

            m_leads = (AlternateBackgroundControl)view.FindViewById (Resource.Id.leadList);
            m_leads.RegisterViewWithResourceID<CombinedDepartmentCell> (Resource.Layout.cell_combined_dept);

            m_combined = (AlternateBackgroundControl)view.FindViewById (Resource.Id.combinedList);
            m_combined.RegisterViewWithResourceID<CombinedDepartmentCell> (Resource.Layout.cell_combined_dept);


//            m_uncombined.ItemLongClick += (object sender, AdapterView.ItemLongClickEventArgs e) => 
//                {
//                    CombinedDepartmentCell cell = e.View as CombinedDepartmentCell;
//                    if(cell!=null)
//                        cell.OnCellBeginDrag();
//                };
//
//            m_leads.ItemLongClick += (object sender, AdapterView.ItemLongClickEventArgs e) => 
//                {
//                    CombinedDepartmentCell cell = e.View as CombinedDepartmentCell;
//                    if(cell!=null)
//                        cell.OnCellBeginDrag();
//                };
//
//            m_combined.ItemLongClick += (object sender, AdapterView.ItemLongClickEventArgs e) => 
//                {
//                    CombinedDepartmentCell cell = e.View as CombinedDepartmentCell;
//                    if(cell!=null)
//                        cell.OnCellBeginDrag();
//                };

            // View Model Binding
            var set = this.CreateBindingSet<CombineDeptView, CombineDeptViewModel> ();
            set.Bind (this).For (v => v.Loading).To (vm => vm.Loading);
            set.Bind (this).For (v => v.CanSave).To (vm => vm.CanSave);
            set.Bind (m_uncombined).To (vm => vm.Uncombined);
            set.Bind (m_leads).To (vm => vm.Leads);
            set.Bind (m_leads).For (v => v.SelectedCommand).To ("OnLeadSelected");
            set.Bind (m_leads).For (v => v.CustomCommand1).To ("OnLeadSelected");
            set.Bind (m_combined).To (vm => vm.CurrentCombined);
            set.Apply ();
        }

        public override void OnDestroy()
        {
            m_refreshButton.Click -= OnRefresh;
            m_saveButton.Click -= OnSave;

            base.OnDestroy ();
        }

        void OnRefresh(object sender, EventArgs e) 
        {
            VM.OnRefresh();
        }

        void OnSave(object sender, EventArgs e) 
        {
            VM.OnSubmit();
        }
    }
}

