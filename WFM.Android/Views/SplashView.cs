﻿using System;
using Android.App;
using Consortium.Client.Android;

namespace WFM.Android
{
    public class SplashView : ICustomSplashView
    {
        public void OnCreate (Activity activity)
        {
        }

        public int AppStartDelayInSeconds 
        {
            get { return 1; }
        }
    }
}
