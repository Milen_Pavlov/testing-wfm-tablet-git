using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.OS;
using Android.Util;
using WFM.Core;

namespace WFM.Android
{
    public class ScalableHoursRulerView : View //HoursRulerView
	{
        private Paint m_linePaint;
        private Paint m_textPaint;
        private int m_width;
        private int m_height;

        // Updated in RefreshHourLimits
        private float m_visibleHours;
        private float m_leftHours;
        private float m_rightHours;

        private float m_scaleFactor;
        public float ScaleFactor
        {
            get { return m_scaleFactor; }
            set
            {
                if (m_scaleFactor != value)
                {
                    m_scaleFactor = value;
                    Invalidate();
                }
            }
        }

        private float m_centreHours = (float)(ScheduleTime.HoursPerDay/2);
        public float CentreHours //The hours at the centre of the view
        {
            get { return m_centreHours; }
            set
            {
                if (m_centreHours != value)
                {
                    m_centreHours = value;
                    Invalidate();
                }
            }
        }

        public Tuple<float,float> Hours
        {
            get;
            private set;
        }

        public ScalableHoursRulerView(Context context) : base(context) 
		{ 
			Init();
		}

        public ScalableHoursRulerView(Context context, IAttributeSet attrs) : base(context, attrs) 
		{ 
			Init();
		}

        public ScalableHoursRulerView(Context context, IAttributeSet attrs, int defStyle ) : base(context, attrs, defStyle) 
		{ 
			Init();
		}

        void ResetScale()
        {
            ScaleFactor = 1.0f;
        }
            
		private void Init()
		{ 
			// Setup Paints
			m_textPaint = new Paint {Color = Color.White, StrokeWidth = 1};
			m_textPaint.AntiAlias = true;
			m_textPaint.TextSize = Resources.GetDimension(Resource.Dimension.Ruler_Text_Size);

			m_linePaint = new Paint {Color = Color.White, StrokeWidth = 1};

            ResetScale();
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			// Get the size of the control
			m_width = View.MeasureSpec.GetSize (widthMeasureSpec);
			m_height = View.MeasureSpec.GetSize (heightMeasureSpec);
			SetMeasuredDimension (m_width, m_height);
		}
		protected override void OnDraw( Canvas canvas )
		{
            RefreshHourLimits();
			DrawText( canvas );
			DrawLines( canvas );
		}

        public void DragX(float deltaX)
        {
            if (deltaX > 0.0f)
            {
                float lastLeftHours = m_leftHours;
                m_leftHours -= PixelsToHours(deltaX);
                if (m_leftHours < 0.0f)
                {
                    m_leftHours = 0.0f;
                }
                float leftDeltaHours = m_leftHours - lastLeftHours;

                // Apply same delta to right-side
                m_rightHours += leftDeltaHours;
            }
            else
            {
                float lastRightHours = m_rightHours;
                m_rightHours -= PixelsToHours(deltaX);
                if (m_rightHours > 24.0f)
                {
                    m_rightHours = 24.0f;
                }
                float rightDeltaHours = m_rightHours - lastRightHours;

                // Apply same delta to left-side
                m_leftHours += rightDeltaHours;
            }
            CentreHours = (m_rightHours+m_leftHours)*0.5f;

            Hours = new Tuple<float, float>(m_leftHours, m_rightHours);
        }

        private void RefreshHourLimits()
        {
            m_visibleHours = ScaleFactor * ScheduleTime.HoursPerDay;

            m_leftHours = CentreHours - m_visibleHours*0.5f;
            if (m_leftHours < 0.0f)
            {
                m_leftHours = 0.0f;
            }

            m_rightHours = CentreHours + m_visibleHours*0.5f;
            if (m_rightHours > 24.0f)
            {
                m_rightHours = 24.0f;
            }

            CentreHours = (m_rightHours+m_leftHours)*0.5f;
            Hours = new Tuple<float, float>(m_leftHours, m_rightHours);
        }

        private float GetX(float hours) // 0.0 -> 24.0
        {
            return ((hours - m_leftHours) / (m_rightHours - m_leftHours)) * m_width;
        }

        private float HoursPerPixel()
        {
            return m_visibleHours / m_width;
        }

        private float PixelsToHours(float pixels)
        {
            return pixels * HoursPerPixel();
        }

		private void DrawText( Canvas canvas )
		{
			// Add time range here
            for(int hours=2; hours<=22; hours+=2)
			{
				String text = (hours % 24).ToString();
                float x = GetX((float)hours);
				x -= 1.0f;
				float y = MeasuredHeight * 0.6f;

                WFM.Android.Utilities.Helpers.drawCentredText(canvas, m_textPaint, text, x, y);
			}		
		}

        private void DrawLines( Canvas canvas )
        {
            Rect rect = new Rect();

            int lineWidth = 1;

            for (int hours = 1; hours <= 23; hours += 1) 
            {
				int y = (int)((hours % 2 == 0) ? MeasuredHeight * 0.75f : MeasuredHeight * 0.85f);
				int h = (int)((hours % 2 == 0) ? MeasuredHeight * 0.25f : MeasuredHeight * 0.15f);

                rect.Left = (int)GetX((float)hours);
                rect.Top = y;
                rect.Right = rect.Left + lineWidth;
                rect.Bottom = rect.Top + h;

                canvas.DrawRect (rect, m_linePaint);
            }       
        }

	}
}

