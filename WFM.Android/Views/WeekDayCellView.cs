﻿using System;
using Android.Widget;
using Android.Views;
using Android.Content;
using Android.Util;
using WFM.Core;
using Android.Graphics;
using Java.Lang;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Android
{
    public class WeekDayCellView : LinearLayout, View.IOnTouchListener
    {
        private ScheduleService m_scheduleService;
        private SpareShiftService m_spareShiftService;

        private TextView m_fadeOverlay;

        // Gesture stuff
        GestureDetector m_gestureDetector;

        private EmployeeData m_employeeData;
        public EmployeeData EmployeeData
        {
            get { return m_employeeData; }

            set 
            { 
                m_employeeData = value; 
                //m_info = m_employeeData == null ? null : m_employeeData.Info;
                //RefreshShiftViews (); 
            }
        }

        public int DayIndex { get; set; }

        public WeekDayCellView(Context context) : base(context) 
        { 
            Init();
        }

        public WeekDayCellView(Context context, IAttributeSet attrs ) : base(context, attrs) 
        { 
            Init();
        }

        public WeekDayCellView(Context context, IAttributeSet attrs, int defStyle ) : base(context, attrs, defStyle) 
        { 
            Init();
        }

        private void Init()
        { 
            m_scheduleService = Mvx.Resolve<ScheduleService> ();
            m_spareShiftService = Mvx.Resolve<SpareShiftService> ();

            SetupClickHandler();
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            /*m_fadeOverlay = new TextView (Context);
            m_fadeOverlay.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
            m_fadeOverlay.SetBackgroundColor (Color.Magenta);
            AddView (m_fadeOverlay);*/
        }
        private void SetupClickHandler()
        {
            m_gestureDetector = new GestureDetector (new GestureListener( this )); // Note we pass in 'this' class so the listener can get at the data
            SetOnTouchListener(this); 
        }

        public void ProcessDroppedShift(DateTime shiftStart, DateTime shiftEnd, int clampedMinutes, int oldEmployeeId, int shiftId)
        {
            
        }

        public void OnShiftReassign(View v, ShiftData shift, Point touchPoint)
        {
            
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            return m_gestureDetector.OnTouchEvent(e);
        }

        private void SendCreateShiftMessage()
        {
            // Can't add a shift to an existing employee.
            foreach (var day in m_employeeData.Days)
            {
                if (day.Shifts.Count > 0)
                {
                    return;
                }
            }

            DateTime centre = m_employeeData.Days [DayIndex].DayStart.AddHours (12);

            ShiftEditMessage request = new ShiftEditMessage(this) 
                {
                    Op = ShiftEditMessage.Operation.CreateDefaultShift,
                    EmployeeID = m_employeeData.EmployeeID,
                    Start = centre,
                    End = centre
                };

            Mvx.Resolve<IMvxMessenger> ().Publish (request);
        }

        public void UpdateCanAcceptShift(bool canAccept)
        {
            if (canAccept)
            {
                SetBackgroundColor (Color.White);
            }
            else
            {
                SetBackgroundColor (Color.Gray);
            }
        }

        private class GestureListener : GestureDetector.SimpleOnGestureListener 
        {
            private WeekDayCellView m_dayCellView;

            public GestureListener(WeekDayCellView dayCellView) : base()
            {
                m_dayCellView = dayCellView;
            }

            public override bool OnDown(MotionEvent e) 
            {
                return true; // Need 'true' to continue listening (for things like double-tap!)
            }

            public override bool OnSingleTapConfirmed (MotionEvent e)
            {
                return base.OnSingleTapConfirmed (e);
            }

            public override bool OnDoubleTap(MotionEvent e) 
            {
                if (m_dayCellView.EmployeeData.SpareShift && m_dayCellView.EmployeeData.CanEdit)
                {
                    m_dayCellView.SendCreateShiftMessage();
                }

                return true; 
            }          
        }
    }
}

