using System;
using Consortium.Client.Android;
using Android.Views;
using Android.Content;
using Android.Util;
using Android.Graphics;
using Android.OS;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using Android.Widget;
using WFM.Android.Helpers;
using Android.App;

namespace WFM.Android
{
    public class UnfilledShiftsView : ConsortiumPopoverView
    {
        private CollectionControl m_unfilledShiftsContentCollectionControl;
        private CollectionControl m_availableEmployeesCollectionControl;

        public UnfilledShiftsView() : base(Resource.Layout.view_unfilled_shifts_popover)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_UnfilledShiftPopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_UnfilledShiftPopup_Height);

            Gravity = GravityFlags.Center;
            BorderWidth = 3;
            BorderColor = Color.Black;
        }

        protected override void OnViewCreated(View view)
        {
            m_unfilledShiftsContentCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.unfilledShifts_content_collectionControl);
            m_unfilledShiftsContentCollectionControl.RegisterView<UnfilledShiftContentCell>();
            m_unfilledShiftsContentCollectionControl.CacheColorHint = Color.White;

            m_availableEmployeesCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.availableEmployees_content_collectionControl);
            m_availableEmployeesCollectionControl.RegisterView<AvailableEmployeeContentCell>();
            m_availableEmployeesCollectionControl.CacheColorHint = Color.White;
        }
    }
}