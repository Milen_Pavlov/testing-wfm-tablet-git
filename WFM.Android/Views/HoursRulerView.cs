using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.OS;
using Android.Util;
//using WFM.Android.Helpers;

namespace WFM.Android
{
	public class HoursRulerView : View
	{
		private Paint m_nextDayPaint;
		private Paint m_linePaint;
		private Paint m_textPaint;
		private int m_width;
		private int m_height;
		private int m_startHour;

		public HoursRulerView(Context context) : base(context) 
		{ 
			init();
		}

		public HoursRulerView(Context context, IAttributeSet attrs ) : base(context, attrs) 
		{ 
			init();
		}

		public HoursRulerView(Context context, IAttributeSet attrs, int defStyle ) : base(context, attrs, defStyle) 
		{ 
			init();
		}

		public int StartHour
		{
			get { return m_startHour; }
			set 
            { 
                m_startHour = value; 
                Invalidate();
            }
		}

		private void init()
		{ 
			// Setup Paints
			m_nextDayPaint = new Paint {Color = Color.DarkGray};

			m_textPaint = new Paint {Color = Color.White, StrokeWidth = 1};
			m_textPaint.AntiAlias = true;
			m_textPaint.TextSize = 15.0f;

			m_linePaint = new Paint {Color = Color.White, StrokeWidth = 1};
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			// Get the size of the control
			m_width = View.MeasureSpec.GetSize (widthMeasureSpec);
			m_height = View.MeasureSpec.GetSize (heightMeasureSpec);
			SetMeasuredDimension (m_width, m_height);
		}
		protected override void OnDraw( Canvas canvas )
		{
			drawNextDay( canvas );
			drawText( canvas );
			drawLines( canvas );
		}

		private void drawNextDay (Canvas canvas)
		{
			if (m_startHour != 0)
			{
				float nextStart = (m_width / 24.0f) * (24.0f - m_startHour);

				canvas.DrawRect( nextStart, 0, m_width, m_height, m_nextDayPaint);
			}
		}

		private void drawText( Canvas canvas )
		{
			// Add time range here
			for(int i=2; i<=22; i+=2)
			{
				String text = ((i + m_startHour) % 24).ToString();
				float x = ((float)i * ((float)m_width / 24.0f));
				x -= 1.0f;
				float y = WFM.Android.Utilities.Helpers.DPtoPX( 42.0f );

                WFM.Android.Utilities.Helpers.drawCentredText(canvas, m_textPaint, text, x, y);
			}		
		}

		private void drawLines( Canvas canvas )
		{
			Rect rect = new Rect();

			int lineWidth = 1; //Helpers.DPtoPX (1.0f);

			for (int i = 1; i <= 23; i += 1) {
				int y = (i % 2 == 0) ? 50 : 55;
                y = WFM.Android.Utilities.Helpers.DPtoPX(y);
				int h = (i % 2 == 0) ? 10 : 5;
                h = WFM.Android.Utilities.Helpers.DPtoPX(h);

				rect.Left = (int)((float)i * ((float)m_width / 24.0f));
				rect.Top = y;
				rect.Right = rect.Left + lineWidth;
				rect.Bottom = rect.Top + h;

				canvas.DrawRect (rect, m_linePaint);
			}		
		}

	}
}

