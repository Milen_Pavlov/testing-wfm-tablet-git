﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.App;

namespace WFM.Android
{
    public class EmployeeInfoView : ConsortiumPopoverView
    {
        public EmployeeInfoView() : base(Resource.Layout.view_employee)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_EmployeePopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_EmployeePopup_Height);
        }

        protected override void OnViewCreated(View view)
        {
        }
    }
}