using System;
using System.Collections.Generic;
using System.Linq;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using WFM.Core;
using Android.Graphics;
using Android.Util;
using Android.App;

namespace WFM.Android
{
    public class ShiftTimeView : ConsortiumPopoverView
    {
        NumberPicker m_startHour;
        NumberPicker m_startMin;
        NumberPicker m_endHour;
        NumberPicker m_endMin;

        public ShiftTimeViewModel VM
        {
            get { return (ShiftTimeViewModel)DataContext; }
        }

        public ShiftTimeView()
            : base(Resource.Layout.view_shift_time)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_TimePopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_TimePopup_Height);

            Gravity = GravityFlags.Center;

            BorderWidth = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Popup_Border_Width);
        }

        protected override void OnViewCreated(View view)
        {
            m_startHour = (NumberPicker)view.FindViewById(Resource.Id.shift_time_hour);
            m_startMin = (NumberPicker)view.FindViewById(Resource.Id.shift_time_mins);
            m_endHour = (NumberPicker)view.FindViewById(Resource.Id.shift_time_e_hours);
            m_endMin = (NumberPicker)view.FindViewById(Resource.Id.shift_time_e_mins);

            m_startHour.MinValue = 0;
            m_startHour.MaxValue = 23;
            m_startHour.Value = VM.Start.Hour;
            m_startHour.WrapSelectorWheel = true;
            m_startHour.DescendantFocusability = DescendantFocusability.BlockDescendants;

            m_startMin.MinValue = 0;
            m_startMin.MaxValue = 3;
            m_startMin.Value = VM.Start.Minute / 15;
            m_startMin.SetDisplayedValues (new string[] { "00", "15", "30", "45" });
            m_startMin.WrapSelectorWheel = true;
            m_startMin.DescendantFocusability = DescendantFocusability.BlockDescendants;

            m_endHour.MinValue = 0;
            m_endHour.MaxValue = 23;
            m_endHour.Value = VM.End.Hour;
            m_endHour.WrapSelectorWheel = true;
            m_endHour.DescendantFocusability = DescendantFocusability.BlockDescendants;

            m_endMin.MinValue = 0;
            m_endMin.MaxValue = 3;
            m_endMin.Value = VM.End.Minute / 15;
            m_endMin.SetDisplayedValues (new string[] { "00", "15", "30", "45" });
            m_endMin.WrapSelectorWheel = true;
            m_endMin.DescendantFocusability = DescendantFocusability.BlockDescendants;
 
            WFM.Android.Utilities.Helpers.SetNumberPickerTextColorAndSize(m_startHour, Color.Black, ComplexUnitType.Sp, 14, TypefaceStyle.Normal);
            WFM.Android.Utilities.Helpers.SetNumberPickerTextColorAndSize(m_startMin, Color.Black, ComplexUnitType.Sp, 14, TypefaceStyle.Normal);
            WFM.Android.Utilities.Helpers.SetNumberPickerTextColorAndSize(m_endHour, Color.Black, ComplexUnitType.Sp, 14, TypefaceStyle.Normal);
            WFM.Android.Utilities.Helpers.SetNumberPickerTextColorAndSize(m_endMin, Color.Black, ComplexUnitType.Sp, 14, TypefaceStyle.Normal);

            var applyButton = (Button)view.FindViewById(Resource.Id.apply_button);
            applyButton.Click += (object sender, EventArgs e) => 
                {
                    ApplyTimeChanges();
                };

            var cancelButton = (Button)view.FindViewById(Resource.Id.cancel_button);
            cancelButton.Click += (object sender, EventArgs e) => {
                    VM.OnCancel();
            };
        }

        void ApplyTimeChanges()
        {
            DateTime start, end;
            start = new DateTime(VM.Start.Year, VM.Start.Month, VM.Start.Day, m_startHour.Value, m_startMin.Value * 15, 0);
            end = new DateTime(VM.End.Year, VM.End.Month, VM.End.Day, m_endHour.Value, m_endMin.Value * 15, 0);

            //Without the check to modification type any decrease to start time pushed the shift onto the next day.
            if(!VM.ModifyingShift && start < VM.ShiftStartTime)
            {
                start = start.AddDays(1);
                end = end.AddDays(1);
            }
            else if(end < start)
            {
                // If the shift ends before the start time, it must end on the next day.
                end = end.AddDays(1);
            }

            VM.OnApply(start, end);
        }
    }
}
