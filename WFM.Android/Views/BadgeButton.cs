using Android.Graphics;
using Android.Widget;
using Android.Content;
using Android.Util;
using Android.Views;

namespace WFM.Android
{
    public class BadgeButton : Button
	{
		// View dimensions
		private int m_width;
		private int m_height;

		// Paint's
        private Paint m_discPaint;
        private Paint m_textPaint;

        // Misc
        bool m_badgeEnabled = true; // Badge is enabled by default
        float m_cxPixels;
        float m_cyPixels;
        float m_radiusPixels;
        string m_badgeText;

        public BadgeButton(Context context) : base(context) 
		{ 
			init();
		}

        public BadgeButton(Context context, IAttributeSet attrs ) : base(context, attrs) 
		{ 
			init();
		}

        public BadgeButton(Context context, IAttributeSet attrs, int defStyle ) : base(context, attrs, defStyle) 
		{ 
			init();
		}
            
		private void setupPaints()
		{
			m_discPaint = new Paint 
			{
				Color = Color.Red, 
				AntiAlias = true,
				StrokeWidth = 1.5f
			};
            m_discPaint.SetStyle(global::Android.Graphics.Paint.Style.Fill);

            m_textPaint = new Paint 
            {
                Color = Color.White,
                AntiAlias = true,
                StrokeWidth = 1.5f,
                TextSize = 17.0f,
                TextAlign = global::Android.Graphics.Paint.Align.Left,
            };
            m_textPaint.SetStyle(global::Android.Graphics.Paint.Style.Fill);
            m_textPaint.SetTypeface(Typeface.Create( Typeface.DefaultBold, TypefaceStyle.Bold));
		}
            
		private void init()
		{ 
			setupPaints ();
		}

        public static void drawDiscWithBorder(Canvas canvas, Paint paint, float cx, float cy, float radius, Color color, Color borderColor, float borderWidth )
        {
            paint.SetStyle(global::Android.Graphics.Paint.Style.Fill);
            paint.Color = color;
            canvas.DrawCircle(cx, cy, radius, paint);

            paint.SetStyle(global::Android.Graphics.Paint.Style.Stroke);
            paint.StrokeWidth = borderWidth;
            paint.Color = borderColor;
            canvas.DrawCircle(cx, cy, radius, paint);
        }

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{
			// Get the size of the control
			m_width = View.MeasureSpec.GetSize(widthMeasureSpec);
			m_height = View.MeasureSpec.GetSize(heightMeasureSpec);
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);

            m_rect = new RectF()
            {
                Left = 0,
                Top = 0,
                Right = m_width,
                Bottom = m_height
            };
		}

        public void EnableBadge(bool enableOrDisable)
        {
            m_badgeEnabled = enableOrDisable;
        }

        public void SetBadgeProperties(float cxPixels, float cyPixels, float radiusPixels, float textSize, string text)
        {
            m_cxPixels = cxPixels;
            m_cyPixels = cyPixels;
            m_radiusPixels = radiusPixels;
            m_textPaint.TextSize = textSize;
            m_badgeText = text;
        }

		protected override void OnDraw(Canvas canvas)
		{
			base.OnDraw(canvas);

            if (m_badgeEnabled)
            {
                drawDiscWithBorder(canvas, m_discPaint, m_cxPixels, m_cyPixels, m_radiusPixels, Color.Red, Color.White, 1.5f);

                float textY = m_cyPixels - ((m_textPaint.Descent() + m_textPaint.Ascent()) * 0.5f);
                Helpers.drawCentredText(canvas, m_textPaint, m_badgeText, m_cxPixels, textY);
            }
		}

	}
}

