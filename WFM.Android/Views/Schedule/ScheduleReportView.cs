﻿using System;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.App;

namespace WFM.Android
{
    [Activity(Label = "ScheduleReportView")]
    public class ScheduleReportView : ConsortiumFragment
    {        
        public override Type AttachTarget 
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        public ScheduleReportView() : base(Resource.Layout.view_schedule_report)
        {
            Title = "ScheduleReport";
        }
    }
}

