﻿using System;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.App;
using Com.ShinobiControls.Charts;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Graphics;
using System.Collections.Generic;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Android
{
	[Activity(Label = "ScheduleGraphView")]
    public class ScheduleGraphView : ConsortiumFragment, IShinobiChartOnAxisRangeChangeListener
	{        
        private ChartView m_ChartView;
        private IShinobiChart m_graph;
        private CollectionControl m_legend;

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        ScheduleGraphDataAdapter UnderSource { get; set; }
        ScheduleGraphDataAdapter CorrectSource { get; set; }
        ScheduleGraphDataAdapter OverSource { get; set; }
        ScheduleGraphDataAdapter DemandSource { get; set; }

        private const int c_minimumMaxYValue = 10;

        public AccuracyData Data
        {
            get
            {
                return null;
            }

            set
            {
                SetDefaultRange();
                AnimateSeries();

                if(m_graph!=null)
                    m_graph.RedrawChart();
            }
        }

		public ScheduleGraphView() : base(Resource.Layout.view_schedule_graph)
		{
			Title = "ScheduleGraph";
		} 

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            if (savedInstanceState == null)
            {
                m_ChartView = (ChartView)View.FindViewById(Resource.Id.chart);

                m_graph = m_ChartView.ShinobiChart;

                m_legend = (CollectionControl)View.FindViewById (Resource.Id.legend);
                m_legend.RegisterView<LegendCell> ();

                UnderSource = new ScheduleGraphDataAdapter(ScheduleGraphDataAdapter.ScheduleType.Under);
                CorrectSource = new ScheduleGraphDataAdapter(ScheduleGraphDataAdapter.ScheduleType.Correct);
                OverSource = new ScheduleGraphDataAdapter(ScheduleGraphDataAdapter.ScheduleType.Over);
                DemandSource = new ScheduleGraphDataAdapter(ScheduleGraphDataAdapter.ScheduleType.Demand);

                StyleGraph();
                BuildSeries();

                // View Model Binding
                var set = this.CreateBindingSet<ScheduleGraphView, ScheduleGraphViewModel>();

                set.Bind(UnderSource).For(c => c.Data).To(m => m.Accuracy);
                set.Bind(CorrectSource).For(c => c.Data).To(m => m.Accuracy);
                set.Bind(OverSource).For(c => c.Data).To(m => m.Accuracy);
                set.Bind(DemandSource).For(c => c.Data).To(m => m.Accuracy);

                set.Bind(UnderSource).For(c => c.DayIndex).To(m => m.DayIndex);
                set.Bind(CorrectSource).For(c => c.DayIndex).To(m => m.DayIndex);
                set.Bind(OverSource).For(c => c.DayIndex).To(m => m.DayIndex);
                set.Bind(DemandSource).For(c => c.DayIndex).To(m => m.DayIndex);

                set.Bind(this).For(v => v.Data).To(m => m.Accuracy);
                set.Apply();
            }
        }

        private void StyleGraph()
        {
            var xAxis = new DateTimeAxis()
            {
                Style = new AxisStyle()
                {
                    InterSeriesPadding = 0.1f
                },
                GesturePanningEnabled = true,
                GestureZoomingEnabled = true,
                MomentumPanningEnabled = true,
                MomentumZoomingEnabled = true,
                AnimationEnabled = true
            };

            m_graph.AddXAxis(xAxis);

            var yAxis = new NumberAxis ()
            {
                RangePaddingHigh = 5,
                GesturePanningEnabled = true,
                GestureZoomingEnabled = true,
                MomentumPanningEnabled = true,
                MomentumZoomingEnabled = true,
                MajorTickFrequency = 1,
                LabelFormat = (Java.Text.DecimalFormat)Java.Text.NumberFormat.IntegerInstance,

                Style = new AxisStyle()
                {
                    GridlineStyle = new GridlineStyle() 
                    { 
                        GridlinesShown = true, 
                        LineWidth = 1f, 
                        LineColor = Resources.GetColor(Resource.Color.darkGrey) 
                    },
                    TickStyle = new TickStyle()
                    {
                        LabelTextSize = 12f
                    }
                },
                AnimationEnabled = false
            };
                        
            m_graph.AddYAxis(yAxis);

            m_graph.Legend.LegendPlacement = Legend.Placement.InsidePlotArea;

            m_graph.Legend.Style = new LegendStyle()
            {
                BackgroundColor = Resources.GetColor(Resource.Color.white),
                BorderColor = Resources.GetColor(Resource.Color.primary),
                BorderWidth = 1f,
                SymbolsShown = true,
                TextSize = 15,
                SymbolAlignment = Legend.SymbolAlignment.Right,
                CornerRadius = 8,
                SymbolCornerRadius = 8,
                Padding = 6
            };

            //We have our own cutom key now so hide it for now
            m_graph.Legend.Visibility = ViewStates.Gone;

            m_graph.Style = new ChartStyle()
            {
                BackgroundColor = Resources.GetColor(Resource.Color.white)
            };

            m_graph.RedrawChart();
            m_graph.SetOnAxisRangeChangeListener (this);
        }

        void BuildSeries()
        {
            //This fixes issues with the graph initial animation 
            foreach(Series series in m_graph.Series)
            {
                series.AnimationEnabled = false;
            }

            var pendingSeriesList = new List<Series>(m_graph.Series);

            foreach(Series series in pendingSeriesList)
            {
                m_graph.RemoveSeries(series);
            }

            IStringService localiser = Mvx.Resolve<IStringService>();

            ColumnSeries underSeries = BuildSeries(localiser.Get("under_covered"), Resources.GetColor(Resource.Color.column_under));
            underSeries.DataAdapter = UnderSource;
            m_graph.AddSeries(underSeries);

            ColumnSeries correctSeries = BuildSeries(localiser.Get("correct_covered"), Resources.GetColor(Resource.Color.column_correct));
            correctSeries.DataAdapter = CorrectSource;
            m_graph.AddSeries(correctSeries);

            ColumnSeries overSeries = BuildSeries(localiser.Get("over_covered"), Resources.GetColor(Resource.Color.column_over));
            overSeries.DataAdapter = OverSource;
            m_graph.AddSeries(overSeries);

            LineSeries demandSeries = new LineSeries();
            demandSeries.Title = localiser.Get("demand_label");
            demandSeries.Baseline = 0;
            demandSeries.CrosshairEnabled = true;

             
            demandSeries.Style = new LineSeriesStyle()
            {
                LineWidth = 2,
                LineColor = Resources.GetColor(Resource.Color.line_demand),
                FillStyle = SeriesStyle.FillStyle.None,
                PointStyle = new PointStyle()
                {
                    PointsShown = true,
                    InnerRadius = 1,
                    Radius = 2,
                    Color = Resources.GetColor(Resource.Color.line_demand)
                }
            };

            demandSeries.DataAdapter = DemandSource;
            m_graph.AddSeries(demandSeries);

            SetDefaultRange();
            AnimateSeries();
        }

        public override void OnPause ()
        {
            base.OnPause ();

            if (m_ChartView != null)
                m_ChartView.OnPause ();
        }

        public override void OnResume ()
        {
            base.OnResume ();

            if (m_ChartView != null)
                m_ChartView.OnResume ();
        }

        ColumnSeries BuildSeries(string title, Color colour)
        {
            ColumnSeries series = new ColumnSeries();
            series.Title = title;
            series.Style.AreaColor = colour;
            series.Style.LineColor = colour;
            series.Style.AreaColorGradient = colour;
            series.Style.FillStyle = SeriesStyle.FillStyle.Flat;

            series.CrosshairEnabled = true;
            series.Baseline = 0;
            series.SeriesSelectionMode = Series.SelectionMode.PointSingle;
            series.StackId = 0;
            series.AnimationEnabled = false;
            series.Hidden = true; 
            series.EntryAnimation = SeriesAnimation.CreateGrowVerticalAnimation();
            series.EntryAnimation.Duration = 1;
            series.ExitAnimation.Duration = 0;

            return series;
        }

        private void SetDefaultRange()
        {
            NumberAxis rangeCheckAxis = m_graph.YAxis as NumberAxis;

            if(rangeCheckAxis != null)
            {
                double currentMinimum = rangeCheckAxis.CurrentDisplayedRange.Minimum;
                double minimum = !double.IsNaN(currentMinimum) && !double.IsInfinity(currentMinimum) ? currentMinimum : 0;

                if(rangeCheckAxis.DataRange.Span < 10)
                {
                    rangeCheckAxis.DefaultRange = new NumberRange(minimum, c_minimumMaxYValue);
                }
                else
                {
                    double dataMinimum = rangeCheckAxis.DataRange.Minimum;
                    double dataMaximum = rangeCheckAxis.DataRange.Maximum;

                    if(!double.IsInfinity(dataMinimum) && !double.IsInfinity(dataMaximum))
                    {
                        rangeCheckAxis.DefaultRange = rangeCheckAxis.DataRange;
                    }
                }
            }
        }

        //Perform the startup 'bounce' to the data in the graph
        void AnimateSeries()
        {
            if (m_graph != null)
                m_graph.SetHidden (m_graph.Series, true);

            foreach (Series series in m_graph.Series)
            {
                var col = series as ColumnSeries;

                if (col != null)
                {
                    series.AnimationEnabled = true;
                }
            }

            if (m_graph != null)
                m_graph.SetHidden (m_graph.Series, false);
        }

        public void OnAxisRangeChange (Com.ShinobiControls.Charts.Axis axis)
        {
            if (axis.AxisOrientation == Com.ShinobiControls.Charts.Axis.Orientation.Vertical)
            {
                NumberAxis numAxis = axis as NumberAxis;
                if (numAxis != null)
                {
                    double span = numAxis.CurrentDisplayedRange.Span;

                    //TODO Ideally want to come up with some nice formula for handling the axis splits
                    //     WE have to do this manually because Tesco only want integer values in the axis
                    //     Setting the label on the axis' to be Integers works, but if there is a 9.1, 9.2, and 9.3 axis
                    //     it will draw 3 axis of value 9...so this looks wrong
                    //     to get over this the MajorTickFrequence can be set to 1, but then you only EVER get axis on each
                    //     integer value, which is too frequent for large datasets, hence the crap below...
                    if (span < 15)
                        numAxis.MajorTickFrequency = 1;
                    else
                    {
                        //10 * 30, 60, 150
                        double max = 0;

                        int i = 10;
                        while (true)
                        {
                            if (span > 3 * i && span > 6 * i && span > 15 * i)
                                i *= 10;
                            else
                            {
                                if (span < 3 * i)
                                    max = (3 * i) / 6;
                                else if (span < 6 * i)
                                    max = (6 * i) / 6;
                                else if (span < 15 * i)
                                    max = (15 * i) / 6;

                                break;
                            }
                        }

                        numAxis.MajorTickFrequency = max;
                    }
                }
            }
        }
    }
}