using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Consortium.Client.Android;
using WFM.Core;
using Dalvik.SystemInterop;
using System.Collections.Generic;
using System.Linq;

namespace WFM.Android
{
	[Activity(Label = "ScheduleGridView")]
    public class ScheduleGridView : ConsortiumFragment
	{
        private ImageView m_employeeNameSortImageView;
        private TextView m_hoursTotalTextView;
        private TextView m_demandTotalTextView;
        private ImageView m_sortedByHoursView;
        private FrameLayout m_weekTotalView;
        private CollectionControl m_headerCollectionControl;
        private DragCollectionControl m_contentCollectionControl;

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        public ScheduleGridViewModel VM
        {
            get { return (ScheduleGridViewModel)this.ViewModel; }
        }

        public SortState NameSort
        {
            get { return null; }
            set 
            { 
                m_employeeNameSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_employeeNameSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public SortState HourSort
        {
            get { return null; }
            set 
            { 
                m_sortedByHoursView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_sortedByHoursView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public ScheduleData Schedule
        {
            get { return null; }
            set 
            {
                if (value == null)
                    return;

                if (value.DayIndex.HasValue)
                {
                    RegisterDayContentCells();
                }
                else
                {
                    RegisterWeekContentCells();
                }
                    
                // This needs to be looked at to match iOS.
                m_contentCollectionControl.InvalidateViews();                                
            }
        }

        public int ScrollToIndex
        {
            get { return 0; }
            set { ScrollCollectionToIndex(value); }
        }

        void RegisterDayContentCells()
        {
            if (m_headerCollectionControl.NumColumns != 1)
            {
                m_contentCollectionControl.RegisterViewWithResourceID<ScheduleGridDayContentCell>(Resource.Layout.cell_day_content);
                m_headerCollectionControl.NumColumns = 1;
                m_headerCollectionControl.RegisterViewWithResourceID<DayHeaderCell>(Resource.Layout.cell_day_header);
                m_contentCollectionControl.Items = Employees;
                VM.ShowSpareShifts = false;
                m_contentCollectionControl.HasFooter = false;
                VM.IsWeekView = false;
            }
        }

        void RegisterWeekContentCells()
        {
            if (m_headerCollectionControl.NumColumns != 7)
            {
                m_contentCollectionControl.RegisterViewWithResourceID<ScheduleGridWeekContentCell>(Resource.Layout.cell_schedule_grid_week_content);
                m_headerCollectionControl.NumColumns = 7;
                m_headerCollectionControl.RegisterViewWithResourceID<WeekHeaderCell>(Resource.Layout.cell_week_header);
                m_contentCollectionControl.RegisterFooter<ScheduleGridWeekFooterCell> ();
                m_contentCollectionControl.HasFooter = true;
                m_contentCollectionControl.ParentViewModel = VM;

                if (Employees != null)
                    m_contentCollectionControl.Items = Employees.Where (x => x.SpareShift == false).ToList ();
                else
                    m_contentCollectionControl.Items = Employees;
                VM.IsWeekView = true;
            }
        }
            
        private List<EmployeeData> m_employees;
        public List<EmployeeData> Employees
        {
            get
            {
                return m_employees;
            }
            set
            {
                m_employees = value;
                if (IsWeekView)
                    m_contentCollectionControl.Items = value.Where (x => x != null && x.SpareShift == false).ToList();
                else
                    m_contentCollectionControl.Items = value;
            }
        }

        public bool IsWeekView { get; set; }

        public ScheduleGridView() : base(Resource.Layout.view_schedule_grid)
		{
			Title = "ScheduleGrid";
		}

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            m_employeeNameSortImageView = (ImageView)view.FindViewById(Resource.Id.sort_imageView);
            m_hoursTotalTextView = (TextView)view.FindViewById(Resource.Id.total_hours_textView);
            m_demandTotalTextView = (TextView)view.FindViewById(Resource.Id.total_demand_textView);
            m_sortedByHoursView = (ImageView)view.FindViewById(Resource.Id.sort_hour_imageView);
            m_weekTotalView = (FrameLayout) view.FindViewById(Resource.Id.weekTotalView);
            m_headerCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.header_collectionControl);
            m_contentCollectionControl = (DragCollectionControl)view.FindViewById(Resource.Id.content_collectionControl);
            m_weekTotalView.Click += (s, e) => VM.OnHourHeaderSelect();

            var set = this.CreateBindingSet<ScheduleGridView, ScheduleGridViewModel>();
            set.Bind (this).For(b => b.NameSort).To (m => m.NameSort);
            set.Bind (this).For(b => b.HourSort).To (m => m.HourSort);

            set.Bind(this).For(b => b.ScrollToIndex).To(m => m.ScrollToEmployeeAtIndex);
            set.Bind (this).For(b => b.Schedule).To (m => m.Schedule);
            set.Bind(m_hoursTotalTextView).To(m => m.TotalHours);
            set.Bind(m_demandTotalTextView).To(m => m.TotalDemand);
            set.Bind(m_headerCollectionControl).To (m => m.Headers);
            set.Bind(this).For(b => b.IsWeekView).To(m => m.IsWeekView);
            set.Bind(this).For(b => b.Employees).To(m => m.Employees);
            set.Bind(m_contentCollectionControl).For(b => b.CustomCommand1).To("OnEmployeeNameSelected");
            set.Bind(m_contentCollectionControl).For(b => b.CustomCommand2).To("OnShiftSelected");
            set.Bind(m_contentCollectionControl).For(b => b.CustomCommand3).To("OnWarningSelected");
            set.Bind(m_contentCollectionControl).For(b => b.CustomCommand4).To("OnTimeOffSelected");
            set.Bind(m_headerCollectionControl).For (b => b.SelectedCommand).To ("OnHeaderSelect");
            set.Apply();
        }

        void ScrollCollectionToIndex(int value)
        {
            m_contentCollectionControl.ClearFocus();
            m_contentCollectionControl.Post(() => 
            {
                m_contentCollectionControl.RequestFocusFromTouch();
                m_contentCollectionControl.SetSelection(value);
                m_contentCollectionControl.RequestFocus();
            });
        }
	}
}