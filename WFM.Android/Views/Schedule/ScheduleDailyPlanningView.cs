﻿using System;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.App;
using WFM.Core;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using Android.Widget;
using Android.Graphics.Drawables;
using System.Globalization;

namespace WFM.Android
{
	[Activity(Label = "ScheduleDailyPlanningView")]
    public class ScheduleDailyPlanningView : ConsortiumFragment
	{        
        private CollectionControl m_headerCollectionControl;
        private CollectionControl m_contentCollectionControl;
        private ImageView m_sortedImageView;

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public List<ColumnHeader> Headers
        {
            get { return null; }
            set
            {
                if (value.Count == 1)
                {
                    m_headerCollectionControl.NumColumns = 1;
                    m_headerCollectionControl.RegisterViewWithResourceID<DayHeaderCell>(Resource.Layout.cell_day_header);
                    m_contentCollectionControl.RegisterViewWithResourceID<SchedulePlanningDayContentCell>(Resource.Layout.cell_schedule_planning_day_content);
                }
                else
                {
                    m_headerCollectionControl.NumColumns = 7;
                    m_headerCollectionControl.RegisterViewWithResourceID<WeekHeaderCell>(Resource.Layout.cell_week_header);
                    m_contentCollectionControl.RegisterViewWithResourceID<SchedulePlanningWeekContentCell>(Resource.Layout.cell_schedule_planning_week_content);
                }
            }
        }

        public bool SortedAscending
        {
            get{return false;}
            set
            {
                m_sortedImageView.SetImageResource(value ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
            }
        }

        public bool IsSorted
        {
            get{ return false;}
            set
            {
                m_sortedImageView.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

		public ScheduleDailyPlanningView() : base(Resource.Layout.view_schedule_daily_planning)
		{
			Title = "ScheduleDailyPlanning";
		}

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            m_headerCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.header_collectionControl);
            m_contentCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.content_collectionControl);
            m_sortedImageView = (ImageView)view.FindViewById(Resource.Id.sort_imageView);

            var set = this.CreateBindingSet<ScheduleDailyPlanningView, ScheduleDailyPlanningViewModel>();
            set.Bind(this).For(b => b.Headers).To(m => m.Headers);
            set.Bind(m_headerCollectionControl).To(m => m.Headers);
            set.Bind(m_contentCollectionControl).To(m => m.Planning);
            set.Bind(this).For(b => b.SortedAscending).To(m => m.SortedAscending);
            set.Bind(this).For(b => b.IsSorted).To(m => m.IsSorted);
            set.Apply();
        }
	}
}

