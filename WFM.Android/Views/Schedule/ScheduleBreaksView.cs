﻿using System;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.App;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Widget;

namespace WFM.Android
{
    [Activity(Label = "ScheduleBreaksView")]
    public class ScheduleBreaksView : ConsortiumFragment
    {        
        private CollectionControl m_contentCollectionControl;
        private FrameLayout m_tabLoading;
        private ProgressBar m_progressBar;

        public bool Loading 
        {
            get{ return false; }
            set
            {
                m_tabLoading.Visibility = value ? ViewStates.Visible : ViewStates.Gone;
                m_progressBar.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
                m_contentCollectionControl.Visibility =  value ? ViewStates.Invisible : ViewStates.Visible;
            }
        }

        public bool NoData 
        {
            get{ return false; }
            set
            {
                m_progressBar.Visibility = value ? ViewStates.Invisible : ViewStates.Visible;
            }
        }

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public ScheduleBreaksView() : base(Resource.Layout.view_schedule_breaks)
        {
            Title = "ScheduleBreaks";
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            m_contentCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.content_collectionControl);
            m_contentCollectionControl.RegisterViewWithResourceID<ScheduleBreaksContentCell>(Resource.Layout.cell_breaks);
            m_tabLoading = view.FindViewById(Resource.Id.breaksLoadingTab) as FrameLayout;
            m_progressBar = view.FindViewById(Resource.Id.breaksLoadingProgress) as ProgressBar;

            var set = this.CreateBindingSet<ScheduleBreaksView, ScheduleBreaksViewModel>();
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(b => b.NoData).To(m => m.NoData);
            set.Apply();
        }
    }
}