﻿using System;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.App;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Android.Widget;
using System.Collections.Generic;

namespace WFM.Android
{
    [Activity(Label = "ResourcePlanningView")]
    public class ScheduleResourcePlanningView : ConsortiumFragment
    {        
        private CollectionControl m_planningCollection;
        private ImageView m_departmentNameSortImageView;
        private ImageView m_plannedHoursSortImageView;
        private ImageView m_scheduledHoursSortImageView;
        private ImageView m_overtimeRequiredSortImageView;
        private ImageView m_overtimeAgreedSortImageView;
		private ImageView m_contractedSortImageView;
		private ImageView m_timeOffSortImageView;
        private FrameLayout m_tabLoading;
        private ProgressBar m_progressBar;
        private Button m_resourceauditbutton;

        public List<ResourcePlanningItem> Rows
        {
            get
            {
                return null;
            }
            set
            {
                m_planningCollection.Items = value;
            }
        }

        public SortState DepartmentSort
        {
            get { return null; }
            set 
            { 
                ResetSortIconsState();

                m_departmentNameSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_departmentNameSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

		public SortState ContractSort
		{
			get { return null; }
			set 
			{ 
				ResetSortIconsState();

				m_contractedSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
				m_contractedSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
			}
		}

        public SortState PlannedHoursSort
        {
            get { return null; }
            set 
            { 
                ResetSortIconsState();

                m_plannedHoursSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_plannedHoursSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public SortState ScheduledHoursSort
        {
            get { return null; }
            set 
            { 
                ResetSortIconsState();

                m_scheduledHoursSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_scheduledHoursSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public SortState OvertimeRequiredSort
        {
            get { return null; }
            set 
            { 
                ResetSortIconsState();

                m_overtimeRequiredSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_overtimeRequiredSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public SortState OvertimeAgreedSort
        {
            get { return null; }
            set 
            { 
                ResetSortIconsState();

                m_overtimeAgreedSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_overtimeAgreedSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

		public SortState TimeOffSort
		{
			get { return null; }
			set 
			{ 
				ResetSortIconsState();

				m_timeOffSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
				m_timeOffSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
			}
		}

        void ResetSortIconsState()
        {
			m_departmentNameSortImageView.Visibility = m_timeOffSortImageView.Visibility = m_contractedSortImageView.Visibility = m_plannedHoursSortImageView.Visibility = m_scheduledHoursSortImageView.Visibility = m_overtimeRequiredSortImageView.Visibility = m_overtimeAgreedSortImageView.Visibility = ViewStates.Invisible;
        }

        public override Type AttachTarget 
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        public ScheduleResourcePlanningView() : base(Resource.Layout.view_resource_planning)
        {
            Title = "ResourcePlanning";
        }
            
        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            m_departmentNameSortImageView = (ImageView)view.FindViewById(Resource.Id.sort_planning_department);
            m_plannedHoursSortImageView = (ImageView)view.FindViewById(Resource.Id.sort_planning_plannedhours);
            m_scheduledHoursSortImageView = (ImageView)view.FindViewById(Resource.Id.sort_planning_scheduledhours);
            m_overtimeRequiredSortImageView = (ImageView)view.FindViewById(Resource.Id.sort_planning_overtimerequired);
			m_contractedSortImageView = (ImageView)view.FindViewById (Resource.Id.sort_planning_contracted);
			m_timeOffSortImageView = (ImageView)view.FindViewById (Resource.Id.sort_planning_timeoff);
			m_overtimeAgreedSortImageView = (ImageView)view.FindViewById(Resource.Id.sort_planning_overtimeagreed);

			m_resourceauditbutton = (Button)view.FindViewById(Resource.Id.resourceauditbutton);
            m_resourceauditbutton.Click += ResourceAuditClick;
            
            m_tabLoading = (FrameLayout)view.FindViewById(Resource.Id.resourcePlanningLoading);
            m_progressBar = (ProgressBar)view.FindViewById(Resource.Id.resourcePlanningProgress);

            m_planningCollection = (CollectionControl)view.FindViewById(Resource.Id.content_resourcePlanningCollectionControl);

            m_planningCollection.RegisterView<ResourcePlanningCell>();

            var set = this.CreateBindingSet<ScheduleResourcePlanningView, ScheduleResourcePlanningViewModel>();
            set.Bind (this).For(b => b.DepartmentSort).To (m => m.DepartmentNameSortState);
            set.Bind (this).For(b => b.PlannedHoursSort).To (m => m.PlannedHoursSortState);
            set.Bind (this).For(b => b.ScheduledHoursSort).To (m => m.ScheduledHoursSortState);
            set.Bind (this).For(b => b.OvertimeRequiredSort).To (m => m.OvertimeRequiredSortState);
            set.Bind (this).For(b => b.OvertimeAgreedSort).To (m => m.OvertimeAgreedSortState);
			set.Bind (this).For(b => b.ContractSort).To (m => m.ContractedSortState);
			set.Bind (this).For(b => b.TimeOffSort).To (m => m.TimeOffSortState);
            set.Bind(this).For(b => b.Rows).To(m => m.Rows);
            set.Apply();
        }

        void ResourceAuditClick (object sender, EventArgs e)
        {            
            ConsortiumPopoverView.SourceView = m_resourceauditbutton;

            ((ScheduleResourcePlanningViewModel)ViewModel).ShowAudit();
        }
    }
}