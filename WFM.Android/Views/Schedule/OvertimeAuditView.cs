﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using Consortium.Client.Core;
using WFM.Core;
using Android.App;

namespace WFM.Android
{
    public class OvertimeAuditView : ConsortiumPopoverView
    {
        public OvertimeAuditView()
            : base(Resource.Layout.view_overtimeaudit)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_ShiftPopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_ShiftPopup_Height);

            Gravity = GravityFlags.Center;

            BorderWidth = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Popup_Border_Width);
        }     

        #region implemented abstract members of ConsortiumPopoverView

        protected override void OnViewCreated(View view)
        {
        }

        #endregion
    }
}
