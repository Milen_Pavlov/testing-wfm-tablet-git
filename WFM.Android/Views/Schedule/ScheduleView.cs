using System;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.App;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Windows.Input;
using Android.Widget;
using Android.Content;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Android
{
    public class ScheduleView : ConsortiumTabFragment
	{
        private FilterControl m_filterPopupControl;

        private SegmentedControl m_tabSegmentedControl;
        private SegmentedControl m_daySegmentedControl;
        private const int c_topBottomMarginDp = 4;
        private const int c_segmentWidth = 90;

        private BarButton m_refreshButton;
        private BarButton m_filterButton;
        private BarButton m_startTime;
        private BarButton m_editButton;
        private BarButton m_unfilledShiftsButton;
        private BarButton m_undoButton;

        private TextView m_accuracyLabel;
        private TextView m_accuracyPercent;

        private int[] m_editButtonImages;

        private LinearLayout m_scheduleTitle;
        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;

        public ScheduleViewModel VM { get { return (ScheduleViewModel)this.ViewModel; } }

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        public bool Loading
        {
            get { return false; }
            set 
            {
                m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Gone; 
                m_loadingProgress.Visibility = value? ViewStates.Visible : ViewStates.Invisible;
                m_refreshButton.Enabled = !value;
            }
        }

        public bool NoData
        {
            get { return false; }
            set 
            {
                if(value)
                {
                    m_loadingProgress.Visibility = ViewStates.Invisible;
                }
            }
        }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set 
            { 
                m_editMode = value; 
                m_editButton.SetImageResource(m_editButtonImages[value ? 1: 0]);

                if (value)
                    m_undoButton.Visibility = ViewStates.Visible;
                else
                    m_undoButton.Visibility = ViewStates.Invisible;
            }
        }

        public bool UndoEnabled
        {
            get
            {
                return false;
            }

            set
            {
                if(value)
                {
                    m_undoButton.Clickable = true;
                    m_undoButton.Alpha = 1f;
                }
                else
                {
                    m_undoButton.Clickable = false;
                    m_undoButton.Alpha = 0.5f;
                }
            }
        }

        private bool m_canFilterByTime;
        public bool CanFilterByTime
        {
            get { return m_canFilterByTime; }
            set { m_canFilterByTime = value; UpdateUIBarButtons(); }
        }

        public ScheduleView() : base(Resource.Layout.view_schedule, Resource.Id.tab_content_layout)
        {
            m_editButtonImages = new int[] { Resource.Drawable.icon_edit, Resource.Drawable.icon_confirm };
        }

		public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            m_filterPopupControl = new FilterControl(Application.Context);

            m_tabSegmentedControl = new SegmentedControl (ConsortiumApp.Context) {
                MinWidth = (int)DimensionUtility.ConvertDpToPixel(c_segmentWidth),
                MaxWidth = (int)DimensionUtility.ConvertDpToPixel(c_segmentWidth),
                SelectedColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.white),
                SelectedTextColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.primary),
                UnSelectedColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.primary),
                UnSelectedTextColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.white),
                LayoutParameters = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent, GravityFlags.CenterVertical),
            };

            IStringService localiser = Mvx.Resolve<IStringService>();
//            m_tabSegmentedControl.Segments = new List<string> (new string[] {
//                localiser.Get("schedule"),
//                localiser.Get("workload"),
//                localiser.Get("planning"),
//                localiser.Get("report"),
//                localiser.Get("breaks"),
//                localiser.Get("resources")
//            });

            m_tabSegmentedControl.OnSelectionChanged += HeaderSelectionChanged;
            NavBar.MiddleLayout.AddView (m_tabSegmentedControl);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += OnRefresh;
				
            m_startTime = new BarButton(ConsortiumApp.Context);
            m_startTime.SetImageResource(Resource.Drawable.icon_time);
            m_startTime.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_startTime.SetTopBottomMargin(c_topBottomMarginDp);
            m_startTime.Click += OnStartTime;

            m_editButton = new BarButton(ConsortiumApp.Context);
            m_editButton.SetImageResource(Resource.Drawable.icon_edit);
            m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_editButton.Click += OnEdit;

            m_filterButton = new BarButton(ConsortiumApp.Context);
            m_filterButton.SetImageResource(Resource.Drawable.icon_filter);
            m_filterButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_filterButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_filterButton.Click += OnFilter;

            m_unfilledShiftsButton = new BarButton(ConsortiumApp.Context);
            m_unfilledShiftsButton.SetImageResource(Resource.Drawable.icon_unfilled_shifts);
            m_unfilledShiftsButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_unfilledShiftsButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_unfilledShiftsButton.Click += OnUnfilledShifts;

            m_undoButton = new BarButton(ConsortiumApp.Context);
            m_undoButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_undoButton.Text = "Undo";
            m_undoButton.Click += OnUndo;

            UpdateUIBarButtons();
		}

        private void UpdateUIBarButtons()
        {
            var controlButtons = new List<BarButton>();
            controlButtons.Add(m_unfilledShiftsButton);
            controlButtons.Add(m_editButton);

            if(CanFilterByTime)
                controlButtons.Add(m_startTime);
            
            controlButtons.Add(m_filterButton);
            controlButtons.Add(m_refreshButton);

            List<BarButton> leftButtons = (NavBar.LeftButtons != null) ? new List<BarButton>(NavBar.LeftButtons) : new List<BarButton>();
            leftButtons.Add(m_undoButton);

            NavBar.RightButtons = controlButtons;
            NavBar.LeftButtons = leftButtons;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_daySegmentedControl = View.FindViewById (Resource.Id.day_segmented_control) as SegmentedControl;
            m_daySegmentedControl.OnSelectionChanged += DateSelectionChanged;

            m_loading = View.FindViewById (Resource.Id.tab_loading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarLoading) as ProgressBar;
            m_scheduleTitle = View.FindViewById(Resource.Id.scheduleTitle) as LinearLayout;

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleView, ScheduleViewModel>();
            set.Bind (m_tabSegmentedControl).For (v => v.Segments).To (vm => vm.TabNames);
            set.Bind (this).For (v => v.CanFilterByTime).To (vm => vm.CanFilterByTime);
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(b => b.EditMode).To(m => m.EditMode);
            set.Bind(m_daySegmentedControl).For(b => b.Segments).To(m => m.Segments);
            set.Bind(m_daySegmentedControl).For(b => b.SelectedIndex).To(m => m.SelectedDayIndex);
            set.Bind(m_filterPopupControl).For(c => c.CurrentFilter).To(vm => vm.CurrentFilter);
            set.Bind(m_filterPopupControl).For(c => c.Filters).To(vm => vm.Filters);
            set.Bind(m_filterPopupControl).For(c => c.Apply).To("OnApplyFilter");
            set.Bind(this).For(b => b.NoData).To(m => m.NoData);
            set.Bind(this).For(v => v.UndoEnabled).To(vm => vm.CanUndo);

            set.Apply();
        }

        public override void OnDestroy()
		{
            m_refreshButton.Click -= OnRefresh;
            m_startTime.Click -= OnStartTime;
            m_editButton.Click -= OnEdit;
            m_filterButton.Click -= OnFilter;
            m_unfilledShiftsButton.Click -= OnUnfilledShifts;
            m_tabSegmentedControl.OnSelectionChanged -= HeaderSelectionChanged;
            m_daySegmentedControl.OnSelectionChanged -= DateSelectionChanged;

            base.OnDestroy ();
		}

        void OnRefresh(object sender, EventArgs e) 
        {
            VM.OnRefresh();
        }

        void OnUndo(object sender, EventArgs e) 
        {
            VM.OnUndo();
        }

        void OnViewAudit(object sender, EventArgs e)
        {
            ConsortiumPopoverView.SourceView = m_unfilledShiftsButton;
            VM.OnViewAuditSelected();
        }

        void OnStartTime(object sender, EventArgs e) 
        {
            ConsortiumPopoverView.SourceView = m_startTime;
            VM.OnStartTimeSelected();
		}

        void OnEdit(object sender, EventArgs e) 
        {
            VM.OnEdit();
        }

        void OnFilter(object sender, EventArgs e) 
        {
            m_filterPopupControl.Show(m_filterButton,10, 10);
        }

        void OnUnfilledShifts(object sender, EventArgs e) 
        {
            ConsortiumPopoverView.SourceView = m_unfilledShiftsButton;
            VM.OnClickUnfilledShifts();
        }

        void HeaderSelectionChanged(int index)
		{
			VM.OnTabSelect(index);
		}

        void DateSelectionChanged(int index)
        {
            VM.OnWeekSelect (index);
        }
	}
}
