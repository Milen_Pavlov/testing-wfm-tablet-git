using System;
using System.Collections.Generic;
using System.Linq;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using WFM.Core;
using Android.App;

namespace WFM.Android
{
    public class ShiftTypeView : ConsortiumPopoverView
    {
        public ShiftTypeViewModel VM
        {
            get { return (ShiftTypeViewModel)DataContext; }
        }

        public ShiftTypeView()
            : base(Resource.Layout.view_shift_type)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_ShiftTypePopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_ShiftTypePopup_Height);

            Gravity = GravityFlags.Center;
            BorderWidth = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Popup_Border_Width);
        }

        protected override void OnViewCreated(View view)
        {
            var cancelButton = (Button)view.FindViewById(Resource.Id.cancel_button);
            cancelButton.Click += (object sender, EventArgs e) => {
                VM.OnCancel();
            };
        }
    }
}
