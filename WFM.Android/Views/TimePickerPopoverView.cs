﻿using Android.Views;
using Android.Widget;
using Consortium.Client.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFM.Android.Helpers;

namespace WFM.Android.Views
{
    // ToDo: Incomplete
    public class TimePickerPopoverView : ConsortiumPopoverView
    {
        public TimeSpan Value { get; set; }
        public TextView CurrentTime { get; set; }
        public Button CancelButton { get; set; }
        public Button OkButton { get; set; }

        public TimePickerPopoverView()
            : base(Resource.Layout.view_time_picker_popover)
        {
            Width = 400;
            Height = 400;
            Left = PixelHelper.LeftWhenCentered(Width);
            Top = PixelHelper.TopWhenCenteredInContentArea(Height);
            AbsolutePosition = true;
            BorderWidth = 4;
        }

        protected override void OnViewCreated(View view)
        {
            CurrentTime = (TextView)view.FindViewById(Resource.Id.current_time);
            CancelButton = (Button)view.FindViewById(Resource.Id.cancel_button);
            OkButton = (Button)view.FindViewById(Resource.Id.ok_button);

            CurrentTime.Text = Value.ToString(@"hh\:mm");

            CancelButton.Click += (sender, e) =>
            {
                Close();
            };

            OkButton.Click += (sender, e) =>
            {
            };
        }
    }
}
