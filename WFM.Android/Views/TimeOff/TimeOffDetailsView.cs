﻿using System;
using Consortium.Client.Android;
using WFM.Core;
using Android.OS;
using Android.Widget;
using Android.Views;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Linq;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class TimeOffDetailsView : ConsortiumTabFragment
    {
        private SegmentedControl m_segmentedControl;
        private const int c_topBottomMarginDp = 4;
        private const int c_segmentWidth = 120;

        private TextView m_fromDate;
        private TextView m_fromTime;

        private TextView m_endDate;
        private TextView m_endTime;

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        public TimeOffDetailsViewModel VM
        { 
            get { return (TimeOffDetailsViewModel)this.ViewModel; }
        }

        public string[] TabTitles
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    m_segmentedControl.Segments = value.ToList();
                }
            }
        }

        public bool DayOnly
        {
            get { return false; }
            set
            {
                m_fromTime.Visibility = (value) ? ViewStates.Invisible : ViewStates.Visible;
                m_endTime.Visibility = (value) ? ViewStates.Invisible : ViewStates.Visible;
            }
        }

        public TimeOffDetailsView () : base(Resource.Layout.view_timeoff_details, Resource.Id.tab_content_layout)
        {
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            m_segmentedControl = new SegmentedControl (ConsortiumApp.Context) {
                MinWidth = (int)DimensionUtility.ConvertDpToPixel(c_segmentWidth),
                MaxWidth = (int)DimensionUtility.ConvertDpToPixel(c_segmentWidth),
                SelectedColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.white),
                SelectedTextColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.primary),
                UnSelectedColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.primary),
                UnSelectedTextColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.white),
                LayoutParameters = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent, GravityFlags.CenterVertical),
            };

            m_segmentedControl.Segments = new List<string> (new string[] { "Pay Allocations", "Accruals", "Peer Requests", "Employee History" });

            m_segmentedControl.OnSelectionChanged += HeaderSelectionChanged;
            NavBar.MiddleLayout.AddView (m_segmentedControl);

        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_fromDate = view.FindViewById<TextView> (Resource.Id.timeoff_details_fromdate);
            m_fromTime = view.FindViewById<TextView> (Resource.Id.timeoff_details_fromtime);
            m_endDate = view.FindViewById<TextView> (Resource.Id.timeoff_details_enddate);
            m_endTime = view.FindViewById<TextView> (Resource.Id.timeoff_details_endtime);

            string dateFormat = Mvx.Resolve<IStringService>().Get("{0:dd/MM/yy}");

            var set = this.CreateBindingSet<TimeOffDetailsView, TimeOffDetailsViewModel>();
            set.Bind (this).For (v => v.TabTitles).To (vm => vm.TabTitles);
            set.Bind (this).For (v => v.DayOnly).To (vm => vm.TimeOffModel.IsTrackedAsWholeDays);
            set.Bind(m_fromDate).To(vm => vm.TimeOffModel.StartDate).WithConversion(new ToStringValueConverter<DateTime>(),dateFormat);
            set.Bind(m_fromTime).To(vm => vm.TimeOffModel.StartDate).WithConversion(new TimeToStringConverter());
            set.Bind(m_endDate).To(vm => vm.TimeOffModel.EndDate).WithConversion(new ToStringValueConverter<DateTime>(),dateFormat);
            set.Bind(m_endTime).To(vm => vm.TimeOffModel.EndDate).WithConversion(new TimeToStringConverter());
            set.Apply();
        }

        public override void OnDestroy()
        {
            m_segmentedControl.OnSelectionChanged -= HeaderSelectionChanged;

            base.OnDestroy ();
        }

        void HeaderSelectionChanged(int index)
        {
            VM.OnTabSelect(index);
        }
    }
}

