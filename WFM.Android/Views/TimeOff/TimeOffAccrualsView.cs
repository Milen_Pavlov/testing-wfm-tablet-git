﻿using System;
using Consortium.Client.Android;

namespace WFM.Android
{
    public class TimeOffAccrualsView : ConsortiumFragment
    {
        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public TimeOffAccrualsView () : base(Resource.Layout.view_timeoff_accruals)
        {
            Title = "Accruals";
        }
    }
}

