﻿using System;
using Consortium.Client.Android;
using WFM.Core;
using System.Collections.Generic;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class TimeOffView : ConsortiumFragment
    {
        private const int c_topBottomMarginDp = 4;

        private BarButton m_refreshButton;

        private CalendarGrid m_calendar;
        private CheckBox m_checkAll;
        private CheckBox m_checkPending;
        private CheckBox m_checkApproved;
        private CheckBox m_checkDenied;
        private TextView m_currentDateLabel;
        private RelativeLayout m_loadingView;

        public TimeOffViewModel VM { get { return (TimeOffViewModel)this.ViewModel; } }

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        public List<string> Segments
        {
            get { return null; }
            set 
            {
                List<string> segs = value;

                if (segs == null)
                    return;

                if(m_calendar!=null)
                    m_calendar.HeaderStrings = segs;
            }
        }

        public DateTime FirstOfMonth
        {
            get { return DateTime.MinValue; }
            set
            {
                if(m_calendar!=null)
                    m_calendar.SetMonth (value.Year, value.Month);

                //Set Label
                if (m_currentDateLabel != null)
                    m_currentDateLabel.Text = string.Format ("{0} {1}", value.ToString ("MMMM"), value.Year);
            }
        }

        //Don't remove this event, Mvvm will look for this and raise it so the VM updates
        public event EventHandler FilterChanged;
        private TimeOffStatus m_filter;
        public TimeOffStatus Filter
        {
            get { return m_filter; }
            set
            {
                m_filter = value;
                m_checkPending.Checked = m_filter.HasFlag (TimeOffStatus.Pending);
                m_checkApproved.Checked = m_filter.HasFlag (TimeOffStatus.Approved);
                m_checkDenied.Checked = m_filter.HasFlag (TimeOffStatus.Denied);
                m_checkAll.Checked = m_filter.HasFlag (TimeOffStatus.Pending) && m_filter.HasFlag (TimeOffStatus.Approved) && m_filter.HasFlag (TimeOffStatus.Denied);

                //Allow MVVM to inform the VM of the change.
                if (FilterChanged != null)
                    FilterChanged (Filter, EventArgs.Empty);
            }
        }

        private bool m_loading;
        public bool IsLoading
        {
            get { return m_loading; }
            set
            {
                m_loading = value;

                m_loadingView.Visibility = (m_loading) ? ViewStates.Visible : ViewStates.Gone;
                m_refreshButton.Enabled = !m_loading;
            }
        }

        public TimeOffView() : base(Resource.Layout.view_timeoff)
        {
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize (Resources.GetDimensionPixelSize (Resource.Dimension.rightButtonSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);

            var controlButtons = new List<BarButton>() { m_refreshButton };
            NavBar.RightButtons = controlButtons;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_calendar = View.FindViewById<CalendarGrid> (Resource.Id.timeoff_calendar);
            m_calendar.RegisterView<CalendarRequestCell> ();

            m_loadingView = View.FindViewById<RelativeLayout> (Resource.Id.loadingView);

            m_checkAll = View.FindViewById<CheckBox> (Resource.Id.timeoff_check_all);
            m_checkPending = View.FindViewById<CheckBox> (Resource.Id.timeoff_check_pending);
            m_checkApproved = View.FindViewById<CheckBox> (Resource.Id.timeoff_check_approved);
            m_checkDenied = View.FindViewById<CheckBox> (Resource.Id.timeoff_check_denied);

            m_currentDateLabel = View.FindViewById<TextView> (Resource.Id.timeoff_current_date);

            m_checkAll.Click += OnToggle;
            m_checkApproved.Click += OnToggle;
            m_checkDenied.Click += OnToggle;
            m_checkPending.Click += OnToggle;

            var set = this.CreateBindingSet<TimeOffView, TimeOffViewModel>();
            set.Bind (this).For (v => v.Segments).To (vm => vm.Segments);
            set.Bind (this).For (v => v.FirstOfMonth).To (vm => vm.FirstOfMonth);
            set.Bind (this).For (v => v.Title).To (vm => vm.Title);
            set.Bind (this).For (v => v.Filter).To (vm => vm.Filter);
            set.Bind (this).For (v => v.IsLoading).To (vm => vm.Loading);
            set.Bind (m_refreshButton).For ("Click").To ("Refresh");
            set.Apply();
        }

        public override void OnDestroy()
        {
            m_checkAll.Click -= OnToggle;
            m_checkApproved.Click -= OnToggle;
            m_checkDenied.Click -= OnToggle;
            m_checkPending.Click -= OnToggle;

            base.OnDestroy ();
        }

        public void OnTimeOffRequestSelected(JDATimeOffRequest row)
        {
            if (IsLoading)
                return;

            if (VM != null)
            {
//                if (m_dayDetailsPopover != null)
//                    m_dayDetailsPopover.Dismiss (true);

                VM.ShowTimeOffRequest (row);
            }
        }

        private void OnToggle(object sender, EventArgs args)
        {
            CheckBox cb = sender as CheckBox;
            //Update filter
            if (cb == m_checkAll)
            {
                if (cb.Checked)
                    Filter = TimeOffStatus.Approved | TimeOffStatus.Denied | TimeOffStatus.Pending;
                else
                    Filter = TimeOffStatus.None;
            }
            else if (cb == m_checkApproved)
            {
                if (cb.Checked)
                    Filter |= TimeOffStatus.Approved;
                else
                    Filter &= ~TimeOffStatus.Approved;
            }
            else if (cb == m_checkDenied)
            {
                if (cb.Checked)
                    Filter |= TimeOffStatus.Denied;
                else
                    Filter &= ~TimeOffStatus.Denied;
            }
            else if (cb == m_checkPending)
            {
                if (cb.Checked)
                    Filter |= TimeOffStatus.Pending;
                else
                    Filter &= ~TimeOffStatus.Pending;
            }
        }
    }
}

