﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.OS;

namespace WFM.Android
{
    public class TimeOffPeerRequestsView : ConsortiumFragment
    {
        private CollectionControl m_peers;

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public TimeOffPeerRequestsView () : base(Resource.Layout.view_timeoff_peerrequests)
        {
            Title = "Peer Requests";
        }

        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_peers = view.FindViewById<CollectionControl> (Resource.Id.timeoff_details_peerrequests);
            m_peers.RegisterView<PeerRequestCell> ();
        }
    }
}

