﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.OS;

namespace WFM.Android
{
    public class TimeOffEmployeeHistoryView : ConsortiumFragment
    {
        private CollectionControl m_history;

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public TimeOffEmployeeHistoryView () : base(Resource.Layout.view_timeoff_employeehistory)
        {
            Title = "Employee History";
        }

        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_history = view.FindViewById<CollectionControl> (Resource.Id.timeoff_details_history);
            m_history.RegisterView<EmployeeHistoryCell> ();
        }
    }
}

