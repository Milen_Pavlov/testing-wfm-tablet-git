﻿using System;
using Consortium.Client.Android;
using Android.Views;

namespace WFM.Android
{
    public class TimeOffCalendarDayView : ConsortiumPopoverView
    {
        public TimeOffCalendarDayView() : base(Resource.Layout.view_timeoff_dayview)
        {
            Width = 400;
            Height = 350;
        }

        protected override void OnViewCreated(View view)
        {

        }
    }
}

