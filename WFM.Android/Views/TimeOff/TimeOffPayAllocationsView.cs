﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.OS;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class TimeOffPayAllocationsView : ConsortiumFragment
    {
        private CollectionControl m_allocations;
        private TextView m_totalPaidLabel;
        private TextView m_totalUnpaidLabel;

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public TimeOffPayAllocationsView () : base(Resource.Layout.view_timeoff_payallocations)
        {
            Title = "Pay Allocations";
        }

        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_allocations = view.FindViewById<CollectionControl> (Resource.Id.timeoff_details_allocations);
            m_allocations.RegisterView<PayAllocationCell> ();
            m_allocations.RegisterSection<PayAllocationSectionCell> ();

            m_totalPaidLabel = view.FindViewById<TextView> (Resource.Id.timeoff_totalpaid);
            m_totalUnpaidLabel = view.FindViewById<TextView> (Resource.Id.timeoff_totalunpaid);

            // View Model Binding
            var set = this.CreateBindingSet<TimeOffPayAllocationsView, TimeOffPayAllocationsViewModel>();
            set.Bind(m_totalPaidLabel).For(b => b.Text).To (vm => vm.TotalPaidHours).WithConversion(new StringFormatValueConverter(),"0.00");
            set.Bind(m_totalUnpaidLabel).For(b => b.Text).To (vm => vm.TotalUnpaidHours).WithConversion(new StringFormatValueConverter(),"0.00");
            set.Apply();
        }
    }
}

