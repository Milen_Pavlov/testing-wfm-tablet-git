﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.OS;
using System.Collections.Generic;
using WFM.Core;
using Android.Widget;

namespace WFM.Android
{
    public class TillAllocationAllocationsView : ConsortiumFragment
    {
        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public TillAllocationAllocationsView() : base(Resource.Layout.view_till_allocation_allocations)
        {
            Title = "TillAllocationAllocations";
        }

    }
}

