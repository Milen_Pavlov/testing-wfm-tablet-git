﻿using System;
using Consortium.Client.Android;
using Android.Widget;
using WFM.Core;
using Android.Views;
using Android.OS;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Threading.Tasks;
using Android.Views.Animations;
using Android.App;
using Android.Graphics;
using Android.Content;

namespace WFM.Android
{

    public class CustomDatePickerDialog : DatePickerDialog
    {
        public CustomDatePickerDialog(Context context, EventHandler<DatePickerDialog.DateSetEventArgs> callBack, int year, int monthOfYear, int dayOfMonth) : 
            base (context, callBack, year, monthOfYear, dayOfMonth)
        {
        }

        protected override void OnStop ()
        {
            //Don't call base!
            // Replacing tryNotifyDateSet() with nothing - this is a workaround for Android bug https://android-review.googlesource.com/#/c/61270/A

            // Would also like to clear focus, but we cannot get at the private members, so we do nothing.  It seems to do no harm...
            // mDatePicker.clearFocus();

            // Now we would like to call super on onStop(), but actually what we would mean is super.super, because
            // it is super.onStop() that we are trying NOT to run, because it is buggy.  However, doing such a thing
            // in Java is not allowed, as it goes against the philosophy of encapsulation (the Creators never thought
            // that we might have to patch parent classes from the bottom up :)
            // However, we do not lose much by doing nothing at all, because in Android 2.* onStop() in androd.app.Dialog actually
            // does nothing and in 4.* it does:
            //      if (mActionBar != null) mActionBar.setShowHideAnimationEnabled(false); 
            // which is not essential for us here because we use no action bar... QED
            // So we do nothing and we intend to keep this workaround forever because of users with older devices, who might
            // run Android 4.1 - 4.3 for some time to come, even if the bug is fixed in later versions of Android.
        }
    }

    public class MyHoursView : ConsortiumFragment
    {
        private readonly IStringService m_localiser;

        private const int c_topBottomMarginDp = 4;
        private const int c_segmentWidth = 90;

        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;
        private BarButton m_refreshButton;
        private BarButton m_undoButton;
        private ImageView m_deptNameSortImageView;
        private CollectionControl m_headerCollectionControl;
        private AlternateBackgroundControl m_contentCollectionControl;
        private TextView m_selectedDepartmentName;
        private TextView m_dateLabel;
        private Button m_returnToWeekButton;

        private ImageButton m_prevButton, m_nextButton;

        public MyHoursViewModel VM { get { return (MyHoursViewModel)this.ViewModel; } }

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        public bool Loading
        {
            get { return false; }
            set 
            {
                m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Gone; 
                m_loadingProgress.Visibility = value? ViewStates.Visible : ViewStates.Invisible;
                m_refreshButton.Enabled = !value;
            }
        }

        //Different to HasUndo, HasUndo makes the button visible, this makes it enabled if something in the Undo stack
        public bool UndoEnabled
        {
            get
            {
                return false;
            }

            set
            {
                if(value)
                {
                    m_undoButton.Clickable = true;
                    m_undoButton.Alpha = 1f;
                }
                else
                {
                    m_undoButton.Clickable = false;
                    m_undoButton.Alpha = 0.5f;
                }
            }
        }

        public SortState DeptSort
        {
            get { return null; }
            set 
            { 
                m_deptNameSortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                m_deptNameSortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        private MyHoursViewModel.MyHoursMode m_currentMode;
        public MyHoursViewModel.MyHoursMode CurrentMode
        {
            get { return m_currentMode; }
            set
            { 
                m_currentMode = value;

                switch (CurrentMode)
                {
                    case MyHoursViewModel.MyHoursMode.Week:
                        RegisterWeekContentCells ();
                        break;
                    case MyHoursViewModel.MyHoursMode.Department:
                        RegisterDayContentCells ();
                        break;
                }

                // This needs to be looked at to match iOS.
                m_contentCollectionControl.InvalidateViews(); 
            }
        }

        public MyHoursRowData ScrollToRow
        {
            get { return null; }
            set { AnimateCell (value); }
        }

        private DateTime m_currentDate;
        public DateTime CurrentDate
        {
            get { return m_currentDate; }
            set { m_currentDate = value; }
        }

        public MyHoursView() : base(Resource.Layout.view_myhours)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            Title = m_localiser.Get ("my_hours");
        }

        void AnimateCell(MyHoursRowData data)
        {
            if (data != null && m_contentCollectionControl != null && m_contentCollectionControl.Items != null)
            {
                int index = m_contentCollectionControl.Items.IndexOf (data);
                if (index >= 0 && index < m_contentCollectionControl.Items.Count)
                {
                    //Don't animate/snap to position if we don't need to
                    int firstItemPos = m_contentCollectionControl.FirstVisiblePosition;
                    int lastItemPos = m_contentCollectionControl.LastVisiblePosition;

                    if (index < firstItemPos || index > lastItemPos)
                    {
                        bool snap = false; //Snapping isn't working yet
                        if (snap)
                        {
                            m_contentCollectionControl.ChoiceMode = ChoiceMode.Single;
                            m_contentCollectionControl.RequestFocusFromTouch ();
                            m_contentCollectionControl.SetSelection (index);
                        }
                        else
                        {
                            //Check if we will actually *need* to scroll
                            m_contentCollectionControl.SmoothScrollToPositionFromTop (index, m_contentCollectionControl.MeasuredHeight / 2, 150);
                        }
                    }
                }
            }
        }

        void RegisterDayContentCells()
        {
            if (m_headerCollectionControl.NumColumns != 3)
                m_headerCollectionControl.NumColumns = 3;
        }

        void RegisterWeekContentCells()
        {
            if (m_headerCollectionControl.NumColumns != 4)
                m_headerCollectionControl.NumColumns = 4;
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += OnRefresh;

            m_undoButton = new BarButton(ConsortiumApp.Context);
            m_undoButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_undoButton.Text = m_localiser.Get ("undo_button");
            m_undoButton.Click += OnUndo;

            var controlButtons = new List<BarButton>() { m_refreshButton };
            NavBar.RightButtons = controlButtons;

            List<BarButton> leftButtons = (NavBar.LeftButtons != null) ? new List<BarButton>(NavBar.LeftButtons) : new List<BarButton>();
            leftButtons.Add(m_undoButton);

            NavBar.LeftButtons = leftButtons;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            var settings = Mvx.Resolve<SettingsService> ();
            m_returnToWeekButton = (Button)view.FindViewById (Resource.Id.returnToWeekButton);
            m_returnToWeekButton.Background.SetColorFilter (ColorUtility.FromHexString (settings.AppTheme.PrimaryColor), PorterDuff.Mode.Multiply);

            m_deptNameSortImageView = (ImageView)view.FindViewById (Resource.Id.sort_imageView);

            m_selectedDepartmentName = (TextView)view.FindViewById (Resource.Id.selectedDepartmentName);
            m_selectedDepartmentName.Click += OnSelectDepartment;
            m_selectedDepartmentName.PaintFlags = m_selectedDepartmentName.PaintFlags | PaintFlags.UnderlineText;

            m_dateLabel = (TextView)view.FindViewById (Resource.Id.dateLabel);
            m_dateLabel.Click += OnSelectDate;
            m_dateLabel.PaintFlags = m_dateLabel.PaintFlags | PaintFlags.UnderlineText;

            m_loading = view.FindViewById (Resource.Id.tab_loading) as FrameLayout;
            m_loadingProgress = view.FindViewById (Resource.Id.progressBarLoading) as ProgressBar;

            m_headerCollectionControl = (CollectionControl)view.FindViewById (Resource.Id.header_collectionControl);
            m_headerCollectionControl.RegisterViewWithResourceID<WeekHeaderCell> (Resource.Layout.cell_myhours_header);
            m_contentCollectionControl = (AlternateBackgroundControl)view.FindViewById (Resource.Id.content_collectionControl);
            m_contentCollectionControl.RegisterViewWithResourceID<MyHoursRowContentCell> (Resource.Layout.cell_myhours_row);

            m_prevButton = (ImageButton)view.FindViewById (Resource.Id.prevButton);
            m_nextButton = (ImageButton)view.FindViewById (Resource.Id.nextButton);

            // View Model Binding
            var set = this.CreateBindingSet<MyHoursView, MyHoursViewModel> ();
            set.Bind (this).For (b => b.DeptSort).To (m => m.DeptSort);
            set.Bind (this).For (b => b.Loading).To (m => m.Loading);
            set.Bind (this).For (v => v.UndoEnabled).To (vm => vm.CanUndo);
            set.Bind (this).For (v => v.CurrentMode).To (vm => vm.CurrentMode);
            set.Bind (this).For (v => v.ScrollToRow).To (vm => vm.ScrollToRow);
            set.Bind (this).For (v => v.CurrentDate).To (vm => vm.WeekStart);
            set.Bind (m_headerCollectionControl).To (m => m.Headers);
            set.Bind (m_contentCollectionControl).To (m => m.Data);
            set.Bind (m_contentCollectionControl).For (v => v.CustomCommand1).To ("OnCellSelected");
            set.Bind (m_contentCollectionControl).For (v => v.CustomCommand2).To ("OnRowSelected");
            set.Bind (m_contentCollectionControl).For (v => v.CustomCommand3).To ("OnUndoConfirmed");
            set.Bind (m_headerCollectionControl).For (b => b.SelectedCommand).To ("OnHeaderSelect");
            set.Apply ();
        }

        public override void OnDestroy()
        {
            m_refreshButton.Click -= OnRefresh;
            m_undoButton.Click -= OnUndo;
            m_selectedDepartmentName.Click -= OnSelectDepartment;

            base.OnDestroy ();
        }

        void OnRefresh(object sender, EventArgs e) 
        {
            VM.OnRefresh();
        }

        void OnUndo(object sender, EventArgs e) 
        {
            VM.Undo();
        }

        void OnSelectDepartment(object sender, EventArgs e) 
        {
            ConsortiumPopoverView.SourceView = m_selectedDepartmentName;
            VM.OnDepartmentDropDownSelect();
        }

        void OnSelectDate(object sender, EventArgs e)
        {
            CustomDatePickerDialog dialog = new CustomDatePickerDialog (Context, OnDateSet, CurrentDate.Year, CurrentDate.Month - 1, CurrentDate.Day);
            dialog.Show ();
        }

        void OnDateSet (object sender, DatePickerDialog.DateSetEventArgs e)
        {
            VM.SetDate(e.Date);
        }
    }
}

