﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.Graphics;
using Android.Content;
using Android.App;

namespace WFM.Android
{
    public class MyHoursDepartmentSelectView : ConsortiumPopoverView
    {        
        public MyHoursDepartmentSelectView() : base(Resource.Layout.view_myhours_departmentselect)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.MyHours_DepartmentSelectPopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.MyHours_DepartmentSelectPopup_Height);


            Gravity = GravityFlags.Center;
            BorderColor = Color.Black;
            BorderWidth = 3;
        }

        protected override void OnViewCreated(View view)
        {

        }
    }
}

