﻿using System;
using Consortium.Client.Android;
using Android.App;
using Android.Graphics;
using Android.Views;

namespace WFM.Android
{
    public class EmployeePickerView : ConsortiumPopoverView
    {        
        public EmployeePickerView () : base(Resource.Layout.view_employee_picker)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Employee_Picker_Popup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Employee_Picker_Popup_Height);

            Gravity = GravityFlags.Center;

            BorderColor = Color.Black;
            BorderWidth = 3;
        }

        protected override void OnViewCreated(View view)
        {
            //view.BringToFront ();
        }
    }
}

