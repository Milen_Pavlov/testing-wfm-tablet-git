using Android.Views;
using Consortium.Client.Android;
using Android.App;

namespace WFM.Android
{
    public class ShiftView : ConsortiumPopoverView
    {
        public ShiftView()
            : base(Resource.Layout.view_shift)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_ShiftPopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_ShiftPopup_Height);
            Gravity = GravityFlags.Center;

            BorderWidth = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Popup_Border_Width);
        }

        protected override void OnViewCreated(View view)
        {
            //var set = this.CreateBindingSet<ShiftView, ShiftViewModel>();
            //set.Apply();
        }
    }
}
