﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.App;

namespace WFM.Android
{
    public class EmployeeWarningsView : ConsortiumPopoverView
    {
        public EmployeeWarningsView() : base(Resource.Layout.view_employee_warnings)
        {
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_WarningPopup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_WarningPopup_Height);
        }

        protected override void OnViewCreated(View view)
        {

        }
    }
}

