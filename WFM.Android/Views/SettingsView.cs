﻿using System;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.Views;
using WFM.Core;
using Android.Views;
using Cirrious.CrossCore;
using Android.Content.Res;
using Android.App;

namespace WFM.Android
{
    public class SettingsView : ConsortiumPopoverView
    {
        CollectionControl m_collection;

        public SettingsView() : base(Resource.Layout.view_settings)
        {
			Width = (int)Application.Context.Resources.GetDimension (Resource.Dimension.settings_View_Width);
			Height = (int)Application.Context.Resources.GetDimension (Resource.Dimension.settings_View_Height);
			CornerRadius = Application.Context.Resources.GetDimension (Resource.Dimension.settings_View_CornerRadius);
        }
            
        protected override void OnViewCreated(View view)
        {
            m_collection = view.FindViewById (Resource.Id.environment_list) as CollectionControl;
            m_collection.ChoiceMode = global::Android.Widget.ChoiceMode.Single;

            var set = this.CreateBindingSet<SettingsView, SettingsViewModel>();
            set.Bind(m_collection).To(m => m.Environments);
            set.Bind (m_collection).For(b => b.SelectedIndex).To (m => m.EnvIndex);
            set.Bind (m_collection).For(b => b.SelectedCommand). To ("OnSetEnvironment");
            set.Apply();
        }
    }
}
