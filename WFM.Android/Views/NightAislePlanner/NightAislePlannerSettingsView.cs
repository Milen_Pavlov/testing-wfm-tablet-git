﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using WFM.Core;

namespace WFM.Android
{
    public class NightAislePlannerSettingsView : ConsortiumFragment
    {
        private const int c_topBottomMarginDp = 4;

        private View m_content;
        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;
        private BarButton m_refreshButton;
        private Button m_saveButton;

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        public bool Loading
        {
            get { return false; }
            set 
            {
                m_content.Visibility = value ? ViewStates.Gone : ViewStates.Visible;
                m_loadingProgress.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
                m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public NightAislePlannerSettingsViewModel VM {
            get { return (NightAislePlannerSettingsViewModel)DataContext; }
        }

        public NightAislePlannerSettingsView () : base(Resource.Layout.view_nightaisleplanner_settings)
        {
            Title = "NightAislePlannerSettingsView";
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_loading = View.FindViewById(Resource.Id.tabSettingsLoading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarSettingsLoading) as ProgressBar;
            m_content = (View)View.FindViewById(Resource.Id.content);

            m_refreshButton = new BarButton(ConsortiumApp.Context);

            var set = this.CreateBindingSet<NightAislePlannerSettingsView, NightAislePlannerSettingsViewModel> ();
            set.Bind(this).For(b => b.Loading).To(vm => vm.Loading);
            set.Apply ();

            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += (sender, args) =>
            {
                if(VM != null)
                    VM.Refresh();
            };

            NightAislePlannerView.StaticNavBar.RightButtons = new List<BarButton>() { m_refreshButton };

            // This allows the popup to show, other it tries to attach to an old view that no longer exists.
            ConsortiumPopoverView.SourceView = m_content;
        }
    }
}

