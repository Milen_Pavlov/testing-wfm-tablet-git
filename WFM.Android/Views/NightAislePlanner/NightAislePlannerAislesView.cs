﻿using System;
using Android.App;
using Consortium.Client.Android;
using Android.Widget;
using WFM.Core;
using Android.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using Android.OS;
using WFM.Core.NightAisle;
using WFM.Droid.Controls.Inputs;

namespace WFM.Android.Views.NightAisle
{
    public class NightAislePlannerAislesView : ConsortiumFragment
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        public NightAislePlannerAislesViewModel VM {
            get { return (NightAislePlannerAislesViewModel)DataContext; }
        }

        private View m_content;

        private DraggableAisleCollectionControl m_aisleList;

        private View m_aisleContainer;

        private TextInput m_aisleName;

        private Button m_createButton;
        private Button m_deleteButton;

        private const int c_topBottomMarginDp = 4;
        private BarButton m_refreshButton;
        private BarButton m_editButton;

        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;


        public bool Loading
        {
            get { return false; }
            set 
            {
                m_content.Visibility = value ? ViewStates.Gone : ViewStates.Visible;
                m_loadingProgress.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
                m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public bool EditMode
        {
            get { return VM.EditMode; }
            set 
            {
                UpdateEditButton();
                UpdateAisle ();
                m_aisleList.DraggingEnabled = value;
            }
        }

        private Aisle m_selectedAisle;
        public Aisle SelectedAisle
        {
            get { return m_selectedAisle; }
            set 
            {
                m_selectedAisle = value;
                UpdateAisle();
            }
        }


        public NightAislePlannerAislesView () : base(Resource.Layout.view_nightaisleplanner_aisles)
        {
            Title = "NightAislePlannerAislesView";
        }

        public override void OnDestroy ()
        {
            base.OnDestroy ();

            NightAislePlannerView.StaticNavBar.RightButtons = null;
                
            if (m_refreshButton != null)
            {
                m_refreshButton.Dispose ();
                m_refreshButton = null;
            }

            if(m_editButton != null)
            {
                m_editButton.Dispose ();
                m_editButton = null;
            }
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            m_loading = View.FindViewById(Resource.Id.tabAislesLoading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarAislesLoading) as ProgressBar;
            m_content = (View)View.FindViewById(Resource.Id.content);
           
            m_aisleContainer = (View)view.FindViewById(Resource.Id.aisle);
            m_aisleList = (DraggableAisleCollectionControl)view.FindViewById(Resource.Id.aisle_list);
            m_aisleList.OnAisleOrderChanged += VM.OnAisleOrderChanged;

            m_createButton = (Button)View.FindViewById(Resource.Id.create_button);
            m_deleteButton = (Button)m_aisleContainer.FindViewById(Resource.Id.delete_button);

            m_aisleName = (TextInput)m_aisleContainer.FindViewById(Resource.Id.aisle_name);
            m_aisleName.SetIMEOptions = global::Android.Views.InputMethods.ImeAction.Done;
            m_aisleName.SetSingleLine = true;

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_editButton = new BarButton(ConsortiumApp.Context);

            var set = this.CreateBindingSet<NightAislePlannerAislesView, NightAislePlannerAislesViewModel>();
            set.Bind(m_aisleList).To(m => m.Aisles);
            set.Bind(m_aisleList).For(b => b.SelectedCommand).To("OnAisleSelected");
            set.Bind(m_aisleList).For(b => b.SelectedIndex).To (m => m.SelectedAisleIndex);
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(b => b.SelectedAisle).To(m => m.SelectedAisle);
            set.Bind(this).For(b => b.EditMode).To(m => m.EditMode);
            set.Apply();

            NightAislePlannerView.StaticNavBar.RightButtons = new List<BarButton>() { m_editButton, m_refreshButton };

            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += (sender, args) =>
                {
                    if(VM != null)
                        VM.OnRefresh();
                };

            m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_editButton.Click += (sender, args) =>
                {
                    if(VM != null)
                        VM.OnEdit();
                };
                    
            m_createButton.Click += (sender, args) =>
                {
                    VM.OnCreateAisle();
                    m_aisleList.InvalidateViews();
                    m_aisleList.Invalidate();
                };

            m_deleteButton.Click += (sender, args) =>
                {
                    VM.OnDeleteAisle(VM.SelectedAisle);
                    m_aisleList.InvalidateViews();
                };

            m_aisleName.ValueChanged += (sender, args) =>
                {
                    if (VM.SelectedAisle != null)
                    {
                        VM.SelectedAisle.AisleNameChanged(args.Value);
                    }
                };
            
            UpdateEditButton();
        }

        private void UpdateEditButton()
        {
            if (m_editButton != null)
            {
                if(VM.EditMode)
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_confirm);
                }
                else
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_edit);
                }
                m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            }
        }

        private void UpdateAisle()
        {
            m_aisleContainer.Visibility = (VM.SelectedAisle == null ? ViewStates.Invisible : ViewStates.Visible);
            if (VM.SelectedAisle != null)
            {
                var aisle = (TextInput)m_aisleContainer.FindViewById(Resource.Id.aisle_name);

                aisle.Value = VM.SelectedAisle.Name;
                aisle.Editable = VM.EditMode;
            }
        }
    }
}

