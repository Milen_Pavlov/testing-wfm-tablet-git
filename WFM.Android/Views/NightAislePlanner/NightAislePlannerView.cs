﻿using System;
using Consortium.Client.Android;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using WFM.Core;
using System.Collections.Generic;
using Android.Widget;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.Android
{
    public class NightAislePlannerView : ConsortiumTabFragment
    {
        public static ConsortiumActionBar StaticNavBar { get; set; }

        private readonly IStringService m_localiser;
        private const int c_topBottomMarginDp = 4;
        private const int c_segmentWidth = 90;
        private IAlertBox m_alertBox;
        private IStringService Localiser;

        public NightAislePlannerViewModel VM { get { return (NightAislePlannerViewModel)this.ViewModel; } }

        public override Type AttachTarget 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        private SegmentedControl m_tabSegmentedControl;
            
        public NightAislePlannerView() : base(Resource.Layout.view_nightaisleplanner, Resource.Id.tab_content_layout)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            // TODO: Hack to gain access to thhe NavBar in tab page views
            StaticNavBar = NavBar;

            m_tabSegmentedControl = new SegmentedControl (ConsortiumApp.Context) {
                MinWidth = (int)DimensionUtility.ConvertDpToPixel(c_segmentWidth),
                MaxWidth = (int)DimensionUtility.ConvertDpToPixel(c_segmentWidth),
                SelectedColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.white),
                SelectedTextColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.primary),
                UnSelectedColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.primary),
                UnSelectedTextColor = ConsortiumApp.Instance.Resources.GetColor (Resource.Color.white),
                LayoutParameters = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent, GravityFlags.CenterVertical),
            };
                
            m_alertBox = Mvx.Resolve<IAlertBox> ();
            Localiser = Mvx.Resolve<IStringService> ();

            m_tabSegmentedControl.OnSelectionChanged += HeaderSelectionChanged;
            NavBar.MiddleLayout.AddView (m_tabSegmentedControl);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            // View Model Binding
            var set = this.CreateBindingSet<NightAislePlannerView, NightAislePlannerViewModel>();
            set.Bind (m_tabSegmentedControl).For (v => v.Segments).To (vm => vm.TabNames);
            set.Apply();
        }

        public override void OnDestroy()
        {
            m_tabSegmentedControl.OnSelectionChanged -= HeaderSelectionChanged;

            base.OnDestroy ();
        }

        void HeaderSelectionChanged(int index)
        {
            if (!VM.EditMode)
                VM.OnTabSelect (index);
            else
            {
                m_alertBox.ShowYesNo
                (
                    Localiser.Get("discard_changes_message"),
                    Localiser.Get("allocations_confirm_discard_message"),
                    (option) =>
                    {
                        switch (option)
                        {
                            case 1: // Yes
                                {
                                    VM.OnTabSelect(index);
                                    return;
                                }
                            case -1:
                            case 0: //no
                                {
                                    m_tabSegmentedControl.OnSelectionChanged -= HeaderSelectionChanged;
                                    m_tabSegmentedControl.SelectedIndex = m_tabSegmentedControl.SelectedIndex == 0 ? 1 : 0;
                                    m_tabSegmentedControl.OnSelectionChanged += HeaderSelectionChanged;
                                    return;
                                }
                        }
                    }
                );
            }
        }
    }
}

