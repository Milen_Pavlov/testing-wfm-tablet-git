﻿using System;
using Android.App;
using Consortium.Client.Android;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Android.Views;
using Android.OS;
using Android.Widget;
using Android.Graphics;
using System.Collections.Generic;

namespace WFM.Android.Views.NightAisle
{
    public class NightAislePlannerPlannerView : ConsortiumFragment
    {
        private const int c_topBottomMarginDp = 4;

        private View m_content;
        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;
        private BarButton m_refreshButton;
        private BarButton m_editButton;
        private TextView m_dateLabel;
        private CollectionControl m_headerCollectionControl;
        private AlternateBackgroundControl m_aisleMappingsCollection;

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); } 
        }

        public NightAislePlannerPlannerViewModel VM {
            get { return (NightAislePlannerPlannerViewModel)DataContext; }
        }

        public bool Loading
        {
            get { return false; }
            set 
            {
                m_content.Visibility = value ? ViewStates.Gone : ViewStates.Visible;
                m_loadingProgress.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
                m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public bool EditMode
        {
            get { return VM.EditMode; }
            set 
            {
                UpdateEditButton();
            }
        }

        private DateTime m_currentDate;
        public DateTime CurrentDate
        {
            get { return m_currentDate; }
            set { m_currentDate = value; }
        }

        public NightAislePlannerPlannerView () : base(Resource.Layout.view_nightaisleplanner_planner)
        {
            Title = "NightAislePlannerPlannerView";
        }

        public override void OnDestroy ()
        {
            base.OnDestroy ();

            NightAislePlannerView.StaticNavBar.RightButtons = null;

            if (m_refreshButton != null)
            {
                m_refreshButton.Dispose ();
                m_refreshButton = null;
            }

            if(m_editButton != null)
            {
                m_editButton.Dispose ();
                m_editButton = null;
            }
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_loading = View.FindViewById(Resource.Id.tabAislesLoading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarAislesLoading) as ProgressBar;
            m_content = (View)View.FindViewById(Resource.Id.content);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_editButton = new BarButton(ConsortiumApp.Context);

            m_dateLabel = (TextView)view.FindViewById (Resource.Id.dateLabel);
            m_dateLabel.Click += OnSelectDate;
            m_dateLabel.PaintFlags = m_dateLabel.PaintFlags | PaintFlags.UnderlineText;

            //Colleciton controls setup
            //Header
            m_headerCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.header_collectionControl);
            m_headerCollectionControl.RegisterViewWithResourceID<WeekHeaderCell>(Resource.Layout.cell_week_header);
            //Aisle Mappings
            m_aisleMappingsCollection = (AlternateBackgroundControl)view.FindViewById(Resource.Id.aislemappings_list);
            m_aisleMappingsCollection.RegisterViewWithResourceID<AisleModelCell> (Resource.Layout.cell_aisle_mapping);

            var set = this.CreateBindingSet<NightAislePlannerPlannerView, NightAislePlannerPlannerViewModel> ();
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(b => b.EditMode).To(m => m.EditMode);
            set.Bind(this).For (v => v.CurrentDate).To(vm => vm.WeekStart);
            set.Bind(m_headerCollectionControl).To (m => m.Headers);
            set.Bind(m_aisleMappingsCollection).To (m => m.Data);
            set.Bind(m_aisleMappingsCollection).For (v => v.CustomCommand1).To ("OnAislePicked");
            set.Apply ();

            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += (sender, args) =>
                {
                    if(VM != null)
                        VM.Refresh();
                };

            m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_editButton.Click += (sender, args) =>
                {
                    if(VM != null)
                        VM.OnEdit();
                };

            NightAislePlannerView.StaticNavBar.RightButtons = new List<BarButton>() { m_editButton, m_refreshButton };
        }

        private void UpdateEditButton()
        {
            if (m_editButton != null)
            {
                if(VM.EditMode)
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_confirm);
                }
                else
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_edit);
                }
                m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            }
        }

        void OnSelectDate(object sender, EventArgs e)
        {
            CustomDatePickerDialog dialog = new CustomDatePickerDialog (Context, OnDateSet, CurrentDate.Year, CurrentDate.Month - 1, CurrentDate.Day);
            dialog.Show ();
        }

        void OnDateSet (object sender, DatePickerDialog.DateSetEventArgs e)
        {
            VM.SetDate(e.Date);
        }
    }
}

