﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.Graphics;
using Android.Content;
using Android.App;

namespace WFM.Android
{
    public class StartTimeView : ConsortiumPopoverView
    {        
        public StartTimeView() : base(Resource.Layout.view_start_time)
        {
            this.Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_StartTimePopup_Width);
            this.Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_StartTimePopup_Height);

            BorderColor = Color.Black;
            BorderWidth = 3;
        }

        protected override void OnViewCreated(View view)
        {

        }
    }
}

