﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Consortium.Client.Android;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Android.Views;
using Android.OS;
using Android.Widget;
using Android.Graphics;
using System.Threading.Tasks;

namespace WFM.Android.Views.TillAllocations
{
    public class VarianceTableRow
    {
        public LinearLayout Holder { get; set; }
        public List<TextView> CreatedViews { get; set; }
        public int RowID { get; set; }
    }

    public class AllocationsOverviewVarianceView : ConsortiumFragment, IAllocationOverviewPanel
    {
        private const int c_boxWidth = 100;
        public override Type AttachTarget
        {
            get { return typeof(AllocationsOverviewView); }
        }

        private List<VarianceOverviewModel> m_rows; 
        public List<VarianceOverviewModel> Rows
        {
            get { return m_rows; }
            set
            {
                m_rows = value;

                RegenerateRows ();

                var minutes = DateTime.Now.Minute + DateTime.Now.Hour * 60;
                minutes = minutes / 15;
                minutes += 5;
                minutes = Math.Min (minutes, 96);
                // we want to show the previous few hours as well so go back in time.
                minutes -= 6; // 6 sections of 15 minutes, so 1.5 hours
                minutes = Math.Max (minutes, 0);
               
                Task.Run (async() =>
                    {
                        // Delay as the scroll view can't scroll straight away.
                        await Task.Delay (100);
                        m_scrollView.ScrollTo (minutes * c_boxWidth, 0);
                    });
            }
        }
            
        private HorizontalScrollView m_scrollView;
        private List<VarianceTableRow> m_varianceRows;

        public AllocationsOverviewVarianceViewModel VM { get { return (AllocationsOverviewVarianceViewModel)this.ViewModel; } }

        public AllocationsOverviewVarianceView () : base(Resource.Layout.view_till_allocations_overview_variance)
        {
            
        }

        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_scrollView = view.FindViewById (Resource.Id.variance_scrollview) as HorizontalScrollView;

            m_varianceRows = new List<VarianceTableRow> ();

            var timeHolder = view.FindViewById (Resource.Id.overview_time_holder) as LinearLayout;
            var timeViews = new List<TextView> ();
            m_varianceRows.Add (new VarianceTableRow () { Holder = timeHolder, CreatedViews = timeViews, RowID = 0 });

            var demandHolder = view.FindViewById (Resource.Id.overview_demand_holder) as LinearLayout;
            var demandViews = new List<TextView> ();
            m_varianceRows.Add (new VarianceTableRow () { Holder = demandHolder, CreatedViews = demandViews, RowID = 1 });

            var scheduledHolder = view.FindViewById (Resource.Id.overview_scheduled_holder) as LinearLayout;
            var scheduledViews = new List<TextView> ();
            m_varianceRows.Add (new VarianceTableRow () { Holder = scheduledHolder, CreatedViews = scheduledViews, RowID = 2 });

            var varianceHolder = view.FindViewById (Resource.Id.overview_variance_holder) as LinearLayout;
            var varianceViews = new List<TextView> ();
            m_varianceRows.Add (new VarianceTableRow () { Holder = varianceHolder, CreatedViews = varianceViews, RowID = 3 });

            var set = this.CreateBindingSet<AllocationsOverviewVarianceView, AllocationsOverviewVarianceViewModel>();
            set.Bind (this).For (v => v.Rows).To (vm => vm.Rows); 
            set.Apply ();
        }

        private void RegenerateRows()
        {
            foreach (var row in m_varianceRows)
            {
                foreach (var textView in row.CreatedViews)
                {
                    row.Holder.RemoveView (textView);
                }
                row.CreatedViews.Clear ();
            }

            if (Rows == null)
                return;
               
            foreach (var row in m_varianceRows)
            {
                if (Rows[row.RowID].Data != null)
                {
                    foreach (var data in Rows[row.RowID].Data)
                    {
                        row.CreatedViews.Add (AddTextBox (data, row.Holder));    
                    }
                }
            }
        }

        private TextView AddTextBox(string text, LinearLayout layout)
        {
            var label = new TextView(Context);
            label.SetText(text, TextView.BufferType.Normal);
            label.TextSize = 14;
            label.SetTextColor (Color.Black);
            label.SetBackgroundResource (Resource.Drawable.variance_cell_background);
            label.Gravity = GravityFlags.Center;
            var layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.MatchParent);
            label.LayoutParameters = layoutParams;
            label.LayoutParameters.Width = c_boxWidth;
            layout.AddView (label);

            return label;
        }

        #region IAllocationOverviewPanel implementation

        public void OnFullRefresh ()
        {
            if (VM != null)
                VM.Refresh ();
        }

        public void OnScheduledRefresh ()
        {
            if (VM != null)
                VM.Refresh ();
        }

        #endregion
    }
}

