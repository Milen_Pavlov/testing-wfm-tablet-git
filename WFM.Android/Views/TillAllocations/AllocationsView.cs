using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore.Converters;
using Cirrious.CrossCore.UI;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Plugins.Color;
using Consortium.Client.Android;
using Consortium.Client.Android.Controls;
using Consortium.Client.Core;
using WFM.Core;
using WFM.Android.Constants;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Android.Graphics;
using Android.App;

namespace WFM.Android.Views.TillAllocations
{
	public class AllocationsView : ConsortiumFragment
    {
        private View m_content;

        protected ScalableHoursRulerView m_scalableHoursRulerView;
        protected PinchableCollectionControl m_tillsCollectionControl;

        private const int c_topBottomMarginDp = 4;
        private BarButton m_runAllocationButton;
        private BarButton m_refreshButton;

        private LinearLayout m_tillDateSelectionTitle;
        private SegmentedControl m_segmentedControl;

        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;
		private TextView m_noDataText;

		private BarButton m_editButton;
		private BarButton m_undoButton;

		private int[] m_editButtonImages;

        private List<AllocationsViewModel.TillModel> m_tills;
        public List<AllocationsViewModel.TillModel> Tills
        {
            get{ return m_tills; }
            set
            {
                m_tills = value;
                m_tillsCollectionControl.Items = value;
            }
        }

		private bool m_editMode;
		public bool EditMode
		{
			get { return m_editMode; }
			set 
			{ 
				m_editMode = value; 
				m_editButton.SetImageResource(m_editButtonImages[value ? 1: 0]);

				if (value)
					m_undoButton.Visibility = ViewStates.Visible;
				else
					m_undoButton.Visibility = ViewStates.Invisible;
			}
		}

		public bool UndoEnabled
		{
			get
			{
				return false;
			}

			set
			{
				if(value)
				{
					m_undoButton.Clickable = true;
					m_undoButton.Alpha = 1f;
				}
				else
				{
					m_undoButton.Clickable = false;
					m_undoButton.Alpha = 0.5f;
				}
			}
		}

        public AllocationsViewModel VM { get { return (AllocationsViewModel)this.ViewModel; } }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); }
        }

        public Type TargetType 
        { 
            get { return typeof(ConsortiumTabFragment); } 
        }

        public bool LoadingSchedule
        {
            get { return VM.LoadingSchedule; }
            set 
            {
                UpdateLoadingState();
            }
        }

        public bool LoadingService
        {
            get { return VM.LoadingService; }
            set 
            {
                UpdateLoadingState();
            }
        }

		public bool LoadingSubmitChanges
		{
			get { return VM.LoadingSubmitChanges; }
			set 
			{
				UpdateLoadingState();
			}
		}

        public void UpdateLoadingState()
        {
			bool value = VM.LoadingService || VM.LoadingSchedule || VM.LoadingSubmitChanges;

            m_content.Visibility = value ? ViewStates.Gone : ViewStates.Visible;
            m_loadingProgress.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            m_tillDateSelectionTitle.Visibility =  !value ? ViewStates.Visible : ViewStates.Invisible;
        }

        public bool NoData
        {
            get { return false; }
            set 
            {
				m_noDataText.Visibility = (value) ? ViewStates.Visible : ViewStates.Gone;
            }
        }

        public AllocationsView() : base(Resource.Layout.view_till_allocations_allocations)
        {
            Title = "Till Allocations";
			m_editButtonImages = new int[] { Resource.Drawable.icon_edit, Resource.Drawable.icon_confirm };
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

			m_loading = View.FindViewById(Resource.Id.tab_loading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarLoading) as ProgressBar;
            m_content = (View)View.FindViewById(Resource.Id.content);

            m_scalableHoursRulerView = (ScalableHoursRulerView)View.FindViewById(Resource.Id.scalable_hours_ruler_view);

			m_noDataText = View.FindViewById<TextView> (Resource.Id.no_data_text);

            m_tillsCollectionControl = (PinchableCollectionControl)View.FindViewById(Resource.Id.till_collection);
            m_tillsCollectionControl.RegisterViewWithResourceID<TillAllocationCell>(Resource.Layout.cell_till_allocation);
			m_tillsCollectionControl.OnTouchUp += () => m_tillsCollectionControl.InvalidateViews();
            m_tillsCollectionControl.OnScaleChanged += (float scale) =>
            {
                m_scalableHoursRulerView.ScaleFactor = scale;
                VM.Hours = m_scalableHoursRulerView.Hours;
            };
            m_tillsCollectionControl.OnXScroll += OnXScroll;

            m_segmentedControl = View.FindViewById(Resource.Id.day_segmented_control) as SegmentedControl;
            m_segmentedControl.OnSelectionChanged += DateSelectionChanged;

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += delegate
            {
                VM.OnRefresh();
            };

            m_runAllocationButton = new BarButton(ConsortiumApp.Context);
            m_runAllocationButton.SetImageResource(Resource.Drawable.icon_run_allocation);
            m_runAllocationButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_runAllocationButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_runAllocationButton.Click += delegate
            {
                ConsortiumPopoverView.SourceView = m_runAllocationButton;
                VM.OnRunAllocation();
            };

			m_editButton = new BarButton(ConsortiumApp.Context);
			m_editButton.SetImageResource(Resource.Drawable.icon_edit);
			m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
			m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
			m_editButton.Click += OnEdit;

			m_undoButton = new BarButton(ConsortiumApp.Context);
			m_undoButton.SetTopBottomMargin(c_topBottomMarginDp);
			m_undoButton.Text = "Undo";
			m_undoButton.Click += OnUndo;

            m_tillDateSelectionTitle = View.FindViewById(Resource.Id.tillDateSelectionTitle) as LinearLayout;

            AddRightButtons ();
            AddLeftButtons ();

            var set = this.CreateBindingSet<AllocationsView, AllocationsViewModel>();
			set.Bind(this).For(b => b.EditMode).To(m => m.EditMode);
			set.Bind(this).For(v => v.UndoEnabled).To(vm => vm.CanUndo);
            set.Bind(this).For(b => b.LoadingService).To(m => m.LoadingService);
            set.Bind(this).For(b => b.LoadingSchedule).To(m => m.LoadingSchedule);
			set.Bind(this).For(b => b.LoadingSubmitChanges).To(m => m.LoadingSubmitChanges);
            set.Bind(m_segmentedControl).For(b => b.Segments).To(m => m.Segments);
            set.Bind(m_segmentedControl).For(b => b.SelectedIndex).To(m => m.SelectedDayIndex);
            set.Bind(this).For(b => b.Tills).To(m => m.Tills);
            set.Bind(this).For(b => b.NoData).To(m => m.NoData);
            set.Apply();
        }

		public override void OnDestroy ()
		{
			m_editButton.Click -= OnEdit;
			m_undoButton.Click -= OnUndo;

			//Remove undo button so it doesn't keep getting readded
			if (RootView.StaticNavBar != null && RootView.StaticNavBar.LeftButtons != null) 
			{
				RootView.StaticNavBar.LeftButtons.Remove (m_undoButton);
			}

			base.OnDestroy ();
		}

        private void DateSelectionChanged(int index)
        {
            VM.OnSelectDay(index);
            Tills = Tills;
        }

		void OnUndo(object sender, EventArgs e) 
		{
			VM.OnUndo();
		}

		void OnEdit(object sender, EventArgs e) 
		{
			VM.OnEdit();
		}

        public virtual void AddRightButtons()
        {
            List<BarButton> controlButtons = new List<BarButton>() {m_editButton, m_runAllocationButton, m_refreshButton };
            RootView.StaticNavBar.RightButtons = controlButtons;
        }

        public virtual void AddLeftButtons()
        {
            List<BarButton> leftButtons = new List<BarButton>(RootView.StaticNavBar.LeftButtons ?? new List<BarButton> ());
            leftButtons.Add(m_undoButton);
            RootView.StaticNavBar.LeftButtons = leftButtons;
        }

        public virtual void OnXScroll(float x)
        {
            m_scalableHoursRulerView.DragX(-x);
            VM.Hours = m_scalableHoursRulerView.Hours;
        }

        public override void OnPause ()
        {
            base.OnPause ();

            if (VM != null)
                VM.DetachEvents ();
        }

        public override void OnResume ()
        {
            base.OnResume ();

            if (VM != null)
                VM.AttachEvents ();
        }
    }
}