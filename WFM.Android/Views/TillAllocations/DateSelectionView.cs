﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WFM.Core;
using Consortium.Client.Android;

namespace WFM.Android
{
    public class DateSelectionView : ConsortiumFragment
    {
        private SegmentedControl m_segmentedControl;

        public DateSelectionViewModel VM { get { return (DateSelectionViewModel)this.ViewModel; } }

        public DateSelectionView(int layoutId) : base(layoutId)
        {

        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_segmentedControl = View.FindViewById (Resource.Id.day_segmented_control) as SegmentedControl;
            m_segmentedControl.Segments = new List<string>() { "Sun", "Mon", "Tues", "Weds", "Thurs", "Fri", "Sat" };
            m_segmentedControl.OnSelectionChanged += DateSelectionChanged;
        }

        void DateSelectionChanged(int index)
        {
            VM.OnWeekSelect(index);
        }
    }
}

