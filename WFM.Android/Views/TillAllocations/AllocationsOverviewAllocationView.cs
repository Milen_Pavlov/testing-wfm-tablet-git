using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Consortium.Client.Android;
using Android.Views;
using Android.OS;
using Android.Content;
using System.Threading.Tasks;
using Android.Widget;

namespace WFM.Android.Views.TillAllocations
{
    public class AllocationsOverviewAllocationView : AllocationsView, IAllocationOverviewPanel
    {
        public override Type AttachTarget
        {
            get { return typeof(AllocationsOverviewView); }
        }

        public AllocationsOverviewAllocationView ()  : base()
        {

        }
            
        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            float newScale = (4.0f / 24.0f);

            if (m_tillsCollectionControl.OnScaleChanged != null)
                m_tillsCollectionControl.OnScaleChanged (newScale);

            m_scalableHoursRulerView.CentreHours = (VM.Hours.Item1 + VM.Hours.Item2) / 2.0f;

            // Disable pinch zooming
            m_tillsCollectionControl.OnScaleChanged = null;
        }

        public override void AddRightButtons()
        {
            // This stops the buttons being added to the right toolbar.
        }

        public override void AddLeftButtons()
        {
            // This stops the buttons being added to the right toolbar.
        }

        public override void OnXScroll(float x)
        {
            // This blocks the horizontal scrolling.
        }

        public override void OnPause ()
        {
            base.OnPause ();

            if (VM != null)
                VM.DetachEvents ();
        }

        public override void OnResume ()
        {
            base.OnResume ();

            if (VM != null)
                VM.AttachEvents ();
        }

        #region IAllocationOverviewPanel implementation

        public void OnFullRefresh ()
        {
            if(VM != null)
                VM.OnRefresh();
        }

        public void OnScheduledRefresh ()
        {
            if(VM != null)
                VM.OnRefresh();
        }

        #endregion
    }
}

