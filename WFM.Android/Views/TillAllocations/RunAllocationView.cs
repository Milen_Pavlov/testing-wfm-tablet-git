﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Android.Content;
using Android.Util;
using Android.Graphics;
using Android.OS;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using Android.Widget;
using WFM.Droid.Controls.Inputs;
using System.Linq;
using Consortium.Client.Core.ViewModels;
using WFM.Android.Helpers;
using Android.App;

namespace WFM.Android
{
    public class RunAllocationView : ConsortiumPopoverView
    {
        private SpinnerInput m_startDaySpinner;
        private SpinnerInput m_endDaySpinner;

        public RunAllocationView() : base(Resource.Layout.view_run_allocation_popover)
        {
			
			Width = (int)Application.Context.Resources.GetDimension (Resource.Dimension.RunAllocations_Window_Width);
			Height = (int)Application.Context.Resources.GetDimension (Resource.Dimension.RunAllocations_Window_Height);
            Gravity = GravityFlags.Center;
            BorderWidth = 3;
            BorderColor = Color.Black;
        }

        protected override void OnViewCreated(View view)
        {

            m_startDaySpinner = (SpinnerInput)view.FindViewById(Resource.Id.spinner_recalc_day_start);
            m_endDaySpinner = (SpinnerInput)view.FindViewById(Resource.Id.spinner_recalc_day_end);
        }
    }
}




