using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Consortium.Client.Android;
using WFM.Core;
using Android.Views;
using Android.OS;
using WFM.Android.Views.TillAllocations;

namespace WFM.Android.Views.TillAllocations
{
    public class AllocationsOverviewBreaksView : ConsortiumFragment, IAllocationOverviewPanel
    {
        private AlternateBackgroundControl m_attendanceCollection;

        public override Type AttachTarget
        {
            get { return typeof(AllocationsOverviewView); }
        }

        public AllocationsOverviewBreaksViewModel VM { get { return (AllocationsOverviewBreaksViewModel)this.ViewModel; } }

        public AllocationsOverviewBreaksView ()  : base(Resource.Layout.view_till_allocations_overview_breaks)
        {

        }

        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);
            m_attendanceCollection = View.FindViewById<AlternateBackgroundControl> (Resource.Id.breaksCollection);
            m_attendanceCollection.RegisterViewWithResourceID<TillBreaksContentCell> (Resource.Layout.cell_allocations_breaks);
        }

        #region IAllocationOverviewPanel implementation

        public void OnFullRefresh ()
        {
            if (VM != null)
                VM.UpdateView (true);
        }

        public void OnScheduledRefresh ()
        {
            if (VM != null)
                VM.UpdateView (false);
        }

        #endregion
    }
}

