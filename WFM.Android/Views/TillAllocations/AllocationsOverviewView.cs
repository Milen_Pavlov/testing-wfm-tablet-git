﻿using System;
using Consortium.Client.Android;
using Consortium.Client.Core;
using System.Collections.Generic;
using Android.Views;
using Android.OS;
using Android.Content;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.Android.Views.TillAllocations
{
    public class AllocationsOverviewView : ConsortiumFragment, IConsortiumViewTarget
    {
        private const int c_topBottomMarginDp = 4;
        private const int PANEL_REFRESH_TIME = 30000;
        private const int SCHEDULE_REFRESH_TIME = 300000;

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); }
        }

        private AllocationsOverviewAllocationView m_allocationView;
        private AllocationsOverviewAttendanceView m_attendanceView;
        private AllocationsOverviewBreaksView m_breaksView;
        private AllocationsOverviewVarianceView m_varianceView;

        private BarButton m_refreshButton;

        public Action OnFullRefresh;
        public Action RefreshActions;
        protected Action OnScheduledPanelRefresh;
        protected Action OnScheduledScheduleRefresh;

//        private Handler m_Handler;
        private Timer m_refreshPanelTimer;
        private Timer m_refreshScheduleTimer;

        public AllocationsOverviewViewModel VM
        {
            get { return (AllocationsOverviewViewModel) this.ViewModel; }
        }

        public AllocationsOverviewView ()  : base(Resource.Layout.view_till_allocations_overview)
        {
            
        }

        public override void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += delegate
                {
                    //Trigger full refresh TODO should we refresh schedule in the root view too????
                    if(OnFullRefresh!=null) 
                    {
                        SetupScheduleRefreshing();//Restart the schedule refresh timer
                        ((AllocationsOverviewViewModel) ViewModel).Refresh(true);
                    }
                };

            List<BarButton> controlButtons = new List<BarButton>() { m_refreshButton };
            RootView.StaticNavBar.RightButtons = controlButtons;

            VM.SetupViewModels ();
        }

        private void SetupPanelRefreshing() 
        {
            if (m_refreshPanelTimer != null)
            {
                m_refreshPanelTimer.Dispose ();
                m_refreshPanelTimer = null;
            }
            
            m_refreshPanelTimer = new Timer ((o) =>
                {
                    #if DEBUG            
                    Console.WriteLine("refreshing panels: " + System.Environment.TickCount); 
                    #endif

                    if(RefreshActions != null)
                        RefreshActions ();
                    
                }, null, PANEL_REFRESH_TIME, PANEL_REFRESH_TIME);

        }

        private void SetupScheduleRefreshing()
        {
            if (m_refreshScheduleTimer != null)
            {
                m_refreshScheduleTimer.Dispose ();
                m_refreshScheduleTimer = null;
            }

            m_refreshScheduleTimer = new Timer ((o) =>
                {
                    #if DEBUG            
                    Console.WriteLine("refreshing schedule: " + System.Environment.TickCount); 
                    #endif

                    if(VM!=null)
                        VM.Refresh (false);

                }, null, SCHEDULE_REFRESH_TIME, SCHEDULE_REFRESH_TIME);
        }

        public override void OnPause ()
        {
            base.OnPause ();

            if (VM != null)
                VM.DetachEvents ();

            if (m_refreshPanelTimer != null)
            {
                m_refreshPanelTimer.Dispose ();
                m_refreshPanelTimer = null;
            }

            if (m_refreshScheduleTimer != null)
            {
                m_refreshScheduleTimer.Dispose ();
                m_refreshScheduleTimer = null;
            }
        }

        public override void OnResume ()
        {
            base.OnResume ();

            if (VM != null)
                VM.AttachEvents ();

            SetupPanelRefreshing ();
            SetupScheduleRefreshing ();
        }

        public bool Attach(IConsortiumViewSource source, ConsortiumViewRequest request)
        {
            if (source == null || !(source is ConsortiumFragment))
                return false;

            if (source is AllocationsOverviewAllocationView)
            {
                m_allocationView = (AllocationsOverviewAllocationView)source;
                if (View.FindViewById (Resource.Id.till_overview_allocation) != null && m_allocationView != null)
                {
                    ChildFragmentManager.BeginTransaction ().Replace (Resource.Id.till_overview_allocation, m_allocationView).Commit ();
                }
            }
            else if (source is AllocationsOverviewAttendanceView)
            {
                m_attendanceView = (AllocationsOverviewAttendanceView)source;
                if (View.FindViewById (Resource.Id.till_overview_attendance) != null && m_attendanceView != null)
                {
                    ChildFragmentManager.BeginTransaction ().Replace (Resource.Id.till_overview_attendance, m_attendanceView).Commit ();
                }
            }
            else if (source is AllocationsOverviewBreaksView)
            {
                m_breaksView = (AllocationsOverviewBreaksView)source;
                if (View.FindViewById (Resource.Id.till_overview_breaks) != null && m_breaksView != null)
                {
                    ChildFragmentManager.BeginTransaction ().Replace (Resource.Id.till_overview_breaks, m_breaksView).Commit ();
                }
            }
            else if (source is AllocationsOverviewVarianceView)
            {
                m_varianceView = (AllocationsOverviewVarianceView)source;
                if (View.FindViewById (Resource.Id.till_overview_variance) != null && m_varianceView != null)
                {
                    ChildFragmentManager.BeginTransaction ().Replace (Resource.Id.till_overview_variance, m_varianceView).Commit ();
                }
            }

            if (source is IAllocationOverviewPanel)
            {
                OnFullRefresh += ((IAllocationOverviewPanel)source).OnFullRefresh;
                RefreshActions += ((IAllocationOverviewPanel)source).OnScheduledRefresh;
            }

            return true;
        }

        public bool Detach(IConsortiumViewSource source)
        {
            if (source == null)
            {
                return false;
            }

            if(m_allocationView == source)
            {
                FragmentManager.BeginTransaction().Remove(m_allocationView).Commit();
                OnFullRefresh -= ((IAllocationOverviewPanel)m_allocationView).OnFullRefresh;
                RefreshActions -= ((IAllocationOverviewPanel)m_allocationView).OnScheduledRefresh;
                return true;
            }

            if(m_attendanceView == source)
            {
                FragmentManager.BeginTransaction().Remove(m_attendanceView).Commit();
                OnFullRefresh -= ((IAllocationOverviewPanel)m_attendanceView).OnFullRefresh;
                RefreshActions -= ((IAllocationOverviewPanel)m_attendanceView).OnScheduledRefresh;
                return true;
            }

            if(m_breaksView == source)
            {
                FragmentManager.BeginTransaction().Remove(m_breaksView).Commit();
                OnFullRefresh -= ((IAllocationOverviewPanel)m_breaksView).OnFullRefresh;
                RefreshActions -= ((IAllocationOverviewPanel)m_breaksView).OnScheduledRefresh;
                return true;
            }

            if(m_varianceView == source)
            {
                FragmentManager.BeginTransaction ().Remove (m_varianceView).Commit ();
                OnFullRefresh -= ((IAllocationOverviewPanel)m_varianceView).OnFullRefresh;
                RefreshActions -= ((IAllocationOverviewPanel)m_varianceView).OnScheduledRefresh;
                return true;
            }

            return false;
        }
    }
}

