﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Consortium.Client.Android;
using Android.Views;
using Android.OS;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Android.Widget;

namespace WFM.Android.Views.TillAllocations
{
    public class AllocationsOverviewAttendanceView : ConsortiumFragment, IAllocationOverviewPanel
    {
        private AlternateBackgroundControl m_attendanceCollection;

        public override Type AttachTarget
        {
            get { return typeof(AllocationsOverviewView); }
        }

        private int m_index = 0;
        public int ScrollToIndex
        {
            get { return m_index; }
            set
            {
                m_index = value; 
                ScrollCollectionToIndex (m_index);
            }
        }

        public AllocationsOverviewAttendanceViewModel VM { get { return  (AllocationsOverviewAttendanceViewModel) this?.ViewModel; } } //not sure why "this" is sometimes null but this fixes it

        public AllocationsOverviewAttendanceView ()  : base(Resource.Layout.view_till_allocations_overview_attendance)
        {

        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_attendanceCollection = (AlternateBackgroundControl)View.FindViewById(Resource.Id.attendanceCollection);
            m_attendanceCollection.RegisterViewWithResourceID<AttendanceEntryCell>(Resource.Layout.cell_attendance_entry);

            var set = this.CreateBindingSet<AllocationsOverviewAttendanceView, AllocationsOverviewAttendanceViewModel>();
            set.Bind (this).For (v => v.ScrollToIndex).To (vm => vm.FirstInIndex);
            set.Apply ();
        }

        void ScrollCollectionToIndex(int value)
        {
            if (m_attendanceCollection != null && m_attendanceCollection.Items != null && m_index >= 0 && m_index < m_attendanceCollection.Items.Count)
            {
//                m_attendanceCollection.ClearFocus();
//                m_attendanceCollection.Post(() => 
//                    {
//                        m_attendanceCollection.RequestFocusFromTouch();
//                        m_attendanceCollection.SetSelection(value);
//                        m_attendanceCollection.RequestFocus();
//                    });


                int index = value;
                if (index >= 0 && index < m_attendanceCollection.Items.Count)
                {
                    //Don't animate/snap to position if we don't need to
                    int firstItemPos = m_attendanceCollection.FirstVisiblePosition;
                    int lastItemPos = m_attendanceCollection.LastVisiblePosition;

                    if (index < firstItemPos || index > lastItemPos)
                    {
                        bool snap = false; //Snapping isn't working yet
                        if (snap)
                        {
                            m_attendanceCollection.ChoiceMode = ChoiceMode.Single;
                            m_attendanceCollection.RequestFocusFromTouch ();
                            m_attendanceCollection.SetSelection (index);
                        }
                        else
                        {
                            //Check if we will actually *need* to scroll
                            m_attendanceCollection.SmoothScrollToPositionFromTop (index, m_attendanceCollection.MeasuredHeight / 2, 150);
                        }
                    }
                }
            }
        }

        public override void OnPause ()
        {
            base.OnPause ();

            if (VM != null)
                VM.DetachEvents ();
        }

        public override void OnResume ()
        {
            base.OnResume ();

            if (VM != null)
                VM.AttachEvents ();
        }

        #region IAllocationOverviewPanel implementation

        public void OnFullRefresh ()
        {
            if (VM != null)
                VM.Refresh ();
        }

        public void OnScheduledRefresh ()
        {
            if (VM != null)
                VM.Refresh ();
        }

        #endregion
    }
}

