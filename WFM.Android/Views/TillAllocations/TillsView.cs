using System;
using System.Collections.Generic;
using System.Linq;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using Consortium.Client.Android.Controls;
using Consortium.Client.Core;
using Consortium.Client.Core.Extensions;
using Consortium.Client.Core.ViewModels;
using WFM.Core;
using WFM.Core.Model;
using WFM.Droid.Controls;
using WFM.Droid.Controls.Inputs;

namespace WFM.Android.Views.TillAllocations
{
    public partial class TillsView : ConsortiumFragment
    {
        private View m_content;

        private DraggableTillCollectionControl m_tillList;
        private View m_tillContainer;

        private TextInput m_tillNumber;
        private SpinnerInput m_tillType;
        private SpinnerInput m_tillStatus;

        private Button m_createButton;
        private Button m_deleteButton;

        private const int c_topBottomMarginDp = 4;
        private BarButton m_refreshButton;
        private BarButton m_editButton;

        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;

        public bool Loading
        {
            get { return false; }
            set 
            {
                m_content.Visibility = value ? ViewStates.Gone : ViewStates.Visible;
                m_loadingProgress.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
                m_loading.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public bool NoData
        {
            get { return false; }
            set 
            {
                if(value)
                {
                    m_loadingProgress.Visibility = ViewStates.Invisible;
                }
            }
        }


        public new TillsViewModel ViewModel
        {
            get
            {
                return (TillsViewModel)DataContext;
            }
        }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); }
        }

        public Type TargetType
        { 
            get
            {
                return typeof(ConsortiumTabFragment);
            } 
        }

        public IList<TillType> TillTypes
        {
            get { return ViewModel.TillTypes; }
            set 
            {
                if (value != null)
                {
                    m_tillType.Items = value.Select(t => new SpinnerItem(t.TillTypeId, t.Name)).ToList();
                }
            }
        }

        public IList<TillStatus> TillStatuses
        {
            get { return ViewModel.TillStatuses; }
            set 
            {
                if (value != null)
                {
                    m_tillStatus.Items = value.Select(s => new SpinnerItem(s.TillStatusId, s.Name)).ToList();
                }
            }
        }

        private TillModel m_selectedTill;
        public TillModel SelectedTill
        {
            get { return m_selectedTill; }
            set 
            {
                m_selectedTill = value;
                UpdateTill();
            }
        }
            
        public bool EditMode
        {
            get { return ViewModel.EditMode; }
            set 
            {
                UpdateEditButton();
                UpdateTill();
                m_tillList.DraggingEnabled = value;
            }
        }

        public TillsView()
            : base(Resource.Layout.view_till_allocations_tills)
        {
            Title = "Tills";
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            m_loading = View.FindViewById(Resource.Id.tabTillsLoading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarTillsLoading) as ProgressBar;
            m_content = (View)View.FindViewById(Resource.Id.content);

        	m_tillContainer = (View)view.FindViewById(Resource.Id.till);
            m_tillList = (DraggableTillCollectionControl)view.FindViewById(Resource.Id.till_list);
            m_tillList.VM = ViewModel;

            m_tillNumber = (TextInput)m_tillContainer.FindViewById(Resource.Id.till_number);
            m_tillType = (SpinnerInput)m_tillContainer.FindViewById(Resource.Id.till_type);
            m_tillStatus = (SpinnerInput)m_tillContainer.FindViewById(Resource.Id.till_status);

            m_createButton = (Button)View.FindViewById(Resource.Id.create_button);
            m_deleteButton = (Button)m_tillContainer.FindViewById(Resource.Id.delete_button);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_editButton = new BarButton(ConsortiumApp.Context);

            var set = this.CreateBindingSet<TillsView, TillsViewModel>();
            set.Bind(m_tillList).To(m => m.UndeletedTills);
            set.Bind(m_tillList).For(b => b.SelectedCommand).To("OnTillSelected");
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(b => b.NoData).To(m => m.NoData);
            set.Bind(this).For(b => b.TillTypes).To(m => m.TillTypes);
            set.Bind(this).For(b => b.TillStatuses).To(m => m.TillStatuses);
            set.Bind(this).For(b => b.SelectedTill).To(m => m.SelectedTill);
            set.Bind(this).For(b => b.EditMode).To(m => m.EditMode);
            set.Apply();

            RootView.StaticNavBar.RightButtons = new List<BarButton>() { m_editButton, m_refreshButton };

            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += (sender, args) =>
            {
                if(ViewModel != null)
                    ViewModel.OnRefresh();
            };

            m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_editButton.Click += (sender, args) =>
            {
               if(ViewModel != null)
                    ViewModel.OnEdit();
            };

            m_createButton.Click += (sender, args) =>
            {
                ViewModel.OnCreateTill();
                m_tillList.InvalidateViews();
                m_tillList.Invalidate();
            };

            m_deleteButton.Click += (sender, args) =>
            {
                ViewModel.OnDeleteTill(ViewModel.SelectedTill);
                m_tillList.InvalidateViews();
            };

            m_tillNumber.ValueChanged += (sender, args) =>
            {
                if (ViewModel.SelectedTill != null)
                {
                    ViewModel.SelectedTill.TillNumberChanged(args.Value);
                    UpdateValidators();
                }
            };

            m_tillNumber.SetSingleLine = true;
            m_tillNumber.SetIMEOptions = global::Android.Views.InputMethods.ImeAction.Done;

            m_tillType.ValueChanged += (sender, args) =>
            {
                if (ViewModel.SelectedTill != null)
                {
                    ViewModel.SelectedTill.TypeIdChanged(int.Parse(args.Value));
                    UpdateTillWindows();
                    UpdateValidators();
                }
            };

            m_tillStatus.ValueChanged += (sender, args) =>
            {
                if (ViewModel.SelectedTill != null)
                {
                    ViewModel.SelectedTill.StatusIdChanged(int.Parse(args.Value));
                    UpdateValidators();
                }
            };

            InitialiseTillWindows();
            UpdateEditButton();
        }

        public void InitialiseTillWindows()
        {
            var windowsContainer = (ViewGroup)m_tillContainer.FindViewById(Resource.Id.windows_container);
            for (var w = 0; w < 3; w++)
            {
                var index = w;
                var windowContainer = windowsContainer.GetChildAt(w + 1);
                var startTime = (TimeInput)windowContainer.FindViewById(Resource.Id.window_start_time);
                var endTime = (TimeInput)windowContainer.FindViewById(Resource.Id.window_end_time);
                var deleteButton = (ImageButton)windowContainer.FindViewById(Resource.Id.window_delete_button);

                startTime.TimeChanged += (time) =>
                {
                    if (ViewModel.SelectedTill != null)
                    {
                        ViewModel.SelectedTill.Windows[index].StartTimeChanged(time);
                        UpdateValidators();
                    }
                };

                endTime.TimeChanged += (time) =>
                {
                    if (ViewModel.SelectedTill != null)
                    {
                        ViewModel.SelectedTill.Windows[index].EndTimeChanged(time);
                        UpdateValidators();
                    }
                };

                deleteButton.Click += (sender, args) =>
                {
                    if (ViewModel.SelectedTill != null)
                    {
                        ViewModel.SelectedTill.Windows[index].Clear();
                        UpdateTillWindows();
                        UpdateValidators();
                    }
                };
            }
        }

        private void UpdateEditButton()
        {
            if (m_editButton != null)
            {
                if(ViewModel.EditMode)
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_confirm);
                }
                else
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_edit);
                }
                m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            }
        }

        private void UpdateTill()
        {
            m_tillContainer.Visibility = (ViewModel.SelectedTill == null ? ViewStates.Invisible : ViewStates.Visible);
            if (ViewModel.SelectedTill != null)
            {
                var tillNumber = (TextInput)m_tillContainer.FindViewById(Resource.Id.till_number);
                var tillType = (SpinnerInput)m_tillContainer.FindViewById(Resource.Id.till_type);
                var tillStatus = (SpinnerInput)m_tillContainer.FindViewById(Resource.Id.till_status);
                
				tillNumber.Value = ViewModel.SelectedTill.TillNumber;
                tillNumber.Editable = ViewModel.EditMode;

                tillType.Value = ViewModel.SelectedTill.TypeId.ToString();
                tillType.Editable = ViewModel.EditMode;

                tillStatus.Value = ViewModel.SelectedTill.StatusId.ToString();
                tillStatus.Editable = ViewModel.EditMode;

                UpdateTillWindows();
            }
        }

        private void UpdateTillWindows()
        {
            if (ViewModel.SelectedTill != null)
            {
                var windowsContainer = (ViewGroup)m_tillContainer.FindViewById(Resource.Id.windows_container);
                windowsContainer.Visibility = (ViewModel.SelectedTill.IsBasket ? ViewStates.Visible : ViewStates.Gone);

                for (var w = 0; w < 3; w++)
                {
                    var window = ViewModel.SelectedTill.Windows[w];
                    var windowContainer = windowsContainer.GetChildAt(w + 1);
                    var startTime = (TimeInput)windowContainer.FindViewById(Resource.Id.window_start_time);
                    var endTime = (TimeInput)windowContainer.FindViewById(Resource.Id.window_end_time);
                    var deleteButton = (ImageButton)windowContainer.FindViewById(Resource.Id.window_delete_button);

                    startTime.Label = string.Format(startTime.Label, (w + 1));
                    startTime.Value = window.StartTime;
                    startTime.Editable = ViewModel.EditMode;

                    endTime.Value = window.EndTime;
                    endTime.Editable = ViewModel.EditMode;

                    deleteButton.Visibility = (EditMode && ViewModel.SelectedTill.Windows[w].Optional ? ViewStates.Visible : ViewStates.Gone);
                }
            }
        }

        // ToDo: Bind
        public void UpdateValidators()
        {
            if (SelectedTill != null && View != null)
            {
                var tillNumberValidator = (ValidationMessageControl)View.FindViewById(Resource.Id.till_number_validator);
                tillNumberValidator.Text = ViewModel.SelectedTill.TillNumberRequiredValidator.ErrorMessage;

                var windowsContainerView = (ViewGroup)m_tillContainer.FindViewById(Resource.Id.windows_container);
                for (var w = 0; w < 3; w++)
                {
                    var windowContainerView = windowsContainerView.GetChildAt(w + 1);
                    var endTimeValidatorView = (ValidationMessageControl)windowContainerView.FindViewById(Resource.Id.validator);
                    var startLessThanEndValidator = ViewModel.SelectedTill.Windows[w].StartLessThanEndValidator;
                    var overlappingWindowsValidator = ViewModel.SelectedTill.Windows[w].OverlappingWindowsValidator;

                    if (!startLessThanEndValidator.IsValid)
                    {
                        endTimeValidatorView.Text = startLessThanEndValidator.ErrorMessage;
                    }
                    else if (!overlappingWindowsValidator.IsValid)
                    {
                        endTimeValidatorView.Text = overlappingWindowsValidator.ErrorMessage;
                    }
                    else
                    {
                        endTimeValidatorView.Text = null;
                    }
                }
            }
        }
    }
}

