﻿using System;

namespace WFM.Android
{
    public interface IAllocationOverviewPanel
    {
        void OnFullRefresh();
        void OnScheduledRefresh();
    }
}

