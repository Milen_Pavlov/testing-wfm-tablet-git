﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using WFM.Android.Views.TillAllocations;

namespace WFM.Android
{
    public class AllocationSettingsView : ConsortiumFragment
	{
        private const int c_topBottomMarginDp = 4;

        private BarButton m_refreshButton;

        public AllocationSettingsViewModel VM { get { return (AllocationSettingsViewModel)this.ViewModel; } }

		public AllocationSettingsView (): base(Resource.Layout.view_till_allocations_settings)
		{
			
		}

		public override void OnViewCreated (View view, Bundle savedInstanceState)
		{
            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += OnRefresh;

            List<BarButton> controlButtons = new List<BarButton>() { m_refreshButton};
            RootView.StaticNavBar.RightButtons = controlButtons;

			base.OnViewCreated (view, savedInstanceState);
			var set = this.CreateBindingSet<AllocationSettingsView, AllocationSettingsViewModel>();

			set.Apply ();
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
		}


		public Type TargetType 
		{ 
			get { return typeof(ConsortiumTabFragment); } 
		}

		#region implemented abstract members of ConsortiumFragment

		public override Type AttachTarget {
			get { return typeof(ConsortiumTabFragment); }
		}

		#endregion

        void OnRefresh(object sender, EventArgs e)
        {
            VM.OnRefresh();
        }
	}
}

