﻿using System;
using Consortium.Client.Android;
using WFM.Core;
using System.Collections.Generic;
using Android.Views;
using Android.OS;
using WFM.Android.Views.TillAllocations;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android.Controls;
using Android.Widget;
using Android.Graphics;

namespace WFM.Android
{
    public class TillsBreaksView : ConsortiumFragment
    {
        private FrameLayout m_tabLoading;
        private ProgressBar m_progressBar;

        private CollectionControl m_breaksCollectionControl;

        private SegmentedControl m_segmentedControl;
        private const int c_topBottomMarginDp = 4;

        private BarButton m_refreshButton;
        private BarButton m_editButton;
        private int[] m_editButtonImages;

        public TillsBreaksViewModel VM { get { return (TillsBreaksViewModel)this.ViewModel; } }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabFragment); }
        }

        private bool m_editMode;
        public bool EditMode
        {
            get { return m_editMode; }
            set 
            { 
                m_editMode = value; 
                m_editButton.SetImageResource(m_editButtonImages[value ? 1: 0]);
            }
        }

        public bool Loading 
        {
            get{ return false; }
            set
            {
                m_tabLoading.Visibility = value ? ViewStates.Visible : ViewStates.Gone;
                m_progressBar.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
                m_breaksCollectionControl.Visibility =  value ? ViewStates.Invisible : ViewStates.Visible;
            }
        }

        public bool NoData 
        {
            get{ return false; }
            set
            {
                m_progressBar.Visibility = value ? ViewStates.Invisible : ViewStates.Visible;
            }
        }

        public IList<BreakItem> Breaks
        {
            get { return VM.Rows; }
            set
            {
                // Clear selection
                m_breaksCollectionControl.SelectedIndex = -1;
            }
        }

        public TillsBreaksView() : base(Resource.Layout.view_till_allocations_breaks)
        {
            Title = "Till Breaks";
            m_editButtonImages = new int[] { Resource.Drawable.icon_edit, Resource.Drawable.icon_confirm };
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_segmentedControl = View.FindViewById (Resource.Id.day_segmented_control) as SegmentedControl;
            m_segmentedControl.OnSelectionChanged += DateSelectionChanged;

            m_breaksCollectionControl = (CollectionControl)View.FindViewById(Resource.Id.breaks_collectionControl);
            m_breaksCollectionControl.RegisterViewWithResourceID<TillBreaksContentCell>(Resource.Layout.cell_till_breaks);

            m_tabLoading = view.FindViewById(Resource.Id.tillBreaksLoading) as FrameLayout;
            m_progressBar = view.FindViewById(Resource.Id.tillProgressBarLoading) as ProgressBar;

            m_editButton = new BarButton(ConsortiumApp.Context);
            m_editButton.SetImageResource(Resource.Drawable.icon_edit);
            m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_editButton.Click += OnEdit;

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += OnRefresh;

            List<BarButton> controlButtons = new List<BarButton>() { m_editButton, m_refreshButton };
            RootView.StaticNavBar.RightButtons = controlButtons;

            var set = this.CreateBindingSet<TillsBreaksView, TillsBreaksViewModel>();

            set.Bind(this).For(b => b.EditMode).To(m => m.EditMode);
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(b => b.NoData).To(m => m.NoData);
            set.Bind(this).For(b => b.Breaks).To(m => m.Rows);

            set.Bind(m_segmentedControl).For(b => b.Segments).To(m => m.Segments);
            set.Bind(m_segmentedControl).For(b => b.SelectedIndex).To(m => m.SelectedDayIndex);

            set.Apply();
        }

        public override void OnPause ()
        {
            base.OnPause ();

            if (VM != null)
                VM.DetachEvents ();
        }

        public override void OnResume ()
        {
            base.OnResume ();

            if (VM != null)
                VM.AttachEvents ();
        }

        private void DateSelectionChanged(int index)
        {
            VM.OnSelectDay(index);
        }

        void OnEdit(object sender, EventArgs e) 
        {
            VM.OnEdit();
        }

        void OnRefresh(object sender, EventArgs e)
        {
            VM.OnRefresh();
        }
    }
}

