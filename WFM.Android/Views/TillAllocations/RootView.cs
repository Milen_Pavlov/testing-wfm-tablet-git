using System;
using System.Collections.Generic;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using Consortium.Client.Core;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.Android.Views.TillAllocations
{
    public class RootView : ConsortiumTabFragment//, IConsortiumViewSource
    {
        public static ConsortiumActionBar StaticNavBar { get; set; }
        private SegmentedControl m_segmentedControl;

        public RootViewModel VM { get { return (RootViewModel)this.ViewModel; } }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumDrawerView); }
        }

        public Type TargetType 
        { 
            get { return typeof(ConsortiumDrawerView); } 
        }

        public int TabSelectedIndex
        {
            get { return 1; }
            set { m_segmentedControl.SelectedIndex = value; }
        }

        public RootView()
            : base(Resource.Layout.view_till_allocations, Resource.Id.tab_content_layout)
        {
            Title = "Till Allocations";
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // TODO: Hack to gain access to thhe NavBar in tab page views
            StaticNavBar = NavBar;

            m_segmentedControl = new SegmentedControl(ConsortiumApp.Context)
            {
                MinWidth = (int)DimensionUtility.ConvertDpToPixel(80),
                MaxWidth = (int)DimensionUtility.ConvertDpToPixel(80),
                SelectedColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.white),
                SelectedTextColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.primary),
                UnSelectedColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.primary),
                UnSelectedTextColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.white),
                LayoutParameters = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent, GravityFlags.CenterVertical),
            };

            IStringService localiser = Mvx.Resolve<IStringService>();
            m_segmentedControl.Segments = new List<string>(new string[] {localiser.Get("allocation_overview"), localiser.Get("allocation") , localiser.Get("breaks"), localiser.Get("tills"), localiser.Get("allocation_settings") });

            m_segmentedControl.OnSelectionChanged += HeaderSelectionChanged;
            NavBar.MiddleLayout.AddView(m_segmentedControl);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            var set = this.CreateBindingSet<RootView, RootViewModel>();
            set.Bind (this).For (v => v.TabSelectedIndex).To (vm => vm.TabIndex);
            set.Apply();
        }

        void HeaderSelectionChanged(int index)
        {
            VM.OnTabSelect(index);
        }

    }
}

