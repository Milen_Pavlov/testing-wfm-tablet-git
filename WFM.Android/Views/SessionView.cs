using Android.App;
using Android.OS;
using Android.Views.InputMethods;
using Android.Widget;
using Android.Content;
using Consortium.Client.Android;
using Consortium.Client.Core;
using WFM.Core;
using Android.Content.PM;
using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.Android
{
    [Activity(Label = "SessionView", ScreenOrientation = ScreenOrientation.Landscape, Theme = "@style/Theme.Popover")]
	public class SessionView : ConsortiumModalView
	{
        private readonly ISpinnerService m_spinnerService;
        private readonly IStringService m_localiser;

        Button m_signInButton;
        SpinnerToken m_spinnerToken;

		public SessionViewModel VM
		{
			get { return (SessionViewModel)this.ViewModel; }
		}

        private bool m_loading = false;
        public bool Loading 
        {
            get 
            { 
                return m_loading;
            }
            set 
            {
                m_loading = value;
                if (value)
                    ShowSpinner ();
                else
                    DismissSpinner ();
            }
        }

        public SessionView() : base(Resource.Layout.view_sign_in)
        {
            m_spinnerService = Mvx.Resolve<ISpinnerService> ();
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate (bundle);

			Mvx.Trace ("DP Value Loaded {0}", Resources.GetString(Resource.String.dimension_value));

            var button = FindViewById (Resource.Id.environmentsButton);
            button.Click += (object sender, EventArgs e) => 
            {
                var vm = ViewModel as SessionViewModel;
                ConsortiumPopoverView.SourceView = button;
                vm.OnShowSettings();
            };

            m_signInButton = (Button)FindViewById(Resource.Id.SignIn);
            m_signInButton.Click += SignIn;

			EditText passwordEditText = FindViewById<EditText>(Resource.Id.Password);
            passwordEditText.EditorAction += (object sender, TextView.EditorActionEventArgs e) => 
            {
                e.Handled = false;
                if (e.ActionId == ImeAction.Go) 
                {
                    InputMethodManager manager = (InputMethodManager)this.GetSystemService (Context.InputMethodService);
                    manager.HideSoftInputFromWindow (passwordEditText.WindowToken, 0);

                    SignIn (null, null);

                    e.Handled = true;
                }
            };

            var set = this.CreateBindingSet<SessionView, SessionViewModel> ();
            set.Bind (this).For(v => v.Loading).To (m => m.Loading);
            set.Apply ();
        }

        void SignIn (object sender, EventArgs e)
        {
            ConsortiumPopoverView.SourceView = m_signInButton;
            VM.OnSignIn();
        }

        public override void OnBackPressed()
        {
            MoveTaskToBack (true);
        }

        private void ShowSpinner ()
        {
            DismissSpinner ();
            m_spinnerToken = m_spinnerService.Show (m_localiser.Get ("signing_in_ellipsis"));
        }

        private void DismissSpinner ()
        {
            if (m_spinnerToken != null) 
            {
                m_spinnerService.Dismiss (m_spinnerToken);
                m_spinnerToken = null;
            }
        }

    }
}
