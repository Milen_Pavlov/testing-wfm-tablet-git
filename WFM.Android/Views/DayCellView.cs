using System;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Android;
using WFM.Core;
using Java.Lang;
using Consortium.Client.Core;
using System.Collections.Generic;
using System.Diagnostics;
using Android.Animation;

namespace WFM.Android
{
    public class DayCellView : FrameLayout, View.IOnTouchListener, View.IOnDragListener, ValueAnimator.IAnimatorUpdateListener
    {
        static protected float c_autoScroll = 100.0f;

        public event Action<ShiftData> OnShiftSelected;
        public event Action<TimeOffData> OnTimeOffSelected;

        private const string SHIFTVIEW_TAG = "Shift View";

        private ScheduleService m_scheduleService;
        private SpareShiftService m_spareShiftService;

		// View dimensions
		private int m_width;
		private int m_height;

		// Drawables
		BitmapDrawable m_Unavailable_BitmapDrawable;
		BitmapDrawable m_TimeOff_BitmapDrawable;
		BitmapDrawable m_Minor_BitmapDrawable;

		// Misc
		private JDAEmployeeInfo m_info;
		private DateTime m_dayStart;
        private List<JDAEmployeeAssignedJob> m_fixedShifts;
        private Paint m_fixedShiftPaint;

		// Backing vars
		private EmployeeData m_employeeData;

        //Internal controls
        CurrentTimeControl m_timeControl;
        ShiftControl m_defaultView = null;
        List<View> m_viewsAttached = new List<View>();

		// Gesture stuff
		GestureDetector m_gestureDetector;

        private Color m_currentColour;

        const int c_sideOverlap = 10; //< Invisible area to left and right to capture resize touches
        const int c_sideTouch = c_sideOverlap * 3; //< Area on left and right which will activate size over move
        const int c_hoursInView = 24;
        const int c_minutesInDay = 1440; 

        private float m_timeInterval = c_minutesInDay / ScheduleTime.DayIntervals;

        public bool IsTimeOff { get; set; } 

        public bool CanEdit
        {
            get
            {
                if (EmployeeData != null)
                    return EmployeeData.CanEdit;
                else
                    return false;
            }
        }

        private bool m_dragActive = false;

        public EmployeeData EmployeeData
        {
            get { return m_employeeData; }

            set 
            { 
                m_employeeData = value; 
                m_info = m_employeeData == null ? null : m_employeeData.Info;
                RefreshShiftViews (); 
                RefreshBackgroundColour ();
            }
        }

        private ShiftControl m_dragControl;
        private PointF m_dragStartPoint;
        static private ShiftData m_draggedShift;
        private DragCollectionControl m_dragView;

		public DayCellView(Context context) : base(context) 
		{ 
			Init();
		}

		public DayCellView(Context context, IAttributeSet attrs ) : base(context, attrs) 
		{ 
			Init();
		}

		public DayCellView(Context context, IAttributeSet attrs, int defStyle ) : base(context, attrs, defStyle) 
		{ 
			Init();
		}

        private void Init()
        { 
            m_scheduleService = Mvx.Resolve<ScheduleService> ();
            m_spareShiftService = Mvx.Resolve<SpareShiftService> ();

            SetWillNotDraw (false);
            CreateResources ();
            SetOnDragListener(this);
            SetupClickHandler();

            ConstructTimeControl();

            m_currentColour = Color.White;
        }

        void CreateResources()
        {
            m_Unavailable_BitmapDrawable    = CreateTileableBitmapDrawable(Resource.Drawable.hatch);
            m_TimeOff_BitmapDrawable        = CreateTileableBitmapDrawable(Resource.Drawable.hatch_dark);
            m_Minor_BitmapDrawable          = CreateTileableBitmapDrawable(Resource.Drawable.hatch_red);
        }

        private void SetupClickHandler()
        {
            m_gestureDetector = new GestureDetector (new GestureListener( this )); // Note we pass in 'this' class so the listener can get at the data
            SetOnTouchListener(this); 
        }

        void ConstructTimeControl()
        {
            m_timeControl = new CurrentTimeControl(Context);
            m_timeControl.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
            AddView(m_timeControl);
        }

        private static BitmapDrawable CreateTileableBitmapDrawable(int resID)
        {
            Bitmap bmp = BitmapFactory.DecodeResource (Application.Context.Resources, resID );
            BitmapDrawable bitmapDrawable = new BitmapDrawable (bmp);
            bitmapDrawable.SetTileModeXY (Shader.TileMode.Repeat, Shader.TileMode.Repeat);      
            return bitmapDrawable;
        }

		public void RefreshShiftViews()
		{
            ClearConstructedViews();

            if (m_employeeData == null || !m_employeeData.DayIndex.HasValue)
                return;

            if (m_employeeData.SpareShift == true)
            {
                //Add Label and set background
                var label = new TextView(Context);
                label.SetText("Add Shift", TextView.BufferType.Normal);
                label.TextSize = Resources.GetDimension (Resource.Dimension.Allocations_Cell_Content_UnassignedTextSize);
                label.SetTextColor (Color.White);
                label.SetBackgroundColor (new Color (0, 0, 0, 48));
                label.Gravity = GravityFlags.Center;
                label.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
                AddView (label, 0);
                m_viewsAttached.Add (label);
            }

            if(m_employeeData.DayIndex.Value == 0) // DayIndex is 0, pass in lastdaypreviousweek to add shift view
            {
                AddShiftViews(m_employeeData.Days [m_employeeData.DayIndex.Value], m_employeeData.LastDayPreviousWeek, m_employeeData.CanEdit);
            }
            else // pass in previous day to add shift view
            {
                AddShiftViews(m_employeeData.Days [m_employeeData.DayIndex.Value], m_employeeData.Days [m_employeeData.DayIndex.Value - 1], m_employeeData.CanEdit);
            }

            var day = m_employeeData.Days[m_employeeData.DayIndex.Value];

            m_timeControl.MinDate = day.DayStart;
            m_timeControl.MaxDate = day.DayStart.AddDays(1);

            m_timeControl.Invalidate();
            PostInvalidate ();
        }

        void ClearConstructedViews()
        {
            foreach (View v in m_viewsAttached)
            {
                RemoveView(v);
            }

            m_viewsAttached.Clear();

            if (m_defaultView != null)
                m_defaultView.Visibility = ViewStates.Invisible;
        }

        private void AddShiftViews(EmployeeDayData day, EmployeeDayData previousDay, bool canEdit)
        {      
            bool canRecycleControl = m_defaultView != null;

			// Current day shifts
            if (day.Shifts != null && day.Shifts.Count > 0)
            {
                foreach (var shift in day.Shifts)
                {
                    if (canRecycleControl)
                        PopulateShiftView (shift, day, canEdit, m_defaultView);
                    else
                        CreateShiftView (shift, day, canEdit);

                    canRecycleControl = false;
                }
            }
            else
            {
                if (canRecycleControl)
                    PopulateShiftView (null, day, canEdit, m_defaultView);
                else
                    CreateShiftView (null, day, canEdit);

                canRecycleControl = false;
            }
                
            // See if the employee has the booked day off
            IsTimeOff = false;
            foreach(var unavail in day.UnavailabilityRanges)
            {
                if (unavail.Type == "t")
                {
                    IsTimeOff = true;
                }
            }

            // Previous day shifts
            if(previousDay != null)
            {
                foreach(var shift in previousDay.Shifts)
                {
                    if(shift.End > day.DayStart) // previous day shift runs over to current day
                    {
                        if (canRecycleControl)
                            PopulateShiftView(shift, day, canEdit, m_defaultView);
                        else
                            CreateShiftView(shift, day, canEdit);

                        canRecycleControl = false;
                    }
                }
            }
        }

        private void PopulateShiftView(ShiftData shift, EmployeeDayData day, bool canEdit, ShiftControl shiftControl)
        {
            shiftControl.Setup(shift, day.DayStart, canEdit);

            // Sets the tag
            shiftControl.SetTag(shiftControl.Id, SHIFTVIEW_TAG);

            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
            shiftControl.LayoutParameters = layoutParams;

            shiftControl.Visibility = ViewStates.Visible;

            m_fixedShifts = day.FixedShifts;

            if(m_fixedShiftPaint == null)
            {
                m_fixedShiftPaint = new Paint 
                {
                    Color = Color.Black, 
                    AntiAlias = true,
                    StrokeWidth = 1.0f
                };
                m_fixedShiftPaint.SetStyle(Paint.Style.Stroke);
            }
        }

        private void CreateShiftView(ShiftData shift, EmployeeDayData day, bool canEdit)
        {
            var shiftControl = new ShiftControl(Context);

            shiftControl.ShiftSelect += OnShiftSelect;
            shiftControl.ShiftReassign += OnShiftReassign;

            PopulateShiftView(shift, day, canEdit, shiftControl);

            AddView(shiftControl);

            if (m_defaultView == null)
                m_defaultView = shiftControl;
            else
                m_viewsAttached.Add(shiftControl);
        }

        public void OnShiftSelect(ShiftData shift)
        {
            if (OnShiftSelected != null)
            {
                OnShiftSelected(shift);
            }
        }

        public void OnTimeOffSelect()
        {
            if (OnTimeOffSelected != null)
            {
                OnTimeOffSelected( new TimeOffData() { Employee = m_employeeData, Day = m_dayStart } );
            }
        }

        public void ProcessDroppedShift(DateTime shiftStart, DateTime shiftEnd, int clampedMinutes, int oldEmployeeId, int shiftId)
        {
            bool isSameEmployee = EmployeeData.EmployeeID == oldEmployeeId;
            if(isSameEmployee && clampedMinutes == 0)
            {
                return; // If no change to shift on update, halt here
            }

            shiftStart = shiftStart.AddMinutes(clampedMinutes);
            shiftEnd = shiftEnd.AddMinutes(clampedMinutes);

            ShiftEditMessage shiftEdit= new ShiftEditMessage(this)
                {
                    Op = ShiftEditMessage.Operation.UpdateShift,
                    EmployeeID = oldEmployeeId,
                    ShiftID = shiftId,
                    Start = shiftStart,
                    End = shiftEnd,
                    RecalculateDetails = true
                };

            if(!isSameEmployee)
            {
                shiftEdit.Op = ShiftEditMessage.Operation.ReassignShift;
                shiftEdit.NewEmployeeID = EmployeeData.EmployeeID;
            }

            Mvx.Resolve<IMvxMessenger> ().Publish (shiftEdit);
        }

        public void OnShiftReassign(View v, ShiftData shift, Point touchPoint)
        {
            // Ensure touchpoint is not negative
            if(touchPoint.X <= 0) { touchPoint.X = 0; }
            if(touchPoint.Y <= 0) { touchPoint.Y = 0; }

            // Setup data to pass throw with drag event
            ClipData.Item shiftId = new ClipData.Item(shift.ShiftID.ToString());
            ClipData.Item oldEmployeeId = new ClipData.Item(shift.EmployeeID.ToString());
            ClipData.Item shiftStartTicks = new ClipData.Item(shift.Start.Ticks.ToString());
            ClipData.Item shiftEndTicks = new ClipData.Item(shift.End.Ticks.ToString());
            ClipData.Item shiftViewWidth = new ClipData.Item(v.Width.ToString());
            ClipData.Item firstTouchPointX = new ClipData.Item(touchPoint.X.ToString());
            ClipData.Item isManualShift = new ClipData.Item (m_employeeData.SpareShift.ToString ());

            string[] clipDescriptor = {ClipDescription.MimetypeTextPlain, ClipDescription.MimetypeTextPlain};

            ClipData dragData = new ClipData(new ClipDescription("Plain", clipDescriptor), shiftId);
            dragData.AddItem(oldEmployeeId);
            dragData.AddItem(shiftStartTicks);
            dragData.AddItem(shiftEndTicks);
            dragData.AddItem(shiftViewWidth);
            dragData.AddItem(firstTouchPointX);
            dragData.AddItem (isManualShift);

            ShiftDragShadow dragShadow = new ShiftDragShadow(v, touchPoint);
            m_dragStartPoint = new PointF(touchPoint.X, touchPoint.Y);
            // Starts the drag
            v.StartDrag(dragData,
                dragShadow,
                dragData, 
                0) ;
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            return m_gestureDetector.OnTouchEvent(e);
        }

        public bool OnDrag(View v, DragEvent e)
        {
            if (m_dragView == null)
            {
                m_dragView = GetDragView (this);
                if (m_dragView != null)
                {
                    m_dragView.AutoScrollWhenDragging = false;
                }
            }
            switch (e.Action) {
                case DragAction.Started:
                    if ( CanAcceptDraggedShift (e) == false)
                    {
                        FadeToColour (Color.LightGray);

                    }
                    return true;
                case DragAction.Entered:
                    ShowDragUI (e);
                    return false;
                case DragAction.Exited:
                    HideDragUI();
                    return false;
                case DragAction.Drop:
                    if (CanAcceptDraggedShift (e) == false)
                    {
                        return false;
                    }
                    // Send message that a shift drag has ended.
                    // Retrieve ClipData
                    int shiftId = Integer.ParseInt (e.ClipData.GetItemAt (0).Text);
                    int oldEmployeeId = Integer.ParseInt (e.ClipData.GetItemAt (1).Text);
                    DateTime shiftStart = new DateTime (Long.ParseLong (e.ClipData.GetItemAt (2).Text));
                    DateTime shiftEnd = new DateTime (Long.ParseLong (e.ClipData.GetItemAt (3).Text));
                    int shiftViewWidth = Integer.ParseInt (e.ClipData.GetItemAt (4).Text);
                    float firstTouchPointX = Float.ParseFloat (e.ClipData.GetItemAt (5).Text);
                    bool isManualShift = bool.Parse (e.ClipData.GetItemAt (6).Text);

                    // Calculate shift time change
                    int clampedMinutes = 0;
                    if (isManualShift == false || oldEmployeeId == m_employeeData.EmployeeID )
                    {
                        float xDelta = (float)(e.GetX () - firstTouchPointX);
                        double minutesAdditional = MinutesFromXPos (xDelta, c_hoursInView, shiftViewWidth);
                        clampedMinutes = (int)(System.Math.Round (minutesAdditional / m_timeInterval) * m_timeInterval);
                    }

                    ProcessDroppedShift(shiftStart, shiftEnd, clampedMinutes, oldEmployeeId, shiftId);

                    if (m_dragView != null)
                    {
                        m_dragView.StopScrolling ();
                    }
                    return false;
                case DragAction.Ended:
                    // Reset cell.
                    FadeToColour (Color.White);
                    m_draggedShift = null;
                    HideDragUI();
                    if (m_dragView != null)
                    {
                        m_dragView.StopScrolling ();
                    }
                    return true;
                case DragAction.Location:
                    UpdateDragUI (e);
                    if (m_dragView != null)
                    {
                        View testView = this;
                        float top = 0.0f;

                        while((testView as DragCollectionControl) == null)
                        {
                            top += testView.Top;
                            testView = (testView.Parent as View);
                            if(testView == null)
                                break;
                        }

                        float dragAreaRelativePos = top + e.GetY ();
                        if (dragAreaRelativePos < c_autoScroll)
                        {
                            // Start Scrolling up
                            m_dragView.StartScrollingUp ();
                        }
                        else if (dragAreaRelativePos > (m_dragView.Bottom - m_dragView.Top) - c_autoScroll)
                        {
                            //Start Scrolling down
                            m_dragView.StartScrollingDown ();
                        }
                        else
                        {
                            m_dragView.StopScrolling ();
                        }
                    }
				    return true;
            }

            return false;
        }

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{
			// Get the size of the control
			m_width = View.MeasureSpec.GetSize(widthMeasureSpec);
			m_height = View.MeasureSpec.GetSize(heightMeasureSpec);
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
		}

		protected override void OnDraw(Canvas canvas)
		{
			base.OnDraw(canvas);

            if (m_employeeData != null && m_employeeData.DayIndex.HasValue)
            {
                List<JDAInterval> unavailabilityRanges = new List<JDAInterval>();

                foreach(var day in m_employeeData.Days)
                {
                    unavailabilityRanges.AddRange(day.UnavailabilityRanges);
                }

                DrawDayArea(canvas, m_employeeData.Days[m_employeeData.DayIndex.Value].DayStart, unavailabilityRanges, m_employeeData.CanEdit);

                DrawFixedShifts(canvas);
            }
		}

        private void DrawDayArea(Canvas canvas, DateTime dayStart, List<JDAInterval> unavailabilityRanges, bool canEdit)
		{
            m_dayStart = dayStart;

            if (unavailabilityRanges != null)
			{
                foreach (var unavail in unavailabilityRanges)
                    DrawUnavailable(canvas, dayStart, unavail);
			}
		}

        private double MinutesFromXPos(float xPos, float hoursInView, int viewWidth)
        {
            float widthOfHour = ((float) viewWidth) / hoursInView;
            float widthOfMinute = widthOfHour / 60;
            return (double) xPos / widthOfMinute;
        }

		private float XPosFromTime(DateTime start, DateTime end, DateTime point)
		{
			DateTime clamped = DateTimeUtility.Clamp(point, start, end);
			TimeSpan offset = clamped - start;
			TimeSpan total = end - start;
			return (float)(offset.TotalMinutes * m_width / total.TotalMinutes);
		}

		private RectF RectFromTime(DateTime dayStart, DateTime start, DateTime end, float yBorder)
		{
			DateTime dayEnd = dayStart.AddDays(1);

			float x1 = XPosFromTime(dayStart, dayEnd, start);
			float x2 = XPosFromTime(dayStart, dayEnd, end);

			return new RectF () 
            {
				Left = x1,
				Top = yBorder,
				Right = x2,
				Bottom = m_height - yBorder
			};
		}

		private bool RectangleWidthIsZero(RectF rect)
		{
			return((int)rect.Left == (int)rect.Right);
		}

		private void DrawUnavailable(Canvas canvas, DateTime dayStart, JDAInterval interval)
		{
			var rect = RectFromTime(dayStart, interval.Start, interval.End, 0);

			if (RectangleWidthIsZero(rect) )
				return;

			BitmapDrawable bitmapDrawable = m_Unavailable_BitmapDrawable; // default

			if (interval.Type == "t")
				bitmapDrawable = m_TimeOff_BitmapDrawable;
			else if (interval.Type == "m")
				bitmapDrawable = m_Minor_BitmapDrawable;

			// Set exact shape of this bitmapDrawable and draw the rectangle
			bitmapDrawable.SetBounds((int)rect.Left, (int)rect.Top, (int)rect.Right, (int)rect.Bottom);
			bitmapDrawable.Draw(canvas);
		}

        private void SendCreateShiftMessage(float atXPosition)
        {
            double minutes = (atXPosition / m_width) * ScheduleTime.MinutesPerDay;
            DateTime centre = m_employeeData.Days[m_employeeData.DayIndex.Value].DayStart.AddMinutes(minutes);

            ShiftEditMessage request = new ShiftEditMessage(this) 
            {
                Op = ShiftEditMessage.Operation.CreateDefaultShift,
                EmployeeID = m_info.EmployeeID,
                Start = centre,
                End = centre
            };

            Mvx.Resolve<IMvxMessenger> ().Publish (request);
        }

        private bool CanAcceptDraggedShift(DragEvent e)
        {
            if (e == null)
                return false;
            
            ClipData clipData = e.LocalState as ClipData;
            if (clipData == null)
                return false;
            
            int  originalEmployeeId = Integer.ParseInt(clipData.GetItemAt(1).Text);
            EmployeeData originalEmployee = m_scheduleService.Schedule.GetEmployee (originalEmployeeId);
            if (originalEmployee == null)
            {
                originalEmployee = m_spareShiftService.GetEmployee (originalEmployeeId);
            }

            if (originalEmployee == null)
            {
                return false;
            }
            
            int  shiftId = Integer.ParseInt(clipData.GetItemAt(0).Text);
            m_draggedShift = originalEmployee.GetShift (shiftId);

            return m_employeeData.CanWorkShift (m_draggedShift);
        }
            
        private void FadeToColour(Color destColour)
        {
            ValueAnimator colorAnimation = ValueAnimator.OfObject (new ArgbEvaluator (), m_currentColour.ToArgb (), destColour.ToArgb ());
            colorAnimation.SetDuration (100);
            colorAnimation.AddUpdateListener (this);
            colorAnimation.Start ();    
        }

        public void OnAnimationUpdate(ValueAnimator animator)
        {
            m_currentColour = new Color ((int)animator.AnimatedValue);
            SetBackgroundColor (m_currentColour);
        }

        private void ShowDragUI(DragEvent e)
        {
            if (e.LocalState != null) 
            {
                ClipData data = e.LocalState as ClipData;
               
                bool isManualShift = bool.Parse (data.GetItemAt (6).Text);
                if (isManualShift == false)
                    return;
                
                if (m_dragControl != null) 
                {
                    RemoveView (m_dragControl);
                    m_dragControl.Dispose ();
                    m_dragControl = null;
                }

                if (m_scheduleService.Schedule == null)
                    return;
                    
                int oldEmployeeId = Integer.ParseInt (data.GetItemAt (1).Text);
                EmployeeData employee = m_scheduleService.Schedule.GetEmployee (oldEmployeeId);
                if (employee == null)
                {
                    employee = m_spareShiftService.GetEmployee (oldEmployeeId);
                }
                if (employee == null)
                {
                    return;
                }

                int shiftId = Integer.ParseInt (data.GetItemAt (0).Text);
                ShiftData shiftData = employee.GetShift(shiftId);

                m_dragControl = new ShiftControl (Context);
                m_dragControl.Setup(shiftData, shiftData.Start.Date, false);

                // Sets the tag
                m_dragControl.SetTag(m_dragControl.Id, SHIFTVIEW_TAG);

                m_dragControl.Alpha = 0.6f;

                m_dragControl.LayoutParameters = new FrameLayout.LayoutParams (FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
                AddView (m_dragControl);
            } 
        }

        private void HideDragUI()
        {
            if (m_dragControl != null) 
            {
                RemoveView (m_dragControl);
                m_dragControl.Dispose ();
                m_dragControl = null;

                SetBackgroundColor (Color.White);
            }
        }

        private void UpdateDragUI(DragEvent e)
        {
            if (m_dragControl != null && m_dragStartPoint != null && e.LocalState != null)
            {
                ClipData data = e.LocalState as ClipData;
                int oldEmployeeId = Integer.ParseInt (data.GetItemAt (1).Text);
                if (oldEmployeeId == EmployeeData.EmployeeID)
                {
                    m_dragControl.SetX (e.GetX () - m_dragStartPoint.X);
                }
            }
        }

        private void RefreshBackgroundColour ()
        {
            if (m_draggedShift == null)
            {
                FadeToColour (Color.White);
                return;
            }
            
            bool canWorkShift = m_employeeData.CanWorkShift (m_draggedShift);
            if (canWorkShift == true)
                FadeToColour (Color.White);
            else
                FadeToColour (Color.LightGray);
        }

        protected override void OnAttachedToWindow ()
        {
            base.OnAttachedToWindow ();
            Utilities.Helpers.dragEventFix (this);
        }

        private DragCollectionControl GetDragView (View view)
        {
            if (view == null)
                return null;

            if ((view as DragCollectionControl) != null)
                return view as DragCollectionControl;

            return GetDragView (view.Parent as View);
        }

        private void DrawFixedShifts(Canvas canvas)
        {
            if (m_fixedShifts != null)
            {
                foreach (var shift in m_fixedShifts)
                    DrawFixedShift(canvas, m_dayStart, shift);
            }
        }

        private void DrawFixedShift(Canvas canvas, DateTime dayStart, JDAEmployeeAssignedJob job)
        {
            if (!job.End.HasValue)
                return;

            var rect = RectFromTime(dayStart, job.Start, job.End.Value, 2);

            if (RectangleWidthIsZero(rect))
                return;

            WFM.Android.Utilities.Helpers.drawRoundRect(canvas, m_fixedShiftPaint, rect, 4.0f);
        }


        private class GestureListener : GestureDetector.SimpleOnGestureListener 
        {
            private DayCellView m_dayCellView;

            public GestureListener(DayCellView dayCellView) : base()
            {
                m_dayCellView = dayCellView;
            }

            public override bool OnDown(MotionEvent e) 
            {
                return true; // Need 'true' to continue listening (for things like double-tap!)
            }

            public override bool OnSingleTapConfirmed (MotionEvent e)
            {
                if (m_dayCellView.IsTimeOff)
                {
                    m_dayCellView.OnTimeOffSelect ();
                }
                return base.OnSingleTapConfirmed (e);
            }

            public override bool OnDoubleTap(MotionEvent e) 
            {
                if (m_dayCellView.CanEdit)
                {
                    m_dayCellView.SendCreateShiftMessage(e.GetX());
                }

                return true; 
            }          
        }
	}

    internal class ShiftDragShadow : View.DragShadowBuilder
    {
        private Point m_touchPoint;
        public bool Invisible { get; set; }
        public ShiftDragShadow(View v, Point touchPoint) : base(v)
        {
            m_touchPoint = touchPoint;
        }

        public override void OnProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
        {
            shadowSize.Set (View.Width, View.Height);
            shadowTouchPoint.Set (m_touchPoint.X, m_touchPoint.Y);
        }

        public override void OnDrawShadow (Canvas canvas)
        {
            if (Invisible)
                return;
            base.OnDrawShadow (canvas);
        }
    }
}
