using System;
using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Android;
using Consortium.Client.Core;
using WFM.Core;
using Android.Views;
using Android.Content.PM;
using Android.Animation;
using Android.Widget;
using Android.Views.InputMethods;

namespace WFM.Android
{
    [Activity(Label = "DrawerView", ScreenOrientation = ScreenOrientation.Landscape)]
	public class DrawerView : ConsortiumDrawerView
	{
        private float m_orgPosition;
        private ViewGroup m_orgSelectGroup;
        private CollectionControl m_drawerList;
        private CollectionControl m_orgSelectList;
        private ObjectAnimator m_animator;

        public bool DrawerOpenState
        {
            get { return this.IsOpen; }
            set 
            {
                if (value)
                    DrawerOpen (); 
                else 
                    DrawerClose(); 
            }
        }

        private int m_currentDrawerItem;
        public int CurrentDrawerIndex
        {
            get { return m_currentDrawerItem; }
            set 
            { 
                m_currentDrawerItem = value; 
                if(m_drawerList!=null)
                    m_drawerList.SelectedIndex = value;
            }
        }

        private int m_currentOrgIndex;
        public int CurrentOrgIndex 
        {
            get { return m_currentOrgIndex; }
            set
            {
                m_currentOrgIndex = value;
                if (m_orgSelectList != null)
                    m_orgSelectList.SelectedIndex = value;
            }
        }

        public bool ShowOrgUnits
        {
            get { return true; }
            set { MoveOrgSelection (value ? 0 : m_orgPosition, 0.2f); }
        }

        public override void DrawerOpen()
        {
            m_orgSelectGroup.TranslationX = m_orgPosition;
            base.DrawerOpen ();

            View currentView = ConsortiumApp.CurrentActivity.Window.CurrentFocus;
            if (currentView != null)
            {
                var manager = (InputMethodManager)ConsortiumApp.CurrentActivity.GetSystemService(Activity.InputMethodService);
                manager.HideSoftInputFromWindow (currentView.WindowToken, 0);
            }
        }

        private void MoveOrgSelection(float target, float length)
        {
            if (m_animator != null)
            {
                m_animator.Cancel ();
                m_animator.Dispose ();
            }

            m_animator = ObjectAnimator.OfFloat(m_orgSelectGroup, "translationX", m_orgSelectGroup.TranslationX, target);
            m_animator.SetDuration((long)(length * 1000));
            m_animator.Start();
        }

        public DrawerView() : base(Resource.Layout.view_drawer, Resource.Id.drawer_layout, Resource.Id.content_frame, Resource.Id.drawer_frame)
        {
            SwapImmediately = true;
            ShowBackButton = true;
        }

		protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate (bundle);

            m_orgSelectList = FindViewById (Resource.Id.drawer_store_list) as CollectionControl;
            m_orgSelectGroup = FindViewById (Resource.Id.drawer_org_layout) as LinearLayout;
            m_drawerList = FindViewById (Resource.Id.drawer_menu_list) as CollectionControl;

            // Store the initial position
            m_orgPosition = m_orgSelectGroup.TranslationX;

            RequestedOrientation = ScreenOrientation.SensorLandscape;

            if (NavBar != null)
            {
                NavBar.BackgroundColorId = Resource.Color.primary;
                if (NavBar.ActionBar != null && NavBar.ActionBar.CustomView != null && NavBar.ActionBar.CustomView.Parent != null)
                {
                    global::Android.Widget.Toolbar toolbar = NavBar.ActionBar.CustomView.Parent as global::Android.Widget.Toolbar;

                    if (toolbar != null)
                        toolbar.SetContentInsetsAbsolute (0, 0);
                }
            }

            m_drawerList.SelectedIndex = 1;
            m_orgSelectList.SelectedIndex = 0;

            // View Model Binding
            var set = this.CreateBindingSet<DrawerView, DrawerViewModel> ();
            set.Bind (this).For (v => v.DrawerOpenState).To (vm => vm.DrawerOpen);
            set.Bind (this).For (v => v.CurrentDrawerIndex).To (vm => vm.CurrentDrawerIndex);
            set.Bind (this).For (v => v.ShowOrgUnits).To (vm => vm.ShowOrgUnits);
            set.Bind (this).For (v => v.CurrentOrgIndex).To (vm => vm.CurrentOrgIndex);
            set.Apply ();
        }
	}
}
