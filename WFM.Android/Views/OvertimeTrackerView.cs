﻿using System;
using Consortium.Client.Android;
using Android.App;
using WFM.Core;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Collections.Generic;

namespace WFM.Android
{
    [Activity(Label = "OvertimeTrackerView")]
    public class OvertimeTrackerView : ConsortiumFragment
    {
        private const int c_topBottomMarginDp = 4;

        private View m_content;
        private FrameLayout m_loading;
        private ProgressBar m_loadingProgress;
        private BarButton m_refreshButton;
        private BarButton m_editButton;
        private TextView m_dateLabel;
        private CollectionControl m_headerCollectionControl;
        private AlternateBackgroundControl m_rowsControl;
        private RelativeLayout m_rowsContainer;

        public override Type AttachTarget
        {
            get
            {
                return typeof(ConsortiumDrawerView);
            }
        }

        public OvertimeTrackerViewModel VM {
            get { return (OvertimeTrackerViewModel)DataContext; }
        }

        public bool Loading
        {
            get { return false; }
            set 
            {
                m_loadingProgress.Visibility = value ? ViewStates.Visible : ViewStates.Invisible;
            }
        }

        public string LoadingMessage
        {
            get
            {
                return string.Empty; 
            }
            set
            {
                var loadingMessageEmpty = string.IsNullOrEmpty(value);
                m_loading.Visibility = loadingMessageEmpty ? ViewStates.Gone : ViewStates.Visible;  
                m_content.Visibility = !loadingMessageEmpty ? ViewStates.Gone : ViewStates.Visible;
            }
        }

        private DateTime m_currentDate;
        public DateTime CurrentDate
        {
            get { return m_currentDate; }
            set { m_currentDate = value; }
        }

        public bool EditMode
        {
            get { return VM.IsModified; }
            set 
            {
                UpdateEditButton();
            }
        }

        public int ScrollToRow
        {
            get { return 0; }
            set 
            { 
                m_rowsControl.PostDelayed (() =>
                    {
                        if (m_rowsControl != null && m_rowsControl.Items != null && value < m_rowsControl.Items.Count)
                            m_rowsControl.SmoothScrollToPosition (value);
                    }, 300);
            }
        }
            
        public List<DailyOvertimeModel> Rows
        {
            get { return null; }
            set
            {
                if(value != null)
                {
                    int cellHeight = (int)Application.Context.Resources.GetDimensionPixelSize (Resource.Dimension.Daily_Overtime_Cell_Height);
                    int items = value.Count;
                    var containerLayoutParams = m_rowsContainer.LayoutParameters;
                    containerLayoutParams.Height = items * cellHeight;
                    m_rowsContainer.LayoutParameters = containerLayoutParams;
                    var controlLayoutParams = m_rowsControl.LayoutParameters;
                    controlLayoutParams.Height = items * cellHeight;
                    m_rowsControl.LayoutParameters = controlLayoutParams;
                }
            }
        }

        public OvertimeTrackerView () : base(Resource.Layout.view_overtime_tracker)
        {
            Title = "Daily Overtime Tracker";
        }

        public override void OnDestroy ()
        {
            base.OnDestroy ();

            if(NavBar != null)
                NavBar.RightButtons = null;

            if (m_refreshButton != null)
            {
                m_refreshButton.Dispose ();
                m_refreshButton = null;
            }
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            m_loading = View.FindViewById(Resource.Id.tabAislesLoading) as FrameLayout;
            m_loadingProgress = View.FindViewById(Resource.Id.progressBarAislesLoading) as ProgressBar;
            m_content = (View)View.FindViewById(Resource.Id.content);

            m_refreshButton = new BarButton(ConsortiumApp.Context);
            m_editButton = new BarButton(ConsortiumApp.Context);

            m_dateLabel = (TextView)view.FindViewById (Resource.Id.dateLabel);
            m_dateLabel.Click += OnSelectDate;
            m_dateLabel.PaintFlags = m_dateLabel.PaintFlags | PaintFlags.UnderlineText;

            //Colleciton controls setup
            //Header
            m_headerCollectionControl = (CollectionControl)view.FindViewById(Resource.Id.header_collectionControl);
            m_headerCollectionControl.RegisterViewWithResourceID<WeekHeaderCell>(Resource.Layout.cell_week_header);
            // Table
            m_rowsControl = (AlternateBackgroundControl)view.FindViewById(Resource.Id.overtimetracker_list);
            m_rowsControl.RegisterViewWithResourceID<DailyOvertimeModelCell> (Resource.Layout.cell_overtimetracker_mapping);
            m_rowsContainer = (RelativeLayout)view.FindViewById(Resource.Id.overtimetracker_list_container);
                
            var set = this.CreateBindingSet<OvertimeTrackerView, OvertimeTrackerViewModel> ();
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For (v => v.CurrentDate).To(vm => vm.WeekStart);
            set.Bind (this).For (v => v.EditMode).To (vm => vm.IsModified);
            set.Bind (this).For (v => v.ScrollToRow).To (vm => vm.SelectedRowIndex);
            set.Bind(m_headerCollectionControl).To (m => m.Headers);
            set.Bind(m_rowsControl).To (m => m.Rows);
            set.Bind(this).For(v => v.Rows).To (m => m.Rows);
            set.Bind(this).For(v => v.LoadingMessage).To(vm => vm.LoadingMessage);
            set.Bind(m_rowsControl).For (v => v.CustomCommand1).To ("OnItemSelected");
            set.Apply ();

            m_refreshButton.SetImageResource(Resource.Drawable.icon_refresh);
            m_refreshButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            m_refreshButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_refreshButton.Click += (sender, args) =>
                {
                    if(VM != null)
                        VM.OnRefresh();
                };

            m_editButton.SetTopBottomMargin(c_topBottomMarginDp);
            m_editButton.Click += (sender, args) =>
                {
                    if(VM != null)
                        VM.OnEdit();
                };
                    
            NavBar.RightButtons = new List<BarButton>() { m_editButton, m_refreshButton };
        }

        void OnSelectDate(object sender, EventArgs e)
        {
            CustomDatePickerDialog dialog = new CustomDatePickerDialog (Context, OnDateSet, CurrentDate.Year, CurrentDate.Month - 1, CurrentDate.Day);
            dialog.Show ();
        }

        void OnDateSet (object sender, DatePickerDialog.DateSetEventArgs e)
        {
            VM.SetDate(e.Date);
        }

        private void UpdateEditButton()
        {
            if (m_editButton != null)
            {
                if(VM.IsModified)
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_confirm);
                }
                else
                {
                    m_editButton.SetImageResource(Resource.Drawable.icon_edit);
                }
                m_editButton.SetImageSize(Resources.GetDimensionPixelSize(Resource.Dimension.NavigationBar_Button_ImageSize));
            }
        }

    }
}

