﻿using System;
using Consortium.Client.Android;
using Android.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Graphics;
using Android.Content.Res;
using Android.App;
using Android.Widget;
using Android.Content;
using Android.Views.InputMethods;
using Android.Text.Method;

namespace WFM.Android
{
    public class ChangePasswordView : ConsortiumPopoverView
    {
        EditText m_currentPassword;
        EditText m_newPassword;
        EditText m_confirmPassword;

        View m_currentView;

        public ChangePasswordView() : base(Resource.Layout.view_change_password)
        {            
            Width = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.ChangePassword_Popup_Width);
            Height = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.ChangePassword_Popup_Height);

            Gravity = GravityFlags.Top;
            Top += Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.ChangePassword_Popup_Top);
            BorderWidth = 3;
            BorderColor = Color.Black;
            CornerRadius = 20;
        }
            
        protected override void OnViewCreated(View view)
        {
            m_currentView = view;

            m_newPassword = (EditText) view.FindViewById(Resource.Id.newPassword);
            m_currentPassword = (EditText) view.FindViewById(Resource.Id.currentPassword);
            m_confirmPassword = (EditText)view.FindViewById(Resource.Id.confirmPassword);

            m_confirmPassword.EditorAction += ConfirmPasswordIMEAction;

            m_newPassword.Typeface = Typeface.Default;
            m_currentPassword.Typeface = Typeface.Default;
            m_confirmPassword.Typeface = Typeface.Default;

            m_newPassword.TransformationMethod = new PasswordTransformationMethod();
            m_currentPassword.TransformationMethod = new PasswordTransformationMethod();
            m_confirmPassword.TransformationMethod = new PasswordTransformationMethod();

            var set = this.CreateBindingSet<ChangePasswordView, ChangePasswordViewModel>();
            set.Apply();
        }

        void ConfirmPasswordIMEAction (object sender, TextView.EditorActionEventArgs e)
        {
            m_confirmPassword.ClearFocus();

            InputMethodManager inputMethodService = (InputMethodManager)m_currentView.Context.GetSystemService(Context.InputMethodService);

            KeyboardUtility.HideKeyboard(m_currentView,inputMethodService);

            ((ChangePasswordViewModel)ViewModel).OnSubmitButton ();
        }
    }
}