using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using WFM.Android;
using WFM.Droid.Controls;
using Android.Views.Animations;
using Android.Animation;
using Java.Lang;

namespace WFM.Android
{
    public class CombinedDepartmentCell : AlternateBackgroundItemView
    {
        private TextView m_nameTextView;

        // Edit Controls
        const float c_scrollThreshold = 20;
        const long c_pressThreshold = 100;
        private bool m_hasMoved;
        private double m_touchStartXPosition;
        private double m_touchStartYPosition;

        private bool m_isSelectedLead;
        public bool IsSelectedLead
        {
            get { return m_isSelectedLead; }
            set { m_isSelectedLead = value; UpdateUI ();}
        }

        private LabourDemandDepartment m_model;
        public LabourDemandDepartment Model
        {
            get { return m_model; }
            set { m_model = value; UpdateUI (); }
        }

        public CombinedDepartmentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_nameTextView = (TextView)FindViewById(Resource.Id.name_textView);

            var set = this.CreateBindingSet<CombinedDepartmentCell, LabourDemandDepartment>();
            set.Bind (this).For (b => b.IsSelectedLead).To (m => m.IsSelected);
            set.Bind (this).For (b => b.Model).To (m => m);
            set.Apply();
        }

        public override void OnShow ()
        {
            base.OnShow ();
            UpdateUI ();
        }

        private void UpdateUI()
        {
            if (m_nameTextView != null)
            {
                if (IsSelectedLead)
                    m_nameTextView.SetBackgroundColor (Color.DeepSkyBlue);
                else
                    m_nameTextView.SetBackgroundColor (Color.Transparent);
                
                if (Model != null && m_nameTextView != null)
                {
                    if (Model.Children != null && Model.Children.Count > 0)
                        m_nameTextView.Text = string.Format ("{0} ({1})", Model.Name, Model.Children.Count);
                    else
                        m_nameTextView.Text = Model.Name;
                }
                else
                    m_nameTextView.Text = string.Empty;
            }
        }

        public override bool DispatchTouchEvent (MotionEvent e)
        {
            // Preview the touch event to detect a swipe: switch (e.ActionMasked) 
            switch (e.Action)
            {
                case MotionEventActions.Down:
                    //Check if point is within till allocation
                    m_hasMoved = false;

                    m_touchStartXPosition = e.GetX();
                    m_touchStartYPosition = e.GetY();

                    return true;//Swallow input otherwise we don't get Move/Up/Cancel events
                case MotionEventActions.Up: 
                case MotionEventActions.Cancel:
                    if (m_hasMoved)
                    {
                        m_hasMoved = false;
                        return true;
                    }
                    else
                    {
                        if (e.EventTime - e.DownTime > c_pressThreshold)
                        {
                            //Handle "selecting" ourselves
                            if (CustomCommand1 != null && CustomCommand1.CanExecute (Model))
                                CustomCommand1.Execute (Model);
                        }
                    }
                    break;
                case MotionEventActions.Move:
                    if (!m_hasMoved && (System.Math.Abs(m_touchStartXPosition - e.GetX()) > c_scrollThreshold || System.Math.Abs(m_touchStartYPosition - e.GetY()) > c_scrollThreshold))
                    {
                        m_hasMoved = true;
                        OnCellBeginDrag(this, m_model, new Point((int)System.Math.Round(e.GetX()), (int)System.Math.Round(e.GetY())));
                    }

                    if (m_hasMoved)
                    {
                        m_touchStartXPosition = e.GetX(); // Update for next time round
                        m_touchStartYPosition = e.GetY(); // Update for next time round

                    }

                    return true;
            }

            return base.DispatchTouchEvent (e);

        }

        public void OnCellBeginDrag()
        {
            OnCellBeginDrag(this, m_model, new Point((int)System.Math.Round(m_touchStartXPosition), (int)System.Math.Round(m_touchStartYPosition)));
        }

        public void OnCellBeginDrag(View v, LabourDemandDepartment dept, Point touchPoint)
        {
            // Ensure touchpoint is not negative
            if(touchPoint.X <= 0) { touchPoint.X = 0; }
            if(touchPoint.Y <= 0) { touchPoint.Y = 0; }

            // Setup data to pass throw with drag event
            ClipData.Item deptId = new ClipData.Item(dept.ID);
            ClipData.Item deptName = new ClipData.Item(dept.Name);
            ClipData.Item deptIsLead = new ClipData.Item (dept.IsLead.ToString ());
            ClipData.Item deptLeadID = new ClipData.Item (dept.LeadID);
            ClipData.Item deptWidth = new ClipData.Item (MeasuredWidth.ToString ());
            ClipData.Item deptHeight = new ClipData.Item (MeasuredHeight.ToString ());
            ClipData.Item deptTouchX = new ClipData.Item (touchPoint.X.ToString ());
            ClipData.Item deptTouchY = new ClipData.Item (touchPoint.Y.ToString ());

            string[] clipDescriptor = {ClipDescription.MimetypeTextPlain, ClipDescription.MimetypeTextPlain};

            ClipData dragData = new ClipData(new ClipDescription("Plain", clipDescriptor), deptId);
            dragData.AddItem(deptName);
            dragData.AddItem(deptIsLead);
            dragData.AddItem(deptLeadID);
            dragData.AddItem(deptWidth);
            dragData.AddItem(deptHeight);
            dragData.AddItem(deptTouchX);
            dragData.AddItem(deptTouchY);

            CombineDeptDragShadow dragShadow = new CombineDeptDragShadow(v, touchPoint);

            // Starts the drag
            v.StartDrag(dragData,
                dragShadow,
                dragData, 
                0) ;
        }
    }

    internal class CombineDeptDragShadow : View.DragShadowBuilder
    {
        private Point m_touchPoint;

        public CombineDeptDragShadow(View v, Point touchPoint) : base(v)
        {
            m_touchPoint = touchPoint;
        }

        public override void OnProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
        {
            shadowSize.Set (View.Width, View.Height);
            shadowTouchPoint.Set (m_touchPoint.X, m_touchPoint.Y);
        }

        public override void OnDrawShadow (Canvas canvas)
        {
//            base.OnDrawShadow (canvas);//Hide the shadow
        }
    }
}