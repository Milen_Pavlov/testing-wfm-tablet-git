﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;

namespace WFM.Android
{
    public class SchedulePlanningWeekContentCell : CollectionItemView
    {
        private TextView m_nameTextView;
        private List<PlanningControl> m_dayControls = new List<PlanningControl>();

        public string Name
        {
            get { return null; }
            set
            {
                m_nameTextView.Text = value;
            }
        }

        public double[] Variance
        {
            get { return null; }
            set
            {
                UpdatePlanningControls(value);
            }
        }

        public SchedulePlanningWeekContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {

        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_nameTextView = (TextView)FindViewById(Resource.Id.name_textView);
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day1_planningControl));
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day2_planningControl));
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day3_planningControl));
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day4_planningControl));
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day5_planningControl));
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day6_planningControl));
            m_dayControls.Add((PlanningControl)FindViewById(Resource.Id.day7_planningControl));

            foreach (PlanningControl control in m_dayControls)
            {
                control.ShowGrid = false;
                control.PadToWeekMax = true;
            }

            var set = this.CreateBindingSet<SchedulePlanningWeekContentCell, PlanningData>();
            set.Bind(this).For(b => b.Name).To(m => m.Name);
            set.Bind(this).For(b => b.Variance).To(m => m.Variance);
            set.Apply();
        }

        private void UpdatePlanningControls(double[] data)
        {
            var start = 0;
            foreach (PlanningControl control in m_dayControls)
            {
                control.SetContentData(data, start, ScheduleTime.DayIntervals);
                start += ScheduleTime.DayIntervals;
            }
        }
    }
}

