using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using WFM.Android;
using WFM.Droid.Controls;
using Android.Views.Animations;
using Android.Animation;

namespace WFM.Android
{
    public class MyHoursRowContentCell : AlternateBackgroundItemView
    {
        private TextView m_nameTextView;
        private CollectionControl m_rowDataCollection;

        public string Name
        {
            get { return null; }
            set
            {
                m_nameTextView.Text = value;
            }
        }

        public bool CanClickTitle
        {
            get
            {
                return true;
            }
            set
            {
                if(value)
                    m_nameTextView.PaintFlags = m_nameTextView.PaintFlags | PaintFlags.UnderlineText;
                else
                    m_nameTextView.PaintFlags = m_nameTextView.PaintFlags &~ PaintFlags.UnderlineText;
            }
        }

        public List<MyHoursCellData> Columns
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    if (m_rowDataCollection.NumColumns != value.Count)
                        m_rowDataCollection.NumColumns = value.Count;
                }

                m_rowDataCollection.Items = value;
            }
        }

        public override View BackgroundView
        {
            get
            {
                return m_rowDataCollection;
            }
        }

        public MyHoursRowContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_nameTextView = (TextView)FindViewById(Resource.Id.name_textView);

            m_rowDataCollection = (CollectionControl)FindViewById(Resource.Id.row_collectionControl);
            m_rowDataCollection.RegisterViewWithResourceID<MyHoursRowColumnEntryContentCell> (Resource.Layout.cell_myhours_row_column_entry);

            if (m_rowDataCollection != null && m_rowDataCollection.SelectedCommand == null && CustomCommand1!=null)
                m_rowDataCollection.SelectedCommand = CustomCommand1;

            if (m_rowDataCollection != null && CustomCommand3!=null)
                m_rowDataCollection.CustomCommand1 = CustomCommand3;
            
            var set = this.CreateBindingSet<MyHoursRowContentCell, MyHoursRowData>();
            set.Bind (this).For (b => b.Name).To (m => m.Title);
            set.Bind (this).For (b => b.Columns).To (m => m.Columns);
            set.Bind (this).For(b => b.CanClickTitle).To(m => m.CanClickTitle);
            set.Apply();
        }

        protected override void OnAttachedToWindow ()
        {
            base.OnAttachedToWindow ();

            if (m_rowDataCollection != null && m_rowDataCollection.SelectedCommand!=null && CustomCommand1!=null)
                m_rowDataCollection.SelectedCommand = CustomCommand1;

            if (m_nameTextView != null)
                m_nameTextView.Click += OnNameClicked;
        }

        protected override void OnDetachedFromWindow ()
        {
            base.OnDetachedFromWindow ();

            if (m_rowDataCollection != null)
                m_rowDataCollection.SelectedCommand = null;

            if (m_nameTextView != null)
                m_nameTextView.Click -= OnNameClicked;
        }

        public void OnNameClicked(object sender, EventArgs e)
        {            
            if (CustomCommand2 != null)
                CustomCommand2.Execute (DataContext);
        }
    }
}