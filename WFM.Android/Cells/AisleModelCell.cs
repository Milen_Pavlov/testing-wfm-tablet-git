using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using WFM.Android;
using WFM.Droid.Controls;
using Android.Views.Animations;
using Android.Animation;
using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;

namespace WFM.Android
{
    public class AisleModelCell : AlternateBackgroundItemView
    {
        private TextView m_nameTextView;

        public string Name
        {
            get { return null; }
            set
            {
                m_nameTextView.Text = value;
            }
        }
#region DAYS
        private String[] m_day1;
        public String[] Day1
        {
            get { return m_day1; }
            set
            { 
                m_day1 = value;
                UpdateCell (value, 0);
            }
        }

        private String[] m_day2;
        public String[] Day2
        {
            get { return m_day2; }
            set
            {
                m_day2 = value;
                UpdateCell (value, 1);
            }
        }

        private String[] m_day3;
        public String[] Day3
        {
            get { return m_day3; }
            set
            {
                m_day3 = value;
                UpdateCell (value, 2);
            }
        }

        private String[] m_day4;
        public String[] Day4
        {
            get { return m_day4; }
            set
            {
                m_day4 = value;
                UpdateCell (value, 3);
            }
        }

        private String[] m_day5;
        public String[] Day5
        {
            get { return m_day5; }
            set
            {
                m_day5 = value;
                UpdateCell (value, 4);
            }
        }

        private String[] m_day6;
        public String[] Day6
        {
            get { return m_day6; }
            set
            {
                m_day6 = value;
                UpdateCell (value, 5);
            }
        }

        private String[] m_day7;
        public String[] Day7
        {
            get { return m_day7; }
            set
            {
                m_day7 = value;
                UpdateCell (value, 6);
            }
        }
#endregion
        private static Color m_evenAisle = new Color (240, 240, 255, 255);
        public override Color EvenCol { get { return m_evenAisle; } }

        private static Color m_oddAisle = new Color (224, 224, 255, 255);
        public override Color OddCol { get { return m_oddAisle; } }

        private List<LinearLayout> m_holders;


        public AisleModelCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_nameTextView = (TextView)FindViewById(Resource.Id.name_textView);

            m_holders = new List<LinearLayout> ();

            var holder_1 = FindViewById<LinearLayout> (Resource.Id.day_1);
            holder_1.Click += Holder_1_Click;
            m_holders.Add (holder_1);

            var holder_2 = FindViewById<LinearLayout> (Resource.Id.day_2);
            holder_2.Click += Holder_2_Click;
            m_holders.Add (holder_2);

            var holder_3 = FindViewById<LinearLayout> (Resource.Id.day_3);
            holder_3.Click += Holder_3_Click;
            m_holders.Add (holder_3);

            var holder_4 = FindViewById<LinearLayout> (Resource.Id.day_4);
            holder_4.Click += Holder_4_Click;
            m_holders.Add (holder_4);

            var holder_5 = FindViewById<LinearLayout> (Resource.Id.day_5);
            holder_5.Click += Holder_5_Click;
            m_holders.Add (holder_5);

            var holder_6 = FindViewById<LinearLayout> (Resource.Id.day_6);
            holder_6.Click += Holder_6_Click;
            m_holders.Add (holder_6);

            var holder_7 = FindViewById<LinearLayout> (Resource.Id.day_7);
            holder_7.Click += Holder_7_Click;
            m_holders.Add (holder_7);
            
            var set = this.CreateBindingSet<AisleModelCell, NightAisleModel>();
            set.Bind (this).For (b => b.Name).To (m => m.AisleName);
            set.Bind (this).For (b => b.Day1).To (m => m.Days[0].Employees);
            set.Bind (this).For (b => b.Day2).To (m => m.Days[1].Employees);
            set.Bind (this).For (b => b.Day3).To (m => m.Days[2].Employees);
            set.Bind (this).For (b => b.Day4).To (m => m.Days[3].Employees);
            set.Bind (this).For (b => b.Day5).To (m => m.Days[4].Employees);
            set.Bind (this).For (b => b.Day6).To (m => m.Days[5].Employees);
            set.Bind (this).For (b => b.Day7).To (m => m.Days[6].Employees);
            set.Apply();
        }

        void Holder_1_Click (object sender, EventArgs e)
        {
            ShowPop (0);
        }

        void Holder_2_Click (object sender, EventArgs e)
        {
            ShowPop (1);
        }

        void Holder_3_Click (object sender, EventArgs e)
        {
            ShowPop (2);
        }

        void Holder_4_Click (object sender, EventArgs e)
        {
            ShowPop (3);
        }

        void Holder_5_Click (object sender, EventArgs e)
        {
            ShowPop (4);
        }

        void Holder_6_Click (object sender, EventArgs e)
        {
            ShowPop (5);
        }

        void Holder_7_Click (object sender, EventArgs e)
        {
            ShowPop (6);
        }

        void ShowPop (int id)
        {
            ConsortiumPopoverView.SourceView = this;
            if (CustomCommand1 != null)
            {
                var context = DataContext as NightAisleModel;
                if(CustomCommand1.CanExecute(context.Days [id] as NightAisleDayModel))
                    CustomCommand1.Execute (context.Days [id] as NightAisleDayModel);
            }
        }
            
        void UpdateCell(String[] names, int holderId)
        {
            if (m_holders == null || m_holders.Count <= holderId || m_holders[holderId] == null)
                return;
            
            m_holders[holderId].RemoveAllViews ();

            if (names != null)
            {
                foreach (String employee in names)
                {
                    Console.Write (employee);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams (LayoutParams.WrapContent, DimensionUtility.TranslateToDP(50));
                    layoutParams.Gravity = GravityFlags.Center;

                    TextView textView = new TextView (Context);
                    textView.SetText (employee, TextView.BufferType.Normal);
                    textView.SetTextSize (global::Android.Util.ComplexUnitType.Dip, 14);
                    textView.SetTextColor (Color.Black);
                    textView.SetBackgroundColor (Color.Transparent);
                    textView.Gravity = GravityFlags.Center;
                    textView.LayoutParameters = layoutParams;

                    LinearLayout.LayoutParams separatorParams = new LinearLayout.LayoutParams (LayoutParams.WrapContent, DimensionUtility.TranslateToDP(1));
                    FrameLayout separator = new FrameLayout (Context);
                    separator.SetBackgroundColor (Color.DarkGray);
                    separator.LayoutParameters = separatorParams;
                    
                    m_holders[holderId].AddView (textView);
                    m_holders[holderId].AddView (separator);
                    m_holders[holderId].PostInvalidate ();
                }
            }
        }
    }
}