﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class EmployeeHistoryCell : CollectionItemView
    {
        private TextView m_dateLabel;
        private readonly IStringService m_localiser;

        public EmployeeHistoryCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            m_dateLabel = FindViewById<TextView>(Resource.Id.employeehistory_dates);
        }

        public override void OnShow ()
        {
            base.OnShow ();

            TimeOffHistory data = DataContext as TimeOffHistory;
            if (data != null)
            {
                
                TimeSpan span = data.To - data.From;
                if (span.TotalDays <= 1)
                    m_dateLabel.Text = data.From.ToString (m_localiser.Get("dd/MM/yyyy"));
                else
                    m_dateLabel.Text = string.Format ("{0} - {1}", data.From.ToString (m_localiser.Get("dd/MM/yyyy")), data.To.ToString (m_localiser.Get("dd/MM/yyyy")));
            }
        }
    }
}

