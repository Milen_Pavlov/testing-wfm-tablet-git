using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views;
using System.Collections.Generic;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
	public class ScheduleGridDayContentCell : CollectionItemView
	{
		private DayCellView m_dayCellView;
        private TextView m_nameLabel;

        private EmployeeData m_EmployeeData;

        public EmployeeData EmployeeData
        {
            get
            {
                return m_EmployeeData;
            }
            set
            {
                m_EmployeeData = value;
                if(m_EmployeeData != null)
                {
                    if(m_warningButton != null)
                    {
                        m_warningButton.Visibility = m_EmployeeData.WarningCount > 0 ? ViewStates.Visible : ViewStates.Invisible;
                        m_warningButton.Text = m_EmployeeData.WarningCount.ToString();
                    }
                    if(m_hoursLabel != null)
                    {
                        m_hoursLabel.Text = string.Format("{0:0.##}", m_EmployeeData.Hours);
                    }
                    if(m_nameLabel != null)
                    {
                        m_nameLabel.Text = value == null ? "" : string.Format("{0} {1}", m_EmployeeData.Name, m_EmployeeData.GetAttributes());
                    }

                    SetupSpareShift ();
                }
            }
        }

        private Button m_warningButton;
        private TextView m_hoursLabel;

		public ScheduleGridDayContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
			: base(context, layoutInflater, dataContext)
		{

		}
            
		protected override void OnFinishInflate()
		{
			base.OnFinishInflate();

            m_dayCellView = (DayCellView)FindViewById(Resource.Id.dayCellView);
            m_nameLabel = (TextView)FindViewById(Resource.Id.name_textView);

            TextView nameTextView = (TextView)FindViewById(Resource.Id.name_textView);
            m_warningButton = (Button)FindViewById(Resource.Id.warningButton);
            m_hoursLabel = (TextView)FindViewById(Resource.Id.hours_textView);

            nameTextView.Click += (object sender, EventArgs e) => {
                if (CustomCommand1 != null && EmployeeData != null && EmployeeData.Info != null)
                {
                    ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.name_popupPosition);
                    CustomCommand1.Execute(EmployeeData.Info);
                }
            };

            m_dayCellView.OnShiftSelected += (ShiftData shift) => {
                if (CustomCommand2 != null)
                {
                    ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.name_popupPosition);
                    CustomCommand2.Execute(shift);
                }
            };

            m_dayCellView.OnTimeOffSelected += ( TimeOffData timeOffData ) => {
                if (CustomCommand4 != null)
                {
                    ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.name_popupPosition);
                    CustomCommand4.Execute( timeOffData );
                } 
            };
                
            // Popover instantiation requires employee data to be set
            m_warningButton.Click += delegate { 
                if (CustomCommand3 != null)
                {
                    ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.warningButton);
                    CustomCommand3.Execute(EmployeeData.GetAllWarnings());
                }
            };

            var set = this.CreateBindingSet<ScheduleGridDayContentCell, EmployeeData>();
            set.Bind(m_dayCellView).For(b => b.EmployeeData).To(m => m);
            set.Bind(this).For(v => v.EmployeeData).To(m => m);
            set.Apply();
		}

        public override void OnShow()
        {
            base.OnShow();
            EmployeeData = EmployeeData;
            m_dayCellView.RefreshShiftViews();
        }

        void SetupSpareShift ()
        {
            if(m_nameLabel != null)
            {
                if (m_EmployeeData.SpareShift)
                {
                    m_nameLabel.Text = Mvx.Resolve<IStringService>().Get ("spare_shifts_label");
                    m_hoursLabel.Text = String.Empty;
                }
            }       
        }
	}
}