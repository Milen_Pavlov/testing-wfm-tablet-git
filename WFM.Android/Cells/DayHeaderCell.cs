﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views;

namespace WFM.Android
{
    public class DayHeaderCell : CollectionItemView
    {
        private ImageView m_sortImageView;

        public SortState HeaderSort
        {
            get { return null; }
            set 
            { 
                if (m_sortImageView != null && value != null)
                {
                    m_sortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                    m_sortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
                }
            }
        }

        public int StartHour 
        {
            get { return 0; }
            set {}
        }

        public DayHeaderCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_sortImageView = (ImageView)this.FindViewById(Resource.Id.sort_imageView);

            var set = this.CreateBindingSet<DayHeaderCell, ColumnHeader>();

            set.Bind (this).For(b => b.HeaderSort).To (m => m.Sort);

            set.Apply();
        }
    }
}
