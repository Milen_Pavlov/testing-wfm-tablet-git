﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class PayAllocationSectionCell : CollectionItemView
    {
        private TextView m_label;
        private readonly IStringService m_localiser;

        public PayAllocationSectionCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            m_label = FindViewById<TextView>(Resource.Id.payallocation_section_title);
        }

        public override void OnShow ()
        {
            base.OnShow ();

            PayAllocationSection data = DataContext as PayAllocationSection;
            if (data != null)
            {
                m_label.Text = string.Format("Week of {0}", data.StartOfWeek.ToString (m_localiser.Get("dd/MM/yyyy")));
            }
        }
    }
}

