﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views;
using Android.OS;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Linq;

namespace WFM.Android
{
    public class AvailableEmployeeContentCell : CollectionItemView
    {
        private ImageButton m_assignButton;

        public AvailableEmployeeContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_assignButton = (ImageButton)this.FindViewById(Resource.Id.assignEmployeeButton);

            m_assignButton.Click += HandleClick;
        }

        void HandleClick (object sender, EventArgs e)
        {
            if(CustomCommand1 != null)
                CustomCommand1.Execute(DataContext);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_assignButton.Click -= HandleClick;
            }

            base.Dispose(disposing);
        }
    }
}