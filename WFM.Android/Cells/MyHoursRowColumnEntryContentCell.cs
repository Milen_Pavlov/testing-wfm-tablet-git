using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Android;
using WFM.Core;
using Android.Graphics;
using Android.Animation;
using System.Threading.Tasks;

namespace WFM.Android
{
    public class MyHoursRowColumnEntryContentCell : CollectionItemView
    {
        private TextView m_dataText;
        private ImageView m_editImage;
        private ValueAnimator m_animation;

        private bool m_isProcted;
        public bool IsProtected
        {
            get { return m_isProcted; }
            set
            {
                m_isProcted = value;
                UpdateBackgroundColour ();
            }
        }

        private ChangedByType m_changedBy;
        public ChangedByType ChangedBy
        {
            get { return m_changedBy; }
            set
            {
                m_changedBy = value;
                UpdateBackgroundColour ();
            }
        }

        private bool m_didUndo;
        public bool DidUndo
        {
            get { return m_didUndo; }
            set
            {
                m_didUndo = value;

                if (DidUndo)
                    AnimateUndo ();
            }
        }

        public MyHoursRowColumnEntryContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
            
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate ();

            m_dataText = (TextView)FindViewById (Resource.Id.data_textView);
            m_editImage = (ImageView)FindViewById (Resource.Id.editIcon);

            //Highlight image
            m_editImage.SetColorFilter (Color.DarkGray);

            var set = this.CreateBindingSet<MyHoursRowColumnEntryContentCell, MyHoursCellData> ();
            set.Bind (m_dataText).For (v => v.Text).To (vm => vm.Text);
            set.Bind (this).For (v => v.IsProtected).To (vm => vm.IsLocked);
            set.Bind (this).For (v => v.DidUndo).To (vm => vm.DidUndo);
            set.Bind (this).For (v => v.ChangedBy).To (vm => vm.ChangedBy);
            set.Apply ();
        }

        protected override void OnDetachedFromWindow ()
        {
            base.OnDetachedFromWindow ();

            if (m_animation != null)
            {
                m_animation.Cancel ();
                m_animation.Dispose ();
                m_animation = null;
            }
        }

        protected override void Dispose (bool disposing)
        {
            if (disposing)
            {
                if (m_animation != null)
                {
                    m_animation.Cancel ();
                    m_animation.Dispose ();
                    m_animation = null;
                }
            }

            base.Dispose (disposing);
        }

        public void AnimateUndo()
        {
            uint WHITE = (uint)Color.Transparent.ToArgb();
            uint BLUE = (uint)Color.LightBlue.ToArgb();


            if (m_animation == null)
            {
                m_animation = ObjectAnimator.OfInt(m_dataText, "backgroundColor", (int)WHITE, (int)BLUE, (int)WHITE);
                m_animation.SetDuration (500);
                m_animation.SetEvaluator (new ArgbEvaluator ());
                m_animation.AnimationEnd += (object sender, EventArgs e) => 
                    {
                        Invalidate();
                        
                        if(m_dataText!=null)
                            m_dataText.SetBackgroundColor (GetColour());
                        
                        if (CustomCommand1 != null && DataContext != null && CustomCommand1.CanExecute(DataContext))
                            CustomCommand1.Execute (DataContext);
                    };
            }

            m_animation.Start ();
        }

        void UpdateBackgroundColour()
        {
            if(m_dataText!=null)
                m_dataText.SetBackgroundColor (GetColour());
        }

        Color GetColour()
        {
            Color result = Color.Transparent;

            if (IsProtected)
            {
                result = Color.Blue;
                result.A = 75;
            }

            switch (m_changedBy)
            {
                case ChangedByType.Store:
                    result = Color.Blue;
                    break;
                case ChangedByType.Central:
                    result = Color.Red;
                    break;
                case ChangedByType.None:
                default:
                    return result;
            }

            result.A = 75;
            return result; 
        }
    }
}