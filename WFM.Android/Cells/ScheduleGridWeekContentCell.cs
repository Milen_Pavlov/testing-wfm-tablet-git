using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Android.Views;
using WFM.Droid.Controls;

namespace WFM.Android
{
    //TODO: Refactor this. Its super naff.
    public class ScheduleGridWeekContentCell : ScheduleGridWeekBaseCell
    {
        private TextView m_nameTextView;
        private TextView m_hoursTextView;
        private Button m_warningButton;

        public JDAEmployeeInfo Info 
        { 
            get; 
            set;
        }

        public string Name
        {
            get { return null; }
            set
            {
                m_nameTextView.Text = value;
            }
        }

        public double Hours
        {
            get { return 0; }
            set
            {
                m_hoursTextView.Text = string.Format("{0:F2}", value);
            }
        }

        public int WarningCount
        {
            get{ return 0; }
            set
            {
                if (value <= 0)
                {
                    m_warningButton.Visibility = ViewStates.Invisible;
                }
                else
                {
                    m_warningButton.Text = value.ToString ();
                    m_warningButton.Visibility = ViewStates.Visible;
                }
            }
        }

        public EmployeeData AdvancedBinding
        {
            get
            {
                return null;
            }

            set
            {
                this.Name = value == null ? "" : string.Format("{0} {1}", value.Name, value.GetAttributes());

                if (m_draggedShift != null)
                    UpdateCanAcceptShift (null, DragAction.Started);
                else
                    HideFades ();
            }
        }

        public override List<EmployeeDayData> Days
        {
            get { return null; }
            set
            {
                foreach (WeeklyJobsControl control in m_dayControls)
                {
                    control.Visibility = ViewStates.Invisible;
                }

                for (int i = 0; i < 7; i++)
                {
                    foreach (var control in m_dynamicControls[i])
                    {
                        m_jobContainerLayouts[i].RemoveView(control);
                    }

                    m_dynamicControls[i].Clear();
                }

                if (value != null)
                {
                    int maxRows = 0;

                    // Add all jobs
                    for (int n = 0; n < 7; n++)
                    {
                        int rows = 0;

                        EmployeeDayData day = value[n];

                        DateTime dayStart = day.DayStart;

                        // Check if time off
                        foreach(var unavailabilityRange in day.UnavailabilityRanges)
                        {
                            if(unavailabilityRange.Type == "t") // t is for time off
                            { 
                                DateTime dayEnd = dayStart.AddDays(1);

                                if(unavailabilityRange.Start < dayEnd && dayStart < unavailabilityRange.End)
                                {
                                    CreateTimeOffControl(n, dayStart);
                                    rows++;
                                }
                            }
                        }

                        if(m_displayPreviousDayShifts)
                        {
                            if(n == 0)  // If first day of week, add any previous week shifts that run over from the previous week
                            {
                                foreach (ShiftData shift in m_employeeData.LastDayPreviousWeek.Shifts)
                                {
                                    foreach (JobData job in shift.Jobs)
                                    {
                                        AddJobControlForDay(n, job, shift);
                                        rows++;
                                    }
                                }
                            } // Else add any previous day shifts that run over
                            else
                            {
                                EmployeeDayData previousDay = value[n-1];
                                foreach (ShiftData shift in previousDay.Shifts)
                                {
                                    foreach (JobData job in shift.Jobs)
                                    {
                                        if(job.End >= value[n].DayStart)
                                        {
                                            AddJobControlForDay(n, job, shift);
                                            rows++;
                                        }
                                    }
                                }
                            }
                        }

                        foreach (ShiftData shift in day.Shifts)
                        {
                            foreach (JobData job in shift.Jobs)
                            {
                                AddJobControlForDay(n, job, shift);
                                rows++;
                            }
                        }

                        if (rows > maxRows)
                            maxRows = rows;
                    }

                    int height = m_rowHeight;

                    if (maxRows > 0)
                        height *= maxRows;

                    m_jobContainerLayouts.ForEach(layout  => layout.SetMinimumHeight(height));
                }
            }
        }

        private bool m_hasDragFixBeenRun = false;

        public ScheduleGridWeekContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
           
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_nameTextView = (TextView)FindViewById(Resource.Id.name_textView);
            m_hoursTextView = (TextView)FindViewById(Resource.Id.hours_textView);
            m_warningButton = (Button)FindViewById(Resource.Id.warningButton);

            // Tapping name view triggers popup at set location
            m_nameTextView.Click += (object sender, EventArgs e) => {
                if (CustomCommand1 != null)
                {
                    // Popup at this view location
                    ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.name_popupPosition);
                    CustomCommand1.Execute(Info);
                }
            };

            // Popover instantiation requires employee data to be set
            m_warningButton.Click += delegate { 
                ConsortiumPopoverView.SourceView = m_warningButton;
                if (CustomCommand3 != null)
                    CustomCommand3.Execute(m_employeeData.GetAllWarnings());
            };

            var set = this.CreateBindingSet<ScheduleGridWeekContentCell, EmployeeData>();
            set.Bind(this).For(b => b.Info).To(m => m.Info);
            set.Bind(this).For(b => b.Hours).To(m => m.Hours);
            set.Bind(this).For(b => b.Days).To(m => m.Days);
            set.Bind(this).For(b => b.AdvancedBinding).To(m => m);
            set.Bind(this).For(b => b.EmployeeData).To(m => m);
            set.Bind(this).For(b => b.WarningCount).To(m => m.WarningCount);
            foreach (var weekDay in m_jobContainerLayouts)
            {
                set.Bind (weekDay).For (b => b.EmployeeData).To (m => m);
            }
            set.Apply();
        }

        public override void OnTimeOffSelect(TimeOffData timeOff)
        {
            if (CustomCommand4 != null && timeOff != null)
            {
                ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.name_popupPosition);
                CustomCommand4.Execute( timeOff );
            }
        }

        private void CreateTimeOffControl(int dayIndex, DateTime startTime)
        {
            JobData timeOffJob =  JobData.Create();
            timeOffJob.JobName = "Time Off";
            timeOffJob.JobColor = "#00ffffff";
            timeOffJob.Edit = EditState.None;
            timeOffJob.Start = startTime;
            timeOffJob.IsTimeOff = true;

            AddJobControlForDay(dayIndex, timeOffJob, null);
        }

        protected override void OnAttachedToWindow ()
        {
            base.OnAttachedToWindow ();

            PostDelayed (() =>
                {
                    Utilities.Helpers.dragEventFix (this);
                }, 100);
        }

        public override void OnShow ()
        {
            base.OnShow ();

            Post (() =>
                {
                    Utilities.Helpers.dragEventFix(this);
                });
        }
    }
}