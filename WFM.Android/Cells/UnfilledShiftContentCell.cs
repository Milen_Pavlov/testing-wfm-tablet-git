﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views;
using Android.OS;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace WFM.Android
{
    public class UnfilledShiftContentCell : CollectionItemView
    {
        private JDAShift m_unfilledShift;
        private TextView m_hours;
        private ImageButton m_deleteButton;

        public UnfilledShiftContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_unfilledShift = DataContext as JDAShift;

            m_deleteButton = (ImageButton)FindViewById(Resource.Id.deleteButton);
            m_deleteButton.Click += OnDelete;

            this.Click += OnClick;           
        }

        public override void OnShow()
        {
            m_unfilledShift = DataContext as JDAShift;
        }

        void OnClick(object sender, EventArgs e)
        {
            if (CustomCommand2 != null)
            {
                CustomCommand2.Execute(DataContext); // This will be OnUnfilledShiftSelected() in UnfilledShiftsViewModel
            }
        }

        void OnDelete(object sender, EventArgs e)
        {
            if (CustomCommand1 != null)
            {
                CustomCommand1.Execute(DataContext); // This will be OnDelete() in UnfilledShiftsViewModel
            }
        }
    }
}