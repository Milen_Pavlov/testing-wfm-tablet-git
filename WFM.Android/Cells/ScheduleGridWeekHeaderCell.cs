﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views;

namespace WFM.Android
{
    public class ScheduleGridWeekHeaderCell : CollectionItemView
    {
        private ImageView m_sortImageView;
        private TextView m_titleTextView;
        private TextView m_hoursTextView;
        private TextView m_demandTextView;

        public SortState HeaderSort
        {
            get { return null; }
            set 
            { 
                if (m_sortImageView != null && value != null)
                {
                    m_sortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                    m_sortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
                }
            }
        }

        public ScheduleGridWeekHeaderCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {

        }

		public override void Inflate(int templateId )
		{
			templateId = Resource.Layout.cell_week_header; // hard-coded explicit resource
			TemplateId = templateId;
			AndroidBindingContext.BindingInflate(templateId, this);

			OnFinishInflate();
		}

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_sortImageView = (ImageView)this.FindViewById(Resource.Id.sort_imageView);
            m_titleTextView = (TextView)this.FindViewById(Resource.Id.title_textView);
            m_hoursTextView = (TextView)this.FindViewById(Resource.Id.hours_textView);
            m_demandTextView = (TextView)this.FindViewById(Resource.Id.demand_textView);

            var set = this.CreateBindingSet<ScheduleGridWeekHeaderCell, ColumnHeader>();
            set.Bind (this).For(b => b.HeaderSort).To (m => m.Sort);
            set.Bind (m_titleTextView).To(m => m.Title);
            set.Bind (m_hoursTextView).To(m => m.Value1);
            set.Bind (m_demandTextView).To(m => m.Value2);
            set.Apply();
        }
    }
}