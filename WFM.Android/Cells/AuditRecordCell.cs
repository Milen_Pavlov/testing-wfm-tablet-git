﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Consortium.Client.Android;
using WFM.Core;
using WFM.Android.Views.TillAllocations;
using Android.Graphics;
using Cirrious.CrossCore;
using WFM.Shared.DataTransferObjects.Audits;

namespace WFM.Android
{
    public class AuditRecordCell : CollectionItemView
    {
        public AuditRecordCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            var set = this.CreateBindingSet<AuditRecordCell, AuditRecord>();
            set.Apply();
        }   
    }
}