﻿using System;
using Consortium.Client.Android;
using System.Collections.Generic;
using WFM.Core;
using Android.Views;
using WFM.Droid.Controls;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Linq;
using WFM.Shared.Extensions;
using Cirrious.CrossCore;
using Java.Lang;
using Android.Graphics;
using Cirrious.MvvmCross.Plugins.Messenger;
using Android.Widget;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class AddJobControl
    {
        public int DayIndex { get; set; }
        public TextView JobControl { get; set; }
        public Space JobSpace { get; set; }

        public AddJobControl( int day, TextView jobView, Space jobSpace)
        {
            DayIndex = day;
            JobControl = jobView;
            JobSpace = jobSpace;
        }
    }

    public class ScheduleGridWeekFooterCell : ScheduleGridWeekBaseCell
    {
        private ScheduleService m_scheduleService;
        private SpareShiftService m_spareShiftService;

        private List<AddJobControl> m_addJobControl;

        private List<EmployeeData> m_employees;
        public List<EmployeeData> Employees
        {
            get { return m_employees; }
            set
            {
                m_employees = value;
                List<EmployeeData> employees = m_employees.Where(x => x != null && x.SpareShift == true).ToList();
                EmployeeData dummyEmployee = CreateDummyEmployeeData ();

                foreach (var employee in employees)
                {
                    for(int day = 0; day < employee.Days.Count; ++day)
                    {
                        if (employee.Days[day].Shifts == null)
                            continue;
                        
                        foreach (var shift in employee.Days[day].Shifts)
                        {
                            dummyEmployee.Shifts._NewIfNull();
                            dummyEmployee.Shifts.Add (shift);
                        }
                    }
                }

                EmployeeData = dummyEmployee;

                foreach (var weekDay in m_jobContainerLayouts)
                {
                    weekDay.EmployeeData = EmployeeData;
                }
            }
        }

        protected override int OnAllJobsAdded()
        {
            if (m_employeeData == null)
            {
                return 0; // Rows to add
            }
                
            int maxShifts = 1;
            foreach (var day in m_employeeData.Days)
            {
                if (day.Shifts.Count > maxShifts)
                    maxShifts = day.Shifts.Count;
            }


            for (int day = 0; day < m_employeeData.Days.Count; ++day)
            {
                if (m_addJobControl != null && m_addJobControl.Count > day)
                {
                    m_jobContainerLayouts[day].RemoveView (m_addJobControl[day].JobSpace);
                    m_jobContainerLayouts[day].RemoveView (m_addJobControl[day].JobControl);

                    int shiftsInDay = m_employeeData.Days[day].Shifts.Count;
                    if (shiftsInDay == 0)
                        shiftsInDay = 1;
                    
                    int newHeight = (maxShifts - shiftsInDay) * m_rowHeight;
                    m_addJobControl[day].JobSpace.SetMinimumHeight (newHeight);

                    m_jobContainerLayouts[day].AddView (m_addJobControl[day].JobSpace);
                    m_jobContainerLayouts[day].AddView (m_addJobControl[day].JobControl);
                }
            }

            return 1; // Rows to add
        }


            
        public ScheduleGridWeekFooterCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
            Init ();
        }

        private void Init()
        {
            m_scheduleService = Mvx.Resolve<ScheduleService> ();
            m_spareShiftService = Mvx.Resolve<SpareShiftService> ();
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_addJobControl = new List<AddJobControl> ();
            m_addJobControl.Add (new AddJobControl(0, (TextView)this.FindViewById (Resource.Id.day1_job_add), (Space)this.FindViewById(Resource.Id.day1_job_space)));
            m_addJobControl.Add (new AddJobControl(1, (TextView)this.FindViewById (Resource.Id.day2_job_add), (Space)this.FindViewById(Resource.Id.day2_job_space)));
            m_addJobControl.Add (new AddJobControl(2, (TextView)this.FindViewById (Resource.Id.day3_job_add), (Space)this.FindViewById(Resource.Id.day3_job_space)));
            m_addJobControl.Add (new AddJobControl(3, (TextView)this.FindViewById (Resource.Id.day4_job_add), (Space)this.FindViewById(Resource.Id.day4_job_space)));
            m_addJobControl.Add (new AddJobControl(4, (TextView)this.FindViewById (Resource.Id.day5_job_add), (Space)this.FindViewById(Resource.Id.day5_job_space)));
            m_addJobControl.Add (new AddJobControl(5, (TextView)this.FindViewById (Resource.Id.day6_job_add), (Space)this.FindViewById(Resource.Id.day6_job_space)));
            m_addJobControl.Add (new AddJobControl(6, (TextView)this.FindViewById (Resource.Id.day7_job_add), (Space)this.FindViewById(Resource.Id.day7_job_space)));

            foreach (var jobControl in m_addJobControl)
            {
                jobControl.JobControl.SetText("Add Shift", TextView.BufferType.Normal);
                jobControl.JobControl.TextSize = Resources.GetDimension (Resource.Dimension.Allocations_Cell_Content_UnassignedTextSize);
                jobControl.JobControl.SetTextColor (Color.White);
                jobControl.JobControl.SetBackgroundColor (new Color (0, 0, 0, 48));
                jobControl.JobControl.Gravity = GravityFlags.Center;
            }

            m_addJobControl[0].JobControl.Click += AddJobDay1;
            m_addJobControl[1].JobControl.Click += AddJobDay2;
            m_addJobControl[2].JobControl.Click += AddJobDay3;
            m_addJobControl[3].JobControl.Click += AddJobDay4;
            m_addJobControl[4].JobControl.Click += AddJobDay5;
            m_addJobControl[5].JobControl.Click += AddJobDay6;
            m_addJobControl[6].JobControl.Click += AddJobDay7;

            var set = this.CreateBindingSet<ScheduleGridWeekFooterCell, ScheduleGridViewModel>();
            set.Bind (this).For (b => b.Employees).To (vm => vm.Employees);
            set.Bind (this).For (b => b.Visibility).To (vm => vm.ShowSpareShifts).WithConversion("Visibility");
            set.Apply();
        }

        void AddJob(int day)
        {
            int departmentId = m_scheduleService.CurrentFilter.Items.Where (p => p.Selected == true).First().ID;
            EmployeeData newEmployee = m_spareShiftService.CreateDummyEmployeeData (departmentId, m_scheduleService.StartDate);
           
            DateTime centre = m_employeeData.Days [day].DayStart.AddHours(12);

            ShiftEditMessage request = new ShiftEditMessage(this) 
                {
                    Op = ShiftEditMessage.Operation.CreateDefaultShift,
                    EmployeeID = newEmployee.EmployeeID,
                    Start = centre,
                    End = centre
                };

            Mvx.Resolve<IMvxMessenger> ().Publish (request);
            m_scheduleService.SetEdit(true);
        }

        void AddJobDay1 (object sender, EventArgs e)
        {
            AddJob (0);
        }

        void AddJobDay2 (object sender, EventArgs e)
        {
            AddJob (1);
        }

        void AddJobDay3 (object sender, EventArgs e)
        {
            AddJob (2);
        }

        void AddJobDay4 (object sender, EventArgs e)
        {
            AddJob (3);
        }

        void AddJobDay5 (object sender, EventArgs e)
        {
            AddJob (4);
        }

        void AddJobDay6 (object sender, EventArgs e)
        {
            AddJob (5);
        }

        void AddJobDay7 (object sender, EventArgs e)
        {
            AddJob (6);
        }

        public EmployeeData CreateDummyEmployeeData()
        {
            JDAScheduleConstraints constraints = new JDAScheduleConstraints () {
                UnavailabilityRanges = new List<JDAInterval>(),
                FixedShifts = new List<JDAEmployeeAssignedJob>(),
                MinMaxHoursByDays = new List<JDAScheduleConstraints.DailyConstraint>(),
                CanWorkSplit = true,
            };

            JDAEmployeeAttribute attribute = new JDAEmployeeAttribute () {
                //Code = "",
            };

            List<JDAEmployeeAttribute> attributeList = new List<JDAEmployeeAttribute> ();
            attributeList.Add (attribute);

            JDAEmployee jdaEmployee = new JDAEmployee () {
                EmployeeID = -10000000,
                FullName = Mvx.Resolve<IStringService>().Get ("spare_shifts_label"),
                IsMinor = false,
                CanWorkJobs = new List<JDAEmployeeAssignedJob> (),
                Shifts = new List<JDAShift>(),
                ScheduleConstraints = constraints,
                Attributes = attributeList,
            };

            List<JDAJob> jobRoles = GetJobRoles ();
            foreach(var job in jobRoles)
            {
                JDAEmployeeAssignedJob jdaJob = new JDAEmployeeAssignedJob ();
                jdaJob.JobID = job.JobID;
                jdaJob.IsPrimary = true;
                jdaEmployee.CanWorkJobs.Add (jdaJob );
            }
            JDAEmployeeInfo employeeInfo = new JDAEmployeeInfo () {
                EmployeeID = jdaEmployee.EmployeeID,
                HomeSiteID = m_scheduleService.GetSiteId(),
            };

            EmployeeData employee = new EmployeeData (jdaEmployee, jobRoles, m_scheduleService.StartDate) { 
                CanEdit = true,
                Info = employeeInfo,
                DayIndex = m_scheduleService.DayIndex,
                SpareShift = true,
            };

            employee.CalculateAllowableJobs (jobRoles);

            return employee;
        }

        private List<JDAJob> GetJobRoles()
        {
            int departmentId = m_scheduleService.CurrentFilter.Items.Where (p => p.Selected == true).First().ID;
            return m_scheduleService.GetJobRolesForDepartment (departmentId);
        }
    }
}

