﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using System.Linq;

namespace WFM.Android
{
    public class SchedulePlanningDayContentCell : CollectionItemView
    {
        private TextView m_nameTextView;
        private PlanningControl m_dayControl;

        public string Name
        {
            get { return null; }
            set
            {
                m_nameTextView.Text = value;
            }
        }

        private int? m_dayIndex;
        public int? DayIndex
        {
            get { return m_dayIndex; }
            set 
            { 
                m_dayIndex = value; 
            }
        }
            
        private double[] m_variance;
        public double[] Variance
        {
            get { return m_variance; }
            set
            {
                m_variance = value;
                UpdatePlanningControls ();
            }
        }

        public SchedulePlanningDayContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {

        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_nameTextView = (TextView)FindViewById(Resource.Id.name_textView);
            m_dayControl = (PlanningControl)FindViewById(Resource.Id.day_planningControl);
            m_dayControl.ShowGrid = true;
            m_dayControl.PadToWeekMax = false;

            var set = this.CreateBindingSet<SchedulePlanningDayContentCell, PlanningData>();
            set.Bind(this).For(b => b.Name).To(m => m.Name);
            set.Bind(this).For(b => b.DayIndex).To(m => m.DayIndex);
            set.Bind(this).For(b => b.Variance).To(m => m.Variance);
            set.Apply();
        }

        public override void OnShow()
        {
            base.OnShow();

            UpdatePlanningControls ();
        }

        private void UpdatePlanningControls()
        {
            var VM  = (PlanningData)DataContext;
            if(VM != null && VM.DayIndex != null)
            {
                m_dayControl.SetContentData(VM.Variance, (int) VM.DayIndex * ScheduleTime.DayIntervals, ScheduleTime.DayIntervals);
            }

            Invalidate();
        }
    }
}

