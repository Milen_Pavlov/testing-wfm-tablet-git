﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using WFM.Shared.DataTransferObjects.Breaks;
using Android.Graphics;

namespace WFM.Android
{
    public class AttendanceEntryCell : CollectionItemView, CompoundButton.IOnCheckedChangeListener
    {
        public AttendanceEntry VM
        {
            get { return (AttendanceEntry)this.DataContext; }
        }

        public bool IsCurrent
        {
            get { return false; }
            set { SetCurrentColor (value); }
        }

        private CheckBox m_switch;
        private Color m_colourInactive, m_colourActive;

        public AttendanceEntryCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_switch = (CheckBox)FindViewById(Resource.Id.attendance_switch);

            //No idea why I'm having to do it here, the style in the AXML doesn't do it, checkbox is too light to see
            int id = Resources.GetIdentifier("btn_check_holo_light", "drawable", "android");
            m_switch.SetOnCheckedChangeListener(this);
            m_switch.SetButtonDrawable(id);

            m_colourInactive = Color.Transparent;
            m_colourActive = ColorUtility.FromHexString("#8833b5e5");

            var set = this.CreateBindingSet<AttendanceEntryCell, AttendanceEntry>();
            set.Bind (this).For (v => v.IsCurrent).To (vm => vm.CurrentShift);
            set.Apply();
        }

        public override void OnShow ()
        {
            base.OnShow ();

            if (VM != null)
                SetCurrentColor (VM.CurrentShift);
        }

        private void SetCurrentColor(bool isCurrent)
        {
            SetBackgroundColor ((isCurrent) ? m_colourActive : m_colourInactive);
        }

        public void OnCheckedChanged(CompoundButton buttonView, bool isChecked)
        {
            if (VM != null && VM.IsIn != isChecked)
            {
                if(CustomCommand1 != null)
                {
                    CustomCommand1.Execute(DataContext);
                }
            }
        }
    }
}