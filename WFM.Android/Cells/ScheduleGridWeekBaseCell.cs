﻿using System;
using Consortium.Client.Android;
using Android.Views;
using WFM.Core;
using Cirrious.CrossCore;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using System.Collections.Generic;
using WFM.Droid.Controls;
using Java.Lang;
using Android.Graphics;
using Cirrious.MvvmCross.Plugins.Messenger;
using Android.Widget;
using Android.Util;

namespace WFM.Android
{
    public class ScheduleGridWeekBaseCell : CollectionItemView, View.IOnDragListener
    {
        static protected float c_autoScroll = 100.0f;

        protected ScheduleService m_scheduleService;
        protected SpareShiftService m_spareShiftService;

        protected bool m_displayPreviousDayShifts;
        protected int m_rowHeight = 0;

        protected List<WeeklyJobsControl> m_dayControls;

        //HACK: Performance increasing caching, should be refactored
        protected List<WeekDayCellView> m_jobContainerLayouts;

        protected List<WeeklyJobsControl>[] m_dynamicControls;

        protected List<TextView> m_dayFades;

        protected EmployeeData m_employeeData;
        public EmployeeData EmployeeData
        {
            get { return m_employeeData; }
            set
            {
                m_employeeData = value;

                if (m_draggedShift != null)
                    UpdateCanAcceptShift (null, DragAction.Started);
                else
                    HideFades ();
            }
        }

        public virtual List<EmployeeDayData> Days
        {
            get { return null; }
            set
            {
                foreach (WeeklyJobsControl control in m_dayControls)
                {
                    control.Visibility = ViewStates.Invisible;
                }

                for (int i = 0; i < 7; i++)
                {
                    foreach (var control in m_dynamicControls[i])
                    {
                        m_jobContainerLayouts[i].RemoveView(control);
                    }

                    m_dynamicControls[i].Clear();
                }

                if (value != null)
                {
                    int maxRows = 0;

                    // Add all jobs
                    for (int n = 0; n < 7; n++)
                    {
                        int rows = 0;

                        EmployeeDayData day = value[n];

                        DateTime dayStart = day.DayStart;

                        if(m_displayPreviousDayShifts)
                        {
                            if(n == 0)  // If first day of week, add any previous week shifts that run over from the previous week
                            {
                                foreach (ShiftData shift in m_employeeData.LastDayPreviousWeek.Shifts)
                                {
                                    foreach (JobData job in shift.Jobs)
                                    {
                                        AddJobControlForDay(n, job, shift);
                                        rows++;
                                    }
                                }
                            } // Else add any previous day shifts that run over
                            else
                            {
                                EmployeeDayData previousDay = value[n-1];
                                foreach (ShiftData shift in previousDay.Shifts)
                                {
                                    foreach (JobData job in shift.Jobs)
                                    {
                                        if(job.End >= value[n].DayStart)
                                        {
                                            AddJobControlForDay(n, job, shift);
                                            rows++;
                                        }
                                    }
                                }
                            }
                        }

                        foreach (ShiftData shift in day.Shifts)
                        {
                            foreach (JobData job in shift.Jobs)
                            {
                                AddJobControlForDay(n, job, shift);
                                rows++;
                            }
                        }

                        if (rows > maxRows)
                            maxRows = rows;
                    }

                    maxRows += OnAllJobsAdded ();
                    int height = m_rowHeight;

                    if (maxRows > 0)
                        height *= maxRows;

                    m_jobContainerLayouts.ForEach(layout  => layout.SetMinimumHeight(height));
                }
            }
        }

        private WeeklyJobsControl m_dragControl;
        static protected ShiftData m_draggedShift;
        private DragCollectionControl m_dragView;

        public ScheduleGridWeekBaseCell (Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
            Init ();
        }

        private void Init()
        {
            m_scheduleService = Mvx.Resolve<ScheduleService> ();
            m_spareShiftService = Mvx.Resolve<SpareShiftService> ();
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            m_rowHeight = this.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_GridWeekCell_MinHeight);

            m_displayPreviousDayShifts = false;

            m_jobContainerLayouts = new List<WeekDayCellView>();

            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day1_moreDayCells));
            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day2_moreDayCells));
            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day3_moreDayCells));
            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day4_moreDayCells));
            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day5_moreDayCells));
            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day6_moreDayCells));
            m_jobContainerLayouts.Add((WeekDayCellView)this.FindViewById(Resource.Id.day7_moreDayCells));

            for (int i = 0; i < m_jobContainerLayouts.Count; ++i)
            {
                m_jobContainerLayouts[i].DayIndex = i;   
            }

            m_dayControls = new List<WeeklyJobsControl>();
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day1_job_control));
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day2_job_control));
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day3_job_control));
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day4_job_control));
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day5_job_control));
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day6_job_control));
            m_dayControls.Add((WeeklyJobsControl)this.FindViewById(Resource.Id.day7_job_control));

            m_dayFades = new List<TextView> ();
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day1_fade));
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day2_fade));
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day3_fade));
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day4_fade));
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day5_fade));
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day6_fade));
            m_dayFades.Add ((TextView)this.FindViewById (Resource.Id.day7_fade));


            m_dynamicControls = new List<WeeklyJobsControl>[7];

            for (int i = 0; i < 7; i++)
                m_dynamicControls[i] = new List<WeeklyJobsControl>();

            SetOnDragListener(this);
        }

        public void AddJobControlForDay(int dayIndex, JobData job, ShiftData shift)
        {
            WeeklyJobsControl control;

            if (m_dayControls[dayIndex].Visibility == ViewStates.Invisible)
            {
                control = m_dayControls[dayIndex];
            }
            else //already in use
            {
                control = new WeeklyJobsControl(this.Context);

                m_dynamicControls[dayIndex].Add(control);
                m_jobContainerLayouts[dayIndex].AddView(control);
            }

            control.Shift = shift;
            control.ShiftSelect = OnShiftSelect;
            control.ShiftReassign += OnShiftReassign;
            control.TimeOffSelect = OnTimeOffSelect;
            control.Employee = EmployeeData;
            control.Job = job;
            control.TimeBackgroundColor = Resources.GetColor(Resource.Color.white);
            control.Visibility = ViewStates.Visible;
        }

        public void OnShiftReassign(View v, ShiftData shift, Point touchPoint)
        {
            if (m_employeeData == null || v == null || shift == null || touchPoint == null)
                return;
            
            // Ensure touchpoint is not negative
            if(touchPoint.X <= 0) { touchPoint.X = 0; }
            if(touchPoint.Y <= 0) { touchPoint.Y = 0; }

            // Setup data to pass throw with drag event
            ClipData.Item shiftId = new ClipData.Item(shift.ShiftID.ToString());
            ClipData.Item oldEmployeeId = new ClipData.Item(shift.EmployeeID.ToString());
            ClipData.Item shiftStartTicks = new ClipData.Item(shift.Start.Ticks.ToString());
            ClipData.Item shiftEndTicks = new ClipData.Item(shift.End.Ticks.ToString());
            ClipData.Item shiftViewWidth = new ClipData.Item(v.Width.ToString());
            ClipData.Item firstTouchPointX = new ClipData.Item(touchPoint.X.ToString());
            ClipData.Item isSpareShift = new ClipData.Item (m_employeeData.SpareShift.ToString ());

            string[] clipDescriptor = {ClipDescription.MimetypeTextPlain, ClipDescription.MimetypeTextPlain};

            ClipData dragData = new ClipData(new ClipDescription("Plain", clipDescriptor), shiftId);
            dragData.AddItem(oldEmployeeId);
            dragData.AddItem(shiftStartTicks);
            dragData.AddItem(shiftEndTicks);
            dragData.AddItem(shiftViewWidth);
            dragData.AddItem(firstTouchPointX);
            dragData.AddItem (isSpareShift);

            m_draggedShift = shift;

            WeekShiftDragShadow dragShadow = new WeekShiftDragShadow(v, touchPoint);

            // Starts the drag
            v.StartDrag(dragData,
                dragShadow,
                dragData, 
                0) ;
        }

        public void OnShiftSelect(ShiftData shift)
        {
            if (CustomCommand2 != null && shift != null)
            {
                ConsortiumPopoverView.SourceView = FindViewById(Resource.Id.name_popupPosition);
                CustomCommand2.Execute(shift);
            }
        }

        public virtual void OnTimeOffSelect(TimeOffData timeOff)
        {
            
        }

        public bool OnDrag(View v, DragEvent e)
        {
            if (m_dragView == null)
            {
                m_dragView = GetDragView (this);
                if (m_dragView != null)
                {
                    m_dragView.AutoScrollWhenDragging = false;
                }
            }

            if (m_employeeData != null)
                Log.Debug ("Drag Event " + e.Action.ToString (), m_employeeData.Name);
            else
                Log.Debug ("Drag Event " + e.Action.ToString (), "unknown");

            switch (e.Action) {
                case DragAction.Started:                    
                    UpdateCanAcceptShift (e);
                    return true;
                case DragAction.Entered:
                    ShowDragUI (e);
                    return false;
                case DragAction.Exited:
                    HideDragUI (e);
                    return false;
                case DragAction.Drop:
                    HideDragUI (e);
                    if (CanAcceptShift (e) == false)
                        return false;
                    // Send message that a shift drag has ended.
                    // Retrieve ClipData
                    int shiftId = Integer.ParseInt (e.ClipData.GetItemAt (0).Text);
                    int oldEmployeeId = Integer.ParseInt (e.ClipData.GetItemAt (1).Text);
                    DateTime shiftStart = new DateTime (Long.ParseLong (e.ClipData.GetItemAt (2).Text));
                    DateTime shiftEnd = new DateTime (Long.ParseLong (e.ClipData.GetItemAt (3).Text));
                    //                    int shiftViewWidth = Integer.ParseInt (e.ClipData.GetItemAt (4).Text);
                    //                    float firstTouchPointX = Float.ParseFloat (e.ClipData.GetItemAt (5).Text);
                    //                    bool isSpareShift = bool.Parse (e.ClipData.GetItemAt (6).Text);

                    ProcessDroppedShift(shiftStart, shiftEnd, oldEmployeeId, shiftId);
                    if (m_dragView != null)
                    {
                        m_dragView.StopScrolling ();
                    }
                    return false;
                case DragAction.Ended:
                    HideFades ();
                    m_draggedShift = null;
                    HideDragUI (e);
                    if (m_dragView != null)
                    {
                        m_dragView.StopScrolling ();
                    }
                    return true;
                case DragAction.Location:
                    if (m_dragView != null)
                    {
                        float dragAreaRelativePos = Top + e.GetY ();
                        if (dragAreaRelativePos < c_autoScroll)
                        {
                            // Start Scrolling up
                            m_dragView.StartScrollingUp ();
                        }
                        else if (dragAreaRelativePos > (m_dragView.Bottom - m_dragView.Top) - c_autoScroll)
                        {
                            //Start Scrolling down
                            m_dragView.StartScrollingDown ();
                        }
                        else
                        {
                            m_dragView.StopScrolling ();
                        }
                    }
                    return true;
            }

            return false;
        }

        public void ProcessDroppedShift(DateTime shiftStart, DateTime shiftEnd, int oldEmployeeId, int shiftId)
        {
            bool isSameEmployee = EmployeeData.EmployeeID == oldEmployeeId;
            if(isSameEmployee)
            {
                return; // If no change to shift on update, halt here
            }

            ShiftEditMessage shiftEdit= new ShiftEditMessage(this)
                {
                    Op = ShiftEditMessage.Operation.UpdateShift,
                    EmployeeID = oldEmployeeId,
                    ShiftID = shiftId,
                    Start = shiftStart,
                    End = shiftEnd,
                    RecalculateDetails = true
                };

            if(!isSameEmployee)
            {
                shiftEdit.Op = ShiftEditMessage.Operation.ReassignShift;
                shiftEdit.NewEmployeeID = EmployeeData.EmployeeID;
            }

            Mvx.Resolve<IMvxMessenger> ().Publish (shiftEdit);
        }

        public override void OnShow()
        {
            base.OnShow ();

            if (EmployeeData != null && EmployeeData.Days != null)
            {
                Days = EmployeeData.Days;
            }
        }

        protected void UpdateCanAcceptShift(DragEvent e)
        {
            UpdateCanAcceptShift(e, e.Action);
        }

        protected void UpdateCanAcceptShift(DragEvent e, DragAction action)
        {
            bool canAcceptShift = true;

            switch (action)
            {
                case DragAction.Ended:
                    canAcceptShift = true;
                    break;
                case DragAction.Started:
                    canAcceptShift = CanAcceptShift (e);
                    break;
                default:
                    return;
            }
                    
            DayOfWeek? dayForShift = null;
            if (e != null)
                dayForShift = GetDayForShift (e);
            else if(m_draggedShift != null)
                dayForShift = m_draggedShift.Start.DayOfWeek;

            for (int i = 0; i < 7; ++i)
            {
                bool valid = canAcceptShift;
                int testDayIndex = (int)(dayForShift.Value - 1);
                if(testDayIndex < 0)
                {
                    testDayIndex += 7;
                }

                if (action != DragAction.Ended && dayForShift.HasValue && i != testDayIndex)
                    valid = false;

                if (m_dayFades[i] != null)
                {
                    if (valid)
                        m_dayFades[i].SetBackgroundColor (Color.Argb (0, 0, 0, 0));
                    else
                        m_dayFades[i].SetBackgroundColor (Color.Argb (127, 0, 0, 0));
                }
            }
        }

        private bool CanAcceptShift(DragEvent e)
        {
            if (m_employeeData == null)
                return false;

            if (m_draggedShift != null)
                return m_employeeData.CanWorkShift(m_draggedShift);

            if (e == null)
                return false;
            
            ClipData clipData = e.LocalState as ClipData;
            if (clipData == null)
                return false;

            int  originalEmployeeId = Integer.ParseInt(clipData.GetItemAt(1).Text);
            EmployeeData originalEmployee = m_scheduleService.Schedule.GetEmployee (originalEmployeeId);
            if (originalEmployee == null)
            {
                originalEmployee = m_spareShiftService.GetEmployee (originalEmployeeId);
            }

            if (originalEmployee == null)
            {
                return false;
            }

            int  shiftId = Integer.ParseInt(clipData.GetItemAt(0).Text);
            ShiftData shift = originalEmployee.GetShift (shiftId);

            return m_employeeData.CanWorkShift (shift);
        }

        private DayOfWeek? GetDayForShift(DragEvent e)
        {
            ClipData clipData = e.LocalState as ClipData;
            if (clipData == null)
                return null;

            int  originalEmployeeId = Integer.ParseInt(clipData.GetItemAt(1).Text);
            EmployeeData originalEmployee = m_scheduleService.Schedule.GetEmployee (originalEmployeeId);
            if (originalEmployee == null)
            {
                originalEmployee = m_spareShiftService.GetEmployee (originalEmployeeId);
            }

            if (originalEmployee == null)
            {
                return null;
            }

            int  shiftId = Integer.ParseInt(clipData.GetItemAt(0).Text);
            ShiftData shift = originalEmployee.GetShift (shiftId);
            if (shift == null)
                return null;
            
            return shift.Start.DayOfWeek; 
        }

        private void ShowDragUI(DragEvent e)
        {
            if (e.LocalState != null) 
            {
                ClipData data = e.LocalState as ClipData;

                /*bool isSpareShift = bool.Parse (data.GetItemAt (6).Text);
                if (isSpareShift == false)
                    return;

                int oldEmployeeId = Integer.ParseInt (data.GetItemAt (1).Text);
                EmployeeData employee = m_scheduleService.Schedule.GetEmployee (oldEmployeeId);
                if (employee == null)
                {
                    employee = m_spareShiftService.GetEmployee (oldEmployeeId);
                }
                if (employee == null)
                {
                    return;
                }
                    
                foreach (var day in m_employeeData.Days)
                {
                    foreach (var shift in day.Shifts)
                    {
                        if (shift.EmployeeID == employee.EmployeeID)
                            return;
                    }
                }

                int shiftId = Integer.ParseInt (data.GetItemAt (0).Text);
                ShiftData shiftData = employee.GetShift(shiftId);

                int dayOfWeek = (int)shiftData.Start.DayOfWeek - 1;
                if (dayOfWeek < 0)
                    dayOfWeek += 7;
                if (m_dragControl != null) 
                {
                    
                    m_jobContainerLayouts[dayOfWeek].RemoveView (m_dragControl);
                    m_dragControl.Dispose ();
                    m_dragControl = null;
                }

                m_dragControl = new WeeklyJobsControl (Context);
                m_dragControl.Shift = shiftData;
                m_dragControl.ShiftSelect = OnShiftSelect;
                m_dragControl.ShiftReassign += OnShiftReassign;
                m_dragControl.TimeOffSelect = OnTimeOffSelect;
                m_dragControl.Employee = EmployeeData;
                m_dragControl.Job = shiftData.Jobs[0];
                m_dragControl.Visibility = ViewStates.Visible;
                m_dragControl.TimeBackgroundColor = Resources.GetColor(Resource.Color.white);

                m_dragControl.Alpha = 0.6f;

                m_dragControl.LayoutParameters = new FrameLayout.LayoutParams (FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.WrapContent);
                m_jobContainerLayouts[dayOfWeek].AddView (m_dragControl);*/
            } 
        }

        private void HideDragUI(DragEvent e)
        {
            if (m_dragControl != null && e.LocalState != null)
            {
                ClipData data = e.LocalState as ClipData;
                int oldEmployeeId = Integer.ParseInt (data.GetItemAt (1).Text);
                EmployeeData employee = m_scheduleService.Schedule.GetEmployee (oldEmployeeId);
                if (employee == null)
                {
                    employee = m_spareShiftService.GetEmployee (oldEmployeeId);
                }

                if (employee == null)
                    return;
                    
                int shiftId = Integer.ParseInt (data.GetItemAt (0).Text);
                ShiftData shiftData = employee.GetShift (shiftId);

                if (shiftData == null)
                    return;

                int dayOfWeek = (int)shiftData.Start.DayOfWeek - 1;
                if (dayOfWeek < 0)
                    dayOfWeek += 7;
                m_jobContainerLayouts[dayOfWeek].RemoveView (m_dragControl);
                m_dragControl.Dispose ();
                m_dragControl = null;
            }

        }

        protected void HideFades()
        {
            foreach (var day in m_dayFades)
            {
                if(day != null)
                    day.SetBackgroundColor (new Color(0, 0, 0, 0));
            }
        }

        internal class WeekShiftDragShadow : View.DragShadowBuilder
        {
            private Point m_touchPoint;
            public bool Invisible { get; set; }

            public WeekShiftDragShadow(View v, Point touchPoint) : base(v)
            {
                m_touchPoint = touchPoint;
            }

            public override void OnProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
            {
                shadowSize.Set (View.Width, View.Height);
                shadowTouchPoint.Set (m_touchPoint.X, m_touchPoint.Y);
            }

            public override void OnDrawShadow (Canvas canvas)
            {
                if (Invisible)
                    return;
                
                base.OnDrawShadow (canvas);
            }
        }

        protected virtual int OnAllJobsAdded()
        {
            // Return how many rows have been added.
            return 0;
        }

        private DragCollectionControl GetDragView (View view)
        {
            if (view == null)
                return null;
            
            if ((view as DragCollectionControl) != null)
                return view as DragCollectionControl;

            return GetDragView (view.Parent as View);
        }
    }
}

