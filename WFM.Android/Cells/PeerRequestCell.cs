﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class PeerRequestCell : CollectionItemView
    {
        private TextView m_dateLabel;
        private readonly IStringService m_localiser;

        public PeerRequestCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            m_dateLabel = FindViewById<TextView>(Resource.Id.peerrequest_dates);
        }

        public override void OnShow ()
        {
            base.OnShow ();

            PeerRequest data = DataContext as PeerRequest;
            if (data != null)
            {
                
                TimeSpan span = data.EndDate - data.StartDate;
                if (span.TotalDays <= 1)
                    m_dateLabel.Text = data.StartDate.ToString (m_localiser.Get("dd/MM/yyyy"));
                else
                    m_dateLabel.Text = string.Format ("{0} - {1}", data.StartDate.ToString (m_localiser.Get("dd/MM/yyyy")), data.EndDate.ToString (m_localiser.Get("dd/MM/yyyy")));
            }
        }
    }
}

