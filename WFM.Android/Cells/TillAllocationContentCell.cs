﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using WFM.Core;
using Java.Lang;
using WFM.Core.Model;

namespace WFM.Android
{
	public class TillAllocationContentCell : FrameLayout, View.IOnDragListener
    {
		private IStringService Localiser;
		private IAlertBox AlertBox;
		private IESSConfigurationService Settings;

		const int c_hoursInView = 24;
		const float c_minutesInDay = 1440;
		private float m_timeInterval = c_minutesInDay / ScheduleTime.DayIntervals;

        // Misc
        private AllocationsViewModel.TillModel m_till;
        public AllocationsViewModel.TillModel Till
        {
            get { return m_till; }
            set
            {
                m_till = value;
                RefreshViews();
            }
        }

		public bool CanEdit
		{
			get
			{
				if (Till != null)
					return Till.CanEdit;
				else
					return false;
			}
		}

		private CurrentTimeControl m_currentTimeControl;
			
		private TillAllocationControl m_dragControl;

        public TillAllocationContentCell(Context context)
            : base(context) 
        {
            Initialise();
        }

        public TillAllocationContentCell(Context context, IAttributeSet attrs)
            : base(context, attrs) 
        {
            Initialise();
        }

        public TillAllocationContentCell(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle) 
        {
            Initialise();
        }

        private void Initialise()
        {
			AlertBox = Mvx.Resolve<IAlertBox> ();
			Localiser = Mvx.Resolve<IStringService> ();
			Settings = Mvx.Resolve<IESSConfigurationService> ();
			
            SetWillNotDraw(false);
			SetOnDragListener(this);
        }

        private void RefreshViews()
        {
            RemoveAllViews();

            if (Till != null)
            {
				Till.HoursChanged = OnHoursChanged;

				//Add background if needed
				if (Till.TillTypeKey == TillAllocation.BufferTillType) 
				{
					//Add Label and set background
					var label = new TextView(Context);
					label.SetText("Unassigned Allocations", TextView.BufferType.Normal);
					label.TextSize = Resources.GetDimension (Resource.Dimension.Allocations_Cell_Content_UnassignedTextSize);
					label.SetTextColor (Color.White);
					label.SetBackgroundColor (new Color (0, 0, 0, 48));
					label.Gravity = GravityFlags.Center;
					label.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
					AddView (label);
				}

                if (Till.TillWindows != null)
                {
                    foreach (var tillWindow in Till.TillWindows)
                    {
                        var tillWindowControl = new TillWindowControl(Context);
                        tillWindowControl.Model = tillWindow;
                        tillWindow.HoursChanged = tillWindowControl.OnHoursUpdated;
                        tillWindowControl.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
                        AddView(tillWindowControl);
                    }
                }

                if (Till.TillAllocations != null)
                {
                    foreach (var tillAllocation in Till.TillAllocations)
                    {
                        var tillAllocationControl = new TillAllocationControl(Context);
                        tillAllocationControl.StartDate = Till.StartDate;
                        tillAllocationControl.Model = tillAllocation;
						tillAllocationControl.TillAllocationReassign += OnTillAllocationReassign;
                        tillAllocation.HoursChanged = tillAllocationControl.OnHoursUpdated;
                        tillAllocationControl.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
                        AddView(tillAllocationControl);
                    }
                }
				
				m_currentTimeControl = m_currentTimeControl ?? new CurrentTimeControl(Context);
				m_currentTimeControl.MinDate = Till.StartDate.AddHours(Till.Hours.Item1);
				m_currentTimeControl.MaxDate = Till.StartDate.AddHours(Till.Hours.Item2);
				m_currentTimeControl.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);
				AddView(m_currentTimeControl);
            }
        }

		private void OnHoursChanged()
		{
            if (Till != null && Till.Hours != null && m_currentTimeControl != null)
            {
                m_currentTimeControl.MinDate = Till.StartDate.AddHours(Till.Hours.Item1);
                m_currentTimeControl.MaxDate = Till.StartDate.AddHours(Till.Hours.Item2);
                m_currentTimeControl.Invalidate();
            }
		}

		private void ShowDragUI(DragEvent e)
		{
			if(!Settings.ShowMultipleTillAllocationBuffers || Till.TillTypeKey != TillAllocation.BufferTillType)
				SetBackgroundColor (Color.LightSkyBlue);
			else
				SetBackgroundColor (Color.White);

			if (e.LocalState != null) 
			{
				ClipData data = e.LocalState as ClipData;
				//Render temp clipdata
				int allocationId = Integer.ParseInt (data.GetItemAt (0).Text);
				int fromTillId = Integer.ParseInt (data.GetItemAt (1).Text);
				string employeeName = data.GetItemAt (2).Text;
				DateTime startTime = new DateTime (Long.ParseLong (data.GetItemAt (3).Text));
				DateTime endTime = new DateTime (Long.ParseLong (data.GetItemAt (4).Text));

				if (m_dragControl != null) 
				{
					RemoveView (m_dragControl);
					m_dragControl.Dispose ();
					m_dragControl = null;
				}

				AllocationsViewModel.TillAllocationModel allocation = new AllocationsViewModel.TillAllocationModel ();
				allocation.EmployeeFullName = employeeName;
				allocation.TillAllocationId = fromTillId;
				allocation.StartTime = startTime;
				allocation.EndTime = endTime;
				allocation.Hours = Till.Hours;

                allocation.Colour = Till.GetColourForAllocation(allocation);

				m_dragControl = new TillAllocationControl (Context);
                m_dragControl.StartDate = Till.StartDate;
				m_dragControl.Model = allocation;
				m_dragControl.LayoutParameters = new FrameLayout.LayoutParams (FrameLayout.LayoutParams.WrapContent, FrameLayout.LayoutParams.WrapContent);

				AddView (m_dragControl);

				//If this overlaps with any existing data then switch to Invalid UI
				bool invalid = IsInvalid(allocation.TillAllocationId, allocation.StartTime, allocation.EndTime, allocation.EmployeeFullName, true, false);
				m_dragControl.InvalidLocation = invalid;

				if (invalid) 
					SetBackgroundColor (Color.PaleVioletRed);
			} 
		}

		private void HideDragUI()
		{
			SetBackgroundColor (Color.White);

			if (m_dragControl != null) 
			{
				RemoveView (m_dragControl);
				m_dragControl.Dispose ();
				m_dragControl = null;
			}
		}

		public bool OnDrag(View v, DragEvent e)
		{
			switch (e.Action) {
			case DragAction.Started:
				return true;
			case DragAction.Entered:
				ShowDragUI(e);
				return false;
			case DragAction.Exited:
				HideDragUI();
				return false;
			case DragAction.Drop:
				// Retrieve ClipData (Note - can only do this in Drop events as its not set in any others, use LocalState if you need it)
				int tillAllocationId = Integer.ParseInt(e.ClipData.GetItemAt(0).Text);
				int fromTillId = Integer.ParseInt(e.ClipData.GetItemAt(1).Text);
				string  employeeName = e.ClipData.GetItemAt(2).Text;
				DateTime  startTime = new DateTime(Long.ParseLong(e.ClipData.GetItemAt(3).Text));
				DateTime  endTime = new DateTime(Long.ParseLong(e.ClipData.GetItemAt(4).Text));

				ProcessDroppedTillAllocation(startTime, endTime, employeeName, tillAllocationId, fromTillId);

				return false;
			case DragAction.Ended:
				HideDragUI();
				return true;
			case DragAction.Location:
				return false;
			}

			return false;
		}

		public bool IsInvalid(int id, DateTime startTime, DateTime endTime, string name, bool ignoreSelf, bool displayError)
		{
            if (Till.SSCSAYS)
                return true;

			//Check if valid
			foreach (AllocationsViewModel.TillAllocationModel alloc in Till.TillAllocations) 
			{
				if (ignoreSelf && alloc.TillAllocationId == id)
					continue;
				
				if (alloc.TillAllocationId == id ||
					(startTime < alloc.EndTime && endTime >= alloc.EndTime) ||
					(endTime > alloc.StartTime && startTime <= alloc.StartTime) ||
					(startTime >= alloc.StartTime && endTime <= alloc.EndTime))
				{
					//Display alert
					if(displayError && alloc.TillAllocationId != id)
						AlertBox.ShowOK(Localiser.Get("tillallocation_invalid_title"), string.Format(Localiser.Get("tillallocation_invalid_clash"),name, alloc.EmployeeFullName));
					return true;
				}
			}

            //Check if till is closed
            if (Till.IsOutOfOrder)
            {
                if(displayError)
                    AlertBox.ShowOK(Localiser.Get("tillallocation_invalid_title"), Localiser.Get("tillallocation_invalid_outoforder"));

                return true;
            }

			//Check Opening times
			if (Till.IsBasket) 
			{
				//Check all windows
				foreach (AllocationsViewModel.TillWindowModel window in Till.TillWindows) 
				{
					if (startTime.TimeOfDay >= window.StartTime && endTime.TimeOfDay <= window.EndTime)
						return false;
				}

				if(displayError)
					AlertBox.ShowOK(Localiser.Get("tillallocation_invalid_title"), Localiser.Get("tillallocation_invalid_window"));
				
				return true;
			}

			return false;
		}

		public void ProcessDroppedTillAllocation(DateTime startTime, DateTime endTime, string employeeName, int tillAllocationId, int fromTillId)
		{
            if (Till.SSCSAYS)
                return;

			//Are we already here?
			foreach (AllocationsViewModel.TillAllocationModel alloc in Till.TillAllocations)
				if (alloc.TillAllocationId == tillAllocationId)
					return;

			//Are we going from Unassigned to Unassgiend
			if (Till.TillTypeKey == TillAllocation.BufferTillType && Settings.ShowMultipleTillAllocationBuffers && fromTillId < 0)
				return;

			TillAllocationEditMessage editMsg = new TillAllocationEditMessage (this) 
			{
				Op = TillAllocationEditMessage.Operation.ReassignTill,
				TillAllocationID = tillAllocationId,
				TillID = Till.TillId,
			};

			if (Till.TillTypeKey == TillAllocation.BufferTillType && Settings.ShowMultipleTillAllocationBuffers) {
				//Check if we are valid
				bool invalid = IsInvalid (tillAllocationId, startTime, endTime, employeeName, false, false);
				if (invalid) 
				{
					editMsg.TillID = TillAllocation.NewBufferTillId;
				}
			} 
			else if (IsInvalid(tillAllocationId, startTime, endTime, employeeName, false, true))
				return;


			Mvx.Resolve<IMvxMessenger> ().Publish (editMsg);
		}

		public void OnTillAllocationReassign(View v, AllocationsViewModel.TillAllocationModel till, Point touchPoint)
		{
			// Ensure touchpoint is not negative
			if(touchPoint.X <= 0) { touchPoint.X = 0; }
			if(touchPoint.Y <= 0) { touchPoint.Y = 0; }

			// Setup data to pass throw with drag event
			ClipData.Item tillAllocationId = new ClipData.Item(till.TillAllocationId.ToString());
			ClipData.Item fromTillId = new ClipData.Item(till.TillId.ToString());
			ClipData.Item employeeName = new ClipData.Item(till.EmployeeFullName);
			ClipData.Item startTicks = new ClipData.Item(till.StartTime.Ticks.ToString());
			ClipData.Item endTicks = new ClipData.Item(till.EndTime.Ticks.ToString());

			string[] clipDescriptor = {ClipDescription.MimetypeTextPlain, ClipDescription.MimetypeTextPlain};

			ClipData dragData = new ClipData(new ClipDescription("Plain", clipDescriptor), tillAllocationId);
			dragData.AddItem(fromTillId);
			dragData.AddItem(employeeName);
			dragData.AddItem(startTicks);
			dragData.AddItem(endTicks);

			TillAllocationDragShadow dragShadow = new TillAllocationDragShadow(v, touchPoint);
	
			// Starts the drag
			v.StartDrag(dragData,
				dragShadow,
				dragData, 
				0) ;
		}
    }

	internal class TillAllocationDragShadow : View.DragShadowBuilder
	{
		private Point m_touchPoint;

		public TillAllocationDragShadow(View v, Point touchPoint) : base(v)
		{
			m_touchPoint = touchPoint;
		}

		public override void OnProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint)
		{
			shadowSize.Set (View.Width, View.Height);
			shadowTouchPoint.Set (m_touchPoint.X, m_touchPoint.Y);
		}

		public override void OnDrawShadow (Canvas canvas)
		{
//			base.OnDrawShadow (canvas);//Hide the shadow
		}
	}
}

