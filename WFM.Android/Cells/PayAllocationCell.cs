﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.Android
{
    public class PayAllocationCell : CollectionItemView
    {
        private TextView m_dateLabel;
        private TextView m_hoursLabel;
        private TextView m_totalLabel;

        public PayAllocationCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            m_dateLabel = FindViewById<TextView>(Resource.Id.payallocation_date);
            m_hoursLabel = FindViewById<TextView>(Resource.Id.payallocation_hours);
            m_totalLabel = FindViewById<TextView>(Resource.Id.payallocations_total);
        }

        public override void OnShow ()
        {
            base.OnShow ();

            PayAllocationItem data = DataContext as PayAllocationItem;

            if (data != null)
            {
                m_dateLabel.Text = data.Allocation.Date.ToString (Mvx.Resolve<IStringService>().Get("dd/MM/yyyy"));
                m_hoursLabel.Text = string.Format("{0:0.00}", data.Allocation.PaidHours);
                m_totalLabel.Text = string.Format("{0:0.00}", data.TotalHours);
            }
        }
    }
}

