﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;
using WFM.Shared.DataTransferObjects.Breaks;

namespace WFM.Android
{
    public class TillBreaksContentCell : CollectionItemView, CompoundButton.IOnCheckedChangeListener
    {
        public BreakItem VM
        {
            get { return (BreakItem)this.DataContext; }
        }

        protected CheckBox m_breakTakenSwitch;

        public TillBreaksContentCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_breakTakenSwitch = (CheckBox)FindViewById(Resource.Id.break_taken_switch);

            //No idea why I'm having to do it here, the style in the AXML doesn't do it, checkbox is too light to see
            int id = Resources.GetIdentifier("btn_check_holo_light", "drawable", "android");
            m_breakTakenSwitch.SetOnCheckedChangeListener(this);
            m_breakTakenSwitch.SetButtonDrawable(id);
        }

        public void OnCheckedChanged(CompoundButton buttonView, bool isChecked)
        {
            if (VM != null && VM.BreakTaken != isChecked)
            {
                if(CustomCommand1 != null)
                {
                    CustomCommand1.Execute(DataContext);
                }
            }
        }
    }
}