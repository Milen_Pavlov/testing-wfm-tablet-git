﻿using System;
using Consortium.Client.Android;
using Android.Widget;
using Android.Graphics;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Views;
using System.Collections.Generic;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views.InputMethods;

namespace WFM.Android
{
    public class DailyOvertimeModelCell : AlternateBackgroundItemView
    {
        private Dictionary<DayOfWeek, TextView> m_days;

        private Dictionary<DayOfWeek, String> m_hourStrings;
        public Dictionary<DayOfWeek, String> HourStrings
        {
            get { return m_hourStrings; }
            set
            {
                m_hourStrings = value;
                foreach (var day in value)
                {
                    m_days[day.Key].Text = day.Value;
                }
            }
        }

        private Dictionary<DayOfWeek, bool> m_dayEdited;
        public Dictionary<DayOfWeek, bool> DayEdited
        {
            get { return m_dayEdited; }
            set
            {
                m_dayEdited = value;
                foreach (var day in value)
                {
                    UpdateCellBackground (m_days[day.Key], day.Value);
                }
            }
        }

        private bool m_editable;
        public bool Editable
        {
            get { return m_editable; }
            set { m_editable = value; SetEditable (value); }
        }

        private static Color m_evenAisle = new Color (240, 240, 255, 255);
        public override Color EvenCol { get { return m_evenAisle; } }

        private static Color m_oddAisle = new Color (224, 224, 255, 255);
        public override Color OddCol { get { return m_oddAisle; } }

        public DailyOvertimeModelCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnAttachedToWindow ()
        {
            base.OnAttachedToWindow ();
        }

        protected override void OnDetachedFromWindow ()
        {
            base.OnDetachedFromWindow ();
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_days = new Dictionary<DayOfWeek, TextView> ();

            m_days.Add(DayOfWeek.Monday, FindViewById (Resource.Id.day_1) as TextView);
            m_days.Add(DayOfWeek.Tuesday, FindViewById (Resource.Id.day_2) as TextView);
            m_days.Add(DayOfWeek.Wednesday, FindViewById (Resource.Id.day_3) as TextView);
            m_days.Add(DayOfWeek.Thursday, FindViewById (Resource.Id.day_4) as TextView);
            m_days.Add(DayOfWeek.Friday, FindViewById (Resource.Id.day_5) as TextView);
            m_days.Add(DayOfWeek.Saturday, FindViewById (Resource.Id.day_6) as TextView);
            m_days.Add(DayOfWeek.Sunday, FindViewById (Resource.Id.day_7) as TextView);
            foreach (var day in m_days)
            {
                day.Value.Click += OnDayClicked;
            }
            var set = this.CreateBindingSet<DailyOvertimeModelCell, DailyOvertimeModel>();
            set.Bind (this).For(v => v.HourStrings).To (vm => vm.HourStrings);
            set.Bind (this).For (v => v.DayEdited).To (vm => vm.DayEdited);
            set.Bind (this).For (v => v.Editable).To (vm => vm.Editable);
            set.Apply();
        }

        void UpdateCellBackground(TextView cell, bool edited)
        {
            if (cell == null)
                return;
            
            if(edited == true)
                cell.SetBackgroundColor (new Color(255, 40, 40, 60));
            else
                cell.SetBackgroundColor (Color.Transparent);
        }

        private void SetEditable(bool editable)
        {
            foreach (var day in m_days)
            {
                SetEditable (day.Value, editable);
            }
        }

        private void SetEditable(TextView editText, bool editable)
        {
            if (editText == null)
                return;
            
            editText.Enabled = editable;
            if (editable)
            {
                editText.SetTypeface (null, TypefaceStyle.Normal);
                editText.SetTextColor (Color.Blue);
            }
            else
            {
                editText.SetTypeface (null, TypefaceStyle.Bold);
                editText.SetTextColor (Color.Black);
            }
        }

        void OnDayClicked (object sender, EventArgs e)
        {
            if (CustomCommand1 != null)
            {
                foreach(var day in m_days)
                {
                    if (day.Value == sender)
                    {
                        var data = BindingData as DailyOvertimeModel;
                        data.SelectedDay = day.Key;
                        if(CustomCommand1.CanExecute (data))
                            CustomCommand1.Execute (data);
                        return;
                    }
                }
            }
        }
    }
}

