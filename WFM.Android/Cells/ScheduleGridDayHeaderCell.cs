﻿using System;
using Consortium.Client.Android;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Android.Views;

namespace WFM.Android
{
	public class ScheduleGridDayHeaderCell : CollectionItemView
    {
        private ImageView m_sortImageView;

        public SortState HeaderSort
        {
            get { return null; }
            set 
            { 
                if (m_sortImageView != null && value != null)
                {
                    m_sortImageView.SetImageResource(value.IsAscending ? Resource.Drawable.sort_up : Resource.Drawable.sort_down);
                    m_sortImageView.Visibility = value.IsSorting ? ViewStates.Visible : ViewStates.Invisible;
                }
            }
        }

		public ScheduleGridDayHeaderCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

		public override void Inflate(int templateId )
		{
			templateId = Resource.Layout.cell_days_header_all; // hard-coded explicit resource
			TemplateId = templateId;
			AndroidBindingContext.BindingInflate(templateId, this);

			OnFinishInflate();
		}

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_sortImageView = (ImageView)this.FindViewById(Resource.Id.sort_imageView);
			var set = this.CreateBindingSet<ScheduleGridDayHeaderCell, ColumnHeader>();

            set.Bind (this).For(b => b.HeaderSort).To (m => m.Sort);

            set.Apply();
        }
    }
}