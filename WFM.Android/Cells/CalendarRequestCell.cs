﻿using System;
using Consortium.Client.Android;
using Android.Widget;
using Android.Content;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Consortium.Client.Core;
using Android.Graphics;
using Android.Views;
using WFM.Core;
using System.Linq;
using System.Collections.Generic;

namespace WFM.Android
{
    public class CalendarRequestCell : CollectionItemView
    {
        private TextView m_dateLabel;

        private TextView[] m_statusLabels;

        public CalendarRequestCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();

            m_dateLabel = FindViewById<TextView>(Resource.Id.dateLabel);

            m_statusLabels = new TextView[3];

            m_statusLabels[0] = FindViewById<TextView> (Resource.Id.approvedText);
            m_statusLabels[1] = FindViewById<TextView> (Resource.Id.pendingText);
            m_statusLabels[2] = FindViewById<TextView> (Resource.Id.deniedText);

            Click += (object sender, EventArgs e) =>
                {
                    if (CustomCommand1 != null)
                    {
                        ConsortiumPopoverView.SourceView = this;
                        CustomCommand1.Execute (BindingData as CalendarDay);
                    }
                };
        }

        public override void OnShow()
        {
            CalendarDay day = BindingData as CalendarDay;
            if (day != null)
            {
                m_dateLabel.Text = day.DateString;
                m_dateLabel.SetTextColor((day.IsInMonth) ? Color.Black : Color.DarkGray);
                SetBackgroundColor((day.IsInMonth) ? Color.White : Color.LightGray);

                if (day.IsToday)
                {
                    SetBackgroundColor(new Color(255, 244, 191, 255));
                    m_dateLabel.SetTextColor(new Color(161, 148, 87, 255));
                }

                //Hide all labels
                foreach (TextView label in m_statusLabels)
                    label.Visibility = ViewStates.Gone;

                TimeOffDayData dataContext = day.DataContext as TimeOffDayData;
                if (dataContext != null && dataContext.Data != null && dataContext.Data.Count > 0)
                {
                    //Get All Approved, then Pending, then Denied
                    var approved = dataContext.Data.Where(x => x.Status == TimeOffStatus.Approved);
                    var pending = dataContext.Data.Where(x => x.Status == TimeOffStatus.Pending);
                    var denied = dataContext.Data.Where(x => x.Status == TimeOffStatus.Denied);

                    int total = 0;
                    if (approved.Count<JDATimeOffRequest> () > 0)
                        total++;
                    if (pending.Count<JDATimeOffRequest> () > 0)
                        total++;
                    if (denied.Count<JDATimeOffRequest> () > 0)
                        total++;

                    float labelHeight = (MeasuredHeight - m_dateLabel.MeasuredHeight) / total;

                    var combined = new List<IEnumerable<JDATimeOffRequest>> (){ approved, pending, denied };

                    for (int i = 0; i < combined.Count; i++)
                    {
                        TimeOffStatus status = TimeOffStatus.Approved;
                        int count = combined[i].Count<JDATimeOffRequest> ();

                        m_statusLabels[i].Visibility = (count == 0) ? ViewStates.Gone : ViewStates.Visible;

                        switch (i) 
                        {
                            case 0:
                                status = TimeOffStatus.Approved;
                                break;
                            case 1:
                                status = TimeOffStatus.Pending;
                                break;
                            case 2:
                                status = TimeOffStatus.Denied;
                                break;
                        }

                        if (count > 0)
                        {
                            m_statusLabels[i].Text = string.Format ("  {0} {1}", count, status);
                            m_statusLabels[i].SetHeight ((int)labelHeight);
                        }
                        else
                        {
                            m_statusLabels[i].Text = string.Empty;
                            m_statusLabels[i].SetHeight (0);
                        }
                        m_statusLabels [i].Alpha = (day.IsInMonth) ? 1.0f : 0.25f;
                    }
                }
            }
            else
            {
                m_dateLabel.Text = string.Empty;

                foreach (TextView label in m_statusLabels)
                    label.Text = string.Empty;

                SetBackgroundColor(Color.Transparent);
            }
        }
    }
}

