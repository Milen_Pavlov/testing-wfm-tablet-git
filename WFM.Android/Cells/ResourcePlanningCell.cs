﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Consortium.Client.Android;
using Cirrious.MvvmCross.Binding.Droid.Views;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Android.Graphics;

namespace WFM.Android
{
    public class ResourcePlanningCell : CollectionItemView
    {
        private TextView m_overtimeHoursAgreedDay;
        private TextView m_overtimeHoursAgreedNight;
        private TextView m_overtimeHoursRequestedDay;
        private TextView m_overtimeHoursRequestedNight;

        public ResourcePlanningItem.OvertimeField DirtyFields
        {
            get
            {
                return ResourcePlanningItem.OvertimeField.None;
            }

            set
            {
                if (value.HasFlag(ResourcePlanningItem.OvertimeField.OvertimeAgreedDay))
                {
                    m_overtimeHoursAgreedDay.SetTextColor(Color.Red);
                }
                else
                {
                    m_overtimeHoursAgreedDay.SetTextColor(Color.Black);
                }
                if (value.HasFlag(ResourcePlanningItem.OvertimeField.OvertimeAgreedNight))
                {
                    m_overtimeHoursAgreedNight.SetTextColor(Color.Red);
                }
                else
                {
                    m_overtimeHoursAgreedNight.SetTextColor(Color.Black);
                }
                if (value.HasFlag(ResourcePlanningItem.OvertimeField.OvertimeRequestedDay))
                {
                    m_overtimeHoursRequestedDay.SetTextColor(Color.Red);
                }
                else
                {
                    m_overtimeHoursRequestedDay.SetTextColor(Color.Black);
                }
                if (value.HasFlag(ResourcePlanningItem.OvertimeField.OvertimeRequestedNight))
                {
                    m_overtimeHoursRequestedNight.SetTextColor(Color.Red);
                }
                else
                {
                    m_overtimeHoursRequestedNight.SetTextColor(Color.Black);
                }
            }
        }

        public ResourcePlanningCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {

        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_overtimeHoursAgreedDay = (TextView)this.FindViewById(Resource.Id.resourceplanningcell_overtimeagreedday);
            m_overtimeHoursAgreedNight = (TextView)this.FindViewById(Resource.Id.resourceplanningcell_overtimeagreednight);
            m_overtimeHoursRequestedDay = (TextView)this.FindViewById(Resource.Id.resourceplanningcell_overtimerequestedday);
            m_overtimeHoursRequestedNight = (TextView)this.FindViewById(Resource.Id.resourceplanningcell_overtimerequestednight);

            var set = this.CreateBindingSet<ResourcePlanningCell, ResourcePlanningItem>();
            set.Bind(this).For(b => b.DirtyFields).To(m => m.DirtyFields);
            set.Apply();
        }            
            
        public override void OnShow()
        {
            base.OnShow();

            if (DataContext != null)
            {
                ResourcePlanningItem VM = (ResourcePlanningItem)DataContext;

                m_overtimeHoursRequestedDay.Text = VM.OvertimeRequestedDay != null ? VM.OvertimeRequestedDay.Value.ToString() : string.Empty;
                m_overtimeHoursRequestedNight.Text = VM.OvertimeRequestedNight != null ? VM.OvertimeRequestedNight.Value.ToString() : string.Empty;
                m_overtimeHoursAgreedDay.Text = VM.OvertimeAgreedDay != null ? VM.OvertimeAgreedDay.Value.ToString() : string.Empty;
                m_overtimeHoursAgreedNight.Text = VM.OvertimeAgreedNight != null ? VM.OvertimeAgreedNight.Value.ToString() : string.Empty;

                DirtyFields = VM.DirtyFields;
            }
        }
    }
}

