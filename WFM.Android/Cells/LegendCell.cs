﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Consortium.Client.Android;
using WFM.Core;
using WFM.Android.Views.TillAllocations;
using Android.Graphics;
using Cirrious.CrossCore;

namespace WFM.Android
{
    public class LegendCell : CollectionItemView
    {
        private FrameLayout m_swatch;

        private ScheduleGraphLegendType m_legedType;
        public ScheduleGraphLegendType LegendType
        {
            get { return m_legedType; }
            set { m_legedType = value; SetSwatch ();}
        }

        public LegendCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_swatch = (FrameLayout)FindViewById(Resource.Id.cell_swatch);

            var set = this.CreateBindingSet<LegendCell, ScheduleGraphLegendItem>();
            set.Bind (this).For (b => b.LegendType).To (m => m.LegendType);
            set.Apply();
        }

        public override void OnShow()
        {
            base.OnShow();
            SetSwatch ();
        }

        private void SetSwatch ()
        {
            if (m_swatch == null)
                return;
            
            Color c = Color.White;
            switch (LegendType)
            {
                case ScheduleGraphLegendType.Correct:
                    c = Resources.GetColor(Resource.Color.column_correct);
                    break;
                case ScheduleGraphLegendType.Demand:
                    c = Resources.GetColor(Resource.Color.line_demand);
                    break;
                case ScheduleGraphLegendType.Over:
                    c = Resources.GetColor(Resource.Color.column_over);
                    break;
                case ScheduleGraphLegendType.Under:
                    c = Resources.GetColor(Resource.Color.column_under);
                    break;
                default:
                    c = Resources.GetColor(Resource.Color.line_demand);
                    break;
            }

            m_swatch.SetBackgroundColor (c);
        }
    }
}