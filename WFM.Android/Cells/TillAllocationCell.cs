﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Consortium.Client.Android;
using WFM.Core;
using WFM.Android.Views.TillAllocations;
using Android.Graphics;
using Cirrious.CrossCore;

namespace WFM.Android
{
    public class TillAllocationCell : CollectionItemView
    {
        private TillAllocationContentCell m_allocationContentView;

        private AllocationsViewModel.TillModel m_till;
        public AllocationsViewModel.TillModel Till
        {
            get{ return m_till; }
            set
            {
                m_till = value;
                m_allocationContentView.Till = m_till;
            }
        }

        public TillAllocationCell(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            m_allocationContentView = (TillAllocationContentCell)FindViewById(Resource.Id.till_allocations_container);

			var set = this.CreateBindingSet<TillAllocationCell, AllocationsViewModel.TillModel>();
			set.Bind (this).For (b => b.Till).To (m => m);
            set.Apply();
        }

        public override void OnShow()
        {
            base.OnShow();
            Till = Till;
        }
    }
}