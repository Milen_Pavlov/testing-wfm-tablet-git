using System;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;

namespace WFM.Android
{
    public class CurrentTimeControl : View
    {
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }

        // Paints
        private Paint m_redLinePaint;

        // Container
        private int m_width;
        private int m_height;

        public CurrentTimeControl(Context context)
            : base(context)
        {
            Initialise();
        }

        public CurrentTimeControl(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet)
        {
            Initialise();
        }

        public CurrentTimeControl(Context context, IAttributeSet attributeSet, int defStyle)
            : base(context, attributeSet, defStyle)
        {
            Initialise();
        }

        private void Initialise()
        {
            m_redLinePaint = new Paint
            {
                Color = Color.Red,
                AntiAlias = true,
                StrokeWidth = 1
            };
            m_redLinePaint.SetStyle(Paint.Style.Fill);
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            m_width = View.MeasureSpec.GetSize(widthMeasureSpec);
            m_height = View.MeasureSpec.GetSize(heightMeasureSpec);
            SetMeasuredDimension(m_width, m_height);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            var now = DateTime.Now;
            if (now >= MinDate && now <= MaxDate)
            {
                var x = GetX(now);

                var rect = new RectF();
                rect.Left = (int)x;
                rect.Top = (int)0;
                rect.Right = rect.Left + 1.0f;
                rect.Bottom = m_height;
                WFM.Android.Utilities.Helpers.drawRect(canvas, m_redLinePaint, rect);
            }
        }

        private double GetX(DateTime date)
        {
            return m_width * (date.Ticks - MinDate.Ticks) / (MaxDate.Ticks - MinDate.Ticks);
        }
    }
}