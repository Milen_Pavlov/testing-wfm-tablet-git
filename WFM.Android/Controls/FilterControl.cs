﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;
using Consortium.Client.Android;
using Java.Interop;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.Android
{
    public class FilterControl : PopupWindow, AdapterView.IOnItemClickListener
    {
        private Context m_context;
        private View m_filterGroupView;
        private ListView m_groupListView;
        private View m_filterItemView;
        private ListView m_itemListView;
        private TextView m_itemListTitle;
        private FilterItemAdapter m_itemAdapter;
        private FilterGroupAdapter m_groupAdapter;
        private IAnalyticsService m_analytics;

        private CheckBox m_scheduledFilterCheckbox;
        private CheckBox m_unscheduledFilterCheckbox;

        private int m_cachedItemScrollPosition = 0;

        public ICommand Apply { get; set; }

        private FilterGroup m_currentFilter;
        public FilterGroup CurrentFilter 
        { 
            get 
            { 
                return m_currentFilter; 
            } 
            set
            {
                m_currentFilter = value;
            }
        }

        private List<FilterGroup> m_filters;
        public List<FilterGroup> Filters 
        {
            get 
            { 
                return m_filters; 
            }
            set 
            {
                m_filters = value;
            }
        }

        public FilterControl (Context context) : base (context)
        {
            m_context = context;
            Initialize (null, 0);
        }

        public FilterControl (Context context, IAttributeSet attrs) : base (context, attrs)
        {
            m_context = context;
            Initialize (attrs, 0);
        }

        public FilterControl (Context context, IAttributeSet attrs, int defStyle) : base (context, attrs, defStyle)
        {
            m_context = context;
            Initialize (attrs, defStyle);
        }             

        void Initialize(IAttributeSet attrs, int defStyle)
        {            
            this.Width = this.m_context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_FilterPopup_Width);
            this.Height = this.m_context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_FilterPopup_Height);

            this.SetBackgroundDrawable(new BitmapDrawable());

            this.Touchable = true;
            this.Focusable = true;
            this.OutsideTouchable = true;

            LayoutInflater inflater = (LayoutInflater)m_context.GetSystemService(Context.LayoutInflaterService);

            m_filterGroupView = inflater.Inflate(Resource.Layout.control_filtergroup_selection,null);
            m_filterItemView = inflater.Inflate(Resource.Layout.control_filter_selection,null);

            var clearButton = (Button)m_filterGroupView.FindViewById(Resource.Id.filter_selection_clear);
            clearButton.Click += (object sender, EventArgs e) => { Apply.Execute(null); Dismiss(); };

            var applyButton = (Button)m_filterItemView.FindViewById(Resource.Id.filter_selection_apply);
            applyButton.Click += (object sender, EventArgs e) => { Apply.Execute(CurrentFilter); Dismiss(); };

            var selectAllButton = (Button)m_filterItemView.FindViewById(Resource.Id.filter_selection_selectall);
            selectAllButton.Click += (object sender, EventArgs e) => SelectAll();

            var selectNoneButton = (Button)m_filterItemView.FindViewById(Resource.Id.filter_selection_selectnone);
            selectNoneButton.Click += (object sender, EventArgs e) => SelectNone();

            var backButton = (ImageButton)m_filterItemView.FindViewById(Resource.Id.filter_selection_back);
            backButton.Click += (object sender, EventArgs e) => SwitchToFirstView();

            m_itemListTitle = (TextView)m_filterItemView.FindViewById(Resource.Id.filter_selection_title);

            m_scheduledFilterCheckbox = (CheckBox)m_filterItemView.FindViewById(Resource.Id.filter_selection_schedcheckbox);
            m_unscheduledFilterCheckbox = (CheckBox)m_filterItemView.FindViewById(Resource.Id.filter_selection_unschedcheckbox);

            m_scheduledFilterCheckbox.CheckedChange += ScheduledChecked;
            m_unscheduledFilterCheckbox.CheckedChange += UnscheduledChecked;

            m_groupAdapter = new FilterGroupAdapter(this.m_context, Android.Resource.Layout.cell_filterentry);
            m_groupListView = (ListView)m_filterGroupView.FindViewById(Resource.Id.filter_selection_list);
            m_groupListView.OnItemClickListener = this;
            m_groupListView.Adapter = m_groupAdapter;

            m_itemAdapter = new FilterItemAdapter(this.m_context, Android.Resource.Layout.cell_filterentry);
            m_itemListView = (ListView)m_filterItemView.FindViewById(Resource.Id.filter_selection_list);
            m_itemListView.OnItemClickListener = this;
            m_itemListView.Adapter = m_itemAdapter;

            m_analytics = Mvx.Resolve<IAnalyticsService> ();
        }

        void ScheduledChecked (object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            CurrentFilter.MemoryFilter.ScheduledSelected = e.IsChecked;

            if(e.IsChecked)
                m_analytics.TrackAction ("Schedule Filter", "Filter On Scheduled");

        }

        void UnscheduledChecked (object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            
            CurrentFilter.MemoryFilter.UnscheduledSelected = e.IsChecked;   

            if(e.IsChecked)
                m_analytics.TrackAction ("Schedule Filter", "Filter On Unscheduled");
        }

        public void Show(View fromView, int fromX, int fromY)
        {
            m_analytics.TrackAction ("Schedule Filter", "Show");

            SetCurrentSelection(CurrentFilter,Filters);

            if (Filters != null)
            {
                if (CurrentFilter == null)
                {
                    this.ContentView = m_filterGroupView;

                    m_itemAdapter.Clear();

                    SwitchToFirstView();

                    m_cachedItemScrollPosition = 0;
                }
                else
                {
                    this.ContentView = m_filterItemView;

                    SwitchToSecondView();

                    m_itemListView.ScrollTo(0, m_cachedItemScrollPosition);
                }

                this.ShowAsDropDown(fromView, fromX, fromY);
            }
        }

        private void SetCurrentSelection(FilterGroup filter, List<FilterGroup> list)
        {
            SetupScheduledFilter(filter);

            if (list != null)
            {
                foreach (var g in list)
                {
                    if (filter != null && g.Type == filter.Type)
                    {
                        // Make a copy
                        g.Items = filter.Clone ().Items;
                    }
                    else
                    {
                        foreach (var i in g.Items)
                            i.Selected = false;
                    }
                }

                m_itemAdapter.Reload();
            }
        }

        void SetupScheduledFilter(FilterGroup filter)
        {
            if (filter != null && filter.MemoryFilter != null)
            {
                var scheduledFilter = (ScheduledFilterGroup)filter.MemoryFilter;
                m_scheduledFilterCheckbox.Checked = scheduledFilter.ScheduledSelected;
                m_unscheduledFilterCheckbox.Checked = scheduledFilter.UnscheduledSelected;
            }
        }

        void SwitchToFirstView()
        {
            m_groupAdapter.LoadData(Filters);

            ViewGroup parent = (ViewGroup)m_filterItemView.Parent;
            if (parent != null)
            {
                parent.RemoveView(m_filterItemView);
                parent.AddView(m_filterGroupView);
            }
        }

        void SwitchToSecondView()
        {
            SetupScheduledFilter(CurrentFilter);
            m_itemAdapter.LoadData(CurrentFilter.Items);
            m_itemListView.InvalidateViews();

            m_itemListTitle.Text = CurrentFilter.Name;

            ViewGroup parent = (ViewGroup)m_filterGroupView.Parent;

            if (parent != null)
            {
                m_groupAdapter.Clear();
                parent.RemoveView(m_filterGroupView);
                parent.AddView(m_filterItemView);
            }
        }

        public void OnItemClick(AdapterView parent, View view, int position, long id)
        {
            var list = (ListView)parent;

            if (list.Adapter == m_itemAdapter)
            {
				if(this.CurrentFilter != null)
				{
					this.CurrentFilter.Items[(int)id].Selected = !this.CurrentFilter.Items[(int)id].Selected;

                    if(this.CurrentFilter.Items[(int)id].Selected)
                        m_analytics.TrackAction ("Schedule Filter", "Filter on " +this.CurrentFilter.Name , this.CurrentFilter.Items[(int)id].Name);
				}                    

                this.m_itemAdapter.Reload();
            }
            else if(list.Adapter == m_groupAdapter)
            {
                m_analytics.TrackAction ("Schedule Filter", "Select Group", this.Filters[(int)id].Name);

                this.CurrentFilter = this.Filters[(int)id];
                SwitchToSecondView();
            }
        }

        void SelectAll()
        {
            m_analytics.TrackAction ("Schedule Filter", "Select All");

            foreach (var filteritem in CurrentFilter.Items)
            {
                filteritem.Selected = true;
            }

            m_itemAdapter.Reload();
        }

        void SelectNone()
        {
            m_analytics.TrackAction ("Schedule Filter", "Select None");

            foreach (var filteritem in CurrentFilter.Items)
            {
                filteritem.Selected = false;
            }

            m_itemAdapter.Reload();
        }

        public override void Dismiss()
        {
            ViewGroup parent = (ViewGroup)m_filterItemView.Parent;
            if (parent != null)
            {
                m_cachedItemScrollPosition = m_itemListView.ScrollY;
                parent.RemoveView(m_filterItemView);
            }

            parent = (ViewGroup)m_filterGroupView.Parent;
            if (parent != null)
            {
                parent.RemoveView(m_filterGroupView);
            }

            base.Dismiss();
        }

        class FilterGroupAdapter : ArrayAdapter<FilterGroup>
        {
            private LayoutInflater m_layoutInflater;
            private List<FilterGroup> m_filters;

            public  FilterGroupAdapter(Context context, int cellId)
                : base(context,cellId,new List<FilterGroup>())
            {
                m_layoutInflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                TextView text;

                if (convertView == null) 
                {
                    convertView = m_layoutInflater.Inflate(Resource.Layout.cell_filterentry, null);
                }

                text = (TextView)convertView.FindViewById(Resource.Id.filterentry_name);


                text.Text = (m_filters[position].Name);

                return convertView;
            }

            public void LoadData(List<FilterGroup> filters)
            {
                this.Clear();
                this.AddAll(filters);

                this.m_filters = filters;
            }
        }

        class FilterItemAdapter : ArrayAdapter<FilterItem>
        {
            private LayoutInflater m_layoutInflater;
            private List<FilterItem> m_filters;

            public FilterItemAdapter(Context context, int cellId)
                : base(context,cellId,new List<FilterItem>())
            {
                m_layoutInflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);
            }

            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                TextView text;
                View selectedCheckbox;

                if (convertView == null) 
                {
                    convertView = m_layoutInflater.Inflate(Resource.Layout.cell_filterentry, null);
                }

                text = (TextView)convertView.FindViewById(Resource.Id.filterentry_name);
                selectedCheckbox = (View)convertView.FindViewById(Resource.Id.filterentry_selected);

                text.Text = (m_filters[position].Name);
                selectedCheckbox.Visibility = m_filters[position].Selected ? ViewStates.Visible : ViewStates.Invisible;

                return convertView;
            }

            public void LoadData(List<FilterItem> filters)
            {
                this.Clear();
                this.AddAll(filters);

                this.m_filters = filters;
            }

            public void Reload()
            {
                this.NotifyDataSetChanged();
            }
        }
    }
}