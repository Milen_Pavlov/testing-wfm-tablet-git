﻿using System;
using Consortium.Client.Android;
using System.Collections.Generic;
using Android.Util;
using WFM.Core.NightAisle;
using Android.Views;
using Android.OS;
using Android.Widget;

namespace WFM.Android
{
    public class DraggableAisleCollectionControl : ScrollableDraggableCollectionControl
    {
        public EventHandler<AisleOrderChangedEventArgs> OnAisleOrderChanged;

        private List<int> m_draggedAisleOrder;
        public List<int> DraggedAisleOrder
        {
            get { return m_draggedAisleOrder; }
            set
            {
                m_draggedAisleOrder = value;
                if(OnAisleOrderChanged != null)
                    OnAisleOrderChanged.Invoke(this, new AisleOrderChangedEventArgs(value));
            }
        }

        public DraggableAisleCollectionControl(global::Android.Content.Context context)
            : base(context)
        {
            Initialize (null, 0);
        }

        public DraggableAisleCollectionControl(global::Android.Content.Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize (attrs, 0);
        }

        public DraggableAisleCollectionControl (global::Android.Content.Context context, IAttributeSet attrs, int defStyle) 
            : base (context, attrs, defStyle)
        {
            Initialize (attrs, defStyle);
        }

        public new void Initialize(IAttributeSet attrs, int defStyle)
        {
            base.Initialize(attrs, defStyle);
        }

        protected override void OnDragCompleted()
        {
            List<int> aisleIDs = new List<int>();
            foreach (var item in Items)
            {
                Aisle aisleModel = item as Aisle;
                if (aisleModel == null)
                    continue;

                aisleIDs.Add(aisleModel.ID);
            }

            DraggedAisleOrder = aisleIDs;
        }
    }
}