﻿using System;
using Consortium.Client.Android;
using Android.Util;
using Android.Content;
using Android.Views;
using System.Windows.Input;

namespace WFM.Android
{
	public class PinchableCollectionControl : DragCollectionControl, ScaleGestureDetector.IOnScaleGestureListener, GestureDetector.IOnGestureListener
    {
        //Controls behaviour of scrolling
        bool m_pinching = false;
        bool horizontalScroll = false;
        bool verticalScroll = false;

        private float m_scaleFactor = 1.0f;
        public float ScaleFactor
        {
            get { return m_scaleFactor; }
            private set
            {
                if (m_scaleFactor != value)
                {
                    m_scaleFactor = value;
                }
            }
        }

        private float m_scaleFactorMin = 0.05f;
        public float ScaleFactorMin
        {
            get { return m_scaleFactorMin; }
            set
            {
                ScaleFactorMin = value;
                if (ScaleFactor < ScaleFactorMin)
                {
                    ScaleFactor = ScaleFactorMin;
                }
            }
        }

        private float m_scaleFactorMax = 1.0f;
        public float ScaleFactorMax
        {
            get { return m_scaleFactorMax; }
            set
            {
                ScaleFactorMax = value;
                if (ScaleFactor > ScaleFactorMax)
                {
                    ScaleFactor = ScaleFactorMax;
                }
            }
        }

        public Action<float> OnScaleChanged
        {
            get;
            set;
        }      


        public Action<float> OnXScroll
        {
            get;
            set;
        }

        public Action OnTouchUp
        {
            get;
            set;
        }

        private ScaleGestureDetector m_scaleDetector;
        private GestureDetector m_gestureDetector;

        public PinchableCollectionControl(global::Android.Content.Context context)
            : base(context)
        {
            Initialize (context, null, 0);
        }

        public PinchableCollectionControl(global::Android.Content.Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize (context, attrs, 0);
        }

        public PinchableCollectionControl (global::Android.Content.Context context, IAttributeSet attrs, int defStyle) 
            : base (context, attrs, defStyle)
        {
            Initialize (context, attrs, defStyle);
        }

        public void Initialize(Context context, IAttributeSet attrs, int defStyle)
        {
            base.Initialize(attrs, defStyle);
            m_scaleDetector = new ScaleGestureDetector(context,this);
            m_gestureDetector = new GestureDetector(context, this);

        }

        public bool OnScale(ScaleGestureDetector detector)
        {
            ScaleFactor /= detector.ScaleFactor;

            // Put a limit on how small or big the scaleFactor can get.
            // Note that a smaller ScaleFactor means more "zoomed-in", so a bigger visual stretch
            if (ScaleFactor > ScaleFactorMax)
            {
                ScaleFactor = ScaleFactorMax;
            }
            else if (ScaleFactor < ScaleFactorMin)
            {
                ScaleFactor = ScaleFactorMin;
            }

            if (OnScaleChanged != null)
                OnScaleChanged(ScaleFactor);

            return true;
        }

        public bool OnScaleBegin(ScaleGestureDetector detector)
        {
            m_pinching = true;
            return true;
        }

        public void OnScaleEnd(ScaleGestureDetector detector)
        {

        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (e.Action == MotionEventActions.Up)
            {
                m_pinching = false;

                horizontalScroll = false;
                verticalScroll = false;

                if(OnTouchUp != null)
                    OnTouchUp();
            }

            m_scaleDetector.OnTouchEvent(e);

            if (e.PointerCount == 1)
            {
                bool gestureDetection = m_gestureDetector.OnTouchEvent(e);

                if (!gestureDetection)
                    return base.OnTouchEvent(e);
                else
                    return true;
            }
            else
                return true;
        }

        public bool OnDown(MotionEvent e)
        {
            return false;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            return false;
        }

        public void OnLongPress(MotionEvent e)
        {
        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            if (m_pinching)
                return true;
            else
            {
                if (horizontalScroll)
                {
                    OnXScroll(distanceX);
                    return true;
                }
                else if (verticalScroll)
                    return false;
                else
                {
                    if (Math.Abs(distanceX) > Math.Abs(distanceY))
                    {
                        if (OnXScroll != null)
                            OnXScroll(distanceX);

                        horizontalScroll = true;

                        return true;
                    }
                    else
                    {
                        verticalScroll = true;
                        return false;
                    }
                } 
            }
        }

        public void OnShowPress(MotionEvent e)
        {

        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            return false;
        }
    }
}