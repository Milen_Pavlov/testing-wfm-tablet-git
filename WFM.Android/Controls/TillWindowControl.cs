using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Android;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using WFM.Core;
using WFM.Android.Constants;

namespace WFM.Android
{
    public class TillWindowControl : View
    {
        public AllocationsViewModel.TillWindowModel Model { get; set; }

        // Drawables
        BitmapDrawable m_windowBitmapDrawable;

        // Container
        private int m_width;
        private int m_height;
        private RectF m_dayFrame;

        public TillWindowControl(Context context)
            : base(context)
        {
            Initialise();
        }

        public TillWindowControl(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet)
        {
            Initialise();
        }

        public TillWindowControl(Context context, IAttributeSet attributeSet, int defStyle)
            : base(context, attributeSet, defStyle)
        {
            Initialise();
        }

        private void Initialise()
        {
            CreateResources();
        }

        private void CreateResources()
        {
            m_windowBitmapDrawable = CreateTileableBitmapDrawable(Resource.Drawable.hatch_green);
        }

        private BitmapDrawable CreateTileableBitmapDrawable(int resID)
        {
            Bitmap bmp = BitmapFactory.DecodeResource(Application.Context.Resources, resID);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);
            bitmapDrawable.SetTileModeXY(Shader.TileMode.Repeat, Shader.TileMode.Repeat);
            return bitmapDrawable;
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            m_width = View.MeasureSpec.GetSize(widthMeasureSpec);
            m_height = View.MeasureSpec.GetSize(heightMeasureSpec);
            m_dayFrame = new RectF(0, 0, m_width, m_height);
            SetMeasuredDimension(m_width, m_height);
        }

        public void OnHoursUpdated()
        {
            Invalidate();
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            if (Model != null)
            {
                var now = DateTime.Now;
                var rect = GetRectangle();

                if (RectangleWidthIsZero(rect))
                {
                    return;
                }

                m_windowBitmapDrawable.SetBounds((int)rect.Left, (int)rect.Top, (int)rect.Right, (int)rect.Bottom);
                m_windowBitmapDrawable.Draw(canvas);
            }
        }

        private float GetX(float hours) // 0.0 -> 24.0
        {
            if (Model.Hours != null)
                return ((hours - Model.Hours.Item1) / (Model.Hours.Item2 - Model.Hours.Item1)) * m_width;
            else
                return 0;
        }

        private RectF GetRectangle()
        {
            var top = 0;
            var bottom = m_dayFrame.Height();
            var left = GetX((float)Model.StartTime.TotalHours % ScheduleTime.HoursPerDay);
            var right = GetX((float)Model.EndTime.TotalHours % ScheduleTime.HoursPerDay);
            return new RectF(left, top, right, bottom);
        }

        private static bool RectangleWidthIsZero(RectF rect)
        {
            return ((int)rect.Left == (int)rect.Right);
        }
    }
}
