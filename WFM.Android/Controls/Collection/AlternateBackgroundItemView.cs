using System;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Content;
using Consortium.Client.Core;
using Android.Graphics;
using Android.Graphics.Drawables;
using System.Windows.Input;
using Android.Views;

namespace Consortium.Client.Android
{
    public class AlternateBackgroundItemView : CollectionItemView
    {
        private static Color m_oddCol = new Color (232, 232, 255, 255);
        private static Color m_evenCol = Color.White;

        public virtual Color OddCol { get { return m_oddCol; } }
        public virtual Color EvenCol { get { return m_evenCol; } }

        private bool m_isOdd = false;
        public bool IsOdd 
        {
            get { return m_isOdd; }
            set { m_isOdd = value; UpdateBackgroundColor (); }
        }

        public virtual View BackgroundView
        {
            get;
        }

        public AlternateBackgroundItemView(Context context, IMvxLayoutInflater layoutInflater, object dataContext)
            : base(context, layoutInflater, dataContext)
        {
        }

        public override void OnShow()
        {
            UpdateBackgroundColor ();
        }

        protected void UpdateBackgroundColor()
        {
            if (BackgroundView != null)
                BackgroundView.SetBackgroundColor (IsOdd ? OddCol : EvenCol);
            else
                SetBackgroundColor (IsOdd ? OddCol : EvenCol);
        }
    }
}
