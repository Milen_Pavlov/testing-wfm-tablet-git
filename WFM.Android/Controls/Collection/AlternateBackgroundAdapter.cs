﻿using Android.Content;
using Android.Graphics.Drawables;
using Cirrious.MvvmCross.Binding.Droid.BindingContext;
using Consortium.Client.Core;
using System.Windows.Input;
using Android.Views;

namespace Consortium.Client.Android
{
    public class AlternateBackgroundAdapter : CollectionAdapter
    {

        public AlternateBackgroundAdapter(Context context)
            : base(context, MvxAndroidBindingContextHelpers.Current())
        {
        }

        public AlternateBackgroundAdapter(Context context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext)
        {
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View cell = base.GetView (position, convertView, parent);

            AlternateBackgroundItemView viewToUse = cell as AlternateBackgroundItemView;
            if (viewToUse != null)
                viewToUse.IsOdd = position % 2 != 0;

            return cell;
        }
    }
}