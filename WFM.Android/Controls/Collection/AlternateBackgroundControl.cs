﻿using Consortium.Client.Android;
using Android.Util;
using Android.Views;
using Android.OS;
using Android.Widget;
using Android.Views.InputMethods;
using Android.Content.Res;
using Android.Graphics;

namespace WFM.Android
{	
    public class AlternateBackgroundControl : CollectionControl
    {

        public AlternateBackgroundControl(global::Android.Content.Context context) : 
            base(context)
        {
            Initialize (null, 0);
        }

        public AlternateBackgroundControl(global::Android.Content.Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize (attrs, 0);
        }

        public AlternateBackgroundControl (global::Android.Content.Context context, IAttributeSet attrs, int defStyle) 
            : base (context, attrs, defStyle)
        {
            Initialize (attrs, defStyle);
        }

        public void Initialize (IAttributeSet attrs, int defStyle)
        {
            ItemAdapter = new AlternateBackgroundAdapter(Context);

            // Clear the selector (do not use!!)
            SetSelector (global::Android.Resource.Color.Transparent);

            // Configure with custom attributes (this is done in base but we've just overwritten the ItemAdapter so grab it again)
            TypedArray a = Context.Theme.ObtainStyledAttributes(attrs, Resource.Styleable.CollectionControl, defStyle, 0);
            ItemAdapter.ItemTemplateId = a.GetResourceId(Resource.Styleable.CollectionControl_itemLayoutId,0);
            ItemAdapter.SectionTemplateId = a.GetResourceId(Resource.Styleable.CollectionControl_sectionLayoutId,0);
            ItemAdapter.HeaderTemplateId = a.GetResourceId(Resource.Styleable.CollectionControl_headerLayoutId,0);
            ItemAdapter.FooterTemplateId = a.GetResourceId(Resource.Styleable.CollectionControl_footerLayoutId,0);
            ItemAdapter.SelectedColor = a.GetColor(Resource.Styleable.CollectionControl_selectedColor,Color.LightSkyBlue);
            ItemAdapter.HasHeader = a.GetBoolean(Resource.Styleable.CollectionControl_hasHeader,false);
            ItemAdapter.HasFooter = a.GetBoolean(Resource.Styleable.CollectionControl_hasFooter,false);
            AllowsSelection = a.GetBoolean(Resource.Styleable.CollectionControl_allowsSelection, true);
        }

    }
}

