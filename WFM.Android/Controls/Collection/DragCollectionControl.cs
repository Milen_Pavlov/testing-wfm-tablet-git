﻿using Consortium.Client.Android;
using Android.Util;
using Android.Views;
using Android.OS;
using Android.Widget;
using Android.Views.InputMethods;

namespace WFM.Android
{	
    public class DragCollectionControl : CollectionControl
    {
        internal class ScrollCountDownTimer : CountDownTimer
        {
            public int m_scrollSpeed {get;set;}
            private GridView m_view;
            private const int c_interval = 50;
            private const long millisInFuture = long.MaxValue; // Scroll until timer cancelled

            public ScrollCountDownTimer(GridView view, int scrollSpeed) : base(millisInFuture, c_interval)
            {
                m_scrollSpeed = scrollSpeed;
                m_view = view;
            }

            public override void OnTick(long millisUntilFinished)
            {
                m_view.SmoothScrollBy(m_scrollSpeed, c_interval);
            }

            public override void OnFinish()
            {
            }
        }

        private ScrollCountDownTimer m_timer;
        private bool m_scrollingDown = false;
        private bool m_scrollingUp = false;
        private const int c_scrollSpeed = 25;

        public bool AutoScrollWhenDragging { get; set; } = true;

        public DragCollectionControl(global::Android.Content.Context context) : 
            base(context)
        {
        }

        public DragCollectionControl(global::Android.Content.Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
        }

        public DragCollectionControl (global::Android.Content.Context context, IAttributeSet attrs, int defStyle) 
            : base (context, attrs, defStyle)
        {
        }

        protected override void OnFinishInflate ()
        {
            base.OnFinishInflate ();
        }

        public override void OnDrag(object sender, DragEventArgs args)
        {
            var delta = LastVisiblePosition - FirstVisiblePosition;

            if (delta != 0)
            {
                float cellHeight = Height / delta;

                if (args.Event.Action != DragAction.Drop && args.Event.Action != DragAction.Exited)
                {
                    float yPos = args.Event.GetY();
                    if (yPos > Height - cellHeight) // Drag position over bottom row
                    {
                        StartScrollingDown ();
                    }
                    else if (yPos < cellHeight) // Drag position over top row
                    {
                        StartScrollingUp ();
                    }
                    else if (m_timer != null)
                    {
                        StopScrolling ();
                    }
                }
                else
                {
                    if (m_timer != null)
                    {
                        StopScrolling ();
                    }
                }
            }
        }
            
        public void StartScrollingDown()
        {
            if (m_scrollingDown)
                return;

            if (m_timer != null)
            {
                m_timer.Cancel();
                m_timer = null;
            }
            m_timer = new ScrollCountDownTimer(this, c_scrollSpeed);
            m_scrollingDown = true;
            m_scrollingUp = false;
            m_timer.Start();
        }

        public void StartScrollingUp()
        {
            if (m_scrollingUp)
                return;
            
            if (m_timer != null)
            {
                m_timer.Cancel();
                m_timer = null;
            }
            m_timer = new ScrollCountDownTimer(this, -c_scrollSpeed);
            m_scrollingUp = true;
            m_scrollingDown = false;
            m_timer.Start();
        }

        public void StopScrolling()
        {
            if (!m_scrollingDown && !m_scrollingUp)
                return;
            
            m_timer.Cancel ();
            m_scrollingDown = false;
            m_scrollingUp = false;
            m_timer = null;
        }
    }
}

