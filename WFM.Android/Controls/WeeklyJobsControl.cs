﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Consortium.Client.Android;
using WFM.Core;
using Android.Graphics;
using Android.Opengl;
using System.Runtime.CompilerServices;
using WFM.Android;

namespace WFM.Droid.Controls
{
    public class WeeklyJobsControl : FrameLayout
    {
        public Action<ShiftData> ShiftSelect { get; set; }
        public Action<View, ShiftData, Point> ShiftReassign { get; set; }
        public Action<TimeOffData> TimeOffSelect { get; set; }
        public const int c_scrollThreshold = 5;

        private ShiftData m_shift;
        private JobData m_job;
        private EmployeeData m_employee;
        private Color m_backgroundColor = Color.White;

        private int m_brightnessThreshold = 0;
        private bool m_hasMoved = false;
        private PointF m_touchStartPos;

        public bool CanEdit { get { return m_employee == null ? false : m_employee.CanEdit; } }

        public JobData Job 
        { 
            get { return m_job; }
            set
            {
                m_job = value;

                m_nameTextView.Text = m_job.JobName;

                Color backgroundColour = ColorUtility.FromHexString(m_job.JobColor);
                if (m_employee != null && m_employee.SpareShift)
                {
                    backgroundColour.R = (byte)(Math.Min(backgroundColour.R + 50, 255));
                    backgroundColour.G = (byte)(Math.Min(backgroundColour.G + 50, 255));
                    backgroundColour.B = (byte)(Math.Min(backgroundColour.B + 50, 255));
                }
                m_nameTextView.SetBackgroundColor(backgroundColour);
                m_nameTextView.SetTextColor(WFM.Android.Utilities.Helpers.GetTotalRGBColorValue(backgroundColour) > m_brightnessThreshold ? Color.Black : Color.White);

            } 
        }

        public Color TimeBackgroundColor
        {
            set
            {
                m_timeTextView.Text = m_job.JobName != "Time Off" ? m_job.StartEnd : string.Empty;
                Color backgroundColour = m_backgroundColor;
                if (m_employee != null && m_employee.SpareShift)
                {
                    backgroundColour = Color.LightGray;
                }
                m_timeTextView.SetBackgroundColor (backgroundColour);
            }
        }

        public ShiftData Shift
        {
            set
            {
                m_shift = value;
            }
        }

        public EmployeeData Employee
        {
            set
            {
                m_employee = value;
            }
        }

        private TextView m_timeTextView;
        private TextView m_nameTextView;

        public WeeklyJobsControl(Context context) 
            : base (context) 
        {
            var LayoutId = WFM.Android.Resource.Layout.control_weekly_job;
            Inflate(Context, LayoutId, this);

            Initialise();
        }   

        public WeeklyJobsControl(Context context, IAttributeSet attributeSet)
            : this(context, attributeSet,WFM.Android.Resource.Layout.control_weekly_job)
        {
        }

        public WeeklyJobsControl(Context context, IAttributeSet attributeSet, int layoutId)
            : base(context, attributeSet,layoutId)
        {
            Inflate(Context, layoutId, this);
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            Initialise();
        }

        private void Initialise() 
        {
            m_brightnessThreshold = WFM.Android.Utilities.Helpers.ConvertPercentageBrightnessToRGBTotalValue(Resources.GetInteger(WFM.Android.Resource.Integer.textColorThreshold));

            m_nameTextView = (TextView)this.FindViewById(WFM.Android.Resource.Id.job_name_textView);
            m_timeTextView = (TextView)this.FindViewById(WFM.Android.Resource.Id.job_time_textView);
        } 

        public void OnClick()
        {
            if (ShiftSelect != null && m_shift != null)
            {
                ShiftSelect(m_shift);
            }

            if (TimeOffSelect != null && m_job.IsTimeOff == true)
            {
                TimeOffSelect ( new TimeOffData() { Employee = m_employee, Day = m_job.Start } );
            }
        }

        public override bool DispatchTouchEvent(MotionEvent e) 
        {
            // Preview the touch event to detect a swipe: switch (e.ActionMasked) 
            switch (e.Action)
            {
                case MotionEventActions.Down:
                    m_hasMoved = false;
                    m_touchStartPos = new PointF(e.GetX (), e.GetY ());
                    return true; // Consume the touch! Continue showing interest
                case MotionEventActions.Up: 
                    if (m_hasMoved == false)
                    {
                        OnClick ();
                    }
                    break;
                case MotionEventActions.Cancel:
                    break;
                case MotionEventActions.Move:
                    // Make sure that slight movements do not swallow a simple tap
                    if (!m_hasMoved && (Math.Abs(m_touchStartPos.X - e.GetX()) > c_scrollThreshold || Math.Abs(m_touchStartPos.Y - e.GetY()) > c_scrollThreshold))
                    {
                        m_hasMoved = true;
                    }

                    if (m_hasMoved)
                    {
                        if (CanEdit && ShiftReassign != null)
                        {
                            ShiftReassign (this, m_shift, new Point ((int)Math.Round (e.GetX ()), (int)Math.Round (e.GetY ())));
                        }
                    }
                        
                    return true;
            }

            return base.DispatchTouchEvent(e);
        }

        public override bool OnTouchEvent(MotionEvent e) 
        {
            // To make sure to receive touch events, tell parent we are handling them: return true;
            return true;
        }

        public void UpdateCanAcceptShift(bool canAccept)
        {
            if (canAccept == false)
            {
                m_timeTextView.SetBackgroundColor (Color.Gray);
                m_nameTextView.SetBackgroundColor (Color.DarkGray);
            }
            else
            {
                m_timeTextView.SetBackgroundColor (m_backgroundColor);
                Color backgroundColour = Color.White;
                if (m_job != null)
                {
                    backgroundColour = ColorUtility.FromHexString (m_job.JobColor);
                    if (m_employee != null && m_employee.SpareShift)
                    {
                        backgroundColour.R = (byte)(Math.Min (backgroundColour.R + 50, 255));
                        backgroundColour.G = (byte)(Math.Min (backgroundColour.G + 50, 255));
                        backgroundColour.B = (byte)(Math.Min (backgroundColour.B + 50, 255));
                    }
                }
                m_nameTextView.SetBackgroundColor(backgroundColour);
            }
        }
    }
}