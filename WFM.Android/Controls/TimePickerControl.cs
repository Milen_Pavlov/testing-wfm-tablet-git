using System;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace WFM.Android
{
    public class TimePickerControl : FrameLayout, TimePickerDialog.IOnTimeSetListener
    {
        private class CustomTimePicker : TimePickerDialog
        {
            public CustomTimePicker(Context context, TimePickerDialog.IOnTimeSetListener listener, int hour, int mins) 
                : base (context, listener, hour, mins, true)
            {
            }

            public override void OnAttachedToWindow()
            {
                base.OnAttachedToWindow ();

                var frame = this.FindViewById(global::Android.Resource.Id.Content) as FrameLayout;
                var numberPicker = FindRecursive(frame);
                numberPicker.SetDisplayedValues(new string[] { "0", "15", "30", "45" });
            }

            private NumberPicker FindRecursive(ViewGroup v)
            {
                for (int i = 0; i < v.ChildCount; ++i)
                {
                    var f = v.GetChildAt (i);

                    if (f is NumberPicker)
                        return f as NumberPicker;

                    if (f is ViewGroup)
                    {
                        var r = FindRecursive (f as ViewGroup);

                        if (r != null)
                            return r;
                    }
                }

                return null;
            }
        }

        private TimeSpan m_value;
        private TextView m_input;
        private View m_view;

        public ICommand Changed { get; set; }

        public TimeSpan Value 
        { 
            get { return m_value; }
            set 
            { 
                m_value = value; 
                m_input.Text = m_value.ToString(@"hh\:mm");
            } 
        }
            
        public TimePickerControl(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet)
        {
            m_view = Inflate(Context, Resource.Layout.view_time_picker, this);
            m_input = (TextView)m_view.FindViewById(Resource.Id.time);

            m_input.Click += (sender, e) =>
            {
                if (Enabled)
                {
                    var dialog = new CustomTimePicker(Context, this, Value.Hours, Value.Minutes);
                    dialog.Show();
                }
            };
        }

        public void OnTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            Value = new TimeSpan(hourOfDay, minute, 0);

            if (Changed != null)
                Changed.Execute (Value);
        }
    }
}