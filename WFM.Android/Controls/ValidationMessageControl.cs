using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using WFM.Android;

namespace WFM.Droid.Controls
{
    public class ValidationMessageControl : FrameLayout
    {
        public ViewGroup ContainerView { get; set; }
        public TextView ErrorMessageView { get; set; }

        public ValidationMessageControl(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet)
        {
            Inflate(Context, Resource.Layout.validation_message, this);
            ContainerView = (ViewGroup)FindViewById(Resource.Id.container);
            ErrorMessageView = (TextView)FindViewById(Resource.Id.error_message);
        }

        public string Text
        {
            get
            {
                return (ContainerView.Visibility == ViewStates.Visible ? ErrorMessageView.Text : null);
            }
            set
            {
                ContainerView.Visibility = (string.IsNullOrEmpty(value) ? ViewStates.Gone : ViewStates.Visible);
                ErrorMessageView.Text = value;
            }
        }
    }
}