﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.Util;
using Android.Views;
using Consortium.Client.Android;
using WFM.Core;
using Java.Lang;
using System.Collections.Generic;
using Android.OS;
using Cirrious.CrossCore;
using Android.App;

namespace WFM.Android
{
    public class PlanningControl : View
    {
        private int m_rowHeight = 45;

        private Paint m_underPaint;
        private Paint m_overPaint;
        private Paint m_gridPaint;

        private int m_start;
        private int m_length;

        private double[] m_data;

        private int m_maxValue;

        private bool m_padToWeekMax;
        public bool PadToWeekMax
        {
            get { return m_padToWeekMax; }
            set { m_padToWeekMax = value; }
        }

        private bool m_showGrid;
        public bool ShowGrid
        {
            get { return m_showGrid; }
            set { m_showGrid = value; }
        }

        public PlanningControl(Context context) 
            : base (context) 
        {
            Init();
        }

        public PlanningControl(Context context, IAttributeSet attrs) 
            : base (context, attrs)
        {
            Init();
        }

        public PlanningControl(Context context, IAttributeSet attrs, int defStyle) 
            : base(context, attrs, defStyle) 
        {
            Init();
        }

        private void Init()
        {
            m_rowHeight = Application.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Planning_Row_ItemHeight);

            Color overColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.column_over);
            Color underColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.column_under);
            Color gridColor = ConsortiumApp.Instance.Resources.GetColor(Resource.Color.lightGrey);

            m_overPaint = new Paint();
            m_overPaint.Color = overColor;
            m_overPaint.SetStyle(Paint.Style.Fill);

            m_underPaint = new Paint();
            m_underPaint.Color = underColor;
            m_underPaint.SetStyle(Paint.Style.Fill);

            m_gridPaint = new Paint();
            m_gridPaint.Color = gridColor;
            m_gridPaint.SetStyle(Paint.Style.Stroke);
        }

        public void SetContentData(double[] data, int start, int length)
        {
            m_data = data ?? new double[0];

            // Find maximum value for day to set row height accordingly
            m_maxValue = 1; 

            int startPoint = PadToWeekMax ? 0 : start;
            int endPoint = PadToWeekMax ? m_data.Length : start + length;

            for(int i = startPoint; i < endPoint; i++)
            {
                int absoluteValue = (int) System.Math.Abs(m_data[i]);
                if(absoluteValue > m_maxValue)
                {
                    m_maxValue = absoluteValue;
                }
            }

            SetMinimumHeight((int)(m_rowHeight * m_maxValue));
            m_start = start;
            m_length = length;

            Invalidate();
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            DrawCoverage(canvas, m_overPaint, 1);
            DrawCoverage(canvas, m_underPaint, -1);

            if (ShowGrid)
            {
                DrawVerticalGridlines(canvas, m_gridPaint);
            }

            DrawHorizontalGridlines(canvas, m_gridPaint);
        }

        private void DrawCoverage(Canvas canvas, Paint paint, int mod)
        {
            Path path = new Path();

            int pos = 0;
            int end = pos + m_length;
            float step = (float)canvas.Width / ScheduleTime.DayIntervals;
            float lastVar = 0;
            float nextVar = 0;

            while (pos < end)
            {
                while (pos < end)
                {
                    nextVar = System.Math.Max(0, (float)(m_data[pos + m_start] * mod));

                    if (nextVar != lastVar)
                        break;

                    ++pos;
                }

                float x = (float)(pos * step);
                path.LineTo(x, lastVar * m_rowHeight);
                path.LineTo(x, nextVar * m_rowHeight);
                lastVar = nextVar;
            }

            path.LineTo ((float)(pos * step), 0);
            path.LineTo(0, 0);
            path.Close();

            canvas.DrawPath(path, paint);
        }

        private void DrawVerticalGridlines(Canvas canvas, Paint paint)
        {
            float step = (float)canvas.Width / ScheduleTime.DayIntervals;
            for (int i = 1; i < ScheduleTime.DayIntervals; ++i)
            {
                float x = i * step;
                canvas.DrawLine(x, 0, x, canvas.Height, paint);
            }
        }

        private void DrawHorizontalGridlines(Canvas canvas, Paint paint)
        {
            for(int i = 0; i < m_maxValue; i++)
            {
                canvas.DrawLine(0, m_rowHeight * i, canvas.Width, m_rowHeight * i, paint);
            }
        }

        public override void Invalidate()
        {
            base.Invalidate();
        }
    }
}

