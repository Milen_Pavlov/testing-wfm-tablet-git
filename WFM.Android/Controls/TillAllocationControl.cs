using System;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Android;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using WFM.Core;
using WFM.Android.Constants;
using Android.App;
using Android.Support.V4.Widget;

namespace WFM.Android
{
    public class TillAllocationControl : View
    {
		public Action<View, AllocationsViewModel.TillAllocationModel, Point> TillAllocationReassign { get; set; }

        private int m_brightnessThreshold = 0;
        private Color m_nameTextColour = Color.White;
        
        private readonly ITimeFormatService m_timeFormatService;
        
        AllocationsViewModel.TillAllocationModel m_model;
        public AllocationsViewModel.TillAllocationModel Model
        {
            get
            {
                return m_model;
            }

            set
            {
                m_model = value;

                SetupControl();
            }
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        // Paints
        private Paint m_tillAllocationPaint;
        private Paint m_textPaint;
        private Paint m_textHoursPaint;

        // Container
        private int m_width;
        private int m_height;
        private RectF m_dayFrame;
        private float m_margin = 4;
        private float m_borderWidth = 1;
        private float m_cornerRadius = 4;

        // Edit Controls
        const float c_scrollThreshold = 10;
        private bool m_hasMoved;
        private double m_touchStartXPosition;
        private double m_touchStartYPosition;

        public bool InvalidLocation { get; set; }

        Color m_borderColour;
        Color m_backgroundColour;

        string m_hoursAsString;

        float m_shiftRight;
        float m_shiftLeft;


        public TillAllocationControl(Context context)
            : base(context)
        {
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
            Initialise();
        }

        public TillAllocationControl(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet)
        {
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
            Initialise();

        }

        public TillAllocationControl(Context context, IAttributeSet attributeSet, int defStyle)
            : base(context, attributeSet, defStyle)
        {
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
            Initialise();
        }

        private void Initialise()
        {
            
            m_brightnessThreshold = WFM.Android.Utilities.Helpers.ConvertPercentageBrightnessToRGBTotalValue(Resources.GetInteger(Resource.Integer.textColorThreshold));

            m_tillAllocationPaint = new Paint
            {
                Color = Color.Black, 
                AntiAlias = true,
                StrokeWidth = 1.0f
            };
            
            m_tillAllocationPaint.SetStyle(Paint.Style.Stroke);

            SetupTextPaints(m_nameTextColour);
        }

        private void SetupTextPaints(Color textColor)
        {
			int textSize = Resources.GetInteger (Resource.Integer.AllocationControl_Text_Size);

            m_textPaint = new Paint
                {
                    Color = textColor,
                    AntiAlias = true,
                    StrokeWidth = 0.0f,
					TextSize = textSize,
                    TextAlign = Paint.Align.Left,
                };
            
            m_textPaint.SetStyle(Paint.Style.Fill);
            m_textPaint.SetTypeface(Typeface.Create(Typeface.DefaultBold, TypefaceStyle.Bold));

            m_textHoursPaint = new Paint
                {
                    Color = Color.Black,
                    AntiAlias = true,
                    StrokeWidth = 0.0f,
					TextSize = textSize,
                    TextAlign = Paint.Align.Left,
                };
            m_textHoursPaint.SetStyle(Paint.Style.Fill);
            m_textHoursPaint.SetTypeface(Typeface.Create(Typeface.Default, TypefaceStyle.Normal));
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            m_width = View.MeasureSpec.GetSize(widthMeasureSpec);
            m_height = View.MeasureSpec.GetSize(heightMeasureSpec);
            m_dayFrame = new RectF(0, 0, m_width, m_height);
            SetMeasuredDimension(m_width, m_height);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            var rectangle = GetRectangle();

			Color background = m_backgroundColour;
			if (Model.CanEdit) 
			{
				//WE have a white background, so simulate the alpha instead as it will be cheaper to render
				background.R += (byte)((255 - background.R) >> 1);
				background.G += (byte)((255 - background.G) >> 1);
				background.B += (byte)((255 - background.B) >> 1);

//				background.A = 128;
			}

			Color borderCol = m_borderColour;
			float borderWidth = m_borderWidth;

			if (InvalidLocation) 
			{
				borderCol = Color.MediumVioletRed;
				borderWidth = 5;
			}

			WFM.Android.Utilities.Helpers.drawRoundRectWithBorder(canvas, m_tillAllocationPaint, rectangle, m_cornerRadius, background, borderCol, borderWidth);

			int textHeightPaddingTop = Resources.GetInteger (Resource.Integer.AllocationControl_Text_HeightPadding_Top);
			int textHeightPaddingBot = Resources.GetInteger (Resource.Integer.AllocationControl_Text_HeightPadding_Bottom);
			WFM.Android.Utilities.Helpers.DrawTextInHorizontalSpace(canvas, Model.EmployeeFullName, rectangle.Left + 6, rectangle.Top + textHeightPaddingTop, rectangle.Width() - 12, m_textPaint);
			WFM.Android.Utilities.Helpers.DrawTextInHorizontalSpace(canvas, m_hoursAsString, rectangle.Left + 6, rectangle.Top + (rectangle.Height() * 0.5f) + textHeightPaddingBot, rectangle.Width() - 12, m_textHoursPaint);
        
			if (Model.CanEdit) 
			{
				//Draw dot to denote draggable
				var centerX = rectangle.CenterX();
				var centerY = rectangle.CenterY();
				var size = Application.Context.Resources.GetDimension (Resource.Dimension.TillAllocation_EditDot_Size);
				var dotBorderWidth = Application.Context.Resources.GetDimension (Resource.Dimension.TillAllocation_EditDot_BorderWidth);
				var dotRect = new RectF(centerX - (size / 2), centerY - (size / 2), centerX + (size / 2), centerY + (size / 2));

				Color dotBorderColor = Application.Context.Resources.GetColor (Resource.Color.tillallocation_editdot_border);

				WFM.Android.Utilities.Helpers.drawRoundRectWithBorder(canvas, m_tillAllocationPaint, dotRect, size / 2, m_backgroundColour, dotBorderColor, dotBorderWidth);
			}
		}

        private float GetX(float hours, float scale) // 0.0 -> 24.0
        {
            return ((hours - Model.Hours.Item1) / scale ) * m_width;
        }

        public void OnHoursUpdated()
        {
            this.Invalidate();
        }

        private RectF GetRectangle()
        {
            var yOffset = m_margin + m_borderWidth;
            var top = yOffset;
            var bottom = m_dayFrame.Height() - yOffset;

            float scale = 0;

			if(Model.Hours != null)
                scale = (Model.Hours.Item2 - Model.Hours.Item1);

            var left = GetX(m_shiftLeft,scale);
            var right = GetX(m_shiftRight,scale) - m_borderWidth;

			return new RectF(left, top, right, bottom);
        }

        private void SetupControl()
        {
            m_backgroundColour = ColorUtility.FromHexString(Model.Colour.BackgroundColour);
            m_borderColour = ColorUtility.FromHexString(Model.Colour.BorderColour);

            m_nameTextColour = WFM.Android.Utilities.Helpers.GetTotalRGBColorValue(m_backgroundColour) > m_brightnessThreshold  ? Color.Black : Color.White;
            SetupTextPaints(m_nameTextColour);

            m_hoursAsString = string.Format("{0} - {1}", m_timeFormatService.FormatTime(Model.StartTime), m_timeFormatService.FormatTime(Model.EndTime));

            float rightHours = 24f;
            float leftHours = 0f;

            if (Model.EndTime.Date > StartDate.Date)
            {
                rightHours = 24f;
            }
            else
            {
                rightHours = (float)Model.EndTime.TimeOfDay.TotalHours;
            }

			if (Model.StartTime.Date < StartDate.Date)
            {
                leftHours = 0f;
            }
            else
            {
                leftHours = (float)Model.StartTime.TimeOfDay.TotalHours;
            }

            m_shiftRight = Math.Min(rightHours, 24);
            m_shiftLeft = Math.Max(leftHours, 0);
        }

		public override bool DispatchTouchEvent (MotionEvent e)
		{
			if (!Model.CanEdit)
				return base.DispatchTouchEvent (e);

			var rectangle = GetRectangle();
			
			// Preview the touch event to detect a swipe: switch (e.ActionMasked) 
			switch (e.Action)
			{
			case MotionEventActions.Down:
				//Check if point is within till allocation
				if (IsPointWithinTill (rectangle, e.GetX (), e.GetY (), 0)) 
				{
					m_hasMoved = false;

					m_touchStartXPosition = e.GetX();
					m_touchStartYPosition = e.GetY();

					//Get the data copied to the clipboard asap
					if (TillAllocationReassign != null)
					{
						TillAllocationReassign(this, m_model, new Point((int)Math.Round(e.GetX()), (int)Math.Round(e.GetY())));
					}

					return true;//Swallow input
				}
				break;
			case MotionEventActions.Up: 
			case MotionEventActions.Cancel:
				if (m_hasMoved) 
				{
					m_hasMoved = false;
					return true;
				}
				break;
			case MotionEventActions.Move:
				if (!m_hasMoved && (Math.Abs(m_touchStartXPosition - e.GetX()) > c_scrollThreshold || Math.Abs(m_touchStartYPosition - e.GetY()) > c_scrollThreshold))
				{
					m_hasMoved = true;
				}

				if (m_hasMoved)
				{
					double xMoved = e.GetX() - m_touchStartXPosition;
					m_touchStartXPosition = e.GetX(); // Update for next time round
					m_touchStartYPosition = e.GetY(); // Update for next time round

				}
				if (TillAllocationReassign != null)
				{
					TillAllocationReassign(this, m_model, new Point((int)Math.Round(e.GetX()), (int)Math.Round(e.GetY())));
				}
				return true;
			}

			return base.DispatchTouchEvent (e);
		}

		bool IsPointWithinTill(RectF rect, float x, float y, float xExpansion)
		{
			if (xExpansion != 0) 
			{
				rect.Left -= xExpansion;
				rect.Right += xExpansion;
			}

			return rect.Contains(x, y);
		}
    }
}
