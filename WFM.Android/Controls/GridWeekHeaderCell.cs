﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content;
using Android.Util;
using WFM.Core;
using WFM.Android;

public class GridWeekHeaderCell : FrameLayout
{
	public TextView Title 
	{
		get;
		private set;
	}

	public TextView LeftHeader 
	{
		get;
		private set;
	}

	public TextView RightHeader 
	{
		get;
		private set;
	}

	public TextView LeftValue
	{
		get;
		private set;
	}

	public TextView RightValue
	{
		get;
		private set;
	}

	public GridWeekHeaderCell(Context context) : base(context)
	{
		Initialise(null, 0);
	}

	public GridWeekHeaderCell(Context context, IAttributeSet attrs) : base(context, attrs)
	{
		Initialise(attrs, 0);
	}

	public GridWeekHeaderCell(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
	{
		Initialise(attrs, defStyle);
	}

	private void Initialise(IAttributeSet attrs, int defStyle)
	{
		Inflate (Context, Resource.Layout.header_week_total, this);

		Title = (TextView)FindViewById (Resource.Id.title_textView);
		LeftHeader = (TextView)FindViewById (Resource.Id.left_heading_textView);
		RightHeader = (TextView)FindViewById (Resource.Id.right_heading_textView);
		LeftValue = (TextView)FindViewById (Resource.Id.left_value_textView);
		RightValue = (TextView)FindViewById (Resource.Id.right_value_textView);
	}
}