using System;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Android;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using WFM.Core;
using System.Threading.Tasks;
using System.Threading;

namespace WFM.Android
{
    public class ShiftControl : View
    {
        enum PanMode
        {
            Move,
            SizeLeft,
            SizeRight
        };

        public Action<ShiftData> ShiftSelect { get; set; }
        public Action<View, ShiftData, Point> ShiftReassign { get; set; }
        private bool padding;

		// Paint's
        private int m_brightnessThreshold = 0;
		private Paint m_shiftPaint;
		private Paint m_jobPaint;
		private Paint m_textPaint;
		private Paint m_textHoursPaint;

        private ShiftData m_shift;
        private DateTime m_dayStart;
        private int m_width;
        private int m_height;
        private RectF m_dayFrame;
        private PanMode m_panMode;
        private float m_intervalWidth;
        private float m_hourWidth;

        // Precalculated Rects for Shift and Jobs
        public RectF m_shiftFrame { get; private set;}
        private List<RectF> m_jobFrames = new List<RectF>();

        private bool m_canEdit = false;

        private Bitmap m_handleBitmap;
        private int m_handleBitmapHalfWidth, m_handleBitmapHalfHeight;

        private bool m_showShiftDetails;
        private bool m_previousDayShift;
        private bool m_hasMoved;
        private double m_touchStartXPosition;
        private double m_touchStartYPosition;

        private DateTime m_lastTouchDownTime;
        private Point m_lastTouchPoint;
        private CancellationTokenSource m_lastTouchCancellationToken;
        const int c_longClickMillseconds = 300;

        const float c_scrollThreshold = 10;
        const int c_sideOverlap = 10; //< Invisible area to left and right to capture resize touches
        const int c_sideTouch = c_sideOverlap * 3; //< Area on left and right which will activate size over move

        float m_TextSize = 17.0f;
        int m_topTextPadding = 17;
        int m_bottomTextPadding = 17;
        int m_rightTextPadding = 12;
        int m_leftTextPadding = 6;

        public ShiftData Shift
        {
            get { return m_shift; }
            set { m_shift = value; }
        }


        private bool m_alertBoxDisplaying;
        public bool AlertBoxDisplaying
        {
            get { return m_alertBoxDisplaying; }
            set { m_alertBoxDisplaying = value; }
        }

        public bool CanEdit
        {
            get { return m_canEdit; }
            set { m_canEdit = value; }
        }
            
		public ShiftControl(Context context) : base(context) 
		{ 
			Init();
		}

		public ShiftControl(Context context, IAttributeSet attrs ) : base(context, attrs) 
		{ 
			Init();
		}

		public ShiftControl(Context context, IAttributeSet attrs, int defStyle ) : base(context, attrs, defStyle) 
		{ 
			Init();
		}

        private void Init()
        { 
            SetupDimensions();
            SetupPaints (); 

            m_brightnessThreshold = Utilities.Helpers.ConvertPercentageBrightnessToRGBTotalValue(Resources.GetInteger(Resource.Integer.textColorThreshold));
            m_handleBitmap = BitmapFactory.DecodeResource(Resources, Resource.Drawable.drag_handle);
            m_handleBitmapHalfHeight = m_handleBitmap.Height/2;
            m_handleBitmapHalfWidth = m_handleBitmap.Width/2;
            AlertBoxDisplaying = false; 
        }

        private void SetupDimensions()
        {
            m_TextSize = this.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_GridDayCell_FontSize);
            m_leftTextPadding = m_rightTextPadding = this.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_GridDayCell_TextHorizontalPadding);
            m_topTextPadding = this.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_GridDayCell_TextVerticalPadding);
            m_bottomTextPadding = this.Context.Resources.GetDimensionPixelSize(Resource.Dimension.Schedule_GridDayCell_TextSpacePadding);
        }

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{
			// Get the size of the control
            m_width = View.MeasureSpec.GetSize(widthMeasureSpec);
            m_height = View.MeasureSpec.GetSize(heightMeasureSpec);
			SetMeasuredDimension(m_width, m_height);

			m_dayFrame = new RectF () {
				Left = 0.0f,
				Top = 0.0f,
                Right = (float)m_width,
				Bottom = (float)m_height
			};

			m_intervalWidth = m_dayFrame.Width() / ScheduleTime.DayIntervals;
            m_hourWidth = m_intervalWidth * ScheduleTime.HourIntervals;

            // Setup shiftFrame here to begin with. Then we keep drawing it.
            UpdateFrames();
		}

        private void UpdateFrames()
        {
            m_jobFrames.Clear();

            if (m_shift == null)
                return;

            m_shiftFrame = RectFromTime(m_shift.Start, m_shift.End, 4);

            foreach (JobData job in m_shift.Jobs)
            {
                m_jobFrames.Add(RectFromTime(job.Start, job.End, 0));
            }
        }

		private void SetupPaints()
		{
			m_shiftPaint = new Paint 
			{
				Color = Color.Black, 
				AntiAlias = true,
				StrokeWidth = 1.0f
			};
			m_shiftPaint.SetStyle (Paint.Style.Stroke);

			m_jobPaint = new Paint 
			{
				Color = Color.Black, 
				AntiAlias = true,
				StrokeWidth = 1.0f
			};
			m_jobPaint.SetStyle (Paint.Style.Stroke);

			m_textPaint = new Paint 
			{
                Color = Resources.GetColor(Resource.Color.shift_text),
				AntiAlias = true,
				StrokeWidth = 0.0f,
                TextSize = m_TextSize,
				TextAlign = Paint.Align.Left,
			};
			m_textPaint.SetStyle (Paint.Style.Fill);
			m_textPaint.SetTypeface( Typeface.Create( Typeface.DefaultBold, TypefaceStyle.Bold));

			m_textHoursPaint = new Paint 
			{
                Color = Resources.GetColor(Resource.Color.shift_hours_text),
				AntiAlias = true,
				StrokeWidth = 0.0f,
                TextSize = m_TextSize,
				TextAlign = Paint.Align.Left,
			};
			m_textHoursPaint.SetStyle (Paint.Style.Fill);
			m_textHoursPaint.SetTypeface( Typeface.Create( Typeface.Default, TypefaceStyle.Normal));
		}

        public void Setup(ShiftData shift, bool canEdit)
        {
            m_shift = shift;
            CanEdit = canEdit;
            padding = false;
        }
		
        public void Setup(ShiftData shift, DateTime dayStart, bool canEdit)
		{
            if (shift != null)
                m_previousDayShift = dayStart.Date != shift.Start.Date;
            else
                m_previousDayShift = false;
            m_shift = shift;
			m_dayStart = dayStart;
            CanEdit = canEdit;
            padding = true;
		}

		protected override void OnDraw(Canvas canvas)
		{
            base.OnDraw(canvas);

            // This works perfectly for allowing a 'handle' to be kept on any Shifts being edited to be dragged around.
            // So if the user moves in circles, the Shift will get dragged left and right as you'd ideally expect.
            var parentViewGroup = Parent;
            if (parentViewGroup != null)
            {
                parentViewGroup.RequestDisallowInterceptTouchEvent(true);
            }

			DrawShift(canvas);
		}

        private void DrawShift(Canvas canvas)
        {
            if (m_shift == null)
                return;

            // Ensure the number of frames matches the number of jobs
            if (m_shift.Jobs.Count != m_jobFrames.Count)
                UpdateFrames ();

            for (int i=0; i<m_shift.Jobs.Count; ++i)
            {
                var job = m_shift.Jobs [i];
                var jobRect = m_jobFrames[i];

                if (jobRect.Width() > 0)
                {
                    jobRect.Top  = 4;
                    jobRect.Bottom = jobRect.Top + m_shiftFrame.Height();

                    DrawJob ( canvas, job, jobRect);

                    if (i > 0)
                    {
                        m_jobPaint.SetStyle(Paint.Style.Fill);
                        Color connectorCol = ColorUtility.FromHexString (job.JobColor);
                        m_jobPaint.Color = connectorCol;
                        canvas.DrawRect( new RectF( jobRect.Left-2, jobRect.Top+1, jobRect.Left+4, jobRect.Bottom-1 ), m_jobPaint );

                        // line
                        m_jobPaint.Color = Color.Black;
                        canvas.DrawRect( new RectF( jobRect.Left, jobRect.Top+1, jobRect.Left+1, jobRect.Bottom-1 ), m_jobPaint );
                    }

                    // Breaks & Meals
                    foreach (var detail in m_shift.Details)
                    {
                        if (detail.Start.HasValue && detail.End.HasValue)
                        {
                            DateTime start = m_dayStart;
                            DateTime end = m_dayStart.AddDays(1);
                            
                            //This adjusts for the scenario where the shift control is inside the shift detail popup and therefore doesn't limit its width.    
                            if(start == DateTime.MinValue)
                            {
                                start = m_shift.Start;
                                end = m_shift.End;
                            }
                        
                            var x1 = XPosFromTime(new RectF(canvas.ClipBounds), start, end, detail.Start.Value);
                            var x2 = XPosFromTime(new RectF(canvas.ClipBounds), start, end, detail.End.Value);
                            
                            m_jobPaint.SetStyle(Paint.Style.Fill);
                            m_jobPaint.Color = Color.Argb(64, 255, 255, 255);

                            float startx = System.Math.Min(jobRect.Right,x1);
                            float endx = System.Math.Min(jobRect.Right,x2);

                            if (startx < canvas.ClipBounds.Right)
                                canvas.DrawRect(new RectF(startx, jobRect.Top, endx, jobRect.Bottom), m_jobPaint);
                        }
                    }
                }
            }

            if(m_shift.Jobs.Count > 0 && CanEdit && m_shift.CanEdit())
            {
                float left = m_jobFrames[0].Left;
                float top = m_jobFrames[0].Top;
                float right = m_jobFrames[0].Right;
                float bottom = m_jobFrames[0].Bottom;

                foreach(var job in m_jobFrames)
                {
                    left = Math.Min(left, job.Left);
                    top = Math.Min(top, job.Top);
                    right = Math.Max(right, job.Right);
                    bottom = Math.Max(bottom, job.Bottom);
                }

                var shiftRect = new RectF(left, top, right, bottom);
                DrawHandles(canvas, shiftRect);
            }
        }

        private void DrawJob( Canvas canvas, JobData job, RectF rect)
        {
            Color color = ColorUtility.FromHexString (job.JobColor);
            m_textPaint.Color = (WFM.Android.Utilities.Helpers.GetTotalRGBColorValue(color) > m_brightnessThreshold  ? Color.Black : Color.White);
            m_textHoursPaint.Color = (WFM.Android.Utilities.Helpers.GetTotalRGBColorValue(color) > m_brightnessThreshold  ? Color.Black : Color.White);
            float cornerRadius = 4.0f;
            Color borderColor = job.Edit != EditState.None ? Color.Red : Color.White;
            float borderWidth = job.Edit != EditState.None ? 2.0f : 1.0f;

            float line1Start = rect.Top + (m_TextSize) + (m_topTextPadding);

            WFM.Android.Utilities.Helpers.drawRoundRectWithBorder( canvas, m_jobPaint, rect, cornerRadius, color, borderColor, borderWidth );
            WFM.Android.Utilities.Helpers.DrawTextInHorizontalSpace( canvas, job.JobName, rect.Left + m_leftTextPadding, line1Start , rect.Width() - m_rightTextPadding, m_textPaint );

            float line2Start = line1Start + (m_TextSize) + m_bottomTextPadding;

            WFM.Android.Utilities.Helpers.DrawTextInHorizontalSpace( canvas, job.StartEnd, rect.Left + m_leftTextPadding, line2Start, rect.Width() - m_rightTextPadding, m_textHoursPaint );
        }

        private void DrawHandles( Canvas canvas, RectF rect)
        {
            canvas.DrawBitmap(m_handleBitmap, rect.Left-m_handleBitmapHalfWidth, rect.CenterY() - m_handleBitmapHalfHeight, m_textPaint);
            canvas.DrawBitmap(m_handleBitmap, rect.Right-m_handleBitmapHalfWidth, rect.CenterY() - m_handleBitmapHalfHeight, m_textPaint);
        }

        public override bool DispatchTouchEvent(MotionEvent e) 
        {
            if (e != null)
            {
                // Preview the touch event to detect a swipe: switch (e.ActionMasked) 
                switch (e.Action)
                {
                    case MotionEventActions.Down:
                        if (IsPointWithinXExpandedShift(e.GetX(), e.GetY(), c_sideTouch))
                        {
                            if (m_previousDayShift && !AlertBoxDisplaying && CanEdit)
                            {
                                AlertBoxDisplaying = true;
                                Mvx.Resolve<IAlertBox>().ShowOK("Shift Edit Error", "Cannot edit shifts that start on the previous day.", l => 
                                            {
                                                AlertBoxClosed();
                                            });
                                return true;
                            }
                            
                            m_showShiftDetails = true;
                            m_hasMoved = false;
                            
                            m_touchStartXPosition = e.GetX();
                            m_touchStartYPosition = e.GetY();
                            if (m_shiftFrame.Left + c_sideTouch > m_touchStartXPosition)
                                m_panMode = PanMode.SizeLeft;
                            else if (m_shiftFrame.Right - c_sideTouch < m_touchStartXPosition)
                                m_panMode = PanMode.SizeRight;
                            else
                                m_panMode = PanMode.Move;
                            
                            m_touchStartXPosition = e.GetX();
                            m_lastTouchCancellationToken = new CancellationTokenSource ();
                            m_lastTouchDownTime = DateTime.Now;
                            m_lastTouchPoint = new Point ((int)Math.Round (e.GetX ()), (int)Math.Round (e.GetY ()));
                            
                            // Start the drag process if a long press occurs, but only in edit mode.
                            if (CanEdit == true)
                            {
                                Task.Run (async () =>
                                    {
                                        await Task.Delay (c_longClickMillseconds);
                                        
                                        if (m_lastTouchCancellationToken != null && m_lastTouchCancellationToken.IsCancellationRequested)
                                            return;
                                        
                                        m_showShiftDetails = false;
                                        m_hasMoved = true;
                                        if (ShiftReassign != null)
                                        {
                                            ShiftReassign (this, m_shift, m_lastTouchPoint);
                                        }
                                        
                                    }, m_lastTouchCancellationToken.Token);
                            }
                            return true; // Consume the touch! Continue showing interest
                        }
                        else
                        {
                            return false;// Do NOT consume the touch. Give up interest
                        }
                    case MotionEventActions.Up: 
                    case MotionEventActions.Cancel:
                        if(m_lastTouchCancellationToken != null)
                            m_lastTouchCancellationToken.Cancel ();
                        
                        if((DateTime.Now - m_lastTouchDownTime).TotalMilliseconds > c_longClickMillseconds && m_panMode == PanMode.Move)
                            return true;
                        
                        if (CanEdit && (m_panMode == PanMode.SizeLeft || m_panMode == PanMode.SizeRight))
                        {
                            SaveChanges();
                        }
                        
                        float x = e.GetX();
                        float y = e.GetY();
                        
                        if (IsPointWithinShift(x, y) && m_showShiftDetails)
                        {
                            if (ShiftSelect != null)
                            {
                                ShiftSelect(m_shift);
                            }
                            
                            return true; // Consume the touch!
                        }
                        break;
                    case MotionEventActions.Move:
                        {
                            if (CanEdit)
                            {
                                // Make sure that slight movements do not swallow a simple tap
                                if (!m_hasMoved && (Math.Abs(m_touchStartXPosition - e.GetX()) > c_scrollThreshold || Math.Abs(m_touchStartYPosition - e.GetY()) > c_scrollThreshold))
                                {
                                    m_showShiftDetails = false;
                                    m_hasMoved = true;
                                    if(m_lastTouchCancellationToken != null)
                                        m_lastTouchCancellationToken.Cancel ();
                                }
                                
                                if (m_hasMoved)
                                {
                                    double xMoved = e.GetX() - m_touchStartXPosition;
                                    m_touchStartXPosition = e.GetX(); // Update for next time round
                                    
                                    if (m_panMode == PanMode.Move)
                                    {
                                        if (ShiftReassign != null)
                                        {
                                            ShiftReassign(this, m_shift, new Point((int)Math.Round(e.GetX()), (int)Math.Round(e.GetY())));
                                        }
                                    }
                                    else if (m_panMode == PanMode.SizeLeft)
                                    {
                                        OnMoveLeft(xMoved);
                                    }
                                    else if (m_panMode == PanMode.SizeRight)
                                    {
                                        OnMoveRight(xMoved);
                                    }
                                }
                                return true;
                            }
                        }
                        break;
                        
                }
            }

            return base.DispatchTouchEvent(e);
        }

        public override bool OnTouchEvent(MotionEvent e) 
        {
            // To make sure to receive touch events, tell parent we are handling them: return true;
            return true;
        }

        private void OnMove(double xMoved)
        {
            float leftDelta = m_shiftFrame.Left;

            m_shiftFrame.Left += (float)xMoved;
            m_shiftFrame.Left = Math.Max(m_shiftFrame.Left, 0);
            m_shiftFrame.Left = Math.Min(m_shiftFrame.Left, m_shiftFrame.Right - m_hourWidth);

            leftDelta = m_shiftFrame.Left - leftDelta;

            // Since this is a Move, we move the Right by the same amount as the Left actually moved
            m_shiftFrame.Right += leftDelta;
            m_shiftFrame.Right = Math.Min(m_shiftFrame.Right, m_dayFrame.Right);
            m_shiftFrame.Right = Math.Max(m_shiftFrame.Right, m_shiftFrame.Left + m_hourWidth);

            m_jobFrames[0].Left = m_shiftFrame.Left;
            m_jobFrames[m_jobFrames.Count - 1].Right = m_shiftFrame.Right;

            int count = m_jobFrames.Count; 
            for (int i=0; i<count; i++)
            {
                if (i>0)
                    m_jobFrames[i].Left += leftDelta;
                if (i<count-1)
                    m_jobFrames[i].Right += leftDelta;
            }

            Invalidate();
        }

        private void OnMoveLeft(double xMoved)
        {
            m_shiftFrame.Left += (float)xMoved;
            m_shiftFrame.Left = Math.Max(m_shiftFrame.Left, 0);
            m_shiftFrame.Left = Math.Min(m_shiftFrame.Left, m_shiftFrame.Right - m_hourWidth);
            m_jobFrames[0].Left = m_shiftFrame.Left;
            Invalidate();
        }

        private void OnMoveRight(double xMoved)
        {
            m_shiftFrame.Right += (float)xMoved;
            m_shiftFrame.Right = Math.Min(m_shiftFrame.Right, m_dayFrame.Right);
            m_shiftFrame.Right = Math.Max(m_shiftFrame.Right, m_shiftFrame.Left + m_hourWidth);
            m_jobFrames[m_jobFrames.Count - 1].Right = m_shiftFrame.Right;
            Invalidate();
        }

        private void SaveChanges()
        {
            // Adjust left and right snap to a period
            m_shiftFrame.Left = (float)Math.Round(m_shiftFrame.Left / m_intervalWidth) * m_intervalWidth;
            float width = (float)Math.Round(m_shiftFrame.Width() / m_intervalWidth) * m_intervalWidth;
            m_shiftFrame.Right = m_shiftFrame.Left + width;

            // Update shift data directly or just send new request
            double minutes = (m_shiftFrame.Left / m_dayFrame.Width()) * ScheduleTime.MinutesPerDay;
            double length = (width / m_dayFrame.Width()) * ScheduleTime.MinutesPerDay;

            // Clamp to remove errors
            minutes = Math.Round(minutes / 15f) * 15f;
            length = Math.Round(length / 15f) * 15f;

            var shiftStart = m_dayStart.AddMinutes(minutes);
            var shiftEnd = shiftStart.AddMinutes (length);

            if (m_shift.Start == shiftStart && m_shift.End == shiftEnd)
                return;
            
            var request = new ShiftEditMessage (this)
            {
                Op = ShiftEditMessage.Operation.UpdateShift,
                ShiftID = m_shift.ShiftID,
                EmployeeID = m_shift.EmployeeID.HasValue ? m_shift.EmployeeID.Value : 0,
                Start = shiftStart,
                End = shiftEnd,
                RecalculateDetails = true
            };

            // Inform listeners of changed size - Doing via message to avoid messy chain back to the view model for now
            // This also allows the view model to handle the update logic or restore previous data
            Mvx.Resolve<IMvxMessenger>().Publish(request);
        }

        bool IsPointWithinShift(float x, float y)
        {
            return m_shiftFrame.Contains(x, y);
        }

        bool IsPointWithinXExpandedShift(float x, float y, float xExpansion)
        {
            if (m_shift == null)
                return false;
            
            RectF rect = new RectF(m_shiftFrame);
            rect.Left -= xExpansion;
            rect.Right += xExpansion;

            return rect.Contains(x, y);
        }

        private float XPosFromTime(RectF frame, DateTime start, DateTime end, DateTime point)
        {
            float scale = 1.0f;

            DateTime clamped = DateTimeUtility.Clamp(point, start, end);
            DateTime clampedStart = DateTimeUtility.Clamp(start, m_dayStart, end);
            TimeSpan offset = clamped - clampedStart;
            TimeSpan total = end - clampedStart;

            //This allows us to deal with dates that should overhang the end of the frame
            if (end.Date > start.Date)
            {
                if (start.Date == m_dayStart.Date) 
                {
                    scale = (float)(total.TotalHours / (end.Subtract(start).TotalHours));
                }
            }

            float xPos =  (float)(offset.TotalMinutes * ((frame.Width() * scale)/ total.TotalMinutes));

            return xPos;
        }

        private RectF RectFromTime(DateTime start, DateTime end, float yBorder)
        {
            var startTime = (padding ? m_dayStart : m_shift.Start);
            var endTime = (padding ? m_dayStart.AddDays(1) : m_shift.End);

            float x1 = XPosFromTime(m_dayFrame, startTime, endTime, start);
            float x2 = XPosFromTime(m_dayFrame, startTime, endTime, end);

            return new RectF () {
                Left = x1,
                Top = yBorder,
                Right = x2,
				Bottom = m_dayFrame.Height() - yBorder
            };
        }

        private float XPosFromTime(DateTime start, DateTime end, DateTime point)
        {
            float width = m_dayFrame.Right - m_dayFrame.Left; 
            DateTime clamped = DateTimeUtility.Clamp(point, start, end);
            TimeSpan offset = clamped - start;
            TimeSpan total = end - start;
            return (float)(offset.TotalMinutes * width / total.TotalMinutes);
        }

        private RectF RectFromTime(DateTime dayStart, DateTime start, DateTime end, float yBorder)
        {
            DateTime dayEnd = dayStart.AddDays(1);

            float x1 = XPosFromTime(dayStart, dayEnd, start);
            float x2 = XPosFromTime(dayStart, dayEnd, end);

            return new RectF () 
            {
                Left = x1,
                Top = yBorder,
                Right = x2,
                Bottom = m_height - yBorder
            };
        }

        private bool RectangleWidthIsZero(RectF rect)
        {
            return((int)rect.Left == (int)rect.Right);
        }

        private void AlertBoxClosed()
        {
            AlertBoxDisplaying = false;
        }
    }
}
