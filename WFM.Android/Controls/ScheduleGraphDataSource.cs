﻿using System;
using WFM.Core;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Com.ShinobiControls.Charts;
using System.Collections.Generic;


namespace WFM.Android
{
    public class ScheduleGraphDataAdapter : DataAdapter
    {
        private const int HoursInWeek = 24 * 7;//This is actually wrong because of daylight savings. Ask Iain+Evan

        public enum ScheduleType
        {
            Under,
            Correct,
            Over,
            Demand
        }

        private AccuracyData m_Data;
        public AccuracyData Data 
        { 
            get
            {
                return m_Data;
            }

            set
            {
                m_Data = value;
                Populate();
            }
        }
            
        private int? m_DayIndex;
        public int? DayIndex
        {
            get
            {
                return m_DayIndex;
            }
            set
            {
                m_DayIndex = value;
                Populate();
            }
        }

        private ScheduleType m_scheduleType;


        //Use this dictionary to filter out duplicate time entries.
        //This happens when the clocks change. WE couldn't think of a really nice
        //way to fix this so we're hiding it for now. Sorry.
        private Dictionary<long, double> m_points = new Dictionary<long, double>();

        public ScheduleGraphDataAdapter(ScheduleType scheduleType)
        {
            m_scheduleType = scheduleType;
        }

        void Populate()
        {
            Clear();
            m_points.Clear ();

            if (DayIndex.HasValue)
            {
                for (int i = 0; i < ScheduleTime.DayIntervals; i++)
                {
                    AddPointAtIndex(i);
                }
            }
            else
            {
                for (int i = 0; i < HoursInWeek; i++)
                {
                    AddPointAtIndex(i);
                }
            }

            this.FireUpdateHandler();
        }
            
        void AddPointAtIndex(int dataIndex)
        {
            DateTime X = DateTime.MinValue;
            double Y = 0;

            if (Data != null)
            {
                if (DayIndex.HasValue)
                {
                    if (dataIndex < ScheduleTime.DayIntervals)
                    {
                        double demand = Data.GetDemandInterval(DayIndex.Value, dataIndex);
                        double scheduled = Data.GetScheduledInterval(DayIndex.Value, dataIndex);
                        X = Data.GetTime(DayIndex.Value, dataIndex);

                        switch (m_scheduleType)
                        {
                            case ScheduleType.Under:
                                Y = demand > scheduled ? scheduled : 0;
                                break;
                            case ScheduleType.Correct:
                                Y = demand == scheduled ? scheduled : 0;
                                break;
                            case ScheduleType.Over:
                                Y = demand < scheduled ? scheduled : 0;
                                break;
                            case ScheduleType.Demand:
                                Y = demand;
                                break;
                        }
                    }
                }
                else
                {
                    int index = dataIndex * ScheduleTime.HourIntervals;
                    double demand = 0;
                    double scheduled = 0;

                    for (int i = 0; i < 4; ++i)
                    {
                        demand += Data.GetDemandInterval(index + i);
                        scheduled += Data.GetScheduledInterval(index + i);
                    }

                    demand = Math.Ceiling(demand / 4.0f);
                    scheduled = Math.Ceiling(scheduled / 4.0f);
                    X = Data.GetTime(index);

                    switch (m_scheduleType)
                    {
                        case ScheduleType.Under:
                            Y = demand > scheduled ? scheduled : 0;
                            break;
                        case ScheduleType.Correct:
                            Y = demand == scheduled ? scheduled : 0;
                            break;
                        case ScheduleType.Over:
                            Y = demand < scheduled ? scheduled : 0;
                            break;
                        case ScheduleType.Demand:
                            Y = demand;
                            break;
                    }
                }
              
                var javaDate = DateUtils.ConvertToJavaDate(X);
                if (!m_points.ContainsKey (javaDate.Time))
                {
                    m_points.Add (javaDate.Time, Y);
                    Add(new DataPoint(javaDate, Y));
                }
            }
        }
    }
}