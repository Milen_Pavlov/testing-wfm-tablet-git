﻿using System;
using Consortium.Client.Android;
using Android.Util;
using Android.OS;
using Android.Widget;
using Android.Views;

namespace WFM.Android
{
    public abstract class ScrollableDraggableCollectionControl : DraggableCollectionControl
    {
        internal class ScrollCountDownTimer : CountDownTimer
        {
            public int m_scrollSpeed {get;set;}
            private GridView m_view;
            private const int c_interval = 250;
            private const long millisInFuture = long.MaxValue; // Scroll until timer cancelled

            public ScrollCountDownTimer(GridView view, int scrollSpeed) : base(millisInFuture, c_interval)
            {
                m_scrollSpeed = scrollSpeed;
                m_view = view;
            }

            public override void OnTick(long millisUntilFinished)
            {
                m_view.SmoothScrollBy(m_scrollSpeed, c_interval);
            }

            public override void OnFinish()
            {
            }
        }

        private ScrollCountDownTimer m_timer;

        protected ScrollableDraggableCollectionControl(global::Android.Content.Context context)
            : base(context)
        {
        }
            

        protected ScrollableDraggableCollectionControl(global::Android.Content.Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
        }

        protected ScrollableDraggableCollectionControl (global::Android.Content.Context context, IAttributeSet attrs, int defStyle) 
            : base (context, attrs, defStyle)
        {
        }

        public override void OnDrag (object sender, DragEventArgs e)
        {
            base.OnDrag (sender, e);

            if (m_timer != null)
            {
                m_timer.Cancel ();
                m_timer = null;
            }

            if (e.Event.GetY () > 500 && e.Event.Action != DragAction.Ended)
            {
                m_timer = new ScrollCountDownTimer (this, 10);
                m_timer.Start ();
            }
            else if (e.Event.GetY () < 30 && e.Event.Action != DragAction.Ended)
            {
                m_timer = new ScrollCountDownTimer (this, -10);
                m_timer.Start ();
            }
        }
    }
}

