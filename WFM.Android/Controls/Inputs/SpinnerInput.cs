using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Consortium.Client.Android.Helpers;
using Consortium.Client.Core.Extensions;
using Consortium.Client.Core.ViewModels;
using WFM.Android;
using System.Windows.Input;

namespace WFM.Droid.Controls.Inputs
{
    public class SpinnerInput : Input<Spinner, string>
    {
        public SpinnerInput(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet, Resource.Layout.input_spinner)
        {
			var width = attributeSet.GetAttributeValue (null, "inputWidth");
			if (width.Contains ("@dimen")) 
			{
				var dimen = Resources.GetDimension (Resources.GetIdentifier (width, "dimen", Context.PackageName));
				InputView.SetMinimumWidth ((int)dimen);
			} 
			else 
			{
				InputView.SetMinimumWidth((int)DimensionHelper.ToPixels(width, context.Resources.DisplayMetrics));
			}

            InputView.ItemSelected += (sender, args) =>
            {
                    OnValueChanged(new ValueChangedEventArgs(Value));
				    
                    if(InputChanged != null)
                    {
                        InputChanged.Execute(Items[args.Position]);
                    }
            };
        }

		public ICommand InputChanged 
		{
			get;
			set;
		}

        public override string Value
        {
            get
            {
                return (InputView.SelectedItemPosition >= Items.Count ? null : Items[InputView.SelectedItemPosition].Key);
            }
            set
            {
                var itemIndex = Items._IndexOf(i => i.Key == value);
                InputView.SetSelection(itemIndex == null ? 0 : itemIndex.Value);
            }
        }

        private IList<SpinnerItem> _items;
        public IList<SpinnerItem> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                var values = value.Select(i => i.Value).ToList();
                InputView.Adapter = new ArrayAdapter(Context, Android.Resource.Layout.spinner_item, Resource.Id.spinner_item, values);
            }
        }
    }
}