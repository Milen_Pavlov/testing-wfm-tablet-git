using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Consortium.Client.Android.Helpers;
using WFM.Android;

namespace WFM.Droid.Controls.Inputs
{
    public abstract class Input<TInput, TValue> : FrameLayout
        where TInput : View
    {
        public int LayoutId { get; set; }
        public TextView LabelView { get; set; }
        public TInput InputView { get; set; }
        public abstract TValue Value { get; set; }

        public Input(Context context, IAttributeSet attributeSet, int layoutId)
            : base(context, attributeSet)
        {
            LayoutId = layoutId;
            Inflate(Context, LayoutId, this);
            LabelView = (TextView)FindViewById(Resource.Id.label);
            InputView = (TInput)FindViewById(Resource.Id.input);

            LabelView.Visibility = (attributeSet.GetAttributeBooleanValue(null, "labelVisible", true) ? ViewStates.Visible : ViewStates.Gone);
            LabelView.Text = attributeSet.GetAttributeValue(null, "labelText");

			var width = attributeSet.GetAttributeValue (null, "labelWidth");
			if (width.Contains ("@dimen")) 
			{
				var dimen = Resources.GetDimension (Resources.GetIdentifier (width, "dimen", Context.PackageName));
				LabelView.SetWidth ((int)dimen);
			} 
			else 
			{
				LabelView.SetWidth((int)DimensionHelper.ToPixels(width, context.Resources.DisplayMetrics));
			}


            var labelGravity = attributeSet.GetAttributeValue(null, "labelGravity");
            if (labelGravity != null)
            {
                LabelView.Gravity = GravityHelper.Parse(labelGravity);
            }
        }

        public string Label
        {
            get
            {
                return LabelView.Text;
            }
            set
            {
                LabelView.Text = value;
            }
        }

        public event Action<object, ValueChangedEventArgs> ValueChanged;
        protected virtual void OnValueChanged(ValueChangedEventArgs eventArgs)
        {
            var handler = ValueChanged;
            if (handler != null)
            {
                handler.Invoke(this, eventArgs);
            }
        }

        public bool Editable
        {
            get
            {
                return InputView.Enabled;
            }
            set
            {
                InputView.Enabled = value;
            }
        }

        public class ValueChangedEventArgs : EventArgs
        {
            public TValue Value { get; set; }

            public ValueChangedEventArgs(TValue value)
            {
                Value = value;
            }
        }
    }
}