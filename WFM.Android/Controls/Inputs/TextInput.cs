using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Consortium.Client.Android.Helpers;
using Android.Text;
using WFM.Android;
using Android.Views.InputMethods;

namespace WFM.Droid.Controls.Inputs
{
    public class TextInput : Input<EditText, string>
    {

        public ImeAction SetIMEOptions
        {
            set
            {
                InputView.ImeOptions = value;
            }

            get
            {
                return InputView.ImeOptions;
            }
        }

        public bool SetSingleLine
        {
            set
            {
                InputView.SetSingleLine (value);
            }

            get
            {
                return InputView.InputType == InputTypes.TextFlagMultiLine;
            }
        }

        public TextInput(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet, Resource.Layout.input_text)
        {
			var width = attributeSet.GetAttributeValue (null, "inputWidth");
			if (width.Contains ("@dimen")) 
			{
				var dimen = Resources.GetDimension (Resources.GetIdentifier (width, "dimen", Context.PackageName));
				InputView.SetWidth ((int)dimen);
			} 
			else 
			{
				InputView.SetWidth((int)DimensionHelper.ToPixels(width, context.Resources.DisplayMetrics));
			}

            InputView.TextChanged += (sender, args) =>
            {
                OnValueChanged(new ValueChangedEventArgs(Value));
            };
        }

        public override string Value
        {
            get
            {
                return InputView.Text;
            }
            set
            {
                InputView.Text = value;
            }
        }
    }
}