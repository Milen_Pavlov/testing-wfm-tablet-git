﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Consortium.Client.Android;
using Java.Lang;
using Java.Lang.Reflect;
using WFM.Android.Views;

namespace WFM.Android.Controls
{
    public class IntervalTimePickerDialog : TimePickerDialog
    {
        private int m_interval;
        private TimePicker m_timePicker;
        private TimePickerDialog.IOnTimeSetListener m_callback;

        public IntervalTimePickerDialog(
            Context context, 
            TimePickerDialog.IOnTimeSetListener callback, 
            int hours, 
            int minutes, 
            bool is24HourView,
            int interval)
            : base(
                context,
                TimePickerDialog.ThemeHoloLight,
                callback,
                hours,
                minutes,
                is24HourView)
        {
            m_interval = interval;
            m_callback = callback;
        }

        public override void OnClick(IDialogInterface dialog, int which) {
            if (m_callback != null && m_timePicker!=null) {
                m_timePicker.ClearFocus();
                m_callback.OnTimeSet(m_timePicker, m_timePicker.CurrentHour.IntValue(),
                    m_timePicker.CurrentMinute.IntValue() * m_interval);
            }
        }
            
        protected override void OnStop()
        {
            // override and do nothing
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            try
            {
                Class rClass = Class.ForName("com.android.internal.R$id");
                Field timePicker = rClass.GetField("timePicker");
                m_timePicker = (TimePicker) FindViewById(timePicker.GetInt(null));
                Field m = rClass.GetField("minute");

                NumberPicker mMinuteSpinner = (NumberPicker)m_timePicker.FindViewById(m.GetInt(null));
                mMinuteSpinner.MinValue = 0;
                mMinuteSpinner.MaxValue = (60/m_interval)-1;
                List<string> displayedValues = new List<string>();
                for(int i=0;i<60;i+=m_interval)
                {
                    displayedValues.Add(i.ToString("00"));
                }
                mMinuteSpinner.SetDisplayedValues(displayedValues.ToArray());
            }
            catch (System.Exception)
            {
                Log.Debug("Interval Time Picker", "Could not display values specified");
            }
        }
    }
}

