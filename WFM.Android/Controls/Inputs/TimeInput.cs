using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using WFM.Android;
using Consortium.Client.Android.Helpers;
using WFM.Android.Controls;
using Android.Content.Res;

namespace WFM.Droid.Controls.Inputs
{
    public class TimeInput : Input<TextView, TimeSpan?>, TimePickerDialog.IOnTimeSetListener
    {
        public Action<TimeSpan> TimeChanged { get; set; }
        public int m_interval = 1;

        public TimeInput(Context context, IAttributeSet attributeSet)
            : base(context, attributeSet, Resource.Layout.input_time)
        {
            if (attributeSet != null) 
            {
                TypedArray array = Context.ObtainStyledAttributes(attributeSet,
                    Resource.Styleable.TimeInput);
                if (array != null) 
                {
                    m_interval = array.GetInteger(Resource.Styleable.TimeInput_timeinterval, 1);
                }
            }

			var width = attributeSet.GetAttributeValue (null, "inputWidth");
			if (width.Contains ("@dimen")) 
			{
				var dimen = Resources.GetDimension (Resources.GetIdentifier (width, "dimen", Context.PackageName));
				InputView.SetWidth ((int)dimen);
			} 
			else 
			{
				InputView.SetWidth((int)DimensionHelper.ToPixels(width, context.Resources.DisplayMetrics));
			}

            InputView.Click += (sender, args) =>
            {
                if (Editable)
                {
                        var dialog = new IntervalTimePickerDialog(Context, this, (Hours ?? 0), (Minutes ?? 0), true, m_interval);
                    dialog.Show();
                }
            };
        }

        public override TimeSpan? Value
        {
            get
            {
                TimeSpan value;
                var success = TimeSpan.TryParse(InputView.Text, out value);
                return (success ? value : (TimeSpan?)null);
            }
            set
            {
                InputView.Text = (value == null ? null : value.Value.ToString(@"hh\:mm"));
            }
        }

        public int? Hours
        {
            get
            {
                return (Value == null ? 0 : Value.Value.Hours);
            }
        }

        public int? Minutes
        {
            get
            {
                return (Value == null ? 0 : Value.Value.Minutes);
            }
        }

        public void Initialise(TimeSpan value)
        {
            Value = value;
            InputView.Text = (Value == null ? null : ((TimeSpan) Value).ToString(@"hh\:mm"));
        }

        public void OnTimeSet(TimePicker timePicker, int hours, int minutes)
        {
            Value = new TimeSpan(hours, minutes, 0);
            InputView.Text = (Value == null ? null : ((TimeSpan) Value).ToString(@"hh\:mm"));
            if(Value != null)
            {
                TimeChanged.Invoke((TimeSpan) Value);
            }
        }
    }
}