﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Consortium.Client.Android;
using Consortium.Client.Core;
using WFM.Core;

namespace WFM.Android
{
    public class ShiftJobControl : View
    {
        private int m_width;
        private int m_height;

        private Color m_jobColor;
        private Paint m_jobPaint;
        private Paint m_textPaint;
        private Paint m_textHoursPaint;

        private JobData m_job;
        public JobData Job
        {
            get { return m_job; }
            set 
            { 
                m_job = value; 
                m_jobColor = ColorUtility.FromHexString (m_job.JobColor);
                m_jobPaint = new Paint
                {
                    Color = m_jobColor, 
                    AntiAlias = true,
                    StrokeWidth = 1.0f
                };
                m_jobPaint.SetStyle(Paint.Style.Fill);
            }
        }

        public ShiftJobControl(Context context)
            : base(context)
        {
            Initialize();
        }

        public ShiftJobControl(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public ShiftJobControl(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            m_textPaint = new Paint
            {
                Color = Color.White,
                AntiAlias = true,
                StrokeWidth = 0.0f,
                TextSize = 17.0f,
                TextAlign = Paint.Align.Left,
            };
            m_textPaint.SetStyle (Paint.Style.Fill);
            m_textPaint.SetTypeface( Typeface.Create( Typeface.DefaultBold, TypefaceStyle.Bold));

            m_textHoursPaint = new Paint
            {
                Color = Color.Black,
                AntiAlias = true,
                StrokeWidth = 0.0f,
                TextSize = 17.0f,
                TextAlign = Paint.Align.Left,
            };
            m_textHoursPaint.SetStyle (Paint.Style.Fill);
            m_textHoursPaint.SetTypeface( Typeface.Create( Typeface.Default, TypefaceStyle.Normal));
        }

        protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
        {
            m_width = View.MeasureSpec.GetSize (widthMeasureSpec);
            m_height = View.MeasureSpec.GetSize (heightMeasureSpec);
            SetMeasuredDimension (m_width, m_height);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            if (m_job != null)
            {
                RectF rect = new RectF(
                    0.0f + PaddingRight, 
                    0.0f + PaddingTop, 
                    m_width - (PaddingLeft + PaddingRight), 
                    m_height - (PaddingTop + PaddingBottom));

                DrawJob(canvas, m_job, rect);
            }
        }

        private void DrawJob(Canvas canvas, JobData job, RectF rect)
        {
            float cornerRadius = 4.0f;
            Color borderColor = job.Edit != EditState.None ? Color.Red : Color.White;
            float borderWidth = job.Edit != EditState.None ? 2.0f : 1.0f;
            WFM.Android.Utilities.Helpers.drawRoundRectWithBorder(canvas, m_jobPaint, rect, cornerRadius, m_jobColor, borderColor, borderWidth);
            m_jobPaint.SetStyle(Paint.Style.Fill);

            WFM.Android.Utilities.Helpers.DrawTextInHorizontalSpace(canvas, job.JobName, rect.Left + 6, rect.Top + 17, rect.Width() - 12, m_textPaint);

            WFM.Android.Utilities.Helpers.DrawTextInHorizontalSpace(canvas, job.StartEnd, rect.Left + 6, rect.Top + (rect.Height() * 0.5f) + 17, rect.Width() - 12, m_textHoursPaint);
        }

        private float XPosFromTime(RectF frame, DateTime start, DateTime end, DateTime point)
        {
            DateTime clamped = DateTimeUtility.Clamp(point, start, end);
            TimeSpan offset = clamped - start;
            TimeSpan total = end - start;
            return (float)(offset.TotalMinutes * (frame.Width() / total.TotalMinutes));
        }
    }
}

