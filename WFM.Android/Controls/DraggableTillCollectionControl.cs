﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Android.Util;
using Consortium.Client.Android;
using WFM.Core;

namespace WFM.Android
{
    public class DraggableTillCollectionControl : ScrollableDraggableCollectionControl
    {
        public TillsViewModel VM { set; get; }

        private List<int> m_draggedTillOrder;
        public List<int> DraggedTillOrder
        {
            get { return m_draggedTillOrder; }
            set
            {
                m_draggedTillOrder = value;
                VM.OnTillOrderChanged(value);
            }
        }

        public DraggableTillCollectionControl(global::Android.Content.Context context)
            : base(context)
        {
            Initialize (null, 0);
        }

        public DraggableTillCollectionControl(global::Android.Content.Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize (attrs, 0);
        }

        public DraggableTillCollectionControl (global::Android.Content.Context context, IAttributeSet attrs, int defStyle) 
            : base (context, attrs, defStyle)
        {
            Initialize (attrs, defStyle);
        }

        public new void Initialize(IAttributeSet attrs, int defStyle)
        {
            base.Initialize(attrs, defStyle);
        }

        protected override void OnDragCompleted()
        {
            List<int> tillIDs = new List<int>();
            foreach (var item in Items)
            {
                TillModel tillModel = item as TillModel;
                tillIDs.Add(tillModel.TillId);
            }

            DraggedTillOrder = tillIDs;
        }
    }
}