﻿using System;
using Android.Widget;
using Android.Views;
using Android.Content;
using Android.Util;
using WFM.Core;
using Android.Graphics;
using Java.Lang;
using System.Windows.Input;
using Android.Content.Res;
using System.Globalization;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.Android
{
    public class CombineDepartmentDragRegion : FrameLayout, View.IOnDragListener
    {
        private IESSConfigurationService m_essConfig;
        private IStringService m_localiser;

        //Drag Shadow
        TextView m_dragControl;
        //Offset of drag shadow from where you started dragging
        int m_touchX, m_touchY;

        //Text hint overlay
        FrameLayout m_overlayView;
        TextView m_overlayText;

        CombinedRegion m_region = CombinedRegion.Unknown;

        public ICommand OnDrop 
        {
            get;
            set;
        }

        public LabourDemandDepartment CurrentCombined
        {
            get;
            set;
        }

        public CombineDepartmentDragRegion (Context context, IAttributeSet attributeSet)
            : base(context, attributeSet)
        {
            Initialise(attributeSet);
        }

        private void Initialise(IAttributeSet attributeSet)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            m_essConfig = Mvx.Resolve<IESSConfigurationService>();

            SetOnDragListener(this);

            //Determine what context this drag region represents
            if (attributeSet != null) 
            {
                TypedArray array = Context.ObtainStyledAttributes(attributeSet,
                    Resource.Styleable.CombineDepartmentDragRegion);
                if (array != null) 
                {
                    int enumVal = array.GetInteger(Resource.Styleable.CombineDepartmentDragRegion_departmentListType, -1);
                    if (enumVal == 0)
                        m_region = CombinedRegion.Uncombined;
                    if (enumVal == 1)
                        m_region = CombinedRegion.Lead;
                    if (enumVal == 2)
                        m_region = CombinedRegion.Combined;
                }
            }
        }


        public bool OnDrag (View v, DragEvent e)
        {
            switch (e.Action) {
                case DragAction.Started:
                    return true;
                case DragAction.Entered:
                    ShowDragUI (e);
                    return false;
                case DragAction.Exited:
                    HideDragUI (e);
                    return false;
                case DragAction.Drop:
                    HideDragUI (e);

                    ClipData data = e.LocalState as ClipData;
                    if (data != null)
                    {
                        string deptId = data.GetItemAt(0).Text;
                        string deptName = data.GetItemAt(1).Text;
                        bool deptIsLead = bool.Parse(data.GetItemAt(2).Text);
                        string deptLeadID = data.GetItemAt(3).Text;
                        
                        LabourDemandDepartment model = new LabourDemandDepartment ();
                        model.IsLead = deptIsLead;
                        model.LeadID = deptLeadID;
                        model.Name = deptName;
                        model.ID = deptId;
                        
                        ProcessDroppedDepartment(model);
                    }
                    return false;
                case DragAction.Ended:
                    HideDragUI (e);
                    return true;
                case DragAction.Location:
                    UpdateDragUI (e);
                    return false;
            }

            return false;
        }

        private void ShowDragUI(DragEvent e)
        {
            if (e.LocalState != null) 
            {
                ClipData data = e.LocalState as ClipData;
                if (data != null)
                {
                    string deptId = data.GetItemAt(0).Text;
                    string deptName = data.GetItemAt(1).Text;
                    bool deptIsLead = bool.Parse(data.GetItemAt(2).Text);
                    string deptLeadID = data.GetItemAt(3).Text;
                    int deptWidth = int.Parse(data.GetItemAt (4).Text);
                    int deptHeight = int.Parse(data.GetItemAt (5).Text);
                    int deptTouchX = int.Parse(data.GetItemAt (6).Text);
                    int deptTouchY = int.Parse(data.GetItemAt (7).Text);
                    
                    if (m_dragControl != null) 
                    {
                        RemoveView (m_dragControl);
                        m_dragControl.Dispose ();
                        m_dragControl = null;
                    }
                    
                    m_touchX = deptTouchX;
                    m_touchY = deptTouchY;
                    
                    m_dragControl = new TextView (Context);
                    m_dragControl.Text = deptName;
                    m_dragControl.SetBackgroundColor (Color.LightBlue);
                    m_dragControl.SetTextColor (Color.Black);
                    m_dragControl.Gravity = GravityFlags.Center;
                    m_dragControl.LayoutParameters = new FrameLayout.LayoutParams (deptWidth, deptHeight);
                    
                    //Add the overlay text that hints (if needed)
                    switch (m_region)
                    {
                        case CombinedRegion.Uncombined:
                            if (!deptIsLead && string.IsNullOrEmpty (deptLeadID))
                                break;//Were in the region we started in

                            ShowOverlayUI (deptName, deptId, deptIsLead, deptLeadID);
                            break;
                        case CombinedRegion.Lead:
                            if (deptIsLead)
                                break;//Were in the region we started in

                            ShowOverlayUI (deptName, deptId, deptIsLead, deptLeadID);
                            break;
                        case CombinedRegion.Combined:
                            if (!string.IsNullOrEmpty (deptLeadID))
                                break;//Were in the region we started in

                            bool error = false;
                            error |= CurrentCombined == null;
                            error |= CurrentCombined != null && deptId == CurrentCombined.ID;
                            error |= deptIsLead;
                            error |= CurrentCombined != null && CurrentCombined.Children != null && m_essConfig.CombineDepartmentsMaxChildDepartments >= 0 && CurrentCombined.Children.Count >= m_essConfig.CombineDepartmentsMaxChildDepartments;
                            ShowOverlayUI (deptName, deptId, deptIsLead, deptLeadID, error);
                            break;
                    }
                    
                    AddView (m_dragControl);
                }
            } 
        }

        private void ShowOverlayUI(string name, string id, bool isLead, string leadid, bool error = false)
        {
            float d = Context.Resources.DisplayMetrics.Density;

            //Urgh, should really be dimension values file
            int left = 10;
            int right = 10;

            if (m_region == CombinedRegion.Uncombined)
            {
                left = 18;
                right = 12;
            }
            if (m_region == CombinedRegion.Combined)
            {
                left = 12;
                right = 18;
            }

            m_overlayView = new FrameLayout (Context);
            m_overlayView.LayoutParameters = new FrameLayout.LayoutParams (LayoutParams.MatchParent, LayoutParams.MatchParent) 
                { 
                    Gravity = GravityFlags.Center,
                    LeftMargin = (int)(left * d),
                    RightMargin = (int)(right * d),
                    TopMargin = (int)(20 * d),
                    BottomMargin = (int)(20 * d),
                };

            Color col = error ? Color.LightSalmon : Color.LimeGreen;
            col.A  = (byte)(64);
            m_overlayView.SetBackgroundColor (col);

            m_overlayText = new TextView (Context);
            m_overlayText.LayoutParameters = new FrameLayout.LayoutParams (LayoutParams.MatchParent, LayoutParams.WrapContent) 
                { 
                    Gravity = GravityFlags.Center,
                };
            m_overlayText.Text = GenerateOverlayText (name, id, isLead, leadid);
            m_overlayText.SetTextAppearance (Context, Android.Resource.Style.TextAppearanceMaster);
            m_overlayText.SetTextSize (ComplexUnitType.Dip, 20);
            m_overlayText.Gravity = GravityFlags.Center;
            m_overlayText.SetBackgroundColor (Color.White);
            m_overlayText.SetTextColor (Color.Black);
            m_overlayText.SetPadding (4, 4, 4, 4);


            m_overlayView.AddView (m_overlayText);
            AddView (m_overlayView);

        }

        private string GenerateOverlayText(string name, string id, bool isLead, string leadid)
        {
            string nameA = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name.ToLower ());
            string nameB = CurrentCombined != null ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CurrentCombined.Name.ToLower ()) : string.Empty;

            string result = string.Empty;
            switch (m_region)
            {
                case CombinedRegion.Uncombined:
                    if (isLead)
                        result = string.Format (m_localiser.Get("combined_dept_hint_uncombined_islead"), nameA);
                    else if (CurrentCombined != null && !string.IsNullOrEmpty (leadid))
                        result = string.Format (m_localiser.Get("combined_dept_hint_uncombined_iscombined"), nameA, nameB);
                    else
                        result = string.Format (m_localiser.Get("combined_dept_hint_uncombined_default"), nameA);
                    break;
                case CombinedRegion.Lead:
                    if (string.IsNullOrEmpty (leadid) || CurrentCombined == null)
                        result = string.Format (m_localiser.Get("combined_dept_hint_lead_uncombined"), nameA);
                    else if (CurrentCombined != null)
                        result = string.Format (m_localiser.Get("combined_dept_hint_lead_combined"), nameA, nameB);
                    break;
                case CombinedRegion.Combined:
                    if (CurrentCombined == null)
                        result = string.Format (m_localiser.Get("combined_dept_hint_combined_nolead"));
                    else if(id == CurrentCombined.ID)
                        result = string.Format (m_localiser.Get("combined_dept_hint_combined_currentlead"), nameA);
                    else if(CurrentCombined.Children != null && m_essConfig.CombineDepartmentsMaxChildDepartments >= 0 && CurrentCombined.Children.Count >= m_essConfig.CombineDepartmentsMaxChildDepartments)
                        result = string.Format (m_localiser.Get("combined_dept_hint_combined_full"), nameB);
                    else if (isLead)
                        result = string.Format (m_localiser.Get("combined_dept_hint_combined_islead_not_allowed"));
                    else
                        result = string.Format (m_localiser.Get("combined_dept_hint_combined_uncombined"), nameA, nameB);
                    break;
            }
            return result;
        }

        private void HideDragUI(DragEvent e)
        {
            if (m_dragControl != null) 
            {
                RemoveView (m_dragControl);
                m_dragControl.Dispose ();
                m_dragControl = null;
            }

            if (m_overlayText != null)
            {
                if(m_overlayView!=null)
                    m_overlayView.RemoveView (m_overlayText);
                
                m_overlayText.Dispose ();
                m_overlayText = null;
            }

            if (m_overlayView != null)
            {
                RemoveView (m_overlayView);
                m_overlayView.Dispose ();
                m_overlayView = null;
            }
        }

        private void UpdateDragUI(DragEvent e)
        {
            if (m_dragControl != null && e.LocalState != null)
            {
                m_dragControl.SetX (e.GetX () - m_touchX);
                m_dragControl.SetY (e.GetY () - m_touchY);
            }
        }


        public void ProcessDroppedDepartment(LabourDemandDepartment model)
        {
            if (OnDrop != null && OnDrop.CanExecute(model))
                OnDrop.Execute (model);
        }
    }
}

