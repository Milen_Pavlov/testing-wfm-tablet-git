using System;
using Android.App;
using Android.Graphics;
using Android.Widget;
using Android.Util;
using Android.Views;
using Java.Lang.Reflect;
using Java.Lang;

namespace WFM.Android.Utilities
{
	public static class Helpers
	{
		// Convert from DeviceIndependentPixels to Pixels
		public static int DPtoPX(int dp )
		{
			var metrics = Application.Context.Resources.DisplayMetrics;
			return (int)( dp * metrics.Density + 0.5f );
		}

		// Convert from Pixels to DeviceIndependentPixels
		public static int PXtoDP( int px )
		{
			var metrics = Application.Context.Resources.DisplayMetrics;
			return (int)( px / metrics.Density + 0.5f );
		}

		// Convert from DeviceIndependentPixels to Pixels (float version)
		public static float DPtoPX( float dp )
		{
			var metrics = Application.Context.Resources.DisplayMetrics;
			return dp * metrics.Density;
		}

		// Convert from Pixels to DeviceIndependentPixels (float version)
		public static float PXtoDP( float px )
		{
			var metrics = Application.Context.Resources.DisplayMetrics;
			return px / metrics.Density;
		}

		// Draw text centred at a given x, and y coord onto a specified Canvas with a specified Paint
		public static void drawCentredText(Canvas canvas, Paint paint, string text, float centreX, float y) 
		{
			// Measure the width of the string
			float textWidth = paint.MeasureText( text );

			// Draw the text string in the centre of the control
			canvas.DrawText( text, centreX-textWidth/2, y, paint );
		}

		public static void drawRect(Canvas canvas, Paint paint, RectF rect)
		{
			canvas.DrawRect (rect, paint);
		}

		public static void drawRoundRect (Canvas canvas, Paint paint, RectF rect, float cornerXRadius, float cornerYRadius)
		{
			canvas.DrawRoundRect (rect, cornerXRadius, cornerYRadius, paint);
		}

		public static void drawRoundRect (Canvas canvas, Paint paint, RectF rect, float cornerRadius )
		{
			canvas.DrawRoundRect (rect, cornerRadius, cornerRadius, paint);
		}

		public static void drawRoundRectWithBorder (Canvas canvas, Paint paint, RectF rect, float cornerRadius, Color color, Color borderColor, float borderWidth )
		{
			// TODO: Consider saving/restoring paint state
			paint.SetStyle(Paint.Style.Fill);
			paint.Color = color;
			canvas.DrawRoundRect (rect, cornerRadius, cornerRadius, paint);

			paint.SetStyle(Paint.Style.Stroke);
			paint.StrokeWidth = borderWidth;
			paint.Color = borderColor;
			canvas.DrawRoundRect (rect, cornerRadius, cornerRadius, paint);
		}

		public static Rect RectFToRect (RectF rectF )
		{
			return new Rect () {
				Left = (int)rectF.Left,
				Top = (int)rectF.Top,
				Right = (int)rectF.Right,
				Bottom = (int)rectF.Bottom
			};
		}

		public static RectF RectToRectF (Rect rect )
		{
			return new RectF () {
				Left = (float)rect.Left,
				Top = (float)rect.Top,
				Right = (float)rect.Right,
				Bottom = (float)rect.Bottom
			};
		}

        /// <summary>
        /// Draws the text in horizontal space. Performs ellipsis truncation if the text was too long to draw. If ellipsis wo
        /// 
        /// WARNING: Ellipsis calculation is naieve and will have a worst case performance of O(N) where n is the number of characters in text
        /// </summary>
        /// <returns><c>true</c>, if text in horizontal space was drawn, <c>false</c> otherwise.</returns>
        /// <param name="canvas">Canvas.</param>
        /// <param name="text">Text.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="maxWidth">Max width.</param>
        /// <param name="paint">Paint.</param>
		public static bool DrawTextInHorizontalSpace( Canvas canvas, string text, float x, float y, float maxWidth, Paint paint )
		{
			// NB: Currently only switches completely to ellipsis. Full functionality needs implementing
            string ellipsis = "...";

            float ellipsisWidth = paint.MeasureText(ellipsis);

			float width = paint.MeasureText (text);

            if (width > maxWidth)
            {
                if (width > ellipsisWidth)
                {
                    if (ellipsisWidth > maxWidth)
                    {
                        text = string.Empty;
                    }
                    else
                    {
                        //Super dumb way, remove characters until we fit
                        do
                        {
                            text = text.Substring(0, text.Length - 1);

                            width = paint.MeasureText(text) + ellipsisWidth;
                        }
                        while(width > maxWidth);

                        text += ellipsis;
                    }
                }
            }

			canvas.DrawText (text, x, y, paint);

			return true;
		}

        public static bool SetNumberPickerTextColorAndSize(NumberPicker numberPicker, Color color, ComplexUnitType complexUnitType, float textSize, TypefaceStyle style)
        {
            int count = numberPicker.ChildCount;
            for (int i = 0; i < count; i++)
            {
                View child = numberPicker.GetChildAt(i);
                if (child.GetType() == typeof(EditText))
                {
                    try
                    {
                        Field selectorWheelPaintField = numberPicker.Class
                            .GetDeclaredField("mSelectorWheelPaint");
                        selectorWheelPaintField.Accessible = true;

                        EditText editText = (EditText) child;
                        editText.SetTextSize(complexUnitType, textSize);
                        editText.SetTypeface(editText.Typeface, style);
                        editText.SetTextColor(color);

                        Paint paint = (Paint) selectorWheelPaintField.Get(numberPicker);
                        paint.TextSize =  TypedValue.ApplyDimension(complexUnitType, textSize, numberPicker.Resources.DisplayMetrics);
                        paint.Color = color;
                        paint.SetTypeface(editText.Typeface);

                        numberPicker.Invalidate();
                        return true;
                    }
                    catch (NoSuchFieldException e)
                    {
                        Log.Warn("setNumberPickerTextColor", e);
                    }
                    catch (IllegalAccessException e)
                    {
                        Log.Warn("setNumberPickerTextColor", e);
                    }
                    catch (IllegalArgumentException e)
                    {
                        Log.Warn("setNumberPickerTextColor", e);
                    }
                }
            }
            return false;
        }

        public static int ConvertPercentageBrightnessToRGBTotalValue(float brightnessThresholdPercentage)
        {
            return (int) ((255 * 3) * (brightnessThresholdPercentage / 100));
        }

        public static int GetTotalRGBColorValue(Color c)
        {
            return c.R + c.G + c.B;
        }

        public static void dragEventFix(View view)
        {
            //http://stackoverflow.com/questions/11697079/how-to-register-a-dragevent-while-already-inside-one-and-have-it-listen-in-the-c
            // Drag events are not called for a drag listener that is created after the drag begins.
            // For some reason setting the view to gone and then visible resets a bunch of stuff and it then gets setup correctly.
            view.Visibility = ViewStates.Gone;
            view.Visibility = ViewStates.Visible;

            //view.PostInvalidate ();
        }
    }
}

