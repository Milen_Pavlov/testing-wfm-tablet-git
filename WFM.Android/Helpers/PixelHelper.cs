﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFM.Android.Helpers
{
    // ToDo: Consider moving core functionality into Consortium
    // BRY 3/12/14: These are hard coded values so shouldnt be part of consortium
    public static class PixelHelper
    {
        public const int Hudl1Width = 1440;
        public const int Hudl1Height = 900;
        public const int Hudl1NotificationBarHeight = 38;
        public const int WfmSubMenuBarHeight = 84;
        public const int WfmContentAreaHeight = (Hudl1Height - Hudl1NotificationBarHeight - WfmSubMenuBarHeight - Hudl1SoftButtonBarHeight); // 706
        public const int Hudl1SoftButtonBarHeight = 72;

        public static int LeftWhenCentered(int width)
        {
            return ((Hudl1Width - width) / 2);
        }

        public static int TopWhenCenteredInContentArea(int height)
        {
            return (Hudl1NotificationBarHeight + WfmSubMenuBarHeight + (WfmContentAreaHeight - height) / 2);
        }
    }
}
