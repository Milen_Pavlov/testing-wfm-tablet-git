using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore.UI;

namespace WFM.Android.Constants
{
    public static class ColourConstant
    {
        public const string LightGrey = "#909090";
        public const string TescoRed = "#EE1C2E";
    }
}