﻿using System;
using Cirrious.CrossCore.UI;
using Cirrious.CrossCore.Converters;
using Cirrious.MvvmCross.Plugins.Color;
using Android.Graphics;
using Consortium.Client.Android;


namespace WFM.Core
{
    public class JobColorValueConverter : MvxColorValueConverter
    {
        protected override MvxColor Convert(object value, object parameter, System.Globalization.CultureInfo culture)
        {
            Color c = ColorUtility.FromHexString(value.ToString());
            return new MvxColor(c.R, c.G, c.B, c.A);
        }
    }
}

