﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class TimeDayValueConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string format = "ddd HH:mm";
            return(value.ToString(format));
        }
    }
}

