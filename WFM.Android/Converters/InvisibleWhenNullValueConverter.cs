using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore.Converters;

namespace WFM.Android.Converters
{
    public class InvisibleWhenNullValueConverter : MvxValueConverter<string, ViewStates>
    {
        protected override ViewStates Convert(string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value == null ? ViewStates.Gone : ViewStates.Visible);
        }
    }
}