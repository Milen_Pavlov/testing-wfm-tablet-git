﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class DayDateMonthValueConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string format = "ddd d/M";
            return(value.ToString(format));
        }
    }
}

