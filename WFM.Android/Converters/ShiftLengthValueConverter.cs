﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.Core
{
    public class ShiftLengthValueConverter : MvxValueConverter<JDAShift, string>
    {
        protected override string Convert(JDAShift shift, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var hours = (shift.End-shift.Start).TotalHours;
            foreach(var job in shift.Jobs)
            {
                foreach(var detail in job.Breaks)
                {
                    if(detail.Start != null && detail.End != null)
                    {
                        hours -= ((DateTime)detail.End-(DateTime)detail.Start).TotalHours;
                    }
                }
                foreach(var detail in job.Meals)
                {
                    if(detail.Start != null && detail.End != null)
                    {
                        hours -= ((DateTime)detail.End-(DateTime)detail.Start).TotalHours;
                    }
                }
            }
            return hours.ToString("0.##");
        }
    }
}
