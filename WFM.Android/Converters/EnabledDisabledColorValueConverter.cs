﻿using System;
using Android.Graphics;
using Cirrious.CrossCore.Converters;
using Consortium.Client.Android;

namespace WFM.Android
{
    public class EnabledDisabledColorValueConverter : MvxValueConverter<bool, Color>
    {
        protected override Color Convert (bool value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value ? ColorUtility.FromHexString("#ffed1b2e") : ColorUtility.FromHexString("#909090");
        }
    }
}

