import xml.etree.ElementTree as ET
import sys
import os

def main():
	inputFile = sys.argv[1]
	outputFilePath = sys.argv[2]

	dir_path = os.path.dirname(os.path.realpath(outputFilePath))

	if not os.path.exists(dir_path):
            os.makedirs(dir_path)

	output = open(outputFilePath,'w+')

	tree = ET.parse(inputFile)
	root = tree.getroot()

	for child in root:
		line ='"{0}"="{1}";\n'.format(child.attrib['name'], child.text)
		print line
		output.write(line)


if __name__ == "__main__":
	main()