﻿#!/usr/bin/python

import json, sys, xml.etree.cElementTree as tree, os.path, plistlib, argparse, codecs, exportVariables

def get_metadata(currentProjectPath, primaryTheme, defaultTheme):
    
    configFileName = "Config.json"
    themeConfigFilePath = os.path.join(currentProjectPath, "Theme", primaryTheme, configFileName)
    defaultConfigFilePath = os.path.join(currentProjectPath, "Theme", defaultTheme, configFileName)
    masterconfig = os.path.join(currentProjectPath, "Theme", configFileName)

    print "Theme Config - " + themeConfigFilePath
    print "Default Config - " + defaultConfigFilePath
    print "Master Config - " + masterconfig
    
    themeconfig = None
                
    if (os.path.isfile(themeConfigFilePath)):
        print "Theme Config found for " + primaryTheme
        themeconfig = themeConfigFilePath
    else:
        if (os.path.isfile(defaultConfigFilePath)):
            print "No Theme Config, using config found for " + defaultTheme
            themeconfig = defaultConfigFilePath
        else:
            print "No Config files found, Theme or Default"

    # Config objects
    rootData = None;
    themeData = None;
    data = None;

    # Properties
    name = None;
    iosBundle = None;
    droidBundle = None;

    if(os.path.isfile(masterconfig)):
        rootData = json.load(open(masterconfig,'r'))

    if (os.path.isfile(themeconfig)):
        themeData = json.load(open(themeconfig,'r'))

    if(rootData):
        data = rootData.copy()
        if(themeData):
            data.update(themeData)

    return data

def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='App Metadata')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath', type=str, help="Path to current project file")
    parser.add_argument('primaryTheme', metavar='primaryTheme',type=str, help="Primary Theme")
    parser.add_argument('defaultTheme', metavar='defaultTheme',type=str, help="Default Theme")
    parser.add_argument('--variableExport', type=str, default="arguments.txt", help="Path to bamboo compatible export file")

    args = parser.parse_args()
    
    metadata = get_metadata(args.currentProjectPath, args.primaryTheme, args.defaultTheme)

    exportVariables.write(args.variableExport, metadata)
		 
if __name__ == "__main__":
   main(sys.argv[1:])