#!/usr/bin/python

import json, sys, xml.etree.cElementTree as etree, os.path, plistlib, argparse, codecs

def set_manifest(currentProjectPath, bundle, name, appVersion, droidVersion):
    
    #Android Manifest
    androidmanifest = os.path.join(currentProjectPath, "Properties", "AndroidManifest.xml")
    print 'Attempting to load ' + androidmanifest
    
    if os.path.isfile(androidmanifest):
        print 'PY: Editing ' + androidmanifest
        etree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
        
        with open(androidmanifest, 'r') as handle:
            tree = etree.parse(handle)
        
        if tree:
            root = tree.getroot()
            if root:
                if bundle:
                    root.attrib["package"] = bundle
                
                if appVersion:
                    root.attrib["{http://schemas.android.com/apk/res/android}versionName"] = appVersion
                
                if droidVersion:
                    root.attrib["{http://schemas.android.com/apk/res/android}versionCode"] = str(droidVersion)
                                        
                application = tree.find('application')
                
                if application and name:
                    application.attrib["{http://schemas.android.com/apk/res/android}label"] = name
                
                tree.write(androidmanifest, xml_declaration=True, encoding='utf-8', method="xml")
            else:
                print 'PY: No Root found in manifest'
        else:
            print 'PY: XML Tree failed to be built form manifest'
    else:
        print 'PY: No Android Manifest in project'


def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Set Android Manifest')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath', type=str, help="Path to current project file")
    
    parser.add_argument('bundle', metavar='bundle',type=str, help="Bundle Id")
    
    parser.add_argument('name', metavar='name',type=str, help="App Name")
    
    parser.add_argument('appVersion', metavar='appVersion',type=str, help="App Version")
    
    parser.add_argument('droidVersion', metavar='droidVersion',type=str, help="Droid Version")

    
    args = parser.parse_args()
    
    set_manifest(args.currentProjectPath, args.bundle, args.name, args.appVersion, args.droidVersion)

if __name__ == "__main__":
    main(sys.argv[1:])