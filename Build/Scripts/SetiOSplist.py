#!/usr/bin/python

import json, sys, xml.etree.cElementTree, shutil, zipfile, os as etree, os.path, plistlib, argparse, codecs

def set_plist(currentProjectPath, bundle, name, appVersion):
    
    #iOS plist shizzle
    iosplist = os.path.join(currentProjectPath, "Info.plist")
    print 'Attempting to load ' + iosplist
    if os.path.isfile(iosplist):
        print 'PY: Editing ' + iosplist

        plist = plistlib.readPlist(iosplist)

        if plist:
            if name:
                plist["CFBundleDisplayName"] = name

            if bundle:
                plist["CFBundleIdentifier"] = bundle

            if appVersion:
                plist["CFBundleVersion"] = appVersion
                plist["CFBundleShortVersionString"] = appVersion

            plistlib.writePlist(plist, iosplist)
        else:
            print 'PY: PList failed to load'
    else:
        print 'PY: No Info.plist in project'



def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Set iOS plist')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath', type=str, help="Path to current project file")
    
    parser.add_argument('bundle', metavar='bundle',type=str, help="Bundle Id")
    
    parser.add_argument('name', metavar='name',type=str, help="App Name")
    
    parser.add_argument('appVersion', metavar='appVersion',type=str, help="App Version")
    
    
    args = parser.parse_args()
    
    set_plist(args.currentProjectPath, args.bundle, args.name, args.appVersion)

if __name__ == "__main__":
    main(sys.argv[1:])