#!/usr/bin/python

import json, sys, xml.etree.cElementTree, shutil, zipfile, os as etree, os.path, plistlib, argparse, codecs, exportVariables

def get_themes(currentProject, overiddenThemes):

    # Relative theme file locations
    currentThemeFile = os.path.join(currentProject, "CurrentTheme.txt")
    currentWorkingDir = os.path.join(currentProject, "Theme")
   
    # Default theme file locations
    defaultTheme = "Default"

    parsedFile = False

    # Check to see if our project has an associated theme file. If so, parse it to determine
    # the theme that is to be packaged.
    if (os.path.isfile(currentThemeFile)):
        print "Current Theme Found. Processing..."
        print "------------------------------------------"

        with open(currentThemeFile, 'r') as file:
            line = file.readline()
         
            themeParts = line.split(',');
         
            if (len(themeParts[0]) == 1):
                if (len(themeParts) == 2):

         
                    # If our theme is aimed at default, then set our theme array to include all child
                    # directories. Otherwise, use the themes as defined.
                    if (themeParts[1] == defaultTheme):
                        themeArray = [name for name in os.listdir(currentWorkingDir)
                                      if (os.path.isdir(os.path.join(currentWorkingDir, name)))]
                    else:
                        themeArray = themeParts[1].split(';');
                             
                    print "Current Theme File Value: " + line
                    parsedFile = True
             

    # If an explicit list is not supplied then use all themes in the directory
    # Suppled themes must be of the format Theme1;Theme2;Theme3
    if (overiddenThemes):
        themeArray = overiddenThemes.split(';')
        print "Command Parameter [%s] overriding Themes to " % overiddenThemes
        print '[%s]' % ', '.join(map(str, themeArray))
    else:
        if not (parsedFile):
            themeArray = [name for name in os.listdir(currentWorkingDir)
                          if (os.path.isdir(os.path.join(currentWorkingDir, name)))]
            print "Command Parameter [%s] overriding Themes to " % overiddenThemes
            print "%s" % ", ".join(map(str, themeArray))
                                    
    # Determine the primary theme, either by the theme marked as "Default", or the first theme
    # that was defined.
    hasDefault = False
    primaryTheme = ""
    for theme in themeArray:
        if (theme == defaultTheme):
            hasDefault = True
            primaryTheme = defaultTheme

    if (hasDefault == False):
        primaryTheme = themeArray[0]
   
    print "Extracted Themes: " + ' '.join(themeArray)
    print "Primary Theme: " + primaryTheme
    print "Default Theme: " + defaultTheme
    print "------------------------------------------"

    return [primaryTheme, defaultTheme, themeArray]


def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Build Process.')
    
    parser.add_argument('currentProject', type=str, help="Path to project File")
    parser.add_argument('--variableExport', type=str, default="arguments.txt", help="Path to bamboo compatible export file")
    parser.add_argument('--overiddenThemes', help="Explicit themes to package", required=False)
   
    args = parser.parse_args()

    themes = get_themes(args.currentProject, args.overiddenThemes)

    variables = {'primaryTheme' : themes[0], 'defaultTheme' : themes[1], 'extractedThemes':','.join(themes[2]) }
    exportVariables.write(args.variableExport, variables)

if __name__ == "__main__":
    main(sys.argv[1:])