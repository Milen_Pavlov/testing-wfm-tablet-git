#!/usr/bin/python

import json, sys, subprocess, xml.etree.cElementTree, shutil, zipfile, os as etree, os.path, plistlib, argparse, codecs

import GenerateCurrentThemeArray
import AppVersioning
import EncryptAssets
import GenerateAppMetaData
import CopyResources
import GenerateZIP
import CopyCoreAssets
import SetAndroidManifest
import SetiOSplist
import CopyAssets
import CopyCoreThemeResources
import CopyCoreThemeAssets

def main(argv):
    
    # Parse arguments
    parser = argparse.ArgumentParser(description='Build Process.')
        
    parser.add_argument('currentProject', metavar='cp',type=str, help="Path to project File")
    parser.add_argument('corePath', metavar='c',type=str, help="Path to project File")
    parser.add_argument('--overiddenThemes', help="Explicit themes to package", required=False)
        
    args = parser.parse_args()

    # Set local variables based on provided arguments
    currentProject = args.currentProject
    corePath = args.corePath
    overiddenThemes = args.overiddenThemes
    
    # Check if Droid or iOS
    isDroid = os.path.isfile(currentProject + "/Properties/AndroidManifest.xml")
    isiOS = os.path.isfile(currentProject + "/Info.plist")

    # Work out the themes we are building [primary, default, [all themes]] TODO-Outputfile
    print "**** **** THEMES **** ****"
    themes = GenerateCurrentThemeArray.get_themes(currentProject, overiddenThemes)
    print "**** Result : " + str(themes)
    
        
    # Read in current version [app version, droid version]
    print "**** **** VERSION **** ****"
    version = AppVersioning.get_version(currentProject, None)
    print "**** Result : " + str(version)

    
    # Read in Config [iosBundle, droidBundle, name] TODO-Outputfile
    print "**** **** METADATA **** ****"
    metadata = GenerateAppMetaData.get_metadata(corePath, themes[0], themes[1])
    print "**** Result : " + str(metadata)


    # Encrypt Assets
    print "**** **** ENCRYPT **** ****"
    if(isDroid):
        bundleID = metadata['DroidBundleId']
    else:
        bundleID = metadata['iOSBundleId']

    
    EncryptAssets.encrypt_assets(currentProject, corePath, themes[0], version[0], bundleID)
    print "**** Result : Done"

    # Copy Core Resources
    print "**** **** COPY CORE THEME RESOURCES **** ****"
    CopyCoreThemeResources.copy_resources(corePath, themes[0], themes[1])
    print "**** Result : Done"

    # Copy Core Assets
    print "**** **** COPY CORE THEME ASSETS **** ****"
    CopyCoreThemeAssets.copy_resources(currentProject, corePath, themes[0], themes[1])
    print "**** Result : Done"

    # Copy Theme Resources
    print "**** **** COPY RESOURCES **** ****"
    CopyResources.copy_resources(currentProject, themes[0], themes[1])
    print "**** Result : Done"

    if(isDroid):
        # Set Android Manifest (iOS will do nothing)
        print "**** **** ANDROID **** ****"
        SetAndroidManifest.set_manifest(currentProject, metadata['DroidBundleId'], metadata['Name'], version[0], version[1])
        print "**** Result : Done"


    if(isiOS):
        # Set iOS plist (Droid will do nothing)
        print "**** **** iOS **** ****"
        SetiOSplist.set_plist(currentProject, metadata['iOSBundleId'], metadata['Name'], version[0])
        print "**** Result : Done"

    # Done

if __name__ == "__main__":
   main(sys.argv[1:])