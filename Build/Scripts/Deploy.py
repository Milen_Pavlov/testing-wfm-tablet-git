import argparse
import ast
import sys
import requests
import fnmatch
import os

def upload(artifactMatch, haToken, haAppId):

	artifactPath = None
	matchingFiles = [file for file in os.listdir('.') if fnmatch.fnmatch(file, artifactMatch)]

	if(len(matchingFiles) > 0):
		artifactPath = matchingFiles[0]
		print "Uploading:  {}".format(artifactPath)
	else:
		sys.exit('{} does not match any files in current directory.'.format(artifactMatch))

	# see API documentation here: https://support.hockeyapp.net/kb/api/api-versions
	url = "https://rink.hockeyapp.net/api/2/apps/{}/app_versions/upload".format(haAppId)
	headers = {
		'X-HockeyAppToken': haToken,
		}
	data = {
		"status": 2,# if availability else 1,
		"notify": 1# if notify else 0
	}

	print " ==> url:  {}".format(url)
	print " ==> data:  {}".format(data)
	
	with open(artifactPath, "rb") as artifact:
		files = {'ipa': artifact}
		r = requests.post(url, headers=headers, files=files, data=data)
	
	# Response, status etc
	print "{} response returned".format(r.status_code)
	
	print(r.text)
	
	if r.status_code != requests.codes.created:
		sys.exit("Upload to HockeyApp failed, response output was:\n  {}".format(r.text))


def main(argv):
	# Parse arguments
	parser = argparse.ArgumentParser()
	parser.add_argument('file', help='Pattern of file (IPA or APK) to upload to HockeyApp.')
	parser.add_argument('-tid', '--token', help='HockeyApp Token', required=True)
	parser.add_argument('-aid', '--appid', help='HockeyApp App ID',  required=True)

	args = parser.parse_args()

	upload(args.file, args.token, args.appid)
		 
if __name__ == "__main__":
   main(sys.argv[1:])