#!/usr/bin/python

import json, sys, xml.etree.cElementTree, shutil, zipfile, os as etree, os.path, plistlib, argparse, codecs

def zip_theme(currentProjectPath, coreProjectPath, primaryTheme, defaultTheme, themeArray):
    
    tempThemeFilePath = os.path.join(currentProjectPath, "Assets/Theme")
    themeFilePath = os.path.join(currentProjectPath, "Assets/Theme.zip")

    
    if (os.path.isfile(themeFilePath)):
        os.remove(themeFilePath)
            
    if (os.path.isdir(tempThemeFilePath)):
        shutil.rmtree(tempThemeFilePath)

   	# Copy all core theme items into the themes folder first
    for theme in themeArray:
        currentThemePath = os.path.join(coreProjectPath, "Theme", theme)
        targetDir = os.path.join(tempThemeFilePath, theme)
        themeZipPath = os.path.join(currentThemePath, "ZIP")
        if not (os.path.isdir(targetDir)):
            os.makedirs(targetDir)
        if (os.path.isdir(currentThemePath) and os.path.isdir(themeZipPath)):
            print "Copying Core " + theme + "/ZIP..." 
            copyDirectory(themeZipPath, targetDir)

    # Add all of our extracted themes to our temporary theme package, overwriting any Core ones in the process
    for theme in themeArray:
        currentThemePath = os.path.join(currentProjectPath, "Theme", theme)
        targetDir = os.path.join(tempThemeFilePath, theme)
        themeZipPath = os.path.join(currentThemePath, "ZIP")
        if not (os.path.isdir(targetDir)):
            os.makedirs(targetDir)
        if (os.path.isdir(currentThemePath) and os.path.isdir(themeZipPath)):
            print "Copying Project " + theme + "/ZIP..."
            copyDirectory(themeZipPath, targetDir)

    # Zip up our temporary theme folder and delete once complete
    zippedThemeFile = zipfile.ZipFile(themeFilePath, 'w', zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(tempThemeFilePath):
        for file in files:
            targetFile = os.path.join(root, file)
            print targetFile
            zippedName = targetFile[len(tempThemeFilePath):]
            zippedThemeFile.write(os.path.join(root, file), zippedName)
    zippedThemeFile.close()
                    
    # Remove temporary theme folder.
    if (os.path.isdir(tempThemeFilePath)):
        shutil.rmtree(tempThemeFilePath)


def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Generate Zip')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath', type=str, help="Path to current project file")

    parser.add_argument('coreProjectPath', metavar='coreProjectPath', type=str, help="Path to core project file")
    
    parser.add_argument('primaryTheme', metavar='primaryTheme',type=str, help="Primary Theme")
    
    parser.add_argument('defaultTheme', metavar='defaultTheme',type=str, help="Default Theme")
    
    parser.add_argument('themeArray', metavar='themeArray',type=list, help="Theme Array")
    
    args = parser.parse_args()
    
    zip_theme(args.currentProjectPath, args.coreProjectPath, args.primaryTheme, args.defaultTheme, args.themeArray)


# Copies the contents of the source directory (including sub directories) to the destination directory.
# Unlike shutil.copytree, this doesn't care if the destination file/directory already exists - it will
# just overrite it if a file, or add to it if it's a directory.
def copyDirectory(source, destination):
    print "Retrieving files for " + source
    for path in os.listdir(source):
        if(path != ".DS_Store"):
            fullPath = os.path.join(source, path)
            if (os.path.isdir(fullPath)):
                copyDirectory(fullPath, os.path.join(destination, path))
            else:
                print "Copying file " + fullPath + " to " + destination
                shutil.copy2(fullPath, destination)

if __name__ == "__main__":
    main(sys.argv[1:])