import sys

def write(outputFilePath, parameterDictionary, append = False):
	writeType = 'w'
	if(append):
		writeType+='a'

	with open(outputFilePath,writeType) as outputFile:
		for k,v in parameterDictionary.iteritems():
			outputFile.write(str(k)+'='+(str(v)))
			outputFile.write('\n')
