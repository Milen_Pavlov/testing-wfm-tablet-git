param([string] $coreProjectPath, [string] $projectPath, [string] $versionString, [string] $bundleId, [string] $overrideTheme = "Default")

Function EncryptFiles([string] $coreProjectPath, [string] $projectPath, [string] $versionString, [string] $bundleId, [string] $overrideTheme = "Default")
{
    echo "Starting task Pre-build Encryption step powershell"
    echo "--------------------------------------------------"

    echo 'Build directory: ' $buildDirectory
    echo "Starting task Pre-build Encryption step powershell"
    echo 'Version to use: ' $versionString
    echo 'BundleId to use: ' $bundleId
    echo 'Project Path: ' $projectPath

    $password = $bundleId + $versionString

    if($password.Length -gt 16)
    {
        $password = $password.Substring($password.Length - 16, 16)
        echo 'password after substring is:' $password
    }

    while($password.Length -lt 16)
    {
        $password = $password + "0"
    }

    echo "Derived password is: " $password
    echo "Override theme: " $overrideTheme

    $themeName = $overrideTheme

    echo "Theme name is:" $themeName
    
    #Read contents of initial file

    $fileToEncryptPath = [io.path]::Combine($coreProjectPath, "Theme", $themeName, "AssetsToEncrypt")

    ForEach($file in Get-ChildItem $fileToEncryptPath)
    {
        echo $fileToEncryptPath
        $fileToEncrypt = $fileToEncryptPath + "\" + $file
      
        $fileData = Get-Content $fileToEncrypt -Raw; 

        [Reflection.Assembly]::LoadWithPartialName("System.Security")

	    # Create a COM Object for AesManaged Cryptography
	    $algorithm = new-Object System.Security.Cryptography.AesManaged

	    # Convert the Passphrase to UTF8 Bytes
	    $encodedPassword = [Text.Encoding]::UTF8.GetBytes($password)

        #todo randomize byte[] values
        [Object]$Random = New-Object  System.Random
        $iv = [Byte[]] (,$Random.Next(0, 255) * 16)

	    # Create the Intersecting Vector 
	    $algorithm.IV = $iv
        $algorithm.Key = $encodedPassword

	    # Starts the New Encryption using the Key and IV   
	    $c = $algorithm.CreateEncryptor()

	    # Creates a MemoryStream to do the encryption in
	    $ms = new-Object IO.MemoryStream

	    # Creates the new Cryptology Stream --> Outputs to $MS or Memory Stream
	    $cs = new-Object Security.Cryptography.CryptoStream $ms,$c,"Write"

	    # Starts the new Cryptology Stream
	    $sw = new-Object IO.StreamWriter $cs

        echo $fileData
	    # Writes the string in the Cryptology Stream
	    $sw.Write($fileData)

	    # Stops the stream writer
	    $sw.Close()

	    # Stops the Cryptology Stream
	    $cs.Close()

	    [byte[]]$result = $ms.ToArray()
	    # Stops writing to Memory
	    $ms.Close()

        #add iv to the result
        $combinedResult = $iv + $result
	    # Converts the array from Base 64 to a string and returns
	    $encryptedContent = [Convert]::ToBase64String($combinedResult)

        echo $encryptedContent
        #push encryptedContent in to file
        $destinationPath = [io.path]::Combine($projectPath, "Assets/Theme", $file.Name)
        echo $destinationPath
        New-Item $destinationPath -type file -Force -Value $encryptedContent;
    }
}

EncryptFiles -coreProjectPath $coreProjectPath -projectPath $projectPath -versionString $versionString -bundleID $bundleId -overrideTheme $overrideTheme 