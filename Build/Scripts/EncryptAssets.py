#!/usr/bin/python

import json, sys, xml.etree.cElementTree as etree, shutil, zipfile, os, os.path, plistlib, argparse, codecs
import base64
import Crypto
import Crypto.Cipher
import re
from Crypto import *
from Crypto import Random
from Crypto.Cipher import AES
from os import listdir
from os.path import isfile, join

def encrypt_assets(currentProjectPath, coreProjectPath, primaryTheme, appVersion, bundle):
    bundleAndVersion = bundle + appVersion
    encryptFile(currentProjectPath, coreProjectPath, bundleAndVersion, primaryTheme)

def encryptFile(currentProjectPath, coreProjectPath, bundleAndVersion, themeName):
    print(bundleAndVersion)
    #important any changes to key generation should be replicated in WFMConfigurationService
    key = generateKey(bundleAndVersion)
    destinationFilePath = os.path.join(currentProjectPath, "Assets/Theme")
    print('PY: Destination Path' + destinationFilePath)
    
    encryptPath(key, os.path.join(coreProjectPath, "Theme", themeName, "AssetsToEncrypt"), destinationFilePath)
    encryptPath(key, os.path.join(currentProjectPath, "Theme", themeName, "AssetsToEncrypt"), destinationFilePath)
                    

def encryptPath(key, parentDir, destinationFilePath):
    print('PY: Parent directory ' + parentDir)
    if(os.path.exists(parentDir)):
        for fileName in [f for f in listdir(parentDir) if isfile(join(parentDir, f))]:
            #read and close file
            if(fileName != ".DS_Store"):
                print('PY: Current file name ' +fileName)
                fullFileNameTheme = os.path.join(parentDir, fileName)
                with open(fullFileNameTheme, 'r+') as existingFile:
                    fileContent = existingFile.read()
                    existingFile.close()
            
                print('PY: Encrypting contents of file....')
                result = encrypt(fileContent, key)
                print('PY: Result ' + result)
                
                #remove file if exists
                
                fullFileName = os.path.join(destinationFilePath, fileName)
                try:
                    os.remove(fullFileName)
                except OSError:
                    pass
    
                #create file again and append encrypted text
                with open(fullFileName, 'w') as newFile:
                        newFile.write(result)


def encrypt( raw, key ):
    BS = 16
    key = base64.b64decode(key)
    pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
    raw = pad(raw)
    iv = Random.new().read(BS)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    res = iv + cipher.encrypt( raw )
    return base64.b64encode(res)

def decrypt( self, enc, key ):
    enc = base64.b64decode(enc)
    BS = 16
    iv = enc[:BS]
    unpad = lambda s : s[:-ord(s[-1])]
    cipher = AES.new(key, AES.MODE_CBC, iv )
    return unpad(cipher.decrypt( enc[BS:] ))

def addZero(key):
    return key + b'0'

def generateKey(bundleAndVersion):
    key = bundleAndVersion
    BS = 16
    #take last 16 chars in greater than BS (16)
    if(len(key) > BS):
        key = key[-BS:]
    print(key)
    #add zeroes if less than BS (16)
    while len(key) < BS:
        key = addZero(key)
                                
    return base64.b64encode(key)

def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Encrypt Asset files.')
    
    parser.add_argument('currentProjectPath', metavar='cp',type=str, help="Path to current project File")
    
    parser.add_argument('coreProjectPath', metavar='c',type=str, help="Path to core project File")
    
    parser.add_argument('primaryTheme', metavar='primaryTheme',type=str, help="Primary Theme")
    
    parser.add_argument('appVersion', metavar='appVersion',type=str, help="App Version")
    
    parser.add_argument('bundle', metavar='bundle',type=str, help="Bundle Name")
    
    args = parser.parse_args()
    
    encrypt_assets(args.currentProjectPath, args.coreProjectPath, args.primaryTheme, args.appVersion, args.bundle)


if __name__ == "__main__":
    main(sys.argv[1:])