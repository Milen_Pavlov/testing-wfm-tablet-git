import json, sys, xml.etree.cElementTree as etree, os.path, plistlib, argparse, codecs, exportVariables
import re
def get_version(currentProjectPath, buildNumber):
    
    androidVersionCode = None
    
    assemblyPath = "{0}{1}".format(currentProjectPath,"/../Properties/AssemblyInfo.VersionInfo.cs")
    
    droidVersionFile = "{0}{1}".format(currentProjectPath,"/../Build/AndroidVersion.txt")

    if(droidVersionFile):
        with open(droidVersionFile, 'r') as codeFile:
            androidVersionCode = codeFile.readline()
    
    print('Android Build Code is: ' + str(androidVersionCode))
        
    appVersion = readFileVersion(assemblyPath, buildNumber)

    semverRegex = r'\d+(\.\d+){2}'
    semver = re.search(semverRegex, appVersion).group()
    
    print 'semver is {}'.format(semver)
    
    return [appVersion, androidVersionCode, semver]

def readFileVersion(assemblyInfoPath, newBuildNumber):
    with open(assemblyInfoPath, "r") as existingFile:
        fileContents = existingFile.read()
        existingFile.close()
    
        regex = r'\d+(?:(\.\d+)+)'
        
        result = re.search(regex, fileContents, re.MULTILINE).group()
        
        if(newBuildNumber):
            result = re.sub('\d+$',newBuildNumber,result)
        
        print('App Version is: ' + result)
        
        return result

def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='App versioning')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath',type=str, help="Path to current project")
    parser.add_argument('--buildNumber', metavar='buildNumber', type=str, help="Build number to override the current build number")
    parser.add_argument('--variableExport', type=str, default="arguments.txt", help="Path to bamboo compatible export file")

    args = parser.parse_args()

    versionInfo = get_version(args.currentProjectPath, args.buildNumber)

    variables = {'appVersion' : versionInfo[0], 'androidBuildNumber' : versionInfo[1], 'semanticVersion' : versionInfo[2] }
    exportVariables.write(args.variableExport, variables)

if __name__ == "__main__":
    main(sys.argv[1:])