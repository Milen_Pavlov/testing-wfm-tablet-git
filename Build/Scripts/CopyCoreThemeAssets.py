#!/usr/bin/python

import json, sys, xml.etree.cElementTree, shutil, zipfile, os as etree, os.path, plistlib, argparse, codecs

def copy_resources(currentProjectPath, coreProjectPath, primaryTheme, defaultTheme):
    # Copy the resources for the designated primary theme to our resources directory.
    # We'll copy default, just in case our primary theme (if not default) is missing files.
    assetsFolder = "Assets"
    themeAssetsDir = os.path.join(coreProjectPath, "Theme", primaryTheme, assetsFolder)
    defaultThemeAssetsDir = os.path.join(coreProjectPath, "Theme", defaultTheme, assetsFolder)
    destinationAssetsDir = os.path.join(currentProjectPath, assetsFolder)
                    
    print "Copying Default Assets..."
    copyDirectory(defaultThemeAssetsDir, destinationAssetsDir)
                            
    if (themeAssetsDir != defaultThemeAssetsDir):
        print "Copying Theme Assets..."
        copyDirectory(themeAssetsDir, destinationAssetsDir)


def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Copy Core Theme Resources')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath', type=str, help="Path to current project file")
    
    parser.add_argument('coreProjectPath', metavar='coreProjectPath',type=str, help="Path to core project file")
    
    parser.add_argument('primaryTheme', metavar='primaryTheme',type=str, help="Primary Theme")
    
    parser.add_argument('defaultTheme', metavar='defaultTheme',type=str, help="Default Theme")
    
    args = parser.parse_args()
    
    copy_resources(args.currentProjectPath, args.coreProjectPath, args.primaryTheme, args.defaultTheme)


# Copies the contents of the source directory (including sub directories) to the destination directory.
# Unlike shutil.copytree, this doesn't care if the destination file/directory already exists - it will
# just overrite it if a file, or add to it if it's a directory.
def copyDirectory(source, destination):
    print "Retrieving files for " + source
    for path in os.listdir(source):
        if(path != ".DS_Store"):
            fullPath = os.path.join(source, path)
            if (os.path.isdir(fullPath)):
                copyDirectory(fullPath, os.path.join(destination, path))
            else:
                print "Copying file " + fullPath + " to " + destination
                shutil.copy2(fullPath, destination)


if __name__ == "__main__":
    main(sys.argv[1:])

