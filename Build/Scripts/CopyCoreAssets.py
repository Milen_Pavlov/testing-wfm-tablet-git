#!/usr/bin/python

import json, sys, xml.etree.cElementTree, shutil, zipfile, os as etree, os.path, plistlib, argparse, codecs

def copy_coreassets(currentProjectPath, coreProjectPath):
    assetsFolder = "Assets"
    
    coreAssetsFolder= os.path.join(coreProjectPath, assetsFolder)
    destinationAssetsDir = os.path.join(currentProjectPath, assetsFolder)
    
    
    # Copy all core assets into the assets folder
    copyDirectory(coreAssetsFolder,
                  destinationAssetsDir)


def main(argv):
    # Parse arguments
    parser = argparse.ArgumentParser(description='Copy Core Assets')
    
    parser.add_argument('currentProjectPath', metavar='currentProjectPath', type=str, help="Path to current project file")
    
    parser.add_argument('coreProjectPath', metavar='coreProjectPath',type=str, help="Path to core project file")
    
    args = parser.parse_args()
    
    copy_coreassets(args.currentProjectPath, args.coreProjectPath)


# Copies the contents of the source directory (including sub directories) to the destination directory.
# Unlike shutil.copytree, this doesn't care if the destination file/directory already exists - it will
# just overrite it if a file, or add to it if it's a directory.
def copyDirectory(source, destination):
    print "Retrieving files for " + source
    for path in os.listdir(source):
        if(path != ".DS_Store"):
            fullPath = os.path.join(source, path)
            if (os.path.isdir(fullPath)):
                copyDirectory(fullPath, os.path.join(destination, path))
            else:
                print "Copying file " + fullPath + " to " + destination
                shutil.copy2(fullPath, destination)


if __name__ == "__main__":
    main(sys.argv[1:])

