﻿using System;
using NUnit.Framework;
using AutoMapper;
using System.IO;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WFM.Core.Test
{
    [TestFixture()]
    public class MyHoursTests
    {        
        MyHoursData testData;

        [TestFixtureSetUp]
        public void Setup()
        {
            //Insert type into automapper
            Mapper.AddProfile<MyHoursProfile> ();

            var assembly = typeof(MyHoursTests).GetTypeInfo().Assembly;

            string[] resources = assembly.GetManifestResourceNames();

            Stream stream = assembly.GetManifestResourceStream("WFM.Core.Test.MyHours.MyHoursTestData.json");
            StreamReader r = new StreamReader (stream);
            string testContent = r.ReadToEnd ();

            testData = JsonConvert.DeserializeObject<List<MyHoursData>> (testContent)[0];
        }
        
        [Ignore("My Hours DTO changed but test not kept up to date")]
        [Test]
        public void EnsureSymmetricalSerialization()
        {
            MyHours.WeekData incomingData = Mapper.Map<MyHoursData, MyHours.WeekData> (testData);

            Assert.IsNotNull (incomingData);

            MyHoursData outgoingData = Mapper.Map<MyHours.WeekData,MyHoursData> (incomingData);

            Assert.IsNotNull (outgoingData);

            Assert.IsTrue (VerifyEquality(testData, outgoingData));
        }

        [Test]
        public void ValidateAutomapperConfiguration()
        {
            AutoMapper.Mapper.AssertConfigurationIsValid<MyHoursProfile> ();      
        }

        private bool VerifyEquality(MyHoursData first, MyHoursData second)
        {
            JToken jFirst = JObject.FromObject(first);
            JToken jsecond = JObject.FromObject(second);

            return JToken.DeepEquals (jFirst, jsecond);
        }
    }
}
