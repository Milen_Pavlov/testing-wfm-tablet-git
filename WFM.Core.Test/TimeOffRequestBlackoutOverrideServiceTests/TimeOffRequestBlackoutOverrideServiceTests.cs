﻿using FluentAssertions;
using NUnit.Framework;

namespace WFM.Core.Test
{
    public class TimeOffRequestBlackoutOverrideServiceTests
    {
        private ITimeOffRequestBlackoutOverrideService m_service;


        [Test]
        public void OverringBlackoutPeriodIsPossibleWhenCanOverrideBlacoutPeriodIsOn()
        {
            //Arrange
            m_service = new TimeOffRequestBlackoutOverrideService();

            //Act
            var result = m_service.CanOverrideBlackoutPeriod(true, string.Empty, string.Empty);

            //Assert

            result.Should().Be(true);
        }

        [Test]
        public void OverringBlackoutPeriodIsPossibleWhenCanOverrideBlacoutPeriodIsOff()
        {
            //Arrange
            m_service = new TimeOffRequestBlackoutOverrideService();

            //Act
            var result = m_service.CanOverrideBlackoutPeriod(false, string.Empty, string.Empty);

            //Assert

            result.Should().Be(false);
        }

        [Test]
        public void OverringBlackoutPeriodIsPossibleWhenCanOverrideBlacoutPeriodIsOffAndListIsEmpty()
        {
            //Arrange
            m_service = new TimeOffRequestBlackoutOverrideService();

            //Act
            var result = m_service.CanOverrideBlackoutPeriod(false, string.Empty, string.Empty);

            //Assert

            result.Should().Be(false);
        }

        [Test]
        public void OverringBlackoutPeriodIsPossibleWhenCanOverrideBlacoutPeriodIsOffAndCurrentRequestTypeIsInTheList()
        {
            //Arrange
            m_service = new TimeOffRequestBlackoutOverrideService();

            //Act
            var result = m_service.CanOverrideBlackoutPeriod(false, "Sickness, Vacation", "Sickness");

            //Assert

            result.Should().Be(true);
        }

        [Test]
        public void OverringBlackoutPeriodIsPossibleWhenCanOverrideBlacoutPeriodIsOffAndCurrentRequestTypeIsNotInTheList()
        {
            //Arrange
            m_service = new TimeOffRequestBlackoutOverrideService();

            //Act
            var result = m_service.CanOverrideBlackoutPeriod(false, "Sickness, Vacation", "Paid Leave");

            //Assert

            result.Should().Be(false);
        }

        [Test]
        public void OverringBlackoutPeriodIsPossibleWhenCanOverrideBlacoutPeriodIsOffAndCurrentRequestTypeIsInTheListAndComparisonIsCaseInsensitive()
        {
            //Arrange
            m_service = new TimeOffRequestBlackoutOverrideService();

            //Act
            var result = m_service.CanOverrideBlackoutPeriod(false, "Sickness, Vacation", "sickness");

            //Assert

            result.Should().Be(true);
        }
    }
}

