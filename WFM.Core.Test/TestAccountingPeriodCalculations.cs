﻿//using NUnit.Framework;
//using System;
//using WFM.Core;

//namespace WFM.Core.Test
//{
//    [TestFixture()]
//    public class TestAccountingPeriodCalculations
//    {
//        //All assuming that week starts on a sunday rather than monday?!
//        [Test()]
//        public void IsYearStartCalculatedCorrectly()
//        {
//            DateTime yearstart2014 = new DateTime(2014, 2, 24);
//            DateTime yearstart2015 = new DateTime(2015, 3, 2);
//            DateTime yearstart2016 = new DateTime(2016, 2, 29);

//            Assert.AreEqual(yearstart2014, TescoAccountingHelper.AccountingYearStart(new DateTime(2014, 2, 24), new TescoAccountingHelper.AccountingConfiguration()));
//            Assert.AreEqual(yearstart2014, TescoAccountingHelper.AccountingYearStart(new DateTime(2015,3,1),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(yearstart2015, TescoAccountingHelper.AccountingYearStart(new DateTime(2015, 3, 2), new TescoAccountingHelper.AccountingConfiguration()));
//            Assert.AreEqual(yearstart2015,TescoAccountingHelper.AccountingYearStart(new DateTime(2016,2,28),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(yearstart2016, TescoAccountingHelper.AccountingYearStart(new DateTime(2016, 2, 29), new TescoAccountingHelper.AccountingConfiguration()));
//        }

//        [Test()]
//        public void IsWeekCountCalculatedCorrectly()
//        {
//            Assert.AreEqual(52,            
//                TescoAccountingHelper.GetAccountingPeriodWeeks(new DateTime(2014, 02, 23),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(53,            
//                TescoAccountingHelper.GetAccountingPeriodWeeks(new DateTime(2014, 02, 24),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(53,            
//                TescoAccountingHelper.GetAccountingPeriodWeeks(new DateTime(2014, 03, 02),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(53,            
//                TescoAccountingHelper.GetAccountingPeriodWeeks(new DateTime(2015, 03, 01),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(52,            
//                TescoAccountingHelper.GetAccountingPeriodWeeks(new DateTime(2015, 03, 02),new TescoAccountingHelper.AccountingConfiguration()));

//        }

//        [Test()]
//        public void IsWeekNumberCalculatedCorrectly()
//        {
//            Assert.AreEqual(1,            
//                TescoAccountingHelper.GetWeekNumberForDate(new DateTime(2014, 03, 02),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(2,            
//                TescoAccountingHelper.GetWeekNumberForDate(new DateTime(2014, 03, 09),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(53,            
//                TescoAccountingHelper.GetWeekNumberForDate(new DateTime(2015, 02, 23),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(53,            
//                TescoAccountingHelper.GetWeekNumberForDate(new DateTime(2015, 03, 01),new TescoAccountingHelper.AccountingConfiguration()));

//            Assert.AreEqual(1,            
//                TescoAccountingHelper.GetWeekNumberForDate(new DateTime(2015, 03, 02),new TescoAccountingHelper.AccountingConfiguration()));

//        }
//    }
//}