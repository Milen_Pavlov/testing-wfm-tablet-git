﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Moq;
using NUnit.Framework;

namespace WFM.Core.Test
{
    public class DailyOverviewFilterServiceTests
    {
        private IDailyOverviewFilterService m_dailyOverviewFilterService;
        private Mock<IEmployeeService> m_mockedEmployeeService;
        private Mock<IFileSystem> m_mockedFileService;
        private Mock<IMvxMessenger> m_mockedMvxMessenger;
        private List<FilterGroup> Filters { get; set; }
        private List<int> EmployeesToFilter;
        private List<string> ExceptionsToFilter;
        private List<SiteTimecardEntry> Timecards;
        private MvxSubscriptionToken m_signOutToken;


        public DailyOverviewFilterServiceTests ()
        {
        }


        [SetUp]
        public async Task SetupServices ()
        {
            SetupMocks ();
            m_dailyOverviewFilterService = new DailyOverviewFilterService (m_mockedEmployeeService.Object, m_mockedFileService.Object, m_mockedMvxMessenger.Object);
            m_dailyOverviewFilterService.CurrentView = "Test";
            await m_dailyOverviewFilterService.GenerateFilterGroups (0);
            Filters = m_dailyOverviewFilterService.CurrentFilters;

            EmployeesToFilter = new List<int> () {
                1,
                2,
            };

            ExceptionsToFilter = new List<string> () {
                "In Late",
                "Out Late",
            };

            Timecards = CreateTimecards();
        }

        private void SetupMocks()
        {
            m_mockedEmployeeService = new Mock<IEmployeeService>();
            m_mockedEmployeeService.Setup(s => s.GetAllEmployees(It.IsAny<int>(), It.IsAny<DateTime?>()))
                                   .ReturnsAsync(CreateEmployees());

            m_mockedFileService = new Mock<IFileSystem>();
            m_mockedFileService.Setup(s => s.Exists(It.IsAny<string>()))
                               .Returns(true);
            m_mockedFileService.Setup(s => s.ReadText(It.IsAny<string>()))
                               .Returns(ExceptionsListJson());

            m_mockedMvxMessenger = new Mock<IMvxMessenger>();
            m_mockedMvxMessenger.Setup(s => s.SubscribeOnMainThread<SignOutMessage>(It.IsAny<Action<Consortium.Client.Core.SignOutMessage>> (), MvxReference.Weak, null))
                                .Returns(CreateSignOutToken());
        }

        private string ExceptionsListJson()
        {
            return "{\n\t\"No Show\" : true,\n\t\"Working without schedule\" : true,\n\t\"In Late\" : true,\n\t\"Out Late\" : true,\n\t\"In Early\" : true,\n\t\"Out Early\" : true,\n\t\"Unpaired Punch\" : true,\n}";    
        }

        private JDAEmployeeInfo[] CreateEmployees()
        {
            var result = new List<JDAEmployeeInfo>();

            result.Add(new JDAEmployeeInfo() {
                FirstName = "Employee1",
                LastName = "Test",
                EmployeeID = 1,
            });

            result.Add(new JDAEmployeeInfo() {
                FirstName = "Employee2",
                LastName = "Test",
                EmployeeID = 2,
            });

            result.Add(new JDAEmployeeInfo() {
                FirstName = "Employee3",
                LastName = "Test",
                EmployeeID = 3,
            });

            result.Add(new JDAEmployeeInfo() {
                FirstName = "Employee4",
                LastName = "Test",
                EmployeeID = 4,
            });

            result.Add(new JDAEmployeeInfo() {
                FirstName = "Employee5",
                LastName = "Test",
                EmployeeID = 5,
            });

            return result.ToArray();
        }

        private List<SiteTimecardEntry> CreateTimecards()
        {
            var result = new List<SiteTimecardEntry>();

            result.Add(new SiteTimecardEntry() {
                EmployeeName = "Test, Employee1",
                EmployeeID = 1,
                Exceptions = new List<TimecardException>() {
                    new TimecardException ()
                    {
                        Text = "No Show",
                    },
                },
            });
            result.Add(new SiteTimecardEntry() {
                EmployeeName = "Test, Employee2",
                EmployeeID = 2,
                Exceptions = new List<TimecardException>() {
                    new TimecardException ()
                    {
                        Text = "In Late",
                    },
                },
            });
            result.Add(new SiteTimecardEntry() {
                EmployeeName = "Test, Employee3",
                EmployeeID = 3,
                Exceptions = new List<TimecardException>() {
                    new TimecardException ()
                    {
                        Text = "Out Late",
                    },
                },
            });
            result.Add(new SiteTimecardEntry() {
                EmployeeName = "Test, Employee4",
                EmployeeID = 4,
                Exceptions = new List<TimecardException>() {
                    new TimecardException ()
                    {
                        Text = "Out Late",
                    },
                },
            });
            result.Add(new SiteTimecardEntry() {
                EmployeeName = "Test, Employee5",
                EmployeeID = 5,
            });

            return result;
        }

        private MvxSubscriptionToken CreateSignOutToken()
        {
            if(m_signOutToken != null)
                m_signOutToken = new MvxSubscriptionToken(Guid.NewGuid(), OnDisposeToken);
            
            return m_signOutToken;
        }

        void OnDisposeToken ()
        {
            m_signOutToken?.Dispose();
        }

        private void ApplyFilterForEmployees()
        {
            FilterGroup newFilterGroup = null;
            foreach(var filterGroup in Filters)
            {
                if(filterGroup.Type != DailyOverviewFilterService.EMPLOYEE_FILTER_CODE)
                {
                    continue;
                }

                foreach(var filterItem in filterGroup.Items)
                {
                    if(EmployeesToFilter.Contains(filterItem.ID))
                    {
                        filterItem.Selected = true;
                    }
                }

                newFilterGroup = filterGroup;
                break;
            }

            m_dailyOverviewFilterService.ApplyFilter(newFilterGroup);
        }

        private void ApplyFilterForExceptions()
        {
            FilterGroup newFilterGroup = null;
            foreach(var filterGroup in Filters)
            {
                if(filterGroup.Type != DailyOverviewFilterService.EXCEPTION_FILTER_CODE)
                {
                    continue;
                }

                foreach(var filterItem in filterGroup.Items)
                {
                    if(ExceptionsToFilter.Contains(filterItem.Name))
                    {
                        filterItem.Selected = true;
                    }
                }

                newFilterGroup = filterGroup;
                break;
            } 

            m_dailyOverviewFilterService.ApplyFilter(newFilterGroup);
        }

        private void AssertEmployeeInFilter(List<SiteTimecardEntry> filteredTimecards)
        {
            foreach(var timecard in filteredTimecards)
            {
                if(EmployeesToFilter.Contains(timecard.EmployeeID) == false)
                    Assert.Fail("Timecard for employee that isn't in the filter");
            }
        }

        private void AssertExceptionInFilter(List<SiteTimecardEntry> filteredTimecards)
        {
            foreach(var timecard in filteredTimecards)
            {
                if(timecard.Exceptions == null || timecard.Exceptions.Count <= 0)
                    Assert.Fail("Timecard with no exceptions, when filtering by exceptions");

                foreach(var exception in timecard.Exceptions)
                {
                    if(ExceptionsToFilter.Contains(exception.Text) == false)
                        Assert.Fail("Timecard with exception, that isn't in the filter");
                }
            }
        }

        [Test]
        public void CanFilterByEmployee()
        {
            // Apply the filter for the employees
            ApplyFilterForEmployees();

            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);

            AssertEmployeeInFilter(filteredTimecards);

            Assert.Pass();
        }

        [Test]
        public void CanFilterByException()
        {
            ApplyFilterForExceptions();

            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);

            AssertExceptionInFilter(filteredTimecards);

            Assert.Pass();
        }

        [Test]
        public void CanFilterByEmployeeAndException()
        {
            // Apply the filter for the employees
            ApplyFilterForEmployees();
            ApplyFilterForExceptions();

            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);

            AssertEmployeeInFilter(filteredTimecards);
            AssertExceptionInFilter(filteredTimecards);

            Assert.Pass();
        }

        [Test]
        public void CanClearFilter()
        {
            ApplyFilterForEmployees();
            ApplyFilterForExceptions();

            m_dailyOverviewFilterService.ClearFilter();

            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);
            Assert.AreEqual(filteredTimecards.Count, Timecards.Count, "Timecards don't match filtered timecards after clearing the filter");
        }

        [Test]
        public void HasSeparateFiltersForDifferenViews()
        {
            ApplyFilterForEmployees();
            ApplyFilterForExceptions();

            m_dailyOverviewFilterService.CurrentView = "Test 2";

            var filteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);
            Assert.AreEqual(filteredTimecards.Count, Timecards.Count, "Timecards don't match filtered timecards after switching to a fresh screen");
        }

        [Test]
        public void DoesMaintainFiltersWhenLeavingView()
        {
            ApplyFilterForEmployees();
            ApplyFilterForExceptions();

            var originalFilteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);

            m_dailyOverviewFilterService.CurrentView = "Test 2";

            var newScreenFilteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);
            Assert.AreEqual(newScreenFilteredTimecards.Count, Timecards.Count, "Timecards don't match filtered timecards after switching to a fresh screen");

            m_dailyOverviewFilterService.CurrentView = "Test";
            var returningFilteredTimecards = m_dailyOverviewFilterService.FilterTimecards(Timecards);
            Assert.AreEqual(returningFilteredTimecards.Count, originalFilteredTimecards.Count, "Filter isn't maintained when returning to a view.");
        }

        [Test]
        public void ClearAllFilters()
        {
            ApplyFilterForEmployees();
            ApplyFilterForExceptions();

            m_dailyOverviewFilterService.CurrentView = "Test 2";
            ApplyFilterForEmployees();

            m_dailyOverviewFilterService.ClearAllFilters();

            m_dailyOverviewFilterService.CurrentView = "Test";
            var testFilter = m_dailyOverviewFilterService.CurrentFilters;

            if(testFilter != null)
                Assert.AreEqual(false, testFilter.SelectMany(x => x.Items).Where(y => y.Selected == true).Any(), "Filter isn't empty after clearing all filters"); 

            m_dailyOverviewFilterService.CurrentView = "Test 2";
            testFilter = m_dailyOverviewFilterService.CurrentFilters;
            if(testFilter != null)
                Assert.AreEqual(false, testFilter.SelectMany(x => x.Items).Where(y => y.Selected == true).Any(), "Filter isn't empty after clearing all filters"); 
        }
    }
}

