﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Consortium.Client.Core;
using Moq;
using NUnit.Framework;

namespace WFM.Core.Test
{
    [TestFixture()]
    public class TestTermsOfService
    {
        private Mock<IFileSystem> fileService = new Mock<IFileSystem>();
        private Mock<IESSConfigurationService> configService = new Mock<IESSConfigurationService>();

        private TermsOfUseService m_tosService;

        private string m_testUserID = "12345";
        private string m_testUserID2 = "123456";
        private string m_tosVersion1JSON = "{\n\t\"Version\" : 1 , \"TextFileLocation\" : \"assets://Theme/TermsOfUseText\"}";
        private string m_tosVersion2JSON = "{\n\t\"Version\" : 2 , \"TextFileLocation\" : \"assets://Theme/TermsOfUseText\"}";
        private string m_noAcceptedJSON = "{ }";
        private string m_acceptedVersion1JSON = "{\"12345\":1}";
        private string m_userAcceptedFile = string.Format("{0}/{1}", TermsOfUseService.USER_ACCEPTED_DIR, TermsOfUseService.USER_ACCEPTED_FILENAME);

        [SetUp]
        public void SetupServices()
        {
            configService.Setup(service => service.HasTermsOfServiceEnabled).Returns(true);

            fileService.Setup(service => service.Exists(
                TermsOfUseService.TOS_FILE_PATH))
                       .Returns(true);

            fileService.Setup(service => service.Exists(
                m_userAcceptedFile))
                       .Returns(true);
            
            fileService.Setup(service => service.ReadText(
                TermsOfUseService.TOS_FILE_PATH))
                       .Returns(m_tosVersion1JSON);

            fileService.Setup(service => service.ReadText(
                m_userAcceptedFile))
                       .Returns(m_noAcceptedJSON);

            m_tosService = new TermsOfUseService(fileService.Object,configService.Object);
            m_tosService.ClearAcceptedState(); 
        }

        [Test]
        public void TermsOfServiceIsNotShownIfTurnedOffInConfig()
        {
            configService.Setup(service => service.HasTermsOfServiceEnabled).Returns(false);

            var result = m_tosService.ShouldShowToS(m_testUserID);

            Assert.IsFalse(result);
        }

        [Test]
        public void TermsOfServiceIsNotShownIfOnInConfigButNoToSExist()
        {
            fileService.Setup(service => service.Exists(
                TermsOfUseService.TOS_FILE_PATH))
                       .Returns(false);

            m_tosService.LoadState();

            var result = m_tosService.ShouldShowToS(m_testUserID);

            Assert.IsFalse(result);
        }

        [Test]
        public void TermsOfServiceIsShownIfTurnedOnInConfigAndUserNotAcceptedAndToSExists()
        {
            var result = m_tosService.ShouldShowToS(m_testUserID);

            Assert.IsTrue(result);
        }

        [Test]
        public void TermsOfServiceIsNotShownIfTurnedOnInConfigAndUserHasAcceptedAndToSExists()
        {
            fileService.Setup(service => service.ReadText(
                m_userAcceptedFile))
                       .Returns(m_acceptedVersion1JSON);

            m_tosService.LoadState();

            var result = m_tosService.ShouldShowToS(m_testUserID);

            Assert.IsFalse(result);
        }

        [Test]
        public void TermsOfServiceIsShownIfTurnedOnInConfigAndUserHasAcceptedAnOlderVersionAndToSExists()
        {
            fileService.Setup(service => service.ReadText(
                m_userAcceptedFile))
                       .Returns(m_acceptedVersion1JSON);

            fileService.Setup(service => service.ReadText(
                TermsOfUseService.TOS_FILE_PATH))
                       .Returns(m_tosVersion2JSON);

            m_tosService.LoadState();

            var result = m_tosService.ShouldShowToS(m_testUserID);

            Assert.IsTrue(result);
        }

        [Test]
        public void TermsOfServiceUserIsAcceptedAfterMarkingAsAccepted()
        {
            var result = m_tosService.ShouldShowToS(m_testUserID);

            //Check ToS *should* be shown
            Assert.IsTrue(result);

            //Mark as shown
            m_tosService.MarkUserAsAccepted(m_testUserID);
            fileService.Verify(m => m.WriteText(m_userAcceptedFile, m_acceptedVersion1JSON));

            //Now recheck, should be false
            var result2 = m_tosService.ShouldShowToS(m_testUserID);
            Assert.IsFalse(result2);
        }

        [Test]
        public void TermsOfServiceUserIsStillShownToSTheyHaveNotAcceptedAfterAnotherUserHasAccepted()
        {
            //Load in User 1 accepting
            fileService.Setup(service => service.ReadText(
                m_userAcceptedFile))
                       .Returns(m_acceptedVersion1JSON);

            m_tosService.LoadState();

            var result = m_tosService.ShouldShowToS(m_testUserID2);

            Assert.IsTrue(result);
        }

    }
}