﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace WFM.Core.Test
{
    public class DailyOverviewEditShiftTests
    {
        public DailyOverviewEditShiftTests ()
        {
        }

        private EditShiftData CreateEditShiftData(bool allowJDAMeals, bool allowConfigMeals, bool allowJDABreaks, bool allowConfigBreaks, bool allowConfigJobs)
        {
            JDAPunchesForSchedule punches = GenerateJDAPunchesForSchedule(allowJDAMeals, allowJDABreaks);
            JDAEmployeeInfo[] employees = GenerateEmployeeInfo();
            List<JDASchedulePunches> scheduledPunches = GenerateScheduledPunches();
            List<JDANameAndID> auditReasons = GenerateAuditReasons();

            return new EditShiftData(punches, employees, scheduledPunches, auditReasons, allowConfigMeals, allowConfigBreaks, allowConfigJobs);
        }

        JDAPunchesForSchedule GenerateJDAPunchesForSchedule(bool allowJDAMeals, bool allowJDABreaks)
        {
            JDAPunchesForSchedule result = new JDAPunchesForSchedule();

            result.AllowPunchForMeal = allowJDAMeals;
            result.AllowPunchForBreak = allowJDABreaks;

            // Create punch details (shift, meal, break, job change)
            List<JDAShiftPunches> punches = new List<JDAShiftPunches>();
            punches.Add(new JDAShiftPunches() { EmployeeID = 100001, PunchCode = "s", Start = DateTime.Now, End = DateTime.Now.AddHours(8), StartPunchID = 1 });
            punches.Add(new JDAShiftPunches() { EmployeeID = 100001, PunchCode = "m", Start = DateTime.Now.AddHours(5), End = DateTime.Now.AddHours(6), StartPunchID = 2 });
            punches.Add(new JDAShiftPunches() { EmployeeID = 100001, PunchCode = "b", Start = DateTime.Now.AddHours(2), End = DateTime.Now.AddHours(2).AddMinutes(15), StartPunchID = 3 });
            punches.Add(new JDAShiftPunches() { EmployeeID = 100001, PunchCode = "j", Start = DateTime.Now.AddHours(7), StartPunchID = 4 });

            result.ShiftPunches = punches;

            // Create Job data
            List<JDACanWorkJobs> canWorkJobs = new List<JDACanWorkJobs>();
            canWorkJobs.Add(new JDACanWorkJobs() {Name="TestJob", ID = 1001, IsPrimary = true });
            result.CanWorkJobs = canWorkJobs;
                
            return result;
        }

        JDAEmployeeInfo[] GenerateEmployeeInfo()
        {
            List<JDAEmployeeInfo> result = new List<JDAEmployeeInfo>();
            return result.ToArray();
        }

        List<JDASchedulePunches> GenerateScheduledPunches()
        {
            List<JDASchedulePunches> result = new List<JDASchedulePunches>();
            return result;
        }

        List<JDANameAndID> GenerateAuditReasons()
        {
            List<JDANameAndID> result = new List<JDANameAndID>();
            return result;
        }

        private EditShiftData CreateEditShiftMealData(bool allowJDAMeals, bool allowConfigMeals)
        {
            // Create some edit shift data with meal, break and job change data, jda meals false.
            return CreateEditShiftData(allowJDAMeals, allowConfigMeals, true, true, true);
        }

        private EditShiftData CreateEditShiftBreakData(bool allowJDABreaks, bool allowConfigBreaks)
        {
            // Create some edit shift data with meal, break and job change data, jda meals false.
            return CreateEditShiftData(true, true, allowJDABreaks, allowConfigBreaks, true);
        }

        private EditShiftData CreateEditShiftJobData(bool allowConfigJobs)
        {
            // Create some edit shift data with meal, break and job change data, jda meals false.
            return CreateEditShiftData(true, true, true, true, allowConfigJobs);
        }

        [Test]
        public void TestMealsAreNotFilteredOutWhenAllowMealPunchesIsTrueAndJDADoesAllowMealPunches()
        {
            // Generate edit shift data with the meal JDA true, config value true.
            var data = CreateEditShiftMealData(true, true);
            Assert.IsTrue(data.Details.Any(x => x.PunchCode == PunchCodes.Meal), "Meal Punches have been filtered out incorrectly");
        }

        [Test]
        public void TestMealsAreNotFilteredOutWhenAllowMealPunchesIsTrueAndJDADoesNotAllowMealPunches() 
        {
            // Generate edit shift data with the meal JDA true, config value false.
            var data = CreateEditShiftMealData(true, false);
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Meal), "Meal Punches havn't been filtered out by JDA setting");
        }

        [Test]
        public void TestMealsAreNotFilteredOutWhenAllowMealPunchesIsFalseAndJDADoesAllowMealPunches()
        {
            // Generate edit shift data with the meal JDA false, config value true.
            var data = CreateEditShiftMealData(false, true);
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Meal), "Meal Punches havn't been filtered out by App Config setting");
        }

        [Test]
        public void TestMealsAreNotFilteredOutWhenAllowMealPunchesIsFalseAndJDADoesNotAllowMealPunches()
        {
            var data = CreateEditShiftMealData(false, false);
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Meal), "Meal Punches havn't been filtered out by JDA setting or App Config setting");
        }

        [Test]
        public void TestBreaksAreNotFilteredOutWhenAllowBreakPunchesIsTrueAndJDADoesAllowBreakPunches()
        {
            // Generate edit shift data with the meal JDA true, config value true.
            var data = CreateEditShiftBreakData(true, true);
            Assert.IsTrue(data.Details.Any(x => x.PunchCode == PunchCodes.Break), "Break Punches have been filtered out incorrectly");
        }

        [Test]
        public void TestBreaksAreFilteredOutWhenAllowBreakPunchesIsTrueAndJDADoesNotAllowBreakPunches()
        {
            // Generate edit shift data with the meal JDA true, config value false.
            var data = CreateEditShiftBreakData(true, false);
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Break), "Break Punches havn't been filtered out by JDA setting");
        }

        [Test]
        public void TestBreaksAreFilteredOutWhenAllowBreakPunchesIsFalseAndJDADoesAllowBreakPunches()
        {
            // Generate edit shift data with the meal JDA false, config value true.
            var data = CreateEditShiftBreakData(false, true);
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Break), "Break Punches havn't been filtered out by App Config setting");
        }

        [Test]
        public void TestBreaksAreFilteredOutWhenAllowBreakPunchesIsFalseAndJDADoesNotAllowBreakPunches()
        {
            var data = CreateEditShiftBreakData(false, false);
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Break), "Break Punches havn't been filtered out by JDA setting or App Config setting");
        }

        [Test]
        public void TestJobsAreNotFilteredOutWhenAllowJobPunchesIsTrue()
        {
            var data = CreateEditShiftJobData(true);
            // Generate edit shift data with the jobs config value set to false.
            Assert.IsTrue(data.Details.Any(x => x.PunchCode == PunchCodes.Job),  "Job Punches have been filtered out incorrectly");
        }

        [Test]
        public void TestJobsAreFilteredOutWhenAllowJobPunchesIsFalse()
        {
            var data = CreateEditShiftJobData(false);
            // Generate edit shift data with the jobs config value set to true.
            Assert.IsFalse(data.Details.Any(x => x.PunchCode == PunchCodes.Job), "Job Punches havn't been filtered out by App Config setting");
        }
    }
}

