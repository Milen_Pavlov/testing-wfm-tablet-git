﻿using System;
using NUnit.Framework;

namespace WFM.Core.Test
{
    [TestFixture()]
    public class TestTimespanOverlappingDetection
    {
        //All assuming that week starts on a sunday rather than monday?!
        [Test()]
        public void TestDetectCompleteOverlap()
        {
            DateTime AStart = new DateTime(2000,1,1,0,0,0);
            DateTime AEnd = new DateTime(2000,1,1,23,0,0);

            DateTime BStart = new DateTime(2000,1,1,12,0,0);
            DateTime BEnd = new DateTime(2000,1,1,13,0,0);


            var overlap = TimePeriodHelper.GetOverlap(AStart, AEnd, BStart, BEnd);

            Assert.IsTrue(overlap.TotalHours == 1);

            overlap = TimePeriodHelper.GetOverlap(BStart, BEnd,AStart, AEnd);

            Assert.IsTrue(overlap.TotalHours == 1);
        }

        [Test()]
        public void TestOverlapDetectionCompleteOverlap()
        {
            DateTime AStart = new DateTime(2000,1,1,0,0,0);
            DateTime AEnd = new DateTime(2000,1,1,23,0,0);

            DateTime BStart = new DateTime(2000,1,1,12,0,0);
            DateTime BEnd = new DateTime(2000,1,1,13,0,0);

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(AStart, AEnd, BStart, BEnd));

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(BStart, BEnd,AStart, AEnd));
        }

        //All assuming that week starts on a sunday rather than monday?!
        [Test()]
        public void TestDetectAStartBeforeB()
        {
            DateTime AStart = new DateTime(2000,1,1,6,0,0);
            DateTime AEnd = new DateTime(2000,1,1,23,0,0);

            DateTime BStart = new DateTime(2000,1,1,5,0,0);
            DateTime BEnd = new DateTime(2000,1,1,7,0,0);


            var overlap = TimePeriodHelper.GetOverlap(AStart, AEnd, BStart, BEnd);

            Assert.AreEqual(1,overlap.TotalHours);

            overlap = TimePeriodHelper.GetOverlap(BStart, BEnd,AStart, AEnd);

            Assert.AreEqual(1,overlap.TotalHours);
        }

        [Test()]
        public void TestOverlapDetectionAStartBeforeB()
        {
            DateTime AStart = new DateTime(2000,1,1,6,0,0);
            DateTime AEnd = new DateTime(2000,1,1,23,0,0);

            DateTime BStart = new DateTime(2000,1,1,5,0,0);
            DateTime BEnd = new DateTime(2000,1,1,7,0,0);

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(AStart, AEnd, BStart, BEnd));

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(BStart, BEnd,AStart, AEnd));
        }

        //All assuming that week starts on a sunday rather than monday?!
        [Test()]
        public void TestDetectBEndAfterA()
        {
            DateTime AStart = new DateTime(2000,1,1,0,0,0);
            DateTime AEnd = new DateTime(2000,1,1,7,0,0);

            DateTime BStart = new DateTime(2000,1,1,6,0,0);
            DateTime BEnd = new DateTime(2000,1,1,8,0,0);


            var overlap = TimePeriodHelper.GetOverlap(AStart, AEnd, BStart, BEnd);

            Assert.AreEqual(1,overlap.TotalHours);

            overlap = TimePeriodHelper.GetOverlap(BStart, BEnd,AStart, AEnd);

            Assert.AreEqual(1,overlap.TotalHours);
        }

        [Test()]
        public void TestOverlapDetectionBEndAfterA()
        {
            DateTime AStart = new DateTime(2000,1,1,0,0,0);
            DateTime AEnd = new DateTime(2000,1,1,7,0,0);

            DateTime BStart = new DateTime(2000,1,1,6,0,0);
            DateTime BEnd = new DateTime(2000,1,1,8,0,0);

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(AStart, AEnd, BStart, BEnd));

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(BStart, BEnd,AStart, AEnd));
        }

        //All assuming that week starts on a sunday rather than monday?!
        [Test()]
        public void TestDetectSameTime()
        {
            DateTime AStart = new DateTime(2000,1,1,1,0,0);
            DateTime AEnd = new DateTime(2000,1,1,7,0,0);

            DateTime BStart = new DateTime(2000,1,1,1,0,0);
            DateTime BEnd = new DateTime(2000,1,1,7,0,0);


            var overlap = TimePeriodHelper.GetOverlap(AStart, AEnd, BStart, BEnd);

            Assert.AreEqual(6,overlap.TotalHours);

            overlap = TimePeriodHelper.GetOverlap(BStart, BEnd,AStart, AEnd);

            Assert.AreEqual(6,overlap.TotalHours);
        }

        [Test()]
        public void TestOverlapDetectionSameTime()
        {
            DateTime AStart = new DateTime(2000,1,1,1,0,0);
            DateTime AEnd = new DateTime(2000,1,1,7,0,0);

            DateTime BStart = new DateTime(2000,1,1,1,0,0);
            DateTime BEnd = new DateTime(2000,1,1,7,0,0);

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(AStart, AEnd, BStart, BEnd));

            Assert.IsTrue(TimePeriodHelper.DoesOverlap(BStart, BEnd,AStart, AEnd));
        }

        //All assuming that week starts on a sunday rather than monday?!
        [Test()]
        public void TestDetectOutOfBounds()
        {
            DateTime AStart = new DateTime(2000,1,1,1,0,0);
            DateTime AEnd = new DateTime(2000,1,1,2,0,0);

            DateTime BStart = new DateTime(2000,1,1,2,0,0);
            DateTime BEnd = new DateTime(2000,1,1,10,0,0);

            var overlap = TimePeriodHelper.GetOverlap(AStart, AEnd, BStart, BEnd);

            Assert.AreEqual(0,overlap.TotalHours);

            overlap = TimePeriodHelper.GetOverlap(BStart, BEnd,AStart, AEnd);

            Assert.AreEqual(0,overlap.TotalHours);
        }

        [Test()]
        public void TestOverlapDetectionOutOfBounds()
        {
            DateTime AStart = new DateTime(2000,1,1,1,0,0);
            DateTime AEnd = new DateTime(2000,1,1,2,0,0);

            DateTime BStart = new DateTime(2000,1,1,2,0,0);
            DateTime BEnd = new DateTime(2000,1,1,10,0,0);

            Assert.IsFalse(TimePeriodHelper.DoesOverlap(AStart, AEnd, BStart, BEnd));

            Assert.IsFalse(TimePeriodHelper.DoesOverlap(BStart, BEnd,AStart, AEnd));
        }
    }
}

