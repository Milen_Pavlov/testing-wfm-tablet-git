﻿using System;
using Consortium.Client.Core;
using Moq;
using NUnit.Framework;

namespace WFM.Core.Test
{
    [TestFixture]
    public class TestTimeFormatService
    {        
        [Test]
        public void TimeFormatReadFromLocaliser()
        {
            //Arrange
            var mockStringService = new Mock<IStringService>();
            mockStringService.Setup(service => service.Get("HH:mm")).Returns("hh:mm");
            var timeFormatService = new TimeFormatService(mockStringService.Object);

            //Act
            DateTime testTime = new DateTime(2000,1,1,19,34,56);
            var formattedTime = timeFormatService.FormatTime(testTime);
            
            //Assert
            Assert.AreEqual("07:34",formattedTime);
        }
        
        [Test]
        public void TimeFormatUsesFallback()
        {
            //Arrange
            var mockStringService = new Mock<IStringService>();
            mockStringService.Setup(service => service.Get("HH:mm")).Returns((string)null);
            var timeFormatService = new TimeFormatService(mockStringService.Object);

            //Act
            DateTime testTime = new DateTime(2000,1,1,19,34,56);
            var formattedTime = timeFormatService.FormatTime(testTime);
            
            //Assert
            Assert.AreEqual("19:34",formattedTime);
        }
        
        [Test]        
        public void RulerFormatReadFromLocaliser()
        {
            //Arrange
            var mockStringService = new Mock<IStringService>();
            mockStringService.Setup(service => service.Get("ruler_time_format")).Returns("hh tt");
            var timeFormatService = new TimeFormatService(mockStringService.Object);

            //Act
            DateTime testTime = new DateTime(2000,1,1,19,34,56);
            var formattedTime = timeFormatService.FormatTimeForRuler(testTime);
            
            //Assert
            StringAssert.AreEqualIgnoringCase("07 PM",formattedTime);
        }
        
        [Test]
        public void RulerFormatUsesFallback()
        {
            //Arrange
            var mockStringService = new Mock<IStringService>();
            mockStringService.Setup(service => service.Get("ruler_time_format")).Returns("ruler_time_format");
            var timeFormatService = new TimeFormatService(mockStringService.Object);

            //Act
            DateTime testTime = new DateTime(2000,1,1,19,34,56);
            var formattedTime = timeFormatService.FormatTimeForRuler(testTime);
            
            //Assert
            Assert.AreEqual("19",formattedTime);
        }
        
        [Test]
        public void TimePickerLocaleReadFromLocaliser()
        {
            //Arrange
            var mockStringService = new Mock<IStringService>();
            mockStringService.Setup(service => service.Get("time_picker_locale")).Returns("es");
            var timeFormatService = new TimeFormatService(mockStringService.Object);

            //Act
            var locale = timeFormatService.TimePickerLocale;
            
            //Assert
            Assert.AreEqual("es",locale);
        }
        
        [Test]        
        public void TimePickerLocaleUsesFallback()
        {
            //Arrange
            var mockStringService = new Mock<IStringService>();
            mockStringService.Setup(service => service.Get("time_picker_locale")).Returns("time_picker_locale");
            var timeFormatService = new TimeFormatService(mockStringService.Object);

            //Act
            var formattedTime = timeFormatService.TimePickerLocale;
            
            //Assert
            Assert.AreEqual("nl",formattedTime);
        }
    }
}

