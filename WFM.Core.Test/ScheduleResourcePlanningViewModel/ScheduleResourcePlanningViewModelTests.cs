﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using WFM.Core.Services;

namespace WFM.Core.Test
{
    public class ScheduleResourcePlanningViewModelTests
    {
        private Mock<ScheduleService> m_mockScheduleService;
        private Mock<IMvxMessenger> m_mockMessenger;
        private Mock<ScheduleOvertimeService> m_mockScheduleOvertimeService;
        private Mock<ResourceDataService> m_mockResourceDataService;
        private Mock<IAlertBox> m_mockAlertBox;
        private Mock<SettingsService> m_mockSettingsService;
        private Mock<ILabourDemandService> m_mockLabourDemandService;
        private readonly ScheduleResourcePlanningViewModel _viewModel;
        private Mock<SessionData> m_mockSessionData;

        public ScheduleResourcePlanningViewModelTests ()
        {
            //InitializeMocks();
            //_viewModel = new ScheduleResourcePlanningViewModel(null,
            //                                                   m_mockMessenger.Object,
            //                                                   m_mockScheduleOvertimeService.Object,
            //                                                   m_mockResourceDataService.Object,
            //                                                   m_mockAlertBox.Object,
            //                                                   null,
            //                                                   m_mockLabourDemandService.Object,
            //                                                   m_mockSessionData.Object);
            //SetupMocks();
        }

        void InitializeMocks ()
        {
            m_mockAlertBox = new Mock<IAlertBox>();
            m_mockMessenger = new Mock<IMvxMessenger>();
       
            m_mockSettingsService = new Mock<SettingsService>();
            m_mockResourceDataService = new Mock<ResourceDataService>();
            m_mockLabourDemandService = new Mock<ILabourDemandService>();
            m_mockScheduleOvertimeService = new Mock<ScheduleOvertimeService>();

            //m_mockSessionData = GetMockedSessionData();   
            //m_mockScheduleService = GetMockedScheduleService();

        }

        [SetUp]
        public void SetupMocks()
        {
            m_mockScheduleOvertimeService.Setup(s => s.ReadDepartmentOvertime(It.IsAny<DateTime>(), It.IsAny<bool>()))
                                         .ReturnsAsync(ReturnCollection<Shared.DataTransferObjects.Overtime.OvertimeRecord>());
            m_mockResourceDataService.Setup(s => s.GetLockedData(It.IsAny<DateTime>()))
                                     .ReturnsAsync(ReturnSuccessfulResult<ResourcePlanningData>());
            m_mockResourceDataService.Setup(s => s.ReadOvertimeAgreedValue(It.IsAny<DateTime>()))
                                     .ReturnsAsync(new Result<double?>{Success = true, Object = 0});

            _viewModel.Init();
            
        }         

        //[Test]
        //public async void AbsenceTargetPercentageSetup()
        //{
        //    //Arrange
        //    m_mockLabourDemandService.Setup(s => s.GetAbsenceTargetAsPercentage(It.IsAny<string>(), It.IsAny<string>()))
        //                             .ReturnsAsync(CreateResult());

        //    //Act
        //    await _viewModel.ReadAbsenceTargetPercentageAsync();

        //    //Assert
        //    _viewModel.AbsenceTargetFactor.Should().Be(0.32);
        //}

        //private Mock<ScheduleService> GetMockedScheduleService()
        //{
        //    var mockSessionData = GetMockedSessionData();
        //    var mockRestClient = new Mock<RestClient>();
        //    var mockScheduleUndoService = new Mock<ScheduleUndoService>();
        //    var mockMobileDevice = new Mock<IMobileDevice>();
        //    var mockStringService = new Mock<IStringService>();
        //    var mockRaygunService = new Mock<IRaygunService>();

        //    return new Mock<ScheduleService>(mockSessionData.Object, mockRestClient.Object, m_mockMessenger.Object, m_mockAlertBox.Object, m_mockSettingsService.Object,
        //                                     mockScheduleUndoService.Object, mockStringService.Object, mockRaygunService.Object);
        //}

        private Mock<SessionData> GetMockedSessionData ()
        {
            var mockRestClient = new Mock<RestClient>();
            var mockStaticDataService = new Mock<StaticDataService>();
            var mockStoredCredentialService = new Mock<IStoredCredentialsService>();
            var mockUserDetailService = new Mock<IUserDetailsService>();
            //settings service
            var mockMobileApp = new Mock<IMobileApp>();
            var mockScheduleUndoService = new Mock<ScheduleUndoService>();
            var mockMobileDevice = new Mock<IMobileDevice>();
            var mockMobileAlalytics = new Mock<IAnalyticsService>();
            var mockStringService = new Mock<IStringService>();
            var mockRaygunService = new Mock<IRaygunService>();
            var mockMessenger = new Mock<IMvxMessenger>();
            var mockSettingsService = new Mock<SettingsService>();
            return new Mock<SessionData>(mockRestClient.Object, mockStaticDataService.Object, mockMessenger.Object,
                                        mockStoredCredentialService.Object, mockUserDetailService.Object, mockSettingsService.Object,
                                        mockMobileApp.Object, mockMobileDevice.Object, mockMobileAlalytics.Object,
                                         mockRaygunService.Object);
        }

        private Result<LabourDemandResponse> CreateResult ()
        {
            return new Result<LabourDemandResponse>()
            {
                Object  = new LabourDemandResponse()
                {
                    StoreAbsenceTargets  = new StoreAbsenceTargets
                    { 
                        StoreNumber = 1, 
                        AbsenceTargets = new AbsenceTargets
                        {
                            TescoYearWeek = 201628,
                            AbsenceTargetAsFactor = 0.32M,
                            AbsenceTargetAsPercentage = 32M
                        } 
                    }
                }
            };
        }

        private IList<T> ReturnCollection<T>()
            where T : class, new()
        {
            return new List<T>{
                new T()
            };
        }


        private Result<T> ReturnSuccessfulResult<T>()
            where T : class, new()
        {
            return new Result<T>
            {
                Success = true,
                Object = new T()
            };
        }
    }
}

