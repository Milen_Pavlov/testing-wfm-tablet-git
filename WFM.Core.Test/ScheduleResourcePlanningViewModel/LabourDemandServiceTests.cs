﻿using System;
using System.Threading.Tasks;
using Consortium.Client.Core;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using Siesta;

namespace WFM.Core.Test
{
    [TestFixture]
    public class LabourDemandServiceTests
    {
        private LabourDemandService m_labourDemandService;

        private Mock<LabourDemandRestClient> m_labourDemandRestClient;
        private Mock<ISettingsService> m_mockSettingsService;

        //[Test]
        //public async Task RestClientReturnsCorrectResponse()
        //{
        //    //Arrange 
        //    m_labourDemandRestClient.Setup(c => c.Get<LabourDemandResponse>(It.IsAny<AbsenceTargetRequest>()))
        //                                     .ReturnsAsync(CreateResult());           
        //    m_labourDemandService = new LabourDemandService(m_labourDemandRestClient.Object);
   
        //    //Act
        //    var result = await m_labourDemandService.GetAbsenceTargetAsPercentage("2055", "201628");

        //    //Assert
        //    result.Should().NotBeSameAs(null);
        //    result.Object.StoreAbsenceTargets.AbsenceTargets.AbsenceTargetAsFactor.Should().Be(0.32);
        //}

        //[SetUp]
        //public void SetupMocks()
        //{
        //    m_mockSettingsService = new Mock<ISettingsService>();
        //    m_mockSettingsService.Setup(s => s.AppConfig)
        //                         .Returns(new ApplicationConfiguration
        //    {
        //        RestClientTimeoutSecs = 10
        //    });
        //    m_mockSettingsService.Setup(s => s.Current)
        //                         .Returns(new AppSettings
        //    {
        //        Env = new Environment{
        //            LabourDemandAddress = "https://testwsscaap.dev.global.tesco.org/labourDemand/V1/"
        //        }
        //    });

        //    m_labourDemandRestClient = new Mock<LabourDemandRestClient>(m_mockSettingsService.Object);
        //    m_labourDemandRestClient.Setup(c => c.BaseUri)
        //                                .Returns(new Uri("https://testwsscaap.dev.global.tesco.org/labourDemand/V1"));
        //    m_labourDemandRestClient.Setup(c => c.TimeoutMs).Returns(5);
        //}

        private RestReply<LabourDemandResponse> CreateResult ()
        {
            return new RestReply<LabourDemandResponse>
            {
                Object = new LabourDemandResponse()
                {
                    StoreAbsenceTargets  = new StoreAbsenceTargets
                    { 
                        StoreNumber = 1, 
                        AbsenceTargets = new AbsenceTargets
                        {
                            TescoYearWeek = 201628,
                            AbsenceTargetAsFactor = 0.32m,
                            AbsenceTargetAsPercentage = 32
                        } 
                    }
                }
            };
        }
    }
}

