﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Consortium.Client.Core;
using Moq;
using NUnit.Framework;

namespace WFM.Core.Test
{
    [TestFixture()]
    public class TestCallOffShiftLogic
    {
        private readonly JDATimeOffType holidayTimeOffType = new JDATimeOffType()
        {
            TypeID = 1,
            TypeName = "Holiday"
        };
        
        private readonly JDATimeOffType absenceTimeOffType = new JDATimeOffType()
        {
            TypeID = 2,
            TypeName = "Absence"
        };
        
        private Mock<ITimeOffService> timeOffService = new Mock<ITimeOffService>();
        private Mock<IStringService> stringService = new Mock<IStringService>();
        private Mock<IESSConfigurationService> mockConfig = new Mock<IESSConfigurationService>();
        
        private ICallOffService m_callOffService;
        
        [SetUp]
        public void SetupServices()
        {
            Result<IEnumerable<JDATimeOffType>> twoHolidayTypeResult = new Result<IEnumerable<JDATimeOffType>>()
            {
                Object = new List<JDATimeOffType> {holidayTimeOffType, absenceTimeOffType},
                Success = true
            };
            
            Result<JDAProcessingResult> raiseHolidayResult = new Result<JDAProcessingResult>();
            raiseHolidayResult.Success = true;
            
            timeOffService.Setup(service => service.GetTimeOffTypesForEmployee(
                It.IsAny<int>()))
            .Returns(Task.FromResult(twoHolidayTypeResult));
                
            timeOffService.Setup(service => service.CreateAndApproveHoliday(
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<List<PayAllocation>>(),
                It.IsAny<bool>()))
            .Returns(Task.FromResult(raiseHolidayResult));

            stringService.Setup(m => m.Get(It.IsAny<string>())).Returns("bleep bloop {0}");
            
            m_callOffService = new CallOffService(timeOffService.Object,stringService.Object);
            
            timeOffService.ResetCalls();
        }
        
        [Test]
        public void CanSingleShiftBeCalledOff()
        {
            var result = m_callOffService.CallOffShift(1,2, new DateTime(2016,1,1,15,0,0), new DateTime(2016,1,1,22,0,0), "Absence" ).Result;
            VerifyTimeOffCalled(Times.Once());
            Assert.IsTrue(result.Success);
        }
        
        [Test]
        public void CanSingleShiftOverlappingMidnightBeCalledOff()
        {
            var result = m_callOffService.CallOffShift(1,2,new DateTime(2016,1,1,15,0,0) ,new DateTime(2016,1,2,9,0,0), "Absence"  ).Result;
            
            VerifyTimeOffCalled(Times.Once());
                
            Assert.IsTrue(result.Success);
        }
        
        [Test]
        public void CanSingleDayBeCalledOff()
        {
            var result = m_callOffService.CallOffDays(1,2,new DateTime[] { new DateTime(2016,1,1) }, "Absence" ).Result;
            
            VerifyTimeOffCalled(Times.Once());
                
            Assert.IsTrue(result.Success);
        }
        
        private void VerifyTimeOffCalled(Times times)
        {
            timeOffService.Verify(x => x.CreateAndApproveHoliday(
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<List<PayAllocation>>(),
                It.IsAny<bool>()),times);     
        }
        
        [Test]
        public void CanMultipleDaysBeCalledOff()
        {            
            var result = m_callOffService.CallOffDays(1,2,new DateTime[] { new DateTime(2016,1,1), new DateTime(2016,1,2) }, "Absence" ).Result;
            
            VerifyTimeOffCalled(Times.Exactly(2));
            
            Assert.IsTrue(result.Success);
        }
        
        [Test]
        public void DoesServiceHandleFailedTypeLookup()
        {   
            Result<IEnumerable<JDATimeOffType>> failedHolidayTypeResult = new Result<IEnumerable<JDATimeOffType>>()
            {
                Object = null,
                Success = false
            };

            timeOffService.Setup(service => service.GetTimeOffTypesForEmployee(
                It.Is<int>(x => x == 100)))
            .Returns(Task.FromResult(failedHolidayTypeResult));   
        
            var result = m_callOffService.CallOffDays(1,100,new DateTime[] { new DateTime(2016,1,1), new DateTime(2016,1,1) }, "Absence" ).Result;
            
            Assert.IsFalse(result.Success);
            Assert.AreEqual((int)CallOffService.CallOffErrorType.UnableToLoadTimeOffList,result.Code);
        }
        
        [Test]
        public void DoesServiceHandleMismatchedTypeLookup()
        {
            var result = m_callOffService.CallOffDays(1,100,new DateTime[] { new DateTime(2016,1,1), new DateTime(2016,1,1) }, "Abcess" ).Result;

            Assert.IsFalse(result.Success);
            Assert.AreEqual((int)CallOffService.CallOffErrorType.CannotRaiseCallOffType,result.Code);
        }
        
        
        [Test]
        public void DoesServiceHandleFailedHolidayCreation()
        {
            Result<JDAProcessingResult> failedRaiseHolidayResult = new Result<JDAProcessingResult>();
            failedRaiseHolidayResult.Success = false;
            
            timeOffService.Setup(service => service.CreateAndApproveHoliday(
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<List<PayAllocation>>(),
                It.IsAny<bool>()))
            .Returns(Task.FromResult(failedRaiseHolidayResult));
            
            var result = m_callOffService.CallOffDays(1,100,new DateTime[] { new DateTime(2016,1,1), new DateTime(2016,1,1) }, "Absence" ).Result;

            Assert.IsFalse(result.Success);
            Assert.AreEqual((int)CallOffService.CallOffErrorType.ErrorDuringHolidayCreation,result.Code);
        }
        
        [Test]
        public void DoesServiceHandlePartiallyFailedHolidayCreation()
        {
            Result<JDAProcessingResult> failedRaiseHolidayResult = new Result<JDAProcessingResult>();
            failedRaiseHolidayResult.Success = false;
            
            Result<JDAProcessingResult> successfulRaiseHolidayResult = new Result<JDAProcessingResult>();
            failedRaiseHolidayResult.Success = false;
            
            timeOffService.Setup(service => service.CreateAndApproveHoliday(
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<List<PayAllocation>>(),
                It.IsAny<bool>()))
            .ReturnsInOrder(Task.FromResult(successfulRaiseHolidayResult),Task.FromResult(failedRaiseHolidayResult));
            
            var result = m_callOffService.CallOffDays(1,100,new DateTime[] { new DateTime(2016,1,1), new DateTime(2016,1,2) }, "Absence" ).Result;

            Assert.IsFalse(result.Success);
            Assert.AreEqual((int)CallOffService.CallOffErrorType.ErrorDuringHolidayCreation,result.Code);
        }
        
        [Test]
        public void DoesServiceHandleNullDateCollection()
        {
            var result = m_callOffService.CallOffDays(1,100,null, "Absence" ).Result;

            Assert.IsFalse(result.Success);
            Assert.AreEqual((int)CallOffService.CallOffErrorType.InvalidCallOffDates,result.Code);
        }
        
        [Test]
        public void DoesServiceHandleEmptyDateCollection()
        {
            var result = m_callOffService.CallOffDays(1,100,new List<DateTime>(), "Absence" ).Result;
            Assert.IsFalse(result.Success);
            Assert.AreEqual((int)CallOffService.CallOffErrorType.InvalidCallOffDates,result.Code);
        }
    }
}
