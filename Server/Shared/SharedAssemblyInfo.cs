using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("Repl Digital")]
[assembly: AssemblyCopyright("Copyright © Repl Digital 2015")]
[assembly: AssemblyTrademark("Repl Digital")]

[assembly: AssemblyConfiguration("")]

// Version information follows the SemVer specification:
//
//      Major.Minor.Patch-Option
// 
//      Major: Any breaking changes
//      Minor: Backwards compatable features
//      Patch: Internal Bug fixes
//      Option: Can be used to tag development/prerelease nuget packages
//
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]   // This is the version that nuget uses
[assembly: AssemblyVersion("1.0.0")]
