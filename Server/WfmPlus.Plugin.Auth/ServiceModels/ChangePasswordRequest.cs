﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    [ConsortiumRoute("/wfmauth/changepassword", "POST", "Changes a user's password using a token to identify the user.")]
    public class ChangePasswordRequest
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
