﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    public class JdaLogInData
    {
        public string Module { get; set; }
        public string SiteId { get; set; }
    }
}
