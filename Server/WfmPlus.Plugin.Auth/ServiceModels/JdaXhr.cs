﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.ModelBinders;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    [ModelBinder(typeof(JsonModelBinder))]
    public class JdaXhr
    {
        public string Status { get; set; }
        public JdaResponse Response { get; set; }
    }
}
