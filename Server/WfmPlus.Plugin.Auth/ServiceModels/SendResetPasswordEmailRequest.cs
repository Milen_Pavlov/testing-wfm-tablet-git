﻿using Consortium.Model.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    [ConsortiumRoute("/wfmauth/sendresetpasswordemail", "POST", "Sends an email, to the given user, containing a secure link to change their password")]
    public class SendResetPasswordEmailRequest
    {
        public string Username { get; set; }
    }
}
