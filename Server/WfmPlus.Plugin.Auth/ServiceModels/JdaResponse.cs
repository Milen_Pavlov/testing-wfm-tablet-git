﻿using Consortium.Service.Engine;
using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    public class JdaResponse
    {
        public IAppSettingsRepository AppSettings { get; set; }
        public int Count { get; set; }
        public JdaResponseStatus Status { get; set; }
        public JdaLogInData Data { get; set; }
        public string Type;

        public JdaResponse()
        {
            AppSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();
        }

        public string EssHomeUrl
        {
            get
            {
                var url = AppSettings.JdaHomePath;
                if (Data != null)
                {
                    if (!string.IsNullOrWhiteSpace(Data.SiteId))
                    {
                        url = url.Replace("{siteId}", Data.SiteId);
                    }
                    if (!string.IsNullOrWhiteSpace(Data.Module))
                    {
                        url = url.Replace("{module}", Data.Module);
                    }
                }

                var formatItemIndex = url.IndexOf('{');
                if (formatItemIndex != -1)
                {
                    url = url.Substring(0, formatItemIndex);
                }

                return url;
            }
        }
    }
}
