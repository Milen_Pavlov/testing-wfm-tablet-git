﻿using Consortium.Service.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    public class ChangePasswordResponse
    {
        public IList<Message> Messages { get; set; }

        public ChangePasswordResponse()
        {
            Messages = new List<Message>();
        }
    }
}
