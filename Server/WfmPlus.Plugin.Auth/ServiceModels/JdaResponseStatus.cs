﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    public enum JdaResponseStatus
    {
        Null = -1,
        Success = 0,
        InvalidCredentials = 1200,
        PasswordExpired = 1201,
        TooManyLogInAttempts = 1205,
        CurrentPasswordIncorrect = 1302,
        PasswordCannotBeSameAsPreviousPassword = 1307,
        PasswordTooShort = 1303,
        PasswordCannotContainUsernameOrName = 1306,
        PasswordMustContainNonLetter = 1305,
        PasswordMustContainUppercaseLetter = 1304,
        //PasswordLettersMustBeUppercase = ???, // todo: cannot replicate in the JDA log in page, so cannot get the error code
    }
}
