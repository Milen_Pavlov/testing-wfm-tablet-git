﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ServiceModels
{
    public class SendResetPasswordEmailResponse
    {
        public IList<Message> Messages { get; set; }

        public SendResetPasswordEmailResponse()
        {
            Messages = new List<Message>();
        }
    }
}
