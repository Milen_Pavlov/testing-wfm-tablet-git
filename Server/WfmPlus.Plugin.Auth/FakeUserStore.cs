﻿using Consortium.Service.Engine;
using Consortium.Service.Engine.Authentication;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth
{

    public class FakeUserStore : IUserPasswordStore<ConsortiumAspnetUser, Guid>, IUserRoleStore<ConsortiumAspnetUser, Guid>, IQueryableUserStore<ConsortiumAspnetUser, Guid>, IRoleProvider
    {
        public IQueryable<ConsortiumAspnetUser> Users
        {
            get { throw new NotImplementedException(); }
        }

        public string ApplicationName { get; set; }

        public async Task<string> GetPasswordHashAsync(ConsortiumAspnetUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> HasPasswordAsync(ConsortiumAspnetUser user)
        {
            throw new NotImplementedException();
        }

        public async Task SetPasswordHashAsync(ConsortiumAspnetUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(ConsortiumAspnetUser user)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteAsync(ConsortiumAspnetUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<ConsortiumAspnetUser> FindByIdAsync(Guid userId)
        {
            throw new NotImplementedException();
        }

        public async Task<ConsortiumAspnetUser> FindByNameAsync(string userName)
        {
            return new ConsortiumAspnetUser();
        }

        public async Task UpdateAsync(ConsortiumAspnetUser user)
        {
            throw new NotImplementedException();
        }

        public async Task AddToRoleAsync(ConsortiumAspnetUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<string>> GetRolesAsync(ConsortiumAspnetUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> IsInRoleAsync(ConsortiumAspnetUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public async Task RemoveFromRoleAsync(ConsortiumAspnetUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public string[] GetRolesForUser(string username)
        {
            throw new NotImplementedException();
        }

        public string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }
    }
}
