﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.Models;

namespace WfmPlus.Plugin.Auth.ModelBinders
{
    public class JsonModelBinder : IModelBinder
    {
        object IModelBinder.BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var jsonValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelMetadata.PropertyName);
            if (jsonValue == null || jsonValue.AttemptedValue == null)
            {
                return null;
            }

            JObject json = null;
            try
            {
                json = JObject.Parse(jsonValue.AttemptedValue);
            }
            catch
            {
            }

            if (json == null)
            {
                return null;
            }

            var model = json.ToObject(bindingContext.ModelMetadata.ModelType);
            return model;
        }
    }
}
