﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Newtonsoft.Json.Linq;
using WfmPlus.Plugin.Auth.Models;
using Consortium.Service.Engine.Helpers;
using Consortium.Service.Engine;
using System.Collections.Generic;
using WfmPlus.Plugin.Auth.ViewModels;
using WfmPlus.Plugin.Auth.Helpers;
using WfmPlus.Plugin.Auth.ServiceModels;

namespace WfmPlus.Plugin.Auth.Controllers
{
    public class LogInController : Controller
    {
        public ActionResult Index()
        {
            var viewModel = new LogInViewModel();
            viewModel.Username = Session.WfmAuth().Username;
            return View("~/Views/LogIn.cshtml", viewModel);
        }

        [HttpPost]
        public ActionResult Index(LogInViewModel viewModel)
        {
            Session.WfmAuth().Username = viewModel.Username;
            switch (viewModel.ActionName)
            {
                case "LogIn":
                {
                    return LogIn(viewModel);
                }
                case "ResetPassword":
                {
                    return ResetPassword(viewModel);
                }
                default:
                {
                    throw new NotSupportedException();
                }
            }
        }

        [NonAction]
        private ActionResult LogIn(LogInViewModel viewModel)
        {
            var layoutViewModel = LayoutViewModel.Current(ViewData, (object)ViewBag);

            if (string.IsNullOrWhiteSpace(viewModel.Username))
            {
                layoutViewModel.AddMessage(new Message(MessageCode.UsernameRequired));
            }
            else if (string.IsNullOrWhiteSpace(viewModel.Password))
            {
                layoutViewModel.AddMessage(new Message(MessageCode.PasswordRequired));
            }
            else
            {
                var status = (viewModel == null || viewModel.Xhr == null || viewModel.Xhr.Response == null ? JdaResponseStatus.Null : viewModel.Xhr.Response.Status);
                switch (status)
                {
                    case JdaResponseStatus.InvalidCredentials:
                    {
                        layoutViewModel.AddMessage(new Message(MessageCode.InvalidCredentials));
                        break;
                    }
                    case JdaResponseStatus.TooManyLogInAttempts:
                    {
                        layoutViewModel.AddMessage(new Message(MessageCode.TooManyLogInAttempts));
                        break;
                    }
                    case JdaResponseStatus.PasswordExpired:
                    {
                        TempData.WfmAuth().Messages.Add(new Message(MessageCode.PasswordHasExpired));
                        return RedirectToRoute("WfmAuth.ChangePassword");
                    }
                    case JdaResponseStatus.Success:
                    {
                        Response.Redirect(viewModel.Xhr.Response.EssHomeUrl);
                        return new EmptyResult();
                    }
                    default:
                    {
                        layoutViewModel.AddMessage(new Message(MessageCode.LogInFailed));
                        break;
                    }
                }
            }

            return View("~/Views/LogIn.cshtml", viewModel);
        }

        [NonAction]
        private ActionResult ResetPassword(LogInViewModel viewModel)
        {
            return RedirectToRoute("WfmAuth.ResetPassword");
        }
    }
}
