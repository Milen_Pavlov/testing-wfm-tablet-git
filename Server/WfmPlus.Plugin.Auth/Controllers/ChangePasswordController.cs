﻿using System;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.ViewModels;
using WfmPlus.Plugin.Auth.Helpers;
using WfmPlus.Plugin.Auth.Models;
using WfmPlus.Plugin.Auth.Security;
using Newtonsoft.Json;
using System.Web;
using WfmPlus.Plugin.Auth.ServiceModels;
using WfmPlus.Plugin.Auth.Services;
using Consortium.Service.Engine;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using WfmPlus.Plugin.Auth.Repositories;
using Consortium.Utility.RestClient;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.Controllers
{
    public class ChangePasswordController : Controller
    {
        public IRestClient RestClient { get; set; }
        public IJdaRepository JdaRepository { get; set; }

        public ChangePasswordController(IRestClient restClient)
        {
            RestClient = restClient;
            JdaRepository = ConsortiumApp.IOC.Resolve<IJdaRepository>();
        }

        public ActionResult Index(string token)
        {
            var tokenObject = ResetPasswordToken.FromToken(token);
            var viewModel = new ChangePasswordViewModel();

            switch (tokenObject.ParseResult)
            {
                case ResetPasswordTokenParseResult.NoToken:
                {
                    break;
                }
                case ResetPasswordTokenParseResult.ValidToken:
                {
                    Session.WfmAuth().Username = tokenObject.Username;
                    break;
                }
                default:
                {
                    Session.WfmAuth().Username = null; // do not set username to prevent token sniffing
                    TempData.WfmAuth().Messages.Add(new Message(MessageCode.PasswordResetTokenExpired));
                    return RedirectToRoute("WfmAuth.ResetPassword");
                }
            }

            viewModel.Token = token;
            viewModel.Username = Session.WfmAuth().Username;
            return View("~/Views/ChangePassword.cshtml", viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(ChangePasswordViewModel viewModel)
        {
            Session.WfmAuth().Username = viewModel.Username;
            switch (viewModel.ActionName)
            {
                case "ChangeExpiredPassword":
                {
                    return ChangeExpiredPassword(viewModel);
                }
                case "ChangeForgottenPassword":
                {
                    return await ChangeForgottenPassword(viewModel);
                }
                case "Cancel":
                {
                    return Cancel(viewModel);
                }
                default:
                {
                    throw new NotSupportedException();
                }
            }
        }

        [NonAction]
        private ActionResult ChangeExpiredPassword(ChangePasswordViewModel viewModel)
        {
            var layoutViewModel = LayoutViewModel.Current(ViewData, (object)ViewBag);

            // validate
            if (string.IsNullOrWhiteSpace(viewModel.Username))
            {
                TempData.WfmAuth().Messages.Add(new Message(MessageCode.SessionExpiredLogInAgain));
                return RedirectToRoute("WfmAuth.LogIn");
            }
            else if (string.IsNullOrWhiteSpace(viewModel.CurrentPassword))
            {
                layoutViewModel.AddMessage(new Message(MessageCode.CurrentPasswordRequired));
            }
            else if (string.IsNullOrWhiteSpace(viewModel.NewPassword1))
            {
                layoutViewModel.AddMessage(new Message(MessageCode.NewPasswordRequired));
            }
            else if (viewModel.NewPassword1 != viewModel.NewPassword2)
            {
                layoutViewModel.AddMessage(new Message(MessageCode.NewPasswordsNotEqual));
            }
            else
            {
                var status = (viewModel == null || viewModel.Xhr == null || viewModel.Xhr.Response == null ? JdaResponseStatus.Null : viewModel.Xhr.Response.Status);
                switch (status)
                {
                    case JdaResponseStatus.CurrentPasswordIncorrect:
                    {
                        layoutViewModel.AddMessage(new Message(MessageCode.CurrentPasswordIncorrect));
                        break;
                    }
                    case JdaResponseStatus.PasswordCannotBeSameAsPreviousPassword:
                    {
                        layoutViewModel.AddMessage(new Message(MessageCode.PasswordCannotBeSameAsPreviousPassword));
                        break;
                    }
                    case JdaResponseStatus.PasswordCannotContainUsernameOrName:
                    {
                        var user = JdaRepository.ReadUserByUsername(viewModel.Username);
                        var firstName = (user == null ? null : user.FirstName);
                        var lastName = (user == null ? null : user.LastName);
                        layoutViewModel.AddMessage(new Message(MessageCode.PasswordCannotContainUsernameOrName, firstName, lastName));
                        break;
                    }
                    case JdaResponseStatus.PasswordTooShort:
                    case JdaResponseStatus.PasswordMustContainNonLetter:
                    case JdaResponseStatus.PasswordMustContainUppercaseLetter:
                    //case JdaResponseStatus.PasswordLettersMustBeUppercase:
                    {
                        layoutViewModel.AddMessage(new Message(MessageCode.InvalidPasswordFormat));
                        break;
                    }
                    case JdaResponseStatus.Success:
                    {
                        Response.Redirect(viewModel.Xhr.Response.EssHomeUrl);
                        return new EmptyResult();
                    }
                    default:
                    {
                        // consider differentiating between a change password fail and a log in fail (in theory this shouldn't happen)
                        layoutViewModel.AddMessage(new Message(MessageCode.UnableToChangePassword));
                        break;
                    }
                }
            }

            return View("~/Views/ChangePassword.cshtml", viewModel);
        }

        [NonAction]
        private async Task<ActionResult> ChangeForgottenPassword(ChangePasswordViewModel viewModel)
        {
            var layoutViewModel = LayoutViewModel.Current(ViewData, (object)ViewBag);

            // validate
            if (string.IsNullOrWhiteSpace(viewModel.NewPassword1))
            {
                layoutViewModel.AddMessage(new Message(MessageCode.NewPasswordRequired));
            }
            else if (viewModel.NewPassword1 != viewModel.NewPassword2)
            {
                layoutViewModel.AddMessage(new Message(MessageCode.NewPasswordsNotEqual));
            }
            else
            {
                // call service
                var request = new ChangePasswordRequest()
                {
                    Token = viewModel.Token,
                    NewPassword = viewModel.NewPassword1,
                };
                var task = await RestClient.Post<ChangePasswordResponse>(request);

                // handle response
                var response = task.Object;
                if (!task.Success || response == null)
                {
                    layoutViewModel.AddMessage(new Message(MessageCode.UnableToChangePassword));
                }
                else if (response.Messages.Any(m => m.Code == MessageCode.PasswordResetTokenExpired))
                {
                    Session.WfmAuth().Username = null; // do not set username to prevent token sniffing
                    TempData.WfmAuth().Messages.Add(new Message(MessageCode.PasswordResetTokenExpired));
                    return RedirectToRoute("WfmAuth.ResetPassword");
                }
                else if (response.Messages.Any(m => m.Code == MessageCode.ProcessingPasswordChange))
                {
                    TempData.WfmAuth().Messages = response.Messages;
                    return RedirectToRoute("WfmAuth.LogIn");
                }
                else
                {
                    layoutViewModel.AddMessages(response.Messages);
                }
            }

            return View("~/Views/ChangePassword.cshtml", viewModel);
        }

        [NonAction]
        private ActionResult Cancel(ChangePasswordViewModel viewModel)
        {
            return RedirectToRoute("WfmAuth.LogIn");
        }
    }
}
