﻿using Consortium.Service.Engine;
using System;
using System.Linq;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.ServiceModels;
using WfmPlus.Plugin.Auth.Services;
using WfmPlus.Plugin.Auth.ViewModels;
using WfmPlus.Plugin.Auth.Helpers;
using Consortium.Utility.RestClient;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.Controllers
{
    public class ResetPasswordController : Controller
    {
        public IRestClient RestClient { get; set; }

        public ResetPasswordController(IRestClient restClient)
        {
            RestClient = restClient;
        }

        public ActionResult Index(bool invalidToken = false)
        {
            var viewModel = new ResetPasswordViewModel();
            viewModel.Username = Session.WfmAuth().Username;
            return View("~/Views/ResetPassword.cshtml", viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(ResetPasswordViewModel viewModel)
        {
            Session.WfmAuth().Username = viewModel.Username;
            switch (viewModel.ActionName)
            {
                case "ResetPassword":
                {
                    return await ResetPassword(viewModel);
                }
                case "Cancel":
                {
                    return Cancel(viewModel);
                }
                default:
                {
                    throw new NotSupportedException();
                }
            }
        }

        [NonAction]
        private async Task<ActionResult> ResetPassword(ResetPasswordViewModel viewModel)
        {
            var layoutViewModel = LayoutViewModel.Current(ViewData, (object)ViewBag);

            // call service
            var request = new SendResetPasswordEmailRequest() { Username = viewModel.Username };
            var task = await RestClient.Post<SendResetPasswordEmailResponse>(request);

            // handle response
            var response = task.Object;
            if (!task.Success || response == null)
            {
                layoutViewModel.AddMessage(new Message(MessageCode.PasswordResetEmailFailed));
                return View("~/Views/ResetPassword.cshtml", viewModel);
            }
            else if (response.Messages.Any(m => m.Code == MessageCode.PasswordResetEmailSent))
            {
                TempData.WfmAuth().Messages = response.Messages;
                return RedirectToRoute("WfmAuth.LogIn");
            }
            else
            {
                layoutViewModel.AddMessages(response.Messages);
                return View("~/Views/ResetPassword.cshtml", viewModel);
            }
        }

        [NonAction]
        private ActionResult Cancel(ResetPasswordViewModel viewModel)
        {
            return RedirectToRoute("WfmAuth.LogIn");
        }
    }
}
