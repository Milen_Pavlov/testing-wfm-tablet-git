﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WfmPlus.Plugin.Auth.Security
{
    public static class Cryptographer
    {
        private static byte[] _password;
        private static byte[] _salt;
        private static int _saltSize;
        private static int _iterations;

        static Cryptographer()
        {
            // use fixed cipher for now
            _password = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(@"ek==fbmeHt++btbTBBB=gggyi!"));
            _salt = new byte[] { 1, 9, 3, 4, 9, 6, 7, 8 };
            _saltSize = 4;
            _iterations = 1000;
        }

        public static string Encrypt(string decryptedValue)
        {
            // validate
            if (decryptedValue == null)
            {
                return null;
            }

            // convert to bytes
            var decryptedBytes = Encoding.UTF8.GetBytes(decryptedValue);

            // create salt bytes
            var saltBytes = new byte[_saltSize];
            RNGCryptoServiceProvider.Create().GetBytes(saltBytes);

            // concatenate salt bytes and decrypted bytes
            var decryptedBytesIncSalt = new byte[saltBytes.Length + decryptedBytes.Length];
            Buffer.BlockCopy(saltBytes, 0, decryptedBytesIncSalt, 0, saltBytes.Length);
            Buffer.BlockCopy(decryptedBytes, 0, decryptedBytesIncSalt, saltBytes.Length, decryptedBytes.Length);

            // encrypt
            byte[] encryptedBytes = null;
            using (var aes = new RijndaelManaged())
            {
                var key = new Rfc2898DeriveBytes(_password, _salt, _iterations);
                aes.KeySize = 256;
                aes.BlockSize = 128;
                aes.Key = key.GetBytes(aes.KeySize / 8);
                aes.IV = key.GetBytes(aes.BlockSize / 8);
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.Zeros;

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(decryptedBytesIncSalt, 0, decryptedBytesIncSalt.Length);
                    }
                    encryptedBytes = memoryStream.ToArray();
                }
            }

            // URL encode
            var encryptedValue = HttpServerUtility.UrlTokenEncode(encryptedBytes);
            
            // done
            return encryptedValue;
        }

        public static string Decrypt(string encryptedValue)
        {
            try
            {
                // URL decode
                var encryptedBytes = HttpServerUtility.UrlTokenDecode(encryptedValue);

                // decrypt
                byte[] decryptedBytesIncSalt = null;
                using (var aes = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(_password, _salt, _iterations);
                    aes.KeySize = 256;
                    aes.BlockSize = 128;
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);
                    aes.Mode = CipherMode.CBC;
                    aes.Padding = PaddingMode.Zeros;

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(encryptedBytes, 0, encryptedBytes.Length);
                        }
                        decryptedBytesIncSalt = memoryStream.ToArray();
                    }
                }

                // remove salt bytes
                var decryptedBytes = new byte[decryptedBytesIncSalt.Length - _saltSize];
                Buffer.BlockCopy(decryptedBytesIncSalt, _saltSize, decryptedBytes, 0, decryptedBytes.Length);

                // convert to string
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);

                // done
                return decryptedValue;
            }
            catch
            {
                return null;
            }
        }
    }
}
