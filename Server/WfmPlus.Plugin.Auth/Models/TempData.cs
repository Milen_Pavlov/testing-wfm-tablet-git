﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.Models
{
    public class TempData
    {
        public IList<Message> Messages { get; set; }

        public TempData()
        {
            Messages = new List<Message>();
        }
    }
}
