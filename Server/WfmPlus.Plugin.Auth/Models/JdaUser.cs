﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.Models
{
    public class JdaUser
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginName { get; set; }
        public string BadgeNumber { get; set; }
        public string Email { get; set; }
        public string BusinessUnitName { get; set; }
    }
}
