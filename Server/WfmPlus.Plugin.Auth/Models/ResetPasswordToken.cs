﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfmPlus.Plugin.Auth.Security;

namespace WfmPlus.Plugin.Auth.Models
{
    public class ResetPasswordToken
    {
        public string Username { get; set; }
        public DateTime ExpiryTime { get; set; }
        public ResetPasswordTokenParseResult ParseResult { get; set; }

        public string CreateToken()
        {
            var tokenData = new
            {
                Username = Username,
                ExpiryTime = ExpiryTime.ToUniversalTime().ToString("u")
            };

            var serialisedToken = JsonConvert.SerializeObject(tokenData);
            var token = Cryptographer.Encrypt(serialisedToken);
            return token;
        }

        public static ResetPasswordToken FromToken(string token)
        {
            var tokenObject = new ResetPasswordToken();
            bool error;

            // decrypt token
            try
            {
                var decryptedToken = Cryptographer.Decrypt(token);
                var tokenData = JsonConvert.DeserializeAnonymousType(decryptedToken, new { Username = string.Empty, ExpiryTime = string.Empty });

                tokenObject.Username = tokenData.Username;
                tokenObject.ExpiryTime = DateTime.Parse(tokenData.ExpiryTime).ToLocalTime();
                error = false;
            }
            catch
            {
                error = true;
            }

            // set parse result
            if (string.IsNullOrEmpty(token))
            {
                tokenObject.ParseResult = ResetPasswordTokenParseResult.NoToken;
            }
            else if (error)
            {
                tokenObject.ParseResult = ResetPasswordTokenParseResult.BadToken;
            }
            else if (tokenObject.ExpiryTime < DateTime.Now)
            {
                tokenObject.ParseResult = ResetPasswordTokenParseResult.ExpiredToken;
            }
            else
            {
                tokenObject.ParseResult = ResetPasswordTokenParseResult.ValidToken;
            }

            return tokenObject;
        }
    }

    public enum ResetPasswordTokenParseResult
    {
        Null,
        NoToken,
        BadToken,
        ExpiredToken,
        ValidToken
    }
}
