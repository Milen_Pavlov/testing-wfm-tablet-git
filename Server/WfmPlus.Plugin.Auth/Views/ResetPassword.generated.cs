﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WfmPlus.Plugin.Auth.Views
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/ResetPassword.cshtml")]
    public partial class ResetPassword : System.Web.Mvc.WebViewPage<WfmPlus.Plugin.Auth.ViewModels.ResetPasswordViewModel>
    {
        public ResetPassword()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\ResetPassword.cshtml"
  
    ViewBag.Title = "Reset Password";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<div");

WriteLiteral(" class=\"jarviswidget jarviswidget-sortable\"");

WriteLiteral(" data-widget-editbutton=\"false\"");

WriteLiteral(" data-widget-custombutton=\"false\"");

WriteLiteral(" role=\"widget\"");

WriteLiteral(">\r\n    <header");

WriteLiteral(" role=\"heading\"");

WriteLiteral(">\r\n        <span");

WriteLiteral(" class=\"widget-icon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-edit\"");

WriteLiteral("></i></span>\r\n        <h2>Reset Password</h2>\r\n    </header>\r\n    <div");

WriteLiteral(" role=\"content\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"widget-body no-padding\"");

WriteLiteral(">\r\n            <form");

WriteLiteral(" class=\"smart-form\"");

WriteLiteral(" novalidate=\"novalidate\"");

WriteLiteral(">\r\n");

WriteLiteral("                ");

            
            #line 14 "..\..\Views\ResetPassword.cshtml"
            Write(Html.HiddenFor(m => m.ActionName));

            
            #line default
            #line hidden
WriteLiteral("\r\n                <fieldset>\r\n                    <section>\r\n                    " +
"    <label");

WriteLiteral(" class=\"input\"");

WriteLiteral(">\r\n                            <i");

WriteLiteral(" class=\"icon-append fa fa-user\"");

WriteLiteral("></i>\r\n");

WriteLiteral("                            ");

            
            #line 19 "..\..\Views\ResetPassword.cshtml"
                        Write(Html.TextBoxFor(m => m.Username, new { placeholder = "Username" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </label>\r\n                    </section>\r\n             " +
"   </fieldset>\r\n                <footer>\r\n                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(" onclick=\"WfmAuth.submit(this, \'ResetPassword\');\"");

WriteLiteral(">\r\n                        Reset Password\r\n                    </button>\r\n       " +
"             <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" onclick=\"WfmAuth.submit(this, \'Cancel\');\"");

WriteLiteral(">\r\n                        Cancel\r\n                    </button>\r\n               " +
" </footer>\r\n            </form>\r\n        </div>\r\n    </div>\r\n</div>\r\n");

        }
    }
}
#pragma warning restore 1591
