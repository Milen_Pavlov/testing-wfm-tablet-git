﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WfmPlus.Plugin.Auth.Views
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 1 "..\..\Views\_Layout.cshtml"
    using WfmPlus.Plugin.Auth.ViewModels;
    
    #line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/_Layout.cshtml")]
    public partial class Layout : System.Web.Mvc.WebViewPage<dynamic>
    {
        public Layout()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\_Layout.cshtml"
  
    ViewBag.IncludeNav = false;
    Layout = "~/Views/Shared/_BaseLayout.cshtml";
    LayoutViewModel model = LayoutViewModel.Current(ViewData, (object)ViewBag);

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n");

DefineSection("head", () => {

WriteLiteral("\r\n    <meta");

WriteLiteral(" http-equiv=\"X-UA-Compatible\"");

WriteLiteral(" content=\"IE=Edge\"");

WriteLiteral(" />\r\n    <meta");

WriteLiteral(" name=\"viewport\"");

WriteLiteral(" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\"");

WriteLiteral(" />\r\n    <link");

WriteLiteral(" rel=\"stylesheet\"");

WriteLiteral(" type=\"text/css\"");

WriteAttribute("href", Tuple.Create(" href=\"", 445), Tuple.Create("\"", 492)
            
            #line 12 "..\..\Views\_Layout.cshtml"
, Tuple.Create(Tuple.Create("", 452), Tuple.Create<System.Object, System.Int32>(Url.Content("~/Content/Css/main.css")
            
            #line default
            #line hidden
, 452), false)
);

WriteLiteral(" />\r\n");

});

WriteLiteral("\r\n");

DefineSection("scripts", () => {

WriteLiteral("\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 535), Tuple.Create("\"", 584)
            
            #line 17 "..\..\Views\_Layout.cshtml"
, Tuple.Create(Tuple.Create("", 541), Tuple.Create<System.Object, System.Int32>(Url.Content("~/Content/Scripts/main.js")
            
            #line default
            #line hidden
, 541), false)
);

WriteLiteral("></script>\r\n    <script>\r\n        WfmAuth.jdaRootPath = \"");

            
            #line 19 "..\..\Views\_Layout.cshtml"
                           Write(model.AppSettings.JdaRootPath);

            
            #line default
            #line hidden
WriteLiteral("\";\r\n    </script>\r\n");

WriteLiteral("    ");

            
            #line 21 "..\..\Views\_Layout.cshtml"
Write(RenderSection("scripts", false));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

});

WriteLiteral("\r\n<div");

WriteLiteral(" class=\"centered-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" id=\"flash-container\"");

WriteLiteral(">\r\n");

            
            #line 26 "..\..\Views\_Layout.cshtml"
        
            
            #line default
            #line hidden
            
            #line 26 "..\..\Views\_Layout.cshtml"
         if (model.FlashMessages != null)
        {
            foreach (var message in model.FlashMessages)
            {
                
            
            #line default
            #line hidden
            
            #line 30 "..\..\Views\_Layout.cshtml"
            Write(Html.Partial("_Flash", message));

            
            #line default
            #line hidden
            
            #line 30 "..\..\Views\_Layout.cshtml"
                                                  
            }
        }

            
            #line default
            #line hidden
WriteLiteral("    </div>\r\n");

WriteLiteral("    ");

            
            #line 34 "..\..\Views\_Layout.cshtml"
Write(RenderBody());

            
            #line default
            #line hidden
WriteLiteral("\r\n</div>\r\n\r\n");

        }
    }
}
#pragma warning restore 1591
