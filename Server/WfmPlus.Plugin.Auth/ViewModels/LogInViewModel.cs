﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfmPlus.Plugin.Auth.ServiceModels;

namespace WfmPlus.Plugin.Auth.ViewModels
{
    public class LogInViewModel
    {
        public string ActionName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public JdaXhr Xhr { get; set; }
    }
}
