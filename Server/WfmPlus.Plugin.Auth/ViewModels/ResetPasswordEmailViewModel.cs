﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ViewModels
{
    public class ResetPasswordEmailViewModel
    {
        public string ToName { get; set; }
        public string FromName { get; set; }
        public string ChangePasswordUrl { get; set; }
    }
}
