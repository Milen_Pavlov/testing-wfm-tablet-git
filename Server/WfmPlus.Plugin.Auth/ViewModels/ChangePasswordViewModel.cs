﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfmPlus.Plugin.Auth.ServiceModels;

namespace WfmPlus.Plugin.Auth.ViewModels
{
    public class ChangePasswordViewModel
    {
        public string ActionName { get; set; }
        public JdaXhr Xhr { get; set; }
        public string Username { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword1 { get; set; }
        public string NewPassword2 { get; set; }
        public string Token { get; set; }

        public ChangePasswordReason ChangePasswordReason
        {
            get
            {
                return (string.IsNullOrEmpty(Token) ? ChangePasswordReason.Expired : ChangePasswordReason.Forgotten);
            }
        }
        
        public string UsernamePasswordCssDisplay
        {
            get
            {
                return (string.IsNullOrEmpty(Token) ? string.Empty : "display: none;");
            }
        }
    }

    public enum ChangePasswordReason
    {
        Expired,
        Forgotten,
    }
}
