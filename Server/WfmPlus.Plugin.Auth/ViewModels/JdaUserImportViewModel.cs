﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ViewModels
{
    public class JdaUserImportViewModel
    {
        public DateTime CreationTimestamp { get; set; }
        public string CreationSource { get; set; }
        public string BusinessUnitId { get; set; }
        public string UserBadgeNumber { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserLoginName { get; set; }
        public string UserPassword { get; set; }

        public JdaUserImportViewModel()
        {
            CreationTimestamp = DateTime.Now;
        }

        public string CreationTimestampAsString
        {
            get
            {
                return CreationTimestamp.ToString(@"yyyy\-MM\-dd\THH\:mm\:ss");
            }
        }
    }
}
