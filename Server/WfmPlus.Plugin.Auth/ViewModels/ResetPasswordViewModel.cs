﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.ViewModels
{
    public class ResetPasswordViewModel
    {
        public string ActionName { get; set; }
        public string Username { get; set; }
    }
}
