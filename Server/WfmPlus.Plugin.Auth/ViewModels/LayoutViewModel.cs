﻿using Consortium.Service.Engine;
using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.Helpers;

namespace WfmPlus.Plugin.Auth.ViewModels
{
    public class LayoutViewModel
    {
        private ViewDataDictionary _viewData;
        private dynamic _viewBag;

        public IAppSettingsRepository AppSettings { get; set; }

        private LayoutViewModel()
        {
            AppSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();
        }

        public static LayoutViewModel Current(ViewDataDictionary viewData, dynamic viewBag)
        {
            if (!viewData.ContainsKey("WfmAuthLayout"))
            {
                var layout = new LayoutViewModel();
                layout._viewData = viewData;
                layout._viewBag = viewBag;
                viewData.Add("WfmAuthLayout", layout);
            }
            return (LayoutViewModel)viewData["WfmAuthLayout"];
        }

        public IList<FlashMessageModel> FlashMessages
        {
            get
            {
                if (_viewBag.Flash == null)
                {
                    _viewBag.Flash = new List<FlashMessageModel>();
                }
                return (IList<FlashMessageModel>)_viewBag.Flash;
            }
        }

        public void AddMessage(Message message)
        {
            FlashMessages.Add(message.ToFlashMessage());
        }

        public void AddMessages(IList<Message> messages)
        {
            foreach (var message in messages)
            {
                AddMessage(message);
            }
        }
    }
}
