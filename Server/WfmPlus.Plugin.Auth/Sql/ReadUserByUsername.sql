﻿
SELECT [u].[user_id], [u].[first_name], [u].[last_name], [e].[badge_number], [a].[e_mail], [da].[name]
FROM [Rad_Sys_User] AS [u]
LEFT JOIN [Employee] as [e] on [e].[employee_id] = [u].[user_id]
LEFT JOIN [Address] AS [a] ON [a].[address_id] = [u].[address_id]
LEFT JOIN [Rad_Sys_Data_Accessor] as [da] on [da].[data_accessor_id] = [e].[home_business_unit_id]
WHERE [u].[login_name] = @Username
