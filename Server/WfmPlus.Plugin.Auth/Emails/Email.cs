﻿using Consortium.Service.Engine;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WfmPlus.Plugin.Auth.Emails
{
    public class Email
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string TemplateName { get; set; }
        public object Model { get; set; }

        public IEmailSender EmailSender { get; set; }

        public Email(IEmailSender emailSender)
        {
            EmailSender = emailSender;
        }

        public void Send()
        {
            // get template
            var assembly = Assembly.GetExecutingAssembly();
            var templateNamespace = string.Format("WfmPlus.Plugin.Auth.Views.{0}.cshtml", TemplateName);
            
            string templateData;
            using (Stream stream = assembly.GetManifestResourceStream(templateNamespace))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    templateData = reader.ReadToEnd();
                }
            }

            // compile template
            var template = new LoadedTemplateSource(templateData);
            var modelType = (Model == null ? null : Model.GetType());
            var body = Engine.Razor.RunCompile(template, TemplateName, modelType, Model);

            // send email
            EmailSender.SendAsync(To, Subject, body);
        }
    }
}
