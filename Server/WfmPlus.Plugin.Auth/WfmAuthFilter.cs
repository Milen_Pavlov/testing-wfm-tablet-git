﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.Models;
using WfmPlus.Plugin.Auth.ViewModels;
using WfmPlus.Plugin.Auth.Helpers;

namespace WfmPlus.Plugin.Auth
{
    public class WfmAuthFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var layoutViewModel = LayoutViewModel.Current(filterContext.Controller.ViewData, (object)filterContext.Controller.ViewBag);
            layoutViewModel.AddMessages(filterContext.Controller.TempData.WfmAuth().Messages);
            base.OnActionExecuting(filterContext);
        }
    }
}
