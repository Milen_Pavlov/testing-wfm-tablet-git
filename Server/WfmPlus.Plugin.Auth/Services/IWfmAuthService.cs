﻿using Consortium.Service.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfmPlus.Plugin.Auth.ServiceModels;

namespace WfmPlus.Plugin.Auth.Services
{
    public interface IWfmAuthService
    {
        SendResetPasswordEmailResponse Post(SendResetPasswordEmailRequest request);
        ChangePasswordResponse Post(ChangePasswordRequest request);
    }
}
