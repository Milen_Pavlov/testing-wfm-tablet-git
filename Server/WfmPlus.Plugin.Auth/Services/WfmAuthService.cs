﻿using Consortium.Service.Engine;
using RazorEngine;
using RazorEngine.Templating;
using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Timers;
using System.Xml.Linq;
using WfmPlus.Plugin.Auth.Emails;
using WfmPlus.Plugin.Auth.Models;
using WfmPlus.Plugin.Auth.Repositories;
using WfmPlus.Plugin.Auth.ServiceModels;
using WfmPlus.Plugin.Auth.ViewModels;

namespace WfmPlus.Plugin.Auth.Services
{
    public class WfmAuthService : ConsortiumService, IWfmAuthService
    {
        public IAppSettingsRepository AppSettings { get; set; }
        public IJdaRepository JdaRepository { get; set; }
        public IEmailSender EmailSender { get; set; }

        public WfmAuthService()
        {
            AppSettings = IOC.Resolve<IAppSettingsRepository>();
            JdaRepository = IOC.Resolve<IJdaRepository>();
            EmailSender = IOC.Resolve<IEmailSender>();
        }

        public SendResetPasswordEmailResponse Post(SendResetPasswordEmailRequest request)
        {
            var response = new SendResetPasswordEmailResponse();

            // validate request
            if (string.IsNullOrWhiteSpace(request.Username))
            {
                response.Messages.Add(new Message(MessageCode.UsernameRequired));
                return response;
            }

            // return success message to prevent username sniffing
            response.Messages.Add(new Message(MessageCode.PasswordResetEmailSent));

            // ensure username is allowed to reset password
            var normalisedUsername = request.Username.Trim().ToLower();
            if (AppSettings.NoChangePasswordUsernames.ToLower().Split(',').Contains(normalisedUsername))
            {
                return response;
            }

            // get JDA user
            var user = JdaRepository.ReadUserByUsername(request.Username);
            if (user == null || string.IsNullOrWhiteSpace(user.Email))
            {
                return response;
            }

            // create security token
            var tokenObject = new ResetPasswordToken()
            {
                Username = request.Username,
                ExpiryTime = DateTime.Now.AddMinutes(AppSettings.MinutesUntilChangePasswordLinkExpires)
            };
            var token = tokenObject.CreateToken();

            // send email
            var email = new Email(EmailSender);
            email.To = user.Email;
            email.Subject = "Employee Self Service - Reset Password";
            email.TemplateName = "ResetPasswordEmail";
            email.Model = new ResetPasswordEmailViewModel()
            {
                ToName = user.FirstName,
                FromName = AppSettings.ChangePasswordEmailFrom,
                ChangePasswordUrl = AppSettings.ChangePasswordPath.Replace("{token}", token)
            };

            try { email.Send(); }
            catch { }

            // done
            return response;
        }

        public ChangePasswordResponse Post(ChangePasswordRequest request)
        {
            var response = new ChangePasswordResponse();

            // validate request
            var tokenObject = ResetPasswordToken.FromToken(request.Token);
            if (tokenObject.ParseResult != ResetPasswordTokenParseResult.ValidToken)
            {
                response.Messages.Add(new Message(MessageCode.PasswordResetTokenExpired));
                return response;
            }
            else if (string.IsNullOrWhiteSpace(request.NewPassword))
            {
                response.Messages.Add(new Message(MessageCode.NewPasswordRequired));
                return response;
            }

            foreach (var newPasswordFormat in AppSettings.NewPasswordFormats)
            {
                if (!Regex.IsMatch(request.NewPassword, newPasswordFormat))
                {
                    response.Messages.Add(new Message(MessageCode.InvalidPasswordFormat));
                    return response;
                }
            }

            // get JDA user
            var user = JdaRepository.ReadUserByUsername(tokenObject.Username);
            if (user == null)
            {
                response.Messages.Add(new Message(MessageCode.UnableToChangePassword));
                return response;
            }
            
            // check password doesn't contain username, first name or last name
            if (AppSettings.PasswordCannotContainUsernameOrName)
            {
                var lowerNewPassword = request.NewPassword.ToLower();
                var passwordContainsUsername = (!string.IsNullOrEmpty(user.LoginName) && lowerNewPassword.Contains(user.LoginName.ToLower()));
                var passwordContainsFirstName = (!string.IsNullOrEmpty(user.FirstName) && lowerNewPassword.Contains(user.FirstName.ToLower()));
                var passwordContainsLastName = (!string.IsNullOrEmpty(user.LastName) && lowerNewPassword.Contains(user.LastName.ToLower()));

                if (passwordContainsUsername || passwordContainsFirstName || passwordContainsLastName)
                {
                    response.Messages.Add(new Message(MessageCode.PasswordCannotContainUsernameOrName, user.FirstName, user.LastName));
                    return response;
                }
            }

            // get XML template
            var assembly = Assembly.GetExecutingAssembly();
            var xmlNamespace = "WfmPlus.Plugin.Auth.Views.JdaUserImport.cshtml";

            string xmlTemplate;
            using (var stream = assembly.GetManifestResourceStream(xmlNamespace))
            {
                using (var reader = new StreamReader(stream))
                {
                    xmlTemplate = reader.ReadToEnd();
                }
            }

            // create import
            var importModel = new JdaUserImportViewModel()
            {
                CreationSource = AppSettings.JdaUserImportCreationSource,
                BusinessUnitId = user.BusinessUnitName,
                UserBadgeNumber = user.BadgeNumber,
                UserFirstName = user.FirstName,
                UserLastName = user.LastName,
                UserLoginName = user.LoginName,
                UserPassword = request.NewPassword,
            };

            // compile template
            var template = new LoadedTemplateSource(xmlTemplate);
            var xmlString = Engine.Razor.RunCompile(template, "JdaUserImport", typeof(JdaUserImportViewModel), importModel);

            // create document
            var xmlPath = AppSettings.JdaEmployeeImportPath.Replace("{fileId}", Guid.NewGuid().ToString("N"));
            File.WriteAllText(xmlPath, xmlString);

            // trigger import
            File.WriteAllText(xmlPath + ".flag", string.Empty);

            var timer = new Timer();
            timer.Interval = 1000;
            timer.AutoReset = false;
            timer.Elapsed += (sender, args) =>
            {
                File.Move(xmlPath + ".flag", xmlPath + ".done");
                timer.Stop();
            };
            timer.Start();

            // done
            response.Messages.Add(new Message(MessageCode.ProcessingPasswordChange));
            return response;
        }
    }
}
