﻿using Consortium.Service.Engine;
using System.Collections.Generic;
using System.Web.Routing;
using System.Linq;
using System.Web.Mvc;

namespace WfmPlus.Plugin.Auth
{
    public class WfmAuthPlugin : IConsortiumPlugin
    {
        private Route _pluginRoute;

        public string GetName()
        { 
            return "wfmauth";
        }

        public NavigationMenu GetMenu()
        {
            return null;
        }

        public void Register(ConsortiumContainer container, ConsortiumConfig config)
        {
            // remove default route for plugin
            _pluginRoute = (System.Web.Routing.Route)RouteTable.Routes.Single(r => r.GetType() == typeof(System.Web.Routing.Route) && ((System.Web.Routing.Route)r).Url.Contains("wfmauth"));
            RouteTable.Routes.Remove(_pluginRoute);

            // add custom routes
            AddRoute("WfmAuth.Default", "wfmauth", new { controller = "LogIn", action = "Index" });
            AddRoute("WfmAuth.LogIn", "wfmauth/login", new { controller = "LogIn", action = "Index" });
            AddRoute("WfmAuth.ResetPassword", "wfmauth/resetpassword", new { controller = "ResetPassword", action = "Index" });
            AddRoute("WfmAuth.ChangePassword", "wfmauth/changepassword/{token}", new { controller = "ChangePassword", action = "Index", token = UrlParameter.Optional });

            // register filters
            GlobalFilters.Filters.Add(new WfmAuthFilter());
        }

        public void AddRoute(string name, string url, object defaults)
        {
            var route = new Route(url, new RouteValueDictionary(defaults), _pluginRoute.RouteHandler);
            route.Constraints = _pluginRoute.Constraints;
            route.DataTokens = _pluginRoute.DataTokens;
            RouteTable.Routes.Add(name, route);
        }
    }
}
