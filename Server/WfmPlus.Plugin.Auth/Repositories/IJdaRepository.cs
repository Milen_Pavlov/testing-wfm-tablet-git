﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfmPlus.Plugin.Auth.Models;

namespace WfmPlus.Plugin.Auth.Repositories
{
    public interface IJdaRepository
    {
        JdaUser ReadUserByUsername(string username);
    }
}
