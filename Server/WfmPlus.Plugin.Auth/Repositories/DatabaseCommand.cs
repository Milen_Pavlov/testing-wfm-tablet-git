﻿using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth.Repositories
{
    public class DatabaseCommand
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
        public IList<SqlParameter> Parameters { get; set; }

        public int Execute()
        {
            return CreateSqlCommand((connection, command) =>
            {
                return command.ExecuteNonQuery();
            });
        }

        public TModel ReadOne<TModel>(Func<SqlDataReader, TModel> map)
        {
            return CreateSqlCommand((connection, command) =>
            {
                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    var model = map(reader);
                    return model;
                }
            });
        }

        public IList<TModel> ReadMany<TModel>(Func<SqlDataReader, TModel> map)
        {
            return CreateSqlCommand((connection, command) =>
            {
                var models = new List<TModel>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var model = map(reader);
                        models.Add(model);
                    }
                }
                return models;
            });
        }

        private TResult CreateSqlCommand<TResult>(Func<SqlConnection, SqlCommand, TResult> ready)
        {
            // get command text
            var assembly = Assembly.GetExecutingAssembly();
            var commandNamespace = string.Format("WfmPlus.Plugin.Auth.Sql.{0}.sql", Name);
            
            string commandText;
            using (var stream = assembly.GetManifestResourceStream(commandNamespace))
            {
                using (var reader = new StreamReader(stream))
                {
                    commandText = reader.ReadToEnd();
                }
            }

            // execute command
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(commandText, connection))
                {
                    if (Parameters != null)
                    {
                        command.Parameters.AddRange(Parameters.ToArray());
                    }

                    connection.Open();
                    return ready(connection, command);
                }
            }
        }
    }
}
