﻿using Consortium.Service.Engine;
using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfmPlus.Plugin.Auth.Models;

namespace WfmPlus.Plugin.Auth.Repositories
{
    public class JdaRepository : IJdaRepository
    {
        public IAppSettingsRepository AppSettings { get; set; }

        public JdaRepository()
        {
            AppSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();
        }

        public JdaUser ReadUserByUsername(string username)
        {
            var command = new DatabaseCommand();
            command.ConnectionString = AppSettings.JdaConnectionString;
            command.Name = "ReadUserByUsername";
            command.Parameters = new List<SqlParameter> { new SqlParameter("Username", username) };

            var user = command.ReadOne(reader =>
            {
                if (!reader.HasRows)
                {
                    return null;
                }

                return new JdaUser()
                {
                    UserId = reader.GetInt32(0),
                    FirstName = (reader.IsDBNull(1) ? null : reader.GetString(1)),
                    LastName = (reader.IsDBNull(2) ? null : reader.GetString(2)),
                    BadgeNumber = (reader.IsDBNull(3) ? null : reader.GetString(3)),
                    Email = (reader.IsDBNull(4) ? null : reader.GetString(4)),
                    BusinessUnitName = (reader.IsDBNull(5) ? null : reader.GetString(5)),
                    LoginName = username,
                };
            });

            return user;
        }
    }
}
