﻿using Consortium.Service.Engine;
using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfmPlus.Plugin.Auth
{
    public class Message
    {
        public MessageCode Code { get; set; }
        public FlashMessageType Severity { get; set; }
        public string Value { get; set; }

        public Message()
        {
        }

        private Message(MessageCode code, FlashMessageType severity, string value)
        {
            Code = code;
            Severity = severity;
            Value = value;
        }

        public Message(MessageCode code, params object[] messageArgs)
        {
            var messageTemplate = MessageTemplates[code]; // assumption that key exists
            Code = messageTemplate.Code;
            Severity = messageTemplate.Severity;
            Value = string.Format(messageTemplate.Value, messageArgs);
        }

        private static IDictionary<MessageCode, Message> _messageTemplates;
        public static IDictionary<MessageCode, Message> MessageTemplates
        {
            get
            {
                if (_messageTemplates == null)
                {
                    var appSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();

                    var values = new List<Message>()
                    {
                        new Message(MessageCode.PasswordResetTokenExpired, FlashMessageType.Error, "Your request to reset your password has expired. Please try again, ensuring that you promptly respond to the email that we will send you."),
                        new Message(MessageCode.UsernameRequired, FlashMessageType.Error, "Please enter your username."),
                        new Message(MessageCode.PasswordRequired, FlashMessageType.Error, "Please enter your password."),
                        new Message(MessageCode.CurrentPasswordRequired, FlashMessageType.Error, "Please enter your current password."),
                        new Message(MessageCode.NewPasswordRequired, FlashMessageType.Error, "Please enter a new password."),
                        new Message(MessageCode.NewPasswordsNotEqual, FlashMessageType.Error, "Please ensure that your new password and the retyped password are the same."),
                        new Message(MessageCode.InvalidPasswordFormat, FlashMessageType.Error, appSettings.NewPasswordFormatError),
                        new Message(MessageCode.UnableToChangePassword, FlashMessageType.Error, "We were unable to change your password. Please contact your manager."),
                        new Message(MessageCode.ProcessingPasswordChange, FlashMessageType.Success, "We are processing your password change. Please come back in 10 minutes and log in with your new password."),
                        new Message(MessageCode.InvalidCredentials, FlashMessageType.Error, "The credentials you entered are incorrect. Please try again."),
                        new Message(MessageCode.LogInFailed, FlashMessageType.Error, "We were unable to log you in. Please contact your manager."),
                        new Message(MessageCode.PasswordResetEmailSent, FlashMessageType.Success, appSettings.PasswordResetEmailSent),
                        new Message(MessageCode.PasswordResetEmailFailed, FlashMessageType.Error, "Something went wrong whilst trying to reset your password. Please contact your manager."),
                        new Message(MessageCode.TooManyLogInAttempts, FlashMessageType.Error, "You have exceeded the maximum number of log in attempts. Please contact your manager."),
                        new Message(MessageCode.SessionExpiredLogInAgain, FlashMessageType.Error, "Your session expired. Please try logging in again."),
                        new Message(MessageCode.CurrentPasswordIncorrect, FlashMessageType.Error, "The current password you entered is incorrect."),
                        new Message(MessageCode.PasswordCannotBeSameAsPreviousPassword, FlashMessageType.Error, "Your new password cannot be the same as your current password."),
                        new Message(MessageCode.PasswordCannotContainUsernameOrName, FlashMessageType.Error, "Your new password must not contain your username, first name ({0}) or last name ({1})."), // todo: what if no first name / last name (will validation always fail)
                        new Message(MessageCode.PasswordHasExpired, FlashMessageType.Error, "Your password has expired. Please change your password."),
                    };

                    _messageTemplates = values.ToDictionary(m => m.Code, m => m);
                }

                return _messageTemplates;
            }
        }

        public FlashMessageModel ToFlashMessage()
        {
            return new FlashMessageModel()
            {
                Type = Severity,
                Message = Value,
            };
        }
    }

    public enum MessageCode
    {
        PasswordResetTokenExpired,
        UsernameRequired,
        PasswordRequired,
        CurrentPasswordRequired,
        NewPasswordRequired,
        NewPasswordsNotEqual,
        InvalidPasswordFormat,
        UnableToChangePassword,
        ProcessingPasswordChange,
        InvalidCredentials,
        LogInFailed,
        PasswordResetEmailSent,
        PasswordResetEmailFailed,
        TooManyLogInAttempts,
        SessionExpiredLogInAgain,
        CurrentPasswordIncorrect,
        PasswordCannotBeSameAsPreviousPassword,
        PasswordCannotContainUsernameOrName,
        PasswordHasExpired,
    }
}
