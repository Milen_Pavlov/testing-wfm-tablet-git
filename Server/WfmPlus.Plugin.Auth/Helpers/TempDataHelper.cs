﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WfmPlus.Plugin.Auth.Models;

namespace WfmPlus.Plugin.Auth.Helpers
{
    public static class TempDataHelper
    {
        private const string _tempDataKey = "17BDF925-244F-4B23-A3A2-A90042FED711";

        public static TempData WfmAuth(this TempDataDictionary source)
        {
            if (source[_tempDataKey] == null)
            {
                source.Add(_tempDataKey, new TempData());
            }
            return (TempData)source[_tempDataKey];
        }
    }
}
