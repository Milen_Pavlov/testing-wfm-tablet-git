﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WfmPlus.Plugin.Auth.Models;

namespace WfmPlus.Plugin.Auth.Helpers
{
    public static class SessionHelper
    {
        private const string _sessionKey = "7EB40A2E-46FE-429B-92A5-AB094614E1A7";

        public static Session WfmAuth(this HttpSessionStateBase source)
        {
            if (source[_sessionKey] == null)
            {
                source.Add(_sessionKey, new Session());
            }
            return (Session)source[_sessionKey];
        }
    }
}
