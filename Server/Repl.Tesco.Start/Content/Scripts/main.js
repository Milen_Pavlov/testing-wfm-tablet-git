﻿
var WfmAuth =
{
    jdaRootPath: null,

    submit: function (source, action, method)
    {
        if (method == null)
        {
            method = "POST";
        }

        var form = $(source).closest("form");
        if (form == null)
        {
            return;
        }

        form.children("#ActionName").val(action);
        form.attr("method", method).submit();
    },

    logOut: function(complete)
    {
        $.ajax(
        {
            url: WfmAuth.jdaRootPath + "rp/logout",
            type: "GET",
            xhrFields: { withCredentials: true },
            complete: function (jqXHR)
            {
                complete(jqXHR);
            },
        });
    },

    logIn: function(username, password, complete)
    {
        $.ajax(
        {
            url: WfmAuth.jdaRootPath + "data/login",
            type: "POST",
            data: "loginName=" + username + "&password=" + password,
            xhrFields: { withCredentials: true },
            complete: function (jqXHR)
            {
                complete(jqXHR);
            },
        });
    },

    changePassword: function(currentPassword, newPassword1, newPassword2, complete)
    {
        $.ajax(
        {
            url: WfmAuth.jdaRootPath + "data/changePassword",
            type: "POST",
            data: "oldPwd=" + currentPassword + "&newPwd=" + newPassword1 + "&confirmPwd=" + newPassword2,
            xhrFields: { withCredentials: true },
            complete: function (jqXHR, textStatus)
            {
                complete(jqXHR);
            },
        });
    },

    convertXhrToJson: function (xhr)
    {
        var xhrAsJson =
        {
            status: xhr.status,
            response: null
        }

        try
        {
            xhrAsJson.response = JSON.parse(xhr.responseText)
        }
        catch (exception)
        {
        }

        return xhrAsJson;
    },

    buttonClicked: function (event)
    {
        var button = $(event.target);
        button.width(button.width()); // set explicit width to prevent button shrinking
        button.attr("disabled", "disabled");
        button.text("");
        button.html("<i class='fa fa-gear fa-spin'></i>");
    },

    keyPressed: function (event)
    {
        if (event.charCode == 13 || event.keyCode == 13)
        {
            $("button.btn.btn-primary").first().click();
        }
    },
}

$("button").click(function (event) { WfmAuth.buttonClicked(event); });
$("form input").keypress(function (event) { WfmAuth.keyPressed(event); });
$(".alert").animate({ opacity: 1 }, 200);
