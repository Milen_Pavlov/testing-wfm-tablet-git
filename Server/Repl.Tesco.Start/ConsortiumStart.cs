﻿using Consortium.Plugin.Azure;
using Consortium.Plugin.MongoDb;
using Consortium.Service.Engine;
using Consortium.Service.Engine.Authentication;
using Consortium.Utility.RestClient;
using Microsoft.AspNet.Identity;
using Repl.Tesco.Data;
using Repl.Tesco.Data.Jda;
using Repl.Tesco.Interfaces;
using Repl.Tesco.Interfaces.Repositories;
using Repl.Tesco.Interfaces.Repositories.Jda;
using Repl.Tesco.Plugin.Services;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WfmPlus.Plugin.Auth;
using WfmPlus.Plugin.Auth.Repositories;
using WfmPlus.Plugin.Auth.Services;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Repl.Tesco.Start.ConsortiumStart), "Start")]  

namespace Repl.Tesco.Start
{
    public class ConsortiumStart
    {
        // Tesco
        public static void Start()
        {
            var appSettings = new AppSettingsRepository(WebConfigurationManager.AppSettings);

            ConsortiumApp.IOC.Register<IAppSettingsRepository>(appSettings);
            ConsortiumApp.IOC.Register<IEmailSender>(new SendGridEmailSender(appSettings.EmailSenderUsername, appSettings.EmailSenderPassword));
            ConsortiumApp.IOC.Register<ITillAllocationRepository>(new TillAllocationRepository());
            ConsortiumApp.IOC.Register<ITillRepository>(new TillRepository());
            ConsortiumApp.IOC.Register<ITillStatusRepository>(new TillStatusRepository());
            ConsortiumApp.IOC.Register<ITillTypeRepository>(new TillTypeRepository());
            ConsortiumApp.IOC.Register<IJdaSessionRepository>(new JdaSessionRepository());
            ConsortiumApp.IOC.Register<IJdaSiteRepository>(new JdaSiteRepository());
            ConsortiumApp.IOC.Register<IJdaScheduleRepository>(new JdaScheduleRepository());
            ConsortiumApp.IOC.Register<IOvertimeRepository>(new OvertimeRepository());
            ConsortiumApp.IOC.Register<IBreakRepository>(new BreakRepository());
            ConsortiumApp.IOC.Register<IJdaRepository>(new JdaRepository());

            var config = new ConsortiumConfig()
            {
                AppName = "WFM Plus",
                Plugins = new IConsortiumPlugin[]
                {
                    new AzurePlugin(),
                    //new MongoDbPlugin(),
                    new TescoServicesPlugin(),
                }
            };

            ConsortiumApp.Start(config);
        }

        // JDW
        //public static void Start()
        //{
        //    var appSettings = new AppSettingsRepository(WebConfigurationManager.AppSettings);
        //    var restClient = new RestClient() { BaseUri = new Uri(appSettings.ApiPath) };

        //    ConsortiumApp.IOC.Register<IRestClient>(restClient);
        //    ConsortiumApp.IOC.Register<IAppSettingsRepository>(appSettings);
        //    ConsortiumApp.IOC.Register<IEmailSender>(new SmtpEmailSender());
        //    //ConsortiumApp.IOC.Register<IEmailSender>(new SendGridEmailSender(appSettings.EmailSenderUsername, appSettings.EmailSenderPassword));
        //    ConsortiumApp.IOC.Register<IJdaRepository>(new JdaRepository());
        //    ConsortiumApp.IOC.Register<IUserStore<ConsortiumAspnetUser, Guid>>(new FakeUserStore());

        //    var config = new ConsortiumConfig()
        //    {
        //        AppName = "WFM Plus",
        //        Plugins = new IConsortiumPlugin[]
        //        {
        //            new WfmAuthPlugin(),
        //        }
        //    };

        //    ConsortiumApp.Start(config);
        //}
    }
}