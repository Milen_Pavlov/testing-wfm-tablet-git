﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsortiumBrowser = Consortium.Service.Engine.Models.Shared.Browser;

namespace Repl.Tesco.Start.Models.Shared
{
    public class Browser : ConsortiumBrowser
    {
        public new static Browser Current
        {
            get
            {
                return ConsortiumBrowser.CurrentOfType<Browser>();
            }
        }

        public Browser()
        {
            if (Ie8)
            {
                LogoPath = "~/Content/Images/replLogo.jpg";
            }
            else
            {
                LogoPath = "~/Content/Images/WFM Logo.png";
            }
        }

        public string LogoPath { get; set; }
    }
}