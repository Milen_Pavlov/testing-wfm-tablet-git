﻿using Repl.Tesco.Models.Jda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Interfaces.Repositories.Jda
{
    public interface IJdaScheduleRepository
    {
        JdaSchedule Read(string sessionId, int siteId, DateTime startDay);
    }
}
