﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Interfaces.Repositories.Jda
{
    public interface IJdaSessionRepository
    {
        bool HasServiceUser { get; }
        string LoginServiceUser();

        string LogIn(string username, string password);
        bool Authenticated(string sessionId);
        bool LogOut();
    }
}
