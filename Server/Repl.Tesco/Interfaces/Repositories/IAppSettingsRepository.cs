﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Interfaces.Repositories
{
    public interface IAppSettingsRepository
    {
        string JDAServerRequestFormat { get; }
        string ScheduleDataEndpoint { get; }
        string LoginEndpoint { get; }
        string KeepAliveEndpoint { get; }
        string LogoutEndpoint { get; }
        string GetOrgUnitsEndpoint { get; }

        string JDAUsername { get; }
        string JDAPassword { get; }
        string JdaConnectionString { get; }
        string EmailSenderUsername { get; }
        string EmailSenderPassword { get; }
        string JdaRootPath { get; }
        string JdaHomePath { get; }
        string JdaEmployeeImportPath { get; }
        string ChangePasswordPath { get; }
        string NoChangePasswordUsernames { get; }
        int MinutesUntilChangePasswordLinkExpires { get; }
        string ChangePasswordEmailFrom { get; }
        IList<string> NewPasswordFormats { get; }
        string NewPasswordFormatError { get; }
        string PasswordResetEmailSent { get; }
        string JdaUserImportCreationSource { get; }
        bool PasswordCannotContainUsernameOrName { get; }
    }
}
