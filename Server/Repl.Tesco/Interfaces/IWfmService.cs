﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Interfaces
{
    public interface IWfmService
    {
        Task<List<Site>> GetSites();
    }
}
