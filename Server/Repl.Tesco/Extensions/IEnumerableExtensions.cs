﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<TItem> _Distinct<TItem, TKey>(this IEnumerable<TItem> source, Func<TItem, TKey> keySelector)
        {
            var keys = new Dictionary<TKey, bool>();
            var distinctIems = new List<TItem>();

            foreach (var item in source)
            {
                var key = keySelector.Invoke(item);
                if (!keys.ContainsKey(key))
                {
                    keys.Add(key, true);
                    distinctIems.Add(item);
                }
            }

            return distinctIems;
        }

        public static IEnumerable<IEnumerable<TItem>> _Group<TItem>(this IEnumerable<TItem> source, Func<TItem, TItem, bool> groupExpression)
        {
            var groups = new List<IEnumerable<TItem>>();
            List<TItem> currentGroup = null;
            var startNewGroup = true;
            var items = source.ToList();

            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                if (startNewGroup)
                {
                    currentGroup = new List<TItem>();
                    groups.Add(currentGroup);
                }

                currentGroup.Add(item);

                if (i + 1 < items.Count)
                {
                    var nextItem = items[i + 1];
                    startNewGroup = !groupExpression.Invoke(item, nextItem);
                }
            }

            return groups;
        }
    }
}
