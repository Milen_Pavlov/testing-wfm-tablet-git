﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Extensions
{
    public static class ExceptionExtensions
    {
        public static string _RecursiveMessage(this Exception source)
        {
            var message = new StringBuilder();
            return source._RecursiveMessage(message).ToString();
        }

        private static StringBuilder _RecursiveMessage(this Exception source, StringBuilder message)
        {
            message.Append(source.Message);
            if (source.InnerException != null)
            {
                message.Append(' ');
                source.InnerException._RecursiveMessage(message);
            }
            return message;
        }
    }
}
