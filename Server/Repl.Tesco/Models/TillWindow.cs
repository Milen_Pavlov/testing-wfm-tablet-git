﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class TillWindow
    {
        
        // Properties
        public int TillWindowId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        // Parents
        public int TillId { get; set; }
        public Till Till { get; set; }

        // Children

    }
}
