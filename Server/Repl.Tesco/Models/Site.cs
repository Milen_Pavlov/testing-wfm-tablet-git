﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class Site
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public bool isDefault { get; set; }

        public IList<OvertimeAgreed> AgreedOvertime { get; set; }
    }
}
