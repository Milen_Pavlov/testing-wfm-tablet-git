﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class OvertimeAgreed
    {
        public int Id { get; set; }
        public DateTimeOffset Week { get; set; }
        public int SiteId { get; set; }
        public double? OvertimeAgreedAmount { get; set; }

        public Site Site { get; set; }
    }
}
