﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class TillType
    {
        // Properties
        public int TillTypeId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public int OpeningSequence { get; set; }

        // Parents

        // Children
        public IList<Till> Tills { get; set; }

        public void Add(Till till)
        {
            if (Tills == null)
            {
                Tills = new List<Till>();
            }

            Tills.Add(till);
            till.Type = this;
            till.TypeId = TillTypeId;
        }

        public void Add(IList<Till> tills)
        {
            foreach (var till in tills)
            {
                Add(till);
            }
        }
    }
}
