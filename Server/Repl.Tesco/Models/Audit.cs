﻿using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class Audit
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string JsonData { get; set; }
        public string Message { get; set; }
        public DateTimeOffset DateOfChange { get; set; }

        public int DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}
