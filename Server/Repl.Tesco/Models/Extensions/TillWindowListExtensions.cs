﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Extensions
{
    public static class TillWindowListExtensions
    {
        public static bool _EachStartTimeLessThanEndTime(this IList<TillWindow> source)
        {
            foreach (var window in source)
            {
                if (window.StartTime >= window.EndTime)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool _AnyOverlaps(this IList<TillWindow> source)
        {
            var times = source.
                OrderBy(w => w.StartTime.TimeOfDay).
                SelectMany(w => new List<TimeSpan>() { w.StartTime.TimeOfDay, w.EndTime.TimeOfDay }).
                ToList();

            var anyOverlaps = times.
                Where((t, i) => i != 0 && t < times[i - 1]).
                Any();

            return anyOverlaps;
        }
    }
}
