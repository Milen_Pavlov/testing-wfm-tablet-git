﻿using Repl.Tesco.Models.Jda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Extensions
{
    public static class TillListExtensions
    {
        // Assumes we have all the tills for a given site
        public static IList<Till> _RefreshOpeningSequence(this IList<Till> source)
        {
            var orderedTills = source.OrderBy(t => t.OpeningSequence).ToList();
            for (var t = 0; t < orderedTills.Count; t++)
            {
                orderedTills[t].OpeningSequence = t;
            }
            return orderedTills;
        }

        /// <summary>
        /// Retrieves the first available till that is available at a specific time
        /// </summary>
        /// <param name="source"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static Till _OpenAndUnallocatedAt(this IList<Till> source, DateTime time)
        {
            foreach (var till in source)
            {
                if (till.Open(time.TimeOfDay) && !till.Allocated(time))
                {
                    return till;
                }
            }

            return null;
        }
    }
}
