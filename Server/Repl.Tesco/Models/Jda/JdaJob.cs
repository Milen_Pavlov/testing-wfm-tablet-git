﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Jda
{
    public class JdaJob
    {
        // Properties
        public int JobId { get; set; }
        public string JobName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        // Parents
        public int ShiftId { get; set; }
        public JdaShift Shift { get; set; }

        // Children

        // Other
        public JdaJob()
        {
        }

        public JdaJob(JToken json)
        {
            JobId = json["JobID"].Value<int>();
            JobName = json["JobName"].Value<string>();
            Start = json["Start"].Value<DateTime>();
            End = json["End"].Value<DateTime>();
        }

        public bool IsCheckoutJob
        {
            get
            {
                return (JobName != null && JobName.ToLower().Contains("checkout")); // ToDo: Weak
            }
        }
    }
}
