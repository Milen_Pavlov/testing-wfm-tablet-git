﻿using Consortium.Utility.RestClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Jda
{
    public class JdaSchedule
    {
        // Properties
        public int ScheduleId { get; set; }

        // Parents

        // Children
        public IList<JdaEmployee> Employees { get; set; }

        public void Add(JdaEmployee employee)
        {
            if (Employees == null)
            {
                Employees = new List<JdaEmployee>();
            }

            Employees.Add(employee);
            employee.Schedule = this;
            employee.ScheduleId = ScheduleId;
        }

        public void Add(IList<JdaEmployee> employees)
        {
            foreach (var employee in employees)
            {
                Add(employee);
            }
        }

        // Other
        public JdaSchedule()
        {
        }

        public JdaSchedule(JToken json)
        {
            var employees = json["Employees"].Select(e => new JdaEmployee(e)).ToList();
            Add(employees);
        }
    }
}
