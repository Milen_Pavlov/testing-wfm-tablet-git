﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Jda
{
    public class JdaSite
    {
        // Properties
        public int SiteId { get; set; }
        public string Name { get; set; }

        // Parents

        // Children

        // Other
        public JdaSite()
        {
        }

        public JdaSite(JToken json)
        {
            SiteId = json["ID"].Value<int>();
            Name = json["Name"].Value<string>();
        }
    }
}
