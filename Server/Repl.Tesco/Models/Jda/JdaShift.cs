﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Jda
{
    public class JdaShift
    {
        // Properties
        public int ShiftId { get; set; }

        // Parents
        public int EmployeeId { get; set; }
        public JdaEmployee Employee { get; set; }

        // Children
        public IList<JdaJob> Jobs { get; set; }

        public void Add(JdaJob job)
        {
            if (Jobs == null)
            {
                Jobs = new List<JdaJob>();
            }

            Jobs.Add(job);
            job.Shift = this;
            job.ShiftId = ShiftId;
        }

        public void Add(IList<JdaJob> jobs)
        {
            foreach (var job in jobs)
            {
                Add(job);
            }
        }

        // Other
        public JdaShift()
        {
        }

        public JdaShift(JToken json)
        {
            ShiftId = json["ShiftID"].Value<int>();

            var jobs = json["Jobs"].ToList().Select(j => new JdaJob(j)).ToList();
            Add(jobs);
        }
    }
}
