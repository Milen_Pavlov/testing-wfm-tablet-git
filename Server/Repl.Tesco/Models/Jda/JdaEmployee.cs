﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Jda
{
    public class JdaEmployee
    {
        // Properties
        public int EmployeeId { get; set; }
        public string FullName { get; set; }

        public Dictionary<DateTime, TimeSpan> HoursFilled { get; set; }

        // Parents
        public int ScheduleId { get; set; }
        public JdaSchedule Schedule { get; set; }

        // Children
        public IList<JdaShift> Shifts { get; set; }

        public void Add(JdaShift shift)
        {
            if (Shifts == null)
            {
                Shifts = new List<JdaShift>();
            }

            Shifts.Add(shift);
            shift.Employee = this;
            shift.EmployeeId = EmployeeId;
        }

        public void Add(IList<JdaShift> shifts)
        {
            foreach (var shift in shifts)
            {
                Add(shift);
            }
        }

        // Other
        public JdaEmployee()
        {
            HoursFilled = new Dictionary<DateTime, TimeSpan>();
        }

        public JdaEmployee(JToken json)
        {
            HoursFilled = new Dictionary<DateTime, TimeSpan>();

            EmployeeId = json["EmployeeID"].Value<int>();
            FullName = json["FullName"].Value<string>();
            
            var shifts = json["Shifts"].ToList().Select(s => new JdaShift(s)).ToList();
            Add(shifts);
        }
    }
}
