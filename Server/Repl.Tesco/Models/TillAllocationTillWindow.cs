﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class TillAllocationTillWindow
    {
        #region Core properties

        // Properties
        public int TillAllocationTillWindowId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }

        // Parents
        public int TillAllocationId { get; set; }
        public TillAllocation TillAllocation { get; set; }

        // Children

        #endregion
    }
}
