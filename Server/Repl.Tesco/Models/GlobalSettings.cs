﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class GlobalSettings
    {
        public int Id { get; set; }
        public int? MaximumHours { get; set; }
        public int Site { get; set; }
        
    }
}
