﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Overtime
{
    public class Break
    {
        //Properties
        public int SiteID { get; set; }
        public int ShiftID { get; set; }
        public int BreakID { get; set; }
        public bool BreakTaken { get; set; }
    }
}