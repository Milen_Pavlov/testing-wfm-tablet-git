﻿using Repl.Tesco.Extensions;
using WFM.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repl.Tesco.Constants;

namespace Repl.Tesco.Models
{
    public class Till
    {
        #region Core properties

        // Properties
        public int TillId { get; set; }
        public string TillNumber { get; set; }
        public int OpeningSequence { get; set; }
        public int SiteId { get; set; }

        // Parents
        public int StatusId { get; set; }
        public TillStatus Status { get; set; }

        public int TypeId { get; set; }
        public TillType Type { get; set; }

        // Children
        public IList<TillWindow> Windows { get; set; }

        public void Add(TillWindow window)
        {
            if (Windows == null)
            {
                Windows = new List<TillWindow>();
            }

            Windows.Add(window);
            window.Till = this;
            window.TillId = TillId;
        }

        public void Add(IList<TillWindow> windows)
        {
            foreach (var window in windows)
            {
                Add(window);
            }
        }

        public IList<TillAllocation> Allocations { get; set; }

        public void Add(TillAllocation allocation)
        {
            if (Allocations == null)
            {
                Allocations = new List<TillAllocation>();
            }

            Allocations.Add(allocation);
            allocation.Till = this;
            allocation.TillId = TillId;
            allocation.StampedTillId = TillId;
        }

        public void Add(IList<TillAllocation> allocations)
        {
            foreach (var allocation in allocations)
            {
                Add(allocation);
            }
        }

        #endregion

        public IList<TillWindow> OrderedWindows
        {
            get
            {
                return (Windows == null ? null : Windows.OrderBy(w => w.StartTime).ToList());
            }
        }

        public bool Open(TimeSpan time)
        {
            if (Status.Key != TillStatusKey.Open)
            {
                return false;
            }
            else if (Type.Key != TillTypeKey.Basket)
            {
                return true; // Main-bank tills always open
            }

            return Windows.Any(w => w.StartTime.TimeOfDay <= time && time < w.EndTime.TimeOfDay);
        }

        public bool Allocated(DateTime time)
        {
            return Allocations._NewIfNull().Any(a => a.StartTime <= time && time < a.EndTime);
        }

        public DateTime? NextOpenAndUnallocatedAfter(DateTime time)
        {
            if (Status.Key != TillStatusKey.Open)
            {
                return null;
            }

            bool timeChanged;
            do
            {
                timeChanged = false;

                if (Type.Key == TillTypeKey.Basket)
                {
                    var inWindow = Windows.Any(w => w.StartTime.TimeOfDay <= time.TimeOfDay && time.TimeOfDay < w.EndTime.TimeOfDay);
                    if (!inWindow)
                    {
                        var nextWindow = OrderedWindows.FirstOrDefault(w => w.StartTime.TimeOfDay >= time.TimeOfDay);
                        if (nextWindow == null)
                        {
                            nextWindow = OrderedWindows.First(); // Wrap around to the first window of the day
                            time = time.AddDays(1);
                        }

                        time = time.Date.Add(nextWindow.StartTime.TimeOfDay);
                        timeChanged = true;
                    }
                }

                var allocations = Allocations._NewIfNull().Where(a => a.StartTime <= time && time < a.EndTime).ToList();
                if (allocations.Any())
                {
                    time = allocations.Max(a => a.EndTime);
                    timeChanged = true;
                }
            }
            while (timeChanged);

            return time;
        }

        /// <summary>
        /// Simple orderby to return tills that have a higher priority then this one
        /// </summary>
        /// <param name="competingTills"></param>
        /// <returns></returns>
        public IList<Till> HigherPriorityTills(IList<Till> competingTills)
        {
            return competingTills.Where(t => t.Type.OpeningSequence < Type.OpeningSequence).ToList();
        }

        public int Priority
        {
            get
            {
                if (OpeningSequence >= 10000) // Assumes maximum OpeningSequence of 9999
                {
                    throw new NotSupportedException();
                }
                return 10000 * Type.OpeningSequence + OpeningSequence;
            }
        }
    }
}
