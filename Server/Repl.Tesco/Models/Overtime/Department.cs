﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Overtime
{
    public class Department
    {
        public Department()
        {
            Audits = new List<Audit>();
        }

        //Properties
        public int DepartmentId { get; set; }
        public int JDADepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int SiteId { get; set; }

        //Children
        public IList<OvertimeMapping> OvertimeHours { get; set; }
        public IList<Audit> Audits { get; set; }
    }
}