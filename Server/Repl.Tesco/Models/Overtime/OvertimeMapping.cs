﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models.Overtime
{
    public class OvertimeMapping
    {
        //Properties
        public DateTime ForDate { get; set; }
        public float? Hours { get; set; }

        public float? ResourceRequestDay { get; set; }
        public float? ResourceRequestNight { get; set; }
        public float? ResourceAgreedNight { get; set; }

        // Parents
        public int DepartmentId { get; set; }
        public Department Department { get; set; }


    }
}
