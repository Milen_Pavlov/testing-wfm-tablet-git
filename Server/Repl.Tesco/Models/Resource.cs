﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Models
{
    public class Resource
    {
        public int Id { get; set; }
        public int Site { get; set; }
        public string JsonData { get; set; }
        public DateTimeOffset Date { get; set; }

        public string Username { get; set; }
        public DateTime LockedDateTime { get; set; }
    }
}
