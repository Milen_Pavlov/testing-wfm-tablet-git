﻿using Repl.Tesco.Extensions;
using WFM.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Constants = WFM.Shared.Constants;

namespace Repl.Tesco.Models
{
    public class TillAllocation
    {
        #region Core properties

        // Properties
        public int TillAllocationId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public int? TillId { get; set; }
        public int? StampedTillId { get; set; } // Preserve the till ID even if the till is deleted
        public string TillNumber { get; set; }
        public bool CheckoutSupport { get; set; }
        public string TillTypeKey { get; set; }
        public string TillStatusKey { get; set; }
        public int? TillOpeningSequence { get; set; }

        public TillAllocation()
        {

        }

        public TillAllocation(WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation fromAllocation)
        {
            TillAllocationId = fromAllocation.TillAllocationId;
            StartTime = fromAllocation.StartTime;
            EndTime = fromAllocation.EndTime;
            SiteId = fromAllocation.SiteId;
            SiteName = fromAllocation.SiteName;
            EmployeeId = fromAllocation.EmployeeId;
            EmployeeFullName = fromAllocation.EmployeeFullName;
            TillId = fromAllocation.TillId;
            StampedTillId = fromAllocation.StampedTillId;
            TillNumber = fromAllocation.TillNumber;
            CheckoutSupport = fromAllocation.CheckoutSupport;
            TillTypeKey = fromAllocation.TillTypeKey;
            TillStatusKey = fromAllocation.TillStatusKey;
            TillOpeningSequence = fromAllocation.TillOpeningSequence;

            TillWindows = new List<TillAllocationTillWindow>();

            if (fromAllocation.TillWindows != null)
            {
                foreach (var window in fromAllocation.TillWindows)
                {
                    TillWindows.Add(new TillAllocationTillWindow()
                        {
                            StartTime = window.StartTime,
                            EndTime = window.EndTime,
                            TillAllocationId = this.TillAllocationId,
                            TillAllocationTillWindowId = window.TillAllocationTillWindowId,
                            TillAllocation = this
                        });
                }
            }
        }

        // Parents
        public Till Till { get; set; }

        public void ClearTill()
        {
            Till = null;
            TillId = null;
            StampedTillId = null;
        }

        // Children
        public IList<TillAllocationTillWindow> TillWindows { get; set; }

        public void Add(TillAllocationTillWindow tillWindow)
        {
            if (TillWindows == null)
            {
                TillWindows = new List<TillAllocationTillWindow>();
            }

            TillWindows.Add(tillWindow);
            tillWindow.TillAllocation = this;
            tillWindow.TillAllocationId = TillAllocationId;
        }

        public void Add(IList<TillAllocationTillWindow> tillWindows)
        {
            foreach (var tillWindow in tillWindows)
            {
                Add(tillWindow);
            }
        }

        #endregion

        public void CalculateEndTime(IList<Till> competingTills, DateTime jobEndTime)
        {
            var endTimes = new List<DateTime>() { jobEndTime };

            // End the allocation when a higher priority till is available
            var higherPriorityTills = (Till == null ? competingTills : Till.HigherPriorityTills(competingTills));
            if (higherPriorityTills.Any())
            {
                // add a unallocated time to higher priority tills
                var unallocatedTimes = new List<DateTime?>();
                foreach (var higherPriorityTill in higherPriorityTills)
                {
                    var unallocatedTime = higherPriorityTill.NextOpenAndUnallocatedAfter(StartTime);
                    unallocatedTimes.Add(unallocatedTime);
                }

                var earliestUnallocatedTime = unallocatedTimes.Where(t => t != null && t.Value < jobEndTime).Min(t => t);
                if (earliestUnallocatedTime != null)
                {
                    endTimes.Add(earliestUnallocatedTime.Value);
                }
            }

            // End the allocation when a basket till's window ends
            if (Till != null && Till.Type.Key == Constants.TillTypeKey.Basket)
            {
                var windowEndTime = Till.Windows.Single(w => w.StartTime.TimeOfDay <= StartTime.TimeOfDay && StartTime.TimeOfDay < w.EndTime.TimeOfDay).EndTime.TimeOfDay;

                if (windowEndTime < jobEndTime.TimeOfDay)
                {
                    endTimes.Add(jobEndTime.Date.Add(windowEndTime));
                }

                //Are we leaking onto the next day?
                var windowLastOpen = Till.Windows.Max(window => window.EndTime);

                //if(StartTime.AddDays((StartTime.Date - jobEndTime.Date).Days) > jobEndTime)
                {
                    endTimes.Add(StartTime.Date + windowLastOpen.TimeOfDay);
                }
            }

            // End the allocation when we run into another allocation
            if (Till != null)
            {
                var conflictingAllocations = Till.Allocations._NewIfNull().Where(a => a != this && StartTime <= a.StartTime && a.StartTime < jobEndTime).ToList();
                if (conflictingAllocations.Any())
                {
                    endTimes.Add(conflictingAllocations.Min(a => a.StartTime));
                }
            }

            // End the allocation when the job ends
            EndTime = endTimes.Min();
        }
    }
}
