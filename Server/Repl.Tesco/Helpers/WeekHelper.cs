﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Helpers
{
    public class WeekHelper
    {
        /// <summary>
        /// Get the tesco week number for a specified datetime
        /// </summary>
        /// <returns>The week number for date.</returns>
        /// <param name="forDate">For date.</param>
        public static int GetWeekNumberForDate(DateTime forDate, AccountingConfiguration configuration)
        {
            var accountingYearStart = AccountingYearStart(forDate, configuration);

            var weekNumber = ((forDate - accountingYearStart).Days / 7) + 1; //Starting from 1

            return weekNumber;
        }

        /// <summary>
        /// Get the start of the accounting period that contains the specified datetime
        /// </summary>
        /// <returns>The start of the accounting year.</returns>
        /// <param name="inYear">For this date.</param>
        public static DateTime AccountingYearStart(DateTime forDate, AccountingConfiguration configuration)
        {
            //First week in the accounting period is always the week that contains 1st march
            var firstMarch = new DateTime(forDate.Year, configuration.PeriodFirstMonth, configuration.PeriodFirstDay);

            //And then we look at the week starting at the configured day of week that contains 1st march
            int dayOffset = (firstMarch.DayOfWeek - configuration.StartDay);

            if (dayOffset < 0)
                dayOffset += 7;

            var firstDate = firstMarch.AddDays(-dayOffset);

            //If our input is before the first date for this year, then we calculate the year start for 1st march for the year before as we're somewhere between 
            if (forDate < firstDate)
            {
                var previous1stMarch = new DateTime(forDate.Year - 1, configuration.PeriodFirstMonth, configuration.PeriodFirstDay);

                dayOffset = (previous1stMarch.DayOfWeek - configuration.StartDay);

                if (dayOffset < 0)
                    dayOffset += 7;

                return previous1stMarch.AddDays(-dayOffset);
            }
            else
                return firstDate;
        }

        public class AccountingConfiguration
        {
            public DayOfWeek StartDay = DayOfWeek.Monday;
            //Accounting period normally starts on the first of march
            public int PeriodFirstDay = 2;
            public int PeriodFirstMonth = 3;
        }
    }
}
