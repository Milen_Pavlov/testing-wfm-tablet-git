﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Helpers
{
    public static class ConfigHelper
    {
        public static ConnectionStringSettings SqlConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SQLDatabase"];
            }
        }
    }
}
