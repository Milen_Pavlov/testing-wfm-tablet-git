﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Constants
{
    public static class TillTypeKey
    {
        public const string MainBank = "MainBank";
        public const string Basket = "Basket";
    }
}
