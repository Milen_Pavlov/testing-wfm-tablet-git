﻿using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Interfaces.Repositories.Jda;
using Repl.Tesco.Plugin.Rest.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace Repl.Tesco.Plugin.Rest.Services
{ 
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class OvertimeService : ConsortiumService, IOvertimeService
    {
        public IOvertimeRepository OvertimeRepository { get; private set; }
        public IJdaSessionRepository JdaSessionRepository { get; private set; }
        public IJdaSiteRepository JdaSiteRepository { get; private set; }

        public OvertimeService(IOvertimeRepository overtimeRepository, IJdaSessionRepository jdaSessionRepository, IJdaSiteRepository jdaSiteRepository, IJdaScheduleRepository jdaScheduleRepository)
        {
            OvertimeRepository = overtimeRepository;
            JdaSessionRepository = jdaSessionRepository;
            JdaSiteRepository = jdaSiteRepository;
        }

        public ReadOvertimeResponse Post(ReadOvertimeRequest request)
        {
            var response = new ReadOvertimeResponse();

            var allocations = OvertimeRepository.GetOvertime(request.SiteId, request.ForDate);

            response.Departments = new List<OvertimeRecord>();

            foreach (var allocation in allocations)
            {
                response.Departments.Add(new OvertimeRecord()
                {
                     JDADepartmentId = allocation.JDADepartmentId,
                     DepartmentName = allocation.DepartmentName,
                     AllocatedHours = allocation.OvertimeHours != null ? allocation.OvertimeHours.FirstOrDefault().Hours : null,
                     ResourceRequestNight = allocation.OvertimeHours != null ? allocation.OvertimeHours.FirstOrDefault().ResourceRequestNight : null,
                     ResourceRequestDay = allocation.OvertimeHours != null ? allocation.OvertimeHours.FirstOrDefault().ResourceRequestDay : null,
                     ResourceAgreedNight = allocation.OvertimeHours != null ? allocation.OvertimeHours.FirstOrDefault().ResourceAgreedNight : null
                });
            }

            return response;
        }

        public SetOvertimeResponseLegacy Post(SetOvertimeRequestLegacy request)
        {
            var response = new SetOvertimeResponseLegacy();

            OvertimeRepository.SetOvertime(request.SiteId, request.ForDate,request.Username, request.Overtime);

            return response;
        }

        public SetOvertimeResponse Post(SetOvertimeRequest request)
        {
            var response = new SetOvertimeResponse();

            OvertimeRepository.SetOvertime(request.SiteId, request.ForDate, request.Username, request.Overtime);

            return response;
        }
    }
}