﻿using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Plugin.Rest.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Breaks;

namespace Repl.Tesco.Plugin.Rest.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class BreakService : ConsortiumService, IBreakService
    {
        public IBreakRepository BreakRepository { get; private set; }

        public BreakService(IBreakRepository breakRepository)
        {
            BreakRepository = breakRepository;
        }

        public ReadBreaksResponse Post(ReadBreaksRequest request)
        {
            var response = BreakRepository.GetBreaks(request.SiteId, request.ShiftIDs);

            return new ReadBreaksResponse() { Breaks = response };
        }

        public SetBreaksResponse Post(SetBreaksRequest request)
        {
            BreakRepository.SetBreaks(request.Breaks);

            return new SetBreaksResponse();
        }
    }
}