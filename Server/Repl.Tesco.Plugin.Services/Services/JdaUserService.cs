﻿using Consortium.Service.Engine;
using Repl.Tesco.Interfaces.Repositories.Jda;
using WFM.Shared.DataTransferObjects.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared;
using Repl.Tesco.Plugin.Rest.Interfaces;

namespace Repl.Tesco.Plugin.Rest.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class JdaUserService : ConsortiumService, IUserService
    {
        public IJdaSessionRepository JdaSessionRepository { get; set; }

        public JdaUserService(IJdaSessionRepository jdaSessionRepository)
        {
            JdaSessionRepository = jdaSessionRepository;
        }

        public LogInResponse Post(LogInRequest request)
        {
            var response = new LogInResponse();
            var messages = new MessageList();
            
            var sessionId = JdaSessionRepository.LogIn(request.JDAUsername, request.JDAPassword);
            if (sessionId == null)
            {
                messages.AddError("JDA login failed").CopyTo(response);
                return response;
            }

            response.JdaSessionId = sessionId;
            messages.AddSuccess("JDA login succeeded").CopyTo(response);
            return response;
        }
    }
}
