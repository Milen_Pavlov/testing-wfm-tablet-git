﻿using AutoMapper;
using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using Repl.Tesco.Plugin.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WFM.Shared.DataTransferObjects.Resources;

namespace Repl.Tesco.Plugin.Services.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class ResourceService : ConsortiumService, IResourceService
    {
        private readonly IResourceRepository m_resourceRepository;
        private readonly IAuditRepository m_auditRepository;
        private readonly IDepartmentRepository m_departmentRepository;

        public ResourceService(IResourceRepository resourceRepo, IAuditRepository auditRepo, IDepartmentRepository departmentRepo)
        {
            m_resourceRepository = resourceRepo;
            m_auditRepository = auditRepo;
            m_departmentRepository = departmentRepo;

            Mapper.CreateMap<Resource, ResourceRecord>()
                .ForMember(x => x.Date, a => a.MapFrom(b => b.Date.DateTime));
        }

        public GetResourceDataResponse Get(GetResourceDataRequest req)
        {
            if (req.Date == null)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a week");
            }

            var results = m_resourceRepository.Get(req.SiteId, req.Date);
            var mapped = Mapper.Map<ResourceRecord>(results);

            return new GetResourceDataResponse() { Resources = mapped };
        }

        public SetResourceDataResponse Post(SetResourceDataRequest req)
        {
            if (req.Date == null)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a date");
            }

            if (string.IsNullOrEmpty(req.Json))
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify json data");
            }

            if (string.IsNullOrEmpty(req.Username))
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a username");
            }

            DateTime lockedDateTime = DateTime.UtcNow;
            var department = m_departmentRepository.Get(req.SiteId);

            if (department != null)
            {
               m_auditRepository.Create(
               new Audit()
               {
                   Message = string.Format("Locked resource data on {0}", lockedDateTime.ToString()),
                   DateOfChange = req.Date,
                   Username = req.Username,
                   JsonData = "",
                   DepartmentId = department.DepartmentId
               });
            }

            m_resourceRepository.Insert(new Resource()
                {
                    Date = req.Date,
                    Site = req.SiteId,
                    JsonData = req.Json,
                    LockedDateTime = lockedDateTime,
                    Username = req.Username
                });

            return new SetResourceDataResponse() { Success = true };
        }

        public UnsetResourceDataResponse Post(UnsetResourceDataRequest req)
        {
            if (req.Date == null)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a date");
            }

            m_resourceRepository.RemoveAll(req.SiteId, req.Date);
            return new UnsetResourceDataResponse() { Success = true };
        }
    }
}
