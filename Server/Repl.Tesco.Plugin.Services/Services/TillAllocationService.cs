﻿using Consortium.Service.Engine;
using Consortium.Model.Engine.Paging;
using Consortium.Utility.RestClient;
using Newtonsoft.Json.Linq;
using WFM.Shared.Constants;
using WFM.Shared.Extensions;
using Repl.Tesco.Data.Jda;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Interfaces.Repositories.Jda;
using Repl.Tesco.Models.Jda;
using Repl.Tesco.Models.Extensions;
using Repl.Tesco.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Repl.Tesco.Models;
using WFM.Shared;
using WFM.Shared.DataTransferObjects.TillAllocation;
using Repl.Tesco.Plugin.Rest.Interfaces;
using System.Web;

namespace Repl.Tesco.Plugin.Rest.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class TillAllocationService : ConsortiumService, ITillAllocationService
    {
        private const int _MAXIMUMUNIONSTANDARDHOURS = 4;

        public ITillAllocationRepository TillAllocationRepository { get; set; }
        public ITillRepository TillRepository { get; set; }
        public IJdaSessionRepository JdaSessionRepository { get; set; }
        public IJdaSiteRepository JdaSiteRepository { get; set; }
        public IJdaScheduleRepository JdaScheduleRepository { get; set; }
        private readonly IGlobalSettingsRepository m_globalSettingsRepository;

        public TillAllocationService(ITillAllocationRepository tillAllocationRepository, ITillRepository tillRepository, IJdaSessionRepository jdaSessionRepository, IJdaSiteRepository jdaSiteRepository, IJdaScheduleRepository jdaScheduleRepository,
             IGlobalSettingsRepository globalSettingsRepo)
        {
            TillAllocationRepository = tillAllocationRepository;
            TillRepository = tillRepository;
            JdaSessionRepository = jdaSessionRepository;
            JdaSiteRepository = jdaSiteRepository;
            JdaScheduleRepository = jdaScheduleRepository;
            m_globalSettingsRepository = globalSettingsRepo;
        }

        private class DateRange
        {
            public DateTime From { get; set; }
            public DateTime To { get; set; }
        }

        public ReadResponse Post(ReadRequest request)
        {
            var response = new ReadResponse();
            var messages = new MessageList();

            // Validate request
            var zeroTime = new TimeSpan();
            if (request.From.TimeOfDay != zeroTime || request.To.TimeOfDay != zeroTime)
            {
                messages.AddError("The time components of 'startDate' and 'endDate' must zero").CopyTo(response);
                return response;
            }
            else if (request.From >= request.To)
            {
                messages.AddError("'startDate' must be before 'endDate'").CopyTo(response);
                return response;
            }

            // Read allocations
            var allocations = TillAllocationRepository.Read(request.SiteId, request.From, request.To, Pager.TakeAll);

            // Determine days where there are no allocations
            var numOfDays = (request.To.Date - request.From.Date).Days;
            var dateRanges = Enumerable.Range(0, numOfDays).
                Select(d => request.From.AddDays(d)).
                Where(d => !allocations.Any(a => d == a.StartTime.Date)).
                _Group((d1, d2) => d1.AddDays(1) == d2).
                Select(i => new DateRange()
                {
                    From = i.Min().Date,
                    To = i.Max().AddDays(1).Date
                }).
                ToList();

            // Create allocations
            foreach (var dateRange in dateRanges)
            {
                var createRequest = new CreateRequest()
                {
                    JdaSessionId = request.JdaSessionId,
                    SiteId = request.SiteId,
                    From = dateRange.From.Date,
                    To = dateRange.To.Date
                };

                var createResponse = Post(createRequest);
                if (!createResponse.Success)
                {
                    messages.Add(createResponse.Messages).CopyTo(response);
                    return response;
                }
            }

            // Refresh allocations
            var pager = request._CreatePager();
            allocations = TillAllocationRepository.Read(request.SiteId, request.From, request.To, pager);

            // Return response
            response.TillAllocations = allocations.
                Select(a => new WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocation()
                {
                    TillAllocationId = a.TillAllocationId,
                    StartTime = a.StartTime,
                    EndTime = a.EndTime,
                    SiteId = a.SiteId,
                    SiteName = a.SiteName,
                    EmployeeId = a.EmployeeId,
                    EmployeeFullName = a.EmployeeFullName,
                    TillId = a.TillId,
                    StampedTillId = a.StampedTillId,
                    TillNumber = a.TillNumber,
                    CheckoutSupport = a.CheckoutSupport,
                    TillTypeKey = a.TillTypeKey,
                    TillStatusKey = a.TillStatusKey,
                    TillOpeningSequence = a.TillOpeningSequence,
                    TillWindows = a.TillWindows.
                        _NewIfNull().
                        Select(w => new WFM.Shared.DataTransferObjects.TillAllocation.Model.TillAllocationTillWindow()
                        {
                            TillAllocationTillWindowId = w.TillAllocationTillWindowId,
                            StartTime = w.StartTime,
                            EndTime = w.EndTime,
                        }).
                        ToArray(),
                }).
                ToArray();

            response.NumOfItemsOverAllPages = pager.NumOfItems;
            messages.AddSuccess("The allocations were read").CopyTo(response);
            return response;
        }

        public CreateResponse Post(CreateRequest request)
        {
            var response = new CreateResponse();
            var messages = new MessageList();

            // Validate request
            var zeroTime = new TimeSpan();
            if (request.From.TimeOfDay != zeroTime || request.To.TimeOfDay != zeroTime)
            {
                messages.AddError("The time components of 'startDate' and 'endDate' must zero").CopyTo(response);
                return response;
            }
            else if (request.From >= request.To)
            {
                messages.AddError("'startDate' must be before 'endDate'").CopyTo(response);
                return response;
            }

            // Ensure logged into JDA

            if (JdaSessionRepository.HasServiceUser)
            {
                var session = JdaSessionRepository.LoginServiceUser();

                if (string.IsNullOrEmpty(session))
                {
                    messages.AddError("JDA Service Login failed").CopyTo(response);
                    return response;
                }
                else
                {
                    request.JdaSessionId = session;
                }
            }
            else if (!String.IsNullOrEmpty(request.JDAUsername) && !String.IsNullOrEmpty(request.JDAPassword))
            {
                var session = JdaSessionRepository.LogIn(request.JDAUsername, request.JDAPassword);

                if (string.IsNullOrEmpty(session))
                {
                    messages.AddError("JDA login failed").CopyTo(response);
                    return response;
                }
                else
                {
                    request.JdaSessionId = session;
                }
            }

            var authenticated = JdaSessionRepository.Authenticated(request.JdaSessionId);
            if (!authenticated)
            {
                messages.AddError("JDA authentication failed").CopyTo(response);
                return response;
            }

            // Read JDA site
            var site = JdaSiteRepository.ReadById(request.JdaSessionId, request.SiteId);
            if (site == null)
            {
                messages.AddError("Unable to read JDA site").CopyTo(response);
                return response;
            }

            // Read JDA schedule between given dates
            var schedules = new List<JdaSchedule>();
            var scheduleDate = request.From.Date;
            do
            {
                var schedule = JdaScheduleRepository.Read(request.JdaSessionId, request.SiteId, scheduleDate);
                if (schedule == null)
                {
                    messages.AddError("Unable to read JDA schedule").CopyTo(response);
                    return response;
                }

                schedules.Add(schedule);

                var jobs = schedule.
                    Employees.
                    _NewIfNull().
                    SelectMany(e => e._NewIfNull().Shifts._NewIfNull()).
                    SelectMany(s => s._NewIfNull().Jobs._NewIfNull()).
                    ToList();

                var scheduleEnd = (jobs.Any() ? jobs.Max(j => j.Start) : DateTime.MinValue).Date;
                scheduleDate = (scheduleEnd < scheduleDate.AddDays(1) ? scheduleDate.AddDays(1) : scheduleEnd); // Must advance at least 1 day
            }
            while (scheduleDate < request.To);

            // Filter jobs
            var allJobs = schedules.
                SelectMany(s => s._NewIfNull().Employees._NewIfNull()).
                SelectMany(e => e._NewIfNull().Shifts._NewIfNull()).
                SelectMany(s => s._NewIfNull().Jobs._NewIfNull()).
                ToList();

            var checkoutJobs = allJobs.
                Where(j =>
                    j != null &&
                    j.Shift != null &&
                    j.Shift.Employee != null &&
                    j.Start >= request.From &&
                    j.End <= request.To &&
                    j.IsCheckoutJob).
                ToList();

            var distinctJobs = checkoutJobs.
                _Distinct(j => string.Format("{0}-{1}-{2}", j.Shift.Employee.EmployeeId, j.Start, j.End)).
                ToList();

            // Read tills
            var tills = TillRepository.
                ReadBySite(request.SiteId, Pager.TakeAll).
                OrderBy(t => t.Type.OpeningSequence).
                ThenBy(t => t.OpeningSequence).
                ToList();

            var settings = m_globalSettingsRepository.Get(site.SiteId);

            settings = settings ?? new GlobalSettings() { MaximumHours = _MAXIMUMUNIONSTANDARDHOURS, Site = request.SiteId };

            // First pass: Create allocations
            var tillAllocations = CreateTillAllocations(distinctJobs, tills, site, settings);

            List<TillAllocation> basketTillAllocations = new List<TillAllocation>();
            if(settings != null && (settings.MaximumHours.HasValue))
            {
                // before we put in till allocations to merge, strip out any that are on basket tills, add them back in after
                basketTillAllocations = tillAllocations.Where(p => p.Till != null && p.Till.Type.Key.Equals("Basket")).ToList();
                tillAllocations.RemoveAll(p => p.Till != null && p.Till.Type.Key.Equals("Basket"));
                // merge basket tills independantly
                basketTillAllocations = MergeTillAllocations(basketTillAllocations);
            }
            
            // Second pass: merge them
            tillAllocations = MergeTillAllocations(tillAllocations);

            // add basket tills back in
            tillAllocations.AddRange(basketTillAllocations);

            // Commit
            TillAllocationRepository.Delete(request.SiteId, request.From, request.To);
            TillAllocationRepository.Create(tillAllocations);

            // Success
            messages.AddSuccess("The till allocations were created").CopyTo(response);
            return response;
        }

        private List<TillAllocation> CreateTillAllocations(IList<JdaJob> jobs, IList<Till> tills, JdaSite site, GlobalSettings settings)
        {
            // order the jobs primarily by the start date, secondarily by the fullname
            var orderedJobs = jobs.
                OrderBy(j => j.Start).
                ThenBy(j => j.Shift.Employee.FullName).
                ToList();

            var tillAllocations = new List<TillAllocation>();
            foreach (JdaJob job in orderedJobs)
            {
                // go through each job, based on its time period (start to end)
                var startTime = job.Start;

                while (startTime < job.End)
                {
                    // check that we can work on a basket through comparing against what we have against the maximum hours value
                    bool canWorkOnBasketTill = true;
                    
                    if (settings != null && (settings.MaximumHours.HasValue))
                    {
                        // we use a dictionary to store per day total basket hours for a user
                        TimeSpan value;
                        bool success = job.Shift.Employee.HoursFilled.TryGetValue(job.Start.Date, out value);
                        if(!success)
                        {
                            value = new TimeSpan(0,0,0);
                            job.Shift.Employee.HoursFilled.Add(job.Start.Date, value);
                        }
                        
                        if (value.Hours >= settings.MaximumHours.GetValueOrDefault())
                        {
                            canWorkOnBasketTill = false;
                        }
                    }

                    // get the first available till at this time
                    Till selectedTill;
                    if(!canWorkOnBasketTill)
                    {
                        // if we are at full capacity for basket working hours, then filter the list we pass down to be everything but basket tills
                        selectedTill = tills.Where(p => !p.Type.Key.Equals("Basket")).ToList()._OpenAndUnallocatedAt(startTime);
                    }
                    else
                    {
                        selectedTill = tills._OpenAndUnallocatedAt(startTime);
                    }


                    if (CanEmployeeBeAllocated(job.Shift.Employee, selectedTill, settings, job.Start.Date))
                    {
                        // create new allocation at this start time, regardless if there is a till or not
                        var tillAllocation = new TillAllocation()
                        {
                            StartTime = startTime,
                            SiteId = site.SiteId,
                            SiteName = site.Name,
                            EmployeeId = job.Shift.Employee.EmployeeId,
                            EmployeeFullName = job.Shift.Employee.FullName,
                            CheckoutSupport = (selectedTill == null)
                        };

                        // if there is a till available at this point
                        if (selectedTill != null)
                        {
                            selectedTill.Add(tillAllocation);
                            tillAllocation.TillNumber = selectedTill.TillNumber;
                            tillAllocation.TillTypeKey = selectedTill.Type.Key;
                            tillAllocation.TillStatusKey = selectedTill.Status.Key;
                            tillAllocation.TillOpeningSequence = selectedTill.OpeningSequence;

                            if (selectedTill.Windows != null)
                            {
                                foreach (var tillWindow in selectedTill.Windows)
                                {
                                    var tillAllocationTillWindow = new TillAllocationTillWindow()
                                    {
                                        StartTime = tillWindow.StartTime.TimeOfDay,
                                        EndTime = tillWindow.EndTime.TimeOfDay,
                                    };
                                    tillAllocation.Add(tillAllocationTillWindow);
                                }
                            }
                        }

                        // again, if we are at full capacity for working on basket tills, do not allow the end time to acommodate based on this
                        if (!canWorkOnBasketTill)
                        {
                            tillAllocation.CalculateEndTime(tills.Where(p => !p.Type.Key.Equals("Basket")).ToList(), job.End);
                        }
                        else
                        {
                            tillAllocation.CalculateEndTime(tills, job.End);
                        }

                        IsTillAllocationValid(tillAllocation, job.Shift.Employee, settings, job.Start.Date);
                        startTime = tillAllocation.EndTime;
                        tillAllocations.Add(tillAllocation);
                    }
                    else
                    {
                        // if we get here, that means the hours have been filled for somebody
                        // in this case, break onto another job
                        startTime = job.End;
                    }
                }
            }

            return tillAllocations;
        }

        private List<TillAllocation> MergeTillAllocations(List<TillAllocation> tillAllocations)
        {
            var tills = tillAllocations.Select(a => a.Till).Distinct().OrderBy(t => (t == null ? int.MaxValue : t.Priority)).ToList(); // Assumes identical tills are same instances
            bool changed;

            do
            {
                changed = false;

                // this will typically execute two iterations, that way we can go through allocations from start data ASC, then EndTime DESC
                for (var direction = 1; direction >= -1; direction -= 2)
                {
                    tillAllocations = (direction == 1 ? tillAllocations.OrderBy(a => a.StartTime) : tillAllocations.OrderByDescending(a => a.EndTime)).ToList();
                    // now go through all allocations in certain order
                    for (var allocationIndex = 0; allocationIndex < tillAllocations.Count; allocationIndex++)
                    {
                        // we need to know if there are any overlaps or 'splits'
                        // for the current employee of a till allocation, if there
                        // are, we need to replace this allocation
                        var anchor = tillAllocations[allocationIndex];
                        TillAllocation split = tillAllocations.SingleOrDefault(a => a.EmployeeId == anchor.EmployeeId && (direction == 1 ? a.StartTime == anchor.EndTime : a.EndTime == anchor.StartTime));

                        if (split != null && !anchor.CheckoutSupport) // The allocation is split
                        {
                            var replacementsByTill = tills.
                                Select(t => new ReplacementTillAllocations(tillAllocations, anchor, split, t, direction)).
                                Where(r => r.Replacements != null).
                                OrderBy(r => r.Priority).
                                ToList();

                            var replacements = replacementsByTill.LastOrDefault(); // Choose replacement from lowest priority till
                            if (replacements != null)
                            {
                                foreach (var replacement in replacements.Replacements)
                                {
                                    if (split.Till == null)
                                    {
                                        replacement.ClearTill();
                                    }
                                    else
                                    {
                                        split.Till.Add(replacement);
                                    }

                                    replacement.TillNumber = split.TillNumber;
                                    replacement.TillOpeningSequence = split.TillOpeningSequence;
                                    replacement.TillTypeKey = split.TillTypeKey;
                                    replacement.TillStatusKey = split.TillStatusKey;
                                    replacement.TillWindows = split.TillWindows;
                                    replacement.CheckoutSupport = split.CheckoutSupport;
                                }

                                anchor.Till.Add(split);
                                split.TillNumber = anchor.TillNumber;
                                split.TillOpeningSequence = anchor.TillOpeningSequence;
                                split.TillTypeKey = anchor.TillTypeKey;
                                split.TillStatusKey = anchor.TillStatusKey;
                                split.TillWindows = anchor.TillWindows;
                                split.CheckoutSupport = anchor.CheckoutSupport;

                                anchor.StartTime = (direction == 1 ? anchor.StartTime : split.StartTime);
                                anchor.EndTime = (direction == 1 ? split.EndTime : anchor.EndTime);
                                tillAllocations.Remove(split);

                                allocationIndex--; // Recheck the anchor because it's boundaries have changed so there may be yet another split
                                changed = true;
                            }
                        }
                    }
                }
            }
            while (changed); // ToDo: Is this loop necessary, inefficient

            return tillAllocations;
        }

        /// <summary>
        /// Checks whether the employee is within the maximum hours threshold for a site
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="till"></param>
        /// <param name="maxHours"></param>
        /// <returns></returns>
        private bool CanEmployeeBeAllocated(JdaEmployee employee, Till till, GlobalSettings settings, DateTime date)
        {
            if (settings != null && till != null && (till.Type.Key.Equals("Basket") && settings.MaximumHours.HasValue))
            {
                return employee.HoursFilled[date].Hours < settings.MaximumHours.Value;
            }
            else
            {
                // if the till is not basket allocate as normal, so return true
                // checkout operator, proceed as normal
                return true;
            }
        }

        /// <summary>
        /// Checks whether based on the maximum hours for a basket till, an allocation can be added
        /// </summary>
        /// <param name="allocation"></param>
        /// <param name="employee"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private void IsTillAllocationValid(TillAllocation allocation, JdaEmployee employee, GlobalSettings settings, DateTime date)
        {
            // when we have assigned an end time, if this is a basket allocation, we must increment the employees allocated basket time
            // capping only applies to basket tills
            if (settings != null && allocation.Till != null && (settings.MaximumHours.HasValue && allocation.Till.Type.Key.Equals("Basket")))
            {
                // calculate the diff
                TimeSpan diff = allocation.EndTime - allocation.StartTime;

                // check that if by adding this to the employees current hours, we are not going over it for the site
                TimeSpan newTotal = employee.HoursFilled[date].Add(diff);
                if (newTotal.Hours >= settings.MaximumHours.Value)
                {
                    // truncate?
                   // int truncateAmount = newTotal - settings.MaximumHours.Value;
                    TimeSpan truncateAmount = new TimeSpan((newTotal.Hours - settings.MaximumHours.Value), newTotal.Minutes, newTotal.Seconds);
                    allocation.EndTime = allocation.EndTime.Subtract(truncateAmount);
                    // recalculate diff to add onto hours filled
                    diff = allocation.EndTime - allocation.StartTime;
                    employee.HoursFilled[date] = employee.HoursFilled[date].Add(diff);
                }
                else
                {
                    // if not then fine to proceed and add onto the employees time
                    employee.HoursFilled[date] = employee.HoursFilled[date].Add(diff);
                }
            }
        }

        private class ReplacementTillAllocations
        {
            private Till _till;
            public IList<TillAllocation> Replacements { get; set; }

            public ReplacementTillAllocations(IList<TillAllocation> tillAllocations, TillAllocation anchor, TillAllocation split, Till till, int direction)
            {
                _till = till;
                var currentTillId = (till == null ? (int?)null : till.TillId);
                var splitTillId = (split.Till == null ? (int?)null : split.Till.TillId);

                tillAllocations = tillAllocations.OrderBy(a => a.StartTime).ToList();
                var currentAllocations = tillAllocations.Where(a => a.TillId == currentTillId).ToList();
                var splitAllocations = tillAllocations.Where(a => a.TillId == splitTillId).ToList();

                // If the anchor and split are on the same till, use the split as the replacement so that the anchor and split merged
                if (anchor.TillId == splitTillId)
                {
                    Replacements = new List<TillAllocation>() { split };
                    return;
                }

                // No point in replacing the split with itself
                if (currentTillId == splitTillId)
                {
                    Replacements = null;
                    return;
                }

                // Include whitespace in the split
                DateTime minSplitStart, maxSplitEnd;
                if (direction == 1)
                {
                    var nextAllocation = splitAllocations.Where(d => d.StartTime >= split.EndTime).OrderBy(d => d.StartTime).FirstOrDefault();
                    minSplitStart = split.StartTime;
                    maxSplitEnd = (nextAllocation == null ? DateTime.MaxValue : nextAllocation.StartTime);
                }
                else
                {
                    var previousAllocation = splitAllocations.Where(d => d.EndTime <= split.StartTime).OrderBy(d => d.EndTime).LastOrDefault();
                    minSplitStart = (previousAllocation == null ? DateTime.MinValue : previousAllocation.EndTime);
                    maxSplitEnd = split.EndTime;
                }

                // Does the anchor till have any allocations that clash with the split
                var useAnchorTill = tillAllocations.Any(a => a.TillId == anchor.TillId.Value && a.StartTime < maxSplitEnd && a.EndTime > minSplitStart); // Assumption that anchor till is not null (ie. not checkout support)

                // Use replacements from the anchor till if they exist
                if (useAnchorTill && currentTillId != anchor.TillId.Value)
                {
                    Replacements = null;
                    return;
                }

                // Do not move allocations into checkout support unless have to
                if (splitTillId == null && !useAnchorTill)
                {
                    Replacements = null;
                    return;
                }

                // The replacements must not overlap all the whitespace at the start of the split
                if (currentAllocations.Any(a => a.EndTime > split.StartTime && a.StartTime < minSplitStart))
                {
                    Replacements = null;
                    return;
                }

                // The replacements must not overlap all the whitespace at the end of the split
                if (currentAllocations.Any(a => a.StartTime < split.EndTime && a.EndTime > maxSplitEnd))
                {
                    Replacements = null;
                    return;
                }

                // Get the minimum number of replacement allocations for the split
                var _replacements = currentAllocations.Where(a => a.StartTime >= minSplitStart && a.EndTime <= maxSplitEnd && a.StartTime < split.EndTime && a.EndTime > split.StartTime).ToList();

                List<TillAllocation> toRemove = new List<TillAllocation>();
                if (split.Till != null && split.Till.Windows != null)
                {
                    foreach (var replacement in _replacements)
                    {
                        bool fitAny = split.Till.Windows.Any(window => window.StartTime <= replacement.StartTime && window.EndTime >= replacement.EndTime);

                        if (!fitAny)
                        {
                            toRemove.Add(replacement);
                        }
                    }
                }

                int removed = _replacements.RemoveAll(replacement => toRemove.Contains(replacement));

                // Check if the replacements are continuous or have gaps
                var _replacementsHaveWhitespace = false;
                for (var t = 0; t < _replacements.Count - 1; t++)
                {
                    if (_replacements[t].EndTime != _replacements[t + 1].StartTime)
                    {
                        _replacementsHaveWhitespace = true;
                        break;
                    }
                }

                if (!_replacements.Any() || _replacements.Min(r => r.StartTime) > split.StartTime || _replacements.Max(r => r.EndTime) < split.EndTime)
                {
                    _replacementsHaveWhitespace = true;
                }

                // Basket till windows must be completely filled
                if (split.TillTypeKey == TillTypeKey.Basket && _replacementsHaveWhitespace)
                {
                    Replacements = null;
                    return;
                }

                // Replacements must fall within basket windows
                var splitFitsInWindow = anchor.Till.Windows._NewIfNull().Any(w => split.StartTime >= w.StartTime && split.EndTime <= w.EndTime);
                if (anchor.TillTypeKey == TillTypeKey.Basket && !splitFitsInWindow)
                {
                    Replacements = null;
                    return;
                }

                // Other rules to consider:
                // What if the till is null (ie. checkout support)
                if (split.CheckoutSupport)
                {
                    if (_replacements != null && _replacements.Count() > 0)
                    {
                        TimeSpan splitTimePeriod = split.EndTime - split.StartTime;
                        var replacementsAreNotExactTimeMatches = _replacements.Where(x => (x.EndTime - x.StartTime) > splitTimePeriod);
                        if (replacementsAreNotExactTimeMatches.Count() > 0)
                        {
                            Replacements = null;
                            return;
                        }
                    }
                }

                // If the replacement is a basket and replacement is not a straight swap between the anchor and split, we would end up with an empty basket
                // If the replacement is a basket we must ensure the split does not have whitespace

                // The replacements are valid
                Replacements = _replacements;
            }

            public int Priority
            {
                get
                {
                    return (_till == null ? int.MaxValue : _till.Priority);
                }
            }
        }

        public AllocationMoveResponse Post(AllocationMoveRequest request)
        {
            AllocationMoveResponse response = new AllocationMoveResponse();

            List<TillAllocation> changes = new List<TillAllocation>();

            foreach (var aloc in request.Changes)
            {
                changes.Add(new TillAllocation(aloc));
            }

            TillAllocationRepository.Move(request.SiteId, changes);

            response.Success = true;

            return response;
        }


        public BasketSetMaxHoursResponse Post(BasketSetMaxHoursRequest request)
        {
            m_globalSettingsRepository.UpdateMaximumHours(request.MaximumHours, request.SiteId);

            return new BasketSetMaxHoursResponse() { Success = true };
        }


        public BasketGetMaxHoursResponse Get(BasketGetMaxHoursRequest request)
        {
            var globalSiteSettings = m_globalSettingsRepository.Get(request.SiteId);

            return new BasketGetMaxHoursResponse()
            {
                 MaxHours = ( globalSiteSettings != null) ? globalSiteSettings.MaximumHours : null 
            };
        }
    }
}
