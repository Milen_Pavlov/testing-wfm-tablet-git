﻿using Consortium.Service.Engine;
using Consortium.Model.Engine.Paging;
using Repl.Tesco.Data.Interfaces;
using WFM.Shared.DataTransferObjects.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared;
using Repl.Tesco.Plugin.Rest.Interfaces;

namespace Repl.Tesco.Plugin.Rest.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class StaticDataService : ConsortiumService, IStaticDataService
    {
        public ITillStatusRepository TillStatusRepository { get; set; }
        public ITillTypeRepository TillTypeRepository { get; set; }

        public StaticDataService(ITillStatusRepository tillStatusRepository, ITillTypeRepository tillTypeRepository)
        {
            TillStatusRepository = tillStatusRepository;
            TillTypeRepository = tillTypeRepository;
        }

        public ReadResponse Get(ReadRequest request)
        {
            var messages = new MessageList();
            var response = new ReadResponse()
            {
                TillStatuses = TillStatusRepository.
                    ReadAll(Pager.TakeAll).
                    Select(s => new WFM.Shared.DataTransferObjects.StaticData.ReadResponse.TillStatus()
                    {
                        TillStatusId = s.TillStatusId,
                        Key = s.Key,
                        Name = s.Name,
                        DisplayOrder = s.DisplayOrder,
                    }).
                    ToArray(),
                TillTypes = TillTypeRepository.
                    ReadAll(Pager.TakeAll).
                    Select(t => new WFM.Shared.DataTransferObjects.StaticData.ReadResponse.TillType()
                    {
                        TillTypeId = t.TillTypeId,
                        Key = t.Key,
                        Name = t.Name,
                        DisplayOrder = t.DisplayOrder,
                        OpeningSequence = t.OpeningSequence,
                    }).
                    ToArray(),
            };

            messages.AddSuccess("The static data was read").CopyTo(response);
            return response;
        }
    }
}