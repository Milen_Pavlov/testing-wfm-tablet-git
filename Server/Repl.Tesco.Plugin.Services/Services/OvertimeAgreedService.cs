﻿using AutoMapper;
using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using Repl.Tesco.Plugin.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WFM.Shared.DataTransferObjects.OvertimeAgreed;

namespace Repl.Tesco.Plugin.Services.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class OvertimeAgreedService : ConsortiumService, IOvertimeAgreedService
    {
        private readonly ISiteRepository m_siteRepository;
        private readonly IOvertimeAgreedRepository m_overtimeAgreedRepository;

        public OvertimeAgreedService(ISiteRepository siteRepo, IOvertimeAgreedRepository overtimeAgreedRepo)
        {
            m_siteRepository = siteRepo;
            m_overtimeAgreedRepository = overtimeAgreedRepo;

            Mapper.CreateMap<OvertimeAgreed, OvertimeAgreedRecord>();
        }

        public ReadOvertimeAgreedResponse Get(ReadOvertimeAgreedRequest req)
        {
            if (req.Week == null)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a week");
            }

            Site site = m_siteRepository.GetById(req.SiteId);

            if(site == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, "The requested site could not be found");
            }

            // get the results for the week, then filter down by the site id passed
            var results = m_overtimeAgreedRepository.GetOvertimeAgreedForWeek(req.Week);
            var filteredBySite = results.Where(p => p.SiteId == site.Id).ToList();

            var mapped = Mapper.Map<List<OvertimeAgreed>, List<OvertimeAgreedRecord>>(filteredBySite); 

            return new ReadOvertimeAgreedResponse() { OvertimeAgreed = mapped };
        }
    }
}
