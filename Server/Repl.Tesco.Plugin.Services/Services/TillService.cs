﻿using Consortium.Model.Engine;
using Consortium.Service.Engine;
using Consortium.Model.Engine.Paging;
using Repl.Tesco.Extensions;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Interfaces.Repositories;
using Repl.Tesco.Models.Extensions;
using WFM.Shared.DataTransferObjects;
using WFM.Shared.DataTransferObjects.Shared;
using WFM.Shared.DataTransferObjects.Till;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repl.Tesco.Models;
using Repl.Tesco.Constants;
using WFM.Shared.Extensions;
using WFM.Shared;
using Repl.Tesco.Plugin.Rest.Interfaces;

namespace Repl.Tesco.Plugin.Rest.Services
{
    [ConsortiumAuth(DefaultRoles.Admin)]
    public class TillService : ConsortiumService, ITillService
    {
        public ITillRepository TillRepository { get; set; }
        public ITillTypeRepository TillTypeRepository { get; set; }

        public TillService(ITillRepository tillRepository, ITillTypeRepository tillTypeRepository)
        {
            TillRepository = tillRepository;
            TillTypeRepository = tillTypeRepository;
        }

        public ReadByIdResponse Get(ReadByIdRequest request)
        {
            var response = new ReadByIdResponse();
            var messages = new MessageList();

            var till = TillRepository.ReadById(request.TillId);
            if (till == null)
            {
                messages.AddError("The till was not found").CopyTo(response);
                return response;
            }

            response.TillId = till.TillId;
            response.TillNumber = till.TillNumber;
            response.OpeningSequence = till.OpeningSequence;
            response.SiteId = till.SiteId;
            response.StatusId = till.StatusId;
            response.TypeId = till.TypeId;
            
            response.Windows = till.
                Windows.
                Select(w => new WFM.Shared.DataTransferObjects.Till.ReadByIdResponse.TillWindow()
                {
                    TillWindowId = w.TillWindowId,
                    StartTime = w.StartTime,
                    EndTime = w.EndTime,
                }).
                ToArray();

            messages.AddSuccess("The till was read").CopyTo(response);
            return response;
        }

        public ReadBySiteResponse Get(ReadBySiteRequest request)
        {
            var messages = new MessageList();
            var pager = request._CreatePager();
            var tills = TillRepository.ReadBySite(request.SiteId, pager);

            var response = new ReadBySiteResponse()
            {
                NumOfItemsOverAllPages = pager.NumOfItems,
                Tills = tills.
                    Select(t => new WFM.Shared.DataTransferObjects.Till.ReadBySiteResponse.Till()
                    {
                        TillId = t.TillId,
                        TillNumber = t.TillNumber,
                        OpeningSequence = t.OpeningSequence,
                        SiteId = t.SiteId,
                        StatusId = t.StatusId,
                        TypeId = t.TypeId,
                        Windows = t.
                            Windows.
                            _NewIfNull().
                            Select(w => new WFM.Shared.DataTransferObjects.Till.ReadBySiteResponse.TillWindow()
                            {
                                TillWindowId = w.TillWindowId,
                                StartTime = w.StartTime,
                                EndTime = w.EndTime,
                            }).
                            ToArray(),
                    }).
                    ToArray(),
            };

            messages.AddSuccess("The tills were read").CopyTo(response);
            return response;
        }

        public DeleteManyResponse Delete(DeleteManyRequest request)
        {
            foreach (var tillId in request.TillIds)
            {
                TillRepository.Delete(tillId);
            }
            
            var response = new DeleteManyResponse();
            var messages = new MessageList();
            messages.AddSuccess("The tills were deleted").CopyTo(response);
            return response;
        }

        public MoveResponse Get(MoveRequest request)
        {
            var response = new MoveResponse();
            var messages = new MessageList();

            if (Math.Abs(request.Direction) != 1)
            {
                messages.AddError("The till was not moved", "The parameter 'direction' must be +/- 1").CopyTo(response);
                return response;
            }

            var till = TillRepository.ReadById(request.TillId);
            if (till == null)
            {
                messages.AddError("The till was not found").CopyTo(response);
                return response;
            }

            var tills = TillRepository.ReadBySite(till.SiteId, Pager.TakeAll)._RefreshOpeningSequence();
            till = tills.Single(t => t.TillId == request.TillId); // Refresh the till because we have refreshed the opening sequence
            var newOpeningSequence = till.OpeningSequence + request.Direction;
            var lastOpeningSequence = tills.Last().OpeningSequence;

            if (newOpeningSequence < 0)
            {
                messages.AddWarning("The till is already at the top of the list").CopyTo(response);
                return response;
            }
            else if (newOpeningSequence > lastOpeningSequence)
            {
                messages.AddWarning("The till is already at the bottom of the list").CopyTo(response);
                return response;
            }

            var neighbouringTill = tills.Single(t => t.OpeningSequence == newOpeningSequence);
            neighbouringTill.OpeningSequence = till.OpeningSequence;
            till.OpeningSequence = newOpeningSequence;
            TillRepository.UpdateOpeningSequences(tills);

            messages.AddSuccess("The till was moved").CopyTo(response);
            return response;
        }

        public SetOpeningSequenceResponse Get(SetOpeningSequenceRequest request)
        {
            var response = new SetOpeningSequenceResponse();
            var messages = new MessageList();

            if (request.OpeningSequence < 0)
            {
                messages.AddError("The opening sequence must be greater than (or equal to) 0").CopyTo(response);
                return response;
            }

            var targetTill = TillRepository.ReadById(request.TillId);
            if (targetTill == null)
            {
                messages.AddError("The till was not found").CopyTo(response);
                return response;
            }

            var tills = TillRepository.ReadBySite(targetTill.SiteId, Pager.TakeAll);
            var lastOpeningSequence = (tills.Count - 1);
            if (request.OpeningSequence > lastOpeningSequence)
            {
                messages.AddError(string.Format("The opening sequence must be less than (or equal to) {0}", lastOpeningSequence)).CopyTo(response);
                return response;
            }

            tills.Single(t => t.TillId == request.TillId).OpeningSequence = request.OpeningSequence;
            var siblingTills = tills.Where(t => t.TillId != request.TillId).OrderBy(t => t.OpeningSequence).ToList();
            for (var t = 0; t < siblingTills.Count; t++)
            {
                siblingTills[t].OpeningSequence = t + (t >= request.OpeningSequence ? 1 : 0);
            }

            TillRepository.UpdateOpeningSequences(tills);
            messages.AddSuccess("The till's opening sequence was changed").CopyTo(response);
            return response;
        }

        public UpdateOpeningSequencesResponse Post(UpdateOpeningSequencesRequest request)
        {
            var response = new UpdateOpeningSequencesResponse();
            MessageList messages;
            UpdateOpeningSequences(request.TillIdsOrderedByOpeningSequence, request.SiteId, out messages);
            messages.CopyTo(response);
            return response;
        }

        public SaveManyResponse Post(SaveManyRequest request)
        {
            var response = new SaveManyResponse();
            var tills = new List<Till>();

            foreach (var tillRequest in request.Tills)
            {
                var till = new Till()
                {
                    TillId = tillRequest.TillId,
                    TillNumber = tillRequest.TillNumber,
                    StatusId = tillRequest.StatusId,
                    TypeId = tillRequest.TypeId,
                    OpeningSequence = tillRequest.OpeningSequence
                };

                tills.Add(till);

                if (tillRequest.Windows != null)
                {
                    foreach (var windowRequest in tillRequest.Windows)
                    {
                        var window = new TillWindow()
                        {
                            StartTime = windowRequest.StartTime,
                            EndTime = windowRequest.EndTime,
                        };
                        till.Add(window);
                    }
                }
            }

            MessageList messages;
            Save(tills, request.SiteId, out messages);
            messages.CopyTo(response);
            return response;
        }

        public bool UpdateOpeningSequences(IList<int> tillIdsOrderedByOpeningSequence, int siteId, out MessageList messages)
        {
            messages = new MessageList();

            // Read tills
            var tills = TillRepository.ReadBySite(siteId, Pager.TakeAll);

            // Validate parameters
            if (tills.Count != tillIdsOrderedByOpeningSequence.Count)
            {
                messages.AddError("The number of till IDs does not match the number of tills for the given site");
                return false;
            }

            foreach (var till in tills)
            {
                var tillId = tillIdsOrderedByOpeningSequence.SingleOrDefault(t => t == till.TillId);
                if (tillId == 0)
                {
                    messages.AddError("The given till IDs do not match those for the given site");
                    return false;
                }
            }

            // Set opening sequence
            for (var openingSequence = 0; openingSequence < tillIdsOrderedByOpeningSequence.Count; openingSequence++)
            {
                var tillId = tillIdsOrderedByOpeningSequence[openingSequence];
                tills.Single(t => t.TillId == tillId).OpeningSequence = openingSequence;
            }

            // Save tills
            TillRepository.UpdateOpeningSequences(tills);

            // Done
            messages.AddSuccess("The tills' opening sequences were updated");
            return true;
        }

        public bool Save(IList<Till> tills, int siteId, out MessageList messages)
        {
            messages = new MessageList();
            foreach (var till in tills)
            {
                MessageList saveMessages;
                Save(till, siteId, out saveMessages);
                messages.Add(saveMessages);
            }

            return messages.Success;
        }

        public bool Save(Till till, int siteId, out MessageList messages)
        {
            messages = new MessageList();
            var isNew = (till.TillId <= 0);
            var action = (isNew ? "created" : "updated");

            try
            {
                // Get data
                var dbTill = (isNew ? new Till() : TillRepository.ReadById(till.TillId));
                var tillsBySite = (isNew ? TillRepository.ReadBySite(siteId, Pager.TakeAll) : null);
                var tillType = TillTypeRepository.ReadById(till.TypeId);

                // Validate request
                if (dbTill == null)
                {
                    messages.AddError("The till was not found");
                    return false;
                }
                else if (tillType == null)
                {
                    messages.AddError("The till-type was not found");
                    return false;
                }

                // Create model
                dbTill.TillNumber = till.TillNumber;
                dbTill.SiteId = siteId;
                dbTill.StatusId = till.StatusId;
                dbTill.TypeId = till.TypeId;
                dbTill.OpeningSequence = till.OpeningSequence;

                switch (tillType.Key)
                {
                    case TillTypeKey.MainBank:
                    {
                        dbTill.Windows = new List<TillWindow>();
                        break;
                    }
                    case  TillTypeKey.Basket:
                    {
                        dbTill.Windows = till.
                            Windows.
                            _NewIfNull().
                            Select(w => new TillWindow()
                            {
                                StartTime = w.StartTime,
                                EndTime = w.EndTime
                            }).
                            ToList();
                        break;
                    }
                    default:
                    {
                        throw new NotSupportedException();
                    }
                }

                if (isNew)
                {
                    dbTill.OpeningSequence = (tillsBySite.Any() ? tillsBySite.Max(t => t.OpeningSequence) + 1 : 0);
                }

                // Validate model
                //if (tillType.Key == TillTypeKey.MainBank && dbTill.Windows._NewIfNull().Any())
                //{
                //    messages._Error("Main-bank tills must not have till-windows");
                //    return false;
                //}
                if (tillType.Key == TillTypeKey.Basket && (dbTill.Windows._NewIfNull().Count < 1 || dbTill.Windows._NewIfNull().Count > 3))
                {
                    messages.AddError("Basket tills must have from 1 to 3 till-windows");
                    return false;
                }
                else if (tillType.Key == TillTypeKey.Basket && !dbTill.Windows._NewIfNull()._EachStartTimeLessThanEndTime())
                {
                    messages.AddError("Each till window's start time must be earlier than its end time");
                    return false;
                }
                else if (tillType.Key == TillTypeKey.Basket && dbTill.Windows._NewIfNull()._AnyOverlaps())
                {
                    messages.AddError("The till windows must not overlap");
                    return false;
                }

                // Save data
                //if (isNew)
                //{
                //    TillRepository.UpdateOpeningSequences(tillsBySite);
                //}
                TillRepository.SaveTillAndWindows(dbTill);

                // Commit
                messages.AddSuccess("The till was " + action);
            }
            catch (Exception exception)
            {
                messages.AddError("The till was not " + action, exception._RecursiveMessage());
            }

            return messages.Success;
        }
    }
}
