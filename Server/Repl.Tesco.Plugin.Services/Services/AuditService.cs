﻿using AutoMapper;
using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using Repl.Tesco.Plugin.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WFM.Shared.DataTransferObjects.Audits;

namespace Repl.Tesco.Plugin.Services.Services
{
    public class AuditService : ConsortiumService, IAuditService
    {
        private readonly IAuditRepository m_auditRepository;

        public AuditService(IAuditRepository auditRepo)
        {
            m_auditRepository = auditRepo;
            Mapper.CreateMap<Audit, AuditRecord>()
                .ForMember(m => m.DateOfChange, a => a.MapFrom(b => b.DateOfChange.DateTime));
        }

        public GetAuditsResponse Get(GetAuditsRequest req)
        {
            if (req.Date == null)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a date");
            }

            var items = m_auditRepository.Get(req.Date, req.Site);
            var mapped = Mapper.Map<List<Audit>, List<AuditRecord>>(items);

            return new GetAuditsResponse() { Audits = mapped };
        }
    }
}
