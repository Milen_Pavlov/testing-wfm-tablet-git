﻿using Repl.Tesco.Models;
using WFM.Shared.DataTransferObjects.TillAllocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Rest.Interfaces
{
    public interface ITillAllocationService
    {
        ReadResponse Post(ReadRequest request);
        CreateResponse Post(CreateRequest request);
        AllocationMoveResponse Post(AllocationMoveRequest request);
        BasketGetMaxHoursResponse Get(BasketGetMaxHoursRequest request);
        BasketSetMaxHoursResponse Post(BasketSetMaxHoursRequest request);
    }
}
