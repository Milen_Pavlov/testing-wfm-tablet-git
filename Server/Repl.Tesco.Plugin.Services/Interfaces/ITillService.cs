﻿using Repl.Tesco.Models;
using WFM.Shared.DataTransferObjects;
using WFM.Shared.DataTransferObjects.Till;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Shared;
using WFM.Shared;

namespace Repl.Tesco.Plugin.Rest.Interfaces
{
    public interface ITillService
    {
        ReadBySiteResponse Get(ReadBySiteRequest request);
        ReadByIdResponse Get(ReadByIdRequest request);
        DeleteManyResponse Delete(DeleteManyRequest request);
        MoveResponse Get(MoveRequest request);
        SetOpeningSequenceResponse Get(SetOpeningSequenceRequest request);
        UpdateOpeningSequencesResponse Post(UpdateOpeningSequencesRequest request);
        SaveManyResponse Post(SaveManyRequest request);

        bool UpdateOpeningSequences(IList<int> tillIdsOrderedByOpeningSequence, int siteId, out MessageList messages);
        bool Save(IList<Till> tills, int siteId, out MessageList messages);
        bool Save(Till till, int siteId, out MessageList messages);
    }
}
