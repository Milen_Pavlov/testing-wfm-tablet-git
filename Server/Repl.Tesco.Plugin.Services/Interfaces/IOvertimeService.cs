﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace Repl.Tesco.Plugin.Rest.Interfaces
{
    public interface IOvertimeService
    {
        ReadOvertimeResponse Post(ReadOvertimeRequest request);

        SetOvertimeResponseLegacy Post(SetOvertimeRequestLegacy request);
        SetOvertimeResponse Post(SetOvertimeRequest request);
    }
}
