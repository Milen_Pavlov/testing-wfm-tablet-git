﻿using WFM.Shared.DataTransferObjects.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Rest.Interfaces
{
    public interface IUserService
    {
        LogInResponse Post(LogInRequest request);
    }
}
