﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Audits;

namespace Repl.Tesco.Plugin.Services.Interfaces
{
    public interface IAuditService
    {
        GetAuditsResponse Get(GetAuditsRequest req);
    }
}
