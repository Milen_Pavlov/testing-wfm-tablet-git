﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.OvertimeAgreed;

namespace Repl.Tesco.Plugin.Services.Interfaces
{
    public interface IOvertimeAgreedService
    {
        ReadOvertimeAgreedResponse Get(ReadOvertimeAgreedRequest req);
    }
}
