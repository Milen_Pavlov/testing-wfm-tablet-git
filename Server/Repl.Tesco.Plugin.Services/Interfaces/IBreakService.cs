﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Breaks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace Repl.Tesco.Plugin.Rest.Interfaces
{
    public interface IBreakService
    {
        ReadBreaksResponse Post(ReadBreaksRequest request);

        SetBreaksResponse Post(SetBreaksRequest request);
    }
}