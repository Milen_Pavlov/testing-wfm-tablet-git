﻿using WFM.Shared.DataTransferObjects.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Rest.Interfaces
{
    public interface IStaticDataService
    {
        ReadResponse Get(ReadRequest request);
    }
}
