﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Resources;

namespace Repl.Tesco.Plugin.Services.Interfaces
{
    public interface IResourceService
    {
        GetResourceDataResponse Get(GetResourceDataRequest req);
        SetResourceDataResponse Post(SetResourceDataRequest req);
        UnsetResourceDataResponse Post(UnsetResourceDataRequest req);
    }
}
