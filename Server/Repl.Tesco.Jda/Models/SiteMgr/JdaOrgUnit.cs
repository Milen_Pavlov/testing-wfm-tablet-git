﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Jda.Models.SiteMgr
{
    public class JdaOrgUnit
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool isDefault { get; set; }
    }
}
