﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Jda.Models.Common
{
    public class Response<T>
    {
        public string message { get; set; }
        public int count { get; set; }
        public string appStatus { get; set; }
        public int status { get; set; }
        public string type { get; set; }
        public T data { get; set; }

        public string StatusText
        {
            get
            {
                switch (status)
                {
                    case 0:
                        return "Unknown Error";
                    case 1200:
                        return "Invalid username or password";
                    case 1201:
                        return "Password has expired";
                    case 1205:
                        return "Login count exceeded, please contact an administrator";
                    default:
                        return string.Format("{0} ({1})", message, status);
                }
            }
        }
    }
}
