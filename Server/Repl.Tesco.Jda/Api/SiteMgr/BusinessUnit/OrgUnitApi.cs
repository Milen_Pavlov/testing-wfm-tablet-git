﻿using Repl.Tesco.Jda.Models.Common;
using Repl.Tesco.Jda.Models.SiteMgr;
using Siesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Jda.Api.SiteMgr.BusinessUnit
{
    [RestRoute("/data/sitemgr/services/BusinessUnit.asmx/GetOrgUnitsForCurrentUser", "GET")]
    public class OrgUnitsRequest
    {
    }

    public class OrgUnitsReply : Response<List<JdaOrgUnit>>
    {
    }
}