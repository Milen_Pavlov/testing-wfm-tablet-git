﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Siesta;
using Repl.Tesco.Jda.Models.Common;

namespace ClockManager.JDA.Api.Common
{
    [RestHeaderField("Accept", "application/json")]
    [RestHeaderField("Content-Type", "application/x-www-form-urlencoded")]
    [RestRoute("/data/login", "POST")]
    public class SessionLoginRequest
    {
        public string loginName { get; set; }
        public string password { get; set; }
    }

    public class SessionLoginReply : Response<Login>
    {
    }
}