﻿using System;
using System.Linq;
using Repl.Tesco.Interfaces.Repositories.Jda;
using Repl.Tesco.Interfaces.Services;
using WFM.Shared.DataTransferObjects.TillAllocation;
using Moq;
using NUnit.Framework;
using Repl.Tesco.Models.Jda;
using System.Collections.Generic;
using Repl.Tesco.Interfaces.Repositories;
using Consortium.Model.Engine.Paging;
using Repl.Tesco.Models;
using Repl.Tesco.Data;
using Repl.Tesco.Constants;

namespace Repl.Tesco.Plugin.Services.Test.TillAllocationService
{
    [TestFixture]
    public class Create
    {
        public Mock<IJdaSessionRepository> MockJdaSessionRepository { get; set; }
        public Mock<IJdaSiteRepository> MockJdaSiteRepository { get; set; }
        public Mock<IJdaScheduleRepository> MockJdaScheduleRepository { get; set; }
        public Mock<ITillRepository> MockTillRepository { get; set; }
        public Mock<ITillAllocationRepository> MockTillAllocationRepository { get; set; }
        public ITillStatusRepository TillStatusRepository { get; set; }
        public ITillTypeRepository TillTypeRepository { get; set; }
        public ITillAllocationService TillAllocationService { get; set; }
        private int _tillId, _tillOpeningSequence, _tillWindowId, _employeeId, _shiftId, _jobId, _tillAllocationIndex;
        private JdaSchedule _schedule;
        private IList<Till> _tills;
        private IList<TillType> _tillTypes;
        private IList<TillStatus> _tillStatuses;
        private IList<TillAllocation> _tillAllocations;

        public void Initialise()
        {
            MockJdaSessionRepository = new Mock<IJdaSessionRepository>();
            MockJdaSiteRepository = new Mock<IJdaSiteRepository>();
            MockJdaScheduleRepository = new Mock<IJdaScheduleRepository>();
            MockTillRepository = new Mock<ITillRepository>();
            MockTillAllocationRepository = new Mock<ITillAllocationRepository>();

            MockJdaSessionRepository.Setup(r => r.Authenticated(It.IsAny<string>())).Returns(true);
            MockJdaSiteRepository.Setup(r => r.ReadById(It.IsAny<string>(), It.IsAny<int>())).Returns(new JdaSite());
            MockJdaScheduleRepository.Setup(r => r.Read(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new JdaSchedule() { Employees = new List<JdaEmployee>() });
            MockTillRepository.Setup(r => r.ReadBySite(It.IsAny<int>(), It.IsAny<Pager>())).Returns(new List<Till>());
            MockTillAllocationRepository.Setup(r => r.Delete(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()));
            MockTillAllocationRepository.Setup(r => r.Create(It.IsAny<IList<TillAllocation>>()));

            TillStatusRepository = new TillStatusRepository();
            TillTypeRepository = new TillTypeRepository();
            TillAllocationService = new Services.TillAllocationService(MockTillAllocationRepository.Object, MockTillRepository.Object, MockJdaSessionRepository.Object, MockJdaSiteRepository.Object, MockJdaScheduleRepository.Object);
        }

        [Test]
        public void TestFromTimeComponentMustBeZeroPOST()
        {
            Initialise();
            var request = new CreateRequest()
            {
                From =  DateTime.Parse("2014-10-20 00:00:01") 
            };
            var response = TillAllocationService.Post(request);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TestFromTimeComponentMustBeZeroGET()
        {
            Initialise();
            var readRequest = new ReadRequest()
            {
                From =  DateTime.Parse("2014-10-20 00:00:01")
            };
            var readResponse = TillAllocationService.Post(readRequest);
            Assert.IsFalse(readResponse.Success);
        }

        [Test]
        public void TestToTimeComponentMustBeZeroPOST()
        {
            Initialise();
            var request = new CreateRequest()
            {
                To = DateTime.Parse("2014-10-20 00:00:01")
            };
            var response = TillAllocationService.Post(request);

            Assert.IsFalse(response.Success);

           
        }

        [Test]
        public void TestToTimeComponentMustBeZeroGET()
        {
            Initialise();
            var readRequest = new ReadRequest()
            {
                To = DateTime.Parse("2014-10-20 00:00:01")
            };
            var readResponse = TillAllocationService.Post(readRequest);
            Assert.IsFalse(readResponse.Success);
        }

        [Test]
        public void TestFromMustBeLessThanToPOST()
        {
            Initialise();
            var request = new CreateRequest()
            {
                From = DateTime.Parse("2014-10-21"),
                To = DateTime.Parse("2014-10-20")
            };
            var response = TillAllocationService.Post(request);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TestFromMustBeLessThanToGET()
        {
            Initialise();
            var readRequest = new ReadRequest()
            {
                From = DateTime.Parse("2014-10-21"),
                To = DateTime.Parse("2014-10-20")
            };
            var readResponse = TillAllocationService.Post(readRequest);
            Assert.IsFalse(readResponse.Success);
        }

        [Test]
        public void TestFromMustNotBeEqualToToPOST()
        {
            Initialise();
            var request = new CreateRequest()
            {
                From = DateTime.Parse("2014-10-20"),
                To = DateTime.Parse("2014-10-20")
            };
            var response = TillAllocationService.Post(request);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TestFromMustNotBeEqualToToGET()
        {
            Initialise();
            var readRequest = new ReadRequest()
            {
                From = DateTime.Parse("2014-10-20"),
                To = DateTime.Parse("2014-10-20")
            };
            var readResponse = TillAllocationService.Post(readRequest);
            Assert.IsFalse(readResponse.Success);
        }

        [Test]
        public void TestFromCanBeLessThanTo()
        {
            Initialise();
            var request = new CreateRequest()
            {
                From = DateTime.Parse("2014-10-01"),
                To = DateTime.Parse("2014-10-20")
            };
            var response = TillAllocationService.Post(request);

            Assert.IsTrue(response.Success);
        }

        private void AddEmployee(string fullName)
        {
            _schedule.Add(new JdaEmployee()
            {
                EmployeeId = _employeeId++,
                FullName = fullName,
            });
        }

        private void AddShift(int employeeId)
        {
            _employeeId = employeeId;
            _schedule.Employees.Single(e => e.EmployeeId == _employeeId).Add(new JdaShift()
            {
                ShiftId = _shiftId++
            });
        }

        private void AddJob(string startTime, string endTime)
        {
            _schedule.Employees.Single(e => e.EmployeeId == _employeeId).Shifts.Last().Add(new JdaJob()
            {
                JobId = _jobId++,
                JobName = "checkout",
                Start = DateTime.Parse(startTime),
                End = DateTime.Parse(endTime),
            });
        }

        private void AddTill(string tillNumber, bool mainBank, bool open)
        {
            _tills.Add(new Till()
            {
                TillId = _tillId++,
                TillNumber = tillNumber,
                OpeningSequence = _tillOpeningSequence++,
                SiteId = 1000652
            });

            if (mainBank)
            {
                _tillTypes.Single(s => s.Key == TillTypeKey.MainBank).Add(_tills.Last());
            }
            else
            {
                _tillTypes.Single(s => s.Key == TillTypeKey.Basket).Add(_tills.Last());
            }

            if (open)
            {
                _tillStatuses.Single(s => s.Key == TillStatusKey.Open).Add(_tills.Last());
            }
            else
            {
                _tillStatuses.Single(s => s.Key == TillStatusKey.Unused).Add(_tills.Last());
            }
        }

        private void AddTillWindow(string startTime, string endTime)
        {
            _tills.Last().Add(new TillWindow()
            {
                TillWindowId = _tillWindowId++,
                StartTime = DateTime.Parse(startTime),
                EndTime = DateTime.Parse(endTime),
            });
        }

        private void AssertTillAllocation(int tillId, int employeeId, string startTime, string endTime, bool checkoutSupport)
        {
            var tillAllocation = _tillAllocations[_tillAllocationIndex++];

            Assert.AreEqual(tillId, tillAllocation.TillId);
            Assert.AreEqual(tillId, tillAllocation.StampedTillId);
            Assert.AreEqual(employeeId, tillAllocation.EmployeeId);
            Assert.AreEqual(DateTime.Parse(startTime), tillAllocation.StartTime);
            Assert.AreEqual(DateTime.Parse(endTime), tillAllocation.EndTime);
            Assert.AreEqual(checkoutSupport, tillAllocation.CheckoutSupport);
        }

        private void InitialiseMainTest()
        {
            Initialise();

            // Create schedule
            _employeeId = 1;
            _shiftId = 1;
            _jobId = 1;
            _schedule = new JdaSchedule();

            AddEmployee("Brogan");
            AddEmployee("Edwards");
            AddEmployee("Spencer");
            AddEmployee("Young");
            AddEmployee("Brown");
            AddEmployee("Jobson");
            AddEmployee("Wilson");
            AddEmployee("Warwick");
            AddEmployee("Williams");
            AddEmployee("Nicholson");

            // Create tills
            _tillId = 1;
            _tillOpeningSequence = 0;
            _tills = new List<Till>();
            _tillTypes = TillTypeRepository.ReadAll(Pager.TakeAll);
            _tillStatuses = TillStatusRepository.ReadAll(Pager.TakeAll);

            AddTill("Basket 1", false, true);
            AddTillWindow("8:00", "20:00");

            AddTill("Basket 2", false, true);
            AddTillWindow("10:00", "12:00");
            AddTillWindow("16:00", "18:00");

            AddTill("Main Bank 1", true, true);
            AddTill("Main Bank 2", true, true);
            AddTill("Main Bank 3", true, true);

            // Setup mocks
            MockJdaScheduleRepository.Setup(r => r.Read(It.IsAny<string>(), It.IsAny<int>(), It.Is<DateTime>(i => i == DateTime.Parse("2014-11-09")))).Returns(_schedule);
            MockTillRepository.Setup(r => r.ReadBySite(It.IsAny<int>(), It.IsAny<Pager>())).Returns(_tills);

            _tillAllocations = null;
            MockTillAllocationRepository.Setup(r => r.Create(It.IsAny<IList<TillAllocation>>())).Callback<IList<TillAllocation>>(a => _tillAllocations = a);
        }

        private void InitialiseSecondaryTest()
        {
            InitialiseMainTest();
            // run allocations for GET response
            AddShift(1);
            AddJob("2014-11-14 09:00", "2014-11-14 15:15");
            AddShift(2);
            AddJob("2014-11-14 09:00", "2014-11-14 14:00");
            AddShift(3);
            AddJob("2014-11-14 16:00", "2014-11-14 22:00");
            AddShift(5);
            AddJob("2014-11-14 11:30", "2014-11-14 15:00");
            AddShift(6);
            AddJob("2014-11-14 11:30", "2014-11-14 14:30");
            AddShift(7);
            AddJob("2014-11-14 17:00", "2014-11-14 20:45");
            AddShift(8);
            AddJob("2014-11-14 15:15", "2014-11-14 16:00");
            AddShift(9);
            AddJob("2014-11-14 15:00", "2014-11-14 22:00");
            var request = new CreateRequest()
            {
                From = DateTime.Parse("2014-11-09"),
                To = DateTime.Parse("2014-11-16")
            };
            TillAllocationService.Post(request);
            // setup with current till allocations
            MockTillAllocationRepository.Setup(r => r.Read(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<Pager>())).Returns(_tillAllocations);
        }

        private void RunMainTest(int? expectedNumOfAllocations)
        {
            var request = new CreateRequest()
            {
                From = DateTime.Parse("2014-11-09"),
                To = DateTime.Parse("2014-11-16")
            };
            var response = TillAllocationService.Post(request);

            // Test
            _tillAllocationIndex = 0;
            _tillAllocations = _tillAllocations.OrderBy(a => a.TillId).ThenBy(a => a.StartTime).ToList();

            Assert.IsTrue(response.Success);
            if (expectedNumOfAllocations != null)
            {
                Assert.AreEqual(expectedNumOfAllocations.Value, _tillAllocations.Count);
            }
        }

        private void RunSecondaryTest()
        {
            ReadRequest read = new ReadRequest()
            { 
                  From = DateTime.Parse("2014-11-09"),
                  To = DateTime.Parse("2014-11-16")          
            };
            var response = TillAllocationService.Post(read);

            Assert.IsTrue(response.Success);

        }

        [Ignore] // TODO: Need to add more mocks
        [Test]
        public void TestTuesday()
        {
            InitialiseMainTest();

            AddShift(1);
            AddJob("2014-11-11 16:45", "2014-11-11 20:00");
            AddShift(2);
            AddJob("2014-11-11 09:00", "2014-11-11 15:45");
            AddShift(3);
            AddJob("2014-11-11 11:00", "2014-11-11 14:00");
            AddShift(6);
            AddJob("2014-11-11 14:30", "2014-11-11 22:00");
            AddShift(7);
            AddJob("2014-11-11 09:00", "2014-11-11 14:00");
            AddShift(8);
            AddJob("2014-11-11 16:30", "2014-11-11 16:45");
            AddShift(9);
            AddJob("2014-11-11 11:30", "2014-11-11 14:30");
            AddShift(10);
            AddJob("2014-11-11 15:30", "2014-11-11 22:00");

            RunMainTest(14);

            AssertTillAllocation(1, 2, "2014-11-11 09:00", "2014-11-11 15:45", false);
            AssertTillAllocation(1, 6, "2014-11-11 15:45", "2014-11-11 20:00", false);
            AssertTillAllocation(2, 7, "2014-11-11 10:00", "2014-11-11 12:00", false);
            AssertTillAllocation(2, 10, "2014-11-11 16:00", "2014-11-11 18:00", false);
            AssertTillAllocation(3, 7, "2014-11-11 09:00", "2014-11-11 10:00", false);
            AssertTillAllocation(3, 3, "2014-11-11 11:00", "2014-11-11 14:00", false);
            AssertTillAllocation(3, 6, "2014-11-11 14:30", "2014-11-11 15:45", false);
            AssertTillAllocation(3, 8, "2014-11-11 16:30", "2014-11-11 16:45", false);
            AssertTillAllocation(3, 1, "2014-11-11 16:45", "2014-11-11 20:00", false);
            AssertTillAllocation(3, 6, "2014-11-11 20:00", "2014-11-11 22:00", false);
            AssertTillAllocation(4, 9, "2014-11-11 11:30", "2014-11-11 14:30", false);
            AssertTillAllocation(4, 10, "2014-11-11 15:30", "2014-11-11 16:00", false);
            AssertTillAllocation(4, 10, "2014-11-11 18:00", "2014-11-11 22:00", false);
            AssertTillAllocation(5, 7, "2014-11-11 12:00", "2014-11-11 14:00", false);
        }

        [Ignore] // TODO: Need to add more mocks
        [Test]
        public void TestThursday()
        {
            InitialiseMainTest();

            AddShift(1);
            AddJob("2014-11-13 09:30", "2014-11-13 19:15");
            AddShift(2);
            AddJob("2014-11-13 14:45", "2014-11-13 22:00");
            AddShift(3);
            AddJob("2014-11-13 10:00", "2014-11-13 14:00");
            AddShift(5);
            AddJob("2014-11-13 09:45", "2014-11-13 18:15");
            AddShift(6);
            AddJob("2014-11-13 11:30", "2014-11-13 14:45");
            AddShift(8);
            AddJob("2014-11-13 09:00", "2014-11-13 10:00");
            AddJob("2014-11-13 16:15", "2014-11-13 16:30");
            AddShift(10);
            AddJob("2014-11-13 16:30", "2014-11-13 22:00");

            RunMainTest(15);

            AssertTillAllocation(1, 8, "2014-11-13 09:00", "2014-11-13 10:00", false);
            AssertTillAllocation(1, 1, "2014-11-13 10:00", "2014-11-13 19:15", false);
            AssertTillAllocation(1, 2, "2014-11-13 19:15", "2014-11-13 20:00", false);
            AssertTillAllocation(2, 3, "2014-11-13 10:00", "2014-11-13 12:00", false);
            AssertTillAllocation(2, 5, "2014-11-13 16:00", "2014-11-13 18:00", false);
            AssertTillAllocation(3, 5, "2014-11-13 09:45", "2014-11-13 16:00", false);
            AssertTillAllocation(3, 8, "2014-11-13 16:15", "2014-11-13 16:30", false);
            AssertTillAllocation(3, 10, "2014-11-13 16:30", "2014-11-13 22:00", false);
            AssertTillAllocation(4, 1, "2014-11-13 09:30", "2014-11-13 10:00", false);
            AssertTillAllocation(4, 6, "2014-11-13 11:30", "2014-11-13 14:45", false);
            AssertTillAllocation(4, 2, "2014-11-13 14:45", "2014-11-13 19:15", false);
            AssertTillAllocation(5, 3, "2014-11-13 12:00", "2014-11-13 14:00", false);
            AssertTillAllocation(5, 5, "2014-11-13 18:00", "2014-11-13 18:15", false);
            AssertTillAllocation(5, 2, "2014-11-13 20:00", "2014-11-13 22:00", false);
        }

        [Ignore] // TODO: Need to add more mocks
        [Test]
        public void TestSaturday()
        {
            InitialiseMainTest();

            AddShift(1);
            AddJob("2014-11-14 09:00", "2014-11-14 15:15");
            AddShift(2);
            AddJob("2014-11-14 09:00", "2014-11-14 14:00");
            AddShift(3);
            AddJob("2014-11-14 16:00", "2014-11-14 22:00");
            AddShift(5);
            AddJob("2014-11-14 11:30", "2014-11-14 15:00");
            AddShift(6);
            AddJob("2014-11-14 11:30", "2014-11-14 14:30");
            AddShift(7);
            AddJob("2014-11-14 17:00", "2014-11-14 20:45");
            AddShift(8);
            AddJob("2014-11-14 15:15", "2014-11-14 16:00");
            AddShift(9);
            AddJob("2014-11-14 15:00", "2014-11-14 22:00");

            RunMainTest(13);

            AssertTillAllocation(1, 1, "2014-11-14 09:00", "2014-11-14 15:15", false);
            AssertTillAllocation(1, 9, "2014-11-14 15:15", "2014-11-14 20:00", false);
            AssertTillAllocation(2, 2, "2014-11-14 10:00", "2014-11-14 12:00", false);
            AssertTillAllocation(2, 3, "2014-11-14 16:00", "2014-11-14 18:00", false);
            AssertTillAllocation(3, 2, "2014-11-14 09:00", "2014-11-14 10:00", false);
            AssertTillAllocation(3, 5, "2014-11-14 11:30", "2014-11-14 15:00", false);
            AssertTillAllocation(3, 9, "2014-11-14 15:00", "2014-11-14 15:15", false);
            AssertTillAllocation(3, 8, "2014-11-14 15:15", "2014-11-14 16:00", false);
            AssertTillAllocation(3, 7, "2014-11-14 17:00", "2014-11-14 20:45", false);
            AssertTillAllocation(4, 6, "2014-11-14 11:30", "2014-11-14 14:30", false);
            AssertTillAllocation(4, 3, "2014-11-14 18:00", "2014-11-14 22:00", false);
            AssertTillAllocation(5, 2, "2014-11-14 12:00", "2014-11-14 14:00", false);
            AssertTillAllocation(5, 9, "2014-11-14 20:00", "2014-11-14 22:00", false);
        }

        [Test]
        [Ignore]
        public void TestResponseForTillAllocationGET()
        {
            InitialiseSecondaryTest();

            //now do secondary test
            RunSecondaryTest();
        }
    }
}
