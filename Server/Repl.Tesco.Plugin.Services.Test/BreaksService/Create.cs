﻿using System;
using NUnit.Framework;
using Moq;
using Repl.Tesco.Data.Interfaces;
using WFM.Shared.DataTransferObjects.Breaks;
using System.Collections.Generic;
using Repl.Tesco.Plugin.Rest.Interfaces;
using Repl.Tesco.Plugin.Rest.Services;

namespace Repl.Tesco.Plugin.Services.Test.BreaksService
{
    [TestFixture]
    public class Create
    {
        public Mock<IBreakRepository> MockBreakRepository { get; set; }
        public IBreakService BreaksService { get; set; }
        private List<int> _shiftIds;
        private static int _siteId = 1000652;

        public void Initialise()
        {
            MockBreakRepository = new Mock<IBreakRepository>();
            MockBreakRepository.Setup(x => x.GetBreaks(It.IsAny<int>(), It.IsAny<List<int>>())).Returns(new List<BreakRecord>() { 
                new BreakRecord(){ BreakID = 1, BreakTaken = true, ShiftID = 1, SiteID = _siteId },
                new BreakRecord(){ BreakID = 2, BreakTaken = false, ShiftID = 2, SiteID = _siteId },
                new BreakRecord(){ BreakID = 3, BreakTaken = false, ShiftID = 3, SiteID = _siteId },
                new BreakRecord(){ BreakID = 4, BreakTaken = true, ShiftID = 1, SiteID = _siteId }
            });
            MockBreakRepository.Setup(x => x.SetBreaks(It.IsAny<List<BreakRecord>>()));
            BreaksService = new BreakService(MockBreakRepository.Object);
        }

        [Test]
        public void GetBreaks()
        {
            Initialise();
            
            this._shiftIds = new List<int>(){2, 3};

            ReadBreaksRequest readRequest = new ReadBreaksRequest()
            {
                ShiftIDs = this._shiftIds,
                SiteId = _siteId
            };

            var response = BreaksService.Post(readRequest);
            Assert.IsTrue(response.Breaks != null);
            Assert.IsTrue(response.Breaks.Count > 0);
        }

        [Test]
        public void SetBreaks()
        {
            Initialise();

            SetBreaksRequest writeRequest = new SetBreaksRequest()
            {
                Breaks = new List<BreakRecord>()
                {
                    new BreakRecord(){ BreakID = 5, BreakTaken = false, ShiftID = 4, SiteID = _siteId },
                    new BreakRecord(){ BreakID = 6, BreakTaken = false, ShiftID = 4, SiteID = _siteId },
                }
            };
            var response = BreaksService.Post(writeRequest);
            Assert.IsTrue(response != null);
        }
    }
}
