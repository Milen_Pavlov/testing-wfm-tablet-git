﻿using System;
using NUnit.Framework;
using Repl.Tesco.Data.Interfaces;
using Moq;
using Consortium.Model.Engine.Paging;
using System.Collections.Generic;
using Repl.Tesco.Models;
using System.Linq;
using Repl.Tesco.Constants;
using Repl.Tesco.Data;
using WFM.Shared.DataTransferObjects.Till;
using Repl.Tesco.Plugin.Rest.Interfaces;

namespace Repl.Tesco.Plugin.Services.Test.TillService
{
    [TestFixture]
    public class TillServiceTests
    {
        public Mock<ITillStatusRepository> TillStatusRepository { get; set; }
        public Mock<ITillRepository> TillRepository { get; set; }
        public Mock<ITillTypeRepository> TillTypeRepository { get; set; }
        public ITillService TillService { get; set; }
        private int _tillId, _tillOpeningSequence, _tillWindowId;
        private static int _siteId = 1000652;
        private IList<TillType> _tillTypes;
        private IList<Till> _tills;
        private IList<TillStatus> _tillStatuses;

        public void Initialise()
        {
            this.TillRepository = new Mock<ITillRepository>();
            this.TillTypeRepository = new Mock<ITillTypeRepository>();
            this.TillStatusRepository = new Mock<ITillStatusRepository>();
            this.TillTypeRepository.Setup(x => x.ReadAll(It.IsAny<Pager>())).Returns(
                new List<TillType>()
                {
                    new TillType(){ DisplayOrder = 0, Key = "MainBank", Name = "Main Bank", OpeningSequence = 1, TillTypeId = 1 },
                    new TillType(){ DisplayOrder = 1, Key = "Basket", Name = "Basket", OpeningSequence = 0, TillTypeId = 2 },
                }
            );
            this.TillTypeRepository.Setup(x => x.ReadById(1)).Returns
            (
                new TillType() { DisplayOrder = 0, Key = "MainBank", Name = "Main Bank", OpeningSequence = 1, TillTypeId = 1 }
            );
            this.TillTypeRepository.Setup(x => x.ReadById(2)).Returns
            (
                new TillType(){ DisplayOrder = 1, Key = "Basket", Name = "Basket", OpeningSequence = 0, TillTypeId = 2 }
            );
            this.TillStatusRepository.Setup(x => x.ReadAll(It.IsAny<Pager>())).Returns(
                new List<TillStatus>(){
                    new TillStatus(){ TillStatusId = 1, Key = "Open", Name="Open", DisplayOrder = 0},
                    new TillStatus(){ TillStatusId = 2, Key = "Unused", Name="Unused", DisplayOrder = 1},
                    new TillStatus(){ TillStatusId = 3, Key = "OutOfOrder", Name="Out of Order", DisplayOrder = 3},
                }
            );
            this.TillService = new Rest.Services.TillService(TillRepository.Object, TillTypeRepository.Object);
        }

        private void InitialiseMainTest()
        {
            Initialise();
            // Create tills
            _tillId = 1;
            _tillOpeningSequence = 0;
            _tills = new List<Till>();
            _tillTypes = TillTypeRepository.Object.ReadAll(Pager.TakeAll);
            _tillStatuses = TillStatusRepository.Object.ReadAll(Pager.TakeAll);

            AddTill("Basket 1", false, true);
            AddTillWindow("8:00", "20:00");

            AddTill("Basket 2", false, true);
            AddTillWindow("10:00", "12:00");
            AddTillWindow("16:00", "18:00");

            AddTill("Main Bank 1", true, true);
            AddTill("Main Bank 2", true, true);
            AddTill("Main Bank 3", true, true);

            TillRepository.Setup(x => x.ReadBySite(It.IsAny<int>(), It.IsAny<Pager>())).Returns(_tills);
            TillRepository.Setup(x => x.ReadById(It.IsAny<int>())).Returns(_tills.First());
        }

        private void AddTill(string tillNumber, bool mainBank, bool open)
        {
            _tills.Add(new Till()
            {
                TillId = _tillId++,
                TillNumber = tillNumber,
                OpeningSequence = _tillOpeningSequence++,
                SiteId = _siteId
            });

            if (mainBank)
            {
                _tillTypes.Single(s => s.Key == TillTypeKey.MainBank).Add(_tills.Last());
            }
            else
            {
                _tillTypes.Single(s => s.Key == TillTypeKey.Basket).Add(_tills.Last());
            }

            if (open)
            {
                _tillStatuses.Single(s => s.Key == TillStatusKey.Open).Add(_tills.Last());
            }
            else
            {
                _tillStatuses.Single(s => s.Key == TillStatusKey.Unused).Add(_tills.Last());
            }
        }

        private void AddTillWindow(string startTime, string endTime)
        {
            _tills.Last().Add(new TillWindow()
            {
                TillWindowId = _tillWindowId++,
                StartTime = DateTime.Parse(startTime),
                EndTime = DateTime.Parse(endTime),
            });
        }

        [Test]
        public void TestGetById()
        {
            InitialiseMainTest();
            ReadByIdRequest request = new ReadByIdRequest()
            {
                Skip = 0,
                Take = 100,
                TillId = 1
            };
            var response = this.TillService.Get(request);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TestGetBySite()
        {
            InitialiseMainTest();
            ReadBySiteRequest request = new ReadBySiteRequest()
            {
                Skip = 0,
                Take = 100,
                SiteId = _siteId
            };
            var response = this.TillService.Get(request);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TestDelete()
        {
            InitialiseMainTest();
            DeleteManyRequest request = new DeleteManyRequest()
            {
                Skip = 0,
                Take = 100,
                TillIds = _tills.Select(x => x.TillId).ToArray()
            };
            var response = this.TillService.Delete(request);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TestMove()
        {
            InitialiseMainTest();
            MoveRequest request = new MoveRequest()
            {
                Direction = 1,
                TillId = 1
            };
            var response = this.TillService.Get(request);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TestMoveFails()
        {
            InitialiseMainTest();
            MoveRequest request = new MoveRequest()
            {
                Direction = -3,
                TillId = 1
            };
            var response = this.TillService.Get(request);
            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TestOpeningSequence()
        {
            InitialiseMainTest();
            SetOpeningSequenceRequest request = new SetOpeningSequenceRequest()
            {
                OpeningSequence = 1,
                TillId = 1
            };
            var response = this.TillService.Get(request);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TestOpeningSequenceFails()
        {
            InitialiseMainTest();
            SetOpeningSequenceRequest request = new SetOpeningSequenceRequest()
            {
                OpeningSequence = 5,
                TillId = 1
            };
            var response = this.TillService.Get(request);
            Assert.AreEqual(response.NumOfItemsOverAllPages, null);
            Assert.AreEqual(response.Messages.FirstOrDefault().FriendlyMessage, "The opening sequence must be less than (or equal to) 4");
            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TestOpeningSequenceFailsWithBadInput()
        {
            InitialiseMainTest();
            SetOpeningSequenceRequest request = new SetOpeningSequenceRequest()
            {
                OpeningSequence = -2,
                TillId = 1
            };
            var response = this.TillService.Get(request);
            Assert.AreEqual(response.NumOfItemsOverAllPages, null);
            Assert.AreEqual(response.Messages.FirstOrDefault().FriendlyMessage, "The opening sequence must be greater than (or equal to) 0");
            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TestUpdateOpeningSequences()
        {
            InitialiseMainTest();
            UpdateOpeningSequencesRequest request = new UpdateOpeningSequencesRequest()
            {
                SiteId = _siteId,
                TillIdsOrderedByOpeningSequence = _tills.OrderBy(x => x.OpeningSequence).Select(x => x.TillId).ToArray()
            };
            var response = this.TillService.Post(request);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TestSaveMany()
        {
            InitialiseMainTest();
            SaveManyRequest request = new SaveManyRequest()
            {
                SiteId = _siteId,
                Tills = _tills.Select(x => new WFM.Shared.DataTransferObjects.Till.SaveManyRequest.Till()
                {
                    TillId = x.TillId,
                    OpeningSequence = x.OpeningSequence,
                    StatusId = x.StatusId,
                    TillNumber = x.TillNumber,
                    TypeId = x.TypeId,
                    Windows = (x.Windows != null) ? x.Windows.Select(b => new WFM.Shared.DataTransferObjects.Till.SaveManyRequest.TillWindow() { EndTime = b.EndTime, StartTime = b.StartTime }).ToArray() : null
                }).ToArray()
            };
            var response = this.TillService.Post(request);
            Assert.IsTrue(response.Success);
        }
    }
}
