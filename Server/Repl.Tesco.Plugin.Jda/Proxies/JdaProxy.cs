﻿using ClockManager.JDA.Api.Common;
using Repl.Tesco.Jda.Api.SiteMgr.BusinessUnit;
using Repl.Tesco.Jda.Models.SiteMgr;
using Siesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Jda.Proxies
{
    public class JdaProxy : IJdaProxy
    {

        public JdaProxy()
        {

        }

        public async Task<bool> LoginAsync(string userName, string password, SiestaClient client)
        {
            // Ensure we clear all cookies before starting a new session
            client.ClearCookies();

            SessionLoginRequest request = new SessionLoginRequest()
            {
                loginName = userName,
                password = password
            };

            RestReply<SessionLoginReply> reply = await client.Post<SessionLoginReply>(request);
            if (reply.Success)
            {
                return true;
            }

            return false;
        }

        public async Task<List<JdaOrgUnit>> GetOrgUnitsAsync(SiestaClient client)
        {
            OrgUnitsRequest request = new OrgUnitsRequest();
            RestReply<OrgUnitsReply> response = await client.Get<OrgUnitsReply>(request);

            if(response.Success)
            {
                if(response.Object.data != null)
                {
                    return response.Object.data;
                }
            }
            return null;
        }

    }
}
