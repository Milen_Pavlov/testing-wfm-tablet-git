﻿using Repl.Tesco.Jda.Models.SiteMgr;
using Siesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Jda.Proxies
{
    public interface IJdaProxy
    {
        Task<bool> LoginAsync(string userName, string Password, SiestaClient client);
        Task<List<JdaOrgUnit>> GetOrgUnitsAsync(SiestaClient client);
    }
}
