﻿using AutoMapper;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Interfaces;
using Repl.Tesco.Jda.Models.SiteMgr;
using Repl.Tesco.Models;
using Repl.Tesco.Plugin.Jda.Proxies;
using Siesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Jda.Services
{
    public class JdaWfmService : IWfmService
    {
        private readonly IJdaProxy m_jdaProxy;
        private readonly SiestaClient m_siesta;
        private readonly IAppSettingsRepository m_appSettings;

        public JdaWfmService(IJdaProxy jdaProxy, IAppSettingsRepository appSettings, SiestaClient siesta)
        {
            m_jdaProxy = jdaProxy;
            m_siesta = siesta;
            m_appSettings = appSettings;

            Mapper.CreateMap<JdaOrgUnit, Site>()
                .ForMember(x => x.SiteId, p => p.MapFrom(a => a.ID)); 
        }

        public async Task<List<Site>> GetSites()
        {
            // get sites from jda, map them, return
            await m_jdaProxy.LoginAsync(m_appSettings.JDAUsername, m_appSettings.JDAPassword, m_siesta);
            var jdaSites = await m_jdaProxy.GetOrgUnitsAsync(m_siesta);

            if(jdaSites != null)
            {
                List<Site> sites = Mapper.Map<List<JdaOrgUnit>, List<Site>>(jdaSites);
                return sites;
            }
            else
            {
                return null;
            }
        }
    }
}
