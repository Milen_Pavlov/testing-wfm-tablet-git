﻿using Consortium.Service.Engine;
using Repl.Tesco.Interfaces;
using Repl.Tesco.Plugin.Jda.Proxies;
using Repl.Tesco.Plugin.Jda.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Jda
{
    public class TescoJdaPlugin : ConsortiumPlugin
    {
        public override NavigationMenu GetMenu()
        {
            return null;
        }

        public override string GetName()
        {
            return "Jda";
        }

        public override void Register(ConsortiumContainer container, ConsortiumConfig config)
        {
            container.Register<IJdaProxy, JdaProxy>();
            container.Register<IWfmService, JdaWfmService>(); 
        }
    }
}
