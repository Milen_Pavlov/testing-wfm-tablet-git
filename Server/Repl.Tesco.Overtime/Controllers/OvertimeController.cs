﻿using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Interfaces;
using Repl.Tesco.Models;
using Repl.Tesco.Plugin.Overtime.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Repl.Tesco.Plugin.Overtime.Helpers;
using Repl.Tesco.Helpers;

namespace Repl.Tesco.Overtime.Controllers
{
    [ConsortiumAuth("Admin",Repl.Tesco.Helpers.Roles._TESCOROLE)]
    public class OvertimeController : ConsortiumController
    {
        private readonly ISiteRepository m_siteRepository;
        private readonly IWfmService m_wfmService;
        private readonly IOvertimeAgreedRepository m_overtimeAgreedRepository;
        private Repl.Tesco.Helpers.WeekHelper.AccountingConfiguration m_accountingConfig;

        public OvertimeController(ISiteRepository siteRepo,IOvertimeAgreedRepository overtimeAgreedRepo, IWfmService wfmService)
        {
            m_siteRepository = siteRepo;
            m_wfmService = wfmService;
            m_overtimeAgreedRepository = overtimeAgreedRepo;
            m_accountingConfig = new WeekHelper.AccountingConfiguration()
            {
                PeriodFirstDay = 2,
                PeriodFirstMonth = 3,
                StartDay = DayOfWeek.Monday
            };
        }

        [HttpGet]
        public ActionResult Index(DateTime? week)
        {
            DateTime currentWeek;

            if(week != null)
            {
                currentWeek = week.Value;
            }
            else
            {
                currentWeek = DateTime.UtcNow.StartOfWeek(DayOfWeek.Monday).Date;
            }

            // get any existing overtime agreed records that exist for this week
            var overtimeAgreedRecords = m_overtimeAgreedRepository.GetOvertimeAgreedForWeek(currentWeek);
            currentWeek = currentWeek.Date;
            IndexViewModel ivm = new IndexViewModel(overtimeAgreedRecords, currentWeek);
            ivm.CurrentWeekNumber = WeekHelper.GetWeekNumberForDate(currentWeek, m_accountingConfig);
            return View(ivm);
        }

        [HttpGet]
        public async Task<ActionResult> AddSite()
        {
            // get sites from jda
            var sites = await m_wfmService.GetSites();

            if(sites != null)
            {
                // compare jda sites against our own, return difference
                sites = m_siteRepository.CompareSites(sites);
                return View(new AddSiteViewModel(sites));
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSite(AddSiteViewModel svm)
        {
            // find site by selected id, add to database
            Site toAdd = svm.Sites.Where(p => p.SiteId == svm.SelectedSite).FirstOrDefault();
            m_siteRepository.InsertSite(toAdd);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int siteId, DateTime week)
        {
            OvertimeAgreed model = m_overtimeAgreedRepository.GetRecordForSiteAndWeek(siteId, week);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OvertimeAgreed oa)
        {
            if(ModelState.IsValid)
            {
                m_overtimeAgreedRepository.InsertOrUpdate(oa);
            }
            return RedirectToAction("Index", "Overtime", new { week = oa.Week});
        }
    }
}
