﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Plugin.Overtime.ViewModels
{
    public class IndexViewModel
    {
        
        public int CurrentWeekNumber { get; set; }
        public DateTime PreviousWeek { get { return CurrentWeek.AddDays(-7); } }
        public DateTime CurrentWeek { get; set; }
        public DateTime NextWeek { get { return CurrentWeek.AddDays(7); } }
        public List<OvertimeAgreed> OvertimeAgreedRecords { get; set; }

        public IndexViewModel()
        {
        }

        public IndexViewModel(List<OvertimeAgreed> records, DateTime week)
        {
            CurrentWeek = week;

            OvertimeAgreedRecords = records;
        }
    }
}
