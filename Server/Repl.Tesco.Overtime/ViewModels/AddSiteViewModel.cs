﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Repl.Tesco.Plugin.Overtime.ViewModels
{
    public class AddSiteViewModel
    {
        public int SelectedSite { get; set; }
        public IEnumerable<SelectListItem> SitesDropDownList { get; set; }
        public List<Site> Sites { get; set; }

        public AddSiteViewModel()
        {
        }

        public AddSiteViewModel(List<Site> sites)
        {
            Sites = sites;
            var t = new List<SelectListItem>();
            foreach(var site in sites)
            {
                t.Add(new SelectListItem() { Text = site.Name, Value = site.SiteId.ToString(), Selected = false });
            }
            SitesDropDownList = t;
        }
    }
}
