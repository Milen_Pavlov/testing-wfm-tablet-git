﻿using Consortium.Service.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Overtime
{
    public class TescoOvertimePlugin : ConsortiumPlugin
    {
        public override NavigationMenu GetMenu()
        {
            var nav = new NavigationMenu()
            {
                TitleResourceKey = "Overtime",
                Icon = NavigationIcon.Clock,
                Pages = new List<NavigationPage>()
                {
                    new NavigationPage() { ControllerName = "Overtime", ActionName = "Index", TitleResourceKey = "Sites", RequiredRoles= new List<string>(){"Admin", Repl.Tesco.Helpers.Roles._TESCOROLE}}
                }
            };
            return nav;
        }

        public override string GetName()
        {
            return "Overtime";
        }

        public override void Register(ConsortiumContainer container, ConsortiumConfig config)
        {
        }

    }
}
