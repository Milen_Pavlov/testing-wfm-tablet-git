﻿using WFM.Shared.Constants;
using Repl.Tesco.Data.Jda;
using Repl.Tesco.Interfaces.Repositories.Jda;
using Repl.Tesco.Models.Jda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consortium.Service.Engine;
using Repl.Tesco.Data.Interfaces;

namespace Repl.Tesco.Data.Repositories
{
    public class JdaSiteRepository : IJdaSiteRepository
    {
        private readonly IAppSettingsRepository m_appSettings;

        public JdaSiteRepository()
        {
            m_appSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();
        }

        public JdaSite ReadById(string sessionId, int siteId)
        {
            using (var context = new JdaDataContext())
            {
                context.Url = m_appSettings.GetOrgUnitsEndpoint; // ToDo: Should be "data/sitemgr/services/BusinessUnit.asmx/GetSite" but endpoint not working
                context.Method = HttpVerb.Get;
                context.SessionId = sessionId;
                context.Send();

                var site = context.Response["data"].ToList().SingleOrDefault(s => s.Value<int>("ID") == siteId);
                if (site == null)
                {
                    return null;
                }

                return new JdaSite(site);
            }
        }
    }
}
