﻿using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class AuditRepository : IAuditRepository
    {
        public void Create(Audit audit)
        {
            using(SqlDataContext context = new SqlDataContext())
            {
                context.Audits.Add(audit);
                context.SaveChanges();
            }
        }

        public List<Audit> Get(DateTime date, int siteId)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                var items = (from r in context.Audits
                             where r.DateOfChange == date
                             && r.Department.SiteId == siteId
                             select r).ToList();

                return items;
            }
        }
    }
}
