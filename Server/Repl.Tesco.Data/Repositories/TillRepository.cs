﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class TillRepository : ITillRepository
    {
        public Till ReadById(int tillId)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.
                    Tills.
                    Select(t => new
                    {
                        Till = t,
                        Windows = t.Windows
                    });

                var till = query.SingleOrDefault(t => t.Till.TillId == tillId);
                return (till == null ? null : till.Till);
            }
        }

        public IList<Till> ReadBySite(int siteId, Pager pager)
        {
            using (var context = new SqlDataContext())
            {
                var unpagedQuery = context.
                    Tills.
                    Where(t => t.SiteId == siteId).
                    Select(t => new
                    {
                        Till = t,
                        Windows = t.Windows,
                        Type = t.Type,
                        Status = t.Status,
                    }).
                    OrderBy(t => t.Till.OpeningSequence);

                pager.NumOfItems = unpagedQuery.Count();

                var pagedQuery = unpagedQuery.
                    Skip(pager.Skip).
                    Take(pager.TakeOrMax);

                return pagedQuery.ToList().Select(t => t.Till).ToList();
            }
        }

        public void SaveTillAndWindows(Till till)
        {
            using (var context = new SqlDataContext())
            {
                var isNew = (till.TillId == 0);

                var dbTill = (isNew ? new Till() : context.
                    Tills.
                    Where(t => t.TillId == till.TillId).
                    Select(t => new
                    {
                        Till = t,
                        Windows = t.Windows
                    }).
                    SingleOrDefault().
                    Till);

                dbTill.TillNumber = till.TillNumber;
                dbTill.OpeningSequence = till.OpeningSequence;
                dbTill.SiteId = till.SiteId;
                dbTill.StatusId = till.StatusId;
                dbTill.TypeId = till.TypeId;

                if (isNew)
                {
                    context.Tills.Add(dbTill);
                }

                if (dbTill.Windows != null)
                {
                    context.TillWindows.RemoveRange(dbTill.Windows);
                }

                if (till.Windows != null)
                {
                    var dbWindows = till.
                        Windows.
                        Select(w => new TillWindow()
                        {
                            TillId = dbTill.TillId,
                            StartTime = w.StartTime,
                            EndTime = w.EndTime
                        }).
                        ToList();

                    context.TillWindows.AddRange(dbWindows);
                }

                context.SaveChanges();
            }
        }

        public void UpdateOpeningSequences(IList<Till> tills)
        {
            using (var context = new SqlDataContext())
            {
                var tillIds = tills.Select(t => t.TillId).ToList();
                var dbTills = context.Tills.Where(t => tillIds.Contains(t.TillId)).ToList();

                foreach (var dbTill in dbTills)
                {
                    dbTill.OpeningSequence = tills.Single(t => t.TillId == dbTill.TillId).OpeningSequence;
                }

                context.SaveChanges();
            }
        }

        public void Delete(int tillId)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.
                    Tills.
                    Where(t => t.TillId == tillId).
                    Select(t => new
                    {
                        Till = t,
                        Allocations = t.Allocations,
                    });

                var till = query.ToList().Select(t => t.Till).SingleOrDefault();
                if (till != null)
                {
                    if (till.Allocations != null)
                    {
                        till.Allocations.Clear();
                    }
                    context.Tills.Remove(till);
                    context.SaveChanges();
                }
            }
        }
    }
}
