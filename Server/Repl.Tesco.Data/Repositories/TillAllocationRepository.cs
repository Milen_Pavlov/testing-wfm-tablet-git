﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class TillAllocationRepository : ITillAllocationRepository
    {
        public IList<TillAllocation> Read(int siteId, DateTime startTime, DateTime endTime, Pager pager)
        {
            using (var context = new SqlDataContext())
            {
                var unpagedQuery = context.
                    TillAllocations.
                    Where(a =>
                        a.SiteId == siteId &&
                        a.EndTime > startTime &&
                        a.StartTime < endTime).
                    Select(a => new
                    {
                        TillAllocation = a,
                        TillWindows = a.TillWindows,
                    }).
                    OrderBy(a => a.TillAllocation.StartTime);

                pager.NumOfItems = unpagedQuery.Count();
                var pagedQuery = unpagedQuery.
                    Skip(pager.Skip).
                    Take(pager.TakeOrMax);

                return pagedQuery.
                    ToList().
                    Select(a => a.TillAllocation).
                    ToList();
            }
        }

        public void Create(IList<TillAllocation> tillAllocations)
        {
            using (var context = new SqlDataContext())
            {
                ConstructAllocations(tillAllocations, context);

                context.SaveChanges();
            }
        }

        private static void ConstructAllocations(IList<TillAllocation> tillAllocations, SqlDataContext context)
        {
            foreach (var tillAllocation in tillAllocations)
            {
                var dbTillAllocation = new TillAllocation()
                {
                    StartTime = tillAllocation.StartTime,
                    EndTime = tillAllocation.EndTime,
                    SiteId = tillAllocation.SiteId,
                    SiteName = tillAllocation.SiteName,
                    EmployeeId = tillAllocation.EmployeeId,
                    EmployeeFullName = tillAllocation.EmployeeFullName,
                    //TillId = tillAllocation.TillId,
                    StampedTillId = tillAllocation.StampedTillId,
                    TillNumber = tillAllocation.TillNumber,
                    CheckoutSupport = tillAllocation.CheckoutSupport,
                    TillTypeKey = tillAllocation.TillTypeKey,
                    TillStatusKey = tillAllocation.TillStatusKey,
                    TillOpeningSequence = tillAllocation.TillOpeningSequence,
                };

                if (tillAllocation.TillWindows != null)
                {
                    foreach (var tillWindow in tillAllocation.TillWindows)
                    {
                        var dbTillWindow = new TillAllocationTillWindow()
                        {
                            StartTime = tillWindow.StartTime,
                            EndTime = tillWindow.EndTime,
                        };
                        dbTillAllocation.Add(dbTillWindow);
                    }
                }

                context.TillAllocations.Add(dbTillAllocation);
            }
        }

        public void Delete(int siteId, DateTime startTime, DateTime endTime)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.TillAllocations.Where(a => a.SiteId == siteId && a.EndTime > startTime && a.StartTime < endTime);
                var dbTillAllocations = query.ToList();
                context.TillAllocations.RemoveRange(dbTillAllocations);
                context.SaveChanges();
            }
        }

        public void Move(int siteId, List<TillAllocation> allocationMoves)
        {
            using (var context = new SqlDataContext())
            {
                var existingAllocationIds = allocationMoves.Select(allocation => allocation.TillAllocationId);

                var toRemove = context.TillAllocations.Where(allocation => existingAllocationIds.Contains(allocation.TillAllocationId) && allocation.SiteId == siteId);

                context.TillAllocations.RemoveRange(toRemove.ToList());

                ConstructAllocations(allocationMoves, context);

                context.SaveChanges();
            }
        }
    }
}
