﻿using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class GlobalSettingsRepository : IGlobalSettingsRepository
    {
        public void UpdateMaximumHours(int? maxHours, int siteId)
        {
            using(SqlDataContext context = new SqlDataContext())
            {
                var settings = context.GlobalSettings.Where(p => p.Site == siteId).FirstOrDefault();

                if(settings == null)
                {
                    settings = new GlobalSettings() { MaximumHours = maxHours, Site = siteId };
                    context.GlobalSettings.Add(settings);
                }
                else
                {
                    settings.MaximumHours = maxHours;
                    context.Entry<GlobalSettings>(settings).State = System.Data.Entity.EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public GlobalSettings Get(int siteId)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                var settings = context.GlobalSettings.Where(p => p.Site == siteId).FirstOrDefault();
                return settings;
            }
        }
    }
}
