﻿using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class OvertimeAgreedRepository : IOvertimeAgreedRepository
    {
        public List<OvertimeAgreed> GetOvertimeAgreedForWeek(DateTime week)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                // get records for the week
                var records = (from r in context.OvertimeAgreed
                               where r.Week == week
                               select r).ToList();

                // just get ids of sites
                int[] siteIds = records.Select(p => p.SiteId).ToArray();

                // get difference site list
                var sites = (from r in context.Sites
                             where !siteIds.Contains(r.Id)
                             select r).ToList();

                // add difference
                if (sites.Count > 0)
                {
                    foreach (var item in sites)
                    {
                        records.Add(new OvertimeAgreed() { Week = week, SiteId = item.Id, Site = item });
                    }  
                }

                foreach (var item in records)
                {
                    if (item.Site == null)
                    {
                        item.Site = context.Sites.Find(item.SiteId);
                    }
                }

                return records;
            }
        }

        public OvertimeAgreed GetRecordForSiteAndWeek(int siteId, DateTime week)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                // get record if one exists
                var record = (from r in context.OvertimeAgreed
                              where r.SiteId == siteId
                              && r.Week == week
                              select r).FirstOrDefault();

                if(record != null)
                {
                    record.Site = context.Sites.Find(record.SiteId);
                    return record;
                }

                // check site exists
                Site site = context.Sites.Where(p => p.Id == siteId).FirstOrDefault();
                
                if(site == null)
                {
                    return null;
                }

                return new OvertimeAgreed() { SiteId = siteId, Week = week, Site = site, Id = -1 };
            }
        }

        public int InsertOrUpdate(OvertimeAgreed otA)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                if(otA.Id == -1)
                {
                    // insert
                    otA.Site = context.Sites.Find(otA.SiteId);
                    context.OvertimeAgreed.Add(otA);
                }
                else
                {
                    // update
                    context.Entry<OvertimeAgreed>(otA).State = System.Data.Entity.EntityState.Modified;
                }
                return context.SaveChanges();
            }
        }
    }
}
