﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class TillStatusRepository : ITillStatusRepository
    {
        public IList<TillStatus> ReadAll(Pager pager)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.TillStatuses.OrderBy(t => t.DisplayOrder);
                pager.NumOfItems = query.Count();
                return query.Skip(pager.Skip).Take(pager.TakeOrMax).ToList();
            }
        }
    }
}
