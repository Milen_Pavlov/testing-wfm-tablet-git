﻿using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class ResourceRepository : IResourceRepository
    {
        public int Insert(Resource resource)
        {
            using(SqlDataContext context = new SqlDataContext())
            {
                context.Resources.Add(resource);
                return context.SaveChanges();
            }
        }

        public Resource Get(int siteId, DateTime week)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                return context.Resources.Where(p => p.Site == siteId && p.Date.Equals(week)).FirstOrDefault();
            }
        }

        public void RemoveAll(int siteId, DateTime week)
        {
            using (SqlDataContext context = new SqlDataContext())
            {
                var toRemove = (from r in context.Resources
                                where r.Date.Equals(week) && r.Site == siteId
                                select r);
                foreach (var item in toRemove)
                {
                    context.Resources.Remove(item);
                }

                context.SaveChanges();
            }
        }
    }
}
