﻿using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Breaks;

namespace Repl.Tesco.Data.Repositories
{
    public class BreakRepository : IBreakRepository
    {
        public void SetBreaks(IList<BreakRecord> toSet)
        {
            using (var context = new SqlDataContext())
            {
                //This monstrosity is because you can't quickly select over a composite key in entity framework, splitting the components out into
                //seperate lists, selecting on them and then joining on the query is (apparently) the best way of doing this.
                var siteids = toSet.Select(record => record.SiteID);
                var breakids = toSet.Select(record => record.BreakID);
                var shiftids = toSet.Select(record => record.ShiftID);

                var rawSelection = from entity in context.Breaks
                    where siteids.Contains(entity.SiteID) && breakids.Contains(entity.BreakID) && shiftids.Contains(entity.ShiftID)
                        select entity;

                var refined = from entity in rawSelection.AsEnumerable()
                        join pair in toSet on new { ID1 = entity.SiteID, ID2 = entity.ShiftID, ID3 = entity.BreakID }
                        equals new { ID1 = pair.SiteID, ID2 = pair.ShiftID, ID3 = pair.BreakID  }
                    select entity;

                foreach (BreakRecord breakRecord in toSet)
                {
                    var existing = refined.SingleOrDefault(b => b.BreakID == breakRecord.BreakID && b.ShiftID == breakRecord.ShiftID && b.SiteID == breakRecord.SiteID);

                    if (existing == null)
                    {
                        existing = context.Breaks.Add(new Break() { SiteID = breakRecord.SiteID, ShiftID = breakRecord.ShiftID, BreakID = breakRecord.BreakID, BreakTaken = breakRecord.BreakTaken });
                    }
                    else
                    {
                        existing.BreakTaken = breakRecord.BreakTaken;
                    }
                }

                context.SaveChanges();
            }
        }

        public IList<BreakRecord> GetBreaks(int forSiteId, List<int> forShiftIDs)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.
                  Breaks.
                  Where(b => b.SiteID == forSiteId && forShiftIDs.Contains(b.ShiftID)).
                  Select(b => new BreakRecord()
                  {
                      SiteID = b.SiteID,
                      ShiftID = b.ShiftID,
                      BreakID = b.BreakID,
                      BreakTaken = b.BreakTaken
                  });

                return query.ToList();
            }
        }
    }
}
