﻿using Repl.Tesco.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class AppSettingsRepository : IAppSettingsRepository
    {
        private NameValueCollection m_appconfig;

        public AppSettingsRepository(NameValueCollection appconfig)
        {
            m_appconfig = appconfig;
        }

        public string JDAServerRequestFormat
        {
            get { return m_appconfig.Get("JDAServer"); }
        }

        public string JdaServer
        {
            get { return m_appconfig.Get("JDAServerUnformatted"); }
        }

        public string ScheduleDataEndpoint
        {
            get { return m_appconfig.Get("ScheduleDataEndpoint"); }
        }

        public string LoginEndpoint
        {
            get { return m_appconfig.Get("LoginEndpoint"); }
        }

        public string KeepAliveEndpoint
        {
            get { return m_appconfig.Get("KeepAliveEndpoint"); }
        }

        public string LogoutEndpoint
        {
            get { return m_appconfig.Get("LogoutEndpoint"); }
        }

        public string GetOrgUnitsEndpoint
        {
            get { return m_appconfig.Get("GetOrgUnitsEndpoint"); }
        }


        public string JDAUsername
        {
            get { return m_appconfig.Get("JDAUsername"); }
        }

        public string JDAPassword
        {
            get { return m_appconfig.Get("JDAPassword"); }
        }
    }
}
