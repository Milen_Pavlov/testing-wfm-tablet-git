﻿using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class DepartmentRepository : IDepartmentRepository
    {

        public Department Get(int siteId)
        {
            using(SqlDataContext context = new SqlDataContext())
            {
                var result = context.Departments.Where(p => p.SiteId == siteId).FirstOrDefault();
                return result;
            }
        }
    }
}
