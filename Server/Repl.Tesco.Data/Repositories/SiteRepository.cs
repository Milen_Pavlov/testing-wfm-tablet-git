﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class SiteRepository : ISiteRepository
    {
        public Site GetById(int siteId)
        {
            using (var context = new SqlDataContext())
            {
                return context.Sites.Where(p => p.SiteId == siteId).FirstOrDefault();
            }
        }

        public IList<Site> ReadAll(Pager pager)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.Sites;
                pager.NumOfItems = query.Count();
                return query.Skip(pager.Skip).Take(pager.TakeOrMax).ToList();
            }
        }

        public int UpdateAll(List<Site> sites)
        {
            using (var context = new SqlDataContext())
            {
                // strip ids out to do SQL IN
                List<int> ids = sites.Select(p => p.Id).ToList();

                // get any that are the same
                var results = (from r in context.Sites
                               where ids.Contains(r.SiteId)
                               select r).ToList();

                // remove from list any that are the same
                foreach (var item in results)
                {
                    sites.RemoveAll(p => p.SiteId == item.SiteId);    
                }

                // finally insert and save
                context.Sites.AddRange(sites);
                int success = context.SaveChanges();
                return success;
            }
        }

        public List<Site> CompareSites(List<Site> sites)
        {
            using (var context = new SqlDataContext())
            {
                // strip ids out to do SQL IN
                List<int> ids = sites.Select(p => p.Id).ToList();

                // get any from db that are the same as in list
                var results = (from r in context.Sites
                               where ids.Contains(r.SiteId)
                               select r).ToList();

                // remove from list any that are the same
                foreach (var item in results)
                {
                    sites.RemoveAll(p => p.SiteId == item.SiteId);
                }

                return sites;
            }
        }

        public void InsertSite(Site site)
        {
            using (var context = new SqlDataContext())
            {
                context.Sites.Add(site);
                context.SaveChanges();
            }
        }
    }
}
