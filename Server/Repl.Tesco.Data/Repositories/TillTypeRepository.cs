﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Repositories
{
    public class TillTypeRepository : ITillTypeRepository
    {
        public TillType ReadById(int tillTypeId)
        {
            using (var context = new SqlDataContext())
            {
                return context.TillTypes.SingleOrDefault(t => t.TillTypeId == tillTypeId);
            }
        }

        public IList<TillType> ReadAll(Pager pager)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.TillTypes.OrderBy(t => t.DisplayOrder);
                pager.NumOfItems = query.Count();
                return query.Skip(pager.Skip).Take(pager.TakeOrMax).ToList();
            }
        }
    }
}
