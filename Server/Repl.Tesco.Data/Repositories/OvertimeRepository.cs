﻿using Newtonsoft.Json;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Models;
using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace Repl.Tesco.Data.Repositories
{
    public class OvertimeRepository : IOvertimeRepository
    {

        public void SetOvertime(int forSiteId, DateTime forDate, string username, IList<OvertimeRecord> toCreate)
        {
            using (var context = new SqlDataContext())
            {
                // get departments relational to the site entered, or new records to be created
                var newDepartmentIds = toCreate.Select(entity => entity.JDADepartmentId);
                var dbDepartments = context.
                  Departments.
                  Where(d => d.SiteId == forSiteId && newDepartmentIds.Contains(d.JDADepartmentId)).ToList();

                foreach(OvertimeRecord otr in toCreate)
                {
                    // check if there is an existing department
                    DateTime dateOfChange = DateTime.UtcNow;
                    Audit audit = new Audit() {  DateOfChange = forDate, Username = username };
                    audit.JsonData = JsonConvert.SerializeObject(new { OvertimeRecord = otr, DateOfChange = dateOfChange });
                    
                    var existing = dbDepartments.SingleOrDefault(department => department.JDADepartmentId == otr.JDADepartmentId);

                    if (existing == null)
                    {
                        // if null add new department and create new overtime record
                        existing = context.Departments.Add(new Department() { JDADepartmentId = otr.JDADepartmentId, SiteId = forSiteId, DepartmentName = otr.DepartmentName });
                        existing.OvertimeHours = new List<OvertimeMapping>();
                        existing.OvertimeHours.Add(new OvertimeMapping() 
                        { 
                            Department = existing,
                            DepartmentId = existing.DepartmentId, 
                            ForDate = forDate,
                            Hours = otr.AllocatedHours, 
                            ResourceRequestNight = otr.ResourceRequestNight,
                            ResourceRequestDay = otr.ResourceRequestDay,
                            ResourceAgreedNight = otr.ResourceAgreedNight 
                        });
                    }
                    else
                    {
                        // here update or create, dependant on if we find a record
                        //Diff seconds because you can't compare datetimes
                        var existingHours = context.Overtime.Where(overtime => overtime.DepartmentId == existing.DepartmentId && DbFunctions.DiffSeconds(overtime.ForDate,forDate) == 0).FirstOrDefault();

                        if (existingHours == null)
                        {
                            existing.OvertimeHours = new List<OvertimeMapping>();
                            existing.OvertimeHours.Add(new OvertimeMapping() 
                            { 
                                Department = existing, 
                                DepartmentId = existing.DepartmentId, 
                                ForDate = forDate,
                                Hours = otr.AllocatedHours,
                                ResourceRequestNight = otr.ResourceRequestNight,
                                ResourceRequestDay = otr.ResourceRequestDay,
                                ResourceAgreedNight = otr.ResourceAgreedNight 
                            });
                        }
                        else
                        {
                            existingHours.Hours = otr.AllocatedHours;
                            existingHours.ResourceAgreedNight = otr.ResourceAgreedNight;
                            existingHours.ResourceRequestDay = otr.ResourceRequestDay;
                            existingHours.ResourceRequestNight = otr.ResourceRequestNight;
                        }
                    }

                    WriteAuditMessage(username, otr, dateOfChange, audit, existing);
                }

                context.SaveChanges();
            }
        }

        private static void WriteAuditMessage(string username, OvertimeRecord otr, DateTime dateOfChange, Audit audit, Department existing)
        {
            audit.DepartmentId = existing.DepartmentId;
            audit.Department = existing;

            bool setRequested = otr.ResourceRequestDay.HasValue || otr.ResourceRequestNight.HasValue;
            bool setAgreed = otr.AllocatedHours.HasValue || otr.ResourceAgreedNight.HasValue;

            StringBuilder message = new StringBuilder();
            message.Append(string.Format("Set {0} ",existing.DepartmentName));
            
            if(setRequested)
            {
                message.Append("requested hours ");

                if (otr.ResourceRequestDay.HasValue)
                {
                    message.Append("day to ");
                    message.Append(otr.ResourceRequestDay.Value);

                    if(otr.ResourceRequestNight.HasValue)
                    {
                        message.Append(", ");
                    }
                }

                if (otr.ResourceRequestNight.HasValue)
                {
                    message.Append("night to ");
                    message.Append(otr.ResourceRequestNight.Value);
                }

                if (setAgreed)
                    message.Append(" and set ");
            }

            if (setAgreed)
            {
                message.Append("agreed hours ");

                if (otr.AllocatedHours.HasValue)
                {
                    message.Append("day to ");
                    message.Append(otr.AllocatedHours.Value);

                    if (otr.ResourceAgreedNight.HasValue)
                    {
                        message.Append(", ");
                    }
                }

                if (otr.ResourceAgreedNight.HasValue)
                {
                    message.Append("night to ");
                    message.Append(otr.ResourceAgreedNight.Value);
                }
            }

            message.Append(", on ");
            message.Append(dateOfChange.ToString());

            audit.Message = message.ToString();
            existing.Audits.Add(audit);
        }

        public IList<Department> GetOvertime(int forSiteId, DateTime forDate)
        {
            using (var context = new SqlDataContext())
            {
                var query = context.
                  Departments.
                  Where(d => d.SiteId == forSiteId).
                  Select(d => new
                  {
                      Department = d,
                      Hour = d.OvertimeHours.FirstOrDefault(overtime => DbFunctions.DiffSeconds(overtime.ForDate, forDate) == 0)
                  });

                var departments = query.
                    ToList().
                    //Select only those departments that have an hours mapping
                    Where(result => result.Hour != null).
                    Select(d => d.Department).
                    ToList();

                return departments;
            }
        }
    }
}
