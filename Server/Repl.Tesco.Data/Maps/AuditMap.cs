﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class AuditMap : EntityTypeConfiguration<Audit>
    {
        public AuditMap()
        {
            //Keys
            HasKey(m => new { m.Id });

            //Properties
            Property(m => m.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);            
            Property(m => m.Username).IsRequired();
            Property(m => m.DepartmentId).IsRequired();
            Property(m => m.DateOfChange).IsRequired();
            Property(m => m.Message).IsRequired();
            Property(m => m.JsonData).IsRequired();

            //Table
            ToTable("Audits");
        }
    }
}
