﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class OvertimeAgreedMap : EntityTypeConfiguration<OvertimeAgreed>
    {
        public OvertimeAgreedMap()
        {
            // keys
            HasKey(m => m.Id);

            // Fields
            Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(m => m.Week).IsRequired();
            Property(m => m.SiteId).IsRequired();
            Property(m => m.OvertimeAgreedAmount).IsOptional();

            // Relationships
            HasRequired(m => m.Site).WithMany(m => m.AgreedOvertime).HasForeignKey(m => m.SiteId);

            ToTable("OvertimeAgreed");
        }
    }
}
