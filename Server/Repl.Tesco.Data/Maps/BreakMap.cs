﻿using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    class BreakMap: EntityTypeConfiguration<Break>
    {
        public BreakMap()
        {
            //Keys
            HasKey(m => new { m.SiteID, m.BreakID, m.ShiftID, });

            //Properties
            Property(m => m.SiteID).IsRequired();
            Property(m => m.ShiftID).IsRequired();
            Property(m => m.BreakTaken).IsRequired();
            Property(m => m.BreakID).IsRequired();
           
            //Table
            ToTable("Breaks");
        }
    }
}
