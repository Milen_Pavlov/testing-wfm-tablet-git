﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class TillMap : EntityTypeConfiguration<Till>
    {
        public TillMap()
        {
            // Key
            HasKey(m => m.TillId);

            // Fields
            Property(m => m.TillId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.TillNumber).IsRequired();
            Property(m => m.OpeningSequence).IsRequired();
            Property(m => m.SiteId).IsRequired();
            Property(m => m.StatusId).IsRequired();
            Property(m => m.TypeId).IsRequired();

            // Relationships
            HasRequired(m => m.Status).WithMany(m => m.Tills).HasForeignKey(m => m.StatusId);
            HasRequired(m => m.Type).WithMany(m => m.Tills).HasForeignKey(m => m.TypeId);

            // Table
            ToTable("Tills");

            // Ignore
            Ignore(m => m.OrderedWindows);
        }
    }
}
