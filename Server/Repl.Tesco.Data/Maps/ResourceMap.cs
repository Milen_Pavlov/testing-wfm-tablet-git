﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class ResourceMap : EntityTypeConfiguration<Resource>
    {
        public ResourceMap()
        {
            HasKey(m => m.Id);

            //Properties
            Property(m => m.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.JsonData).IsRequired();
            Property(m => m.Site).IsOptional();
            Property(m => m.Date).IsOptional();
            Property(m => m.Username).IsRequired();
            Property(m => m.LockedDateTime).IsOptional();

            ToTable("Resources");
        }
    }
}
