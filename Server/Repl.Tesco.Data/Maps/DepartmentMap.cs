﻿using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class DepartmentMap : EntityTypeConfiguration<Department>
    {
        public DepartmentMap()
        {
            //Keys
            HasKey(m => m.DepartmentId);

            //Properties
            Property(m => m.DepartmentId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.JDADepartmentId).IsRequired();
            Property(m => m.DepartmentName).IsOptional();
            
            //Sort of parent
            Property(m => m.SiteId).IsRequired();

            //Table
            ToTable("Departments");
        }
    }
}
