﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class TillAllocationMap : EntityTypeConfiguration<TillAllocation>
    {
        public TillAllocationMap()
        {
            // Keys
            HasKey(m => m.TillAllocationId);

            // Fields
            Property(m => m.TillAllocationId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.StartTime).IsRequired();
            Property(m => m.EndTime).IsRequired();
            Property(m => m.SiteId).IsRequired();
            Property(m => m.SiteName).IsRequired();
            Property(m => m.EmployeeId).IsRequired();
            Property(m => m.EmployeeFullName).IsRequired();
            //Property(m => m.TillId).IsOptional();
            Property(m => m.StampedTillId).IsOptional();
            Property(m => m.TillNumber).IsOptional();
            Property(m => m.CheckoutSupport).IsRequired();
            Property(m => m.TillTypeKey).IsOptional();
            Property(m => m.TillStatusKey).IsOptional();
            Property(m => m.TillOpeningSequence).IsOptional();

            // Relationships
            //HasOptional(m => m.Till).WithMany(m => m.Allocations).HasForeignKey(m => m.TillId);

            // Table
            ToTable("TillAllocations");
        }
    }
}
