﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class GlobalSettingsMap : EntityTypeConfiguration<GlobalSettings>
    {
        public GlobalSettingsMap()
        {
            //Keys
            HasKey(m => m.Id);

            //Properties
            Property(m => m.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.MaximumHours).IsOptional();
            Property(m => m.Site).IsRequired();
        }
    }
}
