﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class TillAllocationTillWindowMap : EntityTypeConfiguration<TillAllocationTillWindow>
    {
        public TillAllocationTillWindowMap()
        {
            // Keys
            HasKey(m => m.TillAllocationTillWindowId);

            // Fields
            Property(m => m.TillAllocationTillWindowId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.StartTime).IsRequired();
            Property(m => m.EndTime).IsRequired();

            // Relationships
            HasRequired(m => m.TillAllocation).WithMany(m => m.TillWindows).HasForeignKey(m => m.TillAllocationId);

            // Table
            ToTable("TillAllocationTillWindows");
        }
    }
}
