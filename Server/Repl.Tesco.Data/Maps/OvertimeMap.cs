﻿using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    class OvertimeMap : EntityTypeConfiguration<OvertimeMapping>
    {
        public OvertimeMap()
        {
            // Keys
            HasKey(m => new { m.DepartmentId, m.ForDate,});

            Property(m => m.ForDate).HasColumnType("datetime2").IsRequired().HasColumnOrder(0);
            Property(m => m.DepartmentId).IsRequired().HasColumnOrder(1);
            Property(m => m.Hours).IsOptional();
            Property(m => m.ResourceAgreedNight).IsOptional();
            Property(m => m.ResourceRequestDay).IsOptional();
            Property(m => m.ResourceRequestNight).IsOptional();

            // Relationships
            HasRequired(m => m.Department).WithMany(m => m.OvertimeHours).HasForeignKey(m => m.DepartmentId);

            // Table
            ToTable("OvertimeHours");
        }
    }
}
