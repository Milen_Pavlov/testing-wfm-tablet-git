﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class TillWindowMap : EntityTypeConfiguration<TillWindow>
    {
        public TillWindowMap()
        {
            // Keys
            HasKey(m => m.TillWindowId);

            // Fields
            Property(m => m.TillWindowId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.StartTime).IsRequired();
            Property(m => m.EndTime).IsRequired();

            // Relationships
            HasRequired(m => m.Till).WithMany(m => m.Windows).HasForeignKey(m => m.TillId);

            // Table
            ToTable("TillWindows");
        }
    }
}
