﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Maps
{
    public class TillTypeMap : EntityTypeConfiguration<TillType>
    {
        public TillTypeMap()
        {
            // Keys
            HasKey(m => m.TillTypeId);

            // Fields
            Property(m => m.TillTypeId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Key).IsRequired();
            Property(m => m.Name).IsRequired();
            Property(m => m.DisplayOrder).IsRequired();

            // Relationships

            // Table
            ToTable("TillTypes");
        }
    }
}
