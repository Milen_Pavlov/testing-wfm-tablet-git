namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePKSite2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites");
            DropIndex("dbo.OvertimeAgreed", new[] { "SiteId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.OvertimeAgreed", "SiteId");
            AddForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites", "Id", cascadeDelete: true);
        }
    }
}
