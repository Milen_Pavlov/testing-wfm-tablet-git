namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KeepAllAsIsAddNewSiteColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sites", "SiteId", c => c.Int(nullable: false));
            CreateIndex("dbo.OvertimeAgreed", "SiteId");
            AddForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites");
            DropIndex("dbo.OvertimeAgreed", new[] { "SiteId" });
            DropColumn("dbo.Sites", "SiteId");
        }
    }
}
