namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GlobalSettingsSiteId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GlobalSettings", "Site", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GlobalSettings", "Site");
        }
    }
}
