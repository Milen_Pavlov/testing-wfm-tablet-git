namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResourcesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Resources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Site = c.Int(),
                        JsonData = c.String(nullable: false),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Resources");
        }
    }
}
