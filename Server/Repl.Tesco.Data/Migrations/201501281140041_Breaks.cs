namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Breaks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Breaks",
                c => new
                    {
                        SiteID = c.Int(nullable: false),
                        BreakID = c.Int(nullable: false),
                        ShiftID = c.Int(nullable: false),
                        BreakTaken = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.SiteID, t.BreakID, t.ShiftID });
        }
        
        public override void Down()
        {
            DropTable("dbo.Breaks");
        }
    }
}
