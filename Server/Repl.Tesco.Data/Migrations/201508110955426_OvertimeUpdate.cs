namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OvertimeUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OvertimeHours", "ResourceRequestDay", c => c.Single());
            AddColumn("dbo.OvertimeHours", "ResourceRequestNight", c => c.Single());
            AddColumn("dbo.OvertimeHours", "ResourceAgreedNight", c => c.Single());
            AlterColumn("dbo.OvertimeHours", "Hours", c => c.Single());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OvertimeHours", "Hours", c => c.Single(nullable: false));
            DropColumn("dbo.OvertimeHours", "ResourceAgreedNight");
            DropColumn("dbo.OvertimeHours", "ResourceRequestNight");
            DropColumn("dbo.OvertimeHours", "ResourceRequestDay");
        }
    }
}
