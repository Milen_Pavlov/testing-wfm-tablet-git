namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GlobalSettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GlobalSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaximumHours = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Tills", "MaximumHours");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tills", "MaximumHours", c => c.Int());
            DropTable("dbo.GlobalSettings");
        }
    }
}
