namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class overtimeservice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        JDADepartmentId = c.Int(nullable: false),
                        DepartmentName = c.String(),
                        SiteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            CreateTable(
                "dbo.OvertimeHours",
                c => new
                    {
                        ForDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DepartmentId = c.Int(nullable: false),
                        Hours = c.Single(nullable: false),
                    })
                .PrimaryKey(t => new { t.ForDate, t.DepartmentId })
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .Index(t => t.DepartmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OvertimeHours", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.OvertimeHours", new[] { "DepartmentId" });
            DropTable("dbo.OvertimeHours");
            DropTable("dbo.Departments");
        }
    }
}
