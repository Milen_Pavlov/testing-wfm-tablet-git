namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TillMaximumHours : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tills", "MaximumHours", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tills", "MaximumHours");
        }
    }
}
