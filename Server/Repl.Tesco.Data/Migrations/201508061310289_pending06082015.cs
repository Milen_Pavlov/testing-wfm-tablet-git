namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pending06082015 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites");
            DropPrimaryKey("dbo.Sites");
            AlterColumn("dbo.Sites", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Sites", "Id");
            AddForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites");
            DropPrimaryKey("dbo.Sites");
            AlterColumn("dbo.Sites", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Sites", "Id");
            AddForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites", "Id", cascadeDelete: true);
        }
    }
}
