namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResourceFieldAdditions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Resources", "Username", c => c.String(nullable: false));
            AddColumn("dbo.Resources", "LockedDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Resources", "LockedDateTime");
            DropColumn("dbo.Resources", "Username");
        }
    }
}
