namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TillAllocations",
                c => new
                    {
                        TillAllocationId = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        SiteId = c.Int(nullable: false),
                        SiteName = c.String(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        EmployeeFullName = c.String(nullable: false),
                        TillId = c.Int(),
                        StampedTillId = c.Int(),
                        TillNumber = c.String(),
                        CheckoutSupport = c.Boolean(nullable: false),
                        TillTypeKey = c.String(),
                        TillStatusKey = c.String(),
                        TillOpeningSequence = c.Int(),
                    })
                .PrimaryKey(t => t.TillAllocationId)
                .ForeignKey("dbo.Tills", t => t.TillId)
                .Index(t => t.TillId);
            
            CreateTable(
                "dbo.Tills",
                c => new
                    {
                        TillId = c.Int(nullable: false, identity: true),
                        TillNumber = c.String(nullable: false),
                        OpeningSequence = c.Int(nullable: false),
                        SiteId = c.Int(nullable: false),
                        StatusId = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TillId)
                .ForeignKey("dbo.TillStatuses", t => t.StatusId, cascadeDelete: true)
                .ForeignKey("dbo.TillTypes", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.StatusId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.TillStatuses",
                c => new
                    {
                        TillStatusId = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        DisplayOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TillStatusId);
            
            CreateTable(
                "dbo.TillTypes",
                c => new
                    {
                        TillTypeId = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        DisplayOrder = c.Int(nullable: false),
                        OpeningSequence = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TillTypeId);
            
            CreateTable(
                "dbo.TillWindows",
                c => new
                    {
                        TillWindowId = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        TillId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TillWindowId)
                .ForeignKey("dbo.Tills", t => t.TillId, cascadeDelete: true)
                .Index(t => t.TillId);
            
            CreateTable(
                "dbo.TillAllocationTillWindows",
                c => new
                    {
                        TillAllocationTillWindowId = c.Int(nullable: false, identity: true),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        TillAllocationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TillAllocationTillWindowId)
                .ForeignKey("dbo.TillAllocations", t => t.TillAllocationId, cascadeDelete: true)
                .Index(t => t.TillAllocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TillAllocationTillWindows", "TillAllocationId", "dbo.TillAllocations");
            DropForeignKey("dbo.TillAllocations", "TillId", "dbo.Tills");
            DropForeignKey("dbo.TillWindows", "TillId", "dbo.Tills");
            DropForeignKey("dbo.Tills", "TypeId", "dbo.TillTypes");
            DropForeignKey("dbo.Tills", "StatusId", "dbo.TillStatuses");
            DropIndex("dbo.TillAllocationTillWindows", new[] { "TillAllocationId" });
            DropIndex("dbo.TillWindows", new[] { "TillId" });
            DropIndex("dbo.Tills", new[] { "TypeId" });
            DropIndex("dbo.Tills", new[] { "StatusId" });
            DropIndex("dbo.TillAllocations", new[] { "TillId" });
            DropTable("dbo.TillAllocationTillWindows");
            DropTable("dbo.TillWindows");
            DropTable("dbo.TillTypes");
            DropTable("dbo.TillStatuses");
            DropTable("dbo.Tills");
            DropTable("dbo.TillAllocations");
        }
    }
}
