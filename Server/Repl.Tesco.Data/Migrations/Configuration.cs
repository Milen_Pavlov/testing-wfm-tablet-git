using Repl.Tesco.Constants;
using Repl.Tesco.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Repl.Tesco.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<SqlDataContext>
    {
        public SqlDataContext DataContext { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SqlDataContext dataContext)
        {
            DataContext = dataContext;
            SeedTillStatuses();
            SeedTillTypes();
            DataContext.SaveChanges();
        }

        private void SeedTillStatuses()
        {
            DataContext.TillStatuses.AddOrUpdate(e => e.Key,
                new TillStatus() { Key = TillStatusKey.Open, Name = "Open", DisplayOrder = 0 },
                new TillStatus() { Key = TillStatusKey.Unused, Name = "Unused", DisplayOrder = 1 },
                new TillStatus() { Key = TillStatusKey.OutOfOrder, Name = "Out of Order", DisplayOrder = 3 });
        }

        private void SeedTillTypes()
        {
            DataContext.TillTypes.AddOrUpdate(e => e.Key,
                new TillType() { Key = TillTypeKey.MainBank, Name = "Main Bank", DisplayOrder = 0, OpeningSequence = 1 },
                new TillType() { Key = TillTypeKey.Basket, Name = "Basket", DisplayOrder = 1, OpeningSequence = 0 });
        }
    }
}
