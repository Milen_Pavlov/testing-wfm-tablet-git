namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OvertimeAgreed : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OvertimeAgreed",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Week = c.DateTime(nullable: false),
                        SiteId = c.Int(nullable: false),
                        OvertimeAgreedAmount = c.Double(nullable:true),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sites", t => t.SiteId, cascadeDelete: true)
                .Index(t => t.SiteId);
            
            CreateTable(
                "dbo.Sites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        isDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OvertimeAgreed", "SiteId", "dbo.Sites");
            DropIndex("dbo.OvertimeAgreed", new[] { "SiteId" });
            DropTable("dbo.Sites");
            DropTable("dbo.OvertimeAgreed");
        }
    }
}
