namespace Repl.Tesco.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDateTimesToOffsets : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Audits", "DateOfChange", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.OvertimeAgreed", "Week", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Resources", "Date", c => c.DateTimeOffset(precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Resources", "Date", c => c.DateTime());
            AlterColumn("dbo.OvertimeAgreed", "Week", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Audits", "DateOfChange", c => c.DateTime(nullable: false));
        }
    }
}
