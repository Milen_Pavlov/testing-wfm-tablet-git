﻿using Repl.Tesco.Helpers;
using Repl.Tesco.Interfaces;
using Repl.Tesco.Models;
using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data
{
    public class SqlDataContext : DbContext, IDisposable
    {
        public DbSet<Audit> Audits { get; set; }
        public DbSet<Break> Breaks { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<GlobalSettings> GlobalSettings { get; set; }
        public DbSet<OvertimeMapping> Overtime { get; set; }
        public DbSet<OvertimeAgreed> OvertimeAgreed { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<Till> Tills { get; set; }
        public DbSet<TillAllocation> TillAllocations { get; set; }
        public DbSet<TillStatus> TillStatuses { get; set; }
        public DbSet<TillType> TillTypes { get; set; }
        public DbSet<TillWindow> TillWindows { get; set; }
        
        public SqlDataContext()
        {
            Database.Connection.ConnectionString = ConfigHelper.SqlConnectionString.ConnectionString;
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
