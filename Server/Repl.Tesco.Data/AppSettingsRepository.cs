﻿using Repl.Tesco.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data
{
    public class AppSettingsRepository : IAppSettingsRepository
    {
        private NameValueCollection m_appconfig;

        public AppSettingsRepository(NameValueCollection appconfig)
        {
            m_appconfig = appconfig;
        }

        public string ApiPath
        {
            get { return m_appconfig.Get("ApiPath"); }
        }

        public string JDAServerRequestFormat
        {
            get { return m_appconfig.Get("JDAServer"); }
        }

        public string ScheduleDataEndpoint
        {
            get { return m_appconfig.Get("ScheduleDataEndpoint"); }
        }

        public string LoginEndpoint
        {
            get { return m_appconfig.Get("LoginEndpoint"); }
        }

        public string KeepAliveEndpoint
        {
            get { return m_appconfig.Get("KeepAliveEndpoint"); }
        }

        public string LogoutEndpoint
        {
            get { return m_appconfig.Get("LogoutEndpoint"); }
        }

        public string GetOrgUnitsEndpoint
        {
            get { return m_appconfig.Get("GetOrgUnitsEndpoint"); }
        }

        public string JDAUsername
        {
            get { return m_appconfig.Get("JDAUsername"); }
        }

        public string JDAPassword
        {
            get { return m_appconfig.Get("JDAPassword"); }
        }

        public string JdaConnectionString
        {
            get { return m_appconfig.Get("JdaConnectionString"); }
        }

        public string EmailSenderUsername
        {
            get { return m_appconfig.Get("EmailSenderUsername"); }
        }

        public string EmailSenderPassword
        {
            get { return m_appconfig.Get("EmailSenderPassword"); }
        }

        public string JdaRootPath
        {
            get { return m_appconfig.Get("JdaRootPath"); }
        }

        public string JdaHomePath
        {
            get { return m_appconfig.Get("JdaHomePath"); }
        }

        public string JdaEmployeeImportPath
        {
            get { return m_appconfig.Get("JdaEmployeeImportPath"); }
        }

        public string ChangePasswordPath
        {
            get { return m_appconfig.Get("ChangePasswordPath"); }
        }

        public string NoChangePasswordUsernames
        {
            get { return m_appconfig.Get("NoChangePasswordUsernames"); }
        }

        public int MinutesUntilChangePasswordLinkExpires
        {
            get { return int.Parse(m_appconfig.Get("MinutesUntilChangePasswordLinkExpires")); }
        }

        public string ChangePasswordEmailFrom
        {
            get { return m_appconfig.Get("ChangePasswordEmailFrom"); }
        }

        // todo: implement generic method for getting arrays
        private IList<string> _newPasswordFormats;
        public IList<string> NewPasswordFormats
        {
            get
            {
                if (_newPasswordFormats == null)
                {
                    _newPasswordFormats = new List<string>();
                    var index = 0;

                    string key, value;
                    do
                    {
                        key = string.Format("NewPasswordFormat[{0}]", index);
                        value = m_appconfig.Get(key);
                        if (value != null)
                        {
                            _newPasswordFormats.Add(value);
                            index++;
                        }
                    }
                    while (value != null);
                }
                return _newPasswordFormats;
            }
        }

        public string NewPasswordFormatError
        {
            get { return m_appconfig.Get("NewPasswordFormatError"); }
        }

        public string PasswordResetEmailSent
        {
            get { return m_appconfig.Get("PasswordResetEmailSent"); }
        }

        public string JdaUserImportCreationSource
        {
            get { return m_appconfig.Get("JdaUserImportCreationSource"); }
        }

        public bool PasswordCannotContainUsernameOrName
        {
            get { return bool.Parse(m_appconfig.Get("PasswordCannotContainUsernameOrName")); }
        }
    }
}
