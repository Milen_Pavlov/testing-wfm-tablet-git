﻿using Consortium.Service.Engine;
using Consortium.Utility.RestClient;
using Repl.Tesco.Constants;
using Repl.Tesco.Data.Interfaces;
using Repl.Tesco.Interfaces.Repositories.Jda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace Repl.Tesco.Data.Jda
{
    public class JdaSessionRepository : IJdaSessionRepository
    {
        private readonly IAppSettingsRepository m_appSettings;

        public JdaSessionRepository(IAppSettingsRepository appRepo)
        {
            m_appSettings = appRepo;
        }

        public string LogIn(string username, string password)
        {
            using (var context = new JdaDataContext())
            {
                context.Url = m_appSettings.LoginEndpoint;
                context.Method = HttpVerb.Post;
                context.ContentType = HttpContentType.FormUrlEncoded;
                context.Request = new { loginName = username, password = password };

                context.Send();
                return context.SessionId;
            }
        }

        public bool Authenticated(string sessionId)
        {
            using (var context = new JdaDataContext())
            {
                context.Url = m_appSettings.KeepAliveEndpoint;
                context.Method = HttpVerb.Post;
                context.SessionId = sessionId;

                context.Send();
                return (context.Success);
            }
        }

        public bool LogOut()
        {
            using (var context = new JdaDataContext())
            {
                context.Url = m_appSettings.LogoutEndpoint;
                context.Method = HttpVerb.Get;

                context.Send();
                return (context.Success);
            }
        }

        public bool HasServiceUser
        {
            get { return !String.IsNullOrEmpty(m_appSettings.JDAPassword) && !String.IsNullOrEmpty(m_appSettings.JDAUsername); }
        }

        public string LoginServiceUser()
        {
            return LogIn(m_appSettings.JDAUsername, m_appSettings.JDAPassword);
        }
    }
}
