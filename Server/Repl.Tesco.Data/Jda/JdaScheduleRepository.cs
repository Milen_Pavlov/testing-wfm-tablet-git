﻿using Newtonsoft.Json.Linq;
using WFM.Shared.Constants;
using Repl.Tesco.Interfaces.Repositories.Jda;
using Repl.Tesco.Models.Jda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Repl.Tesco.Data.Interfaces;
using Consortium.Service.Engine;

namespace Repl.Tesco.Data.Jda
{
    public class JdaScheduleRepository : IJdaScheduleRepository
    {
        private readonly IAppSettingsRepository m_appSettings; 

        public JdaScheduleRepository()
        {
            m_appSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();
        }

        public JdaSchedule Read(string sessionId, int siteId, DateTime startDay)
        {
            using (var context = new JdaDataContext())
            {
                context.SessionId = sessionId;
                context.Url = m_appSettings.ScheduleDataEndpoint;
                context.Method = HttpVerb.Post;

                context.Request = new
                {
                    siteID = siteId,
                    startDay = startDay,
                    filter = string.Empty,
                    filterIDs = new int[0],
                    budgetID = 1000012,
                    metricID = 1000047
                };

                context.Send();
                if (!context.Success)
                {
                    return null;
                }

                return new JdaSchedule(context.Response["data"]);
            }
        }
    }
}
