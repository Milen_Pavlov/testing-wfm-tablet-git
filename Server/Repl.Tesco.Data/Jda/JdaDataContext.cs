﻿using Consortium.Service.Engine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Repl.Tesco.Constants;
using Repl.Tesco.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.Constants;

namespace Repl.Tesco.Data.Jda
{
    public class JdaDataContext : IDisposable
    {
        public string Url { get; set; }
        public string Method { get; set; }
        public string ContentType { get; set; }
        public string SessionId { get; set; }
        public object Request { get; set; }
        public JObject Response { get; set; }
        public string Error { get; set; }

        private readonly IAppSettingsRepository m_appSettings;

        public JdaDataContext()
        {
            Method = HttpVerb.Get;
            ContentType = HttpContentType.Json;
            m_appSettings = ConsortiumApp.IOC.Resolve<IAppSettingsRepository>();
        }

        public string FullUrl
        {
            get
            {
                return string.Format(m_appSettings.JDAServerRequestFormat, Url); // ToDo: Put path in web.config
            }
        }

        public bool Success
        {
            get
            {
                return (Error == null);
            }
        }

        public void Send()
        {
            var baseAddress = new Uri(FullUrl);
            var cookieContainer = new CookieContainer();

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {

                var message = new HttpRequestMessage(new HttpMethod(Method), FullUrl);
                message.Headers.Add("Accept", ContentType);

                if (SessionId != null)
                {
                    cookieContainer.Add(baseAddress, new Cookie("REFSSessionID", SessionId));
                }

                if (Request != null)
                {
                    var json = JsonConvert.SerializeObject(Request);
                    switch (ContentType)
                    {
                        case HttpContentType.Json:
                        {
                            message.Content = new StringContent(json, Encoding.UTF8, ContentType);
                            break;
                        }
                        case HttpContentType.FormUrlEncoded:
                        {
                            var keyValues = JObject.Parse(json).Cast<JProperty>().Select(p => new KeyValuePair<string, string>(p.Name, p.Value.ToString())).ToList();
                            message.Content = new FormUrlEncodedContent(keyValues);
                            break;
                        }
                        default:
                        {
                            throw new NotSupportedException();
                        }
                    }
                }

                Response = null;
                Error = null;
                var response = client.SendAsync(message).Result;
                var content = response.Content.ReadAsStringAsync().Result;

                // ToDo: More descriptive error codes
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Error = JdaDataContextError.BadStatus;
                    return;
                }
                else if (content.Contains("<title>Redirecting...</title>")) // ToDo: Weak
                {
                    Error = JdaDataContextError.NotAuthenticated;
                    return;
                }

                Response = JObject.Parse(content);
                SessionId = cookieContainer.GetCookies(baseAddress)[0].Value;
            }
        }

        public void Dispose()
        {
        }
    }

    public static class JdaDataContextError
    {
        public const string BadStatus = "BadStatus";
        public const string NotAuthenticated = "NotAuthenticated";
    }
}
