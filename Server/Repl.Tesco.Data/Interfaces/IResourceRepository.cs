﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IResourceRepository
    {
        int Insert(Resource resource);
        Resource Get(int siteId, DateTime week);
        void RemoveAll(int siteId, DateTime week);
    }
}
