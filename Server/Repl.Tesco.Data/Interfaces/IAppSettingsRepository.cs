﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IAppSettingsRepository
    {
        string JdaServer{ get; }
        string JDAServerRequestFormat { get; }
        string ScheduleDataEndpoint { get; }
        string LoginEndpoint { get; }
        string KeepAliveEndpoint { get; }
        string LogoutEndpoint { get; }
        string GetOrgUnitsEndpoint { get; }

        string JDAUsername { get; }
        string JDAPassword { get; }
    }
}
