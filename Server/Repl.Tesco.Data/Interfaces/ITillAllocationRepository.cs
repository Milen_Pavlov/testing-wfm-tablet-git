﻿using Consortium.Model.Engine.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repl.Tesco.Models;

namespace Repl.Tesco.Data.Interfaces
{
    public interface ITillAllocationRepository
    {
        IList<TillAllocation> Read(int siteId, DateTime startTime, DateTime endTime, Pager pager);
        void Create(IList<TillAllocation> tillAllocations);
        void Delete(int siteId, DateTime startTime, DateTime endTime);
        void Move(int siteId, List<TillAllocation> allocationMoves);
    }
}
