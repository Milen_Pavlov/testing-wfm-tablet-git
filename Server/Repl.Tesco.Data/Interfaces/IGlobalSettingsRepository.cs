﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IGlobalSettingsRepository
    {
        void UpdateMaximumHours(int? maxHours, int siteId);
        GlobalSettings Get(int siteId);
    }
}
