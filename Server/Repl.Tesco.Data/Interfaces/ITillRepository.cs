﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface ITillRepository
    {
        Till ReadById(int tillId);
        IList<Till> ReadBySite(int siteId, Pager pager);
        void SaveTillAndWindows(Till till);
        void UpdateOpeningSequences(IList<Till> tills);
        void Delete(int tillId);
    }
}
