﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface ITillStatusRepository
    {
        IList<TillStatus> ReadAll(Pager pager);
    }
}
