﻿using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Overtime;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IOvertimeRepository
    {
        void SetOvertime(int forSiteId, DateTime forDate, string username, IList<OvertimeRecord> toCreate);

        IList<Department> GetOvertime(int forSiteId, DateTime forDate);
    }
}
