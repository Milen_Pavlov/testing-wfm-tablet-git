﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface ISiteRepository
    {
        Site GetById(int siteId);
        IList<Site> ReadAll(Pager pager);
        int UpdateAll(List<Site> sites);
        List<Site> CompareSites(List<Site> sites);
        void InsertSite(Site site);
    }
}
