﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IOvertimeAgreedRepository
    {
        List<OvertimeAgreed> GetOvertimeAgreedForWeek(DateTime week);
        OvertimeAgreed GetRecordForSiteAndWeek(int siteId, DateTime week);
        int InsertOrUpdate(OvertimeAgreed otA);
    }
}
