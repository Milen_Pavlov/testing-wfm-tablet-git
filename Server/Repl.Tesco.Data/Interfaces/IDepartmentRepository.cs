﻿using Repl.Tesco.Models.Overtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IDepartmentRepository
    {
        Department Get(int siteId);
    }
}
