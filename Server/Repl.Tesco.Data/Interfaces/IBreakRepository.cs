﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFM.Shared.DataTransferObjects.Breaks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IBreakRepository
    {
        void SetBreaks(IList<BreakRecord> toSet);

        IList<BreakRecord> GetBreaks(int forSiteId, List<int> forShiftIDs);
    }
}
