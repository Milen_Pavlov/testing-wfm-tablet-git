﻿using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface IAuditRepository
    {
        void Create(Audit audit);
        List<Audit> Get(DateTime date, int siteId);
    }
}
