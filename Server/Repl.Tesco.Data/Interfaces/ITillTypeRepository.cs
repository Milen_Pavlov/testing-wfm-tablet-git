﻿using Consortium.Model.Engine.Paging;
using Repl.Tesco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repl.Tesco.Data.Interfaces
{
    public interface ITillTypeRepository
    {
        TillType ReadById(int tillTypeId);
        IList<TillType> ReadAll(Pager pager);
    }
}
