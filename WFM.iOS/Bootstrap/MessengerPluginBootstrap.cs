using Cirrious.CrossCore.Plugins;

namespace WFM.iOS.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}