using Cirrious.CrossCore.Plugins;

namespace WFM.iOS.Bootstrap
{
    public class MethodBindingPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.MethodBinding.PluginLoader>
    {
    }
}