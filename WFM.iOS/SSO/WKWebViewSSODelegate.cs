using System;
using System.Diagnostics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.WebKit;
using WFM.Core;

namespace WFM.iOS
{
    public class WKWebViewSSODelegate : WKNavigationDelegate
    {
        SessionViewModel m_vm;
        int m_loadRefCount;

        public WKWebViewSSODelegate (SessionViewModel vm)
        {
            m_vm = vm;
            m_loadRefCount = 0;
        }

        public override void DecidePolicy (WKWebView webView, WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
        {
            Debug.WriteLine ("Web Load: {0}, {1}", navigationAction.Request.HttpMethod, navigationAction.Request.Url.AbsoluteString);

            if (m_vm.SSOHasToken)
                decisionHandler (WKNavigationActionPolicy.Cancel);

            var cookies = NSHttpCookieStorage.SharedStorage.CookiesForUrl (new NSUrl (m_vm.SSOAddress));
            foreach (var cookie in cookies) {
                Debug.WriteLine ("Cookie: " + cookie.Name);
                var props = new NSMutableDictionary (cookie.Properties);
                var key = NSHttpCookie.KeyExpires;
                if (!props.ContainsKey (key)) {
                    props.SetValueForKey (NSDate.FromTimeIntervalSinceNow (60.0 * 60.0 * 24.0), key);
                    props.Remove (NSHttpCookie.KeyDiscard);
                    var newCookie = NSHttpCookie.CookieFromProperties (props);
                    NSHttpCookieStorage.SharedStorage.SetCookie (newCookie);
                }
            }

            // Test for broken redirect
            var url1 = new Uri (m_vm.SSOAddress);
            var url2 = new Uri (navigationAction.Request.Url.AbsoluteString);
            var url1NoQuery = url1.AbsoluteUri.Substring (0, url1.AbsoluteUri.Length - url1.Query.Length);
            var url2NoQuery = url2.AbsoluteUri.Substring (0, url2.AbsoluteUri.Length - url2.Query.Length);

            if (url1NoQuery == url2NoQuery && url1.Query != url2.Query) {
                decisionHandler (WKNavigationActionPolicy.Cancel);
                m_vm.OnSignInFailed (SessionData.SAML_ERROR);
            }

            // Dont try to load any WFM links
            if (url2.AbsoluteUri.StartsWith (m_vm.WFMAddress, StringComparison.InvariantCultureIgnoreCase)) {
                decisionHandler (WKNavigationActionPolicy.Cancel);
            }

            decisionHandler (WKNavigationActionPolicy.Allow);
        }

        public override void DidReceiveAuthenticationChallenge (WKWebView webView, NSUrlAuthenticationChallenge challenge, Action<NSUrlSessionAuthChallengeDisposition, NSUrlCredential> completionHandler)
        {
            Debug.WriteLine ("Web Challenge: {0}, {1}, {2}", challenge.ProtectionSpace.AuthenticationMethod, challenge.ProtectionSpace.Host, challenge.ProtectionSpace.ServerSecTrust != null);

            if (challenge.ProtectionSpace.AuthenticationMethod == "NSURLAuthenticationMethodClientCertificate") {
                completionHandler (NSUrlSessionAuthChallengeDisposition.PerformDefaultHandling, null);
            } else if (challenge.ProtectionSpace.ServerSecTrust != null) {
                var cred = new NSUrlCredential (challenge.ProtectionSpace.ServerSecTrust);
                completionHandler (NSUrlSessionAuthChallengeDisposition.UseCredential, cred);
            } else {
                completionHandler (NSUrlSessionAuthChallengeDisposition.UseCredential, null);
            }
        }

        public override void DidStartProvisionalNavigation (WKWebView webView, WKNavigation navigation)
        {
            ++m_loadRefCount;
        }

        public override void DidFailProvisionalNavigation (WKWebView webView, WKNavigation navigation, NSError error)
        {
            m_vm.OnSignInFailed (string.Format ("{0} ({1})", error.LocalizedDescription, error.Code));
        }

        public override void DidFailNavigation (WKWebView webView, WKNavigation navigation, NSError error)
        {
            --m_loadRefCount;
            m_vm.OnSignInFailed (string.Format ("{0} ({1})", error.LocalizedDescription, error.Code));
        }

        public override void DidFinishNavigation (WKWebView webView, WKNavigation navigation)
        {
            --m_loadRefCount;

            // Check for valid token
            var js = "document.body.getElementsByTagName('input')[0].getAttribute('name') + '=' + document.body.getElementsByTagName('input')[0].getAttribute('value')";
            webView.EvaluateJavaScript (js, (r, e) => { 
                if (e == null) m_vm.CheckForToken (r.ToString ()); 
            });

            // Show view when we go idle
            if (!m_vm.SSOHasToken && m_loadRefCount == 0) {
                webView.Alpha = 0;
                webView.Hidden = false;
                UIView.Animate (
                    duration: 1f,
                    delay: 2f,
                    options: UIViewAnimationOptions.CurveEaseInOut,
                    animation: () => { webView.Alpha = 1; },
                    completion: () => { }
                );
            }
        }
    }
}
