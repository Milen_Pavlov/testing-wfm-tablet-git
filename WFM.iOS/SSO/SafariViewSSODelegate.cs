using System;
using System.Diagnostics;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.SafariServices;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public class SafariViewSSODelegate : SFSafariViewControllerDelegate
    {
        SessionViewModel m_vm;
        IMvxMessenger m_messenger;
        MvxSubscriptionToken m_messsageToken;

        public SafariViewSSODelegate (SessionViewModel vm)
        {
            m_vm = vm;
            m_messenger = Mvx.Resolve<IMvxMessenger> ();
        }

        public override void DidCompleteInitialLoad (SFSafariViewController controller, bool didLoadSuccessfully)
        {
            Debug.WriteLine ("DidCompleteInitialLoad: {0}", didLoadSuccessfully);
            m_messsageToken = m_messenger.SubscribeOnMainThread<OpenUrlMessage> (OnOpenUrl);
        }

        public override void DidFinish (SFSafariViewController controller)
        {
            Debug.WriteLine ("DidFinish");
            if (m_messsageToken != null) {
                m_messenger.Unsubscribe<OpenUrlMessage> (m_messsageToken);
                m_messsageToken = null;
            }
            m_vm.OnSSOClose ();
        }

        public override UIActivity [] GetActivityItems (SFSafariViewController controller, NSUrl url, string title)
        {
            Debug.WriteLine ("GetActivityItems");
            return new UIActivity[0];
        }

        private void OnOpenUrl (OpenUrlMessage msg)
        {
            Debug.WriteLine ("OnOpenUrl: " + msg.URL.AbsoluteString);
            if (m_messsageToken != null) {
                m_messenger.Unsubscribe<OpenUrlMessage> (m_messsageToken);
                m_messsageToken = null;
            }

            var components = new NSUrlComponents (msg.URL.AbsoluteString);

            if (string.Equals(components.Host, "SSOLogin", StringComparison.InvariantCultureIgnoreCase)) {
                var code = components.QueryItems?.FirstOrDefault (x => string.Equals (x.Name, "code", StringComparison.InvariantCultureIgnoreCase));

                if (code == null) {
                    m_vm.OnSignInFailed ("Redirect was missing login code");
                } else {
                    Debug.WriteLine ("CODE=" + code.Value);
                }
            }
        }
    }
}
