﻿using System;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public interface ISSOLogin
    {
        void ClearCookies ();
        void Show (SessionView session);
        void Hide ();
    }
}

