﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.WebKit;

namespace WFM.iOS
{
    public class WKWebViewSSOLogin : ISSOLogin
    {
        WKWebView m_webView;

        public void ClearCookies ()
        {
            var storage = NSHttpCookieStorage.SharedStorage;
            foreach (var cookie in storage.Cookies)
                storage.DeleteCookie (cookie);
            NSUserDefaults.StandardUserDefaults.Synchronize ();

            var dataStore = WKWebsiteDataStore.DefaultDataStore;
            dataStore.RemoveDataOfTypes (WKWebsiteDataStore.AllWebsiteDataTypes, DateTime.MinValue, () => { });
        }

        public void Show (SessionView session)
        {
            if (m_webView == null) {
                m_webView = new WKWebView (new RectangleF (0, 20, session.View.Frame.Width, session.View.Frame.Height - 20), new WKWebViewConfiguration () {
                    MediaPlaybackAllowsAirPlay = false
                });

                m_webView.AutoresizingMask = UIViewAutoresizing.All;
                m_webView.TranslatesAutoresizingMaskIntoConstraints = true;
                m_webView.Alpha = 0;
                m_webView.Opaque = false;
                m_webView.Hidden = true;
                m_webView.BackgroundColor = UIColor.White;
                m_webView.CustomUserAgent = "Mozilla/5.0 (iPad; CPU OS 9_3 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13E188a Safari/601.1";

                var close = new UIButton (new RectangleF (10, 10, 32, 32));
                close.SetBackgroundImage (UIImage.FromBundle ("icon_notification_denied_grey.png"), UIControlState.Normal);
                close.TouchUpInside += (object sender, EventArgs e) => {
                    session.VM.OnSSOClose ();
                };

                m_webView.AddSubview (close);
                session.View.AddSubview (m_webView);
            }

            if (m_webView.NavigationDelegate == null) {
                m_webView.NavigationDelegate = new WKWebViewSSODelegate (session.VM);
                m_webView.LoadRequest (new NSUrlRequest (new NSUrl (session.VM.SSOAddress), NSUrlRequestCachePolicy.UseProtocolCachePolicy, 20));
            }
        }

        public void Hide ()
        {
            if (m_webView == null)
                return;
            
            m_webView.NavigationDelegate = null;
            m_webView.StopLoading ();
            m_webView.EvaluateJavaScript ("document.activeElement.blur();", (x,y) => { });

            UIView.Animate (
                duration: 0.5f,
                delay: 0f,
                options: UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.BeginFromCurrentState,
                animation: () => { m_webView.Alpha = 0; },
                completion: () => { m_webView.Hidden = true; }
            );
        }
    }
}
