﻿using MonoTouch.Foundation;
using MonoTouch.SafariServices;

namespace WFM.iOS
{
    public class SafariViewSSOLogin : ISSOLogin
    {
        SFSafariViewController m_safariView;

        public void ClearCookies ()
        {
        }

        public void Show (SessionView session)
        {
            if (m_safariView != null)
                return;

            if (session.IsViewLoaded && session.View.Window != null) {
                m_safariView = new SFSafariViewController (new NSUrl (session.VM.SSOAddress));
                m_safariView.Delegate = new SafariViewSSODelegate (session.VM);
                session.PresentViewController (m_safariView, true, () => { });
            }
        }

        public void Hide ()
        {
            if (m_safariView != null) {
                m_safariView.DismissViewController (true, () => { });
                m_safariView.Delegate = null;
                m_safariView = null;
            }
        }
    }
}

