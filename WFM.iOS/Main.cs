using System;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public class Application
    {
        static void Main(string[] args)
        {
            UIApplication.Main(args, "ConsortiumApp", "ConsortiumAppDelegate");
        }
    }
}
 