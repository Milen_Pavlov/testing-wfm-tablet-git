﻿using System;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    static public class Constants
    {
        public static UIColor VirginRed   = UIColor.FromRGB(235,41,47);

        public static UIColor TescoBlue = UIColor.FromRGB(0,83,159);
        public static UIColor TescoRed = UIColor.FromRGB(238,28,46);
    }
}
