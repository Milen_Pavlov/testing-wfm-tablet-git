﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ShiftTypeView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ShiftView); }
        }

        public ShiftTypeView()
            : base (IsPhone ? "ShiftTypeView_iPhone" : "ShiftTypeView_iPad", null)
        {
            Title = "Select Role";
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            TypeTable.RegisterCell<TypeSelectionCell> ();

            // View Model Binding
            var set = this.CreateBindingSet<ShiftTypeView, ShiftTypeViewModel>();
            set.Bind (TypeTable).To (m => m.Types);
            set.Bind (TypeTable).For(b => b.Selected).To ("OnSelected");
            set.Apply();
        }
    }
}

