using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using System.Collections.Generic;
using MonoTouch.Foundation;
using System.Drawing;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public partial class EditEnvironmentView : ConsortiumView
    {
        private RectangleF m_onScreenPosition;
        private RectangleF m_offScreenPosition;

        private float m_bottomBorder = 10;

        public override Type AttachTarget
        {
            get
            {
                return typeof (SettingsView);
            }
        }

        public bool Closing
        {
            get { return false; }
            set
            {
//                if (value)
//                {
//                    UIView.Animate(PickerViewModel.ANIM_TIME, 0, UIViewAnimationOptions.BeginFromCurrentState | UIViewAnimationOptions.CurveEaseInOut,
//                        animation: () =>
//                        {
//                            View.Frame = m_offScreenPosition;
//                        },
//                        completion: () =>
//                        {
//                        });
//                }
            }
        }

        public bool Reset
        {
            get { return false; }

            set
            {
                //Wipe text if needs be
                EditEnvironmentViewModel vm = DataContext as EditEnvironmentViewModel;
                string s = vm.GetTextValue();
                if (string.IsNullOrEmpty(s))
                {
                    EditTextView.Text = vm.CurrentTextHint;
                    EditTextView.TextColor = UIColor.LightGray;
                }
                else
                {
                    EditTextView.Text = s;
                    EditTextView.TextColor = UIColor.Black;
                }
            }
        }

        public string CurrentButtonTitle
        {
            get { return string.Empty; }
            set
            {
                if (DoneButton != null)
                {
                    DoneButton.SetTitle(value, UIControlState.Normal);
                    DoneButton.SetTitle(value, UIControlState.Disabled);
                    DoneButton.SetTitle(value, UIControlState.Selected);
                 }
            }
        }

        private EditEnvironmentViewModel.EditStage m_currentStage;
        public EditEnvironmentViewModel.EditStage CurrentStage
        {
            get { return m_currentStage; }
            set
            {
                m_currentStage = value;

                EditTextView.Hidden = m_currentStage != EditEnvironmentViewModel.EditStage.SetName && m_currentStage != EditEnvironmentViewModel.EditStage.SetAddress;

                //Dismiss keyboard
                if (m_currentStage == EditEnvironmentViewModel.EditStage.Done ||
                   m_currentStage == EditEnvironmentViewModel.EditStage.PingAddress)
                    View.EndEditing(true);
            }
        }

        public EditEnvironmentView()
            : base("EditEnvironmentView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.Layer.CornerRadius = 10;

            DoneButton.SetTitle("Next", UIControlState.Normal);
            ClearButton.SetTitle("Cancel", UIControlState.Normal);

            EditTextView.Text = "Set Name";
            EditTextView.TextColor = UIColor.LightGray;
            EditTextView.Layer.BorderColor = new UIColor(0,0,0,0.25f).CGColor;
            EditTextView.Layer.BorderWidth = 0.5f;
            EditTextView.Layer.CornerRadius = 2;

            EditEnvironmentViewModel vm = DataContext as EditEnvironmentViewModel;
            if (vm != null)
                EditTextView.Text = vm.CurrentTextHint;

            // View Model Binding
            var set = this.CreateBindingSet<EditEnvironmentView, EditEnvironmentViewModel>();
            set.Bind(this).For(v => v.Closing).To(m => m.Closing);
            set.Bind(this).For(v => v.Reset).To(m => m.Reset);
            set.Bind(this).For(v => v.CurrentButtonTitle).To(m => m.CurrentButtonTitle);
            set.Bind(this).For(v => v.CurrentStage).To(m => m.CurrentStage);
            set.Bind(TitleLabel).To(m => m.CurrentTitle);
            set.Bind(DoneButton).To("OnNext");
            set.Bind(ClearButton).To("OnCancel");
            set.Bind(DoneButton).For(v => v.Enabled).To(m => m.NextEnabled);
            set.Bind(Spinner).For(v => v.Hidden).To(m => m.CheckingAddress).WithConversion("InvertedBoolean");
            set.Apply();
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);

            EditTextView.EndEditing(true);
        }

//        public override void ViewDidLayoutSubviews()
//        {
//            base.ViewDidLayoutSubviews();
//
//            //Position at base of window
//            RectangleF window = View.Window.Bounds;
//            View.Frame = new RectangleF(View.Frame.X, window.Height - View.Frame.Height + m_bottomBorder, View.Frame.Width, View.Frame.Height);
//
//            m_onScreenPosition = View.Frame;
//            m_offScreenPosition = View.Frame;
//            m_offScreenPosition.Y += View.Frame.Height;
//
//            //DidLayout happens AFTER DidAppear in 7.X YAY APPLE
//            if(!UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
//                OverwriteOriginalRootFrame(m_onScreenPosition);
//
//            View.Frame = m_offScreenPosition;
//            UIView.Animate(PickerViewModel.ANIM_TIME, 0, UIViewAnimationOptions.BeginFromCurrentState | UIViewAnimationOptions.CurveEaseInOut,
//                animation: () =>
//                {
//                    View.Frame = m_onScreenPosition;
//                },
//                completion: () =>
//                {
//                });
//        }

//        public override void ViewDidAppear(bool animated)
//        {
//            base.ViewDidAppear(animated);
//
//            //DidAppear happens AFTER DidLayout from 8.X onwards, YAY APPLE
//            if(UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
//                OverwriteOriginalRootFrame(m_onScreenPosition);
//        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            EditTextView.Changed += OnTextChanged;
            EditTextView.Started += OnTextEditStart;
            EditTextView.Ended += OnTextEditEnded;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            EditTextView.Changed -= OnTextChanged;
            EditTextView.Started -= OnTextEditStart;
            EditTextView.Ended -= OnTextEditEnded;
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            EditEnvironmentViewModel vm = DataContext as EditEnvironmentViewModel;
            if (vm != null)
                vm.SetTextValue(EditTextView.Text);
        }

        private void OnTextEditStart(object sender, EventArgs e)
        {
            EditEnvironmentViewModel vm = DataContext as EditEnvironmentViewModel;
            //Wipe text if needs be
            if (string.IsNullOrEmpty(vm.GetTextValue()))
            {
                EditTextView.Text = string.Empty;
                EditTextView.TextColor = UIColor.Black;
            }
        }

        private void OnTextEditEnded(object sender, EventArgs e)
        {
            EditEnvironmentViewModel vm = DataContext as EditEnvironmentViewModel;
            //Wipe text if needs be
            if (string.IsNullOrEmpty(vm.GetTextValue()))
            {
                EditTextView.Text = vm.CurrentTextHint;
                EditTextView.TextColor = UIColor.LightGray;
            }
        }     
    }
}
