﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using Consortium.Client.Core;
using WFM.Core;
using MonoTouch.Foundation;
using Cirrious.CrossCore;
using System.Drawing;

namespace WFM.iOS
{
    public partial class DrawerView : ConsortiumDrawerView
    {
        private float m_orgPosition;

        //Keyboard notification and shifting work, eventually should go in ConsortiumView
        private NSObject m_keyboardShowObserver;
        private NSObject m_keyboardHideObserver;
        private RectangleF m_originalRootFrame;
        private RectangleF m_desiredRootFrame;

        public DrawerView()
            : base ("DrawerView", null)
        {
            Title = "WFM";
            DrawerWidth = 260;
        }

        public int CurrentDrawerIndex
        {
            get { return 0; }
            set
            {
                DrawerList.SelectRow(NSIndexPath.FromRowSection(value,0), false, UITableViewScrollPosition.None);
            }
        }

        private bool m_drawerOpenState;
        public bool DrawerOpenState
        {
            get { return this.IsDrawerOpen; }
            set 
            { 
                if (value)
                    DrawerOpen ();
                else
                    DrawerClose (); 
            }
        }
            
        public bool m_showOrgUnits;
        public bool ShowOrgUnits
        {
            get { return m_showOrgUnits; }
            set 
            {
                m_showOrgUnits = value;
                MoveOrgSelection (m_showOrgUnits ? 0 : m_orgPosition, 0.2f); 
                if (m_showOrgUnits == false)
                {
                    SearchText.ResignFirstResponder ();
                }
            }
        }
            
        public override void DrawerOpen()
        {
            if(m_drawerOpenState != true)
            {
                m_drawerOpenState = true;
                MoveOrgSelection (m_orgPosition, 0.01f);
            }
            base.DrawerOpen ();
        }

        public override void DrawerClose ()
        {
            base.DrawerClose ();

            if(m_drawerOpenState != false)
            {
                m_drawerOpenState = false;
                if(SearchText != null && SearchText.IsFirstResponder)
                    SearchText.ResignFirstResponder ();
            }
        }

        private void MoveOrgSelection(float target, float length)
        {
            UIView.Animate(
                duration: length, delay: 0, 
                options: UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.BeginFromCurrentState,
                animation: () =>
                {
                    var frame = OrgSelectView.Frame;
                    frame.X = target;
                    OrgSelectView.Frame = frame;
                },
                completion: () => 
                {
                    
                }
            );
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            IStringService localiser = Mvx.Resolve<IStringService> ();

            LogoImage.ContentMode = UIViewContentMode.ScaleAspectFit;
            LogoImage.BackgroundColor = UIColor.Clear;

            // Register the cell to use for this collection
            DrawerList.RegisterCell<DrawerCell>();
            OrgSelectList.RegisterCell<OrgCell>();

            // Save the org start X position
            m_orgPosition = OrgSelectView.Frame.X;

            SignOutButton.SetTitle (localiser.Get ("sign_out_button"), UIControlState.Normal);

            // View Model Binding
            var set = this.CreateBindingSet<DrawerView, DrawerViewModel>();
            set.Bind(SiteLabel).To(m => m.Store);
            set.Bind(DrawerList).To(m => m.MenuItems);
            set.Bind(DrawerList).For(b => b.Selected).To("OnSelectMenu");
            set.Bind(OrgSelectList).To(m => m.OrgUnits);
            set.Bind(OrgSelectList).For(b => b.Selected).To("OnSelectOrg");
            set.Bind(OrgSelectBack).To("OnCancelOrg");
            set.Bind(this).For(b => b.DrawerOpenState).To(m => m.DrawerOpen);
            set.Bind(this).For(b => b.ShowOrgUnits).To(m => m.ShowOrgUnits);
            set.Bind (this).For (v => v.CurrentDrawerIndex).To (vm => vm.CurrentDrawerIndex);
            set.Bind(SignOutButton).To("OnSignOut");
            set.Bind(LogoImage).For(b => b.Uri).To(m => m.Logo);
            set.Bind(LogoBackground).For(b => b.BackgroundColor).To(m => m.Theme.LoginBarColor).WithConversion (new HexColorCoverter (), null);
            set.Bind(SignOutButton).For(b => b.BackgroundColor).To(m => m.Theme.SignOutColor).WithConversion (new HexColorCoverter (), null);
            set.Bind(OrgSelectBack).For(b => b.TintColor).To(m => m.Theme.HighlightColor).WithConversion (new HexColorCoverter (), null);
            set.Bind(StatusBarView).For(b => b.BackgroundColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind(DrawerButton).For(v => v.NotificationCount).To(vm => vm.DrawerIconNotificationCount);
            set.Bind (SearchText).For (v => v.Text).To (vm => vm.SearchText).TwoWay ();
            set.Bind (SearchText).For (v => v.Placeholder).To (vm => vm.SearchPlaceholderText);
            set.Apply();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);

            if (m_originalRootFrame == RectangleF.Empty)
            {
                m_originalRootFrame = OrgSelectView.Frame;
                m_originalRootFrame.X = 0.0f;
            }

            m_keyboardShowObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, OnKeyboardNotification);
            m_keyboardHideObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, OnKeyboardNotification);
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);

            NSNotificationCenter.DefaultCenter.RemoveObserver(m_keyboardShowObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver(m_keyboardHideObserver);
        }

        private void OnKeyboardNotification(NSNotification notification)
        {
            var showing = notification.Name == UIKeyboard.WillShowNotification;
            var keyboardFrame = showing ? UIKeyboard.FrameEndFromNotification(notification) : UIKeyboard.FrameBeginFromNotification(notification);

            //Get First responder
            UIView firstResponder = OrgSelectView;// view;// GetFirstResponder(View);
            RectangleF frame = RectangleF.Empty;
            if (firstResponder != null)
            {
                frame = View.ConvertRectFromView (new RectangleF (0, 0, firstResponder.Frame.Width, firstResponder.Frame.Height), firstResponder);
            }

            //Check if the base of the Sign in button will be under the keyboard, and by how much
            m_desiredRootFrame = m_originalRootFrame;
            m_desiredRootFrame.X = m_showOrgUnits ? 0.0f : m_originalRootFrame.Width;
            
            if (showing)
            {
                float bottomOfArea = frame.Bottom;
                float keyboardHeight = keyboardFrame.Height;
                m_desiredRootFrame.Height -= keyboardHeight - (View.Frame.Height - bottomOfArea);
            }

            NSNumber duration = (NSNumber)notification.UserInfo.ValueForKey(UIKeyboard.AnimationDurationUserInfoKey);
            NSNumber curve = (NSNumber)notification.UserInfo.ObjectForKey(UIKeyboard.AnimationCurveUserInfoKey);

            double durationVal = (duration.DoubleValue == 0) ? 0.25 : duration.DoubleValue;
            UIViewAnimationOptions curveVal = (curve.Int32Value == 0) ? UIViewAnimationOptions.CurveEaseInOut : (UIViewAnimationOptions) curve.Int32Value;
            UIView.Animate(durationVal, 0, curveVal | UIViewAnimationOptions.BeginFromCurrentState, animation: () =>
                {
                    OrgSelectView.Frame = m_desiredRootFrame;
                }, completion: () =>
                {
                });
        }

        //Used by keyboard notification event to get the correct textview to shift in view
        private UIView GetFirstResponder(UIView root)
        {
            if (root.IsFirstResponder)
                return root;

            foreach (UIView subView in root.Subviews)
            {
                UIView view = GetFirstResponder (subView);
                if (view != null)
                    return view;
            }

            return null;
        }
    }
}
