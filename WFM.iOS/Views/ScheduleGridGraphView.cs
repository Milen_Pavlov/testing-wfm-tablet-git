﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleGridGraphView : ConsortiumView, IConsortiumViewTarget
    {
        ConsortiumView m_topView;
        ConsortiumView m_bottomView;

        public ScheduleGridGraphViewModel VM
        {
            get
            {
                return (ScheduleGridGraphViewModel)ViewModel;
            }
        }

        public override Type AttachTarget
        {
            get
            {
                return typeof (ConsortiumTabView);
            }
        }

        //Trying to get the correct height as current view doesn't appear to be correct height on ipad
        public RectangleF ViewBounds
        {
            get
            {
                return this.View.Superview.Frame;
            }
        }

        ScheduleGridGraphViewModel.ScreenSizeState? m_cachedState = null;

        public ScheduleGridGraphViewModel.ScreenSizeState ScreenState
        {
            get
            {
                return ScheduleGridGraphViewModel.ScreenSizeState.Split;
            }

            set
            {
                if (this.View.Superview != null)
                {
                    if (m_cachedState == null)
                    {
                        m_cachedState = value;

                        switch (value)
                        {        
                            case ScheduleGridGraphViewModel.ScreenSizeState.BottomMax:
                                ShowFullBottom ();
                                break;
                            case ScheduleGridGraphViewModel.ScreenSizeState.Split:
                                ShowSplit ();
                                break;
                            case ScheduleGridGraphViewModel.ScreenSizeState.TopMax:
                            default:
                                ShowFullTop ();
                                break;
                        }
                    }
                    else
                    {
                        m_cachedState = value;

                        switch (value)
                        {                    

                            case ScheduleGridGraphViewModel.ScreenSizeState.BottomMax:
                                AnimateToFullBottom ();
                                break;
                            case ScheduleGridGraphViewModel.ScreenSizeState.Split:
                                AnimateToSplit ();
                                break;
                            case ScheduleGridGraphViewModel.ScreenSizeState.TopMax:
                            default:
                                AnimateToFullTop ();
                                break;
                        }
                    }
                }
            }
        }


        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public ScheduleGridGraphView ()
            : base (UserInterfaceIdiomIsPhone ? "ScheduleGridGraphView_iPhone" : "ScheduleGridGraphView_iPad", null)
        {
        }

        public bool Attach(IConsortiumViewSource source, ConsortiumViewRequest request)
        {
            if (source != null && source is ConsortiumView)
            {                
                if (source is ScheduleGridView)
                {
                    if (m_topView != null)
                    {
                        m_topView.ViewState = ConsortiumViewState.Closing;
                        m_topView.View.RemoveFromSuperview ();
                        m_topView.RemoveFromParentViewController ();
                    }

                    m_topView = source as ConsortiumView;
                    m_topView.SetNavigationItem(NavigationItem); //< Re-route navbar
                    AddChildViewController (m_topView);
                    this.TopContainer.AddSubview (m_topView.View);
                    m_topView.View.Frame = this.TopContainer.Bounds;
                    m_topView.ViewOpened ();
                    return true;
                }
                else if (source is ScheduleGraphView || source is ScheduleReportView)
                {
                    if (m_bottomView != null)
                    {
                        m_bottomView.ViewState = ConsortiumViewState.Closing;
                        m_bottomView.View.RemoveFromSuperview ();
                        m_bottomView.RemoveFromParentViewController ();
                    }

                    m_bottomView = source as ConsortiumView;
                    m_bottomView.SetNavigationItem(NavigationItem); //< Re-route navbar
                    AddChildViewController (m_bottomView);
                    this.BottomContainer.AddSubview (m_bottomView.View);
                    m_bottomView.View.Frame = this.BottomContainer.Bounds;
                    m_bottomView.ViewOpened ();
                    return true;
                }
            }

            return false;
        }

        public bool Detach(IConsortiumViewSource source)
        {
            if (m_topView != null && m_topView == source)
            { 
                m_topView.ViewState = ConsortiumViewState.Closing;
                m_topView.View.RemoveFromSuperview ();
                m_topView.RemoveFromParentViewController ();
                return true;
            }
            else if (m_bottomView != null && m_bottomView == source)
            {
                m_bottomView.ViewState = ConsortiumViewState.Closing;
                m_bottomView.View.RemoveFromSuperview ();
                m_bottomView.RemoveFromParentViewController ();
                return true;
            }

            return false;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            //Apply theming
            var settings = Mvx.Resolve<SettingsService> ();
            UIColor secondary = ColorUtility.FromHexString (settings.AppTheme.SecondaryColor);
            UpButton.TintColor = DownButton.TintColor = ModeButton.TintColor = GraphModeButton.TintColor = secondary;

            var set = this.CreateBindingSet<ScheduleGridGraphView, ScheduleGridGraphViewModel>();

            set.Bind (this).For (v => v.ScreenState).To (vm => vm.ScreenState);

            set.Bind (DownButton).To ("DecreasePanel");
            set.Bind (UpButton).To ("IncreasePanel");
            set.Bind (ModeButton).To ("SwitchToTable");
            set.Bind (GraphModeButton).To ("SwitchToGraph");

            set.Bind (ModeButton).For (c => c.Enabled).To (vm => vm.TableShown).WithConversion (new InvertedBooleanConverter ());
            set.Bind (GraphModeButton).For (c => c.Enabled).To (vm => vm.GraphShown).WithConversion (new InvertedBooleanConverter ());

            set.Bind (UpButton).For (c => c.Enabled).To (vm => vm.CanIncreaseSize);
            set.Bind (DownButton).For (c =>  c.Enabled).To (vm => vm.CanDecreaseSize);

            set.Apply();
        }

        void AnimateToFullTop()
        {
            UIView.Animate (0.2, 0, UIViewAnimationOptions.LayoutSubviews, ShowFullTop, null);
        }

        void AnimateToSplit()
        {
            UIView.Animate (0.2, 0, UIViewAnimationOptions.LayoutSubviews, ShowSplit, null);
        }

        void AnimateToFullBottom()
        {
            UIView.Animate (0.2, 0, UIViewAnimationOptions.LayoutSubviews, ShowFullBottom, null);
        }

        void ShowFullBottom ()
        {
            if (ControlSeperator != null)
            {
                RectangleF topNewFrame = new RectangleF (0,-TopContainer.Frame.Height, TopContainer.Frame.Width, TopContainer.Frame.Height);
                RectangleF seperatorNewFrame = new RectangleF (0,topNewFrame.Bottom, ControlSeperator.Frame.Width, ControlSeperator.Frame.Height);
                RectangleF bottomNewFrame = new RectangleF (0, seperatorNewFrame.Bottom, BottomContainer.Frame.Width, ViewBounds.Height - seperatorNewFrame.Bottom);

                TopContainer.Frame = topNewFrame;
                ControlSeperator.Frame = seperatorNewFrame;
                BottomContainer.Frame = bottomNewFrame;

                UpButton.Enabled = false;
                DownButton.Enabled = true;
            }
        }

        void ShowFullTop ()
        {
            if (ControlSeperator != null)
            {
                RectangleF topNewFrame = new RectangleF (0, 0, TopContainer.Frame.Width, ViewBounds.Height - ControlSeperator.Frame.Height);
                RectangleF seperatorNewFrame = new RectangleF (0, topNewFrame.Bottom, ControlSeperator.Frame.Width, ControlSeperator.Frame.Height);
                RectangleF bottomNewFrame = new RectangleF (0, seperatorNewFrame.Bottom, BottomContainer.Frame.Width, BottomContainer.Frame.Height);

                TopContainer.Frame = topNewFrame;
                ControlSeperator.Frame = seperatorNewFrame;
                BottomContainer.Frame = bottomNewFrame;
            }
        }

        void ShowSplit ()
        {
            if (ControlSeperator != null)
            {
                float availableSpace = ViewBounds.Height - ControlSeperator.Frame.Height;

                int top = (int)(availableSpace / 2);

                availableSpace -= top;

                RectangleF topNewFrame = new RectangleF (0, 0, TopContainer.Frame.Width, top);

                RectangleF seperatorNewFrame = new RectangleF (0, topNewFrame.Bottom, ControlSeperator.Frame.Width, ControlSeperator.Frame.Height);

                RectangleF bottomNewFrame = new RectangleF (0, seperatorNewFrame.Bottom, BottomContainer.Frame.Width, availableSpace);

                TopContainer.Frame = topNewFrame;
                ControlSeperator.Frame = seperatorNewFrame;
                BottomContainer.Frame = bottomNewFrame;

                TopContainer.SetNeedsLayout ();
            }
        }

        void RestoreViewState ()
        {
            this.View.Frame = this.View.Superview.Bounds;
            this.ScreenState = VM.ScreenState;

            ModeButton.Enabled = !VM.TableShown;
            GraphModeButton.Enabled = !VM.GraphShown;
        }

        public override void ViewDidAppear (bool animated)
        {
            RestoreViewState ();
            base.ViewDidAppear (animated);
        }

        public override bool Present ()
        {
            var value = base.Present ();

            if (this.View.Superview != null)
            {
                RestoreViewState ();
            }

            return value;
        }
    }
}