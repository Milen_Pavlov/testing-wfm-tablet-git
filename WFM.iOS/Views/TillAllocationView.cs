﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class TillAllocationView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(DrawerView); }
        }

        public TillAllocationView()
            : base (IsPhone ? "TillAllocationView_iPhone" : "TillAllocationView_iPad", null)
        {
            Title = "Till Allocation";
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            // View Model Binding
            var set = this.CreateBindingSet<TillAllocationView, RootViewModel>();
            set.Apply();
        }
    }
}

