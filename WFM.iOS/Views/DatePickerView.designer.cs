// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("DatePickerView")]
	partial class DatePickerView
	{
		[Outlet]
		Consortium.Client.iOS.TableControl DatePicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FifthDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FirstDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FourthDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SecondDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SeventhDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SixthDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ThirdDay { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DatePicker != null) {
				DatePicker.Dispose ();
				DatePicker = null;
			}

			if (FirstDay != null) {
				FirstDay.Dispose ();
				FirstDay = null;
			}

			if (SecondDay != null) {
				SecondDay.Dispose ();
				SecondDay = null;
			}

			if (ThirdDay != null) {
				ThirdDay.Dispose ();
				ThirdDay = null;
			}

			if (FourthDay != null) {
				FourthDay.Dispose ();
				FourthDay = null;
			}

			if (FifthDay != null) {
				FifthDay.Dispose ();
				FifthDay = null;
			}

			if (SixthDay != null) {
				SixthDay.Dispose ();
				SixthDay = null;
			}

			if (SeventhDay != null) {
				SeventhDay.Dispose ();
				SeventhDay = null;
			}
		}
	}
}
