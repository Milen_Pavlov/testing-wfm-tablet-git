﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using System.Windows.Input;

namespace WFM.iOS
{
    public partial class WorkgroupView : ConsortiumPopoverView
    {
        private UIBarButtonItem m_applyButton;

        private ICommand ApplyCommand;

        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public WorkgroupView ()
            : base (UserInterfaceIdiomIsPhone ? "WorkgroupView_iPhone" : "WorkgroupView_iPad", null)
        {
            ArrowDirection = UIPopoverArrowDirection.Any;
            Title = "Select Group";
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            this.GroupItems.AllowsSelection = true;
            this.GroupItems.AllowsMultipleSelection = true;

            this.SetupNavButtons ();

            GroupItems.RegisterCell<WorkgroupViewCell>();

            var set = this.CreateBindingSet<WorkgroupView, WorkgroupViewModel>();
            set.Bind (GroupItems).To (m => m.Groups);
            set.Bind (GroupItems).For (b => b.Selected).To ("OnSelectGroup");
            set.Bind (ApplyCommand).To("Apply");
            set.Apply();
        }

        private void SetupNavButtons()
        {
            this.NavigationItem.SetLeftBarButtonItems(new UIBarButtonItem[] 
            {
                new UIBarButtonItem ("Select All",UIBarButtonItemStyle.Plain,(sender,args) => 
                {
                     ((WorkgroupViewModel)ViewModel).SelectAll();
                }),
                new UIBarButtonItem ("Clear",UIBarButtonItemStyle.Plain,(sender,args) => 
                {
                    ((WorkgroupViewModel)ViewModel).ClearSelection();
                })}, 
            false);


            this.NavigationItem.SetRightBarButtonItem (new UIBarButtonItem ("Apply",UIBarButtonItemStyle.Plain,(sender,args) => 
                {
                    ((WorkgroupViewModel)ViewModel).Apply();
                }), false);
        }
    }
}

