// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("WeeklyOverviewExceptionsView")]
	partial class WeeklyOverviewExceptionsView
	{
		[Outlet]
		WFM.iOS.DailyOverviewTable Data { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmptyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] HeaderSeparators { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingActivity { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingRootView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EmployeeNameLabel != null) {
				EmployeeNameLabel.Dispose ();
				EmployeeNameLabel = null;
			}

			if (Data != null) {
				Data.Dispose ();
				Data = null;
			}

			if (EmptyLabel != null) {
				EmptyLabel.Dispose ();
				EmptyLabel = null;
			}

			if (HeaderRoot != null) {
				HeaderRoot.Dispose ();
				HeaderRoot = null;
			}

			if (LoadingActivity != null) {
				LoadingActivity.Dispose ();
				LoadingActivity = null;
			}

			if (LoadingLabel != null) {
				LoadingLabel.Dispose ();
				LoadingLabel = null;
			}

			if (LoadingRootView != null) {
				LoadingRootView.Dispose ();
				LoadingRootView = null;
			}
		}
	}
}
