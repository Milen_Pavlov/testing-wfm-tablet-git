﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using MonoTouch.ObjCRuntime;

namespace WFM.iOS
{
    public partial class WeeklyOverviewTimecardView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(WeeklyOverviewView); } 
        }

        private UIColor m_buttonColor;
        public UIColor ButtonColor
        {
            get { return m_buttonColor; } 
            set
            {
                m_buttonColor = value;

                foreach (var separator in HeaderSeparators)
                    separator.BackgroundColor = UIColor.LightGray;

                foreach (var separator in FooterSeparators)
                    separator.BackgroundColor = UIColor.LightGray;

                HeaderRoot.BackgroundColor = value;
                FooterRoot.BackgroundColor = value;
            }
        }

        public string EmployeeNameTitle
        {
            get { return string.Empty; }
            set { EmployeeNameLabel.Text = value; }
        }

        public WeeklyOverviewTimecardView ()
            : base (IsPhone ? "WeeklyOverviewTimecardView_iPhone" : "WeeklyOverviewTimecardView_iPad", null)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            // ios7 layout
            if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            TimecardSummaryTable.RegisterCell<TimecardSummaryCell> ();
            TimecardSummaryTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TimecardSummaryTable.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<WeeklyOverviewTimecardView, WeeklyOverviewTimecardViewModel>();
            set.Bind (LoadingRoot).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingSpinner).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingLabel).To (m => m.LoadingMessage);
            set.Bind (EmptyLabel).To (m => m.EmptyMessage);
            set.Bind (EmptyLabel).For (v => v.Hidden).To (vm => vm.EmptyMessage).WithConversion (new StringVisibleConverter (), null);
            set.Bind (TimecardSummaryTable).For (v => v.Items).To (vm => vm.Timecards);
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (ScheduledTotalLabel).To (vm => vm.TotalScheduled).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (NetHoursTotalLabel).To (vm => vm.TotalNetHours).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (AdjustmentsTotalLabel).To (vm => vm.TotalAdjustments).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (OtherHoursTotalLabel).To (vm => vm.TotalOtherHours).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (TotalHoursTotalLabel).To (vm => vm.TotalTotalHours).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (ExceptionsTotalLabel).To (vm => vm.TotalExceptions);
            set.Bind (this).For (b => b.EmployeeNameTitle).To (vm => vm.EmployeeNameTitle);
            set.Apply();
        }
    }
}
