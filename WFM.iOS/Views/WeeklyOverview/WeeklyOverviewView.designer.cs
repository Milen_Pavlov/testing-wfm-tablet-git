// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("WeeklyOverviewView")]
	partial class WeeklyOverviewView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton DateButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NextButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PrevButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TabControl { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl Tabs { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DateButton != null) {
				DateButton.Dispose ();
				DateButton = null;
			}

			if (NextButton != null) {
				NextButton.Dispose ();
				NextButton = null;
			}

			if (PrevButton != null) {
				PrevButton.Dispose ();
				PrevButton = null;
			}

			if (TabControl != null) {
				TabControl.Dispose ();
				TabControl = null;
			}

			if (Tabs != null) {
				Tabs.Dispose ();
				Tabs = null;
			}
		}
	}
}
