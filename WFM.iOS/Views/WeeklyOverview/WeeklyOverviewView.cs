﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using System.Collections.Generic;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.ObjCRuntime;

namespace WFM.iOS
{
    public partial class WeeklyOverviewView : WFMTabView
    {
        UIBarButtonItem m_refresh;

        public WeeklyOverviewViewModel VM
        {
            get
            {
                return (WeeklyOverviewViewModel)ViewModel;
            }
        }

        private UIColor m_buttonColor;
        public UIColor ButtonColor
        {
            get { return m_buttonColor; } 
            set
            {
                m_buttonColor = value;

                NextButton.TintColor = value;
                NextButton.Layer.BorderColor = value.CGColor;
                PrevButton.TintColor = value;
                PrevButton.Layer.BorderColor = value.CGColor;
                DateButton.TintColor = value;
                DateButton.Layer.BorderColor = value.CGColor;
            }
        }

        public string DateButtonValue
        {
            get{ return null; }
            set
            {
                DateButton.SetTitle (value, UIControlState.Normal);
            }
        }

        private void OnTabSelect (object sender, EventArgs e)
        {
            VM.OnTabSelect(Tabs.SelectedSegment);
        }

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        public WeeklyOverviewView()
            : base (IsPhone ? "WeeklyOverviewView_iPhone" : "WeeklyOverviewView_iPad", null)
        {
        }

        protected override UIView GetTabContentView()
        {
            return TabControl;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();

            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, (v1,v2) => { });
            barButtons.Add (m_refresh);

            NavigationItem.RightBarButtonItems = barButtons.ToArray();

            // Button Layout
            PrevButton.Layer.BorderWidth = 1;
            PrevButton.Layer.CornerRadius = 4;
            NextButton.Layer.BorderWidth = 1;
            NextButton.Layer.CornerRadius = 4;
            DateButton.Layer.BorderWidth = 1;
            DateButton.Layer.CornerRadius = 4;

            // ios7 layout
            if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            Tabs.AddTarget(OnTabSelect, UIControlEvent.ValueChanged);

            // View Model Binding
            var set = this.CreateBindingSet<WeeklyOverviewView, WeeklyOverviewViewModel>();
            set.Bind (this).For (v => v.Title).To (vm => vm.Title);
            set.Bind (m_refresh).For (b => b.Enabled).To (m => m.Loading).WithConversion (new HiddenConverter (), null);
            set.Bind (m_refresh).To("OnRefresh");
            set.Bind (this).For (m => m.DateButtonValue).To (m => m.Date);
            set.Bind (NextButton).To("OnMoveNext");
            set.Bind (PrevButton).To("OnMovePrev");
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (Tabs).For(b => b.TintColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind(DateButton).To("OnDateSelect");
            set.Apply();
        }

        private UIImage ImageWithColor(UIColor color)
        {
            RectangleF rect = new RectangleF (0, 0, 1, 1);
            UIGraphics.BeginImageContext (rect.Size);
            CGContext context = UIGraphics.GetCurrentContext ();

            context.SetFillColor (color.CGColor);
            context.FillRect (rect);

            UIImage image = UIGraphics.GetImageFromCurrentImageContext ();
            UIGraphics.EndImageContext ();

            return image;
        }
    }
}
