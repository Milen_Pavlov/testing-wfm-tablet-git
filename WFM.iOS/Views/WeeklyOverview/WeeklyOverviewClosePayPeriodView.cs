﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public partial class WeeklyOverviewClosePayPeriodView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(WeeklyOverviewView); } 
        }

        public WeeklyOverviewClosePayPeriodView ()
            : base (IsPhone ? "WeeklyOverviewClosePayPeriodView_iPhone" : "WeeklyOverviewClosePayPeriodView_iPad", null)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            // View Model Binding
            //var set = this.CreateBindingSet<WeeklyOverviewClosePayPeriodView, WeeklyOverviewClosePayPeriodViewModel>();
            //set.Apply();
        }
    }
}
