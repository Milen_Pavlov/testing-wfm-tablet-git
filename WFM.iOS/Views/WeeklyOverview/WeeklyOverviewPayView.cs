﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public partial class WeeklyOverviewPayView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(WeeklyOverviewView); } 
        }

        public WeeklyOverviewPayView ()
            : base (IsPhone ? "WeeklyOverviewPayView_iPhone" : "WeeklyOverviewPayView_iPad", null)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            // View Model Binding
            //var set = this.CreateBindingSet<WeeklyOverviewPayView, WeeklyOverviewPayViewModel>();
            //set.Apply();
        }
    }
}
