﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using MonoTouch.ObjCRuntime;
using System.Collections.Generic;
using System.Drawing;

namespace WFM.iOS
{
    public partial class WeeklyOverviewExceptionsView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(WeeklyOverviewView); } 
        }

        //Custom bar button setup
        UIView m_majorFilterButtonRoot;
        UIImageView m_majorFilterButtonImage;
        UIButton m_majorFilterButtonButton;
            
        UIBarButtonItem m_majorFilter;
        UIImage[] m_majorFilterButtons;
        List<UIBarButtonItem> m_originalButtonItems;

        UIBarButtonItem m_filter;
        FilterPopover m_filterPopover;

        private UIColor m_buttonColor;
        public UIColor ButtonColor
        {
            get { return m_buttonColor; } 
            set
            {
                m_buttonColor = value;

                foreach (var separator in HeaderSeparators)
                    separator.BackgroundColor = UIColor.LightGray;

                HeaderRoot.BackgroundColor = value;
            }
        }

        private bool m_isFilter = false;
        public bool IsFilter
        {
            get { return m_isFilter; }
            set
            {
                m_isFilter = value;

                if (m_majorFilterButtonButton != null)
                    m_majorFilterButtonButton.SetTitleColor ((value) ? UIColor.White : UIColor.Black, UIControlState.Normal);
            }
        }

        public string EmployeeNameTitle
        {
            get { return string.Empty; }
            set { EmployeeNameLabel.Text = value; } 
        }

        public WeeklyOverviewExceptionsView ()
            : base (IsPhone ? "WeeklyOverviewExceptionsView_iPhone" : "WeeklyOverviewExceptionsView_iPad", null)
        {
            m_majorFilterButtons = new UIImage[] { UIImage.FromBundle("yellow_triangle"), UIImage.FromBundle("red_triangle") };
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            // Popovers
            m_filterPopover = new FilterPopover ();

            m_majorFilterButtonRoot = new UIView(new RectangleF(0,0,24,24));
            m_majorFilterButtonImage = new UIImageView (new RectangleF (0, 0, 24, 24));
            m_majorFilterButtonButton = new UIButton (new RectangleF (0, 0, 24, 24));
            m_majorFilterButtonButton.SetTitle ("!", UIControlState.Normal);
            m_majorFilterButtonImage.Image = m_majorFilterButtons[0];
            m_majorFilterButtonRoot.Add (m_majorFilterButtonImage);
            m_majorFilterButtonRoot.Add (m_majorFilterButtonButton);
            m_majorFilter = new UIBarButtonItem (m_majorFilterButtonRoot);

            m_originalButtonItems = new List<UIBarButtonItem> ();
            foreach (var button in NavigationItem.RightBarButtonItems)
            {
                m_originalButtonItems.Add (button);
            }
            List<UIBarButtonItem> rightButtons = new List<UIBarButtonItem> (m_originalButtonItems);
            m_filter = new UIBarButtonItem (UIImage.FromBundle ("Filter"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_filterPopover.PresentFromBarButtonItem(m_filter, UIPopoverArrowDirection.Up, true);
            });

            rightButtons.Add (m_filter);
            rightButtons.Add (m_majorFilter);
            NavigationItem.RightBarButtonItems = rightButtons.ToArray();

            // ios7 layout
            if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            Data.RegisterCell<DailyOverviewItemCell> ();
            Data.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            Data.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<WeeklyOverviewExceptionsView, WeeklyOverviewExceptionsViewModel>();
            set.Bind (LoadingRootView).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingActivity).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingLabel).To (m => m.LoadingMessage);
            set.Bind (EmptyLabel).To (m => m.EmptyMessage);
            set.Bind (EmptyLabel).For (v => v.Hidden).To (vm => vm.EmptyMessage).WithConversion (new StringVisibleConverter (), null);
            set.Bind (Data).For (v => v.Items).To (vm => vm.FilteredExceptions);
            set.Bind (Data).For (v => v.CustomCommand1).To ("OnTimecardSelected");
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (m_majorFilterButtonImage).For (b => b.Image).To(m => m.MajorFilter).WithConversion (new ImageBoolToIndexConvertor (), m_majorFilterButtons);
            set.Bind (this).For (v => v.IsFilter).To (m => m.MajorFilter);
            set.Bind (m_majorFilterButtonButton).To ("OnMajorFilter");
            set.Bind(m_filter).To("OnFilter");
            set.Bind (this).For (b => b.EmployeeNameTitle).To (vm => vm.EmployeeNameTitle);
            set.Bind (m_filterPopover).For (b => b.Apply).To("OnApplyFilter");
            set.Bind (m_filterPopover).For (b => b.Filters).To(m => m.Filters);
            set.Apply();
        }

        protected override void ViewClosed ()
        {
            base.ViewClosed ();
            NavigationItem.RightBarButtonItems = m_originalButtonItems.ToArray();
        
            if(m_filterPopover != null)
                m_filterPopover.Dismiss (false);
        }
    }
}
