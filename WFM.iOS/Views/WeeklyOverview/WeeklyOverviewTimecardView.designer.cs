// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("WeeklyOverviewTimecardView")]
	partial class WeeklyOverviewTimecardView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AdjustmentsTotalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmptyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ExceptionsTotalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView FooterRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] FooterSeparators { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] HeaderSeparators { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingSpinner { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NetHoursTotalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OtherHoursTotalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ScheduledTotalLabel { get; set; }

		[Outlet]
		WFM.iOS.DailyOverviewTable TimecardSummaryTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalHoursTotalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EmployeeNameLabel != null) {
				EmployeeNameLabel.Dispose ();
				EmployeeNameLabel = null;
			}

			if (AdjustmentsTotalLabel != null) {
				AdjustmentsTotalLabel.Dispose ();
				AdjustmentsTotalLabel = null;
			}

			if (EmptyLabel != null) {
				EmptyLabel.Dispose ();
				EmptyLabel = null;
			}

			if (ExceptionsTotalLabel != null) {
				ExceptionsTotalLabel.Dispose ();
				ExceptionsTotalLabel = null;
			}

			if (FooterRoot != null) {
				FooterRoot.Dispose ();
				FooterRoot = null;
			}

			if (HeaderRoot != null) {
				HeaderRoot.Dispose ();
				HeaderRoot = null;
			}

			if (LoadingLabel != null) {
				LoadingLabel.Dispose ();
				LoadingLabel = null;
			}

			if (LoadingRoot != null) {
				LoadingRoot.Dispose ();
				LoadingRoot = null;
			}

			if (LoadingSpinner != null) {
				LoadingSpinner.Dispose ();
				LoadingSpinner = null;
			}

			if (NetHoursTotalLabel != null) {
				NetHoursTotalLabel.Dispose ();
				NetHoursTotalLabel = null;
			}

			if (OtherHoursTotalLabel != null) {
				OtherHoursTotalLabel.Dispose ();
				OtherHoursTotalLabel = null;
			}

			if (ScheduledTotalLabel != null) {
				ScheduledTotalLabel.Dispose ();
				ScheduledTotalLabel = null;
			}

			if (TimecardSummaryTable != null) {
				TimecardSummaryTable.Dispose ();
				TimecardSummaryTable = null;
			}

			if (TotalHoursTotalLabel != null) {
				TotalHoursTotalLabel.Dispose ();
				TotalHoursTotalLabel = null;
			}

			if (TotalLabel != null) {
				TotalLabel.Dispose ();
				TotalLabel = null;
			}
		}
	}
}
