﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.CrossCore;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class DatePickerView : ConsortiumPopoverView
    {
        private IStringService m_localiser;

        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public DatePickerView ()
            : base (UserInterfaceIdiomIsPhone ? "DatePickerView_iPhone" : "DatePickerView_iPad", null)
        {
            CenterInView = true;
            m_localiser = Mvx.Resolve<IStringService>();
        }

        public int ScrollToPosition
        {
            get
            {
                return -1;
            }
            set
            {
                NSIndexPath index = NSIndexPath.FromRowSection (value, 0);
                DatePicker.ScrollToRow (index, UITableViewScrollPosition.Middle, true);
            }
        }

        public override bool Present ()
        {
            CenterInView = true;
            return base.Present ();
        }

        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            Title = m_localiser.Get("select_date");
			
            // Perform any additional setup after loading the view, typically from a nib.
            DatePicker.RegisterCell<DatePickerGridCell> ();
            DatePicker.AllowsSelection = false;
            DatePicker.RowHeight = 70;
            DatePicker.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            DatePicker.BackgroundColor = new UIColor (0.878f, 0.878f, 0.878f, 1);

            var set = this.CreateBindingSet<DatePickerView, DatePickerViewModel>();
            set.Bind (DatePicker).To (vm => vm.Data);
            set.Bind (DatePicker).For(m => m.CustomCommand1).To ("OnSetDate");
            set.Bind (FirstDay).To (vm => vm.FirstDay);
            set.Bind (SecondDay).To (vm => vm.SecondDay);
            set.Bind (ThirdDay).To (vm => vm.ThirdDay);
            set.Bind (FourthDay).To (vm => vm.FourthDay);
            set.Bind (FifthDay).To (vm => vm.FifthDay);
            set.Bind (SixthDay).To (vm => vm.SixthDay);
            set.Bind (SeventhDay).To (vm => vm.SeventhDay);
            set.Bind (this).For (m => m.ScrollToPosition).To (m => m.ScrollPosition);

            set.Apply();
        }
    }
}

