﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Touch.Views;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    // Not a consortium view
    public partial class EmployeeInfoView : ConsortiumPopoverView
    {
        public JDAEmployeeInfo EmployeeInfo { get; set; }

        public EmployeeInfoView()
            : base ("EmployeeInfoView", null)
        {
            ArrowDirection = UIPopoverArrowDirection.Left;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            Navigation.NavigationBar.TintColor = UIColor.White;
            Navigation.NavigationBar.BarTintColor = Constants.TescoBlue;
            Navigation.NavigationBar.BarStyle = UIBarStyle.Black;
            Navigation.NavigationBar.Translucent = false;

            var set = this.CreateBindingSet<EmployeeInfoView, EmployeeInfoViewModel>();
            set.Bind (this).For(b => b.Title).To (m => m.EmployeeName);
            set.Bind (BULabel).To (m => m.HomeSite);
            set.Bind (JobLabel).To (m => m.PrimaryJob);
            set.Bind (HomeLabel).To (m => m.HomePhone);
            set.Bind (MobileLabel).To (m => m.MobilePhone);
            set.Bind (MobileLabel).For(b => b.Hidden).To (m => m.ConfigurationService.ShowMobileNumbers).WithConversion(new HiddenConverter(), null);
            set.Bind (MobileTitleLabel).For(b => b.Hidden).To (m => m.ConfigurationService.ShowMobileNumbers).WithConversion(new HiddenConverter(), null);
            set.Apply();
        }
    }
}
