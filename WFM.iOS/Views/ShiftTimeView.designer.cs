// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ShiftTimeView")]
	partial class ShiftTimeView
	{
		[Outlet]
		MonoTouch.UIKit.UIDatePicker DurationPicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SaveButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIDatePicker StartPicker { get; set; }

		[Action ("OnApply:")]
		partial void OnApply (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DurationPicker != null) {
				DurationPicker.Dispose ();
				DurationPicker = null;
			}

			if (SaveButton != null) {
				SaveButton.Dispose ();
				SaveButton = null;
			}

			if (StartPicker != null) {
				StartPicker.Dispose ();
				StartPicker = null;
			}
		}
	}
}
