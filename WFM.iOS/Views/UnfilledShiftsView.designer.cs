// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("UnfilledShiftsView")]
	partial class UnfilledShiftsView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AvailabeEmployeesLabel { get; set; }

		[Outlet]
		Consortium.Client.iOS.TableControl AvailableEmployeesTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingActivityIndicator { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Message1Label { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Message2Label { get; set; }

		[Outlet]
		Consortium.Client.iOS.TableControl UnfilledShiftsTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView UnfilledShiftView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AvailableEmployeesTable != null) {
				AvailableEmployeesTable.Dispose ();
				AvailableEmployeesTable = null;
			}

			if (LoadingActivityIndicator != null) {
				LoadingActivityIndicator.Dispose ();
				LoadingActivityIndicator = null;
			}

			if (LoadingLabel != null) {
				LoadingLabel.Dispose ();
				LoadingLabel = null;
			}

			if (Message1Label != null) {
				Message1Label.Dispose ();
				Message1Label = null;
			}

			if (Message2Label != null) {
				Message2Label.Dispose ();
				Message2Label = null;
			}

			if (UnfilledShiftsTable != null) {
				UnfilledShiftsTable.Dispose ();
				UnfilledShiftsTable = null;
			}

			if (UnfilledShiftView != null) {
				UnfilledShiftView.Dispose ();
				UnfilledShiftView = null;
			}

			if (AvailabeEmployeesLabel != null) {
				AvailabeEmployeesLabel.Dispose ();
				AvailabeEmployeesLabel = null;
			}
		}
	}
}
