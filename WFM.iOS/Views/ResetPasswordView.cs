﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ResetPasswordView : ConsortiumModalView
    {
        UILabel m_title;

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (false);

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor highlight = ColorUtility.FromHexString (settings.AppTheme.HighlightColor);
            UIColor secondary = ColorUtility.FromHexString (settings.AppTheme.SecondaryColor);

            SubmitButton.SetTitleColor (secondary, UIControlState.Normal);
            CancelButton.SetTitleColor(secondary, UIControlState.Normal);
        }

        public ResetPasswordView()
            : base (IsPhone ? "ResetPasswordView_iPhone" : "ResetPasswordView_iPad", null)
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var settings = Mvx.Resolve<SettingsService> ();

            // View Model Binding
            var set = this.CreateBindingSet<ResetPasswordView, ResetPasswordViewModel>();

            set.Bind (Username).To (vm => vm.Username).TwoWay ();
            set.Bind (CancelButton).To ("OnCancelButton");
            set.Bind (SubmitButton).To ("OnSubmitButton");

            set.Bind(SubmitButton).For(b => b.BackgroundColor).To(m => m.Theme.SecondaryColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(CancelButton).For(b => b.BackgroundColor).To(m => m.Theme.SecondaryColor).WithConversion(new HexColorCoverter(), null);

            set.Bind(HeaderView).For(b => b.Uri).To(m => m.Logo);
            set.Bind(HeaderView).For(b => b.BackgroundColor).To(m => m.Theme.LoginBarColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(BackgroundView).For(b => b.Uri).To(m => m.Background);
            set.Bind(UserBackgroundView).For(b => b.BackgroundColor).To(m => m.Theme.LoginBarColor).WithConversion(new HexColorCoverter(), null);
            //set.Bind(Username).For(b => b.TextColor).To(m => m.Theme.LoginTextColor).WithConversion(new HexColorCoverter(), null);
            //set.Bind(UserTitle).For(b => b.TextColor).To(m => m.Theme.LoginTextColor).WithConversion(new HexColorCoverter(), null);

            set.Bind(SubmitButton).For(b => b.BackgroundColor).To(m => m.Theme.HighlightColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(CancelButton).For(b => b.BackgroundColor).To(m => m.Theme.HighlightColor).WithConversion(new HexColorCoverter(), null);

            set.Apply();
        }
            
        public override bool Present ()
        {
            return base.Present ();
        }

        public override void ViewOpened ()
        {
            Username.BecomeFirstResponder ();

            base.ViewOpened ();
        }
    }
}