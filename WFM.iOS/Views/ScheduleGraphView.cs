﻿using System;
using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using ShinobiCharts;
using WFM.Core;
using MonoTouch.Foundation;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleGraphView : ConsortiumView
    {
        const int c_legendWidth = 155;
        const int c_gridStartOffset = 221;

        public override Type AttachTarget
        {
            get { return typeof(IConsortiumViewTarget); } 
        }

        private IStringService m_stringService;
        private ShinobiChart m_graph;
        public ScheduleGraphDataSource DataSource { get; set; }

        public ScheduleGraphView()
            : base (IsPhone ? "ScheduleGraphView_iPhone" : "ScheduleGraphView_iPad", null)
        {
            m_stringService = Mvx.Resolve<IStringService>();
        }

        public AccuracyData Data
        {
            get { return null; }
            set 
            {
                DataSource.Data = value;
                m_graph.ReloadData ();
                m_graph.RedrawChart ();
            }
        }
        
        public int? Day
        {
            get { return 0; }
            set 
            { 
                if (value == null)
                {
                    m_graph.XAxis.LabelFormatter.DateFormatter.DateFormat = m_stringService.Get("graph_week_date_format");
                }
                else
                {
                    m_graph.XAxis.LabelFormatter.DateFormatter.DateFormat = m_stringService.Get("graph_day_date_format");
                }
            }
        }

        public RectangleF CalculateSize(bool inlineMode)
        {
            if (inlineMode)
            {
                float margin = 20;

                return new RectangleF (c_gridStartOffset - c_legendWidth, View.Bounds.Top + margin, View.Bounds.Width - (c_gridStartOffset - c_legendWidth), View.Bounds.Height - (margin));
            }
            else
            {
                float margin = 50;

                return new RectangleF (margin, margin, View.Bounds.Width - (margin * 2), View.Bounds.Height - (margin * 2));
            }
        }

        public override void ViewDidLoad()
        {
            this.View.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin;

            base.ViewDidLoad ();

            // Create the chart
            CreateChart (((ScheduleGraphViewModel)ViewModel).IsInline);

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleGraphView, ScheduleGraphViewModel>();
            set.Bind (DataSource).For (b => b.DayIndex).To(m => m.DayIndex);
            set.Bind (this).For (b => b.Data).To(m => m.Accuracy);
            set.Bind (this).For (b => b.Day).To(m => m.DayIndex);
            set.Apply();

        }
            
        void CreateChart (bool inlineMode)
        {
            m_graph = new ShinobiChart (CalculateSize (inlineMode)) {
                AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
            //LicenseKey = "VyVWhV/IhtPzvYpMjAxNDEwMTBicnlhbkB0aW5kZXJzdG9uZS5jb20=/kiAD7RunVO6+EkHzLKQMmr7suUXWFukvK9q3PNL4hQaxUL6nVLoIGnJz9Fz8/5DMl+dsWN7MJahDxiI7lk/xN1w45KbIhRA7zcxKvt2yTiwCBcNksVeSdDLZb+oTNCj1B9/oU/ngy1VVHwirHU2yt9QFE6A=BQxSUisl3BaWf/7myRmmlIjRnMU2cA7q+/03ZX9wdj30RzapYANf51ee3Pi8m2rVW6aD7t6Hi4Qy5vv9xpaQYXF5T7XzsafhzS3hbBokp36BoJZg8IrceBj742nQajYyV7trx5GIw9jy/V6r0bvctKYwTim7Kzq+YPWGMtqtQoU=PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+"
            };

            // Add a pair of axes
            var xAxis = new SChartDateTimeAxis ();
            xAxis.Style.InterSeriesPadding = 0.1;
            xAxis.EnableGesturePanning = true;
            xAxis.EnableGestureZooming = true;
            xAxis.EnableMomentumPanning = true;
            xAxis.EnableMomentumZooming = true;                         
            
            var yAxis = new SChartNumberAxis ();
            yAxis.RangePaddingHigh = new NSNumber (5);
            yAxis.EnableGesturePanning = true;
            yAxis.EnableGestureZooming = true;
            yAxis.EnableMomentumPanning = true;
            yAxis.EnableMomentumZooming = true;
            yAxis.MajorTickFrequency = new NSNumber (1);
            yAxis.MinorTickFrequency = new NSNumber (1);
            yAxis.Style.MajorGridLineStyle.LineWidth = new NSNumber (1f);
            yAxis.Style.MajorGridLineStyle.LineColor = UIColor.DarkGray;
            yAxis.Style.MajorGridLineStyle.ShowMajorGridLines = true;

            yAxis.AnimationEnabled = true;

            // Legend
            m_graph.Legend.Hidden = false;

            if (inlineMode)
            {
                m_graph.Legend.Placement = SChartLegendPlacement.OutsidePlotArea;
                m_graph.Legend.Position = SChartLegendPosition.TopLeft;
            }
            else
            {
                m_graph.Legend.Placement = SChartLegendPlacement.InsidePlotArea;
            }

            m_graph.Legend.Style.AreaColor = UIColor.White;
            m_graph.Legend.Style.BorderColor = Constants.TescoBlue;
            m_graph.Legend.Style.BorderWidth = new NSNumber (1f);
            // General graph
            m_graph.XAxis = xAxis;
            m_graph.YAxis = yAxis;
            m_graph.BackgroundColor = UIColor.Clear;
            // Add the the view
            View.AddSubview (m_graph);
            // Set the data source
            m_graph.DataSource = DataSource = new ScheduleGraphDataSource ();
        }
    }
}