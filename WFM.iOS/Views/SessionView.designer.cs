// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("SessionView")]
	partial class SessionView
	{
		[Outlet]
		Consortium.Client.iOS.WebImageControl BackgroundImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingView { get; set; }

		[Outlet]
		Consortium.Client.iOS.WebImageControl LogoImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MessageLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView PassBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField Password { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PasswordLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ResetButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SettingsButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SignInButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SignInText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView UserBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField UserName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel UsernameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel VersionLabel { get; set; }

		[Action ("OnPassword:")]
		partial void OnPassword (MonoTouch.Foundation.NSObject sender);

		[Action ("OnSignIn:")]
		partial void OnSignIn (MonoTouch.Foundation.NSObject sender);

		[Action ("OnUserName:")]
		partial void OnUserName (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundImage != null) {
				BackgroundImage.Dispose ();
				BackgroundImage = null;
			}

			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}

			if (LogoImage != null) {
				LogoImage.Dispose ();
				LogoImage = null;
			}

			if (MessageLabel != null) {
				MessageLabel.Dispose ();
				MessageLabel = null;
			}

			if (PassBackgroundView != null) {
				PassBackgroundView.Dispose ();
				PassBackgroundView = null;
			}

			if (Password != null) {
				Password.Dispose ();
				Password = null;
			}

			if (PasswordLabel != null) {
				PasswordLabel.Dispose ();
				PasswordLabel = null;
			}

			if (ResetButton != null) {
				ResetButton.Dispose ();
				ResetButton = null;
			}

			if (SettingsButton != null) {
				SettingsButton.Dispose ();
				SettingsButton = null;
			}

			if (SignInButton != null) {
				SignInButton.Dispose ();
				SignInButton = null;
			}

			if (SignInText != null) {
				SignInText.Dispose ();
				SignInText = null;
			}

			if (UserBackgroundView != null) {
				UserBackgroundView.Dispose ();
				UserBackgroundView = null;
			}

			if (UserName != null) {
				UserName.Dispose ();
				UserName = null;
			}

			if (UsernameLabel != null) {
				UsernameLabel.Dispose ();
				UsernameLabel = null;
			}

			if (VersionLabel != null) {
				VersionLabel.Dispose ();
				VersionLabel = null;
			}
		}
	}
}
