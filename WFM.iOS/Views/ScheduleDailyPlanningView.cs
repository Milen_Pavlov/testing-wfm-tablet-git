﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;
using System.Collections;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleDailyPlanningView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

        // Allow switch over between different cell types based on contents of array
        public IList Headers
        {
            get { return HeaderCollection.Items; }
            set 
            {
                if (value == null)
                    return;

                var layout = HeaderCollection.CollectionViewLayout as UICollectionViewFlowLayout;

                if (value.Count == 1)
                {
                    HeaderCollection.RegisterCell<GridDayHeaderCell> ();
                    layout.ItemSize = new SizeF (HeaderCollection.Frame.Width, 60);
                }
                else
                {
                    HeaderCollection.RegisterCell<GridWeekHeaderCell> ();
                    layout.ItemSize = new SizeF (HeaderCollection.Frame.Width / value.Count, 60);
                }

                HeaderCollection.Items = value;
            }
        }

        public ScheduleDailyPlanningView()
            : base (IsPhone ? "ScheduleDailyPlanningView_iPhone" : "ScheduleDailyPlanningView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            UIColor textColor = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);

            TitleLabel.TextColor = textColor;
            TitleHeader.BackgroundColor = primary;

            PlanningTable.TableFooterView = new UIView (RectangleF.Empty);
            PlanningTable.BackgroundColor = UIColor.White;


            HeaderCollection.RegisterCell<GridDayHeaderCell> ();

            PlanningTable.RegisterCell<PlanningDayCell> ();
            PlanningTable.RowHeight = PlanningDayCell.RowHeight;

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleDailyPlanningView, ScheduleDailyPlanningViewModel>();
            set.Bind (this).For(b => b.Headers).To (m => m.Headers);
            set.Bind (PlanningTable).For(b => b.DayIndex).To (m => m.DayIndex);
            set.Bind (PlanningTable).To (m => m.Planning);
            set.Apply();
        }

        protected override void ViewClosed()
        {
            base.ViewClosed ();

            PlanningTable.Items = null;
            PlanningTable.RemoveFromSuperview ();
            PlanningTable = null;
        }
    }
}

