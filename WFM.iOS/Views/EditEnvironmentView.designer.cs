// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using System.CodeDom.Compiler;
using MonoTouch.Foundation;

namespace  WFM.iOS
{
	[Register ("EditEnvironmentView")]
	partial class EditEnvironmentView
	{
		[Outlet]
        MonoTouch.UIKit.UIButton ClearButton { get; set; }

		[Outlet]
        MonoTouch.UIKit.UIButton DoneButton { get; set; }

		[Outlet]
        MonoTouch.UIKit.UITextView EditTextView { get; set; }

		[Outlet]
        MonoTouch.UIKit.UIActivityIndicatorView Spinner { get; set; }

		[Outlet]
        MonoTouch.UIKit.UILabel TitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ClearButton != null) {
				ClearButton.Dispose ();
				ClearButton = null;
			}

			if (DoneButton != null) {
				DoneButton.Dispose ();
				DoneButton = null;
			}

			if (EditTextView != null) {
				EditTextView.Dispose ();
				EditTextView = null;
			}

			if (TitleLabel != null) {
				TitleLabel.Dispose ();
				TitleLabel = null;
			}

			if (Spinner != null) {
				Spinner.Dispose ();
				Spinner = null;
			}
		}
	}
}
