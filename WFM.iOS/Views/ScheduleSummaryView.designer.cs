// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleSummaryView")]
	partial class ScheduleSummaryView
	{
		[Outlet]
		WFM.iOS.AccordianMultiTableControl AccordianTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton[] ColumnButtons { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView ColumnButtonsBackground { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] ColumnButtonSeparators { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DateNext { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DatePrevious { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmptyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView EmptyView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton GroupButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalAccuracy { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalCostVariance { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalCoverage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalLabourCost { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalLabourHours { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalScheduledCost { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalScheduledHours { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AccordianTable != null) {
				AccordianTable.Dispose ();
				AccordianTable = null;
			}

			if (ColumnButtonsBackground != null) {
				ColumnButtonsBackground.Dispose ();
				ColumnButtonsBackground = null;
			}

			if (DateNext != null) {
				DateNext.Dispose ();
				DateNext = null;
			}

			if (DatePrevious != null) {
				DatePrevious.Dispose ();
				DatePrevious = null;
			}

			if (DateText != null) {
				DateText.Dispose ();
				DateText = null;
			}

			if (EmptyLabel != null) {
				EmptyLabel.Dispose ();
				EmptyLabel = null;
			}

			if (EmptyView != null) {
				EmptyView.Dispose ();
				EmptyView = null;
			}

			if (GroupButton != null) {
				GroupButton.Dispose ();
				GroupButton = null;
			}

			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}

			if (TotalLabourHours != null) {
				TotalLabourHours.Dispose ();
				TotalLabourHours = null;
			}

			if (TotalScheduledHours != null) {
				TotalScheduledHours.Dispose ();
				TotalScheduledHours = null;
			}

			if (TotalCoverage != null) {
				TotalCoverage.Dispose ();
				TotalCoverage = null;
			}

			if (TotalAccuracy != null) {
				TotalAccuracy.Dispose ();
				TotalAccuracy = null;
			}

			if (TotalLabourCost != null) {
				TotalLabourCost.Dispose ();
				TotalLabourCost = null;
			}

			if (TotalScheduledCost != null) {
				TotalScheduledCost.Dispose ();
				TotalScheduledCost = null;
			}

			if (TotalCostVariance != null) {
				TotalCostVariance.Dispose ();
				TotalCostVariance = null;
			}
		}
	}
}
