// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleReportView")]
	partial class ScheduleReportView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel HeaderLabels { get; set; }

		[Outlet]
		Consortium.Client.iOS.TableControl ReportTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ReportTable != null) {
				ReportTable.Dispose ();
				ReportTable = null;
			}

			if (HeaderLabels != null) {
				HeaderLabels.Dispose ();
				HeaderLabels = null;
			}
		}
	}
}
