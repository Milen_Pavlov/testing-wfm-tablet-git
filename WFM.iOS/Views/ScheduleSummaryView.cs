﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleSummaryView : WFMView
    {
        UIBarButtonItem m_refresh;

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); }
        }

        public UIColor ButtonColor
        {
            get { return UIColor.White; } 
            set
            {
                DateNext.TintColor = value;
                DateNext.Layer.BorderColor = value.CGColor;
                DatePrevious.TintColor = value;
                DatePrevious.Layer.BorderColor = value.CGColor;
                GroupButton.TintColor = value;
                GroupButton.Layer.BorderColor = value.CGColor;
                GroupButton.SetTitleColor (value, UIControlState.Normal);
            }
        }

        public string SelectedHighlightedValue
        {
            get { return null; }
            set
            {
                GroupButton.SetTitle (value, UIControlState.Normal);
                GroupButton.SetTitle (value, UIControlState.Disabled);
            }
        }

        public ScheduleSummaryView ()
            : base (IsPhone ? "ScheduleSummaryView_iPhone" : "ScheduleSummaryView_iPad", null)
        {
            Title = "Planned Schedule Summary";
        }

        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            UIBarButtonItem[] barButtons = new UIBarButtonItem[1];

            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, (v1,v2) => { 

            });

            barButtons[0] = m_refresh;

            NavigationItem.RightBarButtonItems = barButtons;
	        // Button Layout
            DatePrevious.Layer.BorderWidth = 1;
            DatePrevious.Layer.CornerRadius = 4;
            DateNext.Layer.BorderWidth = 1;
            DateNext.Layer.CornerRadius = 4;
            GroupButton.Layer.BorderWidth = 1;
            GroupButton.Layer.CornerRadius = 4;
            GroupButton.Enabled = false;

            AccordianTable.RowHeight = 50;
            AccordianTable.RegisterSection<ScheduleSummarySectionCell> ();
            AccordianTable.RegisterCell<ScheduleSummaryDayHeaderCell, JDAScheduleSummaryGroupBaseEntry> ();
            AccordianTable.RegisterCell<ScheduleSummaryDayEntryCell, JDAScheduleSummaryGroupDayEntry> ();
            AccordianTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            //Themeing + formatting
            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            ColumnButtonsBackground.BackgroundColor = primary;
            foreach (UIButton b in ColumnButtons)
            {
                b.TitleLabel.TextAlignment = UITextAlignment.Center;
                b.SetTitleColor (UIColor.White, UIControlState.Normal);
            }

            foreach (UIView s in ColumnButtonSeparators)
            {
                s.BackgroundColor = UIColor.Gray;
            }

            IStringService localiser = Mvx.Resolve<IStringService> ();
            String dateFormat = String.Format("'Week Starting:' {0}", localiser.Get ("dd/MM/yyyy"));
            var set = this.CreateBindingSet<ScheduleSummaryView, ScheduleSummaryViewModel> ();
            set.Bind (DateText).To (m => m.StartDate).WithConversion (new DateConverter (), dateFormat);
            set.Bind (DateNext).To ("OnMoveNext");
            set.Bind (DatePrevious).To ("OnMovePrev");
            set.Bind (GroupButton).To ("ShowGroupList");
            set.Bind (AccordianTable).To (m => m.Data);
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (LoadingView).For (v => v.Hidden).To (vm => vm.IsLoading).WithConversion (new HiddenConverter());
            set.Bind (m_refresh).For (p => p.Enabled).To (p => p.IsLoading).WithConversion (new HiddenConverter());
            set.Bind (m_refresh).To ("OnRefresh");
            set.Bind (GroupButton).For (p => p.Enabled).To (p => p.ReceivedGroupData);
            set.Bind (this).For (v => v.SelectedHighlightedValue).To (m => m.SelectionTitle);
            set.Bind (EmptyView).For (v => v.Hidden).To (vm => vm.IsEmpty).WithConversion (new HiddenConverter());
            set.Bind (AccordianTable).For (v => v.Hidden).To (vm => vm.IsEmpty);
            set.Bind (TotalAccuracy).To (vm => vm.TitleTotals.Accuracy).WithConversion (new StringFormatValueConverter (), "0.00%");
            set.Bind (TotalCostVariance).To (vm => vm.TitleTotals.CostVariance).WithConversion (new StringFormatValueConverter(), "0.00%");
            set.Bind (TotalCoverage).To (vm => vm.TitleTotals.Coverage).WithConversion (new StringFormatValueConverter(), "0.00%");
            set.Bind (TotalLabourCost).To (vm => vm.TitleTotals.LabourCost).WithConversion (new StringFormatValueConverter(), "£0.00");
            set.Bind (TotalLabourHours).To (vm => vm.TitleTotals.LabourHours).WithConversion (new StringFormatValueConverter(), "0.00");
            set.Bind (TotalScheduledCost).To (vm => vm.TitleTotals.ScheduledCost).WithConversion (new StringFormatValueConverter(), "£0.00");
            set.Bind (TotalScheduledHours).To (vm => vm.TitleTotals.ScheduledHours).WithConversion (new StringFormatValueConverter(), "0.00");
            set.Apply ();
        }
    }
}
