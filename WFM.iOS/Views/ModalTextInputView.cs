﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ModalTextInputView : ConsortiumPopoverView
    {
        public ModalTextInputView()
            : base (IsPhone ? "ModalTextInputView_iPhone" : "ModalTextInputView_iPad", null)
        {
            CenterInView = true;
        }

        public ModalTextInputViewModel VM
        {
            get { return ViewModel as ModalTextInputViewModel; }
        }

        private UILabel m_title;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            TextInput.BecomeFirstResponder ();

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            UIColor secondary = ColorUtility.FromHexString (settings.AppTheme.SecondaryColor);

            EditToolbar.SetBackgroundImage(ImageUtility.ImageFromColor(primary), UIToolbarPosition.Bottom, UIBarMetrics.Default);

            CancelButton.TintColor = secondary;
            SubmitButton.TintColor = secondary;

            // Add the title
            AddTitle (VM.ConfigurationService.CanEditSchedule);

            // View Model Binding
            var set = this.CreateBindingSet<ModalTextInputView, ModalTextInputViewModel>();
            set.Bind (TextInput).To (m => m.Text);
            set.Bind (CancelButton).To ("OnCancel");
            set.Bind (SubmitButton).To ("OnSubmit");
            set.Bind (m_title).To (m => m.Title);
            set.Apply();
        }

        private void AddTitle(bool edit)
        {
            UIView titleView = new UIView(new RectangleF(0,0,300,44));

            m_title = new UILabel (new RectangleF (0, 10, 300, 18));
            m_title.Font = UIFont.FromName ("AvenirNext-DemiBold", 22);
            m_title.TextAlignment = UITextAlignment.Center;
            m_title.LineBreakMode = UILineBreakMode.TailTruncation;
            m_title.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            m_title.TextColor = UIColor.White;
            m_title.BackgroundColor = UIColor.Clear;
            titleView.AddSubview (m_title);

            NavigationItem.TitleView = titleView;
        }
    }
}