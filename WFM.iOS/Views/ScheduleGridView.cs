﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using System.Linq;
using System.IO;

namespace WFM.iOS
{
    public partial class ScheduleGridView : ConsortiumView
    {
        private IAlertBox m_alert;
        private UIBarButtonItem m_printButton;

        public override Type AttachTarget
        {
            get { return typeof(IConsortiumViewTarget); } 
        }

        public string EmployeeNameTitle
        {
            get { return string.Empty; }
            set { EmployeeNameLabel.Text = value; }
        }

        public ScheduleGridView()
            : base (IsPhone ? "ScheduleGridView_iPhone" : "ScheduleGridView_iPad", null)
        {
        }

        public SortState NameSort
        {
            get { return null; }
            set 
            { 
                if (value == null)
                    return;

                NameSortImage.Image = UIImage.FromBundle (value.IsAscending ? "sort_up" : "sort_down");
                NameSortImage.Hidden = !value.IsSorting;
            }
        }

        private int? m_lastIndex = -1;
        public ScheduleData Schedule
        {
            get { return null; }
            set 
            {
                if (value == null)
                    return;

                bool reloadRows = true;

                if (value.DayIndex != m_lastIndex)
                {
                    var layout = HeaderCollection.CollectionViewLayout as UICollectionViewFlowLayout;

                    if (value.DayIndex.HasValue)
                    {
                        ScheduleGrid.IsDay = true;
                        ScheduleGrid.RegisterCell<GridDayCell> ();
                        HeaderCollection.RegisterCell<GridDayHeaderCell> ();
                        layout.ItemSize = new SizeF (770, 60);
                        reloadRows = m_lastIndex.HasValue;
                    }
                    else
                    {
                        ScheduleGrid.IsDay = false;
                        ScheduleGrid.RegisterCell<GridWeekCell> ();
                        HeaderCollection.RegisterCell<GridWeekHeaderCell> ();
                        layout.ItemSize = new SizeF (110, 60);
                        reloadRows = !m_lastIndex.HasValue;
                    }

                    m_lastIndex = value.DayIndex;
                }

                // If employees are already set then just reload the visible rows
                if (value.FilteredEmployees == ScheduleGrid.Items && reloadRows)
                {
                    ScheduleGrid.ReloadRows (ScheduleGrid.IndexPathsForVisibleRows, UITableViewRowAnimation.None);
                }
                else
                {
                    ScheduleGrid.Items = value.FilteredEmployees;
                }
            }
        }
            
        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            m_alert = Mvx.Resolve<IAlertBox> ();

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            // ?? NOT USED ?? UIColor textColor = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);

            EmployeeNameButton.Superview.BackgroundColor = primary;
            EmployeeNameLabel.TextColor = UIColor.White;
                
            ScheduleGrid.TableFooterView = new UIView (RectangleF.Empty);
            ScheduleGrid.BackgroundColor = UIColor.White;

            m_printButton = new UIBarButtonItem (UIImage.FromBundle ("Print"), UIBarButtonItemStyle.Plain, (v1,v2) => { 

                //Remove all old PDFs before making an new one, the last one made remains on the device for debugging
                DeleteAllPDF();

                string filepath = ExportScheduleToPDF();
                if(!string.IsNullOrEmpty(filepath))
                {
                    NSUrl url = NSUrl.FromFilename(filepath);
                    var printInfo = UIPrintInfo.PrintInfo;

                    printInfo.OutputType = UIPrintInfoOutputType.General;
                    printInfo.JobName = url.LastPathComponent;
                    printInfo.Duplex = UIPrintInfoDuplex.LongEdge;

                    var printer = UIPrintInteractionController.SharedPrintController;

                    printer.PrintInfo = printInfo;
                    printer.PrintingItem = url;
                    printer.ShowsPageRange = false;
                    printer.ShowsNumberOfCopies = true;

                    //iOS 8 only
                    if (Convert.ToInt16 (UIDevice.CurrentDevice.SystemVersion.Split ('.')[0].ToString ()) >= 8)
                    {
                        printer.ShowsPaperSelectionForLoadedPapers = true;
                    }


                    if(!UIPrintInteractionController.PrintingAvailable)
                    {
                        m_alert.ShowOK("Error", "This device does not support printing");
                    }
                    else if(!UIPrintInteractionController.CanPrint(url))
                    {
                        m_alert.ShowOK("Error", "The PDF produced cannot be printed");
                    }
                    else
                    {
                        
                        printer.PresentFromBarButtonItem(m_printButton, true, (handler, completed, err) => {
                            if(completed && err == null)
                            {
                                m_alert.ShowOK("Success", "The schedule has been successfully printed.");
                            }
                            else if (err != null)
                            {
                                Mvx.Error("Erroring during print - {0}", err);
                                m_alert.ShowOK("Error", string.Format("An error occurred when sending the print job to the printer. {0}", err));
                            }
                        });
                    }
                }
            });

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleGridView, ScheduleGridViewModel>();
            set.Bind (this).For(b => b.Schedule).To (m => m.Schedule);
            set.Bind (this).For(b => b.NameSort).To (m => m.NameSort);
            set.Bind (HeaderCollection).To (m => m.Headers);
            set.Bind (TotalHoursLabel).To (m => m.TotalHours);
            set.Bind (TotalDemandLabel).To (m => m.TotalDemand);
            set.Bind(TotalHoursHeaderLabel).To(m => m.TotalHoursHeader);
            set.Bind(TotalDemandHeaderLabel).To(m => m.TotalDemandHeader);
            set.Bind (EmployeeNameButton).To ("OnNameHeaderSelect");
            set.Bind (ScheduleGrid).For(b => b.CustomCommand1).To ("OnEmployeeNameSelected");
            set.Bind (ScheduleGrid).For (b => b.CustomCommand2).To ("OnAddShiftToWeek");
            set.Bind (ScheduleGrid).For(b => b.CustomCommand3).To ("OnShiftSelected");
            set.Bind (HeaderCollection).For (b => b.SelectedCommand).To ("OnHeaderSelect");
            set.Bind (this).For (b => b.EmployeeNameTitle).To (vm => vm.EmployeeNameTitle);
            set.Apply();
        }

        protected override void ViewClosed()
        {
            base.ViewClosed ();

            ScheduleGrid.Items = null;
            ScheduleGrid.RemoveFromSuperview ();
            ScheduleGrid = null;
        }  

        public override void ViewDidAppear (bool animated)
        {
            this.View.Frame = this.View.Superview.Bounds;
            ScheduleGridViewModel vm = DataContext as ScheduleGridViewModel;

            if (vm.ConfigurationService.CanPrintSchedule)
            {
                if (NavigationItem.RightBarButtonItems == null)
                {
                    NavigationItem.RightBarButtonItems = new UIBarButtonItem[]{ m_printButton };
                }
                else
                {
                    UIBarButtonItem[] items = new UIBarButtonItem[NavigationItem.RightBarButtonItems.Length + 1];
                    for (int i = 0; i < NavigationItem.RightBarButtonItems.Length; i++)
                        items[i] = NavigationItem.RightBarButtonItems[i];

                    items[items.Length - 1] = m_printButton;

                    NavigationItem.RightBarButtonItems = items;
                }
            }

            base.ViewDidAppear (animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            NavigationItem.RightBarButtonItems = NavigationItem.RightBarButtonItems.Where (x => x != m_printButton).ToArray ();
            base.ViewDidDisappear (animated);
        }

        private void DeleteAllPDF()
        {
            try
            {
                IFileSystem fs = Mvx.Resolve<IFileSystem> ();
                string path = fs.GetPath ("documents://Schedules/");
                RemoveAllFiles(fs, path);
            }
            catch(Exception e)
            {
                Mvx.Error ("Error deleting PDFs - {0}", e.Message);
            }
        }

        private void RemoveAllFiles(IFileSystem fs, string dir)
        {
            string[] dirs = fs.Directories(dir);
            string[] files = fs.Files(dir);
            foreach(string f in files)
            {
                fs.Delete(f);
            }

            if (dirs.Length == 0)
            {
                Directory.Delete (dir);
            }
            else
            {
                foreach (string d in dirs)
                {
                    RemoveAllFiles (fs, d);
                }
            }
        }

        private string ExportScheduleToPDF()
        {
            try
            {
                const float viewWidth = 1024;
                const float viewHeight = 768;
                
                const float rowWidth = 1024;
                const float rowHeight = 44;
                const float seperatorSize = 1;
                const float pageWidth = 595;
                const float pageHeight = 842;
                const float margin = 16;
                const float titleHeight = 48;
                const float footerHeight = 42;
                
                float headerHeight = HeaderView.Frame.Height;
                float headerWidth = HeaderView.Frame.Width;
                
                int maxrows = (int)((viewHeight - headerHeight - footerHeight - titleHeight) / rowHeight); //Currently 12
                
                ScheduleGridViewModel vm = DataContext as ScheduleGridViewModel;
                List<EmployeeData> data = vm.Schedule.Employees;
                
                int totalPages = (data.Count / maxrows) + ((data.Count % maxrows == 0) ? 0 : 1);
                
                TableCell cellToUse = null;
                
                if (vm.Schedule.DayIndex.HasValue)
                {
                    object[] views = GridDayCell.Nib.Instantiate (null, null);
                    if (views == null || views.Length == 0 || !(views[0] is GridDayCell))
                        throw new Exception ("GridDayCell not found for PDF builder");
                    
                    cellToUse = views[0] as GridDayCell;
                }
                else
                {
                    object[] views = GridWeekCell.Nib.Instantiate (null, null);
                    if (views == null || views.Length == 0 || !(views[0] is GridWeekCell))
                        throw new Exception ("GridWeekCell not found for PDF builder");
                    
                    cellToUse = views[0] as GridWeekCell;
                }
                
                // Creates a mutable data object for updating with binary data, like a byte array
                var pdfData = new NSMutableData();

                // Work out the scale we need to apply tot eh view to fit snug to the page, we then have to recalcuate the Left Margin to center
                float scale = (pageWidth - 2*margin ) / viewHeight;//Rotated 90
                float viewWidthScaled = viewWidth * scale;
                float diff = (pageHeight - viewWidthScaled) - (margin * 2);
                
                for (int currentPage = 0; currentPage < totalPages; currentPage++)
                {
                    RectangleF pageBounds = new RectangleF (margin, margin + (diff / 2), pageWidth, pageHeight);
                    
                    // Points the pdf converter to the mutable data object and to the UIView to be converted
                    if(currentPage==0)
                        UIGraphics.BeginPDFContext(pdfData, pageBounds, null);


                    UIGraphics.BeginPDFPage();
                    CGContext pdfContext = UIGraphics.GetCurrentContext();
                    pdfContext.RotateCTM ((float)(90 * Math.PI / 180));
                    pdfContext.TranslateCTM (0f, -pageWidth);
                    pdfContext.ScaleCTM (scale, scale);
                    
                    //Render BG
                    UIView pageView = new UIView (new RectangleF (0, 0, viewWidth, viewHeight));
                    //Test - Shows you the view size
//                    pageView.Layer.BorderWidth = 1;
//                    pageView.Layer.BorderColor = UIColor.LightGray.CGColor;
                    
                    UIView seperator = new UIView (new RectangleF(0, 0, rowWidth, seperatorSize));
                    seperator.BackgroundColor = UIColor.Black;
                    pageView.BackgroundColor = UIColor.Clear;
                    
                    //Render header
                    RectangleF origFrame = new RectangleF(HeaderView.Frame.X, HeaderView.Frame.Y, HeaderView.Frame.Width, HeaderView.Frame.Height);
                    HeaderView.RemoveFromSuperview();
                    pageView.Add (HeaderView);
                    HeaderView.Frame = new RectangleF (0, titleHeight, HeaderView.Frame.Width, HeaderView.Frame.Height);
                    pageView.Layer.ContentsScale = 1;//Don't want retina
                    pageView.Layer.RenderInContext (pdfContext);
                    
                    //Remove header and add data objects
                    HeaderView.RemoveFromSuperview ();
                    Add (HeaderView);
                    HeaderView.Frame = origFrame;
                    pageView.Add (cellToUse);
                    pageView.Add (seperator);
                    
                    RectangleF rowBounds = new RectangleF (0, headerHeight + titleHeight, rowWidth, rowHeight);
                    for (int i = currentPage * maxrows; i < data.Count && i < (currentPage + 1) * maxrows; i++)
                    {
                        cellToUse.Frame = rowBounds;
                        cellToUse.DataContext = data[i];
                        cellToUse.BindOnce ();
                        cellToUse.OnShow ();
                        
                        seperator.Frame = new RectangleF (0, rowBounds.Y + rowBounds.Height - seperatorSize, rowWidth, seperatorSize);
                        
                        pageView.Layer.RenderInContext (pdfContext);
                        rowBounds.Y += rowBounds.Height;
                    }
                    
                    //Final UI - Seperator at the left and right of the table, with Header and Footer text
                    seperator.RemoveFromSuperview ();
                    cellToUse.RemoveFromSuperview ();
                    
                    float seperatorHeight = rowBounds.Y - titleHeight;
                    
                    UIView leftLine = new UIView (new RectangleF (0, titleHeight, seperatorSize, seperatorHeight));
                    UIView rightLine = new UIView(new RectangleF (viewWidth - 1, titleHeight, seperatorSize, seperatorHeight));
                    leftLine.BackgroundColor = UIColor.Black;
                    rightLine.BackgroundColor = UIColor.Black;
                    
                    UILabel titleLabel = new UILabel (new RectangleF (0, 0, viewWidth, titleHeight));
                    titleLabel.Text = GetTitle (vm);
                    titleLabel.Lines = 0;
                    titleLabel.TextAlignment = UITextAlignment.Center;
                    
                    UILabel footerLabel = new UILabel (new RectangleF (0, viewHeight - footerHeight, viewWidth, footerHeight));
                    footerLabel.Text = string.Format ("Page {0} of {1}", currentPage + 1, totalPages);
                    footerLabel.Lines = 0;
                    footerLabel.TextAlignment = UITextAlignment.Center;
                    
                    pageView.Add (leftLine);
                    pageView.Add (rightLine);
                    pageView.Add (titleLabel);
                    pageView.Add (footerLabel);
                    pageView.Layer.RenderInContext (pdfContext);
                    
                    UIView[] viewsToDispose = new UIView[]{ cellToUse, titleLabel, footerLabel, seperator, leftLine, rightLine };
                    foreach (UIView v in viewsToDispose)
                    {
                        v.RemoveFromSuperview ();
                        v.Dispose ();
                    }
                }
                
                // remove PDF rendering context
                UIGraphics.EndPDFContent();
                
                SessionData session = Mvx.Resolve<SessionData> ();
                IFileSystem file = Mvx.Resolve<IFileSystem> ();
                //Make directory incase it doesn't exist
                file.MkDir(string.Format("documents://Schedules/{0}", session.Site.SiteID));
                
                string titleStr = GetTitle (vm).Replace ('/', '-').Replace (",", string.Empty); 
                string path = file.GetPath (string.Format ("documents://Schedules/{0}/Schedule {1}.pdf", session.Site.SiteID, titleStr));
                int c = 1;
                while (file.Exists (path))
                {
                    path = file.GetPath (string.Format ("documents://Schedules/{0}/Schedule {1} ({2}).pdf", session.Site.SiteID, titleStr, c++));
                }
                
                Mvx.Trace ("Saved pdf to {0}", path);
                pdfData.Save(path, true);
                return path;
            }
            catch(Exception e)
            {
                Mvx.Error ("Error printing PDF - {0}", e.Message);
                m_alert.ShowOK("Error", string.Format("An error occured when trying to produce the schedule PDF. {0}", e.Message));
                return string.Empty;
            }
        }

        private string GetTitle(ScheduleGridViewModel vm)
        {
            //Date - Taken from ScheduleViewModel
            DateTime start = vm.Schedule.StartDate;
            bool weekNumberDisplayed = vm.ConfigurationService.WeekNumberDisplayed;
            ScheduleService scheduleService = Mvx.Resolve<ScheduleService> ();
            if (vm.Schedule.DayIndex.HasValue)
            {
                var day = start.AddDays (vm.Schedule.DayIndex.Value);
                int weekNumber = scheduleService.AccountingHelper.GetWeekNumberForDate (start);
                return weekNumberDisplayed ?
                    string.Format (vm.Localiser.Get("week_starting_label"), weekNumber, day.ToString(vm.Localiser.Get("dddd, dd/MM/yyyy"))) :
                    string.Format (day.ToString(vm.Localiser.Get("dddd, dd/MM/yyyy")));
            }
            else
            {
                int weekNumber = scheduleService.AccountingHelper.GetWeekNumberForDate (start);
                return weekNumberDisplayed ?
                    string.Format (vm.Localiser.Get("week_starting_label") , weekNumber,  start.ToString (vm.Localiser.Get("dd/MM/yyyy"))) :
                    string.Format (vm.Localiser.Get("starting_label"),  start.ToString (vm.Localiser.Get("dd/MM/yyyy")));
            }
        }
    }
}
