﻿using System;
using System.Collections.Generic;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ShinobiCharts;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public class ScheduleGraphDataSource : SChartDataSource
    {
        public const int c_weekHours = 24 * 7; // Hours per week

        private string[] m_columnTitles;
        private UIColor[] m_columnColors;
        private IStringService m_stringLocaliser;

        public int? DayIndex { get; set; }
        public AccuracyData Data { get; set; }

        public ScheduleGraphDataSource ()
            : base ()
        {
            m_stringLocaliser = Mvx.Resolve<IStringService> ();

            m_columnTitles = new string[] {  m_stringLocaliser.Get("chart_legend_under"), m_stringLocaliser.Get("chart_legend_correct"), m_stringLocaliser.Get("chart_legend_over") };
            m_columnColors = new UIColor[] {
                ColorUtility.FromHexString ("0xFFE281"),
                ColorUtility.FromHexString ("0xD5E1EF"),
                ColorUtility.FromHexString ("0xFFB3B3")
            };
        }

        public override int GetNumberOfSeries (ShinobiChart chart)
        {
            return 4;
        }

        public override SChartSeries GetSeries (ShinobiChart chart, int dataSeriesIndex)
        {
            if (dataSeriesIndex == 3)
            {
                var series = new SChartLineSeries ();
                series.Title = m_stringLocaliser.Get("chart_title");
                series.Baseline = new NSNumber(0);
                series.CrosshairEnabled = true;
                series.SelectionMode = SChartSelection.Point;
                series.Style.LineWidth = new NSNumber(2);
                series.Style.LineColor = ColorUtility.FromHexString ("0x4476AE");
                series.Style.ShowFill = false;
                series.Style.PointStyle.ShowPoints = true;
                series.Style.PointStyle.InnerRadius = new NSNumber(1);
                series.Style.PointStyle.Radius = new NSNumber(2);
                series.Style.PointStyle.Color = UIColor.Blue;

                return series;
            }
            else
            {
                var series = new SChartColumnSeries();

                series.AnimationEnabled = true;
                series.EntryAnimation = SChartAnimation.GrowVerticalAnimation();
                series.EntryAnimation.Duration = 1.0f;
                series.Baseline = new NSNumber(0);
                series.Title = m_columnTitles[dataSeriesIndex];
                series.CrosshairEnabled = true;
                series.SelectionMode = SChartSelection.Point;
                series.StackIndex = new NSNumber(0);
                series.Style.AreaColor = m_columnColors[dataSeriesIndex];
                series.Style.AreaColorGradient = series.Style.AreaColor;
                series.Style.LineColor = series.Style.AreaColor;

                return series;
            }
        }

        public override int GetNumberOfDataPoints (ShinobiChart chart, int dataSeriesIndex)
        {
            if (Data == null)
                return 0;

            return DayIndex.HasValue ? ScheduleTime.DayIntervals : c_weekHours;
        }

        public override SChartData GetDataPoint (ShinobiChart chart, int dataIndex, int dataSeriesIndex)
        {
            var point = new SChartDataPoint ();
            NSNumber zero = new NSNumber (0);

            if (DayIndex.HasValue)
            {
                if (dataIndex < ScheduleTime.DayIntervals)
                {
                    double demand = Data.GetDemandInterval (DayIndex.Value, dataIndex);
                    double scheduled = Data.GetScheduledInterval (DayIndex.Value, dataIndex);
                    DateTime time = Data.GetTime (DayIndex.Value, dataIndex);
                    NSDate date = DateTime.SpecifyKind(time, DateTimeKind.Utc);
                    point.XValue = date;

                    switch(dataSeriesIndex)
                    {
                        case 0: point.YValue = demand > scheduled ? new NSNumber(scheduled) : zero; break;
                        case 1: point.YValue = demand == scheduled ? new NSNumber(scheduled) : zero;  break;
                        case 2: point.YValue = demand < scheduled ? new NSNumber(scheduled) : zero;  break;
                        case 3: point.YValue = new NSNumber(demand); break;
                    }
                }
            }
            else
            {
                if (dataIndex < c_weekHours)
                {
                    int index = dataIndex * ScheduleTime.HourIntervals;
                    double demand = 0;
                    double scheduled = 0;
        
                    for (int i=0; i<4; ++i)
                    {
                        demand += Data.GetDemandInterval (index + i);
                        scheduled += Data.GetScheduledInterval (index + i);
                    }
 
                    demand = Math.Ceiling(demand / 4.0f);
                    scheduled = Math.Ceiling(scheduled / 4.0f);
                    DateTime time = Data.GetTime (index);
                    NSDate date = DateTime.SpecifyKind(time, DateTimeKind.Utc);
                    point.XValue = date;

                    switch(dataSeriesIndex)
                    {
                        case 0: point.YValue = demand > scheduled ? new NSNumber(scheduled) : zero; break;
                        case 1: point.YValue = demand == scheduled ? new NSNumber(scheduled) : zero;  break;
                        case 2: point.YValue = demand < scheduled ? new NSNumber(scheduled) : zero;  break;
                        case 3: point.YValue = new NSNumber(demand); break;
                    }
                }
            }
            
            return point;
        }
    }
}