﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using MonoTouch.Foundation;

namespace WFM.iOS
{
    public partial class TermsOfServiceView : ConsortiumModalView
    {
        private IStringService Localiser { get; set;}

        private string m_termsOfServiceFileLocation;
        public string TermsOfServiceFileLocation
        {
            get { return m_termsOfServiceFileLocation; }
            set
            {
                m_termsOfServiceFileLocation = value;
                //Set WebView NSURL
                if(!string.IsNullOrEmpty(value))
                {
                    
                    var urlpath = NSBundle.MainBundle.GetUrlForResource(value, "rtf");
                    var urlreq = new NSUrlRequest(urlpath);
                    WebView.LoadRequest(urlreq);
                }
            }
        }

        public TermsOfServiceView()
            : base (IsPhone ? "TermsOfServiceView_iPhone" : "TermsOfServiceView_iPad", null)
        {
            Localiser = Mvx.Resolve<IStringService>();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            TitleLabel.Text = Localiser.Get("terms_of_use_title");

            string accept = Localiser.Get("terms_of_use_accept");
            string cancel = Localiser.Get("terms_of_use_cancel");

            AcceptButton.SetTitle(accept,UIControlState.Normal);
            CancelButton.SetTitle(cancel,UIControlState.Normal);

            StyleButton(AcceptButton, new UIColor(0.65f, 0.81f, 0.21f, 1), UIColor.Black);
            StyleButton(CancelButton, new UIColor(0.77f, 0.29f, 0.29f, 1), UIColor.White);

            // View Model Binding
            var set = this.CreateBindingSet<TermsOfServiceView, TermsOfServiceViewModel>();
            set.Bind(this).For(v => v.TermsOfServiceFileLocation).To(vm => vm.TermsOfServiceFileLocation);
            set.Bind(AcceptButton).To("OnAccept");
            set.Bind(CancelButton).To("OnCancel");
            set.Apply();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);

            WebView.ShouldStartLoad += OnWebViewLinkClicked;
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear (animated);

            WebView.ShouldStartLoad -= OnWebViewLinkClicked;
        }

        private void StyleButton(UIButton button, UIColor bgcol, UIColor txtCol)
        {
            button.Layer.CornerRadius = 8;
            button.Layer.BorderWidth = 1;
            button.Layer.BorderColor = UIColor.LightGray.CGColor;
            button.BackgroundColor = bgcol;
            button.SetTitleColor(txtCol, UIControlState.Normal);
        }

        private bool OnWebViewLinkClicked(UIWebView webview, NSUrlRequest request, UIWebViewNavigationType navigationType)
        {
            if(navigationType == UIWebViewNavigationType.LinkClicked)
            {
                UIApplication.SharedApplication.OpenUrl(request.Url);
                return false;
            }
            return true;
        }
    }
}