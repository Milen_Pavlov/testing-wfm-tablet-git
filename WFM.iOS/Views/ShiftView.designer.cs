// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ShiftView")]
	partial class ShiftView
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem AddBreakButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem AddJobButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem AddMealButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem AddRoleButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CallOffButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem CallOffButtonHolder { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem DeleteButtonHolder { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView DeletionView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIToolbar EditToolbar { get; set; }

		[Outlet]
		Consortium.Client.iOS.TableControl ShiftDetailTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView ShiftSummaryView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIToolbar ShiftToolbar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton UnfillButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem UnfillButtonHolder { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AddBreakButton != null) {
				AddBreakButton.Dispose ();
				AddBreakButton = null;
			}

			if (AddJobButton != null) {
				AddJobButton.Dispose ();
				AddJobButton = null;
			}

			if (AddMealButton != null) {
				AddMealButton.Dispose ();
				AddMealButton = null;
			}

			if (CallOffButton != null) {
				CallOffButton.Dispose ();
				CallOffButton = null;
			}

			if (CallOffButtonHolder != null) {
				CallOffButtonHolder.Dispose ();
				CallOffButtonHolder = null;
			}

			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}

			if (DeleteButtonHolder != null) {
				DeleteButtonHolder.Dispose ();
				DeleteButtonHolder = null;
			}

			if (DeletionView != null) {
				DeletionView.Dispose ();
				DeletionView = null;
			}

			if (EditToolbar != null) {
				EditToolbar.Dispose ();
				EditToolbar = null;
			}

			if (ShiftDetailTable != null) {
				ShiftDetailTable.Dispose ();
				ShiftDetailTable = null;
			}

			if (ShiftSummaryView != null) {
				ShiftSummaryView.Dispose ();
				ShiftSummaryView = null;
			}

			if (ShiftToolbar != null) {
				ShiftToolbar.Dispose ();
				ShiftToolbar = null;
			}

			if (AddRoleButton != null) {
				AddRoleButton.Dispose ();
				AddRoleButton = null;
			}

			if (UnfillButton != null) {
				UnfillButton.Dispose ();
				UnfillButton = null;
			}

			if (UnfillButtonHolder != null) {
				UnfillButtonHolder.Dispose ();
				UnfillButtonHolder = null;
			}
		}
	}
}
