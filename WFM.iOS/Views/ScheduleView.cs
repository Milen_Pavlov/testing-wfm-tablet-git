﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.CoreGraphics;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using WFM.Core;
using System.Collections.Generic;

namespace WFM.iOS
{
    public partial class ScheduleView : WFMTabView
    {
        TimePopover m_timePopover;
        FilterPopover m_filterPopover;
        UIBarButtonItem m_refresh;
        UIBarButtonItem m_startTime;
        UIBarButtonItem m_filter;
        NotificationBarButtonItem m_unfilledShifts;
        UIBarButtonItem m_edit;
        UIImage[] m_editButtons;

        public ScheduleViewModel VM
        {
            get
            {
                return (ScheduleViewModel)ViewModel;
            }
        }

        public UISegmentedControl TabSelect 
        { 
            get; 
            set; 
        }

		public UIColor ButtonColor
		{
			get { return UIColor.White; } 
			set
			{
				NextButton.TintColor = value;
				NextButton.Layer.BorderColor = value.CGColor;
				PrevButton.TintColor = value;
				PrevButton.Layer.BorderColor = value.CGColor;
                DateButton.TintColor = value;
                DateButton.Layer.BorderColor = value.CGColor;
                //DateButton.SetTitleColor(value, UIControlState.Normal);
			}
		}
  
		public List<string> Segments
		{
			get { return null; }
			set 
			{
				List<string> segs = value;

				if (segs == null)
					return;

				int current = WeekSegment.SelectedSegment;
				WeekSegment.RemoveAllSegments ();
				for (int i = 0; i < segs.Count; i++)
				{
					WeekSegment.InsertSegment (segs [i], i, false);
				}

				if (current < segs.Count)
					WeekSegment.SelectedSegment = current;
				else
					WeekSegment.SelectedSegment = 0;
			}
		}

        public bool TintEditColor
        {
            get{ return false; }
            set
            {
                if(m_edit != null)
                {
                    if (value)
                    {
                        m_edit.TintColor = new UIColor (0.80f, 0.33f, 0.33f, 1);
                    }
                    else
                    {
                        m_edit.TintColor = new UIColor (1, 1, 1, 1);
                    }
                }
            }
        }

        public string DateButtonValue
        {
            get{ return null; }
            set
            {
                DateButton.SetTitle (value, UIControlState.Normal);
            }
        }

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        private bool m_canFilterByTime;
        public bool CanFilterByTime
        {
            get { return m_canFilterByTime; }
            set { m_canFilterByTime = value; UpdateUIBarButtons(); }
        }

        public ScheduleView()
            : base (IsPhone ? "ScheduleView_iPhone" : "ScheduleView_iPad", null)
        {
            m_editButtons = new UIImage[] { UIImage.FromBundle("Pencil"), UIImage.FromBundle("Confirm") };
        }

        protected override UIView GetTabContentView()
        {
            return TabContent;
        }

        private void OnTabSelect (object sender, EventArgs e)
        {
            ((ScheduleViewModel)ViewModel).OnTabSelect(TabSelect.SelectedSegment);
        }

        private void OnWeekSelect (object sender, EventArgs e)
        {
            ((ScheduleViewModel)ViewModel).OnWeekSelect(WeekSegment.SelectedSegment);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            TabSelect = new UISegmentedControl ( VM.TabNames.ToArray() );

            // Show text if we only have one option
            if (VM.TabNames.Count == 1)
            {
                Title = VM.TabNames[0];
            }
            else
            {
                TabSelect.SelectedSegment = 0;
                TabSelect.AddTarget (OnTabSelect, UIControlEvent.ValueChanged);
                NavigationItem.TitleView = TabSelect;
            }

            // Popovers
            m_timePopover = new TimePopover ();
            m_filterPopover = new FilterPopover ();

            //Bar Buttons
            m_startTime = new UIBarButtonItem (UIImage.FromBundle ("Time"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_timePopover.PresentFromBarButtonItem(m_startTime, UIPopoverArrowDirection.Up, true);
                m_filterPopover.Dismiss(true);
            });


            m_filter = new UIBarButtonItem (UIImage.FromBundle ("Filter"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_filterPopover.PresentFromBarButtonItem(m_filter, UIPopoverArrowDirection.Up, true);
                m_timePopover.Dismiss(true);
            });


            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_filterPopover.Dismiss(true);
                m_timePopover.Dismiss(true);
            });


            if (!VM.ReadOnlySchedule)
            {
                m_edit = new UIBarButtonItem (m_editButtons[0], UIBarButtonItemStyle.Plain, (v1, v2) =>
                    { 
                        m_filterPopover.Dismiss (true);
                        m_timePopover.Dismiss (true);
                    });


                if (VM.ShowFillUnfilledShifts) 
                {
                    m_unfilledShifts = new NotificationBarButtonItem (UIImage.FromBundle ("icon_unfilled_shifts_normal"), UIBarButtonItemStyle.Plain, (v1, v2) => {
                        m_filterPopover.Dismiss (true);
                        m_timePopover.Dismiss (true);
                        VM.ShowUnfilledShifts ();
                    });

                }
            }

            //Update UI Bar Buttons - This may trigger again
            UpdateUIBarButtons();

            // Callback for changing week/day
            WeekSegment.AddTarget(OnWeekSelect, UIControlEvent.ValueChanged);

            // Button Layout
            PrevButton.Layer.BorderWidth = 1;
            PrevButton.Layer.CornerRadius = 4;
            NextButton.Layer.BorderWidth = 1;
            NextButton.Layer.CornerRadius = 4;
            DateButton.Layer.BorderWidth = 1;
            DateButton.Layer.CornerRadius = 4;

            // ios7 layout
            if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleView, ScheduleViewModel>();
            set.Bind (m_refresh).For (b => b.Enabled).To (m => m.Loading).WithConversion (new HiddenConverter (), null);
            set.Bind (m_refresh).To("OnRefresh");

            set.Bind (m_unfilledShifts).For (b => b.Enabled).To (vm => vm.UnfilledShiftsEnabled);
            set.Bind (m_unfilledShifts).For (b => b.NotificationCount).To (vm => vm.UnfilledShiftsCount);

            set.Bind (m_timePopover).For (b => b.Selected).To("OnSetTime");
            set.Bind (m_timePopover).For (b => b.StartHour).To(m => m.StartHour);

            set.Bind (m_filterPopover).For (b => b.Apply).To("OnApplyFilter");
            set.Bind (m_filterPopover).For (b => b.CurrentFilter).To(m => m.CurrentFilter);
            set.Bind (m_filterPopover).For (b => b.Filters).To(m => m.Filters);

            set.Bind (m_edit).For (b => b.Image).To(m => m.EditMode).WithConversion (new ImageBoolToIndexConvertor (), m_editButtons);
            set.Bind (m_edit).For (b => b.Enabled).To (m => m.CanEdit);

            set.Bind(m_startTime).For(b => b.Enabled).To(m => m.CanChangeStartTime);


            set.Bind (m_edit).To("OnEdit");
            set.Bind (this).For (m => m.DateButtonValue).To (m => m.Date);
            set.Bind (this).For (v => v.CanFilterByTime).To (vm => vm.CanFilterByTime);
            set.Bind (DateButton).To("OnDateButtonPressed");
            set.Bind (OverviewLabel).To(m => m.Totals);
            set.Bind (NextButton).To("OnMoveNext");
            set.Bind (PrevButton).To("OnMovePrev");
            set.Bind (TabContent).For (b => b.Hidden).To (m => m.Loading);
            set.Bind (LoadingActivity).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingLabel).To (m => m.LoadingMessage);
            set.Bind (WeekSegment).For(m => m.SelectedSegment).To (m => m.WeekSelect);
            set.Bind (FilterLabel).To (m => m.FilterText);
            set.Bind (TabSelect).For(b => b.TintColor).To(m => m.Theme.SecondaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (WeekSegment).For(b => b.TintColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
	    	set.Bind (this).For (v => v.Segments).To (vm => vm.Segments);
            set.Bind (this).For (v => v.TintEditColor).To (vm => vm.EditMode);
            set.Apply();
        }

        private void UpdateUIBarButtons()
        {
            if(NavigationItem.RightBarButtonItems != null)
                NavigationItem.RightBarButtonItems = null;

            List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();

            if(CanFilterByTime)
                barButtons.Add (m_startTime);

            barButtons.Add (m_filter);
            barButtons.Add (m_refresh);

            if (!VM.ReadOnlySchedule)
            {
                barButtons.Add (m_edit);

                if (VM.ShowFillUnfilledShifts) 
                {
                    barButtons.Add (m_unfilledShifts);
                }
            }

            NavigationItem.RightBarButtonItems = barButtons.ToArray();
        }

        protected override void ViewClosed()
        {
            base.ViewClosed ();
           
            m_timePopover.Dismiss (false);
            m_filterPopover.Dismiss (false);
        }

        public override void ViewOpened ()
        {
            base.ViewOpened ();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);
        }

        public override bool Present ()
        {
            return base.Present ();
        }

        public override void ViewWillAppear (bool animated)
        {
            this.TabContent.LayoutIfNeeded();
            base.ViewWillAppear (animated);
        }
    }
}
