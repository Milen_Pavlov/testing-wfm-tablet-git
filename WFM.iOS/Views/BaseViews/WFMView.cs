﻿using System;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using WFM.Core;
using Cirrious.CrossCore;
using System.Collections.Generic;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public class WFMView : ConsortiumView
    {
        private readonly SessionData m_sessionData;
        private readonly IESSConfigurationService m_settings;

        private UIBarButtonItem m_storeBarItem;

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        public bool RefreshTitle
        {
            get { return true; }
            set
            {
                UpdateStoreInNavBar ();
            }
        }


        public WFMView (string nibName, NSBundle bundle) : base(nibName, bundle)
        {
            m_settings = Mvx.Resolve<IESSConfigurationService> ();
            m_sessionData = Mvx.Resolve<SessionData> ();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var set = this.CreateBindingSet<WFMView, WFMViewModel>();
            set.Bind (this).For (v => v.RefreshTitle).To (vm => vm.RefreshTitle);
            set.Apply ();
        }

        public void UpdateStoreInNavBar()
        {
            //Add Store to navbar
            if (!m_settings.ShowStoreInNavBar)
                return;
            
            if (m_storeBarItem != null)
            {
                List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();
                if (NavigationItem.LeftBarButtonItems != null)
                    barButtons.AddRange (NavigationItem.LeftBarButtonItems);

                if(barButtons.Contains(m_storeBarItem))
                    barButtons.Remove (m_storeBarItem);

                m_storeBarItem = null;
                NavigationItem.LeftBarButtonItems = barButtons.ToArray();
            }

            string store = string.Empty;
            if (m_settings.ShowSiteNumber)
            {
                store = string.Format ("{0}, {1}", m_sessionData.Site.Name, m_sessionData.Site.LongName);
            }
            else
            {
                store = m_sessionData.Site.LongName;
            }

            if (!string.IsNullOrEmpty (store))
            {
                //Add to nav bar in left (and make it not clickable but look normal
                m_storeBarItem = new UIBarButtonItem (store, UIBarButtonItemStyle.Plain, null);
                UITextAttributes attr = new UITextAttributes ()
                    {
                        TextColor = UIColor.White,
                        TextShadowColor = UIColor.Clear,
                    };
                m_storeBarItem.SetTitleTextAttributes (attr, UIControlState.Normal);
                m_storeBarItem.SetTitleTextAttributes (attr, UIControlState.Application);
                m_storeBarItem.SetTitleTextAttributes (attr, UIControlState.Disabled);
                m_storeBarItem.Enabled = false;

                List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();
                if (NavigationItem.LeftBarButtonItems != null)
                    barButtons.AddRange (NavigationItem.LeftBarButtonItems);

                barButtons.Add (m_storeBarItem);

                NavigationItem.LeftBarButtonItems = barButtons.ToArray();
            }
        }
    }
}

