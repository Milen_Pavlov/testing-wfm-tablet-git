﻿using System;
using System.Drawing;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;

namespace WFM.iOS
{
    public partial class SalesForecastView : ConsortiumView
    {
        UIBarButtonItem m_refresh;
        UIBarButtonItem m_submitButton;
        UIBarButtonItem m_cancelButton;
        private UITapGestureRecognizer m_gesture;
        //Keyboard notification and shifting work, eventually should go in ConsortiumView
        private NSObject m_keyboardShowObserver;
        private NSObject m_keyboardHideObserver;
        private RectangleF m_originalRootFrame;
        private RectangleF m_desiredRootFrame;

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); }
        }
            
        public SalesForecastView ()
            : base (IsPhone ? "SalesForecastView_iPhone" : "SalesForecastView_iPad", null)
        {
            Title = "Sales Forecast";
        }

        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();
            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                
            });
            barButtons.Add (m_refresh);
            m_submitButton = new UIBarButtonItem ("Submit", UIBarButtonItemStyle.Plain, (v1,v2) => { 

            });
            barButtons.Add (m_submitButton);
            m_cancelButton = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, (v1,v2) => { 

            });
            barButtons.Add (m_cancelButton);
            NavigationItem.RightBarButtonItems = barButtons.ToArray();

            // Button Layout
            DatePrev.Layer.BorderWidth = 1;
            DatePrev.Layer.CornerRadius = 4;
            DateNext.Layer.BorderWidth = 1;
            DateNext.Layer.CornerRadius = 4;
            GroupButton.Layer.BorderWidth = 1;
            GroupButton.Layer.CornerRadius = 4;

            DataGrid.RegisterCell<SalesForecastViewCell> ();
            DataGrid.RegisterSection<SalesForecastViewHeaderCell> ();
            DataGrid.AllowsSelection = false;

           // DataGrid.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            DataGrid.SectionHeaderHeight = 40;

            NSAction action = () => { 
                DataGrid.EndEditing(true);
            };
            m_gesture = new UITapGestureRecognizer (action);
            m_gesture.NumberOfTapsRequired = 1;
            View.AddGestureRecognizer (m_gesture);

            m_cancelButton.Enabled = false;
            m_submitButton.Enabled = false;
            GroupButton.Enabled = false;

            var set = this.CreateBindingSet<SalesForecastView, SalesForecastViewModel> ();
            set.Bind (DateText).To (m => m.Date);
            set.Bind (DateNext).To ("OnMoveNext");
            set.Bind (DatePrev).To ("OnMovePrev");
            set.Bind (GroupButton).To ("ShowGroupList");
            set.Bind (DataGrid).To (m => m.Items);
            set.Bind (DataGrid).For (m => m.CustomCommand1).To ("OnRowChanged");
            set.Bind (LoadingView).For (p => p.Hidden).To (p => p.IsLoadingViewHidden);
            set.Bind (m_refresh).For (p => p.Enabled).To (p => p.IsLoadingViewHidden);
            set.Bind (m_refresh).To ("GetForecastData");
            set.Bind (m_submitButton).To ("SubmitChanges");
            set.Bind (m_submitButton).For (p => p.Enabled).To (p => p.EnableSubmitButton);
            set.Bind (m_cancelButton).To ("CancelChanges");
            set.Bind (m_cancelButton).For (p => p.Enabled).To (p => p.EnableCancelButton);
            set.Bind (GroupButton).For (p => p.Enabled).To (p => p.ReceivedGroupData);
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);

            set.Bind (this).For (v => v.SelectedHighlightedValue).To (m => m.SelectedGroup.Name);
            set.Bind (this).For (v => v.Day1HighlightedValue).To (m => m.Dates[0]);
            set.Bind (this).For (v => v.Day2HighlightedValue).To (m => m.Dates[1]);
            set.Bind (this).For (v => v.Day3HighlightedValue).To (m => m.Dates[2]);
            set.Bind (this).For (v => v.Day4HighlightedValue).To (m => m.Dates[3]);
            set.Bind (this).For (v => v.Day5HighlightedValue).To (m => m.Dates[4]);
            set.Bind (this).For (v => v.Day6HighlightedValue).To (m => m.Dates[5]);
            set.Bind (this).For (v => v.Day7HighlightedValue).To (m => m.Dates[6]);

            set.Apply ();
        }

        public UIColor ButtonColor
        {
            get { return UIColor.White; } 
            set
            {
                DateNext.TintColor = value;
                DateNext.Layer.BorderColor = value.CGColor;
                DatePrev.TintColor = value;
                DatePrev.Layer.BorderColor = value.CGColor;
                GroupButton.TintColor = value;
                GroupButton.Layer.BorderColor = value.CGColor;
                GroupButton.SetTitleColor (value, UIControlState.Normal);
            }
        }

        public string SelectedHighlightedValue
        {
            get{ return null; }
            set
            {
                GroupButton.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day1HighlightedValue
        {
            get{ return null; }
            set
            {
                Day1Button.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day2HighlightedValue
        {
            get{ return null; }
            set
            {
                Day2Button.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day3HighlightedValue
        {
            get{ return null; }
            set
            {
                Day3Button.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day4HighlightedValue
        {
            get{ return null; }
            set
            {
                Day4Button.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day5HighlightedValue
        {
            get{ return null; }
            set
            {
                Day5Button.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day6HighlightedValue
        {
            get{ return null; }
            set
            {
                Day6Button.SetTitle (value, UIControlState.Normal);
            }
        }
        public string Day7HighlightedValue
        {
            get{ return null; }
            set
            {
                Day7Button.SetTitle (value, UIControlState.Normal);
            }
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);

            if (m_originalRootFrame == RectangleF.Empty)
                m_originalRootFrame = View.Frame;

            m_keyboardShowObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, OnKeyboardNotification);
            m_keyboardHideObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, OnKeyboardNotification);
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);

            NSNotificationCenter.DefaultCenter.RemoveObserver(m_keyboardShowObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver(m_keyboardHideObserver);
        }

        private void OnKeyboardNotification(NSNotification notification)
        {
            var showing = notification.Name == UIKeyboard.WillShowNotification;
            var keyboardFrame = showing ? UIKeyboard.FrameEndFromNotification(notification) : UIKeyboard.FrameBeginFromNotification(notification);

            //Get First responder
            UIView firstResponder = GetFirstResponder(View);
            RectangleF frame = RectangleF.Empty;
            if (firstResponder != null)
            {
                frame = View.ConvertRectFromView (new RectangleF (0, 0, firstResponder.Frame.Width, firstResponder.Frame.Height), firstResponder);
            }

            //Check if the base of the Sign in button will be under the keyboard, and by how much
            m_desiredRootFrame = m_originalRootFrame;
            if (showing)
            {
                float bottomOfArea = frame.Bottom + 10;
                float keyboardHeight = keyboardFrame.Height;
                float topOfKeyboard = View.Frame.Height - keyboardHeight;

                if (bottomOfArea > topOfKeyboard)
                    m_desiredRootFrame.Y -= (bottomOfArea - topOfKeyboard);
            }

            NSNumber duration = (NSNumber)notification.UserInfo.ValueForKey(UIKeyboard.AnimationDurationUserInfoKey);
            NSNumber curve = (NSNumber)notification.UserInfo.ObjectForKey(UIKeyboard.AnimationCurveUserInfoKey);

            double durationVal = (duration.DoubleValue == 0) ? 0.25 : duration.DoubleValue;
            UIViewAnimationOptions curveVal = (curve.Int32Value == 0) ? UIViewAnimationOptions.CurveEaseInOut : (UIViewAnimationOptions) curve.Int32Value;
            UIView.Animate(durationVal, 0, curveVal | UIViewAnimationOptions.BeginFromCurrentState, animation: () =>
                {
                    View.Frame = m_desiredRootFrame;
                }, completion: () =>
                {
                });

        }

        //Used by keyboard notification event to get the correct textview to shift in view
        private UIView GetFirstResponder(UIView root)
        {
            if (root.IsFirstResponder)
                return root;

            foreach (UIView subView in root.Subviews)
            {
                UIView view = GetFirstResponder (subView);
                if (view != null)
                    return view;
            }

            return null;
        }
    }
}

