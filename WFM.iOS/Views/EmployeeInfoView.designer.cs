// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("EmployeeInfoView")]
	partial class EmployeeInfoView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BULabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HomeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel JobLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MobileLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MobileTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BULabel != null) {
				BULabel.Dispose ();
				BULabel = null;
			}

			if (HomeLabel != null) {
				HomeLabel.Dispose ();
				HomeLabel = null;
			}

			if (JobLabel != null) {
				JobLabel.Dispose ();
				JobLabel = null;
			}

			if (MobileLabel != null) {
				MobileLabel.Dispose ();
				MobileLabel = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (MobileTitleLabel != null) {
				MobileTitleLabel.Dispose ();
				MobileTitleLabel = null;
			}
		}
	}
}
