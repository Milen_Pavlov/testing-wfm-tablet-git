// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ResetPasswordView")]
	partial class ResetPasswordView
	{
		[Outlet]
		Consortium.Client.iOS.WebImageControl BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CancelButton { get; set; }

		[Outlet]
		Consortium.Client.iOS.WebImageControl HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Message { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SubmitButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView UserBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField Username { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel UserTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (CancelButton != null) {
				CancelButton.Dispose ();
				CancelButton = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (SubmitButton != null) {
				SubmitButton.Dispose ();
				SubmitButton = null;
			}

			if (Username != null) {
				Username.Dispose ();
				Username = null;
			}

			if (CancelButton != null) {
				CancelButton.Dispose ();
				CancelButton = null;
			}

			if (Message != null) {
				Message.Dispose ();
				Message = null;
			}

			if (SubmitButton != null) {
				SubmitButton.Dispose ();
				SubmitButton = null;
			}

			if (Username != null) {
				Username.Dispose ();
				Username = null;
			}

			if (UserBackgroundView != null) {
				UserBackgroundView.Dispose ();
				UserBackgroundView = null;
			}

			if (UserTitle != null) {
				UserTitle.Dispose ();
				UserTitle = null;
			}
		}
	}
}
