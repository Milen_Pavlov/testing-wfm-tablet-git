// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleView")]
	partial class ScheduleView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton DateButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FilterLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingActivity { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NextButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OverviewLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PrevButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TabContent { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl WeekSegment { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DateButton != null) {
				DateButton.Dispose ();
				DateButton = null;
			}

			if (FilterLabel != null) {
				FilterLabel.Dispose ();
				FilterLabel = null;
			}

			if (LoadingActivity != null) {
				LoadingActivity.Dispose ();
				LoadingActivity = null;
			}

			if (LoadingLabel != null) {
				LoadingLabel.Dispose ();
				LoadingLabel = null;
			}

			if (NextButton != null) {
				NextButton.Dispose ();
				NextButton = null;
			}

			if (OverviewLabel != null) {
				OverviewLabel.Dispose ();
				OverviewLabel = null;
			}

			if (PrevButton != null) {
				PrevButton.Dispose ();
				PrevButton = null;
			}

			if (TabContent != null) {
				TabContent.Dispose ();
				TabContent = null;
			}

			if (WeekSegment != null) {
				WeekSegment.Dispose ();
				WeekSegment = null;
			}
		}
	}
}
