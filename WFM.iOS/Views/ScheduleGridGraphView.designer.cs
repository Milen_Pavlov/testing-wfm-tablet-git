// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleGridGraphView")]
	partial class ScheduleGridGraphView
	{
		[Outlet]
		MonoTouch.UIKit.UIView BottomContainer { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView ControlSeperator { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DownButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton GraphModeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ModeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopContainer { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton UpButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BottomContainer != null) {
				BottomContainer.Dispose ();
				BottomContainer = null;
			}

			if (ControlSeperator != null) {
				ControlSeperator.Dispose ();
				ControlSeperator = null;
			}

			if (DownButton != null) {
				DownButton.Dispose ();
				DownButton = null;
			}

			if (ModeButton != null) {
				ModeButton.Dispose ();
				ModeButton = null;
			}

			if (GraphModeButton != null) {
				GraphModeButton.Dispose ();
				GraphModeButton = null;
			}

			if (TopContainer != null) {
				TopContainer.Dispose ();
				TopContainer = null;
			}

			if (UpButton != null) {
				UpButton.Dispose ();
				UpButton = null;
			}
		}
	}
}
