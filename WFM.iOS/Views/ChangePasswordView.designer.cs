// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ChangePasswordView")]
	partial class ChangePasswordView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton CancelButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField ConfirmPasswordText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField CurrentPasswordText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ErrorMessageLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField NewPasswordText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SubmitButton { get; set; }

		[Action ("ConfirmPasswordDidEndOnExit:")]
		partial void ConfirmPasswordDidEndOnExit (MonoTouch.Foundation.NSObject sender);

		[Action ("CurrentPasswordDidEndOnExit:")]
		partial void CurrentPasswordDidEndOnExit (MonoTouch.Foundation.NSObject sender);

		[Action ("NewPasswordDidEndOnExit:")]
		partial void NewPasswordDidEndOnExit (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ConfirmPasswordText != null) {
				ConfirmPasswordText.Dispose ();
				ConfirmPasswordText = null;
			}

			if (CurrentPasswordText != null) {
				CurrentPasswordText.Dispose ();
				CurrentPasswordText = null;
			}

			if (ErrorMessageLabel != null) {
				ErrorMessageLabel.Dispose ();
				ErrorMessageLabel = null;
			}

			if (NewPasswordText != null) {
				NewPasswordText.Dispose ();
				NewPasswordText = null;
			}

			if (SubmitButton != null) {
				SubmitButton.Dispose ();
				SubmitButton = null;
			}

			if (CancelButton != null) {
				CancelButton.Dispose ();
				CancelButton = null;
			}
		}
	}
}
