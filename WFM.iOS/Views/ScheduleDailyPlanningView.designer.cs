// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleDailyPlanningView")]
	partial class ScheduleDailyPlanningView
	{
		[Outlet]
		Consortium.Client.iOS.CollectionControl HeaderCollection { get; set; }

		[Outlet]
		WFM.iOS.ScheduleDailyPlanningTable PlanningTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TitleHeader { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (HeaderCollection != null) {
				HeaderCollection.Dispose ();
				HeaderCollection = null;
			}

			if (PlanningTable != null) {
				PlanningTable.Dispose ();
				PlanningTable = null;
			}

			if (TitleLabel != null) {
				TitleLabel.Dispose ();
				TitleLabel = null;
			}

			if (TitleHeader != null) {
				TitleHeader.Dispose ();
				TitleHeader = null;
			}
		}
	}
}
