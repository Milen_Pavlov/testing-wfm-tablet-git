// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ModalTextInputView")]
	partial class ModalTextInputView
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem CancelButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIToolbar EditToolbar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem SubmitButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView TextInput { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CancelButton != null) {
				CancelButton.Dispose ();
				CancelButton = null;
			}

			if (SubmitButton != null) {
				SubmitButton.Dispose ();
				SubmitButton = null;
			}

			if (TextInput != null) {
				TextInput.Dispose ();
				TextInput = null;
			}

			if (EditToolbar != null) {
				EditToolbar.Dispose ();
				EditToolbar = null;
			}
		}
	}
}
