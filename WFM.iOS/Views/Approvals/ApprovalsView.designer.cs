// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ApprovalsView")]
	partial class ApprovalsView
	{
		[Outlet]
		WFM.iOS.AlternateBackgroundTable ApprovalList { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView DatePickerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmptyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView FooterView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel[] HeaderLabels { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MainView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NextButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PrevButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl Tabs { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TranslateView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ApprovalList != null) {
				ApprovalList.Dispose ();
				ApprovalList = null;
			}

			if (DateLabel != null) {
				DateLabel.Dispose ();
				DateLabel = null;
			}

			if (DatePickerView != null) {
				DatePickerView.Dispose ();
				DatePickerView = null;
			}

			if (EmptyLabel != null) {
				EmptyLabel.Dispose ();
				EmptyLabel = null;
			}

			if (FooterView != null) {
				FooterView.Dispose ();
				FooterView = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}

			if (MainView != null) {
				MainView.Dispose ();
				MainView = null;
			}

			if (NextButton != null) {
				NextButton.Dispose ();
				NextButton = null;
			}

			if (PrevButton != null) {
				PrevButton.Dispose ();
				PrevButton = null;
			}

			if (Tabs != null) {
				Tabs.Dispose ();
				Tabs = null;
			}

			if (TranslateView != null) {
				TranslateView.Dispose ();
				TranslateView = null;
			}
		}
	}
}
