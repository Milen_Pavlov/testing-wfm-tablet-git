﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.CoreGraphics;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using WFM.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.iOS
{
    public partial class ApprovalsView : WFMView
    {
        private UIBarButtonItem m_refresh;
        private RectangleF m_originalRectangle;
        private RectangleF m_datepickerShiftRectangle;

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        private bool m_loading = false;
        public bool IsLoading
        {
            get { return m_loading; }
            set
            {
                m_loading = value;

                if (LoadingView != null)
                {
                    if (m_loading)
                    {
                        LoadingView.Alpha = 1;
                    }
                    else
                    {
                        UIView.Animate (0.25, 0, UIViewAnimationOptions.BeginFromCurrentState | UIViewAnimationOptions.CurveEaseInOut, () =>
                            {
                                LoadingView.Alpha = 0;
                            }, () =>
                            {
                            });
                    }
                }
            }
        }

        private bool m_showDatePicker = false;
        public bool ShowDatePicker
        {
            get { return m_showDatePicker; }
            set
            {
                m_showDatePicker = value;

                //Animate
                UIView.Animate (0.25, 0, UIViewAnimationOptions.BeginFromCurrentState | UIViewAnimationOptions.CurveEaseInOut, () =>
                    {
                        TranslateView.Frame = (value) ? m_datepickerShiftRectangle : m_originalRectangle;
                        DatePickerView.Alpha = (value) ? 1 : 0;
                    }, () =>
                    {
                    });
            }
        }

        public ApprovalsView()
            : base (IsPhone ? "ApprovalsView_iPhone" : "ApprovalsView_iPad", null)
        {
        }

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            IStringService localiser =  Mvx.Resolve<IStringService> ();
            Title = localiser.Get ("approvals");

            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, null);

            ApprovalList.RegisterCell<ApprovalCell>();
            ApprovalList.SeparatorInset = UIEdgeInsets.Zero;

            // Themeing 
            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            FooterView.BackgroundColor = primary;
            PrevButton.Layer.BorderWidth = 1;
            PrevButton.Layer.CornerRadius = 4;
            NextButton.Layer.BorderWidth = 1;
            NextButton.Layer.CornerRadius = 4;
            NextButton.TintColor = primary;
            NextButton.Layer.BorderColor = primary.CGColor;
            PrevButton.TintColor = primary;
            PrevButton.Layer.BorderColor = primary.CGColor;

            // Setup data
            m_originalRectangle = TranslateView.Frame;
            m_datepickerShiftRectangle = TranslateView.Frame;
            m_datepickerShiftRectangle.Y = DatePickerView.Frame.Bottom;
            m_datepickerShiftRectangle.Height -= DatePickerView.Frame.Height;

            var set = this.CreateBindingSet<ApprovalsView, ApprovalsViewModel>();
            set.Bind (ApprovalList).To (vm => vm.CurrentData);
            set.Bind (ApprovalList).For (v => v.Selected).To ("ApprovalSelected");
            set.Bind (ApprovalList).For (v => v.Hidden).To (vm => vm.IsEmpty);
            set.Bind (Tabs).For (v => v.Enabled).To (vm => vm.Loading).WithConversion ("InvertedBoolean");
            set.Bind (EmptyLabel).To (vm => vm.EmptyText);
            set.Bind (EmptyLabel).For (v => v.Hidden).To (vm => vm.IsEmpty).WithConversion ("InvertedBoolean");
            set.Bind (this).For (v => v.IsLoading).To (vm => vm.Loading);
            set.Bind (this).For (v => v.ShowDatePicker).To (vm => vm.ShowDatePicker);
            set.Bind (m_refresh).For (v => v.Enabled).To (vm => vm.CanRefresh);
            set.Bind (m_refresh).To ("Refresh");
            set.Bind (DateLabel).To (vm => vm.DateString);
            set.Bind (NextButton).To ("OnMoveNext");
            set.Bind (PrevButton).To ("OnMovePrev");
			set.Apply();
		}

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

            NavigationItem.RightBarButtonItems = new UIBarButtonItem[] { m_refresh};

            Tabs.AddTarget (OnTabSelect, UIControlEvent.ValueChanged);
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);

            NavigationItem.RightBarButtonItems = null;

            Tabs.RemoveTarget (OnTabSelect, UIControlEvent.ValueChanged);
        }

        protected override void Dispose (bool disposing)
        {
            if (disposing)
            {
                if (m_refresh != null)
                {
                    m_refresh.Dispose ();
                    m_refresh = null;
                }
            }

            base.Dispose (disposing);
        }

        private void OnTabSelect (object sender, EventArgs e)
        {
            ApprovalsViewModel.ApprovalTab tab = ApprovalsViewModel.ApprovalTab.Pending;
            if (Enum.TryParse<ApprovalsViewModel.ApprovalTab> (Tabs.SelectedSegment.ToString(), out tab))
            {
                ((ApprovalsViewModel)ViewModel).OnTabSelect(tab);
            }
        }
    }
}