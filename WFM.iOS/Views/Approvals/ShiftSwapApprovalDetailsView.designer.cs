// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ShiftSwapApprovalDetailsView")]
	partial class ShiftSwapApprovalDetailsView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton ApproveButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView ApproveImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ApproveLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView ApproveRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DenyButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView DenyImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DenyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView DenyRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView FooterView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoApprovalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView SwappeeWeek { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView SwapperWeek { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView SwatchCurrent { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SwatchCurrentLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView SwatchNew { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SwatchNewLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel WeekStartingLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ApproveButton != null) {
				ApproveButton.Dispose ();
				ApproveButton = null;
			}

			if (ApproveImage != null) {
				ApproveImage.Dispose ();
				ApproveImage = null;
			}

			if (ApproveLabel != null) {
				ApproveLabel.Dispose ();
				ApproveLabel = null;
			}

			if (ApproveRoot != null) {
				ApproveRoot.Dispose ();
				ApproveRoot = null;
			}

			if (DenyButton != null) {
				DenyButton.Dispose ();
				DenyButton = null;
			}

			if (DenyImage != null) {
				DenyImage.Dispose ();
				DenyImage = null;
			}

			if (DenyLabel != null) {
				DenyLabel.Dispose ();
				DenyLabel = null;
			}

			if (DenyRoot != null) {
				DenyRoot.Dispose ();
				DenyRoot = null;
			}

			if (FooterView != null) {
				FooterView.Dispose ();
				FooterView = null;
			}

			if (NoApprovalLabel != null) {
				NoApprovalLabel.Dispose ();
				NoApprovalLabel = null;
			}

			if (SwappeeWeek != null) {
				SwappeeWeek.Dispose ();
				SwappeeWeek = null;
			}

			if (SwapperWeek != null) {
				SwapperWeek.Dispose ();
				SwapperWeek = null;
			}

			if (SwatchCurrent != null) {
				SwatchCurrent.Dispose ();
				SwatchCurrent = null;
			}

			if (SwatchCurrentLabel != null) {
				SwatchCurrentLabel.Dispose ();
				SwatchCurrentLabel = null;
			}

			if (SwatchNew != null) {
				SwatchNew.Dispose ();
				SwatchNew = null;
			}

			if (SwatchNewLabel != null) {
				SwatchNewLabel.Dispose ();
				SwatchNewLabel = null;
			}

			if (WeekStartingLabel != null) {
				WeekStartingLabel.Dispose ();
				WeekStartingLabel = null;
			}
		}
	}
}
