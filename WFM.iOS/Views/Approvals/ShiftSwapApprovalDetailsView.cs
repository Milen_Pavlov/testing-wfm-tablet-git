﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.CoreGraphics;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using WFM.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.iOS
{
    public partial class ShiftSwapApprovalDetailsView : WFMView
    {
        private IStringService m_localiser;

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        public string OwnerName 
        {
            get { return string.Empty; }
            set
            {
                Title = string.Format (m_localiser.Get ("swapshift_title"), value);
            }
        }

        private ShiftSwapWeekControl m_swapperWeek, m_swappeeWeek;

        public ShiftSwapApprovalDetailsView()
            : base (IsPhone ? "ShiftSwapApprovalDetailsView_iPhone" : "ShiftSwapApprovalDetailsView_iPad", null)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            m_localiser =  Mvx.Resolve<IStringService> ();

            // Themeing 
            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);

            FooterView.BackgroundColor = primary;

            // Inject Week Controls and "bind" up
            m_swapperWeek = new ShiftSwapWeekControl();
            m_swapperWeek.View.Frame = new RectangleF (0, 0, SwapperWeek.Frame.Width, SwapperWeek.Frame.Height);
            SwapperWeek.Add (m_swapperWeek.View);

            m_swappeeWeek = new ShiftSwapWeekControl();
            m_swappeeWeek.View.Frame = new RectangleF (0, 0, SwappeeWeek.Frame.Width, SwappeeWeek.Frame.Height);
            SwappeeWeek.Add (m_swappeeWeek.View);

            ApproveLabel.Text = m_localiser.Get ("swapshift_approve");
            DenyLabel.Text = m_localiser.Get ("swapshift_deny");
            NoApprovalLabel.Text = m_localiser.Get ("swapshift_noapproval");
            SwatchCurrentLabel.Text = m_localiser.Get ("swapshift_swatch_current");
            SwatchNewLabel.Text = m_localiser.Get ("swapshift_swatch_new");

            SwatchCurrent.Layer.CornerRadius = 4;
            SwatchCurrent.BackgroundColor = SwapShiftWeekShiftCell.COLOUR_LOSING;

            SwatchNew.Layer.CornerRadius = 4;
            SwatchNew.BackgroundColor = SwapShiftWeekShiftCell.COLOUR_GAINING;

            var set = this.CreateBindingSet<ShiftSwapApprovalDetailsView, ShiftSwapApprovalDetailsViewModel>();
            set.Bind (this).For (v => v.OwnerName).To (vm => vm.SwapperName);
            set.Bind (ApproveRoot).For (v => v.Hidden).To (vm => vm.CanApprove).WithConversion (new HiddenConverter());
            set.Bind (DenyRoot).For (v => v.Hidden).To (vm => vm.CanApprove).WithConversion (new HiddenConverter());
            set.Bind (NoApprovalLabel).For (v => v.Hidden).To (vm => vm.CanApprove);
            set.Bind (m_swappeeWeek).For (v => v.Details).To (vm => vm.Details.Swappee);
            set.Bind (m_swapperWeek).For (v => v.Details).To (vm => vm.Details.Swapper);
            set.Bind (WeekStartingLabel).To (vm => vm.DateLabel);
            set.Bind (ApproveButton).To ("Approve");
            set.Bind (DenyButton).To ("Deny");
            set.Apply();
		}
    }
}