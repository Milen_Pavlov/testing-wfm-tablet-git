// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("DrawerView")]
	partial class DrawerView
	{
		[Outlet]
		Consortium.Client.iOS.TableControl DrawerList { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LogoBackground { get; set; }

		[Outlet]
		Consortium.Client.iOS.WebImageControl LogoImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton OrgSelectBack { get; set; }

		[Outlet]
		Consortium.Client.iOS.TableControl OrgSelectList { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView OrgSelectView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField SearchText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SignOutButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SiteLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView StatusBarView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DrawerList != null) {
				DrawerList.Dispose ();
				DrawerList = null;
			}

			if (LogoBackground != null) {
				LogoBackground.Dispose ();
				LogoBackground = null;
			}

			if (LogoImage != null) {
				LogoImage.Dispose ();
				LogoImage = null;
			}

			if (OrgSelectBack != null) {
				OrgSelectBack.Dispose ();
				OrgSelectBack = null;
			}

			if (OrgSelectList != null) {
				OrgSelectList.Dispose ();
				OrgSelectList = null;
			}

			if (OrgSelectView != null) {
				OrgSelectView.Dispose ();
				OrgSelectView = null;
			}

			if (SearchText != null) {
				SearchText.Dispose ();
				SearchText = null;
			}

			if (SignOutButton != null) {
				SignOutButton.Dispose ();
				SignOutButton = null;
			}

			if (SiteLabel != null) {
				SiteLabel.Dispose ();
				SiteLabel = null;
			}

			if (StatusBarView != null) {
				StatusBarView.Dispose ();
				StatusBarView = null;
			}
		}
	}
}
