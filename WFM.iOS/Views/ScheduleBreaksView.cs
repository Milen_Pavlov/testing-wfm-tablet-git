﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleBreaksView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

		public bool Loading 
		{
			get{ return false; }
			set
			{
				BreaksTable.Hidden =  value;
				LoadingLabel.Hidden = !value;

				if (value)
					LoadingSpinner.StartAnimating ();
				else
					LoadingSpinner.StopAnimating ();
				
			}
		}

		public bool NoData 
		{
			get{ return false; }
			set
			{
				if(value)
					LoadingLabel.Hidden = false;

                if (NoDataLabel != null)
                    NoDataLabel.Hidden = !value;
			}
		}

		public ScheduleBreaksView()
			: base (IsPhone ? "ScheduleBreaksView_iPhone" : "ScheduleBreaksView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            UIColor textColor = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);

            Header.BackgroundColor = primary;
            HeaderTitles.TextColor = textColor;

            BreaksTable.RegisterCell<BreakCell> ();
            BreaksTable.RowHeight = 30;

            LoadingSpinner.HidesWhenStopped = true;

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleBreaksView, ScheduleBreaksViewModel> ();
            set.Bind (BreaksTable).To (b => b.Rows);
            set.Bind (LoadingLabel).For (v => v.Text).To (vm => vm.LoadingMessage);
            set.Bind (this).For (b => b.Loading).To (m => m.Loading);
            set.Bind (this).For (b => b.NoData).To (m => m.NoData);
            set.Bind (ColleagueLabel).For (v => v.Text).To (vm => vm.ColleagueText);
            set.Apply ();
        }
    }
}

