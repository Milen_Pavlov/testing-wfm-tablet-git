﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using WFM.Core;

namespace WFM.iOS
{
    [Register("ScheduleGridTable")]
    public class ScheduleGridTable : DragTableControl
    {
        public bool IsDay { get; set; }

        public ScheduleGridTable (IntPtr handle) 
            : base (handle)
        {
            Delegate = new DragTableControlDelegate(GetHeightForRow);
        }

        public ScheduleGridTable (RectangleF frame) 
            : base(frame)
        {
            Delegate = new DragTableControlDelegate(GetHeightForRow);
        }
         
        public float GetHeightForRow(NSIndexPath indexPath)
        {
            if (IsDay)
                return 44;

            var item = GetRow (indexPath) as EmployeeData;

            if (item != null)
            {
                if (item.CanEdit)
                {
                    return Math.Max ((item.MaxJobCount + 1) * 44, 44);
                }
                else
                {
                    return Math.Max (item.MaxJobCount * 44, 44);
                }
            }

            return base.OnGetHeightForRow (indexPath);
        }
    }
}
    