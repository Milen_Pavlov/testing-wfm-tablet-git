// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleGridView")]
	partial class ScheduleGridView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton EmployeeNameButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeNameLabel { get; set; }

		[Outlet]
		Consortium.Client.iOS.CollectionControl HeaderCollection { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView NameSortImage { get; set; }

		[Outlet]
		WFM.iOS.ScheduleGridTable ScheduleGrid { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalDemandHeaderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalDemandLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalHoursHeaderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalHoursLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (TotalDemandHeaderLabel != null) {
				TotalDemandHeaderLabel.Dispose ();
				TotalDemandHeaderLabel = null;
			}

			if (TotalHoursHeaderLabel != null) {
				TotalHoursHeaderLabel.Dispose ();
				TotalHoursHeaderLabel = null;
			}

			if (EmployeeNameButton != null) {
				EmployeeNameButton.Dispose ();
				EmployeeNameButton = null;
			}

			if (EmployeeNameLabel != null) {
				EmployeeNameLabel.Dispose ();
				EmployeeNameLabel = null;
			}

			if (HeaderCollection != null) {
				HeaderCollection.Dispose ();
				HeaderCollection = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (NameSortImage != null) {
				NameSortImage.Dispose ();
				NameSortImage = null;
			}

			if (ScheduleGrid != null) {
				ScheduleGrid.Dispose ();
				ScheduleGrid = null;
			}

			if (TotalDemandLabel != null) {
				TotalDemandLabel.Dispose ();
				TotalDemandLabel = null;
			}

			if (TotalHoursLabel != null) {
				TotalHoursLabel.Dispose ();
				TotalHoursLabel = null;
			}
		}
	}
}
