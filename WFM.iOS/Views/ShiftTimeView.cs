﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ShiftTimeView : ConsortiumView
    {
        private readonly ITimeFormatService m_timeFormat;
        
        public override Type AttachTarget
        {
            get { return typeof(ShiftView); }
        }

		//TODO : Refactor to use MVVM properly
        public ShiftTimeViewModel VM
        {
            get { return (ShiftTimeViewModel)DataContext; }
        }

        public ShiftTimeView()
            : base (IsPhone ? "ShiftTimeView_iPhone" : "ShiftTimeView_iPad", null)
        {
            Title = "Select Times";
            m_timeFormat = Mvx.Resolve<ITimeFormatService>();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            // View Model Binding
            var set = this.CreateBindingSet<ShiftTimeView, ShiftTimeViewModel>();
            set.Apply();

            StartPicker.MaximumDate = new DateTime(VM.Start.Year, VM.Start.Month, VM.Start.Day, 23, 59, 59, 999);
            StartPicker.MinimumDate = new DateTime(VM.Start.Year, VM.Start.Month, VM.Start.Day, 0, 0, 0, 0);

            StartPicker.Date = VM.Start;
            StartPicker.Locale = new MonoTouch.Foundation.NSLocale(m_timeFormat.TimePickerLocale);
            DurationPicker.CountDownDuration = (VM.End - VM.Start).TotalSeconds;
        }

        partial void OnApply (MonoTouch.Foundation.NSObject sender)
        {
            DateTime start = StartPicker.Date;
            start = start.ToLocalTime();
            VM.OnApply(start, TimeSpan.FromSeconds(DurationPicker.CountDownDuration));
        }
    }
}

