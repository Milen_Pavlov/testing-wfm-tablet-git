// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("SettingsView")]
	partial class SettingsView
	{
		[Outlet]
		Consortium.Client.iOS.TableControl EnvironmentTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EnvironmentTable != null) {
				EnvironmentTable.Dispose ();
				EnvironmentTable = null;
			}
		}
	}
}
