// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
    [Register ("WorkgroupView")]
	partial class WorkgroupView
	{
		[Outlet]
		WFM.iOS.AlternateBackgroundTable GroupItems { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (GroupItems != null) {
				GroupItems.Dispose ();
				GroupItems = null;
			}
		}
	}
}
