// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleBreaksView")]
	partial class ScheduleBreaksView
	{
		[Outlet]
		Consortium.Client.iOS.TableControl BreaksTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ColleagueLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView Header { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HeaderTitles { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingSpinner { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoDataLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BreaksTable != null) {
				BreaksTable.Dispose ();
				BreaksTable = null;
			}

			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}

			if (HeaderTitles != null) {
				HeaderTitles.Dispose ();
				HeaderTitles = null;
			}

			if (LoadingLabel != null) {
				LoadingLabel.Dispose ();
				LoadingLabel = null;
			}

			if (LoadingSpinner != null) {
				LoadingSpinner.Dispose ();
				LoadingSpinner = null;
			}

			if (NoDataLabel != null) {
				NoDataLabel.Dispose ();
				NoDataLabel = null;
			}

			if (ColleagueLabel != null) {
				ColleagueLabel.Dispose ();
				ColleagueLabel = null;
			}
		}
	}
}
