﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public partial class ChangePasswordView : ConsortiumPopoverView
    {
        public ChangePasswordView()
            : base (IsPhone ? "ChangePasswordView_iPhone" : "ChangePasswordView_iPad", null)
        {
            CenterInView = true;
            Title = "Change Password";
        }


        partial void CurrentPasswordDidEndOnExit (NSObject sender)
        {
            NewPasswordText.BecomeFirstResponder();
        }

        partial void NewPasswordDidEndOnExit (NSObject sender)
        {
            ConfirmPasswordText.BecomeFirstResponder();
        }

        partial void ConfirmPasswordDidEndOnExit (NSObject sender)
        {
            ((ChangePasswordViewModel)ViewModel).OnSubmitButton();
        }

        public override void ViewDidAppear (bool animated)
        {
            CurrentPasswordText.BecomeFirstResponder ();
        }                     

        protected override void ViewClosed ()
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            // View Model Binding
            var set = this.CreateBindingSet<ChangePasswordView, ChangePasswordViewModel>();
            set.Bind (CurrentPasswordText).To (m => m.OldPassword);
            set.Bind (NewPasswordText).To (m => m.NewPassword);
            set.Bind (ConfirmPasswordText).To (m => m.ConfirmPassword);
            set.Bind (ErrorMessageLabel).To (m => m.ErrorMessage);
            set.Bind (SubmitButton).To("OnSubmitButton");
            set.Bind (CancelButton).To("OnCancelButton");
            set.Apply();
        }
    }
}

