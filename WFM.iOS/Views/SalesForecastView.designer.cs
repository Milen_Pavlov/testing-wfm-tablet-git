// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("SalesForecastView")]
	partial class SalesForecastView
	{
		[Outlet]
		Consortium.Client.iOS.TableControl DataGrid { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DateNext { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DatePrev { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day1Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day2Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day3Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day4Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day5Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day6Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Day7Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton GroupButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DataGrid != null) {
				DataGrid.Dispose ();
				DataGrid = null;
			}

			if (DateNext != null) {
				DateNext.Dispose ();
				DateNext = null;
			}

			if (DatePrev != null) {
				DatePrev.Dispose ();
				DatePrev = null;
			}

			if (DateText != null) {
				DateText.Dispose ();
				DateText = null;
			}

			if (Day1Button != null) {
				Day1Button.Dispose ();
				Day1Button = null;
			}

			if (Day2Button != null) {
				Day2Button.Dispose ();
				Day2Button = null;
			}

			if (Day3Button != null) {
				Day3Button.Dispose ();
				Day3Button = null;
			}

			if (Day4Button != null) {
				Day4Button.Dispose ();
				Day4Button = null;
			}

			if (Day5Button != null) {
				Day5Button.Dispose ();
				Day5Button = null;
			}

			if (Day6Button != null) {
				Day6Button.Dispose ();
				Day6Button = null;
			}

			if (Day7Button != null) {
				Day7Button.Dispose ();
				Day7Button = null;
			}

			if (GroupButton != null) {
				GroupButton.Dispose ();
				GroupButton = null;
			}

			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}
		}
	}
}
