﻿using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class UnfilledShiftsView : ConsortiumPopoverView
    {
        private UIBarButtonItem m_cancel;

        public UnfilledShiftsView ()
            : base (IsPhone ? "UnfilledShiftsView_iPhone" : "UnfilledShiftsView_iPad", null)
        {
            CenterInView = true;
        }

        public int SelectedRow {
            get 
            { 
                return UnfilledShiftsTable.IndexPathForSelectedRow.Row;
            }
            set 
            { 
                if(UnfilledShiftsTable.Items.Count > 0)
                {
                    UnfilledShiftsTable.SelectRow (NSIndexPath.FromRowSection (value, 0), false, UITableViewScrollPosition.None); 
                }
            }
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            UnfilledShiftsTable.RowHeight = 50;
            UnfilledShiftsTable.AllowsSelection = true;
            AvailableEmployeesTable.RowHeight = 50;
            AddTitle ();

            UnfilledShiftsTable.RegisterCell<UnfilledShiftDetailCell> ();
            AvailableEmployeesTable.RegisterCell<AvailableEmployeeCell> ();

            var set = this.CreateBindingSet<UnfilledShiftsView, UnfilledShiftsViewModel>();

            set.Bind (UnfilledShiftsTable).To (vm => vm.UnfilledShifts);
            set.Bind (UnfilledShiftsTable).For (v => v.Selected).To ("OnUnfilledShiftSelected");
            set.Bind (UnfilledShiftsTable).For (v => v.CustomCommand1).To ("OnDelete");
            set.Bind(this).For(t => t.SelectedRow).To(vm => vm.UnfilledShiftIndex);
            set.Bind (AvailableEmployeesTable).To (vm => vm.AvailableEmployees);
            set.Bind (AvailableEmployeesTable).For (v => v.CustomCommand1).To ("OnFillUnfilledShift");

            set.Bind (LoadingLabel).For (v => v.Hidden).To (vm => vm.Loading).WithConversion (new InvertedBooleanConverter ());
            set.Bind (LoadingActivityIndicator).For (v => v.Hidden).To (vm => vm.Loading).WithConversion (new InvertedBooleanConverter ());

            set.Bind (Message2Label).To (vm => vm.Message2);
            set.Bind (Message2Label).For(l => l.Hidden).To (vm => vm.Message2).WithConversion (new InvertedBooleanConverter ()); ;
            set.Bind (Message1Label).To (vm => vm.Message1);
            set.Bind(AvailabeEmployeesLabel).To(vm => vm.AvailableEmployeeLabel);

            set.Apply();
        }

        private void AddTitle ()
        {
            var titleView = new UIView (new RectangleF (0, 0, 300, 44));

            var titleLabel = new UILabel (new RectangleF (0, 4, 300, 40));
            titleLabel.Font = UIFont.FromName ("AvenirNext-DemiBold", 16);
            titleLabel.TextAlignment = UITextAlignment.Center;
            titleLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            titleLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            titleLabel.TextColor = UIColor.White;
            titleLabel.BackgroundColor = UIColor.Clear;
            titleLabel.Text = "Unfilled Shifts";
            titleView.AddSubview (titleLabel);

            m_cancel = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, (sender, e) => {
                DismissViewController (true, null);
            });

            NavigationItem.TitleView = titleView;
            NavigationItem.LeftBarButtonItem = m_cancel;
        }
   }
}
