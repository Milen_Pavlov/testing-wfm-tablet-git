﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using MonoTouch.Foundation;
using System.Collections.Generic;

namespace WFM.iOS
{
    public partial class SettingsView : ConsortiumPopoverView
    {
        public int SelectedRow
        {
            get { return EnvironmentTable.IndexPathForSelectedRow.Row; }
            set { EnvironmentTable.SelectRow (NSIndexPath.FromRowSection (value, 0), false, UITableViewScrollPosition.None); }
        }

        public int ScrollToRow
        {
            get { return 0; }
            set 
            { 
                if(EnvironmentTable != null && EnvironmentTable.Items != null && value < EnvironmentTable.Items.Count)
                    EnvironmentTable.ScrollToRow (NSIndexPath.FromRowSection (value, 0), UITableViewScrollPosition.Bottom, true);
            }
        }

        public bool CanEdit
        {
            get { return false; }
            set
            {
                if (value)
                    AddRightBarButton(EditButton);
                else
                    RemoveRightBarButton(EditButton);
            }
        }

        public BarButton EditButton { get; set; }

        public SettingsView()
            : base (IsPhone ? "SettingsView_iPhone" : "SettingsView_iPad", null)
        {
            ArrowDirection = UIPopoverArrowDirection.Any;
            Title = "Select Environment";
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            Navigation.NavigationBar.TintColor = UIColor.White;
            Navigation.NavigationBar.BarTintColor = Constants.TescoBlue;
            Navigation.NavigationBar.BarStyle = UIBarStyle.Black;
            Navigation.NavigationBar.Translucent = false;

            EnvironmentTable.RegisterCell<EnvironmentCell> ();

            EditButton = new BarButton("Edit");

            // View Model Binding
            var set = this.CreateBindingSet<SettingsView, SettingsViewModel>();
            set.Bind (EnvironmentTable).To (vm => vm.Environments);
            set.Bind (this).For (v => v.SelectedRow).To (vm => vm.EnvIndex);
            set.Bind (this).For (v => v.ScrollToRow).To (vm => vm.ScrollToRow);
            set.Bind (this).For (v => v.CanEdit).To (vm => vm.CanEdit);
            set.Bind (EnvironmentTable).For (v => v.Selected).To ("OnSetEnvironment");
            set.Bind (EnvironmentTable).For (v => v.CustomCommand1).To ("OnDeleteEnvironment");
            set.Bind (EditButton).To ("OnToggleEdit");
            set.Bind (EditButton).For (v => v.Title).To (vm => vm.EditingTitle);
            set.Apply();
        }

        protected override void ViewClosed ()
        {
            base.ViewClosed ();

            SettingsViewModel svm = DataContext as SettingsViewModel;
            if(svm!=null && svm.Editing)
                svm.Save();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            SettingsViewModel svm = DataContext as SettingsViewModel;
            if(svm!=null && svm.CanEdit)
                AddRightBarButton(EditButton);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            RemoveRightBarButton(EditButton);
        }

        protected void AddRightBarButton(UIBarButtonItem button)
        {
            List<UIBarButtonItem> buttons = new List<UIBarButtonItem>();
            if (NavigationItem.RightBarButtonItems != null)
            {
                buttons.AddRange(NavigationItem.RightBarButtonItems);
            }

            if (!buttons.Contains(button))
                buttons.Add(button);

            NavigationItem.RightBarButtonItems = buttons.ToArray();
        }

        protected void RemoveRightBarButton(UIBarButtonItem button)
        {
            if (button == null)
                return;

            List<UIBarButtonItem> buttons = new List<UIBarButtonItem>();
            if (NavigationItem.RightBarButtonItems != null)
            {
                buttons.AddRange(NavigationItem.RightBarButtonItems);
            }

            if(buttons.Contains(button))
                buttons.Remove(button);

            NavigationItem.RightBarButtonItems = buttons.ToArray();
        }
    }
}

