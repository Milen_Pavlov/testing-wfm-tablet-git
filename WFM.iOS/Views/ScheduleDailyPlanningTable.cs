﻿using System;
using Consortium.Client.iOS;
using System.Drawing;
using MonoTouch.Foundation;
using WFM.Core;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    [Register("ScheduleDailyPlanningTable")]
    public class ScheduleDailyPlanningTable : TableControl
    {
        public int? DayIndex { get; set; }

        public ScheduleDailyPlanningTable (IntPtr handle) : base (handle) {}
        public ScheduleDailyPlanningTable (RectangleF frame) : base(frame) {}

        protected override float OnGetHeightForRow(NSIndexPath indexPath)
        {
            var item = GetRow (indexPath) as PlanningData;

            if (!DayIndex.HasValue)
                return (int)Math.Max(item.WeekMax * RowHeight, 0);

            return (int)Math.Max(item.DayMax[DayIndex.Value] * RowHeight, 0);
        }
    }
}
