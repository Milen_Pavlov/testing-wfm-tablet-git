﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class ShiftView : ConsortiumPopoverView
    {
        UILabel m_jobName;
        UILabel m_jobDescription;
        UIBarButtonItem m_cancel;
        UIBarButtonItem m_save;
        RectangleF m_deletionFrame;
        RectangleF m_listFrame;
        RectangleF m_toolbarFrame;

        ShiftControl m_shiftSummary;

        private ShiftViewModel VM 
        {
            get 
            {
                return (ShiftViewModel)ViewModel;
            }
        }

        private bool m_showDetails;
        public bool ShowDetails
        {
            get { return m_showDetails; }
            set
            {
                m_showDetails = value;
                UpdateDetailsBar ();
            }
        }

        private bool m_allowDelete;
        public bool AllowDelete
        {
            get { return m_allowDelete; }
            set
            {
                m_allowDelete = value;

                var flex = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);

                if (value)
                    ShiftToolbar.Items = new UIBarButtonItem[] { flex, UnfillButtonHolder, flex, CallOffButtonHolder, flex, DeleteButtonHolder, flex };
                else
                    ShiftToolbar.Items = new UIBarButtonItem[] { flex, UnfillButtonHolder, flex, CallOffButtonHolder, flex };

                UpdateDetailsBar ();
            }
        }

        public ShiftData ShiftSummary 
        {
            get { return null; }
            set 
            {
                if (value == null)
                    return;

                if (m_shiftSummary != null)
                    m_shiftSummary.RemoveFromSuperview ();

                RectangleF rect = new RectangleF(0,0,ShiftSummaryView.Frame.Width,ShiftSummaryView.Frame.Height);
                m_shiftSummary = new ShiftControl (rect, value, false, m_showDetails);
                ShiftSummaryView.AddSubview (m_shiftSummary);
            }
        }

        private bool m_editable;
        public bool Editable
        {
            get { return m_editable; }
            set 
            {
                m_editable = value;
                SetupUI ();
            }
        }

        public ShiftView()
            : base (IsPhone ? "ShiftView_iPhone" : "ShiftView_iPad", null)
        {
            CenterInView = true;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            UIColor secondary = ColorUtility.FromHexString (settings.AppTheme.SecondaryColor);

            m_deletionFrame = DeletionView.Frame;
            m_listFrame = ShiftDetailTable.Frame;
            m_toolbarFrame = EditToolbar.Frame;

            EditToolbar.SetBackgroundImage(ImageUtility.ImageFromColor(primary), UIToolbarPosition.Bottom, UIBarMetrics.Default);


            AddJobButton.TintColor = secondary;
            AddBreakButton.TintColor = secondary;
            AddMealButton.TintColor = secondary;
            AddRoleButton.TintColor = secondary;

            UnfillButton.Layer.CornerRadius = 8;
            UnfillButton.BackgroundColor = primary;
            UnfillButton.SetTitleColor(secondary, UIControlState.Normal);

            CallOffButton.Layer.CornerRadius = 8;
            CallOffButton.BackgroundColor = primary;
            CallOffButton.SetTitleColor(secondary, UIControlState.Normal);

            DeleteButton.Layer.CornerRadius = 8;
            DeleteButton.BackgroundColor = primary;
            DeleteButton.SetTitleColor(secondary, UIControlState.Normal);

            // Add the title
            AddTitle (VM.ConfigurationService.CanEditSchedule);

            ShiftDetailTable.RowHeight = 40;
            ShiftDetailTable.SectionHeaderHeight = 30;
            ShiftDetailTable.HideEmptySections = true;
            ShiftDetailTable.RegisterSection<ShiftDetailHeaderCell> ();
            ShiftDetailTable.RegisterCell<ShiftDetailItemCell> ();

            // View Model Binding
            var set = this.CreateBindingSet<ShiftView, ShiftViewModel>();
            set.Bind (m_jobName).To (m => m.FullName);
            set.Bind (m_jobDescription).To (m => m.ShiftDescription);
            set.Bind (this).For (b => b.ShowDetails).To (m => m.ConfigurationService.ShowScheduleDetails);
            set.Bind (this).For (b => b.ShiftSummary).To (m => m.Shift);
            set.Bind (this).For (b => b.Editable).To (m => m.Editable);
            set.Bind (ShiftDetailTable).To (m => m.ShiftItems);

            set.Bind (AddJobButton).To ("OnAddJob");
            set.Bind (AddBreakButton).To ("OnAddBreak");
            set.Bind (AddMealButton).To ("OnAddMeal");
            set.Bind (UnfillButton).To ("OnUnfillShift");
            set.Bind (DeleteButton).To ("OnDeleteShift");
            set.Bind (this).For (v => v.AllowDelete).To (m => m.AllowDelete);
            set.Bind (CallOffButton).To ("OnCallOffShift");
            set.Bind (m_cancel).To ("OnCancel");
            set.Bind (m_save).To ("OnSave");
            //set.Bind(AddRoleButton).To("OnAddRole");
            set.Bind (CallOffButton).For (b => b.Hidden).To (m => m.ShowCallOff).WithConversion("InvertedBoolean");
            set.Bind (DeletionView).For (b => b.Hidden).To (m => m.ShowShiftToolbar).WithConversion ("InvertedBoolean");
            set.Apply();
        }

        private void AddTitle(bool edit)
        {
            UIView titleView = new UIView(new RectangleF(0,0,300,44));

            m_jobName = new UILabel (new RectangleF (0, 4, 300, 18));
            m_jobName.Font = UIFont.FromName ("AvenirNext-DemiBold", 16);
            m_jobName.TextAlignment = UITextAlignment.Center;
            m_jobName.LineBreakMode = UILineBreakMode.TailTruncation;
            m_jobName.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            m_jobName.TextColor = UIColor.White;
            m_jobName.BackgroundColor = UIColor.Clear;
            titleView.AddSubview (m_jobName);

            m_jobDescription = new UILabel (new RectangleF (0, 22, 300, 18));
            m_jobDescription.Font = UIFont.FromName ("AvenirNext-Regular", 12);
            m_jobDescription.TextColor = UIColor.White;
            m_jobDescription.TextAlignment = UITextAlignment.Center;
            m_jobDescription.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            m_jobDescription.BackgroundColor = UIColor.Clear;
            titleView.AddSubview (m_jobDescription);

            m_cancel = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, null);
            m_save = new UIBarButtonItem ("Save", UIBarButtonItemStyle.Plain, null);

            NavigationItem.TitleView = titleView;
            NavigationItem.LeftBarButtonItem = m_cancel;
        }

        private void SetupUI()
        {
            NavigationItem.RightBarButtonItem = (Editable) ? m_save : null;
            EditToolbar.Hidden = !Editable;

            //Setup Positioning
            DeletionView.Frame = new RectangleF (DeletionView.Frame.X,
                (Editable) ? m_deletionFrame.Y : m_deletionFrame.Y + m_toolbarFrame.Height,
                DeletionView.Frame.Width,
                DeletionView.Frame.Height);

            float height = m_listFrame.Height;

            if (!Editable)
                height += m_toolbarFrame.Height;
            
            if (!VM.ShowShiftToolbar)
                height += m_deletionFrame.Height;
            
            ShiftDetailTable.Frame = new RectangleF (
                0, 
                ShiftDetailTable.Frame.Y, 
                ShiftDetailTable.Frame.Width, 
                height);
        }

        private void UpdateDetailsBar()
        {
            var flex = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);

            if (m_showDetails && m_allowDelete)
            {
                EditToolbar.Items = new UIBarButtonItem[] {  flex, AddJobButton, //flex, AddRoleButton, 
                    flex, AddBreakButton, flex, AddMealButton, flex }; 
            }
            else if( !m_showDetails && m_allowDelete)
            {
                EditToolbar.Items = new UIBarButtonItem[] { flex, AddJobButton, flex //, AddRoleButton, flex
                }; 
            }
            else if( m_showDetails && !m_allowDelete)
            {
                EditToolbar.Items = new UIBarButtonItem[] { flex, AddJobButton, //flex, AddRoleButton, 
                    flex, AddBreakButton, flex, AddMealButton, flex }; 
            }
            else if( !m_showDetails && !m_allowDelete)
            {
                EditToolbar.Items = new UIBarButtonItem[] { flex, AddJobButton, //flex, AddRoleButton, flex 
                }; 
            }
        }
    }
}