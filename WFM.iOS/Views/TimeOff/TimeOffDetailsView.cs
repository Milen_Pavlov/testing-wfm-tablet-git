﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class TimeOffDetailsView : TimeOffBaseDetailView
    {
        UIBarButtonItem m_cancelTimeOffButton;

        UIBarButtonItem[] m_editButtons;

        public TimeOffDetailsViewModel VM
        {
            get { return ViewModel as TimeOffDetailsViewModel; }
        }

        public UISegmentedControl TabSelect 
        { 
            get; 
            set; 
        }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumView); } 
        }

        public string[] TabTitles
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    if (TabSelect != null)
                    {
                        TabSelect.Dispose ();
                        TabSelect = null;
                    }

                    TabSelect = new UISegmentedControl (value);
                    TabSelect.SelectedSegment = 0;
                    TabSelect.AddTarget (OnTabSelect, UIControlEvent.ValueChanged);
                    NavigationItem.TitleView = TabSelect;
                }
            }
        }


        public bool CanApproveTimeOff
        {
            get
            {
                return false;
            }

            set
            {
                var commentsFrame = this.CommentsTable.Frame;

                if (value)
                {
                    ApprovalControls.Hidden = false;
                    commentsFrame = new RectangleF(commentsFrame.X,commentsFrame.Y,commentsFrame.Width,
                        this.View.Frame.Bottom - commentsFrame.Y);

                    m_cancelTimeOffButton.Enabled = true;

                }
                else
                {
                    ApprovalControls.Hidden = true;
                    commentsFrame = new RectangleF(commentsFrame.X,commentsFrame.Y,commentsFrame.Width,
                        this.View.Frame.Bottom - ApprovalControls.Frame.Top);

                    m_cancelTimeOffButton.Enabled = false;
                }

                CommentsTable.Frame = commentsFrame;
            }
        }

        private bool m_isEmployeeViewOnly;
        public bool IsEmployeeViewOnly
        {
            get { return m_isEmployeeViewOnly; }
            set
            {
                m_isEmployeeViewOnly = value;

                TimeOffRequestInfo.Hidden = m_isEmployeeViewOnly;
                EmployeeInfo.Hidden = !m_isEmployeeViewOnly;
            }
        }

        public TimeOffDetailsView()
            : base (IsPhone ? "TimeOffDetailsView_iPhone" : "TimeOffDetailsView_iPad", null)
        {
            
        }

        protected override UIView GetTabContentView()
        {
            return TabContent;
        }

        private void OnTabSelect (object sender, EventArgs e)
        {
            ((TimeOffDetailsViewModel)ViewModel).OnTabSelect(TabSelect.SelectedSegment);
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var settings = Mvx.Resolve<SettingsService> ();

            NameViewContainer.BackgroundColor = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);

            SetupBarButtons ();

            CommentsTable.RegisterCell<TimeOffCell> ();

            string dateFormat = Mvx.Resolve<IStringService>().Get("{0:dd/MM/yy}");
                
            var set = this.CreateBindingSet<TimeOffDetailsView, TimeOffDetailsViewModel> ();
            set.Bind (this).For (v => v.TabTitles).To (vm => vm.TabTitles);
            set.Bind (this).For (v => v.IsEmployeeViewOnly).To (vm => vm.TimeOffModel.EmployeeViewOnly);
            set.Bind (EmployeeNameText).To (vm => vm.TimeOffModel.EmployeeName);
            set.Bind (StatusText).To (vm => vm.TimeOffModel.Status).WithConversion (new TimeOffStatusStringConverter ());
            set.Bind (FromDateText).To (vm => vm.TimeOffModel.StartDate).WithConversion (new ToStringValueConverter<DateTime> (), dateFormat);
            set.Bind (ToDateText).To (vm => vm.TimeOffModel.EndDate).WithConversion (new ToStringValueConverter<DateTime> (), dateFormat);
            set.Bind (FromTimeText).To (vm => vm.TimeOffModel.StartDate).WithConversion (new TimeToStringConverter());
            set.Bind (ToTimeText).To (vm => vm.TimeOffModel.EndDate).WithConversion (new TimeToStringConverter());
            set.Bind (FromTimeText).For (c => c.Hidden).To (vm => vm.TimeOffModel.IsTrackedAsWholeDays);
            set.Bind (ToTimeText).For (c => c.Hidden).To (vm => vm.TimeOffModel.IsTrackedAsWholeDays);
            set.Bind (TypeText).To (vm => vm.TimeOffModel.Type);
            set.Bind (CommentsTable).To (vm => vm.TimeOffModel.Comments);

            set.Bind (EmployeeHomePhone).To (vm => vm.TimeOffModel.EmployeeInfo.HomePhone).WithConversion(new EmptyStringConverter());
            set.Bind (EmployeeMobilePhone).To (vm => vm.TimeOffModel.EmployeeInfo.CellPhone).WithConversion(new EmptyStringConverter());
            set.Bind (EmployeeSiteName).To (vm => vm.TimeOffModel.EmployeeInfo.HomeSiteName).WithConversion(new EmptyStringConverter());
            set.Bind (EmployeeJobName).To (vm => vm.TimeOffModel.EmployeeInfo.PrimaryJobName).WithConversion(new EmptyStringConverter());
            set.Bind (EmployeeActive).To (vm => vm.TimeOffModel.EmployeeInfo.IsActive).WithConversion(new StringBoolConverter());
            set.Bind (EmployeeBorrowed).To (vm => vm.TimeOffModel.EmployeeInfo.IsBorrowed).WithConversion(new StringBoolConverter());
            set.Bind (EmployeeMinor).To (vm => vm.TimeOffModel.EmployeeInfo.IsMinor).WithConversion(new StringBoolConverter());

            set.Bind (this).For (v => v.CanApproveTimeOff).To (vm => vm.CanApproveTimeOff);

            set.Bind (ApproveButton).To ("ConfirmApproveTimeOffRequest");
            set.Bind (RejectButton).To ("ConfirmRejectTimeOffRequest");

            set.Apply ();
        }

        public void SetupBarButtons()
        {
            if (VM.CanApproveTimeOff)
            {
                List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();

                m_cancelTimeOffButton = new UIBarButtonItem (UIImage.FromBundle ("ActionbarTrash"), UIBarButtonItemStyle.Plain, (v1, v2) =>
                    { 
                        VM.ConfirmDeleteTimeOffRequest ();
                    });

                barButtons.Add (m_cancelTimeOffButton);

                m_editButtons = barButtons.ToArray ();

                NavigationItem.RightBarButtonItems = m_editButtons;
            }
        }
    }
}