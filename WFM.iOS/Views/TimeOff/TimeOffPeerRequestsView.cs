﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class TimeOffPeerRequestsView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

        public TimeOffPeerRequestsView()
            : base (IsPhone ? "TimeOffPeerRequestsView_iPhone" : "TimeOffPeerRequestsView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            PeerTable.RegisterCell<PeerRequestCell> ();
            PeerTable.RowHeight = 30;
            PeerTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            PeerTable.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<TimeOffPeerRequestsView, TimeOffPeerRequestsViewModel>();
            set.Bind (PeerTable).For (v => v.Items).To (vm => vm.Peers);
            set.Bind (NoDataLabel).For (v => v.Hidden).To (vm => vm.NoDataLabel).WithConversion (new StringVisibleConverter ());
            set.Bind (NoDataLabel).For (v => v.Text).To (vm => vm.NoDataLabel);
            set.Bind (EmployeeLabel).For (v => v.Text).To (vm => vm.EmployeeText);
            set.Apply();
        }
    }
}

