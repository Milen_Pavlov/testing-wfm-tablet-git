// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffBaseDetailView")]
	partial class TimeOffBaseDetailView
	{
		[Outlet]
		protected MonoTouch.UIKit.UIView EmployeeNameContainer { get; private set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeNameText { get; set; }

		[Outlet]
		protected MonoTouch.UIKit.UIView TabContent { get; private set; }

		[Outlet]
		Consortium.Client.iOS.WebImageControl Watermark { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EmployeeNameContainer != null) {
				EmployeeNameContainer.Dispose ();
				EmployeeNameContainer = null;
			}

			if (EmployeeNameText != null) {
				EmployeeNameText.Dispose ();
				EmployeeNameText = null;
			}

			if (TabContent != null) {
				TabContent.Dispose ();
				TabContent = null;
			}

			if (Watermark != null) {
				Watermark.Dispose ();
				Watermark = null;
			}
		}
	}
}
