// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffDetailsView")]
	partial class TimeOffDetailsView
	{
		[Outlet]
		MonoTouch.UIKit.UIView ApprovalControls { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ApproveButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView CommentHistoryCollection { get; set; }

		[Outlet]
		WFM.iOS.CommentTable CommentsTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeActive { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeBorrowed { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeHomePhone { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView EmployeeInfo { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeJobName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeMinor { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeMobilePhone { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeNameText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeSiteName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FromDateText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FromTimeText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView NameViewContainer { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton RejectButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel StatusText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TimeOffRequestInfo { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ToDateText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ToTimeText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TypeText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NameViewContainer != null) {
				NameViewContainer.Dispose ();
				NameViewContainer = null;
			}

			if (ApprovalControls != null) {
				ApprovalControls.Dispose ();
				ApprovalControls = null;
			}

			if (ApproveButton != null) {
				ApproveButton.Dispose ();
				ApproveButton = null;
			}

			if (CommentHistoryCollection != null) {
				CommentHistoryCollection.Dispose ();
				CommentHistoryCollection = null;
			}

			if (CommentsTable != null) {
				CommentsTable.Dispose ();
				CommentsTable = null;
			}

			if (EmployeeActive != null) {
				EmployeeActive.Dispose ();
				EmployeeActive = null;
			}

			if (EmployeeBorrowed != null) {
				EmployeeBorrowed.Dispose ();
				EmployeeBorrowed = null;
			}

			if (EmployeeHomePhone != null) {
				EmployeeHomePhone.Dispose ();
				EmployeeHomePhone = null;
			}

			if (EmployeeInfo != null) {
				EmployeeInfo.Dispose ();
				EmployeeInfo = null;
			}

			if (EmployeeJobName != null) {
				EmployeeJobName.Dispose ();
				EmployeeJobName = null;
			}

			if (EmployeeMinor != null) {
				EmployeeMinor.Dispose ();
				EmployeeMinor = null;
			}

			if (EmployeeMobilePhone != null) {
				EmployeeMobilePhone.Dispose ();
				EmployeeMobilePhone = null;
			}

			if (EmployeeNameText != null) {
				EmployeeNameText.Dispose ();
				EmployeeNameText = null;
			}

			if (EmployeeSiteName != null) {
				EmployeeSiteName.Dispose ();
				EmployeeSiteName = null;
			}

			if (FromDateText != null) {
				FromDateText.Dispose ();
				FromDateText = null;
			}

			if (FromTimeText != null) {
				FromTimeText.Dispose ();
				FromTimeText = null;
			}

			if (RejectButton != null) {
				RejectButton.Dispose ();
				RejectButton = null;
			}

			if (StatusText != null) {
				StatusText.Dispose ();
				StatusText = null;
			}

			if (TimeOffRequestInfo != null) {
				TimeOffRequestInfo.Dispose ();
				TimeOffRequestInfo = null;
			}

			if (ToDateText != null) {
				ToDateText.Dispose ();
				ToDateText = null;
			}

			if (ToTimeText != null) {
				ToTimeText.Dispose ();
				ToTimeText = null;
			}

			if (TypeText != null) {
				TypeText.Dispose ();
				TypeText = null;
			}
		}
	}
}
