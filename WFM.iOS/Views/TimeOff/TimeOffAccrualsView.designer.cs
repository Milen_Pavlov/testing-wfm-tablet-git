// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffAccrualsView")]
	partial class TimeOffAccrualsView
	{
		[Outlet]
		WFM.iOS.AlternateBackgroundTable AccrualTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoDataLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AccrualTable != null) {
				AccrualTable.Dispose ();
				AccrualTable = null;
			}

			if (NoDataLabel != null) {
				NoDataLabel.Dispose ();
				NoDataLabel = null;
			}
		}
	}
}
