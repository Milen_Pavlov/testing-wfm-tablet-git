﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class TimeOffBaseDetailView : WFMTabView
    {
        public UISegmentedControl TabSelect 
        { 
            get; 
            set; 
        }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumView); } 
        }

        public string[] TabTitles
        {
            get { return null; }
            set
            {
                if (value != null)
                {
                    if (TabSelect != null)
                    {
                        TabSelect.Dispose ();
                        TabSelect = null;
                    }

                    TabSelect = new UISegmentedControl (value);
                    TabSelect.SelectedSegment = 0;
                    TabSelect.AddTarget (OnTabSelect, UIControlEvent.ValueChanged);
                    NavigationItem.TitleView = TabSelect;
                }
            }
        }

        //Keyboard notification and shifting work, eventually should go in ConsortiumView
        private NSObject m_keyboardShowObserver;
        private NSObject m_keyboardHideObserver;
        private RectangleF m_originalRootFrame;
        private RectangleF m_desiredRootFrame;
        private UITapGestureRecognizer m_dismissKeyboardGesture;

        public TimeOffBaseDetailView(string nibName, NSBundle bundle)
            : base (nibName,bundle)
        {
            
        }

        protected override UIView GetTabContentView()
        {
            return TabContent;
        }

        private void OnTabSelect (object sender, EventArgs e)
        {
            ((TimeOffBaseDetailViewModel)ViewModel).OnTabSelect(TabSelect.SelectedSegment);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();
            
            var settings = Mvx.Resolve<SettingsService> ();
            var primaryCol = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            
            EmployeeNameContainer.BackgroundColor = primaryCol;

            Watermark.ContentMode = UIViewContentMode.ScaleAspectFit;
            Watermark.Alpha = 0.05f;

            var set = this.CreateBindingSet<TimeOffBaseDetailView, TimeOffBaseDetailViewModel> ();
            set.Bind (this).For (v => v.TabTitles).To (vm => vm.TabTitles);
            set.Bind (EmployeeNameText).To (vm => vm.EmployeeName);

            set.Bind (Watermark).For (v => v.Hidden).To (vm => vm.Watermark).WithConversion (new StringVisibleConverter ());
            set.Bind (Watermark).For (v => v.Uri).To (vm => vm.Watermark);

            set.Apply ();

            // Add gesture recognizer to hide keyboard
            m_dismissKeyboardGesture = new UITapGestureRecognizer { CancelsTouchesInView = false };
            m_dismissKeyboardGesture.AddTarget(() => View.EndEditing(true));
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);

            if (m_originalRootFrame == RectangleF.Empty)
                m_originalRootFrame = View.Frame;

            m_keyboardShowObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, OnKeyboardNotification);
            m_keyboardHideObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, OnKeyboardNotification);
            View.AddGestureRecognizer (m_dismissKeyboardGesture);
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);

            NSNotificationCenter.DefaultCenter.RemoveObserver(m_keyboardShowObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver(m_keyboardHideObserver);
            View.RemoveGestureRecognizer(m_dismissKeyboardGesture);
        }

        private void OnKeyboardNotification(NSNotification notification)
        {
            var showing = notification.Name == UIKeyboard.WillShowNotification;
            var keyboardFrame = showing ? UIKeyboard.FrameEndFromNotification(notification) : UIKeyboard.FrameBeginFromNotification(notification);

            //Get First responder
            UIView firstResponder = GetFirstResponder(View);
            RectangleF frame = RectangleF.Empty;
            if (firstResponder != null)
            {
                frame = View.ConvertRectFromView (new RectangleF (0, 0, firstResponder.Frame.Width, firstResponder.Frame.Height), firstResponder);
            }

            //Check if the base of the Sign in button will be under the keyboard, and by how much
            m_desiredRootFrame = m_originalRootFrame;
            if (showing)
            {
                float bottomOfArea = frame.Bottom + 10;
                float keyboardHeight = keyboardFrame.Height;
                float topOfKeyboard = View.Frame.Height - keyboardHeight;

                if (bottomOfArea > topOfKeyboard)
                    m_desiredRootFrame.Y -= (bottomOfArea - topOfKeyboard);
            }

            NSNumber duration = (NSNumber)notification.UserInfo.ValueForKey(UIKeyboard.AnimationDurationUserInfoKey);
            NSNumber curve = (NSNumber)notification.UserInfo.ObjectForKey(UIKeyboard.AnimationCurveUserInfoKey);

            double durationVal = (duration.DoubleValue == 0) ? 0.25 : duration.DoubleValue;
            UIViewAnimationOptions curveVal = (curve.Int32Value == 0) ? UIViewAnimationOptions.CurveEaseInOut : (UIViewAnimationOptions) curve.Int32Value;
            UIView.Animate(durationVal, 0, curveVal | UIViewAnimationOptions.BeginFromCurrentState, animation: () =>
                {
                    View.Frame = m_desiredRootFrame;
                }, completion: () =>
                {
                });

        }

        //Used by keyboard notification event to get the correct textview to shift in view
        private UIView GetFirstResponder(UIView root)
        {
            if (root.IsFirstResponder)
                return root;

            foreach (UIView subView in root.Subviews)
            {
                UIView view = GetFirstResponder (subView);
                if (view != null)
                    return view;
            }

            return null;
        }
    }
}