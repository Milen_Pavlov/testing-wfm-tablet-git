﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;

namespace WFM.iOS
{
    public partial class TimeOffPayAllocationsView : ConsortiumView
    {
        private RectangleF m_originalFooterFrame = RectangleF.Empty;
        private RectangleF m_originalTableFrame = RectangleF.Empty;

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

        public bool SectionsUpdated
        {
            get { return false; }
            set
            {
                //Update frames
                UpdateFooterFraming ();
            }
        }

        public TimeOffPayAllocationsView()
            : base (IsPhone ? "TimeOffPayAllocationsView_iPhone" : "TimeOffPayAllocationsView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            AllocationTable.RegisterCell<PayAllocationItemCell> ();
            AllocationTable.RegisterSection<PayAllocationSectionCell> ();
            AllocationTable.RowHeight = 30;
            AllocationTable.SectionHeaderHeight = 30;
            AllocationTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            AllocationTable.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<TimeOffPayAllocationsView, TimeOffPayAllocationsViewModel>();
            set.Bind (AllocationTable).For (v => v.Items).To (vm => vm.Sections);
            set.Bind (this).For (v => v.SectionsUpdated).To (vm => vm.SectionsUpdated);
            set.Bind (NoDataLabel).For (v => v.Hidden).To (vm => vm.NoDataLabel).WithConversion (new StringVisibleConverter ());
            set.Bind (NoDataLabel).For (v => v.Text).To (vm => vm.NoDataLabel);
            set.Bind (TotalPaidHoursLabel).For (v => v.Text).To (vm => vm.TotalPaidHours).WithConversion(new StringFormatValueConverter(),"0.00");
            set.Bind (TotalNonPaidHoursLabel).For (v => v.Text).To (vm => vm.TotalUnpaidHours).WithConversion(new StringFormatValueConverter(),"0.00");
            set.Apply();
        }

        public override void DidMoveToParentViewController (UIViewController parent)
        {
            base.DidMoveToParentViewController (parent);
            if(parent != null && ViewState == ConsortiumViewState.Opening)
                ViewWillAppear (false);
        }
       
        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

            UpdateFooterFraming ();
        }

        private void UpdateFooterFraming()
        {
            if (m_originalFooterFrame == RectangleF.Empty)
                m_originalFooterFrame = TotalFooterView.Frame;

            if (m_originalTableFrame == RectangleF.Empty)
                m_originalTableFrame = AllocationTable.Frame;

            float maxContentSize = m_originalTableFrame.Height - m_originalFooterFrame.Height;
            float tableContentSize = AllocationTable.EstimatedHeight ();//AllocationTable.ContentSize.Height;

            float tableHeight = Math.Min (tableContentSize, maxContentSize);

            AllocationTable.AlwaysBounceVertical = tableContentSize > maxContentSize;
            AllocationTable.Bounces = tableContentSize > maxContentSize;

            AllocationTable.Frame = new RectangleF (m_originalTableFrame.X, m_originalTableFrame.Y, m_originalTableFrame.Width, tableHeight);
            TotalFooterView.Frame = new RectangleF (m_originalFooterFrame.X, AllocationTable.Frame.Bottom, m_originalFooterFrame.Width, m_originalFooterFrame.Height);

        }
    }
}

