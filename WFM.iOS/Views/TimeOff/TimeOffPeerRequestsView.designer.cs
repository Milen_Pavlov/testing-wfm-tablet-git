// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffPeerRequestsView")]
	partial class TimeOffPeerRequestsView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoDataLabel { get; set; }

		[Outlet]
		WFM.iOS.AlternateBackgroundTable PeerTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EmployeeLabel != null) {
				EmployeeLabel.Dispose ();
				EmployeeLabel = null;
			}

			if (NoDataLabel != null) {
				NoDataLabel.Dispose ();
				NoDataLabel = null;
			}

			if (PeerTable != null) {
				PeerTable.Dispose ();
				PeerTable = null;
			}
		}
	}
}
