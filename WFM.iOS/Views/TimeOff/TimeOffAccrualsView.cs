﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class TimeOffAccrualsView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

        public TimeOffAccrualsView()
            : base (IsPhone ? "TimeOffAccrualsView_iPhone" : "TimeOffAccrualsView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            AccrualTable.RegisterCell<AccrualCell> ();
            AccrualTable.RowHeight = 30;
            AccrualTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            AccrualTable.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<TimeOffAccrualsView, TimeOffAccrualsViewModel>();
            set.Bind (AccrualTable).For (v => v.Items).To (vm => vm.Accruals);
            set.Bind (NoDataLabel).For (v => v.Hidden).To (vm => vm.NoDataLabel).WithConversion (new StringVisibleConverter ());
            set.Bind (NoDataLabel).For (v => v.Text).To (vm => vm.NoDataLabel);
            set.Apply();
        }
    }
}

