﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using System.Collections.Generic;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;

namespace WFM.iOS
{
    public partial class TimeOffView : WFMView
    {
        private readonly IStringService m_localiser;
        UIBarButtonItem m_refresh;
        NotificationBarButtonItem m_notifications;
        ICommand m_cellSelectCommand;
        ICommand m_employeeSelectCommand;
        UIPopoverController m_dayDetailsPopover;
        TimeOffNotificationPopover m_notificationsPopover;
        EmployeeUpcomingRequestsPopover m_upcomingRequestsPopover;

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        public TimeOffViewModel VM
        {
            get { return ViewModel as TimeOffViewModel; }
        }

		public List<string> Segments
		{
			get { return null; }
			set 
			{
				List<string> segs = value;

				if (segs == null)
					return;

				Calendar.HeaderStrings = segs;
			}
		}

		public UIColor ButtonColor
		{
			get { return UIColor.White; } 
			set
			{
				ForwardButton.TintColor = value;
				ForwardButton.Layer.BorderColor = value.CGColor;
				BackButton.TintColor = value;
				BackButton.Layer.BorderColor = value.CGColor;
                HeaderBackground.BackgroundColor = value;
                EmployeesHeaderBackground.BackgroundColor = value;

                CheckboxAll.TintColor = value;
                CheckboxPending.TintColor = value;
                CheckboxApproved.TintColor = value;
                CheckboxDenied.TintColor = value;

                CheckboxAll.CheckboxBorderColor = value.CGColor;
                CheckboxPending.CheckboxBorderColor = value.CGColor;
                CheckboxApproved.CheckboxBorderColor = value.CGColor;
                CheckboxDenied.CheckboxBorderColor = value.CGColor;
			}
		}

		public DateTime FirstOfMonth
		{
			get { return DateTime.MinValue; }
			set
			{
				Calendar.SetMonth (value.Year, value.Month);

				//Set Label
				if (CurrentDateLabel != null)
					CurrentDateLabel.Text = string.Format ("{0} {1}", value.ToString ("MMMM"), value.Year);
			}
		}

        //Don't remove this event, Mvvm will look for this and raise it so the VM updates
        public event EventHandler FilterChanged;
        private TimeOffStatus m_filter;
        public TimeOffStatus Filter
        {
            get { return m_filter; }
            set
            {
                m_filter = value;
                CheckboxPending.IsToggled = m_filter.HasFlag (TimeOffStatus.Pending);
                CheckboxApproved.IsToggled = m_filter.HasFlag (TimeOffStatus.Approved);
                CheckboxDenied.IsToggled = m_filter.HasFlag (TimeOffStatus.Denied);
                CheckboxAll.IsToggled = m_filter.HasFlag (TimeOffStatus.Pending) && m_filter.HasFlag (TimeOffStatus.Approved) && m_filter.HasFlag (TimeOffStatus.Denied);

                //Allow MVVM to inform the VM of the change.
                if (FilterChanged != null)
                    FilterChanged (Filter, EventArgs.Empty);
            }
        }

        public bool IsLoading
        {
            get;
            set;
        }

        private bool m_loadingEmployees = false;
        public bool IsLoadingEmployees
        {
            get { return m_loadingEmployees; }
            set
            {
                m_loadingEmployees = value;

                if (LoadingEmployees != null)
                {
                    if (m_loadingEmployees)
                        LoadingEmployees.StartAnimating ();
                    else
                        LoadingEmployees.StopAnimating ();
                
                    LoadingEmployees.Hidden = !m_loadingEmployees;
                }

                if (Employees != null)
                {
                    if (m_loadingEmployees)
                    {
                        Employees.Alpha = 0;
                    }
                    else
                    {
                        UIView.Animate (0.25, 0, UIViewAnimationOptions.BeginFromCurrentState | UIViewAnimationOptions.CurveEaseInOut, () =>
                            {
                                Employees.Alpha = 1;
                            }, () =>
                            {
                            });
                    }
                }
            }
        }

		public TimeOffView()
			: base (IsPhone ? "TimeOffView_iPhone" : "TimeOffView_iPad", null)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			var settings = Mvx.Resolve<SettingsService> ();
			UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);

			CurrentDateLabel.Layer.BorderColor = primary.CGColor;
			CurrentDateLabel.Layer.BorderWidth = 1;
			CurrentDateLabel.Layer.CornerRadius = 4;
			CurrentDateLabel.TextColor = primary;

			ForwardButton.Layer.BorderWidth = 1;
			ForwardButton.Layer.CornerRadius = 4;

			BackButton.Layer.BorderWidth = 1;
			BackButton.Layer.CornerRadius = 4;

            LoadingView.Hidden = true; 
            LoadingView.Layer.CornerRadius = 10;
            LoadingView.Alpha = 0.75f;

            CheckboxAll.CheckboxBorderColor = primary.CGColor;
            CheckboxPending.CheckboxBorderColor = primary.CGColor;
            CheckboxApproved.CheckboxBorderColor = primary.CGColor;
            CheckboxDenied.CheckboxBorderColor = primary.CGColor;

            CheckboxAll.TextColor = primary;
            CheckboxPending.TextColor = primary;
            CheckboxApproved.TextColor = primary;
            CheckboxDenied.TextColor = primary;

            CheckboxAll.CheckboxBorderWidth = 1;
            CheckboxPending.CheckboxBorderWidth = 1;
            CheckboxApproved.CheckboxBorderWidth = 1;
            CheckboxDenied.CheckboxBorderWidth = 1;

            CheckboxAll.CheckboxCornerRadius = 4;
            CheckboxPending.CheckboxCornerRadius = 4;
            CheckboxApproved.CheckboxCornerRadius = 4;
            CheckboxDenied.CheckboxCornerRadius = 4;

            CheckboxAll.OnImage = UIImage.FromFile ("TickGrey.png");
            CheckboxPending.OnImage = UIImage.FromFile ("TickGrey.png");
            CheckboxApproved.OnImage = UIImage.FromFile ("TickGrey.png");
            CheckboxDenied.OnImage = UIImage.FromFile ("TickGrey.png");

            Calendar.RegisterCell<CalendarRequestCell> ();

            Employees.RegisterCell<EmployeeCell> ();
            Employees.RowHeight = 40;

            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, (v1, v2) =>
                {
                    m_notificationsPopover.Dismiss(true);
                });
            m_notifications = new NotificationBarButtonItem (UIImage.FromBundle ("Notifications"), UIBarButtonItemStyle.Plain, (v1, v2) =>
                {
                    m_notificationsPopover.PresentFromBarButtonItem(m_notifications, UIPopoverArrowDirection.Up, true);

                    if(VM!=null)
                    {
                        VM.LoadNotificationItems();
                    }
                });

            NavigationItem.RightBarButtonItems = new UIBarButtonItem[] { m_refresh, m_notifications};

            m_cellSelectCommand = new MvxCommand<CalendarRequestCell> (OnDaySelected);
            m_employeeSelectCommand = new MvxCommand<EmployeeCell> (OnEmployeeSelected);

            m_notificationsPopover = new TimeOffNotificationPopover ();
            m_upcomingRequestsPopover = new EmployeeUpcomingRequestsPopover ();
            m_upcomingRequestsPopover.DidDismiss += (object sender, EventArgs e) => 
                {
                    if(m_lastSelection!=null)
                        m_lastSelection.Selected = false;
                };

			var set = this.CreateBindingSet<TimeOffView, TimeOffViewModel>();
			set.Bind (this).For (v => v.Segments).To (vm => vm.Segments);
			set.Bind (this).For (v => v.FirstOfMonth).To (vm => vm.FirstOfMonth);
			set.Bind (this).For (v => v.ButtonColor).To (vm => vm.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (this).For (v => v.Title).To (vm => vm.Title);
            set.Bind (this).For (v => v.Filter).To (vm => vm.Filter);
            set.Bind (this).For (v => v.IsLoading).To (vm => vm.Loading);
            set.Bind (this).For (v => v.IsLoadingEmployees).To (vm => vm.LoadingEmployees);
            set.Bind (FilterTitle).For (v => v.Text).To (vm => vm.FilterTitle);
            set.Bind (EmployeesHeaderLabel).For (v => v.Text).To (vm => vm.EmployeesTitle);
            set.Bind (Employees).For (v => v.Items).To (vm => vm.AllEmployees);
            set.Bind (ForwardButton).To ("MoveForward");
            set.Bind (BackButton).To ("MoveBack");
            set.Bind (Calendar).For (v => v.Items).To (vm => vm.VisibleRequests);
            set.Bind (LoadingView).For (v => v.Hidden).To (vm => vm.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingSpinner).For(v => v.Hidden).To (vm => vm.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingLabel).To (vm => vm.LoadingMessage);
            set.Bind (m_refresh).For (v => v.Enabled).To (vm => vm.Loading).WithConversion (new HiddenConverter (), null);
            set.Bind (m_refresh).To("Refresh");
            set.Bind (m_notifications).For (v => v.NotificationCount).To (vm => vm.UnreadNotificationCount);
            set.Bind (m_notificationsPopover).For (v => v.Items).To (vm => vm.NotificationItems);
            set.Bind (m_notificationsPopover).For (v => v.Loading).To (vm => vm.NotificationsLoading);
            set.Bind (m_notificationsPopover).For (v => v.Selected).To ("OnNotificationTapped");
            set.Bind (CheckboxAll).For (v => v.Text).To (vm => vm.CheckboxAllTitle);
            set.Bind (CheckboxPending).For (v => v.Text).To (vm => vm.CheckboxPendingTitle);
            set.Bind (CheckboxApproved).For (v => v.Text).To (vm => vm.CheckboxApprovedTitle);
            set.Bind (CheckboxDenied).For (v => v.Text).To (vm => vm.CheckboxDeniedTitle);
            set.Bind (m_upcomingRequestsPopover).For (v => v.EmployeeInfo).To (vm => vm.SelectedEmployeeInfo);
            set.Bind (m_upcomingRequestsPopover).For (v => v.Items).To (vm => vm.EmployeeUpcomingRequests);
            set.Bind (m_upcomingRequestsPopover).For (v => v.Loading).To (vm => vm.LoadingEmployeeUpcomingRequests);
            set.Bind (m_upcomingRequestsPopover).For (v => v.Selected).To ("OnUpcomingRequestTapped");
            set.Bind (m_upcomingRequestsPopover).For (v => v.OnCreateSelected).To ("OnCreateHolidayRequestSelected");
            set.Bind (m_upcomingRequestsPopover).For (v => v.ShowAllHistory).To (vm => vm.ShowAllHistoryInPopover);
			set.Apply();
		}

        private EmployeeCell m_lastSelection;
        public void OnEmployeeSelected(EmployeeCell employeeCell)
        {
            m_lastSelection = employeeCell;

            if (VM != null)
            {
                if(VM.ConfigurationService.ShowPopoverEmployeeTimeOffView)
                    m_upcomingRequestsPopover.PresentFromRect (new RectangleF(0,0,employeeCell.Frame.Width, employeeCell.Frame.Height), employeeCell, UIPopoverArrowDirection.Left, true);
                
                VM.OnEmployeeSelected (employeeCell.DataContext as JDAEmployeeInfo);
            }
        }


        //This is done here instead of the ViewModel as we already have the data inside the cell, and we want to display a popover coming
        //out of the cell. If we passed to the viewmodel all we would have is the data, but not "where" its being dislayed from.
        public void OnDaySelected(CalendarRequestCell dayCell)
        {
            if (IsLoading)
                return;

            if (m_dayDetailsPopover != null)
            {
                m_dayDetailsPopover.Dismiss (false);
                m_dayDetailsPopover = null;
            }

            if (dayCell == null)
                return;

            CalendarDay dayData = dayCell.DataContext as CalendarDay;
            if (dayData == null)
                return;

            TimeOffDayData data = dayData.DataContext as TimeOffDayData;
            if (data == null)
                return;

            if (data.Data == null || data.Data.Count == 0)
                return;

            JDATimeOffRequestSection approved = new JDATimeOffRequestSection () { Date = dayData.Date, Status = TimeOffStatus.Approved };
            JDATimeOffRequestSection pending = new JDATimeOffRequestSection () { Date = dayData.Date, Status = TimeOffStatus.Pending };
            JDATimeOffRequestSection denied = new JDATimeOffRequestSection () { Date = dayData.Date, Status = TimeOffStatus.Denied };

            foreach (JDATimeOffRequest request in data.Data)
            {
                DateTime start = request.Start.Date;
                DateTime end = request.End.Date;

                if ((start < data.Date && end < data.Date) ||
                    (start > data.Date && end > data.Date))
                    continue;
                
                switch (request.Status)
                {
                    case TimeOffStatus.Approved:
                        approved.Items.Add (request);
                        break;
                    case TimeOffStatus.Denied:
                        denied.Items.Add (request);
                        break;
                    case TimeOffStatus.Pending:
                        pending.Items.Add (request);
                        break;
                }
            }

            List<ISection> items = new List<ISection> ();
            if (approved.Items.Count > 0)
                items.Add (approved);
            if (pending.Items.Count > 0)
                items.Add (pending);
            if (denied.Items.Count > 0)
                items.Add (denied);

            int total = approved.Items.Count + pending.Items.Count + denied.Items.Count;

            int sectionHeight = 25;
            int rowHeight = 40;
            int height = items.Count * sectionHeight + total * 40;

            IMobileDevice device = Mvx.Resolve<IMobileDevice> ();
            if (device.Platform == Consortium.Client.Core.Platform.IOS && device.SoftwareVersion.StartsWith ("7."))
            {
                height += 50;
            }

            RectangleF frame = new RectangleF (0, 0, 250, Math.Min (height, 200));

            //var viewController = new UIViewController();
            var viewController = new UINavigationController(new UIViewController());

            //Display popover
            m_dayDetailsPopover = new UIPopoverController(viewController);
            m_dayDetailsPopover.DidDismiss += (object s, EventArgs e) => { m_dayDetailsPopover = null; }; 
            m_dayDetailsPopover.PopoverContentSize = frame.Size;
            
            //Alter Nav bar
            float offsetY = 0;
            var nav = m_dayDetailsPopover.ContentViewController as UINavigationController;
            if (nav != null)
            {
                nav.NavigationBar.TintColor = UIColor.White;
                nav.NavigationBar.BarTintColor = Constants.TescoBlue;
                nav.NavigationBar.BarStyle = UIBarStyle.Black;
                nav.NavigationBar.Translucent = false;

                offsetY = nav.NavigationBar.Frame.Height;
                
                var root = nav.ViewControllers [0];
                root.Title = dayData.Date.ToString (m_localiser.Get("dd MMMM yyyy"));
                root.PreferredContentSize = new SizeF (frame.Size.Width, frame.Size.Height);
            }

            //Build ViewController
            var tableControl = new AlternateBackgroundTable(new RectangleF(0, offsetY, frame.Width, frame.Height));
            tableControl.RegisterSection<TimeOffRequestSectionCell> ();
            tableControl.RegisterCell<TimeOffRequestCell> ();
            tableControl.Items = items;
            tableControl.AlwaysBounceVertical = height > 200;
            tableControl.Bounces = height > 200;
            tableControl.SectionHeaderHeight = sectionHeight;
            tableControl.RowHeight = rowHeight;
            tableControl.DismissOnSelection = true;
            tableControl.Selected = new MvxCommand<JDATimeOffRequest> (OnTimeOffRequestSelected);
            tableControl.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            viewController.Add(tableControl);


            UIPopoverArrowDirection dir = UIPopoverArrowDirection.Any;
            RectangleF fromDir = new RectangleF (dayCell.Frame.Width, dayCell.Frame.Height / 2, 0, 0);

            if (dayCell.Frame.X > (Calendar.Frame.Width / 2) &&
                dayCell.Frame.Y > ((3 * Calendar.Frame.Height) / 2))
            {
                dir = UIPopoverArrowDirection.Down;
                fromDir = new RectangleF (dayCell.Frame.Width / 2, 0, 0, 0);
            }
            else if (dayCell.Frame.X > (Calendar.Frame.Width / 2))
            {
                dir = UIPopoverArrowDirection.Right;
                fromDir = new RectangleF (0, dayCell.Frame.Height / 2, 0, 0);
            }
            else
            {
                dir = UIPopoverArrowDirection.Left;
                fromDir = new RectangleF (dayCell.Frame.Width, dayCell.Frame.Height / 2, 0, 0);
            }

            m_dayDetailsPopover.PresentFromRect(fromDir, dayCell, dir, true);
        }

        public void OnTimeOffRequestSelected(JDATimeOffRequest row)
        {
            if (IsLoading)
                return;

            if (VM != null)
            {
                if (m_dayDetailsPopover != null)
                    m_dayDetailsPopover.Dismiss (true);

                if(VM.ConfigurationService.CanViewTimeOffTypeDetails)
                {                
                    VM.ShowTimeOffRequest (row);
                }
            }
        }

        private void OnToggle(CheckBox cb, bool isToggled)
        {
            //Update filter
            if (cb == CheckboxAll)
            {
                if (isToggled)
                    Filter = TimeOffStatus.Approved | TimeOffStatus.Denied | TimeOffStatus.Pending;
                else
                    Filter = TimeOffStatus.None;
            }
            else if (cb == CheckboxApproved)
            {
                if (isToggled)
                    Filter |= TimeOffStatus.Approved;
                else
                    Filter &= ~TimeOffStatus.Approved;
            }
            else if (cb == CheckboxDenied)
            {
                if (isToggled)
                    Filter |= TimeOffStatus.Denied;
                else
                    Filter &= ~TimeOffStatus.Denied;
            }
            else if (cb == CheckboxPending)
            {
                if (isToggled)
                    Filter |= TimeOffStatus.Pending;
                else
                    Filter &= ~TimeOffStatus.Pending;
            }
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

            if (CheckboxAll != null)
                CheckboxAll.OnToggle += OnToggle;

            if (CheckboxApproved != null)
                CheckboxApproved.OnToggle += OnToggle;

            if (CheckboxDenied != null)
                CheckboxDenied.OnToggle += OnToggle;

            if (CheckboxPending != null)
                CheckboxPending.OnToggle += OnToggle;

            if (Calendar != null)
                Calendar.CustomAction1 = m_cellSelectCommand;

            if (Employees != null)
                Employees.CustomCommand1 = m_employeeSelectCommand;

            if(m_lastSelection!=null)
                m_lastSelection.Selected = false;

        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);

            if (CheckboxAll != null)
                CheckboxAll.OnToggle -= OnToggle;

            if (CheckboxApproved != null)
                CheckboxApproved.OnToggle -= OnToggle;

            if (CheckboxDenied != null)
                CheckboxDenied.OnToggle -= OnToggle;

            if (CheckboxPending != null)
                CheckboxPending.OnToggle -= OnToggle;

            if (Calendar != null)
                Calendar.CustomAction1 = null;

            if (Employees != null)
                Employees.CustomCommand1 = null;

        }
    }
}