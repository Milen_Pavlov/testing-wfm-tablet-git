// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffEmployeeHistoryView")]
	partial class TimeOffEmployeeHistoryView
	{
		[Outlet]
		WFM.iOS.AlternateBackgroundTable HistoryTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoDataLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (HistoryTable != null) {
				HistoryTable.Dispose ();
				HistoryTable = null;
			}

			if (NoDataLabel != null) {
				NoDataLabel.Dispose ();
				NoDataLabel = null;
			}
		}
	}
}
