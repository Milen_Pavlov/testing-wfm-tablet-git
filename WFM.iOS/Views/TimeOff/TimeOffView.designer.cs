// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffView")]
	partial class TimeOffView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton BackButton { get; set; }

		[Outlet]
		Consortium.Client.iOS.CalendarGrid Calendar { get; set; }

		[Outlet]
		WFM.iOS.CheckBox CheckboxAll { get; set; }

		[Outlet]
		WFM.iOS.CheckBox CheckboxApproved { get; set; }

		[Outlet]
		WFM.iOS.CheckBox CheckboxDenied { get; set; }

		[Outlet]
		WFM.iOS.CheckBox CheckboxPending { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CurrentDateLabel { get; set; }

		[Outlet]
		WFM.iOS.AlternateBackgroundTable Employees { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView EmployeesHeaderBackground { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeesHeaderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView FilterIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FilterTitle { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ForwardButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderBackground { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingEmployees { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingSpinner { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (LoadingEmployees != null) {
				LoadingEmployees.Dispose ();
				LoadingEmployees = null;
			}

			if (BackButton != null) {
				BackButton.Dispose ();
				BackButton = null;
			}

			if (Calendar != null) {
				Calendar.Dispose ();
				Calendar = null;
			}

			if (CheckboxAll != null) {
				CheckboxAll.Dispose ();
				CheckboxAll = null;
			}

			if (CheckboxApproved != null) {
				CheckboxApproved.Dispose ();
				CheckboxApproved = null;
			}

			if (CheckboxDenied != null) {
				CheckboxDenied.Dispose ();
				CheckboxDenied = null;
			}

			if (CheckboxPending != null) {
				CheckboxPending.Dispose ();
				CheckboxPending = null;
			}

			if (CurrentDateLabel != null) {
				CurrentDateLabel.Dispose ();
				CurrentDateLabel = null;
			}

			if (Employees != null) {
				Employees.Dispose ();
				Employees = null;
			}

			if (EmployeesHeaderBackground != null) {
				EmployeesHeaderBackground.Dispose ();
				EmployeesHeaderBackground = null;
			}

			if (EmployeesHeaderLabel != null) {
				EmployeesHeaderLabel.Dispose ();
				EmployeesHeaderLabel = null;
			}

			if (FilterIcon != null) {
				FilterIcon.Dispose ();
				FilterIcon = null;
			}

			if (FilterTitle != null) {
				FilterTitle.Dispose ();
				FilterTitle = null;
			}

			if (ForwardButton != null) {
				ForwardButton.Dispose ();
				ForwardButton = null;
			}

			if (HeaderBackground != null) {
				HeaderBackground.Dispose ();
				HeaderBackground = null;
			}

			if (LoadingLabel != null) {
				LoadingLabel.Dispose ();
				LoadingLabel = null;
			}

			if (LoadingSpinner != null) {
				LoadingSpinner.Dispose ();
				LoadingSpinner = null;
			}

			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}
		}
	}
}
