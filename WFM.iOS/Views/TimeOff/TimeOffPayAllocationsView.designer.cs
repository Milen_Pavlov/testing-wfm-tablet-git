// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffPayAllocationsView")]
	partial class TimeOffPayAllocationsView
	{
		[Outlet]
		WFM.iOS.PayAllocationTable AllocationTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoDataLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TotalFooterView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalNonPaidHoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalPaidHoursLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AllocationTable != null) {
				AllocationTable.Dispose ();
				AllocationTable = null;
			}

			if (NoDataLabel != null) {
				NoDataLabel.Dispose ();
				NoDataLabel = null;
			}

			if (TotalFooterView != null) {
				TotalFooterView.Dispose ();
				TotalFooterView = null;
			}

			if (TotalPaidHoursLabel != null) {
				TotalPaidHoursLabel.Dispose ();
				TotalPaidHoursLabel = null;
			}

			if (TotalNonPaidHoursLabel != null) {
				TotalNonPaidHoursLabel.Dispose ();
				TotalNonPaidHoursLabel = null;
			}
		}
	}
}
