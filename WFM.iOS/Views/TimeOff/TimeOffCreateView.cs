﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Collections.Generic;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class TimeOffCreateView : TimeOffBaseDetailView
    {
        private UIColor m_primaryCol, m_primaryTextCol, m_primaryColHighlight;
        private TimeOffTypePopover m_timeOffTypePopover;
        private DatePickerPopover m_datePickerPopover;

        public TimeOffCreateViewModel VM
        {
            get { return ViewModel as TimeOffCreateViewModel; }
        }

        public override Type AttachTarget
        {
            get { return typeof(ConsortiumDrawerView); } 
        }

        private bool m_isAllDay;
        public bool IsAllDay
        {
            get { return m_isAllDay; }
            set
            {
                m_isAllDay = value;

                TabAllDay.BackgroundColor = (IsAllDay) ? m_primaryColHighlight : UIColor.LightGray;
                TabPartialDay.BackgroundColor = (IsAllDay) ? UIColor.LightGray : m_primaryColHighlight;

                TabAllDay.SetTitleColor ((IsAllDay) ? m_primaryTextCol : UIColor.Black, UIControlState.Normal);
                TabPartialDay.SetTitleColor ((IsAllDay) ? UIColor.Black : m_primaryTextCol, UIControlState.Normal);

                FromTimeText.Hidden = IsAllDay;
                ToTimeText.Hidden = IsAllDay;
                FromTimeButton.Hidden = IsAllDay;
                ToTimeButton.Hidden = IsAllDay;
                ContainerViews[1].Hidden = IsAllDay;//UGLY!!!! YAY!
                ContainerViews[3].Hidden = IsAllDay;//UGLY!!!! YAY!
            }
        }

        private bool m_loading;
        public bool Loading
        {
            get { return m_loading; }
            set
            {
                m_loading = value;

                if (m_loading)
                {
                    LoadingView.Alpha = 1;
                }
                else
                {
                    UIView.Animate (0.25, 0, UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.BeginFromCurrentState, () =>
                        {
                            LoadingView.Alpha = 0;
                        },
                        () =>
                        {
                        });
                }
            }
        }

        public TimeOffCreateView()
            : base (IsPhone ? "TimeOffDetailsView_iPhone" : "TimeOffCreateView_iPad", null)
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            // Themeing 
            var settings = Mvx.Resolve<SettingsService> ();
            m_primaryCol = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            m_primaryTextCol = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);
            m_primaryColHighlight = m_primaryCol;

            TypeDropdownIcon.Image = TypeDropdownIcon.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            TypeDropdownIcon.TintColor = m_primaryCol;

            SubmitButton.Layer.CornerRadius = 4;
            SubmitButton.BackgroundColor = m_primaryCol;
            SubmitButton.SetTitleColor (m_primaryTextCol, UIControlState.Normal);
            
            TabPartialDay.BackgroundColor = m_primaryCol;
            TabAllDay.BackgroundColor = m_primaryCol;

            foreach (UIView container in ContainerViews)
            {
                container.Layer.BorderColor = m_primaryCol.CGColor;
                container.Layer.BorderWidth = 1;
                container.Layer.CornerRadius = 4;
            }

            m_timeOffTypePopover = new TimeOffTypePopover ();
            m_datePickerPopover = new DatePickerPopover ();

            EmployeeNameContainer.BackgroundColor = m_primaryCol;
            Comment.Layer.BorderColor = m_primaryCol.CGColor;
            Comment.Layer.BorderWidth = 1;
            Comment.Layer.CornerRadius = 4;

            String dateFormat = Mvx.Resolve<IStringService> ().Get("{0:dd/MM/yy}");
            var set = this.CreateBindingSet<TimeOffCreateView, TimeOffCreateViewModel> ();
            set.Bind (this).For (v => v.IsAllDay).To (vm => vm.IsAllDay);

            set.Bind (m_timeOffTypePopover).For(v => v.Items).To (vm => vm.TimeOffTypes);
            set.Bind (m_timeOffTypePopover).For(v => v.Selected).To ("OnTimeOffTypeSelected");
            set.Bind (m_datePickerPopover).For (v => v.OnDateTimeSelected).To ("OnDateTimeSelected");

            set.Bind (TabAllDay).To ("OnAllDaySelected");
            set.Bind (TabPartialDay).To ("OnPartialDaySelected");
            set.Bind (SubmitButton).To ("SubmitRequest");

            set.Bind (FromDateText).To (vm => vm.StartTime).WithConversion (new ToStringValueConverter<DateTime> (), dateFormat);
            set.Bind (ToDateText).To (vm => vm.EndTime).WithConversion (new ToStringValueConverter<DateTime> (), dateFormat);
            set.Bind (FromTimeText).To (vm => vm.StartTime).WithConversion (new TimeToStringConverter());
            set.Bind (ToTimeText).To (vm => vm.EndTime).WithConversion (new TimeToStringConverter());
            set.Bind (TypeLabel).To (vm => vm.CurrentType.TypeName);
            set.Bind (Comment).To (vm => vm.Comment);

            set.Bind (this).For (v => v.Loading).To (vm => vm.Loading);

            set.Apply ();
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

            TypeSelectionButton.TouchUpInside += OnSelectType;

            //Date/Time Pickers
            FromDateButton.TouchUpInside += OnFromDateSelected;
            FromTimeButton.TouchUpInside += OnFromTimeSelected;
            ToDateButton.TouchUpInside += OnToDateSelected;
            ToTimeButton.TouchUpInside += OnToTimeSelected;
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);

            TypeSelectionButton.TouchUpInside -= OnSelectType;

            //Date/Time Pickers
            FromDateButton.TouchUpInside -= OnFromDateSelected;
            FromTimeButton.TouchUpInside -= OnFromTimeSelected;
            ToDateButton.TouchUpInside -= OnToDateSelected;
            ToTimeButton.TouchUpInside -= OnToTimeSelected;
        }

        private void OnFromDateSelected(object sender, EventArgs e)
        {
            VM.PopoverMode = TimeOffCreateViewModel.DateTimePopoverMode.StartDate;
            m_datePickerPopover.Setup ("Start Date", VM.StartTime.Date, UIDatePickerMode.Date);
            m_datePickerPopover.PresentFromRect(new RectangleF(FromDateButton.Frame.Width / 2, FromDateButton.Frame.Height, 0, 0), FromDateButton, UIPopoverArrowDirection.Up, true);
        }

        private void OnToDateSelected(object sender, EventArgs e)
        {
            VM.PopoverMode = TimeOffCreateViewModel.DateTimePopoverMode.EndDate;
            m_datePickerPopover.Setup ("End Date", VM.EndTime.Date, UIDatePickerMode.Date);
            m_datePickerPopover.PresentFromRect(new RectangleF(ToDateButton.Frame.Width / 2, ToDateButton.Frame.Height, 0, 0), ToDateButton, UIPopoverArrowDirection.Up, true);
        }

        private void OnFromTimeSelected(object sender, EventArgs e)
        {
            VM.PopoverMode = TimeOffCreateViewModel.DateTimePopoverMode.StartTime;
            m_datePickerPopover.Setup ("Start Time", VM.StartTime, UIDatePickerMode.Time);
            m_datePickerPopover.PresentFromRect(new RectangleF(FromTimeButton.Frame.Width / 2, FromTimeButton.Frame.Height, 0, 0), FromTimeButton, UIPopoverArrowDirection.Up, true);
        }

        private void OnToTimeSelected(object sender, EventArgs e)
        {
            VM.PopoverMode = TimeOffCreateViewModel.DateTimePopoverMode.EndTime;
            m_datePickerPopover.Setup ("End Time", VM.EndTime, UIDatePickerMode.Time);
            m_datePickerPopover.PresentFromRect(new RectangleF(ToTimeButton.Frame.Width / 2, ToTimeButton.Frame.Height, 0, 0), ToTimeButton, UIPopoverArrowDirection.Up, true);
        }

        void OnSelectType (object sender, EventArgs e)
        {
            m_timeOffTypePopover.Deselect ();
            m_timeOffTypePopover.PresentFromRect ( new RectangleF(TypeSelectionButton.Frame.Width / 2, TypeSelectionButton.Frame.Height, 0, 0), TypeSelectionButton, UIPopoverArrowDirection.Up, true);
        }
    }
}