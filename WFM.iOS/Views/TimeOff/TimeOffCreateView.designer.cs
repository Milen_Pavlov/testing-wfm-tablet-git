// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffCreateView")]
	partial class TimeOffCreateView
	{
		[Outlet]
		MonoTouch.UIKit.UITextView Comment { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] ContainerViews { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton FromDateButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FromDateText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton FromTimeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FromTimeText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HolidayDetailsRoot { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LoadingView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SubmitButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TabAllDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TabHeader { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TabPartialDay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TimeOffRequestInfo { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ToDateButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ToDateText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ToTimeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ToTimeText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView TypeDropdownIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TypeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TypeSelectionButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Comment != null) {
				Comment.Dispose ();
				Comment = null;
			}

			if (FromDateButton != null) {
				FromDateButton.Dispose ();
				FromDateButton = null;
			}

			if (FromDateText != null) {
				FromDateText.Dispose ();
				FromDateText = null;
			}

			if (FromTimeButton != null) {
				FromTimeButton.Dispose ();
				FromTimeButton = null;
			}

			if (FromTimeText != null) {
				FromTimeText.Dispose ();
				FromTimeText = null;
			}

			if (HolidayDetailsRoot != null) {
				HolidayDetailsRoot.Dispose ();
				HolidayDetailsRoot = null;
			}

			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}

			if (SubmitButton != null) {
				SubmitButton.Dispose ();
				SubmitButton = null;
			}

			if (TabAllDay != null) {
				TabAllDay.Dispose ();
				TabAllDay = null;
			}

			if (TabHeader != null) {
				TabHeader.Dispose ();
				TabHeader = null;
			}

			if (TabPartialDay != null) {
				TabPartialDay.Dispose ();
				TabPartialDay = null;
			}

			if (TimeOffRequestInfo != null) {
				TimeOffRequestInfo.Dispose ();
				TimeOffRequestInfo = null;
			}

			if (ToDateButton != null) {
				ToDateButton.Dispose ();
				ToDateButton = null;
			}

			if (ToDateText != null) {
				ToDateText.Dispose ();
				ToDateText = null;
			}

			if (ToTimeButton != null) {
				ToTimeButton.Dispose ();
				ToTimeButton = null;
			}

			if (ToTimeText != null) {
				ToTimeText.Dispose ();
				ToTimeText = null;
			}

			if (TypeDropdownIcon != null) {
				TypeDropdownIcon.Dispose ();
				TypeDropdownIcon = null;
			}

			if (TypeLabel != null) {
				TypeLabel.Dispose ();
				TypeLabel = null;
			}

			if (TypeSelectionButton != null) {
				TypeSelectionButton.Dispose ();
				TypeSelectionButton = null;
			}
		}
	}
}
