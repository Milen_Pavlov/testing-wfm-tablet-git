﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class TimeOffEmployeeHistoryView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

        public TimeOffEmployeeHistoryView()
            : base (IsPhone ? "TimeOffEmployeeHistoryView_iPhone" : "TimeOffEmployeeHistoryView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            HistoryTable.RegisterCell<EmployeeHistoryCell> ();
            HistoryTable.RowHeight = 30;
            HistoryTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            HistoryTable.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<TimeOffEmployeeHistoryView, TimeOffEmployeeHistoryViewModel>();
            set.Bind (HistoryTable).For (v => v.Items).To (vm => vm.History);
            set.Bind (NoDataLabel).For (v => v.Hidden).To (vm => vm.NoDataLabel).WithConversion (new StringVisibleConverter (), null);
            set.Bind (NoDataLabel).For (v => v.Text).To (vm => vm.NoDataLabel);
            set.Apply();
        }
    }
}

