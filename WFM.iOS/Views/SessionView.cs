using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class SessionView : ConsortiumModalView
    {
        ISSOLogin m_webLogin;

        public bool Loading
        {
            get { return SignInButton.Hidden; }
            set 
            { 
                SignInButton.Hidden = value;
                ResetButton.Enabled = !value;
                SettingsButton.Enabled = !value;
                LoadingView.Hidden = !value;
            }
        }

        // Cookie clearing special for web page (different from normal client)
        public bool ClearCookies {
            get { return false; }
            set { if (value) m_webLogin.ClearCookies (); }
        }

        // Special case for show/hide browser
        private bool m_ssoLogin;
        public bool SSOLogin {
            get { return m_ssoLogin; }
            set 
            {
                if (!m_ssoLogin && value) 
                {
                    m_ssoLogin = true;

                    if (IsViewLoaded && View.Window != null)
                        m_webLogin.Show (this);
                } 
                else if (m_ssoLogin && !value) 
                {
                    m_ssoLogin = false;
                    m_webLogin.Hide ();
                }
            }
        }

        public SessionViewModel VM
        {
            get { return (SessionViewModel)this.ViewModel; }
        }

        public SessionView ()
            : base (IsPhone ? "SessionView_iPhone" : "SessionView_iPad", null)
        {
            m_webLogin = new WKWebViewSSOLogin ();
            //m_webLogin = new SafariViewSSOLogin ();
        }

        partial void OnUserName (NSObject sender)
        {
            Password.BecomeFirstResponder();
        }

        partial void OnPassword (NSObject sender)
        {
            OnSignIn(null);
        }

        partial void OnSignIn (NSObject sender)
        {
            UserName.ResignFirstResponder();
            Password.ResignFirstResponder();
            VM.OnSignIn ();
        }

        public override void ViewOpened ()
        {
            base.ViewOpened ();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor secondary = ColorUtility.FromHexString (settings.AppTheme.SecondaryColor);
            UIColor loginColour = ColorUtility.FromHexString (settings.AppTheme.VersionAndEnvironmentColor);

            SignInButton.SetTitleColor (secondary, UIControlState.Normal);
            SettingsButton.SetTitleColor(loginColour, UIControlState.Normal);
            VersionLabel.TextColor = loginColour;

            // m_ssoLogin can be set before view appears
            if (m_ssoLogin)
                m_webLogin.Show (this);
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            IStringService localiser = Mvx.Resolve<IStringService> ();

            // Set the app version
            VersionLabel.Text = "v" + NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();

            LoadingView.Hidden = true; 
            LoadingView.Layer.CornerRadius = 10;
            LoadingView.Alpha = 0.8f;

            LogoImage.ContentMode = UIViewContentMode.ScaleAspectFit;
            BackgroundImage.ContentMode = UIViewContentMode.ScaleAspectFill;

            SignInButton.SetTitle (localiser.Get ("sign_in_button"), UIControlState.Normal);
            SignInButton.SetTitle (localiser.Get ("sign_in_button"), UIControlState.Disabled);

            var set = this.CreateBindingSet<SessionView, SessionViewModel>();
            set.Bind(UserName).To(m => m.UserName);
            set.Bind(Password).To(m => m.Password);
            set.Bind(MessageLabel).To(m => m.Message);
            set.Bind(this).For(b => b.Loading).To(m => m.Loading);
            set.Bind(this).For(v => v.SSOLogin).To(vm => vm.ShowSSOLogin);
            set.Bind(this).For(x => x.ClearCookies).To(m => m.ClearCookies);
            set.Bind(SignInText).For(x => x.Text).To(m => m.LoadingText);
            set.Bind(SettingsButton).For (c => c.Hidden).To (m => m.HideSettingsButton);
            set.Bind(SettingsButton).To("OnShowSettings");
            set.Bind(LogoImage).For(b => b.Uri).To(m => m.Logo);
            set.Bind(LogoImage).For(b => b.BackgroundColor).To(m => m.Theme.LoginBarColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(LogoImage).For(b => b.Hidden).To(m => m.Theme.ShowLoginBar).WithConversion("InvertedBoolean");
            set.Bind(BackgroundImage).For(b => b.Uri).To(m => m.Background);
            set.Bind(PassBackgroundView).For(b => b.BackgroundColor).To(m => m.Theme.LoginBarColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(PassBackgroundView).For (v => v.Hidden).To (vm => vm.ShowCredentials).WithConversion ("InvertedBoolean");
            set.Bind(UserBackgroundView).For(b => b.BackgroundColor).To(m => m.Theme.LoginBarColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(UserBackgroundView).For (v => v.Hidden).To (vm => vm.ShowCredentials).WithConversion ("InvertedBoolean");
            set.Bind(UserName).For(b => b.TextColor).To(m => m.Theme.LoginTextColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(Password).For(b => b.TextColor).To(m => m.Theme.LoginTextColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(UsernameLabel).For(b => b.TextColor).To(m => m.Theme.LoginTextColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(PasswordLabel).For(b => b.TextColor).To(m => m.Theme.LoginTextColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(MessageLabel).For(b => b.TextColor).To(m => m.Theme.VersionAndEnvironmentColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(SignInButton).For(b => b.BackgroundColor).To(m => m.Theme.HighlightColor).WithConversion (new HexColorCoverter (), null);
            set.Bind(VersionLabel).For(b => b.TextColor).To(m => m.Theme.VersionAndEnvironmentColor).WithConversion(new HexColorCoverter(), null);
            set.Bind(VersionLabel).To(m => m.Version);
            set.Bind(ResetButton).To ("OnResetPassword");
            set.Bind(ResetButton).For (v => v.Hidden).To (vm => vm.ShowResetPassword).WithConversion("InvertedBoolean");
            set.Apply();
        }
    }
}
