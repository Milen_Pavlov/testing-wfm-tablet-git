﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ScheduleDataView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ConsortiumTabView); } 
        }

        public ScheduleDataView()
            : base (IsPhone ? "ScheduleDataView_iPhone" : "ScheduleDataView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleDataView, ScheduleDataViewModel>();
            set.Apply();
        }
    }
}

