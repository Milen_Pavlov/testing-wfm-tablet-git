﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ForecastGroupView : ConsortiumPopoverView
    {
        public int SelectedRow
        {
            get { return GroupItems.IndexPathForSelectedRow.Row; }
            set { GroupItems.SelectRow (NSIndexPath.FromRowSection (value, 0), false, UITableViewScrollPosition.None); }
        }

        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public ForecastGroupView ()
            : base (UserInterfaceIdiomIsPhone ? "ForecastGroupView_iPhone" : "ForecastGroupView_iPad", null)
        {
            ArrowDirection = UIPopoverArrowDirection.Any;
            Title = "Select Group";
        }

        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            GroupItems.RegisterCell<ForecastGroupViewCell>();
            // Perform any additional setup after loading the view, typically from a nib.
            // View Model Binding
            var set = this.CreateBindingSet<ForecastGroupView, ForecastGroupViewModel>();
            set.Bind (GroupItems).To (m => m.Groups);
            set.Bind (this).For(b => b.SelectedRow).To (m => m.GroupIndex);
            set.Bind (GroupItems).For (b => b.Selected).To ("OnSelectGroup");
            set.Apply();
        }
    }
}

