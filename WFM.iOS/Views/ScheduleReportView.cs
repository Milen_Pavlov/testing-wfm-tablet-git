﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleReportView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(ScheduleGridGraphView); } 
        }

        public ScheduleReportView()
            : base (IsPhone ? "ScheduleReportView_iPhone" : "ScheduleReportView_iPad", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            UIColor textColor = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);

            HeaderLabels.TextColor = textColor;
            HeaderLabels.Superview.BackgroundColor = primary;

            ReportTable.RegisterCell<ReportCell> ();
            ReportTable.RowHeight = 30;

            // View Model Binding
            var set = this.CreateBindingSet<ScheduleReportView, ScheduleReportViewModel>();
            set.Bind (ReportTable).To (b => b.Rows);
            set.Apply();
        }
    }
}

