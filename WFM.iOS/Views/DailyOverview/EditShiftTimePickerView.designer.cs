// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("EditShiftTimePickerView")]
	partial class EditShiftTimePickerView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton Apply { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIDatePicker TimePicker { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Apply != null) {
				Apply.Dispose ();
				Apply = null;
			}

			if (TimePicker != null) {
				TimePicker.Dispose ();
				TimePicker = null;
			}
		}
	}
}
