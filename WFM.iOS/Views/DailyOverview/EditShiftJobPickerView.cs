﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;

namespace WFM.iOS
{
    public partial class EditShiftJobPickerView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(DailyOverviewEditShiftView); }
        }

        public EditShiftJobPickerView ()
            : base (IsPhone ? "EditShiftJobPickerView_iPhone" : "EditShiftJobPickerView_iPad", null)
        {
            Title = "Select Role";
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            Table.RegisterCell<EditShiftJobCell> ();
            Table.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            // View Model Binding
            var set = this.CreateBindingSet<EditShiftJobPickerView, EditShiftJobPickerViewModel>();
            set.Bind (Table).To (m => m.Types);
            set.Bind (Table).For(b => b.Selected).To ("OnSelected");
            set.Apply();
        }
    }
}
