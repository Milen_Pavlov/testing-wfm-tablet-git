// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("DailyOverviewEditShiftView")]
	partial class DailyOverviewEditShiftView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton AddBreak { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton[] AddButtons { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton AddJob { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton AddMeal { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton[] ButtonsToFormat { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Date { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditDate { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditJob { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Job { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Loading { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LoadingMessage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView LoadingSpinner { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton MatchPunches { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Name { get; set; }

		[Outlet]
		WFM.iOS.AlternateBackgroundTable PunchTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AddBreak != null) {
				AddBreak.Dispose ();
				AddBreak = null;
			}

			if (AddJob != null) {
				AddJob.Dispose ();
				AddJob = null;
			}

			if (AddMeal != null) {
				AddMeal.Dispose ();
				AddMeal = null;
			}

			if (Date != null) {
				Date.Dispose ();
				Date = null;
			}

			if (EditDate != null) {
				EditDate.Dispose ();
				EditDate = null;
			}

			if (EditJob != null) {
				EditJob.Dispose ();
				EditJob = null;
			}

			if (EditName != null) {
				EditName.Dispose ();
				EditName = null;
			}

			if (Job != null) {
				Job.Dispose ();
				Job = null;
			}

			if (Loading != null) {
				Loading.Dispose ();
				Loading = null;
			}

			if (LoadingMessage != null) {
				LoadingMessage.Dispose ();
				LoadingMessage = null;
			}

			if (LoadingSpinner != null) {
				LoadingSpinner.Dispose ();
				LoadingSpinner = null;
			}

			if (MatchPunches != null) {
				MatchPunches.Dispose ();
				MatchPunches = null;
			}

			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}

			if (PunchTable != null) {
				PunchTable.Dispose ();
				PunchTable = null;
			}
		}
	}
}
