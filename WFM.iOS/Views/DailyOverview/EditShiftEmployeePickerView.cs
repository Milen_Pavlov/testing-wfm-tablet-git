﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public partial class EditShiftEmployeePickerView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(DailyOverviewEditShiftView); }
        }

        public EditShiftEmployeePickerView ()
            : base (IsPhone ? "EditShiftEmployeePickerView_iPhone" : "EditShiftEmployeePickerView_iPad", null)
        {
            Title = "Select Employee";
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            Table.RegisterCell<EditShiftJobCell> ();
            Table.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            // View Model Binding
            var set = this.CreateBindingSet<EditShiftEmployeePickerView, EditShiftEmployeePickerViewModel>();
            set.Bind (Table).To (m => m.Employees);
            set.Bind (Table).For(b => b.Selected).To ("OnSelected");
            set.Apply();
        }
    }
}
