using System;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.CoreGraphics;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using WFM.Core;
using System.Collections.Generic;

namespace WFM.iOS
{
    public partial class DailyOverviewView : WFMView
    {
        public static UIColor Green = new UIColor(0.22f, 0.71f, 0.29f, 1.0f);
        public static UIColor Orange = UIColor.Orange;
        public static UIColor Red = new UIColor(0.93f, 0.15f, 0.15f, 1.0f);

        //Custom bar button setup
        UIView m_majorFilterButtonRoot;
        UIImageView m_majorFilterButtonImage;
        UIButton m_majorFilterButtonButton;

        UIBarButtonItem m_refresh;
        UIBarButtonItem m_filter;
        UIBarButtonItem m_majorFilter;
        UIImage[] m_majorFilterButtons;

        FilterPopover m_filterPopover;

        public DailyOverviewViewModel VM
        {
            get
            {
                return (DailyOverviewViewModel)ViewModel;
            }
        }

        private UIColor m_buttonColor;
		public UIColor ButtonColor
		{
            get { return m_buttonColor; } 
			set
			{
                m_buttonColor = value;

				NextButton.TintColor = value;
				NextButton.Layer.BorderColor = value.CGColor;
				PrevButton.TintColor = value;
				PrevButton.Layer.BorderColor = value.CGColor;
                DateButton.TintColor = value;
                DateButton.Layer.BorderColor = value.CGColor;
                AddShift.TintColor = value;
                AddShift.Layer.BorderColor = value.CGColor;
                AddShift.TitleLabel.TextColor = value;

                WeekSegmentRoot.Layer.BorderColor = value.CGColor;
                foreach (var button in WeekSegmentButtons)
                {
                    button.SetTitleColor (ButtonColor, UIControlState.Normal);
                    button.SetTitleColor (UIColor.White, UIControlState.Selected);
                    button.SetBackgroundImage (ImageWithColor (UIColor.White), UIControlState.Normal);
                    button.SetBackgroundImage (ImageWithColor (ButtonColor), UIControlState.Selected);
                }

                foreach (var separator in WeekSegmentSeparators)
                    separator.BackgroundColor = value;

                foreach (var separator in HeaderSeparators)
                    separator.BackgroundColor = UIColor.LightGray;

                HeaderRoot.BackgroundColor = value;
			}
		}
  
		public List<string> Segments
		{
			get { return null; }
			set 
			{
				List<string> segs = value;

				if (segs == null)
					return;

                for (int i = 0; i < segs.Count; i++)
                {
                    WeekSegmentButtons[i].SetTitle (segs[i], UIControlState.Normal);
                    WeekSegmentButtons[i].SetTitle (segs[i], UIControlState.Selected);
                }
			}
		}

        public List<Tuple<int, int>> SegmentCounts
        {
            get { return null; }
            set 
            {
                if (value != null)
                {
                    for (int i = 0; i < value.Count; i++)
                    {
                        if (i < WeekSegmentMinorExceptions.Length)
                        {
                            WeekSegmentMinorExceptions[i].Text = string.Format ("{0}", (value[i].Item1 != DailyOverviewViewModel.BlankCountValue) ? value[i].Item1.ToString () : string.Empty);
                            WeekSegmentMinorExceptions[i].TextColor = (value[i].Item1 > 0) ? Orange : Green;
                        }

                        if (i < WeekSegmentMajorExceptions.Length)
                        {

                            WeekSegmentMajorExceptions[i].Text = string.Format ("{0}", (value[i].Item2 != DailyOverviewViewModel.BlankCountValue) ? value[i].Item2.ToString () : string.Empty);
                            WeekSegmentMajorExceptions[i].TextColor = (value[i].Item2 > 0) ? Red : Green;
                        }
                    }
                }
                else
                {
                    foreach (var count in WeekSegmentMinorExceptions)
                        count.Text = string.Empty;

                    foreach (var count in WeekSegmentMajorExceptions)
                        count.Text = string.Empty;
                }
            }
        }

        private int m_selectedWeekSegment;
        public int SelectedWeekSegment
        {
            get { return m_selectedWeekSegment; }
            set 
            { 
                m_selectedWeekSegment = value; 

                for (int i = 0; i < WeekSegmentButtons.Length; i++)
                {
                    WeekSegmentButtons[i].Selected = i == value;
                }
            }
        }

        public string DateButtonValue
        {
            get{ return null; }
            set
            {
                DateButton.SetTitle (value, UIControlState.Normal);
            }
        }

        public override Type AttachTarget
        {
            get { return typeof(DrawerView); } 
        }

        private bool m_isMajorFilter = false;
        public bool IsMajorFilter
        {
            get { return m_isMajorFilter; }
            set
            {
                m_isMajorFilter = value;

                if (m_majorFilterButtonButton != null)
                    m_majorFilterButtonButton.SetTitleColor ((value) ? UIColor.White : UIColor.Black, UIControlState.Normal);
            }
        }

        public string EmployeeNameTitle
        {
            get { return string.Empty; }
            set { EmployeeNameLabel.Text = value; }
        }

        public DailyOverviewView()
            : base (IsPhone ? "DailyOverviewView_iPhone" : "DailyOverviewView_iPad", null)
        {   
            m_majorFilterButtons = new UIImage[] { UIImage.FromBundle("yellow_triangle"), UIImage.FromBundle("red_triangle") };
        }

        private void OnWeekSelect (object sender, EventArgs e)
        {
            //Get index
            int index = -1;
            for (int i = 0; i < WeekSegmentButtons.Length; i++)
                if (sender == WeekSegmentButtons[i])
                    index = i;

            if (index != -1)
            {
                SelectedWeekSegment = index;
                ((DailyOverviewViewModel)ViewModel).OnWeekSelect(SelectedWeekSegment).ConfigureAwait(false);
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            m_majorFilterButtonRoot = new UIView(new RectangleF(0,0,24,24));
            m_majorFilterButtonImage = new UIImageView (new RectangleF (0, 0, 24, 24));
            m_majorFilterButtonButton = new UIButton (new RectangleF (0, 0, 24, 24));
            m_majorFilterButtonButton.SetTitle ("!", UIControlState.Normal);
            m_majorFilterButtonImage.Image = m_majorFilterButtons[0];
            m_majorFilterButtonRoot.Add (m_majorFilterButtonImage);
            m_majorFilterButtonRoot.Add (m_majorFilterButtonButton);
            m_majorFilter = new UIBarButtonItem (m_majorFilterButtonRoot);

            // Popovers
            m_filterPopover = new FilterPopover ();

            m_filter = new UIBarButtonItem (UIImage.FromBundle ("Filter"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_filterPopover.PresentFromBarButtonItem(m_filter, UIPopoverArrowDirection.Up, true);
            });

            m_refresh = new UIBarButtonItem (UIImage.FromBundle ("Refresh"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_filterPopover.Dismiss(true);
            });

            List<UIBarButtonItem> barButtons = new List<UIBarButtonItem> ();
            barButtons.Add (m_refresh);
            barButtons.Add (m_filter);
            barButtons.Add (m_majorFilter);

            NavigationItem.RightBarButtonItems = barButtons.ToArray();
                
            // Button Layout
            PrevButton.Layer.BorderWidth = 1;
            PrevButton.Layer.CornerRadius = 4;
            NextButton.Layer.BorderWidth = 1;
            NextButton.Layer.CornerRadius = 4;
            DateButton.Layer.BorderWidth = 1;
            DateButton.Layer.CornerRadius = 4;
            AddShift.Layer.BorderWidth = 1;
            AddShift.Layer.CornerRadius = 4;

            // Border shenanigans
            WeekSegmentRoot.Layer.BorderWidth = 1;
            WeekSegmentRoot.Layer.CornerRadius = 4;

            for (int i = 0; i < WeekSegmentMinorExceptions.Length; i++)
                WeekSegmentMinorExceptions[i].TextColor = Green;

            for (int i = 0; i < WeekSegmentMajorExceptions.Length; i++)
                WeekSegmentMajorExceptions[i].TextColor = Green;

            // ios7 layout
            if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            //TODO Array button bindings to single function?
            foreach(var button in WeekSegmentButtons)
                button.TouchUpInside += OnWeekSelect;

            Data.RegisterCell<DailyOverviewItemCell> ();
            Data.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            Data.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<DailyOverviewView, DailyOverviewViewModel>();
            set.Bind (this).For (v => v.Title).To (vm => vm.Title);
            set.Bind (m_refresh).For (b => b.Enabled).To (m => m.Loading).WithConversion (new HiddenConverter (), null);
            set.Bind (m_refresh).To("OnRefresh");
            set.Bind (this).For (m => m.DateButtonValue).To (m => m.Date);
            set.Bind (NextButton).To("OnMoveNext");
            set.Bind (PrevButton).To("OnMovePrev");
            set.Bind(LoadingRootView).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingActivity).For(b => b.Hidden).To (m => m.Loading).WithConversion(new HiddenConverter(), null);
            set.Bind (LoadingLabel).To (m => m.LoadingMessage);
            set.Bind (EmptyLabel).To (m => m.EmptyMessage);
            set.Bind (EmptyLabel).For (v => v.Hidden).To (vm => vm.EmptyMessage).WithConversion (new StringVisibleConverter (), null);
            set.Bind (Data).For (v => v.CustomCommand1).To ("OnTimecardSelected");
            set.Bind (AddShift).To ("OnAddShift");
            set.Bind(AddShift).For(v => v.Hidden).To(vm => vm.CanAddShift).WithConversion(new HiddenConverter());
            set.Bind (Data).For (v => v.Items).To (vm => vm.FilteredTimecards);
            set.Bind (this).For(m => m.SelectedWeekSegment).To (m => m.WeekSelect);
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
	    	set.Bind (this).For (v => v.Segments).To (vm => vm.Segments);
            set.Bind (this).For (v => v.SegmentCounts).To (vm => vm.SegmentCounts);
            set.Bind (m_majorFilterButtonImage).For (b => b.Image).To(m => m.MajorFilter).WithConversion (new ImageBoolToIndexConvertor (), m_majorFilterButtons);
            set.Bind (this).For (v => v.IsMajorFilter).To (m => m.MajorFilter);
            set.Bind (m_majorFilterButtonButton).To ("OnMajorFilter");
            set.Bind (this).For (v => v.EmployeeNameTitle).To (vm => vm.EmployeeNameTitle);
            set.Bind(DateButton).To("OnDateSelect");

            set.Bind (m_filterPopover).For (b => b.Apply).To("OnApplyFilter");
            set.Bind (m_filterPopover).For (b => b.Filters).To(m => m.Filters);

            set.Apply();
        }

        private UIImage ImageWithColor(UIColor color)
        {
            RectangleF rect = new RectangleF (0, 0, 1, 1);
            UIGraphics.BeginImageContext (rect.Size);
            CGContext context = UIGraphics.GetCurrentContext ();

            context.SetFillColor (color.CGColor);
            context.FillRect (rect);

            UIImage image = UIGraphics.GetImageFromCurrentImageContext ();
            UIGraphics.EndImageContext ();

            return image;
        }

        protected override void ViewClosed()
        {
            base.ViewClosed ();

            if(m_filterPopover != null)
                m_filterPopover.Dismiss (false);
        }
    }
}
