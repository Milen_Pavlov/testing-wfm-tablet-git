﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public partial class EditShiftAuditReasonPickerView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(DailyOverviewEditShiftView); }
        }

        public EditShiftAuditReasonPickerView ()
            : base (IsPhone ? "EditShiftAuditReasonPickerView_iPhone" : "EditShiftAuditReasonPickerView_iPad", null)
        {
            Title = "Select Audit Reason";
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            Table.RegisterCell<EditShiftJobCell> ();
            Table.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            // View Model Binding
            var set = this.CreateBindingSet<EditShiftAuditReasonPickerView, EditShiftAuditReasonPickerViewModel>();
            set.Bind (Table).To (m => m.Types);
            set.Bind (Table).For(b => b.Selected).To ("OnSelected");
            set.Apply();
        }
    }
}
