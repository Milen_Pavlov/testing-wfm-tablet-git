﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using MonoTouch.Foundation;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class EditShiftTimePickerView : ConsortiumView
    {
        public override Type AttachTarget
        {
            get { return typeof(DailyOverviewEditShiftView); }
        }

        public EditShiftTimePickerViewModel VM
        {
            get { return (EditShiftTimePickerViewModel)DataContext; }
        }

        private DateTime m_time;
        public DateTime Time
        {
            get { return m_time; }
            set 
            {
                m_time = value;
                TimePicker.MinimumDate = m_time.Date;
                TimePicker.MaximumDate = m_time.Date.AddDays(1);
                TimePicker.Date = m_time;
            }
        }

        public EditShiftTimePickerView ()
            : base (IsPhone ? "EditShiftTimePickerView_iPhone" : "EditShiftTimePickerView_iPad", null)
        {
            Title = "Select Time";
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad ();

            Apply.Layer.CornerRadius = 4.0f;
            Apply.SetTitleColor (UIColor.White, UIControlState.Normal);
            Apply.SetTitleColor (UIColor.White, UIControlState.Selected);

            Apply.TouchUpInside += OnApply;

            // View Model Binding
            var set = this.CreateBindingSet<EditShiftTimePickerView, EditShiftTimePickerViewModel>();
            set.Bind(Apply).For(b => b.BackgroundColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (this).For (b => b.Time).To (m => m.Time);
            set.Apply();

            TimePicker.TimeZone = NSTimeZone.LocalTimeZone;
            TimePicker.Locale = new NSLocale(Mvx.Resolve<ITimeFormatService>().TimePickerLocale);
        }

        void OnApply (object sender, EventArgs e)
        {
            VM.Time = TimePicker.Date;
            VM.OnApply ();   
        }
    }
}
