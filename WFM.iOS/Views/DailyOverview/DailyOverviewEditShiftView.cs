using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using System.Drawing;
using System.Linq;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class DailyOverviewEditShiftView : ConsortiumPopoverView
    {
        private readonly float c_buttonSpacing = 24;

        private readonly IStringService m_localiser;
        private UILabel m_title;
        private UIBarButtonItem m_cancel;
        private UIBarButtonItem m_save;

        public DailyOverviewEditShiftViewModel VM
        {
            get { return (ViewModel as DailyOverviewEditShiftViewModel); }
        }

        private UIColor m_buttonColor;
        public UIColor ButtonColor
        {
            get { return m_buttonColor; } 
            set
            {
                m_buttonColor = value;

                foreach (var button in ButtonsToFormat)
                {
                    button.Layer.BorderColor = value.CGColor;
                    button.BackgroundColor = value;
                }
            }
        }

        private bool m_addJobEnabled;
        public bool AddJobEnabled
        {
            get { return m_addJobEnabled; }
            set 
            {
                m_addJobEnabled = value; 
                AddJob.Enabled = value;
                if (value == true)
                {
                    AddJob.BackgroundColor = m_buttonColor;
                }
                else
                {
                    AddJob.BackgroundColor = m_buttonColor.ColorWithAlpha(0.5f);
                }
            }
        }

        private string m_loadingMessageText;
        public string LoadingMessageText
        {
            get { return m_loadingMessageText; }
            set
            {
                m_loadingMessageText = value;
                if (String.IsNullOrEmpty (value))
                {
                    LoadingMessage.Hidden = true;
                }
                else
                {
                    LoadingMessage.Text = value;
                    LoadingMessage.Hidden = false;
                }

            }
        }

        private bool m_allowAddMeal;
        public bool AllowAddMeal
        {
            get { return m_allowAddMeal; }
            set { m_allowAddMeal = value; AddMeal.Hidden = !m_allowAddMeal; LayoutButtons (); }
        }

        private bool m_allowAddBreak;
        public bool AllowAddBreak
        {
            get { return m_allowAddBreak; }
            set { m_allowAddBreak = value; AddBreak.Hidden = !m_allowAddBreak; LayoutButtons (); }
        }

        private bool m_allowAddJob;
        public bool AllowAddJob
        {
            get { return m_allowAddJob; }
            set { m_allowAddJob = value; AddJob.Hidden = !m_allowAddJob; LayoutButtons (); }
        }

        private bool m_canSave;
        public bool CanSave
        {
            get { return m_canSave;}
            set { m_canSave = value; UpdateSaveButton (); }
        }

        public DailyOverviewEditShiftView ()
            : base (IsPhone ? "DailyOverviewEditShiftView_iPhone" : "DailyOverviewEditShiftView_iPad", null)
        {
            m_localiser = Mvx.Resolve<IStringService> ();

            CenterInView = true;
            DismissOnClickOutside = false;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            AddTitle ();

            foreach (var button in ButtonsToFormat)
            {
                button.Layer.CornerRadius = 4;
                button.Layer.BorderWidth = 1;
            }

            PunchTable.RegisterCell<EditShiftPunchCell> ();
            PunchTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            PunchTable.AllowsSelection = false;

            // View Model Binding
            var set = this.CreateBindingSet<DailyOverviewEditShiftView, DailyOverviewEditShiftViewModel>();
            set.Bind (Name).To (vm => vm.EmployeeName);
            set.Bind (Date).To (vm => vm.Date).WithConversion (new NullableDateTimeDateToStringConverter (), "dd-MMM-yy");
            set.Bind (this).For(b => b.ButtonColor).To(m => m.Theme.PrimaryColor).WithConversion (new HexColorCoverter (), null);
            set.Bind (m_cancel).To ("OnCancel");
            set.Bind (m_save).To ("OnSave");
            set.Bind (EditName).For (b => b.Hidden).To (vm => vm.CreateShift).WithConversion(new InvertedBooleanConverter());
            set.Bind (EditName).To("OnPickEmployee");
            set.Bind (EditDate).For (b => b.Hidden).To (vm => vm.CreateShift).WithConversion(new InvertedBooleanConverter());
            set.Bind (EditDate).To ("OnPickDate");
            set.Bind (AddMeal).To ("OnAddMeal");
            set.Bind (AddBreak).To ("OnAddBreak");
            set.Bind (AddJob).To ("OnAddJob");
            set.Bind (this).For (b => b.AddJobEnabled).To (vm => vm.HasJob);
            set.Bind (Job).To (vm => vm.JobName);
            set.Bind (EditJob).To ("OnPickJob");
            set.Bind (EditJob).For (b => b.Hidden).To (vm => vm.CanEditJob).WithConversion(new InvertedBooleanConverter());
            set.Bind (MatchPunches).To ("OnMatchPunches");
            set.Bind (MatchPunches).For(b => b.Hidden).To (vm => vm.CanMatchAllPunches).WithConversion(new InvertedBooleanConverter());
            set.Bind(Loading).For( v => v.Hidden).To(vm => vm.Loading).WithConversion(new InvertedBooleanConverter());
            set.Bind(LoadingSpinner).For( v => v.Hidden).To(vm => vm.Loading).WithConversion(new InvertedBooleanConverter());
            set.Bind (this).For (v => v.LoadingMessageText).To (vm => vm.LoadingMessage);
            set.Bind (PunchTable).For (v => v.Items).To (vm => vm.Punches);
            set.Bind (PunchTable).For (v => v.CustomCommand1).To ("OnPickPunchJob");
            set.Bind (PunchTable).For (v => v.CustomCommand2).To ("OnPickPunchActual");
            set.Bind (PunchTable).For (v => v.CustomCommand3).To ("OnMatchPunch");
            set.Bind(this).For(v => v.AllowAddMeal).To(vm => vm.AllowPunchesForMeal);
            set.Bind(this).For(v => v.AllowAddBreak).To(vm => vm.AllowPunchesForBreak);
            set.Bind(this).For(v => v.AllowAddJob).To(vm => vm.AllowPunchesForJob);
            set.Bind (this).For (v => v.CanSave).To (vm => vm.CanEditShift);
            set.Bind (m_title).For (v => v.Text).To (vm => vm.TitleText);
            set.Apply();
        }

        private void AddTitle()
        {
            UIView titleView = new UIView(new RectangleF(0,0,300,44));

            m_title = new UILabel (new RectangleF (0, 0, 300, 44));
            m_title.Font = UIFont.FromName ("AvenirNext-DemiBold", 20);
            m_title.TextAlignment = UITextAlignment.Center;
            m_title.LineBreakMode = UILineBreakMode.TailTruncation;
            m_title.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            m_title.TextColor = UIColor.White;
            m_title.BackgroundColor = UIColor.Clear;
            titleView.AddSubview (m_title);

            m_cancel = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, null);
            m_save = new UIBarButtonItem ("Save", UIBarButtonItemStyle.Plain, null);

            NavigationItem.TitleView = titleView;
            NavigationItem.LeftBarButtonItem = m_cancel;
            NavigationItem.RightBarButtonItem = m_save;
        }

        private void LayoutButtons()
        {
            var activeButtons = AddButtons.Where(x => x.Hidden == false).ToList();

            if (activeButtons == null || activeButtons.Count() <= 0)
                return;

            float buttonWidth = activeButtons[0].Frame.Width;
            float startX = (View.Frame.Width * 0.5f) - ((float)(activeButtons.Count ()) * 0.5f * buttonWidth);
            if(activeButtons.Count() > 1)
            {
                startX -= (float)(activeButtons.Count () - 1) * c_buttonSpacing;
            }

            foreach (var button in activeButtons)
            {
                var frame = button.Frame;
                frame.X = startX;
                button.Frame = frame;

                startX += buttonWidth + c_buttonSpacing;
            }
        }

        private void UpdateSaveButton()
        {
            if (m_canSave)
                m_save.Enabled = true;
            else
                m_save.Enabled = false;
        }
    }
}
