﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using MonoTouch.CoreGraphics;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    [Register("DatePickerDayView")]
    public class DatePickerDayView : UIView
    {
        private const int _FONTSIZE = 14;

        public EventHandler OnSelected { get; set; }
        public DateTime Date { get; set; }

        private UILabel m_label;

        public DatePickerDayView (IntPtr handle) : base(handle)
        {
            Initialise ();
        }

        private void Initialise()
        {
            Layer.BorderColor = new CGColor (DatePickerGridCell._LIGHTGRAY, DatePickerGridCell._LIGHTGRAY, DatePickerGridCell._LIGHTGRAY);

            m_label = new UILabel ();
            m_label.Font = UIFont.FromName ("Avenir-Light", _FONTSIZE);
            m_label.TextAlignment = UITextAlignment.Left;
            m_label.LineBreakMode = UILineBreakMode.TailTruncation;
            m_label.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            m_label.TextColor = UIColor.Black;
            m_label.BackgroundColor = UIColor.Clear;
            m_label.TranslatesAutoresizingMaskIntoConstraints = false;
            AddSubview (m_label);

            NSLayoutConstraint topConstraint = NSLayoutConstraint.Create (m_label, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 5);
            NSLayoutConstraint rightConstraint = NSLayoutConstraint.Create (m_label, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, -5);
            NSLayoutConstraint widthConstraint = NSLayoutConstraint.Create (m_label, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 0, 46);
            AddConstraint (topConstraint);
            AddConstraint (rightConstraint);
            AddConstraint (widthConstraint);

            // gesture
            UITapGestureRecognizer gesture = new UITapGestureRecognizer (SetEventHandler);
            gesture.NumberOfTapsRequired = 1;
            AddGestureRecognizer (gesture);
        }

        public void SetEventHandler()
        {
            OnSelected.Invoke (this, null);
        }
            
        public void SetLabel(DateTime date, bool weekSelected, DateTime? daySelected)
        {
            Date = date;

            m_label.Font = UIFont.FromName ("Avenir-Light", _FONTSIZE);
            m_label.Text = (date.Day == 1) ? date.ToString(Mvx.Resolve<IStringService>().Get("dd MMM")) : date.ToString("dd");
            int rightPadding = (date.Day == 1) ? -5 : 5;

            // auto layout constraints
            if(Constraints != null && Constraints.Length > 0)
            {
                // right constraint
                Constraints[2].Constant = rightPadding;
            }

            if(Date.Date.Equals(DateTime.Now.Date))
            {
                m_label.Font = UIFont.FromName ("Avenir-Heavy", _FONTSIZE);
            }

            Layer.BackgroundColor = new CGColor (1, 1, 1, 1);
            if (Date.Month % 2 == 0)
            {
                Layer.BackgroundColor = new CGColor (0.85f, 0.45f);
            }
            else
            {
                Layer.BackgroundColor = new CGColor (0.0f,0.2f,0.2f, 0.25f);
            }

            if(!weekSelected)
            {
                if(daySelected.HasValue && daySelected.Equals(date))
                {
                    Layer.BackgroundColor = new CGColor(0.847f, 0.7490f, 0.847f, 0.5f);
                }
            }
        }

    }
}

