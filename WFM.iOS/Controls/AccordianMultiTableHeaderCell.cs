﻿using System;
using Consortium.Client.iOS;

namespace WFM.iOS
{
    public class AccordianMultiTableHeaderCell : TableCell
    {
        public bool IsExpanded { get; set; }

        public AccordianMultiTableHeaderCell (IntPtr handle) : base (handle)
        {
        }

        public virtual void OnExpanded()
        {
            IsExpanded = true;
        }

        public virtual void OnCollapsed()
        {
            IsExpanded = false;
        }
    }
}