﻿using System;
using MonoTouch.Foundation;
using System.Drawing;
using WFM.Core;

namespace WFM.iOS
{
    [Register("PayAllocationTable")]
    public class PayAllocationTable : AlternateBackgroundTable
    {
        private const int c_rowHeight = 30;
        private const int c_expandedRowHeight = 60;

        public PayAllocationTable (IntPtr handle) : base (handle)
        {
        }

        public PayAllocationTable (RectangleF frame) 
            : base(frame)
        {
        }
            
        public float EstimatedHeight()
        {
            if (Items == null)
                return 0;

            float c = 0;
            foreach (var item in Items)
            {
                PayAllocationSection section = item as PayAllocationSection;

                if (section != null)
                {
                    c += SectionHeaderHeight;
                    foreach (var subItem in section.Items)
                    {
                        PayAllocationItem payItem = subItem as PayAllocationItem;
                        if (payItem.DisplayTotal)
                            c += c_expandedRowHeight;
                        else
                            c += c_rowHeight;
                    }
                }
            }

            return c;
        }

        protected override float OnGetHeightForRow(NSIndexPath indexPath)
        {
            var item = GetRow (indexPath) as PayAllocationItem;

            if (item != null)
            {
                if (item.DisplayTotal)
                    return c_expandedRowHeight;
                else
                    return c_rowHeight;
            }

            return base.OnGetHeightForRow (indexPath);
        }
    }
}

