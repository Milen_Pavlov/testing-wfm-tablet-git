﻿using System;
using MonoTouch.UIKit;
using System.Windows.Input;
using MonoTouch.Foundation;
using System.Collections.Generic;
using WFM.Core;

namespace WFM.iOS
{
    public class TimeOffTypeSource : UITableViewSource
    {
        public ICommand Selected { get; set; }
        public Action<bool> Dismiss { get; set; }
        public JDATimeOffType[] TimeOffTypes { get; set; }
        public int SelectedRow { get; set; }

        public override int RowsInSection (UITableView tableview, int section)
        {
            return TimeOffTypes != null ? TimeOffTypes.Length : 0;
        }

        public override int NumberOfSections (UITableView tableView)
        {
            return 1;
        }

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell("TimeCell");

            if (cell == null)
            {
                cell = new UITableViewCell (UITableViewCellStyle.Default, "TimeCell");
                cell.TextLabel.Font = UIFont.FromName ("AvenirNext-Regular", 20f);
                cell.TextLabel.TextAlignment = UITextAlignment.Left;
                cell.SelectionStyle = UITableViewCellSelectionStyle.Default;
            }

            cell.TextLabel.Text = TimeOffTypes[indexPath.Row].TypeName;
                
            return cell;
        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            SelectedRow = indexPath.Row;

            if (Selected != null)
                Selected.Execute (indexPath.Row);

            Dismiss (true);
        }
    }
}

