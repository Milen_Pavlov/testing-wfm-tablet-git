﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.CrossCore;
using Consortium.Client.iOS;
using WFM.Core;
using Consortium.Client.Core;
using System.Linq;

namespace WFM.iOS
{
    public partial class ShiftSwapWeekControl : UIViewController
    {
        private readonly UIColor COLOUR_GREEN = new UIColor (0.22f, 0.71f, 0.29f, 1.0f);
        private readonly UIColor COLOUR_RED = new UIColor (0.93f, 0.15f, 0.15f, 1.0f);

        private IStringService m_localiser;
        private SettingsService m_settings;

        private JDAAwaitingSwapApproval m_details;
        public JDAAwaitingSwapApproval Details
        {
            get { return m_details; }
            set { m_details = value; BuildUI (); }
        }

        public ShiftSwapWeekControl ()
            : base ("ShiftSwapWeekControl", null)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            m_localiser = Mvx.Resolve<IStringService> ();
            m_settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (m_settings.AppTheme.PrimaryColor);
            UIColor textcol = ColorUtility.FromHexString (m_settings.AppTheme.SecondaryTextColor);

            //Setup themeing and the shiz
            foreach (UILabel date in DateLabels)
            {
                date.BackgroundColor = primary;
                date.TextColor = textcol;
            }

            foreach (UILabel date in DayLabels)
            {
                date.BackgroundColor = primary;
                date.TextColor = textcol;
            }

            NetHoursTitle.BackgroundColor = primary;
            NetHoursTitle.TextColor = textcol;

            //Setup Tables
            foreach (TableControl table in DayTables)
            {
                table.RegisterCell<SwapShiftWeekShiftCell> ();
                table.RowHeight = 55;
                table.SectionFooterHeight = 0;
                table.SectionHeaderHeight = 0;
                table.AllowsSelection = false;
                table.SeparatorInset = UIEdgeInsets.Zero;
            }
        }

        private void BuildUI()
        {
            if (Details == null)
                return;
            
            NameTitle.Text = string.Format ((Details.swapperShift) ? m_localiser.Get("swapshift_swapperweek_title") : m_localiser.Get("swapshift_swappeeweek_title"), Details.EmployeeName);

            //Setup day labels
            for (int i = 0; i < DayLabels.Length; i++)
            {
                DateTime current = Details.StartOfWeek.AddDays (i);
                DayLabels[i].Text = current.ToString ("ddd");
                DateLabels[i].Text = current.Day.ToString();
            }

            //Nethours
            double delta = Details.SwappeeNetWeeklyHoursDelta;

            NetHoursOriginal.Text = Details.NetWeeklyTotalHours.ToString("#0.00");
            NetHoursTotal.Text = (Details.NetWeeklyTotalHours + delta).ToString ("#0.00");
        
            if (delta >= 0)
            {
                NetHoursDifference.TextColor = COLOUR_GREEN;
                NetHoursDifference.Text = string.Format("+ {0}", delta.ToString ("#0.00"));
            }
            else
            {
                NetHoursDifference.TextColor = COLOUR_RED;
                NetHoursDifference.Text = string.Format("- {0}", Math.Abs(delta).ToString ("#0.00"));
            }

            //Set shift data
            var days = Details.PendingApprovalShifts.GroupBy(x => x.Date);
            foreach (var day in days)
            {
                int index = (int)(day.Key.Date - Details.StartOfWeek.Date).TotalDays;

                if (index >= 0 && index < DayTables.Length)
                {
                    var items = day.ToList ();
                    items.Sort ((x, y) => x.Date.CompareTo (y.Date));
                    DayTables[index].Items = items;
                    DayTables[index].ReloadData ();
                }
            }
        }
    }
}

