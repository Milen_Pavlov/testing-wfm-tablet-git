﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using System.Windows.Input;
using MonoTouch.Foundation;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public class DatePickerPopover : PopoverSessionCell
    {
        private UINavigationController m_nav;
        private UIDatePicker m_picker;

        public ICommand OnDateTimeSelected
        { 
            get;
            set;
        }

        private DateTime m_currentDate;
    
        public DatePickerPopover() : base(new UINavigationController(new UIViewController()))
        {
            m_nav =ContentViewController as UINavigationController;
            m_nav.NavigationBar.TintColor = UIColor.White;
            m_nav.NavigationBar.BarTintColor = Constants.TescoBlue;
            m_nav.NavigationBar.BarStyle = UIBarStyle.Black;
            m_nav.NavigationBar.Translucent = false;

            var root = m_nav.ViewControllers [0];
            root.NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Select", UIBarButtonItemStyle.Plain, OnSelect);
            root.NavigationItem.LeftBarButtonItem = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, OnCancel);

            m_picker = new UIDatePicker ();
            m_picker.Date = DateTime.Now.Date;
            
            m_picker.Mode = UIDatePickerMode.Date;
            m_picker.ValueChanged += OnDateChanged;
            m_picker.AutoresizingMask = UIViewAutoresizing.None;

            root.View.AddSubview (m_picker);

            SetPopoverContentSize (new SizeF (m_picker.Frame.Width, m_picker.Frame.Height + m_nav.NavigationBar.Frame.Height), false);

            Setup ("Select Date", DateTime.Now, UIDatePickerMode.Date);
        }

        private void OnDateChanged(object sender, EventArgs e)
        {
            //DatePicker returns NSDate which is in UTC, need to convert to local
            NSTimeZone sourceTimeZone = new NSTimeZone ("UTC");
            NSTimeZone destinationTimeZone = NSTimeZone.LocalTimeZone;

            int sourceGMTOffset = sourceTimeZone.SecondsFromGMT (m_picker.Date);
            int destinationGMTOffset = destinationTimeZone.SecondsFromGMT (m_picker.Date);
            int interval = destinationGMTOffset - sourceGMTOffset;

            var destinationDate = m_picker.Date.AddSeconds (interval);

            m_currentDate = new DateTime (2001, 1, 1, 0, 0, 0).AddSeconds (destinationDate.SecondsSinceReferenceDate);
        }

        public void OnSelect(object sender, EventArgs e)
        {
            Dismiss (true);

            if (OnDateTimeSelected != null && OnDateTimeSelected.CanExecute(m_currentDate))
            {
                OnDateTimeSelected.Execute (m_currentDate);
            }
        }

        public void OnCancel(object sender, EventArgs e)
        {
            Dismiss (true);
        }

        public void Setup(string title, DateTime date, UIDatePickerMode mode)
        {
            var root = m_nav.ViewControllers [0];
            root.Title = title;

            m_currentDate = date;

            m_picker.Date = date;
            m_picker.Mode = mode;
            
            if(m_picker.Mode ==  UIDatePickerMode.Time)
                m_picker.Locale = new NSLocale(Mvx.Resolve<ITimeFormatService>().TimePickerLocale);
            else
                m_picker.Locale = NSLocale.CurrentLocale;

        }
    }
}

