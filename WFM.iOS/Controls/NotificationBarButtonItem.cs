﻿using System;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using System.Drawing;

namespace WFM.iOS
{
    public class NotificationBarButtonItem : UIBarButtonItem
    {
        private NotificationBadge m_badge;
        public int NotificationCount
        {
            get { return 0; }
            set
            {
                string text = value.ToString();

                if( m_badge == null )
                    m_badge = CreateBadge( text );


                m_badge.AutoBadgeSizeWithString( text );
                m_badge.Hidden = (value <= 0);
            }
        }

        public NotificationBarButtonItem (UIImage image, UIBarButtonItemStyle style, EventHandler handler) : base(image, style, handler)
        {
            var button = new UIButton(UIButtonType.Custom);
            button.Frame = new RectangleF(0f, 0f, image.Size.Width, image.Size.Height);
            button.TouchUpInside += (object sender, EventArgs e) => {
                Target.PerformSelector(Action,null,0); 
            };
            button.ShowsTouchWhenHighlighted = false;
            button.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            button.SetImage(image, UIControlState.Normal);
            CustomView = button;
        }

        private NotificationBadge CreateBadge( string text )
        {
            float size =  CustomView.Frame.Width;

            m_badge = new NotificationBadge( text, new PointF( size / 2, -(size / 2) ) );
            CustomView.AddSubview( m_badge );

            return m_badge;
        }
    }
}

