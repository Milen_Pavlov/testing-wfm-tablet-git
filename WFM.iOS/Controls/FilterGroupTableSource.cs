﻿using System;
using MonoTouch.UIKit;
using System.Windows.Input;
using MonoTouch.Foundation;
using System.Collections.Generic;
using WFM.Core;

namespace WFM.iOS
{
    public class FilterGroupTableSource : UITableViewSource
    {
        public List<FilterGroup> Filters { get; set; }
        public Action<int> Selected { get; set; }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 40;
        }

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell("FilterGroupCell");

            if (cell == null)
            {
                cell = new UITableViewCell (UITableViewCellStyle.Default, "FilterGroupCell");
                cell.TextLabel.Font = UIFont.FromName ("AvenirNext-Regular", 14f);
                cell.TextLabel.TextAlignment = UITextAlignment.Left;
                cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
            }
                
            cell.TextLabel.Text = string.Format ("{0}", Filters[indexPath.Row].Name);
            return cell;
        }

        public override int RowsInSection (UITableView tableview, int section)
        {
            return Filters != null ? Filters.Count : 0;
        }

        public override int NumberOfSections (UITableView tableView)
        {
            return 1;
        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            if (Selected != null)
                Selected (indexPath.Row);
        }
    }
}

