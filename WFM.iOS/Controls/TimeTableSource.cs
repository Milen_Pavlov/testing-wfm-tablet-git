﻿using System;
using MonoTouch.UIKit;
using System.Windows.Input;
using MonoTouch.Foundation;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public class TimeTableSource : UITableViewSource
    {
        public ICommand Selected { get; set; }
        public Action<bool> Dismiss { get; set; }
        public int StartHour { get; set; }
        
        private readonly ITimeFormatService m_TimeFormatService;
        
        public TimeTableSource()
        {
            m_TimeFormatService = Mvx.Resolve<ITimeFormatService>();
        }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 40;
        }

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell("TimeCell");

            if (cell == null)
            {
                cell = new UITableViewCell (UITableViewCellStyle.Default, "TimeCell");
                cell.TextLabel.Font = UIFont.FromName ("AvenirNext-Regular", 14f);
                cell.TextLabel.TextAlignment = UITextAlignment.Left;
                cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
            }

            cell.Accessory = indexPath.Row == StartHour ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
            
            
            DateTime hour = new DateTime();
            hour = hour.AddHours(indexPath.Row);
            
            cell.TextLabel.Text = string.Format ("  {0}", m_TimeFormatService.FormatTime(hour));
       
            return cell;
        }

        public override int RowsInSection (UITableView tableview, int section)
        {
            return 24;
        }

        public override int NumberOfSections (UITableView tableView)
        {
            return 1;
        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            if (Selected != null)
                Selected.Execute (indexPath.Row);

            StartHour = indexPath.Row;
            Dismiss (true);
        }
    }
}

