﻿using System;
using MonoTouch.Foundation;
using System.Windows.Input;
using System.Drawing;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using System.Collections.Generic;

namespace WFM.iOS
{
    /// <summary>
    /// Sections are used for grouping items, however they aren't used for UI.
    /// Sections can't be selected, and we need to be able to select a section to expand and collapse.
    /// Sections are therefore set to 0 height, and the first item in each section is a copy of the section cell.
    /// 
    /// This is identical to AccordianTableControl however it allows multiple cell types for different item models
    /// </summary>
    [Register("AccordianMultiTableControl")]
    public class AccordianMultiTableControl : TableControl
    {
        NSMutableIndexSet expandedSections;
        Dictionary<Type,NSString> m_multiCellIdentifiers; 

        public ICommand SectionExpanded 
        { 
            get; 
            set;
        }

        public override float SectionHeaderHeight
        {
            get
            {
                return 0;
            }
        }

        public override float SectionFooterHeight
        {
            get
            {
                return 0;
            }
        }

        public AccordianMultiTableControl (IntPtr handle) : base (handle)
        {
            expandedSections = new NSMutableIndexSet();
            m_multiCellIdentifiers = new Dictionary<Type, NSString> ();
        }

        public AccordianMultiTableControl (RectangleF frame) 
            : base(frame)
        {
            expandedSections = new NSMutableIndexSet();
            m_multiCellIdentifiers = new Dictionary<Type, NSString> ();
        }

        public new void RegisterSection<T>() where T : AccordianMultiTableHeaderCell
        {
            try
            {
                var key = typeof(T).GetField("Key").GetValue(null) as NSString;
                var nib = typeof(T).GetField("Nib").GetValue(null) as UINib;
                RegisterNibForCellReuse(nib, key);
                m_sectionIdentifier = key;
            }
            catch
            {
                Mvx.Trace("Section registration failed, ensure the section cell has public fields Key (NSString) & Nib (UINib)");
            }
        }

        public void RegisterCell<T,U>() 
            where T : TableCell 
            where U : new()
        {
            var key = typeof(T).GetField("Key").GetValue(null) as NSString;
            var nib = typeof(T).GetField("Nib").GetValue(null) as UINib;
            RegisterNibForCellReuse(nib, key);

            if (!m_multiCellIdentifiers.ContainsKey (typeof (U)))
            {
                m_multiCellIdentifiers.Add (typeof (U), key);
            }
            else
                throw new Exception (string.Format ("Cell already registered for type {0}", typeof (U)));
        }

        public override int NumberOfRowsInSection(int sectionIndex)
        {
            if(m_items.Count == 0)
                return 0;

            if(expandedSections.Contains((uint)sectionIndex))
            {
                var sectioncollection = m_items[(int)sectionIndex] as ISection;
                return sectioncollection.Items.Count + 1;
            }
            else
            {
                return 1;
            }
        }

        protected override int OnGetCount(int sectionIndex)
        {
            return NumberOfRowsInSection (sectionIndex);
        }

        protected override UITableViewCell OnGetCell(NSIndexPath indexPath)
        {
            if (m_sectionIdentifier == null)
                throw new Exception("No header type was set");

            //Check for header status
            if(indexPath.Row == 0)
            {
                var cell = (TableCell)DequeueReusableCell(m_sectionIdentifier);

                if (cell == null)
                    throw new Exception("No cell of valid type found for identifier: " + m_sectionIdentifier);

                if (indexPath.Section < m_items.Count)
                {
                    cell.DataContext = m_items[indexPath.Section];
                    cell.BindOnce();
                }

                AccordianMultiTableHeaderCell headerCell = cell as AccordianMultiTableHeaderCell;
                if(headerCell != null)
                    headerCell.IsExpanded = expandedSections.Contains((uint)indexPath.Section);

                cell.OnShow();

                return cell;
            }
            else
            {
                //Get Item type and see if we have a custom cell registered for that type, otherwise fall back to the default
                NSString identifier = m_cellIdentifier;
                if (indexPath.Section < m_items.Count)
                {
                    var section = m_items[indexPath.Section] as ISection;
                    if (section != null && section.Items != null && indexPath.Row <= section.Items.Count)
                    {
                        var item = section.Items[indexPath.Row - 1];

                        if(m_multiCellIdentifiers.ContainsKey(item.GetType()))
                            identifier = m_multiCellIdentifiers[item.GetType()];
                    }
                }

                if (identifier == null)
                    throw new Exception("No cell type was set");

                var cell = (TableCell)DequeueReusableCell(identifier, indexPath);
                if (cell == null)
                    throw new Exception("No cell of valid type found for identifier: " + identifier);

                if (indexPath.Section < m_items.Count)
                {
                    var section = m_items[indexPath.Section] as ISection;

                    if (section != null && section.Items != null && indexPath.Row <= section.Items.Count)
                    {
                        //The negative offset allows us to bind the header as an actual row in the list. 
                        cell.DataContext = section.Items[indexPath.Row-1];
                        cell.CustomCommand1 = CustomCommand1;
                        cell.CustomCommand2 = CustomCommand2;
                        cell.CustomCommand3 = CustomCommand3;
                        cell.BindOnce();
                    }
                }

                cell.OnShow();

                var alernateBGCell = cell as AlternateBackgroundCell;
                if (alernateBGCell != null)
                {
                    if (indexPath.Row % 2 != 0)
                        alernateBGCell.IsOddNumberedRow = true;
                    else
                        alernateBGCell.IsOddNumberedRow = false;
                }


                return cell;
            }
        }

        protected override void OnSelect(NSIndexPath indexPath)
        {
            if(indexPath.Row > 0)
            {
                if (Selected == null)
                    return;

                if (indexPath.Section < m_items.Count)
                {
                    var section = m_items[indexPath.Section] as ISection;

                    if (section != null && section.Items != null && indexPath.Row <= section.Items.Count)
                        Selected.Execute(section.Items[indexPath.Row-1]);
                    else if (Selected.CanExecute (indexPath.Row))
                    {
                        Selected.Execute (indexPath.Row);
                    }
                }
            }
            else
            {
                BeginUpdates();
                // only first row toggles exapand/collapse
                DeselectRow(indexPath, true);

                int section = indexPath.Section;
                bool currentlyExpanded = expandedSections.Contains((uint)indexPath.Section);
                int rows;

                List<NSIndexPath> temp = new List<NSIndexPath>();

                if(currentlyExpanded)
                {
                    rows = NumberOfRowsInSection(section);
                    expandedSections.Remove((uint)section);
                }
                else
                {
                    expandedSections.Add((uint)section);
                    rows = NumberOfRowsInSection(section);
                }

                for(int i = 1; i < rows; i++)
                {
                    NSIndexPath path = NSIndexPath.FromRowSection(i, section);
                    temp.Add(path);
                }

                //UITableViewCell cell = CellAt(indexPath);

                if(currentlyExpanded)
                {
                    DeleteRows(temp.ToArray(), UITableViewRowAnimation.Top);
                }
                else
                {
                    InsertRows(temp.ToArray(), UITableViewRowAnimation.Top);
                }

                EndUpdates();

                if(SectionExpanded != null)
                {
                    if(indexPath.Section < m_items.Count)
                    {
                        var sectionObject = m_items[indexPath.Section] as ISection;

                        if(sectionObject != null && sectionObject.Items != null && indexPath.Row < sectionObject.Items.Count)
                            SectionExpanded.Execute(sectionObject);
                    }
                }

                AccordianMultiTableHeaderCell headerCell = CellAt(indexPath) as AccordianMultiTableHeaderCell;
                if(headerCell != null)
                {
                    if(currentlyExpanded)
                    {
                        headerCell.OnCollapsed();
                    }
                    else
                    {
                        headerCell.OnExpanded();
                    }
                }
                else
                    throw new Exception("Header cell does not inherit from AccordianTableHeaderCell");
            }
        }
    }
}

