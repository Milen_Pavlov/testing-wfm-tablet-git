﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using System.Drawing;

namespace WFM.iOS
{
    [Register("AlternateBackgroundTable")]
    public class AlternateBackgroundTable : TableControl
    {
        public AlternateBackgroundTable (IntPtr handle) : base (handle)
        {
        }

        public AlternateBackgroundTable (RectangleF frame) 
            : base(frame)
        {
        }

        protected override UITableViewCell OnGetCell(NSIndexPath indexPath)
        {
            var tableCell = base.OnGetCell(indexPath);

            var cell = tableCell as AlternateBackgroundCell;

            if (cell != null)
            {
                if (indexPath.Row % 2 != 0)
                    cell.IsOddNumberedRow = true;
                else
                    cell.IsOddNumberedRow = false;
            }

            return tableCell;
        }
    }
}

