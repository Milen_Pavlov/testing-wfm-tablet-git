﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using System.Windows.Input;
using System.Collections.Generic;
using WFM.Core;
using MonoTouch.Foundation;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public class EmployeeUpcomingRequestsPopover : PopoverSessionCell
    {
        const float c_width = 400;
        const float c_height = 400;

        private UINavigationController m_nav;
        private UITableView m_table;
        private UILabel m_emptyLabel;
        private UIActivityIndicatorView m_loadingSpinner;
        private EmployeeUpcomingRequestTableSource m_tableSource;
        private string m_currentUser;
        private bool m_canCreateTimeOff;
        private bool m_canApproveOwnTimeOff;

        public ICommand Selected 
        { 
            get { return m_tableSource.Selected; }
            set { m_tableSource.Selected = value; }
        }

        public ICommand OnCreateSelected
        { 
            get;
            set;
        }

        private JDAEmployeeInfo m_EmployeeInfo;
        public JDAEmployeeInfo EmployeeInfo
        {
            get
            {
                return m_EmployeeInfo;
            }
            set
            {
                m_EmployeeInfo = value;

                if (m_EmployeeInfo != null)
                {
                    var root = m_nav.ViewControllers [0];
                    if (m_canCreateTimeOff && (!m_EmployeeInfo.LoginName.Equals(m_currentUser, StringComparison.InvariantCultureIgnoreCase) || m_canApproveOwnTimeOff))
                        root.NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Create", UIBarButtonItemStyle.Plain, OnCreate);
                    else
                        root.NavigationItem.RightBarButtonItem = null;
                }
            }
        }

        public List<JDATimeOffRequest> Items
        {
            get { return m_tableSource.Notifications; }
            set 
            { 
                m_tableSource.Notifications = value; 
                m_table.ReloadData ();

                bool hasData = value != null && value.Count > 0;
                m_table.Hidden = !hasData;
                m_emptyLabel.Hidden = hasData;
            }
        }

        private bool m_loading = false;
        public bool Loading
        {
            get
            {
                return m_loading;
            }
            set
            {
                m_loading = value;
                m_loadingSpinner.Hidden = !m_loading;

                if (m_loading)
                {
                    m_table.Alpha = 0;
                    m_emptyLabel.Hidden = true;
                    m_loadingSpinner.StartAnimating ();
                }
                else
                {
                    UIView.Animate (0.2, 0, UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.BeginFromCurrentState, () =>
                        {
                            if(m_table!=null)
                                m_table.Alpha = 1;
                        }, () =>
                        {
                        });
                }
            }
        }

        private bool m_allHistory = false;
        public bool ShowAllHistory
        {
            get { return m_allHistory; }
            set
            {
                m_allHistory = value;

                if (m_nav != null)
                {
                    m_nav.ViewControllers[0].Title = (value) ? "Recent Requests" : "Upcoming Requests";
                }
            }
        }

        public EmployeeUpcomingRequestsPopover (): base(new UINavigationController(new UIViewController()))
        {
            IESSConfigurationService essConfig = Mvx.Resolve<IESSConfigurationService>();
            m_canCreateTimeOff = essConfig.CanCreateTimeOff;
            m_canApproveOwnTimeOff = essConfig.CanApproveOwnTimeOff;

            SessionData jdaSession = Mvx.Resolve<SessionData> ();

            m_currentUser = jdaSession.CurrentUsername;

            m_nav = ContentViewController as UINavigationController;
            m_nav.NavigationBar.TintColor = UIColor.White;
            m_nav.NavigationBar.BarTintColor = Constants.TescoBlue;
            m_nav.NavigationBar.BarStyle = UIBarStyle.Black;
            m_nav.NavigationBar.Translucent = false;

            var root = m_nav.ViewControllers [0];
            root.Title = (ShowAllHistory) ? "Upcoming Requests" : "Upcoming Requests";
            root.PreferredContentSize = new SizeF (c_width, c_height);
            root.View.BackgroundColor = UIColor.White;

            m_table = new UITableView (new RectangleF (0, 0, c_width, c_height));
            m_table.SeparatorInset = UIEdgeInsets.Zero;
            m_table.Source = m_tableSource = new EmployeeUpcomingRequestTableSource ();
            m_table.RegisterNibForCellReuse (TimeOffHistoryCell.Nib, TimeOffHistoryCell.Key);
            m_table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            m_tableSource.Dismiss += Dismiss;
            root.View.AddSubview (m_table);

            m_emptyLabel = new UILabel(new RectangleF (0, 0, c_width, c_height));
            m_emptyLabel.Lines = 0;
            m_emptyLabel.TextAlignment = UITextAlignment.Center;
            m_emptyLabel.Font = UIFont.FromName ("AvenirNext-Regular", 14f);
            m_emptyLabel.Text = "No Requests";
            root.View.AddSubview (m_emptyLabel);

            m_loadingSpinner = new UIActivityIndicatorView (new RectangleF (0, 0, c_width, c_height));
            m_loadingSpinner.HidesWhenStopped = true;
            m_loadingSpinner.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
            root.View.AddSubview (m_loadingSpinner);

            SetPopoverContentSize (new SizeF (c_width, c_height), false);
        }

        public void OnCreate(object sender, EventArgs e)
        {
            Dismiss (true);

            if (OnCreateSelected.CanExecute(EmployeeInfo))
            {
                OnCreateSelected.Execute (EmployeeInfo);
            }
        }

        public override void PresentFromRect (RectangleF rect, UIView view, UIPopoverArrowDirection arrowDirections, bool animated)
        {
            var root = m_nav.ViewControllers [0];
            root.NavigationItem.RightBarButtonItem = null;

            base.PresentFromRect (rect, view, arrowDirections, animated);
        }

        [Export("EmployeeUpcomingRequestsPopoverVisibility:")]
        void UpdateVisibility(NSObject parameter)
        {
            bool hasData = Items != null && Items.Count > 0;

            if(m_table!=null)
                m_table.Hidden = !hasData;

            if(m_emptyLabel!=null)
                m_emptyLabel.Hidden = hasData;
        }
    }
}

