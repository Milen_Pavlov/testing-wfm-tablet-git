﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public class ResizableRowTableControlDelegate : UITableViewDelegate
    {
        private readonly Func<NSIndexPath, float> m_onGetHeightForRow;

        public ResizableRowTableControlDelegate (Func<NSIndexPath, float> onGetHeightForRow = null)
        {
            m_onGetHeightForRow = onGetHeightForRow;
        }

        public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
        {
            if(m_onGetHeightForRow != null)
            {
                return m_onGetHeightForRow.Invoke(indexPath);
            }

            return base.GetHeightForRow (tableView, indexPath);
        }
    }

}

