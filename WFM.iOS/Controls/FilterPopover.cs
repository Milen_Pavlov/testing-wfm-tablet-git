﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public class FilterPopover : PopoverSessionCell
    {
        const float c_width = 320;
        const float c_height = 440;

        private UINavigationController m_nav;
        private UITableView m_groupTable;
        private UITableView m_itemTable;
        private UIView m_itemView;
        private UIViewController m_itemController;
        private FilterGroupTableSource m_groupSource;
        private FilterItemTableSource m_itemSource;
        private FilterGroup m_currentFilter;

        public ICommand Apply { get; set; }

        public FilterGroup CurrentFilter 
        { 
            get { return m_currentFilter; } 
            set
            {
                m_currentFilter = value;
            }
        }

        public List<FilterGroup> Filters 
        {
            get { return m_groupSource.Filters; }
            set 
            {
                m_groupSource.Filters = value;
                m_groupTable.ReloadData ();
                if(m_itemController.NavigationItem.LeftBarButtonItem != null)
                    m_nav.PopViewControllerAnimated(false);
            }
        }

        public FilterPopover() 
            : base(new UINavigationController(new UIViewController()))
        {
            m_nav = this.ContentViewController as UINavigationController;
            m_nav.NavigationBar.TintColor = UIColor.White;
            m_nav.NavigationBar.BarTintColor = Constants.TescoBlue;
            m_nav.NavigationBar.BarStyle = UIBarStyle.Black;
            m_nav.NavigationBar.Translucent = false;

            var root = m_nav.ViewControllers [0];
            root.Title = "Filter Group";
            root.PreferredContentSize = new SizeF (c_width, c_height);

            root.NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Clear", UIBarButtonItemStyle.Plain, (v1,v2) => { 
                if (Apply != null)
                    Apply.Execute(null);

                Dismiss(true);
            });

            m_groupSource = new FilterGroupTableSource ();
            m_groupSource.Selected += OnSelect;
            m_itemSource = new FilterItemTableSource ();

            m_groupTable = new UITableView (new RectangleF (0, 0, c_width, c_height));
            m_groupTable.SeparatorInset = UIEdgeInsets.Zero;
            m_groupTable.Source = m_groupSource;
            root.View = m_groupTable;

            var all = new UIBarButtonItem ("Select All", UIBarButtonItemStyle.Plain, (v1,v2) => { 
                foreach(var item in m_itemSource.Filter.Items)
                    item.Selected = true;

                m_itemTable.ReloadData ();
            });

            var none = new UIBarButtonItem ("Select None", UIBarButtonItemStyle.Plain, (v1,v2) => { 
                foreach(var item in m_itemSource.Filter.Items)
                    item.Selected = false;

                m_itemTable.ReloadData ();
            });

            var flexible = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);

            m_itemView = new UIView(new RectangleF (0, 44, c_width, c_height+44));
            m_itemTable = new UITableView (new RectangleF (0, 0, c_width, c_height-44));
            m_itemTable.SeparatorInset = UIEdgeInsets.Zero;
            m_itemTable.Source = m_itemSource;
            var toolbar = new UIToolbar(new RectangleF (0, c_height-44, c_width, 44));
            toolbar.Items = new UIBarButtonItem[] { flexible, all, flexible, none, flexible };
            m_itemView.AddSubview (m_itemTable);
            m_itemView.AddSubview (toolbar);

            m_itemController = new UIViewController ();
            m_itemController.View = m_itemView;
            m_itemController.PreferredContentSize = new SizeF (c_width, c_height);
        }

        public override void PresentFromRect(RectangleF rect, UIView view, UIPopoverArrowDirection arrowDirections, bool animated)
        {
            SetCurrentSelection (m_currentFilter, Filters);
            base.PresentFromRect (rect, view, arrowDirections, animated);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose (disposing);
            m_groupSource.Selected -= OnSelect;

            m_nav = null;
            m_groupTable = null;
            m_itemTable = null;
            m_itemView = null;
            m_itemController = null;
            m_groupSource = null;
            m_itemSource = null;
            m_currentFilter = null;
        }

        private void SetCurrentSelection(FilterGroup filter, List<FilterGroup> list)
        {
            if (list != null)
            {
                foreach (var g in list)
                {
                    if (filter != null && g.Type == filter.Type)
                    {
                        // Make a copy
                        g.Items = filter.Clone ().Items;
                    }
                    else
                    {
                        foreach (var i in g.Items)
                            i.Selected = false;
                    }
                }

                m_itemTable.ReloadData ();
            }
        }

        private void OnSelect(int index)
        {
            NSIndexPath indexPath = m_groupTable.IndexPathForSelectedRow;
            if (indexPath != null)
                m_groupTable.DeselectRow (indexPath, false);

            m_itemSource.Filter = m_groupSource.Filters[index];
            m_itemTable.ReloadData ();

            m_itemController.Title = m_groupSource.Filters [index].Name;
            m_itemController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (UIImage.FromBundle ("back"), UIBarButtonItemStyle.Plain, (v1,v2) => { 
                m_nav.PopViewControllerAnimated(true);
            });

            var apply = new UIBarButtonItem ("Apply", UIBarButtonItemStyle.Plain, (v1,v2) => { 
                if (Apply != null) Apply.Execute(m_itemSource.Filter);
                Dismiss(true);
            });

            m_itemController.NavigationItem.RightBarButtonItem = apply;
            m_nav.PushViewController (m_itemController, true);
        }
    }
}
