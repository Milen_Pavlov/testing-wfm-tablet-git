﻿using System;
using System.Drawing;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public enum ScrollState
    {
        Stopped,
        Up,
        Down,
    }

    [Register("DragTableControl")]
    public class DragTableControl : TableControl
    {
        private ScrollState m_scrolling;
        private const int c_scrollSpeed = 100;

        public DragTableControl (RectangleF frame)
            : base (frame)
        {
            SetupDelegate();
        }

        public DragTableControl (IntPtr handle)
            : base (handle)
        {
            SetupDelegate();
        }

        public void StartScrolling (ScrollState state)
        {
            if (m_scrolling != ScrollState.Stopped)
                return;
            
            m_scrolling = state;

            ContinueScroll ();
        }

        public void ContinueScroll ()
        {
            if (m_scrolling == ScrollState.Stopped)
                return;
            
            PointF offset = ContentOffset;
            switch (m_scrolling) 
            {
            case ScrollState.Up:
                offset.Y -= c_scrollSpeed;
                if (offset.Y < 0.0f)
                    offset.Y = 0.0f;
                break;
            case ScrollState.Down:
                offset.Y += c_scrollSpeed;
                if (offset.Y > this.ContentSize.Height - Frame.Height)
                    offset.Y = this.ContentSize.Height - Frame.Height;
                break;
            default:
                break;
            }

            SetContentOffset (offset, true);
        }

        public void StopScrolling ()
        {
            if (m_scrolling == ScrollState.Stopped)
                return;

            m_scrolling = ScrollState.Stopped;
        }

        public override UITableViewCell DequeueReusableCell (string identifier)
        {
            var result = base.DequeueReusableCell (identifier);
            var dragCell = result as DraggableDestinationCell;
            if (dragCell != null && dragCell.IsDragging)
                return DequeueReusableCell (identifier);
            return result;
        }

        public override UITableViewCell DequeueReusableCell (NSString reuseIdentifier, NSIndexPath indexPath)
        {
            return base.DequeueReusableCell (reuseIdentifier, indexPath);
        }

        private void SetupDelegate ()
        {
            Delegate = new DragTableControlDelegate();
        }
    }

    internal class DragTableControlDelegate : ResizableRowTableControlDelegate
    {
        public DragTableControlDelegate (Func<NSIndexPath, float> onGetHeightForRow = null)
            : base(onGetHeightForRow)
        {
        }
        
        public override void ScrollAnimationEnded (UIScrollView scrollView)
        {
            DragTableControl view = scrollView as DragTableControl;
            if (view == null)
                return;

            view.ContinueScroll ();
        }
    }
}

