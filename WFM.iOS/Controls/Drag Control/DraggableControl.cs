using System;
using System.Drawing;
using Cirrious.CrossCore;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    abstract public class DraggableControl : UIView, IDragSource
    {
        private readonly DragService m_dragService;
        protected string InitialDragData { get; set; }

        public DraggableControl ()
        {
            m_dragService = Mvx.Resolve<DragService> ();
        }

        abstract public void CreateDragData ();

        public void DragStart ()
        {
            CreateDragData ();

            if (m_dragService != null)
                m_dragService.OnStartDrag (this, InitialDragData);
        }

        public void DragUpdate (PointF offset )
        {
            if (m_dragService != null)
                m_dragService.OnUpdateDrag (offset);
        }

        public void DragEnd ()
        {
            if (m_dragService != null)
                m_dragService.OnEndDrag ();
            
            Hidden = false;
        }
    }
}

