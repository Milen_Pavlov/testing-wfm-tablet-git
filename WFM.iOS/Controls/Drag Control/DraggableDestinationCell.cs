﻿using System;
using System.Drawing;
using Cirrious.CrossCore;
using Consortium.Client.iOS;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public abstract class DraggableDestinationCell : TableCell
    {
        public abstract void DragStarted ();
        public abstract void DragEnded ();
        public abstract void DragMoved (PointF movementPos);
        public abstract void DragEntered ();
        public abstract void DragExited ();
        public abstract void DragDropped ();
        public bool IsDragging { get; set; }

        protected readonly DragService m_dragService;

        public DraggableDestinationCell (IntPtr handle) : base (handle)
        {
            m_dragService = Mvx.Resolve<DragService> ();
            IsDragging = false;
        }

        protected override void Dispose (bool disposing)
        {
            base.Dispose (disposing);

            if (m_dragService != null)  
                m_dragService.UnregisterDestination (this);
        }

        public override void OnShow ()
        {
            base.OnShow ();

            if (m_dragService != null)
                m_dragService.RegisterDestination (this);
        }

        public override void OnHide ()
        {
            base.OnHide ();

            if (m_dragService != null)  
                m_dragService.UnregisterDestination (this);
        }
    }
}

