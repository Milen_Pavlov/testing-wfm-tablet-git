﻿using System;
namespace WFM.iOS
{
    public class ShiftDragData
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int ShiftID { get; set; }
        public int EmployeeID { get; set; }
        public bool IsManualShift { get; set; }

        public ShiftDragData ()
        {
        }
    }
}

