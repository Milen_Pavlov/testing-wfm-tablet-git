﻿using System;
using MonoTouch.UIKit;
using System.Windows.Input;
using MonoTouch.Foundation;
using System.Collections.Generic;
using WFM.Core;

namespace WFM.iOS
{
    public class FilterItemTableSource : UITableViewSource
    {
        public FilterGroup Filter { get; set; }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 40;
        }

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell("FilterItemCell");

            if (cell == null)
            {
                cell = new UITableViewCell (UITableViewCellStyle.Default, "FilterItemCell");
                cell.TextLabel.Font = UIFont.FromName ("AvenirNext-Regular", 14f);
                cell.TextLabel.TextAlignment = UITextAlignment.Left;
                cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
            }

            cell.Accessory = Filter.Items[indexPath.Row].Selected ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
            cell.TextLabel.Text = string.Format ("{0}", Filter.Items[indexPath.Row].Name);
            return cell;
        }

        public override int RowsInSection (UITableView tableview, int section)
        {
            return (Filter != null && Filter.Items != null) ? Filter.Items.Count : 0;
        }

        public override int NumberOfSections (UITableView tableView)
        {
            return 1;
        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            Filter.Items [indexPath.Row].Selected = !Filter.Items [indexPath.Row].Selected;
            tableView.ReloadRows (new NSIndexPath[] { indexPath }, UITableViewRowAnimation.None);
        }
    }
}

