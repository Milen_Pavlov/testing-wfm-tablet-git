﻿using System;
using MonoTouch.Foundation;
using System.Drawing;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    [Register("DailyOverviewTable")]
    public class DailyOverviewTable : AlternateBackgroundTable
    {
        private DynamicCellHeightService m_cellHeightService;

        //The base height of an item cell, probably 44 but were loading the cell in anyway so check
        public float m_baseRowHeight;

        //The Y positions of jobs and exceptions table, this will be combined with their height and bottom "border" so we get a true total height
        public float m_jobsTableY;
        public float m_exceptionsTableY;

        public float m_bottomBorder;

        public DailyOverviewTable (IntPtr handle) : base (handle)
        {
            Setup();
        }

        public DailyOverviewTable (RectangleF frame) 
            : base(frame)
        {
            Setup();
        }

        protected void Setup()
        {
            m_cellHeightService = Mvx.Resolve<DynamicCellHeightService>();

            //Load in a cell to measure
            DailyOverviewItemCell cellToUse = null;

            object[] views = DailyOverviewItemCell.Nib.Instantiate (null, null);
            if (views == null || views.Length == 0 || !(views[0] is DailyOverviewItemCell))
                throw new Exception ("DailyOverviewItemCell not found for dynamic cell height");

            cellToUse = views[0] as DailyOverviewItemCell;

            m_baseRowHeight = cellToUse.Frame.Height;
            m_jobsTableY = cellToUse.Jobs.Frame.Y - DailyOverviewItemJobCell.Border;
            m_exceptionsTableY = cellToUse.Exceptions.Frame.Y - DailyOverviewItemExceptionCell.Border;
            m_bottomBorder = m_jobsTableY;//TODO Determine whats good
        }
            
        protected override float OnGetHeightForRow(NSIndexPath indexPath)
        {
            var item = GetRow (indexPath) as SiteTimecardEntry;

            if (item != null)
            {
                //Get total jobs height
                float jobs = 0;
                if(item.Jobs != null)
                {
                    foreach(var job in item.Jobs)
                        jobs += m_cellHeightService.GetHeightForData(job, DailyOverviewItemJobCell.Nib, job.JobName);

                    //Add borders
                    jobs += m_jobsTableY + m_bottomBorder;
                }

                //Get total exceptions height
                float exceptions = 0;
                if(item.FilteredExceptions != null)
                {
                    foreach(var exc in item.FilteredExceptions)
                        exceptions += m_cellHeightService.GetHeightForData(exc, DailyOverviewItemExceptionCell.Nib, exc.Text);

                    //Add borders
                    exceptions += m_exceptionsTableY + m_bottomBorder;
                }

                //Return max of these heights and base
                float maxDynamic = Math.Max(jobs, exceptions);

                return Math.Max(m_baseRowHeight, maxDynamic);
            }

            return base.OnGetHeightForRow (indexPath);
        }
    }
}

