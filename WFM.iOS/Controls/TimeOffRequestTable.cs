﻿using System;
using MonoTouch.Foundation;
using Consortium.Client.iOS;
using System.Drawing;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    [Register("TimeOffRequestTable")]
    public class TimeOffRequestTable : TableControl
    {
        public TimeOffRequestTable (IntPtr handle) : base (handle)
        {
        }

        public TimeOffRequestTable (RectangleF frame) 
            : base(frame)
        {
        }

        protected override UITableViewCell OnGetCell(NSIndexPath indexPath)
        {
            var tableCell = base.OnGetCell(indexPath);

            var cell = tableCell as TimeOffRequestCell;

            if (tableCell != null && indexPath.Row % 2 != 0)
            {
                cell.IsOddNumberedRow = true;
            }

            return tableCell;
        }
    }
}

