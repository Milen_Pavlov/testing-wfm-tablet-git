﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace WFM.iOS
{
    [Register("DatePickerWeekView")]
    public class DatePickerWeekView : UIView
    {
        public EventHandler OnSelected
        {
            get;
            set;
        }

        private UILabel m_label;

        public DatePickerWeekView (IntPtr handle) : base(handle)
        {
            Initialise ();
        }

        public DateTime Week
        {
            get;
            set;
        }

        private void Initialise()
        {
            m_label = new UILabel ();
            m_label.Font = UIFont.FromName ("Avenir-Light", 14);
            m_label.TextAlignment = UITextAlignment.Left;
            m_label.LineBreakMode = UILineBreakMode.TailTruncation;
            m_label.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            m_label.TextColor = UIColor.Black;
            m_label.BackgroundColor = UIColor.Clear;
            m_label.TranslatesAutoresizingMaskIntoConstraints = false;
            AddSubview (m_label);

            Layer.BackgroundColor = new CGColor (1,1,1,1);
            Layer.BorderColor = new CGColor (DatePickerGridCell._LIGHTGRAY, DatePickerGridCell._LIGHTGRAY, DatePickerGridCell._LIGHTGRAY);

            // auto layout constraints
            NSLayoutConstraint centreConstraint = NSLayoutConstraint.Create (m_label, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0);
            NSLayoutConstraint rightConstraint = NSLayoutConstraint.Create (m_label, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 5);
            NSLayoutConstraint widthConstraint = NSLayoutConstraint.Create (m_label, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 0, 125);
            AddConstraint (centreConstraint);
            AddConstraint (rightConstraint);
            AddConstraint (widthConstraint);

            // gesture
            UITapGestureRecognizer gesture = new UITapGestureRecognizer (SetEventHandler);
            gesture.NumberOfTapsRequired = 1;
            AddGestureRecognizer (gesture);
        }

        public void SetEventHandler()
        {
            OnSelected.Invoke (this, null);
        }

        public void SetLabel(string text)
        {
            m_label.Text = text;
        }
    }
}

