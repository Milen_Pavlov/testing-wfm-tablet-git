﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace WFM.iOS
{
    [Register("CheckBox")]
    public class CheckBox : UIView
    {
        private UIImageView m_onImageView;
        private UIImageView m_offImageView;
        private UILabel m_label;
        private UIButton m_button;

        public Action<CheckBox,bool> OnToggle { get; set; }

        private UIImage m_onImage;
        public UIImage OnImage
        {
            get { return m_onImage; } 
            set 
            { 
                UIImage alignmentRectImage = (value!=null) ? value.ImageWithAlignmentRectInsets(new UIEdgeInsets(10,10,10,10)) : null; 
                m_onImage = (alignmentRectImage != null) ? alignmentRectImage.ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate) : null;
                m_onImageView.Image = m_onImage;
            }
        }

        private UIImage m_offImage;
        public UIImage OffImage
        {
            get { return m_offImage; } 
            set 
            { 
                UIImage alignmentRectImage = (value!=null) ? value.ImageWithAlignmentRectInsets(new UIEdgeInsets(10,10,10,10)) : null; 
                m_offImage = (alignmentRectImage != null) ? alignmentRectImage.ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate) : null;
                m_offImageView.Image = m_offImage;
            }
        }

        public override UIColor TintColor
        {
            get { return m_onImageView.TintColor; }
            set
            {
                m_onImageView.TintColor = value;
                m_offImageView.TintColor = value;
            }
        }

        private bool m_isToggled;
        public bool IsToggled
        {
            get { return m_isToggled; }
            set 
            { 
                m_isToggled = value; 

                UIView.Animate (0.15f, 0, UIViewAnimationOptions.BeginFromCurrentState | UIViewAnimationOptions.CurveEaseInOut, () =>
                    {
                        m_onImageView.Alpha = (!m_isToggled) ? 0 : 1;
                        m_offImageView.Alpha = (!m_alwaysShowOffImage && m_isToggled) ? 0 : 1;
                    }, () =>
                    {
                    });
            }
        }

        private string m_text;
        public string Text
        {
            get { return m_text; }
            set
            {
                m_text = value;
                m_label.Text = m_text;
            }
        }
            
        public UIColor TextColor
        {
            get { return m_label.TextColor; }
            set { m_label.TextColor = value; }
        }

        private bool m_alwaysShowOffImage = false;
        public bool AlwaysShowOffImage
        {
            get { return m_alwaysShowOffImage; }
            set
            {
                m_alwaysShowOffImage = value;

                if (m_alwaysShowOffImage)
                    m_offImageView.Hidden = false;
                else
                    m_offImageView.Hidden = IsToggled;
            }
        }

        private CGColor m_checkboxBorderColor;
        public CGColor CheckboxBorderColor 
        {
            get { return m_checkboxBorderColor; }
            set
            {
                m_checkboxBorderColor = value;

                m_offImageView.Layer.BorderColor = m_checkboxBorderColor;
                m_onImageView.Layer.BorderColor = m_checkboxBorderColor;
            }
        }

        private float m_checkboxBorderWidth;
        public float CheckboxBorderWidth
        {
            get { return m_checkboxBorderWidth; }
            set
            {
                m_checkboxBorderWidth = value;

                m_offImageView.Layer.BorderWidth = m_checkboxBorderWidth;
                m_onImageView.Layer.BorderWidth = m_checkboxBorderWidth;
            }
        }

        private float m_checkboxCornerRadius;
        public float CheckboxCornerRadius
        {
            get { return m_checkboxCornerRadius; }
            set
            {
                m_checkboxCornerRadius = value;

                m_offImageView.Layer.CornerRadius = m_checkboxCornerRadius;
                m_onImageView.Layer.CornerRadius = m_checkboxCornerRadius;
            }
        }

        public CheckBox(IntPtr handle) : base(handle)
        {
            Initialise ();
        }

        private void Initialise()
        {
            float size = Math.Min (60, Frame.Height);
            float imageSize = size * 0.7f;

            //Add both UIImages on the right
            m_onImageView = new UIImageView(new RectangleF(Frame.Width - size, (size - imageSize) / 2, imageSize, imageSize));
            m_onImageView.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
            m_onImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            m_onImageView.Image = m_onImage;
            m_onImageView.Alpha = 0;

            m_offImageView = new UIImageView(new RectangleF(Frame.Width - size, (size - imageSize) / 2, imageSize, imageSize));
            m_offImageView.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
            m_offImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            m_offImageView.Image = m_offImage;

            //Add label on the Left
            m_label = new UILabel(new RectangleF(0, 0, Frame.Width - size, size));
            m_label.Font = UIFont.FromName ("AvenirNext-Regular", 14f);
            m_label.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin;

            //Add button over the whole thing for tapping
            m_button = new UIButton(new RectangleF(0,0,Frame.Width, Frame.Height));
            m_button.AutoresizingMask = UIViewAutoresizing.None;
            m_button.BackgroundColor = UIColor.Clear;
            m_button.TintColor = UIColor.Clear;
            m_button.SetTitle (string.Empty, UIControlState.Normal);
            m_button.SetTitleColor (UIColor.Clear, UIControlState.Normal);
            m_button.TouchUpInside += OnTapped;

            AddSubview (m_onImageView);
            AddSubview (m_offImageView);
            AddSubview (m_label);
            AddSubview (m_button);

            IsToggled = false;
            Text = "Toggle";
        }

        protected override void Dispose (bool disposing)
        {
            if (disposing)
            {
                if (m_onImage != null)
                {
                    m_onImage.Dispose ();
                    m_onImage = null;
                }

                if (m_offImage != null)
                {
                    m_offImage.Dispose ();
                    m_offImage = null;
                }

                if (m_onImageView != null)
                {
                    m_onImageView.Dispose ();
                    m_onImageView = null;
                }

                if (m_offImageView != null)
                {
                    m_offImageView.Dispose ();
                    m_offImageView = null;
                }

                if (m_label != null)
                {
                    m_label.Dispose ();
                    m_label = null;
                }

                if (m_button != null)
                {
                    m_button.TouchUpInside -= OnTapped;
                    m_button.Dispose ();
                    m_button = null;
                }
            }

            base.Dispose (disposing);
        }

        private void OnTapped(object sender, EventArgs e)
        {
            IsToggled = !IsToggled;


            if (OnToggle != null)
                OnToggle (this, IsToggled);
        }
    }
}

