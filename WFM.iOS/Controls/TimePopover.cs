﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using Consortium.Client.iOS;
using System.Collections;
using System.Windows.Input;
using MonoTouch.Foundation;
using Cirrious.CrossCore;
using WFM.Core;

namespace WFM.iOS
{
    public class TimePopover : PopoverSessionCell
    {
        const float c_width = 140;
        const float c_height = 400;
        const float c_cellSize = 44;

        private UINavigationController m_nav;
        private UITableView m_table;
        private TimeTableSource m_tableSource;

        public TimePopover() : base(new UINavigationController(new UIViewController()))
        {
        
            var timeFormatService = Mvx.Resolve<ITimeFormatService>();
            
            m_nav = this.ContentViewController as UINavigationController;
            m_nav.NavigationBar.TintColor = Constants.TescoBlue;
            m_nav.NavigationBar.BarStyle = UIBarStyle.Black;
            m_nav.NavigationBar.Translucent = false;

            var root = m_nav.ViewControllers [0];
            root.Title = "Start Time";

            m_table = new UITableView (new RectangleF (0, 0, c_width, c_height - c_cellSize));
            m_table.SeparatorInset = UIEdgeInsets.Zero;
            m_table.Source = m_tableSource = new TimeTableSource ();
            m_tableSource.Dismiss += this.Dismiss;
            root.View.AddSubview (m_table);

            this.SetPopoverContentSize (new SizeF (c_width, c_height), false);
        }

        public ICommand Selected 
        { 
            get { return m_tableSource.Selected; }
            set { m_tableSource.Selected = value; }
        }

        public int StartHour 
        {
            get { return m_tableSource.StartHour; }
            set 
            {
                NSIndexPath indexPath = m_table.IndexPathForSelectedRow;
                if (indexPath != null)
                    m_table.DeselectRow (indexPath, false);

                m_tableSource.StartHour = value;
                m_table.ReloadData ();
                //m_table.SelectRow (NSIndexPath.FromRowSection(value,0), true, UITableViewScrollPosition.Middle);
            }
        }
    }
}
