// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ShiftSwapWeekControl")]
	partial class ShiftSwapWeekControl
	{
		[Outlet]
		MonoTouch.UIKit.UILabel[] DateLabels { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel[] DayLabels { get; set; }

		[Outlet]
		Consortium.Client.iOS.TableControl[] DayTables { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameTitle { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NetHoursDifference { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NetHoursOriginal { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NetHoursTitle { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NetHoursTotal { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NameTitle != null) {
				NameTitle.Dispose ();
				NameTitle = null;
			}

			if (NetHoursDifference != null) {
				NetHoursDifference.Dispose ();
				NetHoursDifference = null;
			}

			if (NetHoursOriginal != null) {
				NetHoursOriginal.Dispose ();
				NetHoursOriginal = null;
			}

			if (NetHoursTitle != null) {
				NetHoursTitle.Dispose ();
				NetHoursTitle = null;
			}

			if (NetHoursTotal != null) {
				NetHoursTotal.Dispose ();
				NetHoursTotal = null;
			}
		}
	}
}
