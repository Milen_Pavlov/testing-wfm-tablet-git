﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using System.Windows.Input;
using System.Collections.Generic;
using WFM.Core;
using MonoTouch.Foundation;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public class TimeOffNotificationPopover : PopoverSessionCell
    {
        const float c_width = 350;
        const float c_height = 400;

        private readonly SessionData m_sessionData;
        private readonly IAlertBox m_alertBox;

        private UINavigationController m_nav;
        private AlternateBackgroundTable m_table;
        private UILabel m_emptyLabel;
        private UIActivityIndicatorView m_loadingSpinner;
        private TimeOffNotificationTableSource m_tableSource;

        public ICommand Selected 
        { 
            get { return m_tableSource.Selected; }
            set { m_tableSource.Selected = value; }
        }

        public List<JDATimeOffRequest> Items
        {
            get { return m_tableSource.Notifications; }
            set 
            { 
                m_tableSource.Notifications = value; 
                m_table.ReloadData ();

                bool hasData = value != null && value.Count > 0;
                m_table.Hidden = !hasData;
                m_emptyLabel.Hidden = hasData;
            }
        }

        private bool m_loading = false;
        public bool Loading
        {
            get
            {
                return m_loading;
            }
            set
            {
                m_loading = value;
                m_table.Hidden = m_loading;
                m_loadingSpinner.Hidden = !m_loading;

                if (m_loading)
                {
                    m_emptyLabel.Hidden = true;
                    m_loadingSpinner.StartAnimating ();
                }
            }
        }

        public TimeOffNotificationPopover (): base(new UINavigationController(new UIViewController()))
        {
            m_sessionData = Mvx.Resolve<SessionData> ();
            m_alertBox = Mvx.Resolve<IAlertBox> ();

            m_nav = ContentViewController as UINavigationController;
            m_nav.NavigationBar.TintColor = UIColor.White;
            m_nav.NavigationBar.BarTintColor = Constants.TescoBlue;
            m_nav.NavigationBar.BarStyle = UIBarStyle.Black;
            m_nav.NavigationBar.Translucent = false;

            var root = m_nav.ViewControllers [0];
            root.Title = "Pending Requests";
            root.PreferredContentSize = new SizeF (c_width, c_height);
            root.View.BackgroundColor = UIColor.White;

            m_table = new AlternateBackgroundTable (new RectangleF (0, 0, c_width, c_height));
            m_table.SeparatorInset = UIEdgeInsets.Zero;
            m_table.Source = m_tableSource = new TimeOffNotificationTableSource ();
            m_table.RegisterNibForCellReuse (TimeOffNotificationCell.Nib, TimeOffNotificationCell.Key);
            m_table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            m_tableSource.Dismiss += Dismiss;
            root.View.AddSubview (m_table);

            m_emptyLabel = new UILabel(new RectangleF (0, 0, c_width, c_height));
            m_emptyLabel.Lines = 0;
            m_emptyLabel.TextAlignment = UITextAlignment.Center;
            m_emptyLabel.Font = UIFont.FromName ("AvenirNext-Regular", 14f);
            m_emptyLabel.Text = "No Pending Requests";
            root.View.AddSubview (m_emptyLabel);

            m_loadingSpinner = new UIActivityIndicatorView (new RectangleF (0, 0, c_width, c_height));
            m_loadingSpinner.HidesWhenStopped = true;
            m_loadingSpinner.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
            root.View.AddSubview (m_loadingSpinner);

            SetPopoverContentSize (new SizeF (c_width, c_height), false);
        }

        [Export("TimeOffNotificationPopoverVisibility:")]
        void UpdateVisibility(NSObject parameter)
        {
            bool hasData = Items != null && Items.Count > 0;

            if(m_table!=null)
                m_table.Hidden = !hasData;

            if(m_emptyLabel!=null)
                m_emptyLabel.Hidden = hasData;
        }
    }
}

