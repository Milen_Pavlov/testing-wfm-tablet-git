﻿using System;
using Consortium.Client.iOS;
using System.Drawing;
using MonoTouch.Foundation;
using WFM.Core;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    [Register("CommentTable")]
    public class CommentTable : AlternateBackgroundTable
    {
        private const int c_textHeightPadding = 20;
        private const int c_fontSize = 15;
        private const int c_textLabelWidth = 272;

        public CommentTable (IntPtr handle) : base (handle)
        {
        }

        public CommentTable (RectangleF frame) 
            : base(frame)
        {
        }

        protected override float OnGetHeightForRow(NSIndexPath indexPath)
        {
            var item = GetRow (indexPath) as TimeOffComment;

            if (item != null)
            {
                return heightForText(item.ToString());
            }

            var x = base.OnGetHeightForRow (indexPath);

            return x;
        }

        private float heightForText(string text)
        {
            UIFont cellFont = UIFont.SystemFontOfSize(c_fontSize);
            var size = new SizeF(c_textLabelWidth, float.MaxValue);

            var fontSize = new NSString(text).StringSize(cellFont,size, UILineBreakMode.WordWrap);

            return fontSize.Height + c_textHeightPadding;
        }
    }
}

