﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Newtonsoft.Json;
using WFM.Core;

namespace WFM.iOS
{
    public class ShiftControl : DraggableControl
    {
        enum PanMode
        {
            Move,
            SizeLeft,
            SizeRight
        };

        public Action<ShiftData> ShiftSelect { get; set; }

        private ShiftData m_shift;
        private DateTime m_dayStart;
        private DateTime m_dayEnd;
        private RectangleF m_dayFrame;
        private UIPanGestureRecognizer m_pan;
        private UITapGestureRecognizer m_tap;
        private RectangleF m_panStart;
        private UIView m_shiftView;
        private PanMode m_panMode;
        private float m_intervalWidth;
        private float m_hourWidth;

        private bool m_showDetails;
        private bool m_previousDayShift;

        const int c_sideOverlap = 10; //< Invisible area to left and right to capture resize touches
        const int c_sideTouch = c_sideOverlap * 3; //< Area on left and right which will activate size over move
        const int c_handleSize = 10; //< Area on left and right which will activate size over move

        public ShiftControl(RectangleF frame, ShiftData shift, bool canEdit, bool showDetails) 
            : this(frame, shift, shift.Start, shift.End, canEdit, showDetails)
        {

        }

        public ShiftControl(RectangleF dayFrame, ShiftData shift, DateTime dayStart, bool canEdit, bool showDetails) 
            : this(dayFrame, shift, dayStart, dayStart.AddDays(1), canEdit, showDetails)
        {

        }

        public ShiftControl(RectangleF dayFrame, ShiftData shift, DateTime dayStart, DateTime dayEnd, bool canEdit, bool showDetails) 
        {
            m_shift = shift;
            m_dayStart = dayStart;
            m_dayEnd = dayEnd;
            m_dayFrame = dayFrame;
            m_intervalWidth = m_dayFrame.Width / ScheduleTime.DayIntervals;
            m_hourWidth = m_intervalWidth * ScheduleTime.HourIntervals;
            m_showDetails = showDetails;
            m_previousDayShift = dayStart.Date != shift.Start.Date;

            //BackgroundColor = UIColor.Purple;
            DrawShift (shift, showDetails);

            // Add a tap gesture for selecting a shift
            m_tap = new UITapGestureRecognizer (OnTap);
            AddGestureRecognizer (m_tap);

            // Add edit handles and touch gestures to shift view
            if (shift.CanEdit() && canEdit)
            {
                m_pan = new UIPanGestureRecognizer (OnPan);
                AddGestureRecognizer (m_pan);

                // Add handles
                UIImage handle = UIImage.FromBundle ("drag_handle");
                var lhandle = CreateHandle(m_shiftView.Frame.X, m_shiftView.Frame.Height/2, handle);
                var rhandle = CreateHandle(m_shiftView.Frame.X+m_shiftView.Frame.Width, m_shiftView.Frame.Height/2, handle);

                lhandle.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin;
                rhandle.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;

                AddSubview (lhandle);
                AddSubview (rhandle);
            }
        }

        private void OnTap()
        {
            if (m_tap.State == UIGestureRecognizerState.Ended)
            {
                if (ShiftSelect != null)
                    ShiftSelect (m_shift);
            }
        }

        private void OnPan()
        {
            if (m_pan.State == UIGestureRecognizerState.Began)
            {
                m_panStart = GetShiftFrame();
                var point = m_pan.LocationOfTouch (0, this);

                // Redraw the shift to remove all jobs
                m_shift.Resize (m_shift.Start, m_shift.End); 
                DrawShift(m_shift, m_showDetails);

                if(m_dayStart > m_shift.Start)
                {
                    Mvx.Resolve<IAlertBox> ().ShowOK ("Shift Edit Error", "Cannot edit shifts that start on the previous day.");
                    return;
                }

                // What type of pan is this?
                if (point.X < c_sideTouch)
                {
                    m_panMode = PanMode.SizeLeft;
                }
                else if (point.X > Frame.Width - c_sideTouch - c_sideOverlap)
                {
                    m_panMode = PanMode.SizeRight;
                }
                else
                {
                    m_panMode = PanMode.Move;

                    DragStart ();
                }
            }
            else if (m_pan.State == UIGestureRecognizerState.Ended)
            {
                if (m_panMode == PanMode.Move) {
                    DragEnd ();
                    return;
                }
                
                // Adjust left and right snap to a period
                RectangleF frame = GetShiftFrame();
                frame.X = (float)Math.Round (frame.X / m_intervalWidth) * m_intervalWidth;
                frame.Width = (float)Math.Round (frame.Width / m_intervalWidth) * m_intervalWidth;
                SetShiftFrame(frame);

                // Update shift data directly or just send new request
                double minutes = (frame.X / m_dayFrame.Width) * ScheduleTime.MinutesPerDay;
                double length = (frame.Width / m_dayFrame.Width) * ScheduleTime.MinutesPerDay;

                // Clamp to remove errors
                minutes = Math.Round (minutes / 15f) * 15f;
                length = Math.Round (length / 15f) * 15f;

                var shiftStart = m_dayStart.AddMinutes (minutes);

                if (!m_shift.CanEditAtStart (shiftStart))
                {
                    // Redraw the shift to remove all jobs
                    m_shift.Resize (m_shift.Start, m_shift.End); 
                    DrawShift(m_shift, m_showDetails);
                }

                var request = new ShiftEditMessage (this) {
                    Op = ShiftEditMessage.Operation.UpdateShift,
                    ShiftID = m_shift.ShiftID,
                    EmployeeID = m_shift.EmployeeID.HasValue ? m_shift.EmployeeID.Value : 0,
                    Start = shiftStart,
                    End = shiftStart.AddMinutes(length),
                    RecalculateDetails = true
                };

                // Inform listeners of changed size - Doing via message to avoid messy chain back to the view model for now
                // This also allows the view model to handle the update logic or restore previous data
                Mvx.Resolve<IMvxMessenger> ().Publish(request);
            }
            else if (m_pan.State != UIGestureRecognizerState.Cancelled && 
                     m_pan.State != UIGestureRecognizerState.Failed &&
                     m_pan.State != UIGestureRecognizerState.Possible)
            {
                PointF offset = m_pan.TranslationInView(this);

                switch(m_panMode) 
                {
                    case PanMode.Move: OnMove(offset); break;
                    case PanMode.SizeLeft: OnSizeLeft(offset); break;
                    case PanMode.SizeRight: OnSizeRight(offset); break;
                }
            }
        }

        private void OnMove(PointF offset)
        {
            DragUpdate (offset);
        }

        private void OnSizeLeft(PointF offset)
        {
            RectangleF frame = m_panStart;
            float Y = frame.X + frame.Width;
            frame.X = Math.Max (frame.X + offset.X, 0);
            frame.X = Math.Min (frame.X, Y - m_hourWidth); //< Ensure we keep a minimum shift length
            frame.Width = Math.Min (Y - frame.X, m_dayFrame.Width); //< Cant have a shift longer than 24hr 
            SetShiftFrame(frame);
            CalculateAndSendHeaderMessage (frame);
        }

        private void OnSizeRight(PointF offset)
        {
            RectangleF frame = m_panStart;
            frame.Width = Math.Max (frame.Width + offset.X, m_hourWidth); //< Ensure we keep a minimum shift length
            frame.Width = Math.Min (frame.Width, m_dayFrame.Width - frame.X);
            SetShiftFrame(frame);
            CalculateAndSendHeaderMessage (frame);
        }

        private void DrawShift(ShiftData shift, bool showDetails)
        {
            // Remove the current shift view (includes all jobs)
            if (m_shiftView != null)
            {
                m_shiftView.RemoveFromSuperview ();
                m_shiftView.Dispose ();
            }
                
            // Create the shift view to hold all job views
            var rect = RectFromTime(shift.Start, shift.End, 4);
            var frameRect = new RectangleF(rect.X-c_sideOverlap, rect.Y, rect.Width + (c_sideOverlap*2), rect.Height);

            // Only update the frame if its different
            if (!frameRect.Equals(Frame))
                Frame = frameRect;

            m_shiftView = new UIView (new RectangleF (c_sideOverlap, 0, rect.Width, rect.Height));
            AddSubview (m_shiftView);
            SendSubviewToBack (m_shiftView);

            // Add jobs
            for (int i=0; i<shift.Jobs.Count; ++i)
            {
                var job = shift.Jobs [i];
                var jobRect = RectFromTime(job.Start, job.End, 0);

                if (jobRect.Width > 0)
                {
                    jobRect.X -= (Frame.X + c_sideOverlap); //< Adjust as we will draw in shift view frame
                    jobRect.Height = rect.Height;

                    var jobView = CreateJob (shift, job, jobRect);
                    m_shiftView.AddSubview (jobView);

                    // Breaks & Meals
                    if (showDetails)
                    {
                        foreach (var d in shift.Details)
                        {
                            if (d.Start.HasValue && d.End.HasValue)
                            {
                                //exclude roles for now
                                if(!d.IsRole)
                                {
                                    var x1 = XPosFromTime (m_dayFrame, m_dayStart, m_dayEnd, d.Start.Value);
                                    var x2 = XPosFromTime (m_dayFrame, m_dayStart, m_dayEnd, d.End.Value);
                                    var bar = new UIView (new RectangleF (x1 - Frame.X - m_shiftView.Frame.X , 0, x2 - x1 , m_shiftView.Frame.Height));
                                    bar.BackgroundColor = UIColor.FromRGBA (1, 1, 1, 0.25f);
                                    m_shiftView.AddSubview (bar);
                                }
                            }
                        }
                    }

                    if (i > 0)
                    {
                        UIView connector = new UIView (new RectangleF (jobRect.X-2,1,6,jobRect.Height-2));
                        connector.BackgroundColor = ColorUtility.FromHexString (job.JobColor);

                        UIView line = new UIView (new RectangleF (2,0,1,jobRect.Height-2));
                        line.BackgroundColor = UIColor.Black;
                        connector.AddSubview (line);

                        m_shiftView.AddSubview (connector);
                    }
                }
            }
        }

        private UIView CreateHandle(float x, float y, UIImage handle)
        {
            var rect = new RectangleF(x-(c_handleSize/2), y-(c_handleSize/2), c_handleSize, c_handleSize);
            return new UIImageView (rect) {
                Image = handle,
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
        }

        private UIView CreateJob(ShiftData shift, JobData job, RectangleF rect)
        {
            var jobBar = new UIView (rect);
            jobBar.BackgroundColor = ColorUtility.FromHexString (job.JobColor);
            jobBar.Layer.BorderColor = job.Edit != EditState.None ? UIColor.Red.CGColor : UIColor.White.CGColor;
            jobBar.Layer.BorderWidth = job.Edit != EditState.None ? 2 : 1;
            jobBar.Layer.CornerRadius = 4;
            jobBar.AutoresizingMask = UIViewAutoresizing.All;

            UILabel jobName = new UILabel (new RectangleF (4, 2, jobBar.Frame.Width - 8, (jobBar.Frame.Height/2) - 4));
            jobName.Text = job.JobName;
            if (shift.IsManualShift ())
            {
                jobName.Text = "*" + job.JobName;
            }
            jobName.Font = UIFont.FromName ("Verdana-Bold", 10);
            jobName.TextAlignment = UITextAlignment.Left;
            jobName.LineBreakMode = UILineBreakMode.TailTruncation;
            jobName.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            jobName.TextColor = UIColor.White;
            jobName.BackgroundColor = UIColor.Clear;
            jobBar.AddSubview (jobName);

            UILabel time = new UILabel (new RectangleF (4, (jobBar.Frame.Height/2) - 1, jobBar.Frame.Width - 8, (jobBar.Frame.Height/2) - 4));
            time.Text = string.Format (job.StartEnd);
            time.Font = UIFont.FromName ("Verdana", 10);
            time.TextAlignment = UITextAlignment.Left;
            time.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            time.BackgroundColor = UIColor.Clear;
            jobBar.AddSubview (time);


            //Borrowed
            if (!shift.IsCurrentSite () && shift.ShiftID > 0)
            {
                jobName.Text = string.Format ("{0} - ({1})", job.JobName, shift.SiteName);
                jobBar.BackgroundColor = ColorUtility.FromHexString (job.JobColor).ColorWithAlpha (0.4f);
                time.TextColor = UIColor.DarkGray;
            }

            return jobBar;
        }

        private float XPosFromTime(RectangleF frame, DateTime start, DateTime end, DateTime point)
        {
            DateTime clamped = DateTimeUtility.Clamp(point, start, end);
            TimeSpan offset = clamped - start;
            TimeSpan total = end - start;
            return (float)(offset.TotalMinutes * (frame.Size.Width / total.TotalMinutes));
        }

        private RectangleF RectFromTime(DateTime start, DateTime end, float yBorder)
        {
            float x1 = XPosFromTime (m_dayFrame, m_dayStart, m_dayEnd, start);
            float x2 = XPosFromTime (m_dayFrame, m_dayStart, m_dayEnd, end);

            return new RectangleF () {
                X = x1,
                Y = yBorder,
                Width = x2 - x1,
                Height = m_dayFrame.Size.Height - (yBorder * 2)
            };
        }

        private RectangleF GetShiftFrame()
        {
            return new RectangleF(Frame.X + c_sideOverlap, 0, Frame.Width - (c_sideOverlap*2), Frame.Height);
        }

        private void SetShiftFrame(RectangleF rect)
        {
            Frame = new RectangleF(rect.X-c_sideOverlap, Frame.Y, rect.Width + (c_sideOverlap*2), Frame.Height);
            m_shiftView.Frame = new RectangleF (c_sideOverlap, 0, rect.Width, rect.Height);
        }

        private void CalculateAndSendHeaderMessage(RectangleF frame)
        {
            // grab new times
            double minutes = (frame.X / m_dayFrame.Width) * ScheduleTime.MinutesPerDay;
            double length = (frame.Width / m_dayFrame.Width) * ScheduleTime.MinutesPerDay;

            // Clamp to remove errors
            minutes = Math.Round (minutes / 15f) * 15f;
            length = Math.Round (length / 15f) * 15f;

            // send off message
            var shiftStart = m_dayStart.AddMinutes (minutes);
            var shiftEnd = shiftStart.AddMinutes (length);
            var request = new ScheduleRulerMessageUpdate (this) {
                AdjustedStart = shiftStart,
                AdjustedEnd = shiftEnd
            };

            Mvx.Resolve<IMvxMessenger> ().Publish(request);
        }

        public override void CreateDragData ()
        {
            var data = new ShiftDragData () {
                ShiftID = m_shift.ShiftID,
                EmployeeID = m_shift.EmployeeID.GetValueOrDefault (),
                Start = m_shift.Start,
                End = m_shift.End,
                IsManualShift = m_shift.IsManualShift (),
            };

            InitialDragData = JsonConvert.SerializeObject (data);
        }
    }
}
