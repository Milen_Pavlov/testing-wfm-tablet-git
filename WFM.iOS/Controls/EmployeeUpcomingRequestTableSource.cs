﻿using System;
using MonoTouch.UIKit;
using System.Windows.Input;
using MonoTouch.Foundation;
using System.Collections.Generic;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public class EmployeeUpcomingRequestTableSource : UITableViewSource
    {
        public ICommand Selected { get; set; }
        public Action<bool> Dismiss { get; set; }
        public List<JDATimeOffRequest> Notifications { get; set; }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 55;
        }

        public override int RowsInSection (UITableView tableview, int section)
        {
            return Notifications != null ? Notifications.Count : 0;
        }

        public override int NumberOfSections (UITableView tableView)
        {
            return 1;
        }

        //TODO Sort this out with new cell
        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            TimeOffHistoryCell cell = tableView.DequeueReusableCell (TimeOffHistoryCell.Key, indexPath) as TimeOffHistoryCell;

            if (cell == null)
                throw new Exception("No cell type was set");

            cell.DataContext = Notifications[indexPath.Row];
            cell.OnShow ();

            return cell;
        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            if (Selected != null)
                Selected.Execute (indexPath.Row);
            
            Dismiss (true);
        }
    }
}

