﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using Consortium.Client.iOS;
using System.Collections;
using System.Windows.Input;
using MonoTouch.Foundation;
using System.Collections.Generic;
using WFM.Core;

namespace WFM.iOS
{
    public class TimeOffTypePopover : PopoverSessionCell
    {
        const float c_width = 350;
        const float c_height = 200;

        private UINavigationController m_nav;
        private UITableView m_table;
        private TimeOffTypeSource m_tableSource;

        public JDATimeOffType[] Items
        {
            get { return m_tableSource.TimeOffTypes; }
            set 
            { 
                m_tableSource.TimeOffTypes = value; 
                m_table.ReloadData ();

                bool hasData = value != null && value.Length > 0;
                m_table.Hidden = !hasData;
            }
        }

        public ICommand Selected 
        { 
            get { return m_tableSource.Selected; }
            set { m_tableSource.Selected = value; }
        }

        public TimeOffTypePopover() : base(new UINavigationController(new UIViewController()))
        {
            m_nav = this.ContentViewController as UINavigationController;
            m_nav.NavigationBar.TintColor = Constants.TescoBlue;
            m_nav.NavigationBar.BarStyle = UIBarStyle.Black;
            m_nav.NavigationBar.Translucent = false;

            var root = m_nav.ViewControllers [0];
            root.Title = "Time Off Type";

            m_table = new UITableView (new RectangleF (0, 0, c_width, c_height-m_nav.NavigationBar.Frame.Height));
            m_table.SeparatorInset = UIEdgeInsets.Zero;
            m_table.RowHeight = 44;
            m_table.SectionFooterHeight = 0;
            m_table.SectionHeaderHeight = 0;
            m_table.Source = m_tableSource = new TimeOffTypeSource ();
            m_tableSource.Dismiss += this.Dismiss;
            root.View.AddSubview (m_table);

            SetPopoverContentSize (new SizeF (c_width, c_height), false);

            //Annoying Seperator 0 issue http://stackoverflow.com/questions/25770119/ios-8-uitableview-separator-inset-0-not-working
            if (RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setSeparatorInset")))
                m_table.SeparatorInset = UIEdgeInsets.Zero;

            // Prevent the cell from inheriting the Table View's margin settings
            if (RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setPreservesSuperviewLayoutMargins")))
                m_table.PreservesSuperviewLayoutMargins = false;

            // Explictly set your cell's layout margins
            if (RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setLayoutMargins")))
                m_table.LayoutMargins = UIEdgeInsets.Zero;
        }

        public void Deselect()
        {
            if (m_table == null)
                return;

            m_table.DeselectRow (NSIndexPath.FromRowSection(m_tableSource.SelectedRow, 0), false);
        }
    }
}
