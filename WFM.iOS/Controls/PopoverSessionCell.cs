﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Consortium.Client.Core;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    // Dismiss popover on sign out automatically for simple management
    public class PopoverSessionCell : UIPopoverController
    {
        protected MvxSubscriptionToken m_signOutToken;
        private IMvxMessenger m_messager;

        public PopoverSessionCell(UIViewController vc) : base(vc)
        {
            m_messager = Mvx.Resolve<IMvxMessenger> ();
            m_signOutToken = Mvx.Resolve<IMvxMessenger>().SubscribeOnMainThread<SignOutMessage> (OnSignOut);
        }

        ~PopoverSessionCell()
        {
            m_messager.Unsubscribe<SignOutMessage> (m_signOutToken);
        }

        private void OnSignOut(SignOutMessage message)
        {
            Dismiss (false);
        }
    }
}
