﻿using System;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class GridWeekCell : TableCell
    {
        public static readonly NSString Key = new NSString ("GridWeekCell");
        public static readonly UINib Nib;
        public static readonly UIImage TriangleImage;
        public static readonly UIImage UnavailableImage;
        public static readonly UIImage TimeOffImage;
        public static readonly UIImage MinorImage;

        private const int c_rowHeight = 44;
        private const int c_rowWidth = 110;
        private const int c_handleSize = 10; //< Area on left and right which will activate size over move

        private UIPopoverController m_warningPopover;
        private UIView m_weekView;
        private JDAEmployeeInfo m_info;

        private readonly IESSConfigurationService m_essConfig;

        static GridWeekCell()
        {
            Nib = UINib.FromName (IsPhone ? "GridWeekCell_iPhone" : "GridWeekCell_iPad", NSBundle.MainBundle);
            TriangleImage = UIImage.FromBundle ("yellow_triangle");
            UnavailableImage = UIImage.FromBundle ("hatch");
            TimeOffImage = UIImage.FromBundle ("hatch_dark");
            MinorImage = UIImage.FromBundle ("hatch_red");
        }

        public GridWeekCell(IntPtr handle) : base (handle)
        {
            m_essConfig = Mvx.Resolve<IESSConfigurationService>();
        }

        public override void OnShow()
        {
            EmployeeData data = DataContext as EmployeeData;

            // Store for later
            m_info = data.Info;

            // Warning Count
            WarningButton.SetTitle (data.WarningCount == 0 ? "" : data.WarningCount.ToString (), UIControlState.Normal);
            WarningImage.Image = data.WarningCount == 0 ? null : TriangleImage;

            NameLabel.Text = string.Format("{0} {1}", data.Name, data.GetAttributes());
            HoursLabel.Text = string.Format ("{0:0.00}", data.Hours / m_essConfig.TotalHoursPerDayDivisor);

            CreateWeekArea (data.Days, data.MaxJobCount, data.CanEdit);
        }

        public override void OnHide()
        {
        }

        partial void OnNameSelect (NSObject sender)
        {
            if (CustomCommand1 != null)
                CustomCommand1.Execute(m_info);
        }

        public void OnShiftSelect (ShiftData shift)
        {
            if (CustomCommand3 != null)
                CustomCommand3.Execute(shift);
        }

        partial void OnWarningSelect (NSObject sender)
        {
            var data = DataContext as EmployeeData;

            if (data.WarningCount == 0)
                return;

            var tableControl = new TableControl(new RectangleF(0,0,400,240));
            tableControl.RegisterCell<WarningCell> ();
            tableControl.Items = data.GetAllWarnings();
            var viewController = new UIViewController();
            viewController.Add(tableControl);
            m_warningPopover = new UIPopoverController(viewController);
            m_warningPopover.DidDismiss += (object s, EventArgs e) => { m_warningPopover = null; }; 
            m_warningPopover.PopoverContentSize = tableControl.Frame.Size;
            m_warningPopover.PresentFromRect(WarningButton.Frame, this, UIPopoverArrowDirection.Left, true);
        }

        private void CreateWeekArea(List<EmployeeDayData> days, int maxJobsCount, bool canEdit)
        {
            if (m_weekView != null)
            {
                m_weekView.RemoveFromSuperview ();
                m_weekView.Dispose ();
            }

            m_weekView = new UIView (new RectangleF (0, 0, ShiftView.Frame.Width, 
                                                     canEdit ? 
                                                     Math.Max((maxJobsCount + 1) * c_rowHeight, c_rowHeight) :
                                                     Math.Max(maxJobsCount * c_rowHeight, c_rowHeight)));

            NameLabel.Center = new PointF(NameLabel.Frame.X + NameLabel.Frame.Width / 2, m_weekView.Frame.Height / 2);
            HoursLabel.Center = new PointF(HoursLabel.Frame.X + HoursLabel.Frame.Width / 2, m_weekView.Frame.Height / 2);
            WarningImage.Center = new PointF(WarningImage.Frame.X + WarningImage.Frame.Width / 2, m_weekView.Bounds.Height / 2);
            WarningButton.Center = new PointF(WarningButton.Frame.X + WarningButton.Frame.Width / 2, m_weekView.Bounds.Height / 2);
            ShiftView.Frame = new RectangleF(ShiftView.Frame.X, ShiftView.Frame.Y, ShiftView.Frame.Width, m_weekView.Frame.Height);

            for (int dayNumber = 0; dayNumber < 7; ++dayNumber)
            {
                int yCoord = 0;

                foreach (var shift in days[dayNumber].Shifts)
                {
                    foreach (var job in shift.Jobs)
                    {
                        DrawJob(m_weekView, dayNumber * c_rowWidth, yCoord, job, shift, canEdit);
                        yCoord += c_rowHeight;   
                    }
                }

                //do not display Add Shift if there's time off request for the day
                if(days[dayNumber].UnavailabilityRanges.Any(r => r.Type == "t") || days[dayNumber].UnavailabilityRanges.Any(r => r.Type == "m"))
                {
                    
                    foreach (var unavailable in days[dayNumber].UnavailabilityRanges)
                    {
                        if(unavailable.Start.Date > days[dayNumber].DayStart.Date || unavailable.End.Date <= days[dayNumber].DayStart.Date)
                            continue;
                        
                        if(DrawUnavailablity (m_weekView, dayNumber * c_rowWidth, yCoord, unavailable))
                            yCoord += c_rowHeight;
                    }
                }
                else
                {
                    if(canEdit)
                    {
                        DrawUnallocatedShift (m_weekView, dayNumber * c_rowWidth, yCoord, days[dayNumber]);   
                    }
                }
            }

            ShiftView.AddSubview (m_weekView);
            ShiftView.SendSubviewToBack(m_weekView);
        }

        private void DrawJob(UIView parent, int x, int y, JobData jobAssignment, ShiftData shift, bool canEdit)
        {
            UIView content = new UIView (new RectangleF (x, y, c_rowWidth, c_rowHeight));
            content.BackgroundColor = UIColor.White;

            UILabel time = new UILabel (new RectangleF (1, 0, c_rowWidth-1, 16));
            time.Font = UIFont.FromName ("Verdana", 10);
            time.TextAlignment = UITextAlignment.Center;
            time.Text = jobAssignment.StartEnd;
            time.AdjustsFontSizeToFitWidth = true;
            content.AddSubview (time);

            UILabel job = new UILabel (new RectangleF (0, 16, c_rowWidth, 28));
            job.Font = UIFont.FromName ("Verdana-Bold", 10);
            job.TextAlignment = UITextAlignment.Center;
            job.LineBreakMode = UILineBreakMode.WordWrap;
            job.Lines = 2;
            job.TextColor = UIColor.White;
            job.BackgroundColor = ColorUtility.FromHexString(jobAssignment.JobColor);
            job.Text = jobAssignment.JobName;
            
            if (shift.IsManualShift())
            {
                job.Text = "*" + jobAssignment.JobName;
            }
            
            content.AddSubview (job);

            //Tappable button
            UIButton button = new UIButton(new RectangleF (0, 0, c_rowWidth, c_rowHeight));
            button.BackgroundColor = UIColor.Clear;
            button.SetTitleColor (UIColor.Clear, UIControlState.Normal);
            button.SetTitle (string.Empty, UIControlState.Normal);
            button.TouchUpInside += (object sender, EventArgs e) => 
                {
                    OnShiftSelect(shift);
                };
            content.AddSubview (button);

            //Borrowed
            if (!shift.IsCurrentSite () && shift.ShiftID > 0)
            {
                job.Text = string.Format ("{0} - ({1})", jobAssignment.JobName, shift.SiteName);
                job.BackgroundColor = ColorUtility.FromHexString (jobAssignment.JobColor).ColorWithAlpha (0.4f);
                time.TextColor = UIColor.DarkGray;
                time.BackgroundColor = UIColor.LightGray;
            }

            parent.Add(content);
        }

        private bool DrawUnavailablity(UIView parent, int x, int y, JDAInterval interval)
        {
            //Just ignore types M and U, but left the code in below for future
            if (interval.Type != "t")
                return false;

            var content = new UIView (new RectangleF (x, y, c_rowWidth, c_rowHeight));

            switch (interval.Type)
            {
                case "t": content.BackgroundColor = UIColor.FromPatternImage (TimeOffImage); break;
                case "m": content.BackgroundColor = UIColor.FromPatternImage (MinorImage); break;

                default: // "u"
                    content.BackgroundColor = UIColor.FromPatternImage (UnavailableImage); 
                    break; 
            }


            if (interval.Type == "t")
            {
                var job = new UILabel (new RectangleF (0, 0, c_rowWidth, c_rowHeight));
                job.Font = UIFont.FromName ("AvenirNext-Regular", 14);
                job.TextAlignment = UITextAlignment.Center;
                job.Lines = 0;
                job.TextColor = UIColor.Black;
                job.Text = "Time Off";
                content.AddSubview (job);
            }

            parent.Add(content);

            return true;
        }

        private void DrawUnallocatedShift(UIView parent, int x, int y, EmployeeDayData day)
        {          
            UIView content = new UIView (new RectangleF (x, y, c_rowWidth, c_rowHeight));
            content.BackgroundColor = UIColor.White;

            NSAttributedString attrString = new NSAttributedString ("Add Shift", UIFont.FromName ("Avenir Next Medium", 11), null, null, null, null, NSLigatureType.Default, 0, NSUnderlineStyle.None, null, 0, NSUnderlineStyle.None); 

            //Tappable button
            UIButton button = new UIButton(new RectangleF (0, 0, c_rowWidth, c_rowHeight));
            button.ApplyCustomFonts ();
            button.SetAttributedTitle (attrString, UIControlState.Normal);
            button.BackgroundColor = UIColor.Clear;
            button.SetTitleColor (UIColor.Black, UIControlState.Normal);

            button.TouchUpInside += (object sender, EventArgs e) => 
                {
                    AddShift(day);
                };
            content.AddSubview (button);

            parent.Add (content);
        }

        private void AddShift(EmployeeDayData day)
        {
                if (CustomCommand2 != null)
                CustomCommand2.Execute ( new WeekItemData(){ Day = day, EmployeeId = m_info.EmployeeID } );
        }
    }
}

