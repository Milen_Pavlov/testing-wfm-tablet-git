﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class PayAllocationSectionCell : TableCell
    {
        public static readonly NSString Key = new NSString ("PayAllocationSectionCell");
        public static readonly UINib Nib;

        static PayAllocationSectionCell()
        {
            Nib = UINib.FromName (IsPhone ? "PayAllocationSectionCell_iPhone" : "PayAllocationSectionCell_iPad", NSBundle.MainBundle);
        }

        public PayAllocationSectionCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            PayAllocationSection data = DataContext as PayAllocationSection;
            if (data != null)
            {
                TitleLabel.Text = string.Format("Week of {0}", data.StartOfWeek.ToString (Mvx.Resolve<IStringService>().Get("dd/MM/yyyy")));
            }

            //Annoyingly, this never seems to autolayout correctly!
            BorderView.Frame = new RectangleF(0, Frame.Height - 1, Frame.Width, 1);
        }
    }
}

