// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("PeerRequestCell")]
	partial class PeerRequestCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel DatesLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel StatusLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EmployeeLabel != null) {
				EmployeeLabel.Dispose ();
				EmployeeLabel = null;
			}

			if (StatusLabel != null) {
				StatusLabel.Dispose ();
				StatusLabel = null;
			}

			if (DatesLabel != null) {
				DatesLabel.Dispose ();
				DatesLabel = null;
			}
		}
	}
}
