// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("EditShiftPunchCell")]
	partial class EditShiftPunchCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton Actual { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Delete { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Job { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Match { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PunchType { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Scheduled { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Delete != null) {
				Delete.Dispose ();
				Delete = null;
			}

			if (Actual != null) {
				Actual.Dispose ();
				Actual = null;
			}

			if (Job != null) {
				Job.Dispose ();
				Job = null;
			}

			if (Match != null) {
				Match.Dispose ();
				Match = null;
			}

			if (PunchType != null) {
				PunchType.Dispose ();
				PunchType = null;
			}

			if (Scheduled != null) {
				Scheduled.Dispose ();
				Scheduled = null;
			}
		}
	}
}
