﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class GridDayHeaderCell : CollectionCell
    {
        private const int LARGE_SEPERATOR_Y = 50;
        private const int LARGE_SEPERATOR_HEIGHT = 10;
        private const int SMALL_SEPERATOR_Y = 55;
        private const int SMALL_SEPERATOR_HEIGHT = 5;
        private const int RULER_SEGMENT_COUNT = 23;
        private const int SEPERATOR_WIDTH = 1;
        public static readonly NSString Key = new NSString ("GridDayHeaderCell");
        public static readonly UINib Nib;

        private UIView m_timeView;
        private readonly ITimeFormatService m_timeFormatService;
        UIColor m_primaryColour;
        UIColor m_textColour;

        static GridDayHeaderCell()
        {
            Nib = UINib.FromName (IsPhone ? "GridDayHeaderCell_iPhone" : "GridDayHeaderCell_iPad", NSBundle.MainBundle);
        }

        public SortState Sort
        {
            get { return null; }
            set 
            { 
                SortImage.Image = UIImage.FromBundle (value.IsAscending ? "sort_up" : "sort_down");
                SortImage.Hidden = !value.IsSorting;
            }
        }

        public int StartHour
        {
            get { return 0; }
            set 
            {
               SetupRuler(value);
            }
        }
        
        public GridDayHeaderCell(IntPtr handle) : base (handle)
        {
            var settings = Mvx.Resolve<SettingsService> ();
            m_primaryColour = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            m_textColour = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
        }
        
        void SetupRuler(int startHour)
        {
            if (m_timeView != null)
            {
                m_timeView.RemoveFromSuperview ();
                m_timeView.Dispose ();
            }

            float width = Frame.Width;
            m_timeView = new UIView (new RectangleF (0, 0, width, Frame.Height));
            m_timeView.BackgroundColor = UIColor.Clear;
            AddSubview (m_timeView);
            SendSubviewToBack (m_timeView);

            if (startHour != 0)
            {
                float nextStart = (width / 24) * (24 - startHour);
                float nextWidth = width - nextStart;
                var nextDay = new UIView (new RectangleF (nextStart, 0, nextWidth, Frame.Height));
                nextDay.BackgroundColor = UIColor.DarkGray;
                m_timeView.AddSubview (nextDay);
            }
            
            UIFont hourLabelFont = UIFont.FromName("Verdana", 11);
            
            DateTime rulerSegmentTime = new DateTime();
            rulerSegmentTime = rulerSegmentTime.AddHours(startHour);
                    
            for(int i=1; i<= RULER_SEGMENT_COUNT; i+=1)
            {
                rulerSegmentTime = rulerSegmentTime.AddHours(1);
                
                bool shouldDisplayNumber = i % 2 == 0;
                
                int lineYStart = SMALL_SEPERATOR_Y;
                int lineHeight = SMALL_SEPERATOR_HEIGHT;
                
                float seperatorXPosition = i*(width/24);

                if(shouldDisplayNumber)
                {                 
                    string formattedRulerText = m_timeFormatService.FormatTimeForRuler(rulerSegmentTime);
                
                    lineYStart = LARGE_SEPERATOR_Y;
                    lineHeight = LARGE_SEPERATOR_HEIGHT;
                    
                    var rulerLabel = new UILabel(new RectangleF(seperatorXPosition-20, 25, 40, 25));
                    
                    rulerLabel.Font = hourLabelFont;
                    rulerLabel.TextAlignment = UITextAlignment.Center;
                    rulerLabel.BackgroundColor = UIColor.Clear;
                    rulerLabel.TextColor = m_textColour;
                    rulerLabel.Text = formattedRulerText;
                    
                    m_timeView.AddSubview (rulerLabel);
                }
                
                UIView rulerLine = new UILabel(new RectangleF(seperatorXPosition, lineYStart, SEPERATOR_WIDTH, lineHeight));
                
                rulerLine.BackgroundColor = UIColor.White;
                
                m_timeView.AddSubview(rulerLine);
            }
        }

        

        public override void OnBind()
        {
            this.BackgroundColor = m_primaryColour;
            this.TitleLabel.TextColor = m_textColour;

            var set = this.CreateBindingSet<GridDayHeaderCell, ColumnHeader>();
            set.Bind (this).For(b => b.StartHour).To (m => m.StartHour);
            set.Bind (this).For(b => b.Sort).To (m => m.Sort);
            set.Bind (TitleLabel).To (m => m.Title);
            set.Apply();
        }
    }
}