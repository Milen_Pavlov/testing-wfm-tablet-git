﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class TypeSelectionCell : TableCell
    {
        public static readonly NSString Key = new NSString ("TypeSelectionCell");
        public static readonly UINib Nib;

        static TypeSelectionCell()
        {
            Nib = UINib.FromName (IsPhone ? "TypeSelectionCell_iPhone" : "TypeSelectionCell_iPad", NSBundle.MainBundle);
        }

        public TypeSelectionCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnBind()
        {
            var set = this.CreateBindingSet<TypeSelectionCell, JDANameAndID>();
            set.Bind (TitleLabel).To (m => m.Name);
            set.Apply();
        }
    }
}

