// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("GridWeekHeaderCell")]
	partial class GridWeekHeaderCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView SortImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Title1Label { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Title2Label { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Value1Label { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Value2Label { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (SortImage != null) {
				SortImage.Dispose ();
				SortImage = null;
			}

			if (TitleLabel != null) {
				TitleLabel.Dispose ();
				TitleLabel = null;
			}

			if (Title1Label != null) {
				Title1Label.Dispose ();
				Title1Label = null;
			}

			if (Title2Label != null) {
				Title2Label.Dispose ();
				Title2Label = null;
			}

			if (Value1Label != null) {
				Value1Label.Dispose ();
				Value1Label = null;
			}

			if (Value2Label != null) {
				Value2Label.Dispose ();
				Value2Label = null;
			}
		}
	}
}
