// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
    [Register ("UnfilledShiftDetailCell")]
    partial class UnfilledShiftDetailCell
    {
        [Outlet]
        MonoTouch.UIKit.UILabel DateLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UIButton DeleteShiftLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel HoursLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel JobLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel StartLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DateLabel != null) {
                DateLabel.Dispose ();
                DateLabel = null;
            }

            if (DeleteShiftLabel != null) {
                DeleteShiftLabel.Dispose ();
                DeleteShiftLabel = null;
            }

            if (HoursLabel != null) {
                HoursLabel.Dispose ();
                HoursLabel = null;
            }

            if (JobLabel != null) {
                JobLabel.Dispose ();
                JobLabel = null;
            }

            if (StartLabel != null) {
                StartLabel.Dispose ();
                StartLabel = null;
            }
        }
    }
}