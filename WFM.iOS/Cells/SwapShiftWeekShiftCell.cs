﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class SwapShiftWeekShiftCell : TableCell
    {
        public static readonly UIColor COLOUR_GAINING = new UIColor (0.22f, 0.71f, 0.29f, 0.5f);
        public static readonly UIColor COLOUR_LOSING = new UIColor (0.93f, 0.15f, 0.15f, 0.5f);
        
        private readonly ITimeFormatService m_timeFormatService;
        
        public static readonly NSString Key = new NSString ("SwapShiftWeekShiftCell");
        public static readonly UINib Nib;

        static SwapShiftWeekShiftCell()
        {
            Nib = UINib.FromName (IsPhone ? "SwapShiftWeekShiftCell_iPhone" : "SwapShiftWeekShiftCell_iPad", NSBundle.MainBundle);
        }

        public SwapShiftWeekShiftCell(IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;
            
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
        }

        public override void OnShow()
        {
            JDAApprovalShift data = DataContext as JDAApprovalShift;

            if (data == null)
                return;
            
            JobLabel.Text = data.JobNames;
            
            
            TimeLabel.Text = string.Format ("{0} - {1}", m_timeFormatService.FormatTime(data.ShiftStart), m_timeFormatService.FormatTime(data.ShiftEnd));

            JobLabel.BackgroundColor = UIColor.Clear;
            TimeLabel.BackgroundColor = UIColor.Clear;

            switch (data.GetShiftType ())
            {
                case ApprovalShiftType.Gaining:
                    BackgroundView.BackgroundColor = COLOUR_GAINING;
                    break;
                case ApprovalShiftType.Losing:
                    BackgroundView.BackgroundColor = COLOUR_LOSING;
                    break;
                case ApprovalShiftType.Normal:
                    BackgroundView.BackgroundColor = UIColor.White;
                    break;
            }

            BackgroundView.Layer.CornerRadius = 4;

        }
    }
}

