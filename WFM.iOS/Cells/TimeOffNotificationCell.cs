﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class TimeOffNotificationCell : AlternateBackgroundCell
    {
        private readonly string m_newHolidayString = "{0} {1}";
        private readonly string m_newIcon = "assets://Icons/icon_notification_new.png";
        private readonly IStringService m_localiser;

        public static readonly NSString Key = new NSString ("TimeOffNotificationCell");
        public static readonly UINib Nib;

        static TimeOffNotificationCell()
        {
            Nib = UINib.FromName (IsPhone ? "TimeOffNotificationCell_iPhone" : "TimeOffNotificationCell_iPad", NSBundle.MainBundle);
        }

        public TimeOffNotificationCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnShow()
        {
            base.OnShow ();

            JDATimeOffRequest data = this.DataContext as JDATimeOffRequest;
            if (data != null)
            {

                Icon.BackgroundColor = UIColor.Clear;
                Icon.Uri = m_newIcon;

                string format = "{0} - {1}";
                if ((data.End - data.Start).TotalDays <= 1)
                    format = "{0}, All Day";
                        

                SubtitleLabel.Text = string.Format (format, data.Start.ToString (m_localiser.Get("dd MMMM yyyy")), data.End.ToString (m_localiser.Get("dd MMMM yyyy")));
                TitleLabel.Text = string.Format(m_newHolidayString, data.EmployeeFirstName, data.EmployeeLastName);
            }
        }
    }
}

