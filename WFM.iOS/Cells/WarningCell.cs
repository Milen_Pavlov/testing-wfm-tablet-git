﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class WarningCell : TableCell
    {
        public static readonly NSString Key = new NSString ("WarningCell");
        public static readonly UINib Nib;

        static WarningCell()
        {
            Nib = UINib.FromName (IsPhone ? "WarningCell_iPhone" : "WarningCell_iPad", NSBundle.MainBundle);
        }

        public WarningCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            TitleLabel.Text = this.DataContext as string;
        }
    }
}

