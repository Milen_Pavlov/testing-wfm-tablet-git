﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class DailyOverviewItemExceptionCell : DynamicHeightCell
    {
        public static readonly float Border = 4;

        public static readonly NSString Key = new NSString ("DailyOverviewItemExceptionCell");
        public static readonly UINib Nib;

        static DailyOverviewItemExceptionCell()
        {
            Nib = UINib.FromName (IsPhone ? "DailyOverviewItemExceptionCell_iPhone" : "DailyOverviewItemExceptionCell_iPad", NSBundle.MainBundle);
        }

        private bool m_isMajor = false;
        public bool IsMajor
        {
            get { return m_isMajor; }
            set
            {
                m_isMajor = value;
                Name.TextColor = (value) ? DailyOverviewView.Red : DailyOverviewView.Orange;
            }
        }

        public DailyOverviewItemExceptionCell(IntPtr handle) : base (handle)
        {
        }
         
        public override void OnBind ()
        {
            base.OnBind ();

            //Ensure its clear for background of container cell to appear
            BackgroundColor = UIColor.Clear;
            if (BackgroundView != null)
                BackgroundView.BackgroundColor = UIColor.Clear;

            if (Name != null)
                Name.BackgroundColor = UIColor.Clear;

            var set = this.CreateBindingSet<DailyOverviewItemExceptionCell, TimecardException>();
            set.Bind (this).For (v => v.IsMajor).To (vm => vm.IsMajor);
            set.Bind (Name).To (vm => vm.Text);
            set.Apply();
        }

        public override float GetHeightResized ()
        {
            //Custom cell resize
            float heightBefore = Name.Frame.Height;
            Name.SizeToFit();
            float heightAfter = Name.Frame.Height;

            float diff = heightAfter - heightBefore;
            float newheight = Frame.Height + diff;

            //Add border (above+below combined)
            newheight += Border * 2;

            return newheight;
        }
    }
}

