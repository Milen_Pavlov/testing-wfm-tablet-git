﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class GridWeekHeaderCell : CollectionCell
    {
        public static readonly NSString Key = new NSString ("GridWeekHeaderCell");
        public static readonly UINib Nib;

        UIColor m_primaryColour;
        UIColor m_textColour;

        public SortState Sort
        {
            get { return null; }
            set 
            { 
                SortImage.Image = UIImage.FromBundle (value.IsAscending ? "sort_up" : "sort_down");
                SortImage.Hidden = !value.IsSorting;
            }
        }

        static GridWeekHeaderCell()
        {
            Nib = UINib.FromName (IsPhone ? "GridWeekHeaderCell_iPhone" : "GridWeekHeaderCell_iPad", NSBundle.MainBundle);
        }
            
        public GridWeekHeaderCell(IntPtr handle) : base (handle)
        {
            var settings = Mvx.Resolve<SettingsService> ();
            m_primaryColour = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            m_textColour = ColorUtility.FromHexString (settings.AppTheme.SecondaryTextColor);
        }

        public override void OnBind()
        {
            this.BackgroundColor = m_primaryColour;
            this.TitleLabel.TextColor = m_textColour;
            this.Title1Label.TextColor = m_textColour;
            this.Title2Label.TextColor = m_textColour;

            this.Value1Label.TextColor = m_textColour;
            this.Value2Label.TextColor = m_textColour;

            var set = this.CreateBindingSet<GridWeekHeaderCell, ColumnHeader>();
            set.Bind (TitleLabel).To(m => m.Title);
            set.Bind (Title1Label).To(m => m.Title1);
            set.Bind (Title2Label).To(m => m.Title2);
            set.Bind (Value1Label).To(m => m.Value1);
            set.Bind (Value2Label).To(m => m.Value2);
            set.Bind (this).For(b => b.Sort).To (m => m.Sort);
            set.Apply();
        }
    }
}

