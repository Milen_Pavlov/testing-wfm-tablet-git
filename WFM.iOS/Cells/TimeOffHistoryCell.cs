﻿using System;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class TimeOffHistoryCell : TableCell
    {
        private readonly string m_deniedIcon = "assets://Icons/icon_notification_denied_red.png";
        private readonly string m_approvedIcon = "assets://Icons/icon_notification_approved.png";
        private readonly string m_pendingIcon = "assets://Icons/icon_notification_change.png";

        public static readonly NSString Key = new NSString ("TimeOffHistoryCell");
        public static readonly UINib Nib;
        private IStringService m_localiser;
        private readonly bool m_hideTimeOffType;

        static TimeOffHistoryCell()
        {
            Nib = UINib.FromName (IsPhone ? "TimeOffHistoryCell_iPhone" : "TimeOffHistoryCell_iPad", NSBundle.MainBundle);
        }

        public TimeOffHistoryCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            m_hideTimeOffType = Mvx.Resolve<IESSConfigurationService>().HideTimeOffType;

        }

        public override void OnShow()
        {
            base.OnShow ();

            JDATimeOffRequest data = this.DataContext as JDATimeOffRequest;
            if (data != null)
            {

                Icon.BackgroundColor = UIColor.Clear;

                TimeSpan span = data.End - data.Start;
                if (span.TotalDays <= 1)
                    Title.Text = data.Start.ToString (m_localiser.Get("dd/MM/yyyy"));
                else
                    Title.Text = string.Format ("{0} - {1}", data.Start.ToString (m_localiser.Get("dd/MM/yyyy")), data.End.ToString (m_localiser.Get("dd/MM/yyyy")));

                switch (data.Status)
                {
                    case TimeOffStatus.Approved:
                        Icon.Uri = m_approvedIcon;
                        if(!m_hideTimeOffType)
                        {
                            SubTitle.Text = string.Format ("{0} - {1}", data.PayAdjustmentCategoryName, m_localiser.Get("approvalstatus_managerapproved"));
                        }
                        else
                        {
                            SubTitle.Text = m_localiser.Get("approvalstatus_managerapproved");
                        }
                        break;
                    case TimeOffStatus.Denied:
                        Icon.Uri = m_deniedIcon;
                        if(!m_hideTimeOffType)
                        {
                            SubTitle.Text = string.Format ("{0} - {1}", data.PayAdjustmentCategoryName, m_localiser.Get("approvalstatus_denied"));
                        }
                        else
                        {
                            SubTitle.Text = m_localiser.Get("approvalstatus_denied");
                        }
                        break;
                    case TimeOffStatus.Deleted:
                        Icon.Uri = m_deniedIcon;
                        if(!m_hideTimeOffType)
                        {
                            SubTitle.Text = string.Format ("{0} - {1}", data.PayAdjustmentCategoryName, m_localiser.Get("approvalstatus_deleted"));
                        }
                        else
                        {    
                            SubTitle.Text = m_localiser.Get("approvalstatus_deleted");
                        }
                        break;
                    case TimeOffStatus.Pending:
                        Icon.Uri = m_pendingIcon;
                        if(!m_hideTimeOffType)
                        {
                            SubTitle.Text = string.Format ("{0} - {1}", data.PayAdjustmentCategoryName, m_localiser.Get("approvalstatus_pending"));
                        }
                        else
                        {
                            SubTitle.Text = m_localiser.Get("approvalstatus_pending");
                        }
                        break;
                    default:
                        Icon.Uri = m_pendingIcon;
                        if(!m_hideTimeOffType)
                        {
                            SubTitle.Text = string.Format ("{0} - {1}", data.PayAdjustmentCategoryName, m_localiser.Get("approvalstatus_none"));
                        }
                        else
                        {
                            SubTitle.Text = m_localiser.Get("approvalstatus_none");
                        }
                        break;
                }
            }
        }
    }
}

