﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleSummaryDayHeaderCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("ScheduleSummaryDayHeaderCell");
        public static readonly UINib Nib;

        private IStringService m_localiser;

        static ScheduleSummaryDayHeaderCell()
        {
            Nib = UINib.FromName (IsPhone ? "ScheduleSummaryDayHeaderCell_iPhone" : "ScheduleSummaryDayHeaderCell_iPad", NSBundle.MainBundle);
        }

        public ScheduleSummaryDayHeaderCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            SelectionStyle = UITableViewCellSelectionStyle.None;
        }

        public override void OnShow()
        {
            JDAScheduleSummaryGroupBaseEntry data = this.DataContext as JDAScheduleSummaryGroupBaseEntry;

            if (data != null)
            {
                TitleLabel.Text = data.Title;
            }
        }

        public override void SetSelected (bool selected, bool animated)
        {
            if (Separator != null)
            {
                UIColor col = Separator.BackgroundColor;
                base.SetSelected (selected, animated);
                Separator.BackgroundColor = col;
            }
            else
            {
                base.SetSelected (selected, animated);
            }
        }

        public override void SetHighlighted (bool highlighted, bool animated)
        {
            if (Separator != null)
            {
                UIColor col = Separator.BackgroundColor;
                base.SetHighlighted (highlighted, animated);
                Separator.BackgroundColor = col;
            }
            else
            {
                base.SetHighlighted (highlighted, animated);
            }
        }
    }
}

