﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class TimeOffRequestCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("TimeOffRequestCell");
        public static readonly UINib Nib;

        private readonly IStringService m_localiser;
        private readonly bool m_hideTimeOffType;

        private static UIColor CustomOddCol = new UIColor (0.85f, 0.85f, 0.85f, 1.0f);
        private static UIColor CustomEvenCol = new UIColor (0.92f, 0.92f, 1.0f, 1.0f);

        public override UIColor OddColour
        {
            get { return CustomOddCol; }
        }

        public override UIColor EvenColour
        {
            get { return CustomEvenCol; }
        }

        public override UIView BackgroundViewToRecolour
        {
            get { return CellBackgroundView; }
        }

        static TimeOffRequestCell()
        {
            Nib = UINib.FromName (IsPhone ? "TimeOffRequestCell_iPhone" : "TimeOffRequestCell_iPad", NSBundle.MainBundle);

        }

        public TimeOffRequestCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            m_hideTimeOffType = Mvx.Resolve<IESSConfigurationService>().HideTimeOffType;
        }

        public override void OnShow()
        {
            base.OnShow ();

            JDATimeOffRequest data = this.DataContext as JDATimeOffRequest;
            if (data != null)
            {
                if(!m_hideTimeOffType)
                {
                    TitleLabel.Text = string.Format("{0}, {1} - {2}", data.EmployeeLastName, data.EmployeeFirstName, data.PayAdjustmentCategoryName);
                }
                else
                {
                    TitleLabel.Text = string.Format("{0}, {1}", data.EmployeeLastName, data.EmployeeFirstName);
                }

                string format = "{0} - {1}";
                if ((data.End - data.Start).TotalDays <= 1)
                    format = "{0}, All Day";
                     
                SubtitleLabel.Text = string.Format (format, data.Start.ToString (m_localiser.Get("dd MMMM yyyy")), data.End.ToString (m_localiser.Get("dd MMMM yyyy")));
            }
        }
    }
}

