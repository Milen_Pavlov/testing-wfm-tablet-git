﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ForecastGroupViewCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("ForecastGroupViewCell");
        public static readonly UINib Nib;

        static ForecastGroupViewCell ()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                Nib = UINib.FromName ("ForecastGroupViewCell_iPhone", NSBundle.MainBundle);
            else
                Nib = UINib.FromName ("ForecastGroupViewCell_iPad", NSBundle.MainBundle);
        }

        public ForecastGroupViewCell (IntPtr handle) : base (handle)
        {
        }

        public override void OnShow ()
        {
            base.OnShow ();

            JDANameAndID data = DataContext as JDANameAndID;

            if (data != null)
            {
                NameLabel.Text = string.Format ("{0}", data.Name);
            }
        }

        public static ForecastGroupViewCell Create ()
        {
            return (ForecastGroupViewCell)Nib.Instantiate (null, null)[0];
        }
    }
}

