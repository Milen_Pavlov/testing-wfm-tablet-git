﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;

namespace WFM.iOS
{
    public partial class WorkgroupViewCell : TableCell
    {
        public static readonly NSString Key = new NSString ("WorkgroupViewCell");
        public static readonly UINib Nib;

        static WorkgroupViewCell ()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                Nib = UINib.FromName ("WorkgroupViewCell_iPhone", NSBundle.MainBundle);
            else
                Nib = UINib.FromName ("WorkgroupViewCell_iPad", NSBundle.MainBundle);
        }

        public WorkgroupViewCell (IntPtr handle) : base (handle)
        {
            this.SelectionStyle = UITableViewCellSelectionStyle.None;
        }

        public override void OnShow ()
        {
            base.OnShow ();

            SelectableWorkgroup data = DataContext as SelectableWorkgroup;

            if (data != null)
            {
                NameLabel.Text = string.Format ("{0}", data.Name);
            }

            if (!data.Selected)
            {
                this.Accessory = UITableViewCellAccessory.None;
            }
            else
            {
                this.Accessory = UITableViewCellAccessory.Checkmark;
            }
             
        }

        public static WorkgroupViewCell Create ()
        {
            return (WorkgroupViewCell)Nib.Instantiate (null, null)[0];
        }
    }
}