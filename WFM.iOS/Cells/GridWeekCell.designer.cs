// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("GridWeekCell")]
	partial class GridWeekCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton EmployeeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView NameBackground { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView ShiftView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton WarningButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView WarningImage { get; set; }

		[Action ("OnNameSelect:")]
		partial void OnNameSelect (MonoTouch.Foundation.NSObject sender);

		[Action ("OnWarningSelect:")]
		partial void OnWarningSelect (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (EmployeeButton != null) {
				EmployeeButton.Dispose ();
				EmployeeButton = null;
			}

			if (HoursLabel != null) {
				HoursLabel.Dispose ();
				HoursLabel = null;
			}

			if (NameBackground != null) {
				NameBackground.Dispose ();
				NameBackground = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (ShiftView != null) {
				ShiftView.Dispose ();
				ShiftView = null;
			}

			if (WarningButton != null) {
				WarningButton.Dispose ();
				WarningButton = null;
			}

			if (WarningImage != null) {
				WarningImage.Dispose ();
				WarningImage = null;
			}
		}
	}
}
