﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using WFM.Core;

namespace WFM.iOS
{
    [Register("JobDataTableControl")]
    public class JobDataTableControl : TableControl
    {
        DynamicCellHeightService m_cellHeightService;

        public JobDataTableControl (IntPtr handle) 
            : base (handle)
        {
            Delegate = new DragTableControlDelegate(GetHeightForRow);

            m_cellHeightService = Mvx.Resolve<DynamicCellHeightService>();
        }

        public float GetHeightForRow(NSIndexPath indexPath)
        {
            var data = GetRow(indexPath) as JobData;
            float height = m_cellHeightService.GetHeightForData(data, DailyOverviewItemJobCell.Nib, data.JobName);

            if(height > 0)
                return height;
            else
                return base.OnGetHeightForRow (indexPath);
        }
    }
}

