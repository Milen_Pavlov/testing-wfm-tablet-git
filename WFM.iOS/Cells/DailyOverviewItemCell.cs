﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using System.Globalization;
using System.Drawing;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class DailyOverviewItemCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("DailyOverviewItemCell");
        public static readonly UINib Nib;

        static DailyOverviewItemCell()
        {
            Nib = UINib.FromName (IsPhone ? "DailyOverviewItemCell_iPhone" : "DailyOverviewItemCell_iPad", NSBundle.MainBundle);
        }

        private bool m_dailyCellSet;
        
        private TimecardDurationToStringConverter m_durationConverter = new TimecardDurationToStringConverter ();
        private float m_exceptionTableRootY, m_jobsTableRootY;
        private ITimeFormatService m_timeFormatService; 

        private bool m_dailyCell;
        public bool DailyCell 
        {
            get { return m_dailyCell; }
            set { UpdateCellPositions (value); }
        }

        private SiteTimecardStatus m_clockStatus;
        public SiteTimecardStatus ClockStatus
        {
            get { return m_clockStatus; }
            set { m_clockStatus = value; UpdateStatus (); }
        }
           
        //So many properties needed for exceptions, so i'm cheating and just binding the data, otherwise there would
        //be 9 bool properties all setting "set ui" updates ect
        private SiteTimecardEntry m_data;
        public SiteTimecardEntry Data
        {
            get { return m_data; }
            set { m_data = value; UpdateState ();}
        }
            

        public DailyOverviewItemCell(IntPtr handle) : base (handle)
        {
        }
         
        public override void OnBind ()
        {
            base.OnBind ();

            CellButton.TouchUpInside += OnClicked;

            Jobs.RegisterCell<DailyOverviewItemJobCell> ();
            Jobs.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            Jobs.AllowsSelection = false;
            //Jobs.BackgroundColor = new UIColor(1,0,0,0.25f); //Uncomment to test resize

            Exceptions.RegisterCell<DailyOverviewItemExceptionCell> ();
            Exceptions.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            Exceptions.AllowsSelection = false;
            //Exceptions.BackgroundColor = new UIColor(1,0,0,0.25f); //Uncomment to test resize

            var set = this.CreateBindingSet<DailyOverviewItemCell, SiteTimecardEntry>();
            set.Bind (Name).To (vm => vm.EmployeeName);
            set.Bind (this).For (v => v.ClockStatus).To (vm => vm.Status);
            set.Bind (Date).To (vm => vm.Date).WithConversion (new NullableDateTimeDateToStringConverter (), "MMM d");
            set.Bind (PlannedStart).To (vm => vm.ShiftStartPlanned).WithConversion (new NullableDateTimeTimeToStringConverter ());
            set.Bind (ActualStart).To(vm => vm.ShiftStartActual).WithConversion (new NullableDateTimeTimeToStringConverter ());
            set.Bind (PlannedEnd).To (vm => vm.ShiftEndPlanned).WithConversion (new NullableDateTimeTimeToStringConverter ());
            set.Bind (ActualEnd).To (vm => vm.ShiftEndActual).WithConversion (new NullableDateTimeTimeToStringConverter ());
            set.Bind (PlannedDuration).To (vm => vm.ShiftDurationHoursPlanned).WithConversion (new TimecardDurationToStringConverter ());
            set.Bind (Jobs).To (vm => vm.Jobs);
            set.Bind (Exceptions).To (vm => vm.FilteredExceptions);
            set.Bind (this).For (v => v.DailyCell).To (vm => vm.DailyCell);
            set.Bind (this).For (v => v.Data).To (vm => vm);//Sorry
            set.Apply();
        }

        void OnClicked (object sender, EventArgs e)
        {
            if (CustomCommand1 != null)
            {
                CustomCommand1.Execute (DataContext as SiteTimecardEntry);
            }
        }

        /// <summary>
        /// Update any non binding UI
        /// </summary>
        private void UpdateStatus()
        {
            switch (ClockStatus)
            {
                case SiteTimecardStatus.NoData:
                    Status.Text = string.Empty;
                    break;
                case SiteTimecardStatus.OffClock:
                    Status.Text = "Off Clock";
                    break;
                case SiteTimecardStatus.OnClock:
                    Status.Text = "Working";
                    break;
            }
        }

        private void UpdateState()
        {
            m_timeFormatService = m_timeFormatService ?? Mvx.Resolve<ITimeFormatService>();

            if(m_exceptionTableRootY == 0)
                m_exceptionTableRootY = Exceptions.Frame.Y;
            if(m_jobsTableRootY == 0)
                m_jobsTableRootY = Jobs.Frame.Y;

            //Start
            string start = "Missing";
            
            if(m_data!=null && !m_data.StartTimeMissing)
            {
                if(m_data.ShiftStartActual == null)
                    start = String.Empty;
                else
                    start = m_timeFormatService.FormatTime(m_data.ShiftStartActual.Value);
            }

            ActualStart.Text = start;
            ActualStart.TextColor = UIColor.Black;


            //End
            string end = "Missing";
            
            if(m_data!=null && !m_data.EndTimeMissing)
            {
                if(m_data.ShiftEndActual == null)
                    end = String.Empty;
                else
                    end = m_timeFormatService.FormatTime(m_data.ShiftEndActual.Value);
            }
            
            ActualEnd.Text = end;
            ActualEnd.TextColor = UIColor.Black;

            //Duration
            string duration = "???";
            if(m_data!=null && !m_data.DurationMissing)
                duration = (string)m_durationConverter.Convert(m_data.ShiftDurationHoursActual, typeof(double),null, CultureInfo.CurrentCulture);

            ActualDuration.Text = duration;
            ActualDuration.TextColor = UIColor.Black;


            if (m_data != null)
            {
                if (m_data.StartTimeMajorException)
                    ActualStart.TextColor = DailyOverviewView.Red;
                else if (m_data.StartTimeMinorException)
                    ActualStart.TextColor = DailyOverviewView.Orange;

                if (m_data.EndTimeMajorException)
                    ActualEnd.TextColor = DailyOverviewView.Red;
                else if (m_data.EndTimeMinorException)
                    ActualEnd.TextColor = DailyOverviewView.Orange;

                if (m_data.DurationMajorException)
                    ActualDuration.TextColor = DailyOverviewView.Red;
                else if (m_data.DurationMinorException)
                    ActualDuration.TextColor = DailyOverviewView.Orange;
            }

            //Resize tables and center in height
            RectangleF frame = Jobs.Frame;
            frame.Height = Jobs.ContentSize.Height;
            //Shift height by 1 border of the cells
            frame.Y = m_jobsTableRootY - DailyOverviewItemJobCell.Border;
            Jobs.Frame = frame;

            frame = Exceptions.Frame;
            frame.Height = Exceptions.ContentSize.Height;
            //Shift height by 1 border of the cells
            frame.Y = m_exceptionTableRootY - DailyOverviewItemExceptionCell.Border;
            Exceptions.Frame = frame;

        }

        private void UpdateCellPositions(bool dailyCell)
        {
            if (m_dailyCellSet)
                return;

            m_dailyCell = dailyCell;
            m_dailyCellSet = true;
            if (m_dailyCell)
            {
                float hiddenWidth = HideCells (DailylyCellsToHide);
                MoveCells (DailyCellsToMove, hiddenWidth);
                // Couldn't add jobs and exceptions to the UIView arrays in xcode, so move them separately here
                MoveCell (Jobs, hiddenWidth);
                MoveCell (Exceptions, hiddenWidth);
            }
            else
            {
                float hiddenWidth = HideCells (WeeklyCellsToHide);
                MoveCells (WeeklyCellsToMove, hiddenWidth);
                // Couldn't add jobs and exceptions to the UIView arrays in xcode, so move them separately here
                MoveCell (Exceptions, hiddenWidth);
            }
        }

        private float HideCells(UIView[] cells)
        {
            float hiddenWidth = 0.0f;
            foreach (var cell in cells)
            {
                cell.Hidden = true;
                hiddenWidth += cell.Frame.Width;
            }

            return hiddenWidth;
        }

        private void MoveCells(UIView[] cells, float x)
        {
            foreach (var cell in cells)
            {
                MoveCell (cell, x);
            }
        }

        private void MoveCell(UIView cell, float x)
        {
            var frame = cell.Frame;
            frame.X -= x;
            cell.Frame = frame;
        }
    }
}