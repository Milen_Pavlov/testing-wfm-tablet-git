﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ReportCell : TableCell
    {
        public static readonly NSString Key = new NSString ("ReportCell");
        public static readonly UINib Nib;

        static ReportCell()
        {
            Nib = UINib.FromName (IsPhone ? "ReportCell_iPhone" : "ReportCell_iPad", NSBundle.MainBundle);
        }

        public ReportCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            ReportItem data = DataContext as ReportItem;

            TimeLabel.Text = data.Time;
            DemandLabel.Text = data.Demand.ToString();
            ScheduledLabel.Text = data.Scheduled.ToString();
            VarianceLabel.Text = data.Variance.ToString();

            VarianceLabel.TextColor = data.Variance < 0 ? UIColor.Red : UIColor.Black;
        }
    }
}

