﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using MonoTouch.CoreGraphics;
using Consortium.Client.iOS;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class DatePickerGridCell : TableCell
    {
        public static readonly NSString Key = new NSString ("DatePickerGridCell");
        public static readonly UINib Nib;
        private IStringService m_localiser;
        public const float _LIGHTGRAY = 0.878f;

        static DatePickerGridCell ()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                Nib = UINib.FromName ("DatePickerGridCell_iPhone", NSBundle.MainBundle);
            else
                Nib = UINib.FromName ("DatePickerGridCell_iPad", NSBundle.MainBundle);
        }

        public DatePickerModel Model
        {
            get;
            set;
        }

        public bool ShowWeek { get; set; }

        public DatePickerGridCell (IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService>();
        }

        public static DatePickerGridCell Create ()
        {
            return (DatePickerGridCell)Nib.Instantiate (null, null)[0];
        }
            
        public override void OnBind ()
        {
            base.OnBind ();

            Layer.BorderColor = UIColor.DarkGray.CGColor;
            Layer.BorderWidth = 1f;
        }

        public override void OnShow ()
        {
            base.OnShow ();

            BackgroundColor = new UIColor (1f, 1f, 1f, 1.0f);

            Model = (DatePickerModel)DataContext;

            Week.Week = Model.Date;
            Week.SetLabel (string.Format (m_localiser.Get ("week_format"), Model.WeekNumber, Model.Date.ToString (m_localiser.Get("dd-MM-yyyy"))));
            Monday.SetLabel(Model.Date, Model.IsWeekSelected, Model.SelectedDate);
            Tuesday.SetLabel(Model.Date.AddDays(1), Model.IsWeekSelected, Model.SelectedDate);
            Wednesday.SetLabel(Model.Date.AddDays(2), Model.IsWeekSelected, Model.SelectedDate);
            Thursday.SetLabel(Model.Date.AddDays(3), Model.IsWeekSelected, Model.SelectedDate);
            Friday.SetLabel(Model.Date.AddDays(4), Model.IsWeekSelected, Model.SelectedDate);
            Saturday.SetLabel(Model.Date.AddDays(5), Model.IsWeekSelected, Model.SelectedDate);
            Sunday.SetLabel(Model.Date.AddDays(6), Model.IsWeekSelected, Model.SelectedDate);

            Week.OnSelected += OnItemTapped;
            Monday.OnSelected += OnItemTapped;
            Tuesday.OnSelected += OnItemTapped;
            Wednesday.OnSelected += OnItemTapped;
            Thursday.OnSelected += OnItemTapped;
            Friday.OnSelected += OnItemTapped;
            Saturday.OnSelected += OnItemTapped;
            Sunday.OnSelected += OnItemTapped;

            if(Model.IsCurrentWeek)
            {
                Week.Layer.BackgroundColor =  new CGColor(0.847f, 0.7490f, 0.847f, 0.5f);
            }

            Layer.BorderWidth = 0;
        }

        public override void OnHide ()
        {
            base.OnHide ();
            Week.Layer.BackgroundColor = new CGColor (1,1,1,1);
            Week.OnSelected -= OnItemTapped;
            Monday.OnSelected -= OnItemTapped;
            Tuesday.OnSelected -= OnItemTapped;
            Wednesday.OnSelected -= OnItemTapped;
            Thursday.OnSelected -= OnItemTapped;
            Friday.OnSelected -= OnItemTapped;
            Saturday.OnSelected -= OnItemTapped;
            Sunday.OnSelected -= OnItemTapped;
        }

        private void OnItemTapped(object sender, EventArgs e)
        {
            DateTime value = Model.Date;
            bool weekSelected = false;

            try
            {
                DatePickerWeekView weekView = sender as DatePickerWeekView;
                value = weekView.Week;
                weekSelected = true;
            }
            catch(NullReferenceException ex)// defensive programming = bad
            {
                DatePickerDayView dayView = sender as DatePickerDayView;
                value = dayView.Date;
            }

            if (CustomCommand1 != null)
                CustomCommand1.Execute(new SelectedPickerItem(){ Date = value, IsWeek = weekSelected});
        }
    }
}

