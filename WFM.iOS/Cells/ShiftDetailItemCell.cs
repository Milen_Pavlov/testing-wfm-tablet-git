﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class ShiftDetailItemCell : TableCell
    {
        public static readonly NSString Key = new NSString ("ShiftDetailItemCell");
        public static readonly UINib Nib;

        static ShiftDetailItemCell()
        {
            Nib = UINib.FromName (IsPhone ? "ShiftDetailItemCell_iPhone" : "ShiftDetailItemCell_iPad", NSBundle.MainBundle);
        }

        public ShiftDetailItemCell(IntPtr handle) : base (handle)
        {
        }

        public string TypeText
        {
            get { return string.Empty; }
            set 
            { 
                TypeButton.SetTitle (value, UIControlState.Normal);
                TypeButton.SetTitle (value, UIControlState.Disabled); 
            }
        }

        public string TimeText
        {
            get { return string.Empty; }
            set 
            { 
                TimeButton.SetTitle (value, UIControlState.Normal); 
                TimeButton.SetTitle (value, UIControlState.Disabled); 
            }
        }

        public override void OnBind()
        {
            TypeButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            TimeButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Right;

            TypeButton.SetTitleColor (UIColor.DarkGray, UIControlState.Disabled);
            TimeButton.SetTitleColor (UIColor.DarkGray, UIControlState.Disabled);

            var set = this.CreateBindingSet<ShiftDetailItemCell, ShiftItem>();
            set.Bind (this).For (m => m.TypeText).To (b => b.Title);
            set.Bind (this).For (m => m.TimeText).To (b => b.Time);
            set.Bind (TypeButton).To ("OnJobEdit");
            set.Bind (TypeButton).For(b => b.Enabled).To (m => m.CanEdit);
            set.Bind (TimeButton).To ("OnTimeEdit");
            set.Bind (TimeButton).For(b => b.Enabled).To (m => m.CanEdit);
            set.Bind (DeleteButton).To ("OnDelete");
            set.Bind (DeleteButton).For(b => b.Hidden).To (m => m.CanDelete).WithConversion(new HiddenConverter(), null);
            set.Apply();
        }
    }
}
