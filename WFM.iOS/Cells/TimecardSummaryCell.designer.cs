// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimecardSummaryCell")]
	partial class TimecardSummaryCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AdjustmentsLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ExceptionsLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NetHoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OtherHoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ScheduledLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] Separators { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalHoursLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
		}
	}
}
