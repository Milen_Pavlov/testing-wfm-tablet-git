// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("AccrualCell")]
	partial class AccrualCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AvailableLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TypeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (TypeLabel != null) {
				TypeLabel.Dispose ();
				TypeLabel = null;
			}

			if (AvailableLabel != null) {
				AvailableLabel.Dispose ();
				AvailableLabel = null;
			}
		}
	}
}
