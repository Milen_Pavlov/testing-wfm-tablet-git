﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ApprovalCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("ApprovalCell");
        public static readonly UINib Nib;

        private readonly IStringService m_localiser;

        private ApprovalStatusStringConverter m_approvalStatusConverter;

        static ApprovalCell()
        {
            Nib = UINib.FromName (IsPhone ? "ApprovalCell_iPhone" : "ApprovalCell_iPad", NSBundle.MainBundle);
        }

        public ApprovalCell(IntPtr handle) : base (handle)
        {
            m_approvalStatusConverter = new ApprovalStatusStringConverter ();

            SelectionStyle = UITableViewCellSelectionStyle.None;
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnShow()
        {
            base.OnShow ();

            IApprovalData data = DataContext as IApprovalData;
            if (data != null)
            {
                DateLabel.Text = data.StartDate.ToString (m_localiser.Get("dd/MM/yyyy"));
                NamesLabel.Text = data.Names;
                TypeLabel.Text = data.TypeString;
                StatusLabel.Text = (string)m_approvalStatusConverter.Convert (data.Status, typeof (ApprovalStatus), null, System.Globalization.CultureInfo.CurrentCulture);
            }
        }
    }
}

