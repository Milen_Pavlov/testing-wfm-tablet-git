// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("DailyOverviewItemCell")]
	partial class DailyOverviewItemCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel ActualDuration { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ActualEnd { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ActualStart { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CellButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] DailyCellsToMove { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] DailylyCellsToHide { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Date { get; set; }

		[Outlet]
		public WFM.iOS.ExceptionDataTableControl Exceptions { get; set; }

		[Outlet]
		public WFM.iOS.JobDataTableControl Jobs { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Name { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PlannedDuration { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PlannedEnd { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PlannedStart { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] Separators { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Status { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] WeeklyCellsToHide { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView[] WeeklyCellsToMove { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ActualDuration != null) {
				ActualDuration.Dispose ();
				ActualDuration = null;
			}

			if (ActualEnd != null) {
				ActualEnd.Dispose ();
				ActualEnd = null;
			}

			if (ActualStart != null) {
				ActualStart.Dispose ();
				ActualStart = null;
			}

			if (CellButton != null) {
				CellButton.Dispose ();
				CellButton = null;
			}

			if (Date != null) {
				Date.Dispose ();
				Date = null;
			}

			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}

			if (PlannedDuration != null) {
				PlannedDuration.Dispose ();
				PlannedDuration = null;
			}

			if (PlannedEnd != null) {
				PlannedEnd.Dispose ();
				PlannedEnd = null;
			}

			if (PlannedStart != null) {
				PlannedStart.Dispose ();
				PlannedStart = null;
			}

			if (Status != null) {
				Status.Dispose ();
				Status = null;
			}

			if (Exceptions != null) {
				Exceptions.Dispose ();
				Exceptions = null;
			}

			if (Jobs != null) {
				Jobs.Dispose ();
				Jobs = null;
			}
		}
	}
}
