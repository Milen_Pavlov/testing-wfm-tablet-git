using System;
using Consortium.Client.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class EditShiftPunchCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("EditShiftPunchCell");
        public static readonly UINib Nib = UINib.FromName ("EditShiftPunchCell", NSBundle.MainBundle);

        public IStringService Localiser { get; set; }

        private bool m_isStart;
        public bool IsStart
        {
            get { return m_isStart; }
            set { m_isStart = value; SetPunchCodeText (); }
        }

        private PunchCodes m_punchCode = PunchCodes.Unknown;
        public PunchCodes PunchCode
        {
            get { return m_punchCode; }
            set { m_punchCode = value; SetPunchCodeText ();}
        }

        public string JobDescription
        {
            get { return String.Empty; }
            set { SetButtonText (Job, value); }
        }

        private string m_actualText;
        public string ActualText
        {
            get { return m_actualText; }
            set { m_actualText = value; UpdateActualText (); }
        }

        private bool m_canEdit;
        public bool CanEdit
        {
            get { return m_canEdit; }
            set { m_canEdit = value;  UpdateActualText (); }
        }

        public EditShiftPunchCell (IntPtr handle) : base (handle)
        {
            Localiser = Mvx.Resolve<IStringService>();
        }

        public override void OnBind ()
        {
            SetupItem (PunchType);
            SetupItem (Job);
            SetupItem (Scheduled);
            SetupItem (Actual);
            SetupButton (Match);

            Job.TouchUpInside += OnJobPressed;
            Actual.TouchUpInside += OnActualPressed;
            Match.TouchUpInside += OnMatchOrDeletePressed;
            Delete.TouchUpInside += OnMatchOrDeletePressed;

            var set = this.CreateBindingSet<EditShiftPunchCell, EditShiftPunchData>();
            set.Bind (this).For(c => c.IsStart).To (vm => vm.IsStart);
            set.Bind (this).For(c => c.PunchCode).To (vm => vm.PunchCode);
            set.Bind (this).For (c => c.JobDescription).To (vm => vm.Description);
            set.Bind (Scheduled).To (vm => vm.Scheduled).WithConversion (new NullableDateTimeTimeToStringConverter());
            set.Bind (this).For(c => c.ActualText).To (vm => vm.Actual).WithConversion (new NullableDateTimeTimeToStringConverter());
            set.Bind (Match).For (b => b.Hidden).To (vm => vm.CanMatch).WithConversion (new InvertedBooleanConverter ());
            set.Bind (this).For (c => c.CanEdit).To (vm => vm.CanEdit);
            set.Bind (Delete).For (b => b.Hidden).To (vm => vm.CanDelete).WithConversion (new InvertedBooleanConverter ());
            set.Apply();
        }

        private void SetupItem(UIView item)
        {
            item.Layer.BorderWidth = 1;
            item.Layer.BorderColor = UIColor.LightGray.CGColor;
        }

        private void SetupButton(UIButton button)
        {
            var settings = Mvx.Resolve<SettingsService> ();
            button.Layer.CornerRadius = 4;
            button.BackgroundColor = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            button.SetTitleColor (UIColor.White, UIControlState.Normal);
        }

        private void SetButtonText(UIButton button, string text)
        {
            button.SetTitle (text, UIControlState.Normal);
            button.SetTitle (text, UIControlState.Highlighted);
        }

        private void SetPunchCodeText()
        {
            switch (m_punchCode)
            {
                case PunchCodes.Shift:
                    if(m_isStart)
                        PunchType.Text = Localiser.Get("shift_start_label");
                    else
                        PunchType.Text = Localiser.Get("shift_end_label");
                    break;
                case PunchCodes.Job:
                    PunchType.Text = Localiser.Get("job_change_label");
                    break;
                case PunchCodes.Break:
                    if(m_isStart)
                        PunchType.Text = Localiser.Get("break_start_label");
                    else
                        PunchType.Text = Localiser.Get("break_end_label");
                    break;
                case PunchCodes.Meal:
                    if(m_isStart)
                        PunchType.Text = Localiser.Get("meal_start_label");
                    else
                        PunchType.Text = Localiser.Get("meal_end_label");
                    break;
                case PunchCodes.Unknown:
                default:
                    PunchType.Text = String.Empty;
                    break;
            }
        }

        public void OnJobPressed(object sender, EventArgs e)
        {
            // Don't allow job selection for breaks or meals.
            if (m_punchCode == PunchCodes.Break || m_punchCode == PunchCodes.Meal || CanEdit == false)
                return;
            
            if (CustomCommand1 != null)
            {
                CustomCommand1.Execute (DataContext as EditShiftPunchData);
            }
        }

        public void OnActualPressed(object sender, EventArgs e)
        {
            if (CanEdit == false)
                return;
            
            if (CustomCommand2 != null)
            {
                CustomCommand2.Execute (DataContext as EditShiftPunchData);
            }
        }

        public void OnMatchOrDeletePressed(object sender, EventArgs e)
        {
            if (CanEdit == false)
                return;
            
            if (CustomCommand3 != null)
            {
                CustomCommand3.Execute (DataContext as EditShiftPunchData);
            }
        }

        private void UpdateActualText()
        {
            if (CanEdit || PunchCode == PunchCodes.Shift || PunchCode == PunchCodes.Job)
            {
                SetButtonText (Actual, m_actualText); 
            }
            else
            {
                SetButtonText (Actual, @"N/A");
            }
        }
    }
}
