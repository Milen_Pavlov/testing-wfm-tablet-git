﻿using System;
using System.Globalization;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class DrawerCell : TableCell
    {
        public static readonly NSString Key = new NSString ("DrawerCell");
        public static readonly UINib Nib = UINib.FromName ("DrawerCell", NSBundle.MainBundle);

        public UIColor m_selectColor;
        public UIImage m_select;
        public UIImage m_icon;

        public DrawerCell(IntPtr handle) : base (handle)
        {
            if (m_selectColor == null)
            {
                var settings = Mvx.Resolve<SettingsService> ();
                m_selectColor = ColorUtility.FromHexString (settings.AppTheme.HighlightColor);
            }
        }

        public override void OnShow()
        {
            base.OnShow ();

            var vm = DataContext as DrawerItem;

            m_icon = new ImageConverter ().Convert (vm.Image, typeof(UIImage), null, CultureInfo.CurrentCulture) as UIImage;
            m_select = new ImageInvConverter ().Convert (vm.Image, typeof(UIImage), null, CultureInfo.CurrentCulture) as UIImage;

            Name.Text = vm.Name;
            Name.TextColor = (Selected || Highlighted) ? UIColor.White : UIColor.Black;
            Icon.Image = (Selected || Highlighted) ? m_select : m_icon;

            Accessory = vm.Indicator ? UITableViewCellAccessory.DisclosureIndicator : UITableViewCellAccessory.None;

            SelectedBackgroundView = new UIView(Frame);
            SelectedBackgroundView.BackgroundColor = m_selectColor;

            NotificationLabel.Layer.CornerRadius = NotificationLabel.Frame.Width * 0.5f;
            NotificationLabel.Text = vm.Notification;
            NotificationLabel.Hidden = string.IsNullOrEmpty(vm.Notification);

        }

        public override void SetHighlighted (bool highlighted, bool animated)
        {
            base.SetHighlighted (highlighted, animated);

            NotificationLabel.BackgroundColor = UIColor.Red;
            Name.TextColor = (highlighted || Selected) ? UIColor.White : UIColor.Black;
            Icon.Image = (highlighted || Selected) ? m_select : m_icon;
        }

        public override void SetSelected(bool selected, bool animated)
        {
            base.SetSelected (selected, animated);

            NotificationLabel.BackgroundColor = UIColor.Red;
            Name.TextColor = (selected || Highlighted) ? UIColor.White : UIColor.Black;
            Icon.Image = (selected || Highlighted) ? m_select : m_icon;
        }
    }
}
