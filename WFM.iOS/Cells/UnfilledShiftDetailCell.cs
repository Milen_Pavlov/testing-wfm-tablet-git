﻿using System;
using System.Linq;
using Cirrious.CrossCore;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class UnfilledShiftDetailCell : TableCell
    {
        public static readonly NSString Key = new NSString ("UnfilledShiftDetailCell");
        public static readonly UINib Nib = UINib.FromName ("UnfilledShiftDetailCell", NSBundle.MainBundle);

        private readonly IESSConfigurationService m_configurationService;
        private readonly IStringService m_localizer;
        private readonly ITimeFormatService m_timeFormatService;

        public UnfilledShiftDetailCell (IntPtr handle) : base (handle)
        {
            m_configurationService = Mvx.Resolve<IESSConfigurationService>();
            m_localizer = Mvx.Resolve<IStringService>();
            m_timeFormatService = Mvx.Resolve<ITimeFormatService>();
        }

        public override void OnShow ()
        {
            var data = DataContext as JDAShift;


            DateLabel.Text = data.Start.ToString (m_localizer.Get("ddd dd/MM"));
            StartLabel.Text =  m_timeFormatService.FormatTime(data.Start);
            HoursLabel.Text = (data.End - data.Start).TotalHours.ToString ();
            JobLabel.Text = data.Jobs.FirstOrDefault () == null ? string.Empty : data.Jobs.FirstOrDefault ().JobName;
            DeleteShiftLabel.Enabled = !m_configurationService.IsScheduleEditorReadOnlyForUserRole;
            DeleteShiftLabel.TouchUpInside += OnDeleteButtonPressed;
        }

        public override void OnHide ()
        {
            base.OnHide ();
            DeleteShiftLabel.TouchUpInside -= OnDeleteButtonPressed;
        }

        private void OnDeleteButtonPressed (object sender, EventArgs e)
        {
            if (CustomCommand1 != null) 
            {
                CustomCommand1.Execute (DataContext);
            }
        }

        private void OnUnfilledShiftSelected (object sender, EventArgs e)
        {
            if (CustomCommand2 != null) 
            {
                CustomCommand2.Execute (DataContext);
            }
        }
    }
}
