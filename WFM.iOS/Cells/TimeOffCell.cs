﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public partial class TimeOffCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString("TimeOffCell");
        public static readonly UINib Nib;

        static TimeOffCell()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                Nib = UINib.FromName("TimeOffCell_iPhone", NSBundle.MainBundle);
            else
                Nib = UINib.FromName("TimeOffCell_iPad", NSBundle.MainBundle);
        }

        public TimeOffCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnBind()
        {
            var set = this.CreateBindingSet<TimeOffCell, TimeOffComment>();
            set.Bind(StatusText).To(m => m);
            set.Apply();
        }            
    }
}