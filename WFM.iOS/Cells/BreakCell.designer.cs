// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("BreakCell")]
	partial class BreakCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel StartTimeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DurationLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EmployeeNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DepartmentLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (StartTimeLabel != null) {
				StartTimeLabel.Dispose ();
				StartTimeLabel = null;
			}

			if (DurationLabel != null) {
				DurationLabel.Dispose ();
				DurationLabel = null;
			}

			if (EmployeeNameLabel != null) {
				EmployeeNameLabel.Dispose ();
				EmployeeNameLabel = null;
			}

			if (DepartmentLabel != null) {
				DepartmentLabel.Dispose ();
				DepartmentLabel = null;
			}
		}
	}
}
