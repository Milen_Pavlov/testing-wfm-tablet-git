// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("EnvironmentCell")]
	partial class EnvironmentCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView AddIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel AddressLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView DeleteIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AddressLabel != null) {
				AddressLabel.Dispose ();
				AddressLabel = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (DeleteIcon != null) {
				DeleteIcon.Dispose ();
				DeleteIcon = null;
			}

			if (AddIcon != null) {
				AddIcon.Dispose ();
				AddIcon = null;
			}

			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}
		}
	}
}
