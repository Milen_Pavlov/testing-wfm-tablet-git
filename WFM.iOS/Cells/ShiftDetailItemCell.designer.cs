// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ShiftDetailItemCell")]
	partial class ShiftDetailItemCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TimeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TypeButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}

			if (TimeButton != null) {
				TimeButton.Dispose ();
				TimeButton = null;
			}

			if (TypeButton != null) {
				TypeButton.Dispose ();
				TypeButton = null;
			}
		}
	}
}
