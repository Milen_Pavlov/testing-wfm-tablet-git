﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using MonoTouch.CoreGraphics;

namespace WFM.iOS
{
    public partial class ScheduleSummarySectionCell : AccordianMultiTableHeaderCell
    {
        public static readonly NSString Key = new NSString ("ScheduleSummarySectionCell");
        public static readonly UINib Nib;

        private IStringService m_localiser;

        static ScheduleSummarySectionCell()
        {
            Nib = UINib.FromName (IsPhone ? "ScheduleSummarySectionCell_iPhone" : "ScheduleSummarySectionCell_iPad", NSBundle.MainBundle);
        }

        public ScheduleSummarySectionCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnShow()
        {
            UpdateArrow ();

            JDAScheduleSummaryGroup data = this.DataContext as JDAScheduleSummaryGroup;
            if (data != null)
            {
                TitleLabel.Text = data.Name;

                SalesForecastLabel.Text = data.SalesForecast.ToString("0.00");
                LabourHoursLabel.Text = data.LabourHours.ToString("0.00");
                ScheduledHoursLabel.Text = data.ScheduledHours.ToString("0.00");
                CoverageLabel.Text = string.Format("{0}%", data.Coverage.ToString("0.00"));
                AccuracyLabel.Text = string.Format("{0}%", data.Accuracy.ToString("0.00"));
                LabourCostLabel.Text = string.Format("£{0}", data.LabourCost.ToString("0.00"));
                ScheduledCostLabel.Text = string.Format("£{0}", data.ScheduledCost.ToString("0.00"));
                CostVarianceLabel.Text = string.Format("{0}%", data.CostVariance.ToString("0.00"));
            }
        }

        public override void SetSelected (bool selected, bool animated)
        {
            if (Separator != null)
            {
                UIColor col = Separator.BackgroundColor;
                base.SetSelected (selected, animated);
                Separator.BackgroundColor = col;
            }
            else
            {
                base.SetSelected (selected, animated);
            }
        }

        public override void SetHighlighted (bool highlighted, bool animated)
        {
            if (Separator != null)
            {
                UIColor col = Separator.BackgroundColor;
                base.SetHighlighted (highlighted, animated);
                Separator.BackgroundColor = col;
            }
            else
            {
                base.SetHighlighted (highlighted, animated);
            }
        }

        public override void OnCollapsed ()
        {
            base.OnCollapsed ();
            UpdateArrow ();
        }

        public override void OnExpanded ()
        {
            base.OnExpanded ();
            UpdateArrow ();
        }

        private void UpdateArrow()
        {
            CollapseArrow.Transform = CGAffineTransform.MakeRotation ((IsExpanded) ? 0 : (float)Math.PI);
        }
    }
}

