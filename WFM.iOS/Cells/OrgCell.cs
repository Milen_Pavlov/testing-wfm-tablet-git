﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class OrgCell : TableCell
    {
        public static readonly NSString Key = new NSString ("OrgCell");
        public static readonly UINib Nib = UINib.FromName ("OrgCell", NSBundle.MainBundle);

        public UIColor m_selectColor;

        public OrgCell(IntPtr handle) : base (handle)
        {
            if (m_selectColor == null)
            {
                var settings = Mvx.Resolve<SettingsService> ();
                m_selectColor = ColorUtility.FromHexString (settings.AppTheme.HighlightColor);
            }
        }

        public override void OnBind()
        {
            var set = this.CreateBindingSet<OrgCell, JDAOrgUnit>();
            set.Bind(Name).To(b => b.Name);
            set.Apply();

            SelectedBackgroundView = new UIView(Frame);
            SelectedBackgroundView.BackgroundColor = m_selectColor;
        }

        public override void SetSelected(bool selected, bool animated)
        {
            base.SetSelected (selected, animated);

            if (selected)
            {
                Name.TextColor = UIColor.White;
            }
            else
            {
                Name.TextColor = UIColor.Black;
            }
        }
    }
}
    