﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class ScheduleSummaryDayEntryCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("ScheduleSummaryDayEntryCell");
        public static readonly UINib Nib;

        private IStringService m_localiser;

        static ScheduleSummaryDayEntryCell()
        {
            Nib = UINib.FromName (IsPhone ? "ScheduleSummaryDayEntryCell_iPhone" : "ScheduleSummaryDayEntryCell_iPad", NSBundle.MainBundle);
        }

        public ScheduleSummaryDayEntryCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
            SelectionStyle = UITableViewCellSelectionStyle.None;
        }

        public override void OnShow()
        {
            JDAScheduleSummaryGroupDayEntry data = this.DataContext as JDAScheduleSummaryGroupDayEntry;
            if (data != null)
            {
                TitleLabel.Text = data.Title;

                SalesForecastLabel.Hidden = !data.SalesForecast.HasValue;
                if(data.SalesForecast.HasValue)
                    SalesForecastLabel.Text = data.SalesForecast.Value.ToString("0.00");


                LabourHoursLabel.Text = data.LabourHours.ToString("0.00");
                ScheduledHoursLabel.Text = data.ScheduledHours.ToString("0.00");
                CoverageLabel.Text = string.Format("{0}%", data.Coverage.ToString("0.00"));
                AccuracyLabel.Text = string.Format("{0}%", data.Accuracy.ToString("0.00"));
                LabourCostLabel.Text = string.Format("£{0}", data.LabourCost.ToString("0.00"));
                ScheduledCostLabel.Text = string.Format("£{0}", data.ScheduledCost.ToString("0.00"));
                CostVarianceLabel.Text = string.Format("{0}%", data.CostVariance.ToString("0.00"));

                //Medium or Regular
                var font = data.IsTotal ? UIFont.FromName("AvenirNext-DemiBold", 14) : UIFont.FromName("AvenirNext-Regular", 14);
                TitleLabel.Font = font;
                SalesForecastLabel.Font = font;
                LabourHoursLabel.Font = font;
                ScheduledHoursLabel.Font = font;
                CoverageLabel.Font = font;
                AccuracyLabel.Font = font;
                LabourCostLabel.Font = font;
                ScheduledCostLabel.Font = font;
                CostVarianceLabel.Font = font;
            }
        }

        public override void SetSelected (bool selected, bool animated)
        {
            if (Separator != null)
            {
                UIColor col = Separator.BackgroundColor;
                base.SetSelected (selected, animated);
                Separator.BackgroundColor = col;
            }
            else
            {
                base.SetSelected (selected, animated);
            }
        }

        public override void SetHighlighted (bool highlighted, bool animated)
        {
            if (Separator != null)
            {
                UIColor col = Separator.BackgroundColor;
                base.SetHighlighted (highlighted, animated);
                Separator.BackgroundColor = col;
            }
            else
            {
                base.SetHighlighted (highlighted, animated);
            }
        }
    }
}

