﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreAnimation;

namespace WFM.iOS
{
    public partial class PlanningDayCell : TableCell
    {
        public const int RowHeight = 20;

        public static readonly NSString Key = new NSString ("PlanningDayCell");
        public static readonly UINib Nib;
        private UIView m_dayView;

        static PlanningDayCell()
        {
            Nib = UINib.FromName (IsPhone ? "PlanningDayCell_iPhone" : "PlanningDayCell_iPad", NSBundle.MainBundle);
        }

        public PlanningDayCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            PlanningData data = DataContext as PlanningData;
            GroupLabel.Text = data.Name;

            int start;
            int length;
            int height;
            double step;

            // Day or week
            if (data.DayIndex.HasValue)
            {
                start = data.DayIndex.Value * ScheduleTime.DayIntervals;
                length = ScheduleTime.DayIntervals;
                step = PlanningArea.Frame.Width / ScheduleTime.DayIntervals;
                height = (int)data.DayMax [data.DayIndex.Value];
            }
            else
            {
                start = 0;
                length = ScheduleTime.WeekIntervals;
                step = PlanningArea.Frame.Width / ScheduleTime.WeekIntervals;
                height = (int)data.WeekMax;
            }

            // create holding view 
            if (m_dayView != null)
            {
                m_dayView.RemoveFromSuperview ();
                m_dayView.Dispose ();
            }

            m_dayView = new UIView(new RectangleF(0,0,PlanningArea.Frame.Width, PlanningArea.Frame.Height));

            // Draw Overs & Unders )2 pass)
            DrawCoverage (data.Variance, start, length, step, 1, ColorUtility.FromHexString ("0xFFB3B3").CGColor);
            DrawCoverage (data.Variance, start, length, step, -1, ColorUtility.FromHexString ("0xFFE281").CGColor);

            // Vertical lines
            if (data.DayIndex.HasValue)
            {
                for (int i = 1; i < ScheduleTime.DayIntervals; ++i)
                {
                    var line = new UIView (new RectangleF ((float)(i * step), 0, 1, Frame.Height));
                    line.BackgroundColor = UIColor.LightGray;
                    m_dayView.AddSubview (line);
                }
            }
            else
            {
                double dayStep = PlanningArea.Frame.Width / 7;

                for (int i = 1; i < 7; ++i)
                {
                    var line = new UIView (new RectangleF ((float)(i * dayStep), 0, 1, Frame.Height));
                    line.BackgroundColor = UIColor.LightGray;
                    m_dayView.AddSubview (line);
                }
            }

            // Horizontal lines
            double verticalStep = Frame.Height / height;

            for (int i = 1; i < height; ++i)
            {
                var line = new UIView (new RectangleF (0, (float)(i * verticalStep), Frame.Width, 1));
                line.BackgroundColor = UIColor.LightGray;
                m_dayView.AddSubview (line);
            }


            PlanningArea.AddSubview (m_dayView);
            PlanningArea.SendSubviewToBack(m_dayView);
        }

        private void DrawCoverage(double[] values, int offset, int length, double width, int mod, CGColor col)
        {
            int pos = 0;
            int end = pos + length;
            float lastVar = 0;
            float nextVar = 0;

            var over = new CGPath ();
            over.MoveToPoint (0, 0);

            while (pos < end)
            {
                while (pos < end)
                {
                    nextVar = Math.Max(0, (float)(values [pos + offset]*mod));

                    if (nextVar != lastVar)
                        break;

                    ++pos;
                }

                float x = (float)(pos * width);
                over.AddLineToPoint (x, lastVar * RowHeight);
                over.AddLineToPoint (x, nextVar * RowHeight);
                lastVar = nextVar;
            }

            over.AddLineToPoint ((float)(pos * width), 0);
            over.AddLineToPoint (0, 0);
            over.CloseSubpath ();

            CAShapeLayer overLayer = new CAShapeLayer ();
            overLayer.Path = over;
            overLayer.FillColor = col;
            overLayer.Position = PointF.Empty;
            m_dayView.Layer.AddSublayer (overLayer);
        }
    }
}

