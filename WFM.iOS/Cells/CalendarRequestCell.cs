﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
	public partial class CalendarRequestCell : CollectionCell
    {
        public static readonly NSString Key = new NSString ("CalendarRequestCell");
        public static readonly UINib Nib;

		public static UIColor ApprovedBG = new UIColor (0.22f, 0.71f, 0.29f, 1.0f);
		public static UIColor PendingBG = new UIColor (0.99f, 0.9f, 0.1f, 1.0f);
		public static UIColor DeniedBG = new UIColor (0.93f, 0.15f, 0.15f, 1.0f);

        private IStringService m_localiser;

		static CalendarRequestCell()
        {
            Nib = UINib.FromName (IsPhone ? "CalendarRequestCell_iPhone" : "CalendarRequestCell_iPad", NSBundle.MainBundle);
        }

		public CalendarRequestCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
		}

		public override void OnBind ()
		{
			base.OnBind ();

			Layer.BorderColor = UIColor.DarkGray.CGColor;
			Layer.BorderWidth = 1f;
		}

        public override void OnSelected ()
        {
            base.OnSelected ();

            //Display popover
            if (CustomAction1 != null)
                CustomAction1.Execute (this);
        }

		public override void OnShow ()
		{
			base.OnShow ();

            CalendarDay cd = DataContext as CalendarDay;

			if (cd != null) 
			{
				DateLabel.Text = cd.DateString;
				DateLabel.TextColor = (cd.IsInMonth) ? UIColor.Black : UIColor.DarkGray;

				BackgroundColor = (cd.IsInMonth) ? UIColor.White : UIColor.LightGray;
				if (cd.IsToday) 
				{
					BackgroundColor = new UIColor (1.0f, 0.96f, 0.75f, 1.0f);
					DateLabel.TextColor = new UIColor (0.63f, 0.58f, 0.34f, 1.0f);
				}

				if (StatusLabels != null && StatusLabels.Length > 0)
				{
					foreach (UILabel label in StatusLabels)
						label.Hidden = true;

					TimeOffDayData dataContext = cd.DataContext as TimeOffDayData;
                    if (dataContext != null && dataContext.Data != null && dataContext.Data.Count > 0) 
					{
                        float y = DateLabel.Frame.Bottom;

                        //Get All Approved, then Pending, then Denied
                        var approved = dataContext.Data.Where(x => x.Status == TimeOffStatus.Approved);
                        var pending = dataContext.Data.Where(x => x.Status == TimeOffStatus.Pending);
                        var denied = dataContext.Data.Where(x => x.Status == TimeOffStatus.Denied);

                        int total = 0;
                        if (approved.Count<JDATimeOffRequest> () > 0)
                            total++;
                        if (pending.Count<JDATimeOffRequest> () > 0)
                            total++;
                        if (denied.Count<JDATimeOffRequest> () > 0)
                            total++;

                        float labelHeight = (Frame.Height - DateLabel.Frame.Height) / total;

                        var combined = new List<IEnumerable<JDATimeOffRequest>> (){ approved, pending, denied };

                        for (int i = 0; i < combined.Count; i++)
                        {
                            TimeOffStatus status = TimeOffStatus.Approved;
                            int count = combined[i].Count<JDATimeOffRequest> ();

                            StatusLabels[i].Hidden = count == 0;

                            switch (i) 
                            {
                                case 0:
                                    status = TimeOffStatus.Approved;
                                    StatusLabels [i].BackgroundColor = ApprovedBG;
                                    StatusLabels [i].TextColor = UIColor.White;
                                    StatusLabels [i].Text = string.Format ("  {0} {1}", count, m_localiser.Get("approvalstatus_managerapproved"));
                                    break;
                                case 1:
                                    status = TimeOffStatus.Pending;
                                    StatusLabels [i].BackgroundColor = PendingBG;
                                    StatusLabels [i].TextColor = UIColor.Black;
                                    StatusLabels [i].Text = string.Format ("  {0} {1}", count, m_localiser.Get("approvalstatus_pending"));
                                    break;
                                case 2:
                                    status = TimeOffStatus.Denied;
                                    StatusLabels [i].BackgroundColor = DeniedBG;
                                    StatusLabels [i].TextColor = UIColor.White;
                                    StatusLabels [i].Text = string.Format ("  {0} {1}", count, m_localiser.Get("approvalstatus_denied"));
                                    break;
                            }

                            StatusLabels [i].Frame = new RectangleF (StatusLabels [i].Frame.X, y, StatusLabels [i].Frame.Width, labelHeight);
                            StatusLabels [i].Alpha = (cd.IsInMonth) ? 1.0f : 0.25f;

                            y += (StatusLabels[i].Hidden) ? 0 : (float)Math.Floor(labelHeight);
                        }
					}
				}
			}
		}
    }
}

