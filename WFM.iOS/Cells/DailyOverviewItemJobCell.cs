﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class DailyOverviewItemJobCell : DynamicHeightCell
    {
        public static readonly float Border = 4;

        public static readonly NSString Key = new NSString ("DailyOverviewItemJobCell");
        public static readonly UINib Nib;

        static DailyOverviewItemJobCell()
        {
            Nib = UINib.FromName (IsPhone ? "DailyOverviewItemJobCell_iPhone" : "DailyOverviewItemJobCell_iPad", NSBundle.MainBundle);
        }

        public DailyOverviewItemJobCell(IntPtr handle) : base (handle)
        {
        }
         
        public override void OnBind ()
        {
            base.OnBind ();

            //Ensure its clear for background of container cell to appear
            BackgroundColor = UIColor.Clear;
            if (BackgroundView != null)
                BackgroundView.BackgroundColor = UIColor.Clear;

            if (Name != null)
                Name.BackgroundColor = UIColor.Clear;

            var set = this.CreateBindingSet<DailyOverviewItemJobCell, JobData>();
            set.Bind (Name).To (vm => vm.JobName);
            set.Apply();
        }

        public override float GetHeightResized ()
        {
            //Custom cell resize
            float heightBefore = Name.Frame.Height;
            Name.SizeToFit();
            float heightAfter = Name.Frame.Height;

            float diff = heightAfter - heightBefore;
            float newheight = Frame.Height + diff;

            //Add border (above+below combined)
            newheight += Border * 2;

            return newheight;
        }
    }
}

