﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class ShiftDetailHeaderCell : TableCell
    {
        public static readonly NSString Key = new NSString ("ShiftDetailHeaderCell");
        public static readonly UINib Nib;

        static ShiftDetailHeaderCell()
        {
            Nib = UINib.FromName (IsPhone ? "ShiftDetailHeaderCell_iPhone" : "ShiftDetailHeaderCell_iPad", NSBundle.MainBundle);
        }

        public ShiftDetailHeaderCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnBind()
        {
            TitleLabel.Text = "123";

            var set = this.CreateBindingSet<ShiftDetailHeaderCell, ShiftItem>();
            set.Bind (TitleLabel).To (m => m.Title);
            set.Apply();
        }
    }
}

