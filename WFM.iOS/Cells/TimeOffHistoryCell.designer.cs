// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffHistoryCell")]
	partial class TimeOffHistoryCell
	{
		[Outlet]
		Consortium.Client.iOS.WebImageControl Icon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SubTitle { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Title { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Icon != null) {
				Icon.Dispose ();
				Icon = null;
			}

			if (Title != null) {
				Title.Dispose ();
				Title = null;
			}

			if (SubTitle != null) {
				SubTitle.Dispose ();
				SubTitle = null;
			}
		}
	}
}
