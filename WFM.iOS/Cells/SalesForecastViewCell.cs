﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using WFM.Core;

namespace WFM.iOS
{
    public partial class SalesForecastViewCell : TableCell
    {
        public static readonly NSString Key = new NSString ("SalesForecastViewCell");
        public static readonly UINib Nib;
        private SalesForecastDataItem m_data;
        private static readonly string _FORMATSPECIFIER = "0,0.00";
        private static readonly UIColor m_unmodifiedColor = new UIColor(0, 0,0,1);
        private static readonly UIColor m_modifiedColor = new UIColor(1.0f, 0,0,1);

        private void ReloadRows()
        {
            if (m_data.WeekTotalModified)
            {
                Day1Value.Text = m_data.Days[0].StringDayValue;
                Day2Value.Text = m_data.Days[1].StringDayValue;
                Day3Value.Text = m_data.Days[2].StringDayValue;
                Day4Value.Text = m_data.Days[3].StringDayValue;
                Day5Value.Text = m_data.Days[4].StringDayValue;
                Day6Value.Text = m_data.Days[5].StringDayValue;
                Day7Value.Text = m_data.Days[6].StringDayValue;    
            }
            else
            {
                TotalValue.Text = m_data.WeekTotal.ToString (_FORMATSPECIFIER);
            }            

            // modified
            Day1Value.TextColor = m_data.Days[0].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day2Value.TextColor = m_data.Days[1].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day3Value.TextColor = m_data.Days[2].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day4Value.TextColor = m_data.Days[3].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day5Value.TextColor = m_data.Days[4].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day6Value.TextColor = m_data.Days[5].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day7Value.TextColor = m_data.Days[6].Modified ? m_modifiedColor : m_unmodifiedColor;
            TotalValue.TextColor = m_data.WeekTotalModified ? m_modifiedColor : m_unmodifiedColor;
        }

        static SalesForecastViewCell ()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                Nib = UINib.FromName ("SalesForecastViewCell_iPhone", NSBundle.MainBundle);
            else
                Nib = UINib.FromName ("SalesForecastViewCell_iPad", NSBundle.MainBundle);
        }

        public SalesForecastViewCell (IntPtr handle) : base (handle)
        {
        }

        public static SalesForecastViewCell Create ()
        {
            return (SalesForecastViewCell)Nib.Instantiate (null, null)[0];
        }

        public override void OnShow ()
        {
            base.OnShow ();

            m_data = DataContext as SalesForecastDataItem;
            Day1Value.TextColor = m_data.Days[0].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day2Value.TextColor = m_data.Days[1].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day3Value.TextColor = m_data.Days[2].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day4Value.TextColor = m_data.Days[3].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day5Value.TextColor = m_data.Days[4].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day6Value.TextColor = m_data.Days[5].Modified ? m_modifiedColor : m_unmodifiedColor;
            Day7Value.TextColor = m_data.Days[6].Modified ? m_modifiedColor : m_unmodifiedColor;
            TotalValue.TextColor = m_data.WeekTotalModified ? m_modifiedColor : m_unmodifiedColor;
            Day1Value.Text = m_data.Days[0].StringDayValue;
            Day2Value.Text = m_data.Days[1].StringDayValue;
            Day3Value.Text = m_data.Days[2].StringDayValue;
            Day4Value.Text = m_data.Days[3].StringDayValue;
            Day5Value.Text = m_data.Days[4].StringDayValue;
            Day6Value.Text = m_data.Days[5].StringDayValue;
            Day7Value.Text = m_data.Days[6].StringDayValue;
            TotalValue.Text = m_data.WeekTotal.ToString (_FORMATSPECIFIER);
            MetricName.Text = m_data.MetricName;
            Day1Value.Editable = m_data.IsEditable;
            Day2Value.Editable = m_data.IsEditable;
            Day3Value.Editable = m_data.IsEditable;
            Day4Value.Editable = m_data.IsEditable;
            Day5Value.Editable = m_data.IsEditable;
            Day6Value.Editable = m_data.IsEditable;
            Day7Value.Editable = m_data.IsEditable;
            TotalValue.Editable = m_data.IsEditable;

            Day1Value.Layer.BorderWidth = 0.5f;
            Day2Value.Layer.BorderWidth = 0.5f;
            Day3Value.Layer.BorderWidth = 0.5f;
            Day4Value.Layer.BorderWidth = 0.5f;
            Day5Value.Layer.BorderWidth = 0.5f;
            Day6Value.Layer.BorderWidth = 0.5f;
            Day7Value.Layer.BorderWidth = 0.5f;
            TotalValue.Layer.BorderWidth = 0.5f;

            Day1Value.ShouldChangeText += ShouldEditText;
            Day2Value.ShouldChangeText += ShouldEditText;
            Day3Value.ShouldChangeText += ShouldEditText;
            Day4Value.ShouldChangeText += ShouldEditText;
            Day5Value.ShouldChangeText += ShouldEditText;
            Day6Value.ShouldChangeText += ShouldEditText;
            Day7Value.ShouldChangeText += ShouldEditText;
            TotalValue.ShouldChangeText += ShouldEditText;

            Day1Value.Changed += OnEditTextEnd;
            Day2Value.Changed += OnEditTextEnd;
            Day3Value.Changed += OnEditTextEnd;
            Day4Value.Changed += OnEditTextEnd;
            Day5Value.Changed += OnEditTextEnd;
            Day6Value.Changed += OnEditTextEnd;
            Day7Value.Changed += OnEditTextEnd;
            TotalValue.Changed += OnEditTextEnd;
        }

        public override void OnHide ()
        {
            base.OnHide ();
            Day1Value.Changed -= OnEditTextEnd;
            Day2Value.Changed -= OnEditTextEnd;
            Day3Value.Changed -= OnEditTextEnd;
            Day4Value.Changed -= OnEditTextEnd;
            Day5Value.Changed -= OnEditTextEnd;
            Day6Value.Changed -= OnEditTextEnd;
            Day7Value.Changed -= OnEditTextEnd;
            TotalValue.Changed -= OnEditTextEnd;

            Day1Value.ShouldChangeText -= ShouldEditText;
            Day2Value.ShouldChangeText -= ShouldEditText;
            Day3Value.ShouldChangeText -= ShouldEditText;
            Day4Value.ShouldChangeText -= ShouldEditText;
            Day5Value.ShouldChangeText -= ShouldEditText;
            Day6Value.ShouldChangeText -= ShouldEditText;
            Day7Value.ShouldChangeText -= ShouldEditText;
            TotalValue.ShouldChangeText -= ShouldEditText;
        }  

        void OnEditTextEnd(object sender, EventArgs e)
        {
            m_data.WeekTotalModified = false;
            int index = -1;
            string value = "";

            if(sender == Day1Value)
            {
                index = 0;
                value = Day1Value.Text;
            }
            else if(sender == Day2Value)
            {
                index = 1;
                value = Day2Value.Text;
            }
            else if(sender == Day3Value)
            {
                index = 2;
                value = Day3Value.Text;
            }
            else if(sender == Day4Value)
            {
                index = 3;
                value = Day4Value.Text;
            }
            else if(sender == Day5Value)
            {
                index = 4;
                value = Day5Value.Text;
            }
            else if(sender == Day6Value)
            {
                index = 5;
                value = Day6Value.Text;
            }
            else if(sender == Day7Value)
            {
                index = 6;
                value = Day7Value.Text;
            }

            if(CustomCommand1 != null)
            {
                CustomCommand1.Execute (new{});
            }

            if (sender == TotalValue)
            {
                m_data.UpdateDaysForNewTotal (TotalValue.Text);
            }
            else
            {
                m_data.Days[index].SetDayValue (value); 
            }
            m_data.RecalculateTotal ();
            ReloadRows ();
        }

        bool ShouldEditText (UITextView textView, NSRange range, string text)
        {
            if(text.Equals("\n"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

