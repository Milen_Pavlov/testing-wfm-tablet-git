// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("DrawerCell")]
	partial class DrawerCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView Icon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Name { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NotificationLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Icon != null) {
				Icon.Dispose ();
				Icon = null;
			}

			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}

			if (NotificationLabel != null) {
				NotificationLabel.Dispose ();
				NotificationLabel = null;
			}
		}
	}
}
