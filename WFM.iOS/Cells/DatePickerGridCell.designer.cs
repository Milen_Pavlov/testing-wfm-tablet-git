// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("DatePickerGridCell")]
	partial class DatePickerGridCell
	{
		[Outlet]
		WFM.iOS.DatePickerDayView Friday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerDayView Monday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerDayView Saturday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerDayView Sunday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerDayView Thursday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerDayView Tuesday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerDayView Wednesday { get; set; }

		[Outlet]
		WFM.iOS.DatePickerWeekView Week { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Friday != null) {
				Friday.Dispose ();
				Friday = null;
			}

			if (Monday != null) {
				Monday.Dispose ();
				Monday = null;
			}

			if (Saturday != null) {
				Saturday.Dispose ();
				Saturday = null;
			}

			if (Sunday != null) {
				Sunday.Dispose ();
				Sunday = null;
			}

			if (Thursday != null) {
				Thursday.Dispose ();
				Thursday = null;
			}

			if (Tuesday != null) {
				Tuesday.Dispose ();
				Tuesday = null;
			}

			if (Wednesday != null) {
				Wednesday.Dispose ();
				Wednesday = null;
			}

			if (Week != null) {
				Week.Dispose ();
				Week = null;
			}
		}
	}
}
