﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Consortium.Client.iOS;
using Cirrious.MvvmCross.Binding.BindingContext;
using WFM.Core;

namespace WFM.iOS
{
    public partial class SalesForecastViewHeaderCell : TableCell
    {
        public static readonly NSString Key = new NSString ("SalesForecastViewHeaderCell");
        public static readonly UINib Nib;

        static SalesForecastViewHeaderCell ()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                Nib = UINib.FromName ("SalesForecastViewHeaderCell_iPhone", NSBundle.MainBundle);
            else
                Nib = UINib.FromName ("SalesForecastViewHeaderCell_iPad", NSBundle.MainBundle);
        }

        public SalesForecastViewHeaderCell (IntPtr handle) : base (handle)
        {
        }

        public static SalesForecastViewHeaderCell Create ()
        {
            return (SalesForecastViewHeaderCell)Nib.Instantiate (null, null)[0];
        }

        public override void OnShow ()
        {
            base.OnShow ();
            SalesForecastItem item = DataContext as SalesForecastItem;
            if(item != null)
            {
                NameLabel.Text = item.Title;
            }
        }

       
    }
}

