﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using WFM.Core;

namespace WFM.iOS
{
    [Register("ExceptionDataTableControl")]
    public class ExceptionDataTableControl : TableControl
    {
        DynamicCellHeightService m_cellHeightService;

        public ExceptionDataTableControl (IntPtr handle) 
            : base (handle)
        {
            Delegate = new DragTableControlDelegate(GetHeightForRow);

            m_cellHeightService = Mvx.Resolve<DynamicCellHeightService>();
        }

        public float GetHeightForRow(NSIndexPath indexPath)
        {
            var data = GetRow(indexPath) as TimecardException;
            float height = m_cellHeightService.GetHeightForData(data, DailyOverviewItemExceptionCell.Nib, data.Text);

            if(height > 0)
                return height;
            else
                return base.OnGetHeightForRow (indexPath);
        }
    }
}

