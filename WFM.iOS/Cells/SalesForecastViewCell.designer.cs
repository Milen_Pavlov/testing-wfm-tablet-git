// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("SalesForecastViewCell")]
	partial class SalesForecastViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UITextView Day1Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView Day2Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView Day3Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView Day4Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView Day5Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView Day6Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView Day7Value { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MetricName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView TotalValue { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MetricName != null) {
				MetricName.Dispose ();
				MetricName = null;
			}

			if (Day1Value != null) {
				Day1Value.Dispose ();
				Day1Value = null;
			}

			if (Day2Value != null) {
				Day2Value.Dispose ();
				Day2Value = null;
			}

			if (Day3Value != null) {
				Day3Value.Dispose ();
				Day3Value = null;
			}

			if (Day4Value != null) {
				Day4Value.Dispose ();
				Day4Value = null;
			}

			if (Day5Value != null) {
				Day5Value.Dispose ();
				Day5Value = null;
			}

			if (Day6Value != null) {
				Day6Value.Dispose ();
				Day6Value = null;
			}

			if (Day7Value != null) {
				Day7Value.Dispose ();
				Day7Value = null;
			}

			if (TotalValue != null) {
				TotalValue.Dispose ();
				TotalValue = null;
			}
		}
	}
}
