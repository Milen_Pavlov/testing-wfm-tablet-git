// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("PlanningDayCell")]
	partial class PlanningDayCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel GroupLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView PlanningArea { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (GroupLabel != null) {
				GroupLabel.Dispose ();
				GroupLabel = null;
			}

			if (PlanningArea != null) {
				PlanningArea.Dispose ();
				PlanningArea = null;
			}
		}
	}
}
