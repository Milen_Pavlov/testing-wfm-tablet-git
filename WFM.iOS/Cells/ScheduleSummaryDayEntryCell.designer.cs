// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ScheduleSummaryDayEntryCell")]
	partial class ScheduleSummaryDayEntryCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AccuracyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CostVarianceLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CoverageLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LabourCostLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LabourHoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SalesForecastLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ScheduledCostLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ScheduledHoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView Separator { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AccuracyLabel != null) {
				AccuracyLabel.Dispose ();
				AccuracyLabel = null;
			}

			if (CostVarianceLabel != null) {
				CostVarianceLabel.Dispose ();
				CostVarianceLabel = null;
			}

			if (CoverageLabel != null) {
				CoverageLabel.Dispose ();
				CoverageLabel = null;
			}

			if (LabourCostLabel != null) {
				LabourCostLabel.Dispose ();
				LabourCostLabel = null;
			}

			if (LabourHoursLabel != null) {
				LabourHoursLabel.Dispose ();
				LabourHoursLabel = null;
			}

			if (SalesForecastLabel != null) {
				SalesForecastLabel.Dispose ();
				SalesForecastLabel = null;
			}

			if (ScheduledCostLabel != null) {
				ScheduledCostLabel.Dispose ();
				ScheduledCostLabel = null;
			}

			if (ScheduledHoursLabel != null) {
				ScheduledHoursLabel.Dispose ();
				ScheduledHoursLabel = null;
			}

			if (TitleLabel != null) {
				TitleLabel.Dispose ();
				TitleLabel = null;
			}

			if (Separator != null) {
				Separator.Dispose ();
				Separator = null;
			}
		}
	}
}
