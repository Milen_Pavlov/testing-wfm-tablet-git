﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Drawing;

namespace WFM.iOS
{
    public partial class EnvironmentCell : TableCell
    {
        public static readonly NSString Key = new NSString ("EnvironmentCell");
        public static readonly UINib Nib;

        private bool m_inEditMode;
        public bool InEditMode
        {
            get { return m_inEditMode; }
            set
            {
                m_inEditMode = value;
                UpdateEditingUI();
            }
        }

        private bool m_canEdit;
        public bool CanEdit
        {
            get { return m_canEdit; }
            set
            {
                m_canEdit = value;
                UpdateEditingUI();
            }
        }

        private bool m_isAddRow;
        public bool IsAddRow
        {
            get { return m_isAddRow; }
            set
            {
                m_isAddRow = value;
                UpdateEditingUI();
            }
        }

        //Original UI State for reverting
        private UIColor m_titleCol;
        private UIColor m_addressCol;
        private RectangleF m_nameRect;

        static EnvironmentCell()
        {
            Nib = UINib.FromName (IsPhone ? "EnvironmentCell_iPhone" : "EnvironmentCell_iPad", NSBundle.MainBundle);
        }

        public EnvironmentCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnBind()
        {
            m_titleCol = NameLabel.TextColor;
            m_addressCol = AddressLabel.TextColor;

            m_nameRect = NameLabel.Frame;

            var set = this.CreateBindingSet<EnvironmentCell, WFM.Core.Environment>();
            set.Bind(this).For(v => v.InEditMode).To(vm => vm.InEditMode);
            set.Bind(this).For(v => v.CanEdit).To(vm => vm.Editable);
            set.Bind(this).For(v => v.IsAddRow).To(vm => vm.IsAddRow);
            set.Bind(NameLabel).To(vm => vm.Name);
            set.Bind(AddressLabel).To(vm => vm.Address);
            set.Apply();
        }

        public override void OnShow()
        {
            base.OnShow();

            UpdateEditingUI();

            DeleteButton.TouchUpInside += OnDelete;
        }

        public override void OnHide()
        {
            base.OnHide();

            DeleteButton.TouchUpInside -= OnDelete;
        }

        private void UpdateEditingUI()
        {
            NameLabel.TextColor = (InEditMode && !CanEdit) ? UIColor.LightGray : m_titleCol;
            AddressLabel.TextColor = (InEditMode && !CanEdit) ? UIColor.LightGray : m_addressCol;

            DeleteIcon.Hidden = !InEditMode || !CanEdit || IsAddRow;
            DeleteButton.Hidden = !InEditMode || !CanEdit || IsAddRow;

            AddIcon.Hidden = !IsAddRow;

            if (IsAddRow)
                NameLabel.Frame = new RectangleF(NameLabel.Frame.X, 0, DeleteIcon.Frame.X, Frame.Height);
            else
                NameLabel.Frame = m_nameRect;
        }

        private void OnDelete(object sender, EventArgs e)
        {
            WFM.Core.Environment data = DataContext as WFM.Core.Environment;

            if (data != null && CustomCommand1 != null && CustomCommand1.CanExecute(data))
                CustomCommand1.Execute(data);
        }
    }
}

