﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class EmployeeCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("EmployeeCell");
        public static readonly UINib Nib;

        static EmployeeCell()
        {
            Nib = UINib.FromName (IsPhone ? "EmployeeCell_iPhone" : "EmployeeCell_iPad", NSBundle.MainBundle);
        }

        public EmployeeCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            base.OnShow ();

            JDAEmployeeInfo data = DataContext as JDAEmployeeInfo;

            if (data != null)
            {
                NameLabel.Text = string.Format ("{0}{1}, {2}", (data.IsBorrowed) ? "[b] " : "" ,data.LastName, data.FirstName);
            }
        }

        private bool isSelected = false;
        public override void SetSelected (bool selected, bool animated)
        {
            base.SetSelected (selected, animated);

            if (selected && !isSelected && CustomCommand1 != null)
                CustomCommand1.Execute (this);

            isSelected = selected;
        }
    }
}

