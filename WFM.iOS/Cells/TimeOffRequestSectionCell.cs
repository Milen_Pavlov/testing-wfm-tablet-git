﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class TimeOffRequestSectionCell : TableCell
    {
        public static readonly NSString Key = new NSString ("TimeOffRequestSectionCell");
        public static readonly UINib Nib;

        private IStringService m_localiser;

        static TimeOffRequestSectionCell()
        {
            Nib = UINib.FromName (IsPhone ? "TimeOffRequestSectionCell_iPhone" : "TimeOffRequestSectionCell_iPad", NSBundle.MainBundle);
        }

        public TimeOffRequestSectionCell(IntPtr handle) : base (handle)
        {
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnShow()
        {
            JDATimeOffRequestSection data = this.DataContext as JDATimeOffRequestSection;
            if (data != null)
            {
                switch (data.Status)
                {
                    case TimeOffStatus.None:
                        TitleLabel.Text = m_localiser.Get ("approvalstatus_none");
                        break;
                    case TimeOffStatus.Pending:
                        TitleLabel.Text = m_localiser.Get ("approvalstatus_pending");
                        break;
                    case TimeOffStatus.Approved:
                        TitleLabel.Text = m_localiser.Get ("approvalstatus_managerapproved");
                        break;
                    case TimeOffStatus.Denied:
                        TitleLabel.Text = m_localiser.Get ("approvalstatus_denied");
                        break;
                    case TimeOffStatus.Deleted:
                        TitleLabel.Text = m_localiser.Get ("approvalstatus_deleted");
                        break;
                    default:
                        TitleLabel.Text = m_localiser.Get ("approvalstatus_none");
                        break;
                }

                SubtitleLabel.Text = string.Empty;//string.Format ("({0})", data.Date.ToString ("dd MMMM yyyy"));
            }
        }
    }
}

