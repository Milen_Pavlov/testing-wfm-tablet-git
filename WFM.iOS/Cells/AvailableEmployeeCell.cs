﻿using System;
using Cirrious.CrossCore;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class AvailableEmployeeCell : TableCell
    {
        public static readonly NSString Key = new NSString ("AvailableEmployeeCell");
        public static readonly UINib Nib = UINib.FromName ("AvailableEmployeeCell", NSBundle.MainBundle);

        private readonly IESSConfigurationService m_configurationService;

        public AvailableEmployeeCell (IntPtr handle) : base (handle)
        {
            m_configurationService = Mvx.Resolve<IESSConfigurationService>();
        }

        public override void OnShow ()
        {
            var data = DataContext as EmployeeData;

            NameLabel.Text = data.Name;
            PrimaryJobLabel.Text = data.Info.PrimaryJobName;
            HoursLabel.Text = data.Hours.ToString ("0.##");
            StoreLabel.Text = data.Info.HomeSiteName;
            ApproveShiftButton.Enabled = !m_configurationService.IsScheduleEditorReadOnlyForUserRole;
            ApproveShiftButton.TouchUpInside += OnFillUnfilledShift;
        }

        public override void OnHide ()
        {
            base.OnHide ();
            ApproveShiftButton.TouchUpInside -= OnFillUnfilledShift;
        }

        private void OnFillUnfilledShift (object sender, EventArgs e)
        {
            if (CustomCommand1 != null) 
            {
                CustomCommand1.Execute (DataContext);
            }
        }
   }
}
