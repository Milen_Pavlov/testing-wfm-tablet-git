﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Globalization;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class PeerRequestCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("PeerRequestCell");
        public static readonly UINib Nib;

        private readonly IStringService m_localiser;
        private TimeOffStatusStringConverter m_statusConverter;

        static PeerRequestCell()
        {
            Nib = UINib.FromName (IsPhone ? "PeerRequestCell_iPhone" : "PeerRequestCell_iPad", NSBundle.MainBundle);
        }

        public PeerRequestCell(IntPtr handle) : base (handle)
        {
            m_statusConverter = new TimeOffStatusStringConverter ();
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnShow()
        {
            base.OnShow ();

            PeerRequest data = DataContext as PeerRequest;

            EmployeeLabel.Text = data.EmployeeName;

            StatusLabel.Text = (string)m_statusConverter.Convert (data.Status, typeof (string), null, CultureInfo.CurrentCulture);


            TimeSpan span = data.EndDate - data.StartDate;
            if (span.TotalDays <= 1)
                DatesLabel.Text = data.StartDate.ToString (m_localiser.Get("dd/MM/yyyy"));
            else
                DatesLabel.Text = string.Format ("{0} - {1}", data.StartDate.ToString (m_localiser.Get("dd/MM/yyyy")), data.EndDate.ToString (m_localiser.Get("dd/MM/yyyy")));
        }
    }
}

