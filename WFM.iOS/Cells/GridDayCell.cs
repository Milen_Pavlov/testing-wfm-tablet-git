﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Diagnostics;
using Newtonsoft.Json;

namespace WFM.iOS
{
    public partial class GridDayCell : DraggableDestinationCell
    {
        public static readonly NSString Key = new NSString ("GridDayCell");
        public static readonly UINib Nib;
        public static readonly UIImage TriangleImage;
        public static readonly UIImage UnavailableImage;
        public static readonly UIImage TimeOffImage;
        public static readonly UIImage MinorImage;

        private readonly ScheduleService m_schedule;
        private readonly SpareShiftService m_spareShiftService;
        private readonly IESSConfigurationService m_essConfig;

        const int c_hoursInView = 24;
        const int c_minutesInDay = 1440;
        private float m_timeInterval = c_minutesInDay / ScheduleTime.DayIntervals;

        private UIPopoverController m_warningPopover;

        private UIView m_dayView;
        private JDAEmployeeInfo m_info;
        private UITapGestureRecognizer m_tap;
        private DateTime m_dayStart;

        private List<UIView> m_unavailableViewList;

        private static bool? m_showDetails;
        private bool ShowDetails
        {
            get
            {
                if (!m_showDetails.HasValue)
                    m_showDetails = Mvx.Resolve<IESSConfigurationService>().ShowScheduleDetails;

                return m_showDetails.Value;
            }
        }

        static GridDayCell()
        {
            Nib = UINib.FromName (IsPhone ? "GridDayCell_iPhone" : "GridDayCell_iPad", NSBundle.MainBundle);
            TriangleImage = UIImage.FromBundle ("yellow_triangle");
            UnavailableImage = UIImage.FromBundle ("hatch");
            TimeOffImage = UIImage.FromBundle ("hatch_dark");
            MinorImage = UIImage.FromBundle ("hatch_red");
        }

        public GridDayCell(IntPtr handle) : base (handle)
        {
            m_schedule = Mvx.Resolve<ScheduleService> ();
            m_spareShiftService = Mvx.Resolve<SpareShiftService> ();
            m_essConfig = Mvx.Resolve<IESSConfigurationService>();
        }

        public override void OnShow()
        {
            base.OnShow ();

            EmployeeData data = DataContext as EmployeeData;

            // Store for later
            m_info = data.Info;

            // Warning Count
            WarningButton.SetTitle (data.WarningCount == 0 ? "" : data.WarningCount.ToString (), UIControlState.Normal);
            WarningImage.Image = data.WarningCount == 0 ? null : TriangleImage;

            NameLabel.Text = string.Format("{0} {1}", data.Name, data.GetAttributes());
            HoursLabel.Text = string.Format ("{0:0.00}", data.Hours / m_essConfig.TotalHoursPerDayDivisor);

            EmployeeDayData lastDayData;
            
            if(data.DayIndex.Value != 0)
            {
                lastDayData = data.Days[data.DayIndex.Value - 1];
            }
            else
            {
                lastDayData = data.LastDayPreviousWeek;
            }

            CreateDayArea (data.Days [data.DayIndex.Value], lastDayData, data.CanEdit);

            UpdateCanAcceptShift (!m_dragService.IsDragging());
        }

        public override void OnHide()
        {
            base.OnHide ();
        }

        partial void OnNameSelect (NSObject sender)
        {
            if (CustomCommand1 != null)
                CustomCommand1.Execute(m_info);
        }

        partial void OnWarningSelect (NSObject sender)
        {
            var data = DataContext as EmployeeData;

            if (data.WarningCount == 0)
                return;

            var tableControl = new TableControl(new RectangleF(0,0,400,240));
            tableControl.RegisterCell<WarningCell> ();
            tableControl.Items = data.GetAllWarnings();
            var viewController = new UIViewController();
            viewController.Add(tableControl);
            m_warningPopover = new UIPopoverController(viewController);
            m_warningPopover.DidDismiss += (object s, EventArgs e) => { m_warningPopover = null; }; 
            m_warningPopover.PopoverContentSize = tableControl.Frame.Size;
            m_warningPopover.PresentFromRect(WarningButton.Frame, this, UIPopoverArrowDirection.Left, true);
        }

        public void OnShiftSelect (ShiftData shift)
        {
            if (CustomCommand3 != null)
                CustomCommand3.Execute(shift);
        }

        private void CreateDayArea(EmployeeDayData day, EmployeeDayData previousDay, bool canEdit)
        {
            // create holding view 
            if (m_dayView != null)
            {
                if (m_tap != null)
                {
                    m_dayView.RemoveGestureRecognizer (m_tap);
                    m_tap.Dispose ();
                    m_tap = null;
                }

                m_dayView.RemoveFromSuperview ();
                m_dayView.Dispose ();
            }
                
            m_dayStart = day.DayStart;
            m_dayView = new UIView(new RectangleF(0,0,ShiftView.Frame.Width, ShiftView.Frame.Height));

            if (canEdit)
            {
                m_tap = new UITapGestureRecognizer (OnTap);
                m_tap.NumberOfTapsRequired = 2; // Double tap
                m_dayView.AddGestureRecognizer (m_tap);
            }

            // Unavailable Time
            if (day.UnavailabilityRanges != null)
            {
                List<JDAInterval> unavailabilityRanges = new List<JDAInterval>();
                unavailabilityRanges.AddRange(day.UnavailabilityRanges);
                // check to see if any shifts from previous day overlap onto this one
                var overlaps = previousDay.UnavailabilityRanges.Where(p => p.End.Date.Equals(day.DayStart.Date));
                unavailabilityRanges.AddRange (overlaps);

                if(m_unavailableViewList == null)
                    m_unavailableViewList = new List<UIView>();
                m_unavailableViewList.Clear();

                foreach (var unavail in unavailabilityRanges)
                {   
                    DrawUnavailable (m_dayView, day.DayStart, unavail);
                }
            }

            // Shifts
            if (day.Shifts != null)
            {
                foreach (var shift in day.Shifts)
                {
                    var shiftControl = new ShiftControl (ShiftView.Frame, shift, day.DayStart, canEdit, ShowDetails);
                    shiftControl.ShiftSelect += OnShiftSelect;
                    m_dayView.AddSubview (shiftControl);
                }
            }

            // Previous day shifts
            if(previousDay != null)
            {
                foreach(var shift in previousDay.Shifts)
                {
                    if(shift.End > day.DayStart) // previous day shift runs over to current day
                    {
                        var shiftControl = new ShiftControl (ShiftView.Frame, shift, day.DayStart, canEdit, ShowDetails);
                        shiftControl.ShiftSelect += OnShiftSelect;
                        m_dayView.AddSubview (shiftControl);
                    }
                }
            }

            // Fixed Shifts
            if (day.FixedShifts != null)
            {
                foreach (var shift in day.FixedShifts)
                    DrawFixedShift(m_dayView, day.DayStart, shift);
            }

            ShiftView.AddSubview (m_dayView);
            ShiftView.SendSubviewToBack(m_dayView);

            // Draw Red line for now
            var now = DateTime.Now;
            if (now >= day.DayStart && now < day.DayStart.AddDays(1))
            {
                var x = XPosFromTime(m_dayView, day.DayStart, day.DayStart.AddDays(1), now);
                var nowLine = new UIView (new RectangleF(x, 0, 1, m_dayView.Frame.Height));
                nowLine.BackgroundColor = UIColor.Red;
                m_dayView.AddSubview (nowLine);
            }
        }

        private void OnTap()
        {
            if (m_tap != null && m_tap.State == UIGestureRecognizerState.Ended)
            {
                var point = m_tap.LocationOfTouch (0, m_dayView);

                // Request is for the location of where to put the shift, leave the decision
                // on where it actually goes, size etc. to common logic and not this view
                double minutes = (point.X / m_dayView.Frame.Width) * ScheduleTime.MinutesPerDay;
                var centre = m_dayStart.AddMinutes (minutes);

                var request = new ShiftEditMessage (this) {
                    Op = ShiftEditMessage.Operation.CreateDefaultShift,
                    EmployeeID = m_info.EmployeeID,
                    Start = centre,
                    End = centre
                };

                Mvx.Resolve<IMvxMessenger> ().Publish(request);
            }
        }

        private float XPosFromTime(UIView target, DateTime start, DateTime end, DateTime point)
        {
            DateTime clamped = DateTimeUtility.Clamp(point, start, end);
            TimeSpan offset = clamped - start;
            TimeSpan total = end - start;
            return (float)(offset.TotalMinutes * (target.Frame.Size.Width / total.TotalMinutes));
        }

        private RectangleF RectFromTime(UIView target, DateTime dayStart, DateTime start, DateTime end, float yBorder)
        {
            DateTime dayEnd = dayStart.AddDays (1);

            float x1 = XPosFromTime (target, dayStart, dayEnd, start);
            float x2 = XPosFromTime (target, dayStart, dayEnd, end);

            return new RectangleF () {
                X = x1,
                Y = yBorder,
                Width = x2 - x1,
                Height = target.Frame.Size.Height - (yBorder * 2) - 1
            };
        }

        private void DrawUnavailable(UIView parent, DateTime dayStart, JDAInterval interval)
        {
            var rect = RectFromTime(parent, dayStart, interval.Start, interval.End, 0);

            if (rect.Width == 0)
                return;

            var unavail = new UIView (rect);

            switch (interval.Type)
            {
                case "t": unavail.BackgroundColor = UIColor.FromPatternImage (TimeOffImage); break;
                case "m": unavail.BackgroundColor = UIColor.FromPatternImage (MinorImage); break;

                default: // "u"
                    unavail.BackgroundColor = UIColor.FromPatternImage (UnavailableImage); 
                    break; 
            }

            parent.AddSubview(unavail);
            m_unavailableViewList.Add(unavail);
        }

        private void DrawFixedShift(UIView parent, DateTime dayStart, JDAEmployeeAssignedJob job)
        {
            if (!job.End.HasValue)
                return;

            var rect = RectFromTime(parent, dayStart, job.Start, job.End.Value, 2);

            if (rect.Width == 0)
                return;

            var shift = new UIView (rect);
            shift.BackgroundColor = UIColor.Clear;
            shift.Layer.BorderWidth = 1;
            shift.Layer.BorderColor = UIColor.Black.CGColor;
            shift.Layer.CornerRadius = 4;
            shift.UserInteractionEnabled = false;
            parent.AddSubview(shift);
        }

        public override void DragStarted ()
        {
            UpdateCanAcceptShift (false);
        }

        public override void DragEnded ()
        {
            UpdateCanAcceptShift (true);
        }

        public override void DragMoved (PointF movementPos)
        {
        }

        public override void DragEntered ()
        {
        }

        public override void DragExited ()
        {
        }

        public override void DragDropped ()
        {
            if (CanAcceptDraggedShift () == false)
                return;

            // Send message that a shift drag has ended.
            // Retrieve Drag data
            ShiftDragData shiftData = null;
            try
            {
                shiftData = JsonConvert.DeserializeObject<ShiftDragData> (m_dragService.InitialDragData);
            }
            catch
            {
                Mvx.Error ("Unable to deserialize drag data");
            }

            if (shiftData == null)
                return;
            
            int shiftId = shiftData.ShiftID;
            int oldEmployeeId = shiftData.EmployeeID;
            DateTime shiftStart = shiftData.Start;
            DateTime shiftEnd = shiftData.End;

            //// Calculate shift time change
            int clampedMinutes = 0;
            if (shiftData.IsManualShift == false || oldEmployeeId == m_info.EmployeeID) 
            {
                var dragShadowFrame = m_dragService.DragShadow.Frame;
                float minuteSize = c_minutesInDay / m_dayView.Frame.Width;
                float newStart = dragShadowFrame.X * minuteSize;
                float oldStart = m_dragService.OriginalDragPos.X * minuteSize;
                float delta = newStart - oldStart;
                clampedMinutes = (int)(System.Math.Round (delta / m_timeInterval) * m_timeInterval);
            }

            ProcessDroppedShift (shiftStart, shiftEnd, clampedMinutes, oldEmployeeId, shiftId);
        }

        public void ProcessDroppedShift (DateTime shiftStart, DateTime shiftEnd, int clampedMinutes, int oldEmployeeId, int shiftId)
        {
            bool isSameEmployee = m_info.EmployeeID == oldEmployeeId;
            if (isSameEmployee && clampedMinutes == 0)
            {
                return; // If no change to shift on update, halt here
            }

            shiftStart = shiftStart.AddMinutes (clampedMinutes);
            shiftEnd = shiftEnd.AddMinutes (clampedMinutes);

            ShiftEditMessage shiftEdit = new ShiftEditMessage (this) 
            {
                Op = ShiftEditMessage.Operation.UpdateShift,
                EmployeeID = oldEmployeeId,
                ShiftID = shiftId,
                Start = shiftStart,
                End = shiftEnd,
                RecalculateDetails = true
            };

            if (!isSameEmployee)
            {
                shiftEdit.Op = ShiftEditMessage.Operation.ReassignShift;
                shiftEdit.NewEmployeeID = m_info.EmployeeID;
            }

            Mvx.Resolve<IMvxMessenger> ().Publish (shiftEdit);
        }

        protected void UpdateCanAcceptShift (bool forceAccept)
        {
            bool canAcceptShift = true;

            if (forceAccept)
                canAcceptShift = true;
            else
                canAcceptShift = CanAcceptDraggedShift ();

            if(canAcceptShift)
            {
                SetBackgroundColor (UIColor.White);
                HideUnavailableShifts(false);
            }
            else
            {
                SetBackgroundColor (UIColor.Gray);
                HideUnavailableShifts(true);
            }
        }

        void SetBackgroundColor (UIColor newColour)
        {
            UIView.Animate (0.2f,
                            () => {
                                m_dayView.BackgroundColor = newColour;
                            },
                            () => { });
        }

        void HideUnavailableShifts(bool hide)
        {
            if(m_unavailableViewList == null || m_unavailableViewList.Count <= 0)
                return;
            
            float newAlpha = hide ? 0.0f : 1.0f;
            foreach(var view in m_unavailableViewList)
            {
                UIView.Animate(0.2f,
                                () => {
                                    view.Alpha = newAlpha;
                                },
                                () => {});
            }
        }

        private bool CanAcceptDraggedShift ()
        {
            ShiftDragData shiftData = null;
            try 
            {
                shiftData = JsonConvert.DeserializeObject<ShiftDragData> (m_dragService.InitialDragData);
            }
            catch 
            {
                Mvx.Error ("Unable to deserialize drag data");
            }

            if (shiftData == null)
                return true;

            if (m_info == null)
                return true;

            //if dragging shift is for same employee no further checks are needed
            if(shiftData.EmployeeID == m_info.EmployeeID)
            {
                return true;       
            }

            EmployeeData originalEmployee = m_schedule.Schedule.GetEmployee (shiftData.EmployeeID);
            if (originalEmployee == null)
                originalEmployee = m_spareShiftService.GetEmployee (shiftData.EmployeeID);
            
            if (originalEmployee == null)
                return true;

            EmployeeData employee = m_schedule.Schedule.GetEmployee (m_info.EmployeeID);
            if (employee == null)
                employee = m_spareShiftService.GetEmployee (m_info.EmployeeID);
            
            if (employee == null)
                return true;
            
            var draggedShift = originalEmployee.GetShift (shiftData.ShiftID);
            return employee.CanWorkShift (draggedShift);
        }
    }
}

