// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
    [Register ("AvailableEmployeeCell")]
    partial class AvailableEmployeeCell
    {
        [Outlet]
        MonoTouch.UIKit.UIButton ApproveShiftButton { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel HoursLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel NameLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel PrimaryJobLabel { get; set; }


        [Outlet]
        MonoTouch.UIKit.UILabel StoreLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ApproveShiftButton != null) {
                ApproveShiftButton.Dispose ();
                ApproveShiftButton = null;
            }

            if (HoursLabel != null) {
                HoursLabel.Dispose ();
                HoursLabel = null;
            }

            if (NameLabel != null) {
                NameLabel.Dispose ();
                NameLabel = null;
            }

            if (PrimaryJobLabel != null) {
                PrimaryJobLabel.Dispose ();
                PrimaryJobLabel = null;
            }

            if (StoreLabel != null) {
                StoreLabel.Dispose ();
                StoreLabel = null;
            }
        }
    }
}