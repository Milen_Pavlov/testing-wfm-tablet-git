// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
    [Register ("DailyOverviewItemExceptionCell")]
    partial class DailyOverviewItemExceptionCell
	{
		[Outlet]
		public MonoTouch.UIKit.UILabel Name { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}
		}
	}
}
