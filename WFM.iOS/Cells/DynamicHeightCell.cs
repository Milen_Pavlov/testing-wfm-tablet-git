﻿using System;
using Consortium.Client.iOS;

namespace WFM.iOS
{
    public abstract class DynamicHeightCell : TableCell
    {
        public DynamicHeightCell(IntPtr handle) : base (handle)
        {
        }

        public abstract float GetHeightResized();
    }
}

