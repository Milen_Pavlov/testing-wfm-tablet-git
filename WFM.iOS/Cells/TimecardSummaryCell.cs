﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using System.Globalization;

namespace WFM.iOS
{
    public partial class TimecardSummaryCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("TimecardSummaryCell");
        public static readonly UINib Nib;

        static TimecardSummaryCell()
        {
            Nib = UINib.FromName (IsPhone ? "TimecardSummaryCell_iPhone" : "TimecardSummaryCell_iPad", NSBundle.MainBundle);
        }

        public TimecardSummaryCell(IntPtr handle) : base (handle)
        {
        }
         
        public override void OnBind ()
        {
            base.OnBind ();

            var set = this.CreateBindingSet<TimecardSummaryCell, EmployeeTimecardSummary>();
            set.Bind (NameLabel).To (vm => vm.EmployeeName);
            set.Bind (ScheduledLabel).To (vm => vm.Scheduled).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (NetHoursLabel).To (vm => vm.NetHours).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (AdjustmentsLabel).To (vm => vm.Adjustments).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (OtherHoursLabel).To (vm => vm.OtherHours).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (TotalHoursLabel).To (vm => vm.TotalHours).WithConversion(new ToStringValueConverter<double>(), "{0:0.00}");
            set.Bind (ExceptionsLabel).To (vm => vm.Exceptions);
            set.Apply();
        }
    }
}

