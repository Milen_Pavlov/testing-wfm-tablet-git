﻿using System;
using Consortium.Client.iOS;
using Consortium.Client.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace WFM.iOS
{
    public partial class EditShiftJobCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("EditShiftJobCell");
        public static readonly UINib Nib = UINib.FromName ("EditShiftJobCell", NSBundle.MainBundle);

        public EditShiftJobCell (IntPtr handle) : base (handle)
        {
        }

        public override void OnBind ()
        {
            var set = this.CreateBindingSet<EditShiftJobCell, JDANameAndID>();
            set.Bind (TitleLabel).To (m => m.Name);
            set.Apply();
        }
    }
}
