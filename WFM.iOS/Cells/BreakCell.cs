﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class BreakCell : TableCell
    {
		public static readonly NSString Key = new NSString ("BreakCell");
        public static readonly UINib Nib;

		static BreakCell()
        {
			Nib = UINib.FromName (IsPhone ? "BreakCell_iPhone" : "BreakCell_iPad", NSBundle.MainBundle);
        }

		public BreakCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
			BreakItem data = DataContext as BreakItem;

			StartTimeLabel.Text = data.StartTimeFormatted;
			DurationLabel.Text = data.Duration.ToString("N0");
			EmployeeNameLabel.Text = data.EmployeeName;
            DepartmentLabel.Text = data.Department;
        }
    }
}

