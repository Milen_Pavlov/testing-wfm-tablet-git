﻿using System;
using Consortium.Client.iOS;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public class AlternateBackgroundCell : TableCell
    {
        public static UIColor OddCol = new UIColor (0.95f, 0.95f, 1f, 1);
        public static UIColor EvenCol = UIColor.Clear;

        public virtual UIColor OddColour
        {
            get { return OddCol; }
        }

        public virtual UIColor EvenColour
        {
            get { return EvenCol; }
        }

        public virtual UIView BackgroundViewToRecolour
        {
            get { return ContentView; }
        }

        private bool m_isOddNumberedRow = false;
        public bool IsOddNumberedRow
        {
            get { return m_isOddNumberedRow; }

            set
            {
                m_isOddNumberedRow = value;
                BackgroundViewToRecolour.BackgroundColor = (m_isOddNumberedRow) ? OddColour : EvenColour;
            }
        }

        public AlternateBackgroundCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            BackgroundViewToRecolour.BackgroundColor = (m_isOddNumberedRow) ? OddColour : EvenColour;
        }
    }
}

