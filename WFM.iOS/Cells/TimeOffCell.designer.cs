// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffCell")]
	partial class TimeOffCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel CommentText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel StatusText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (StatusText != null) {
				StatusText.Dispose ();
				StatusText = null;
			}

			if (CommentText != null) {
				CommentText.Dispose ();
				CommentText = null;
			}
		}
	}
}
