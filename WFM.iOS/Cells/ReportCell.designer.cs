// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("ReportCell")]
	partial class ReportCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel DemandLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ScheduledLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TimeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel VarianceLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}

			if (DemandLabel != null) {
				DemandLabel.Dispose ();
				DemandLabel = null;
			}

			if (ScheduledLabel != null) {
				ScheduledLabel.Dispose ();
				ScheduledLabel = null;
			}

			if (VarianceLabel != null) {
				VarianceLabel.Dispose ();
				VarianceLabel = null;
			}
		}
	}
}
