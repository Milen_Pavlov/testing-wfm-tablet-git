// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("TimeOffNotificationCell")]
	partial class TimeOffNotificationCell
	{
		[Outlet]
		Consortium.Client.iOS.WebImageControl Icon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SubtitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (SubtitleLabel != null) {
				SubtitleLabel.Dispose ();
				SubtitleLabel = null;
			}

			if (TitleLabel != null) {
				TitleLabel.Dispose ();
				TitleLabel = null;
			}

			if (Icon != null) {
				Icon.Dispose ();
				Icon = null;
			}
		}
	}
}
