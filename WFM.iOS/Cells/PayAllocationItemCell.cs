﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

namespace WFM.iOS
{
    public partial class PayAllocationItemCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("PayAllocationItemCell");
        public static readonly UINib Nib;

        public bool IsDirty
        {
            get
            {
                return false;
            }

            set
            {
                if (value)
                    HoursTextEntry.TextColor = UIColor.Red;
                else
                    HoursTextEntry.TextColor = UIColor.Black;
            }
        }

        static PayAllocationItemCell()
        {
            Nib = UINib.FromName (IsPhone ? "PayAllocationItemCell_iPhone" : "PayAllocationItemCell_iPad", NSBundle.MainBundle);
        }

        public PayAllocationItemCell(IntPtr handle) : base (handle)
        {
        }
         
        public override void OnShow()
        {
            base.OnShow ();

            PayAllocationItem payItem = (DataContext as PayAllocationItem);
            HoursTextEntry.Text = string.Format ("{0:0.##}", payItem.Allocation.PaidHours);

            HoursTextEntry.EditingChanged += OnHoursChanged;
            HoursTextEntry.EditingDidEndOnExit += HoursEditingEnded;
        }

        public override void OnHide ()
        {
            base.OnHide ();

            HoursTextEntry.EditingChanged -= OnHoursChanged;
            HoursTextEntry.EditingDidEndOnExit -= HoursEditingEnded;
        }

        public override void OnBind ()
        {
            base.OnBind ();

            String dateFormat = Mvx.Resolve<IStringService> ().Get("dd/MM/yyyy");
            var set = this.CreateBindingSet<PayAllocationItemCell, PayAllocationItem>();
            set.Bind (DayLabel).To (m => m.Allocation.Date.DayOfWeek);
            set.Bind (TotalHoursLabel).To (m => m.TotalHours).WithConversion(new StringFormatValueConverter(),"0.00");
            set.Bind (this).For (c => c.IsDirty).To (m => m.Allocation.IsDirty);
            set.Bind (HoursLabel).To (m => m.Allocation.PaidHours).WithConversion(new StringFormatValueConverter(),"0.00");
            set.Bind (HoursTextEntry).For (c => c.Hidden).To (m => m.CanModify).WithConversion (new InvertedBooleanConverter ());
            set.Bind (HoursLabel).For (c => c.Hidden).To (m => m.CanModify);
            set.Bind (TotalView).For (c => c.Hidden).To (m => m.DisplayTotal).WithConversion (new InvertedBooleanConverter ());
            set.Bind (DateLabel).To (m => m.Allocation.Date).WithConversion (new DateConverter(),dateFormat);
            set.Bind (TypeLabel).To (m => m.Allocation.Type);

            set.Apply();
        }

        void HoursEditingEnded (object sender, EventArgs e)
        {
            PayAllocationItem payItem = (DataContext as PayAllocationItem);

            HoursTextEntry.Text = string.Format ("{0:0.##}", payItem.Allocation.PaidHours);
        }

        void OnHoursChanged (object sender, EventArgs e)
        {
            PayAllocationItem payItem = (DataContext as PayAllocationItem);

            double hours = payItem.Allocation.PaidHours;

            if (double.TryParse (HoursTextEntry.Text, out hours))
            {
                payItem.Allocation.PaidHours = hours;
                payItem.OnHoursChanged ();
            }
            else if (string.IsNullOrEmpty (HoursTextEntry.Text))
            {
                payItem.Allocation.PaidHours = 0;
                payItem.OnHoursChanged ();
            }
            else
            {
                HoursTextEntry.Text = string.Format ("{0:0.##}", payItem.Allocation.PaidHours);
            }
        }
    }
}

