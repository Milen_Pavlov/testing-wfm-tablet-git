﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    public partial class AccrualCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("AccrualCell");
        public static readonly UINib Nib;

        static AccrualCell()
        {
            Nib = UINib.FromName (IsPhone ? "AccrualCell_iPhone" : "AccrualCell_iPad", NSBundle.MainBundle);
        }

        public AccrualCell(IntPtr handle) : base (handle)
        {
        }

        public override void OnShow()
        {
            base.OnShow ();

            Accrual data = DataContext as Accrual;

            TypeLabel.Text = data.Type;
            AvailableLabel.Text = data.Available;
        }
    }
}

