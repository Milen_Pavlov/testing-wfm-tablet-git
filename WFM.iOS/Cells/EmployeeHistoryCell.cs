﻿using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using System.Globalization;
using Cirrious.CrossCore;
using Consortium.Client.Core;

namespace WFM.iOS
{
    public partial class EmployeeHistoryCell : AlternateBackgroundCell
    {
        public static readonly NSString Key = new NSString ("EmployeeHistoryCell");
        public static readonly UINib Nib;

        private readonly IStringService m_localiser;

        private TimeOffStatusStringConverter m_statusConverter;

        static EmployeeHistoryCell()
        {
            Nib = UINib.FromName (IsPhone ? "EmployeeHistoryCell_iPhone" : "EmployeeHistoryCell_iPad", NSBundle.MainBundle);
        }

        public EmployeeHistoryCell(IntPtr handle) : base (handle)
        {
            m_statusConverter = new TimeOffStatusStringConverter ();
            m_localiser = Mvx.Resolve<IStringService> ();
        }

        public override void OnShow()
        {
            base.OnShow ();

			TimeOffHistory data = DataContext as TimeOffHistory;

            if (data != null)
            {
                StatusLabel.Text = (string)m_statusConverter.Convert(data.Status, typeof(string), null, CultureInfo.CurrentCulture);
                TypeLabel.Text = data.Type;


                TimeSpan span = data.To - data.From;
                if (span.TotalDays <= 1)
                    DatesLabel.Text = data.From.ToString (m_localiser.Get("dd/MM/yyyy"));
                else
                    DatesLabel.Text = string.Format ("{0} - {1}", data.From.ToString (m_localiser.Get("dd/MM/yyyy")), data.To.ToString (m_localiser.Get("dd/MM/yyyy")));
            }
        }
    }
}

