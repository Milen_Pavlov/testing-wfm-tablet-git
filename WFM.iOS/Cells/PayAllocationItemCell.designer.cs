// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace WFM.iOS
{
	[Register ("PayAllocationItemCell")]
	partial class PayAllocationItemCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel DateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DayLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField HoursTextEntry { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TotalHoursLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TotalView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TypeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DateLabel != null) {
				DateLabel.Dispose ();
				DateLabel = null;
			}

			if (DayLabel != null) {
				DayLabel.Dispose ();
				DayLabel = null;
			}

			if (HoursLabel != null) {
				HoursLabel.Dispose ();
				HoursLabel = null;
			}

			if (HoursTextEntry != null) {
				HoursTextEntry.Dispose ();
				HoursTextEntry = null;
			}

			if (TotalHoursLabel != null) {
				TotalHoursLabel.Dispose ();
				TotalHoursLabel = null;
			}

			if (TotalView != null) {
				TotalView.Dispose ();
				TotalView = null;
			}

			if (TypeLabel != null) {
				TypeLabel.Dispose ();
				TypeLabel = null;
			}
		}
	}
}
