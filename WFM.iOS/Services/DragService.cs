﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    [ConsortiumService]
    public class DragService
    {
        const float c_autoScroll = 100.0f;

        public ImageControl DragShadow { get; private set; }
        public PointF OriginalDragPos { get; private set; }

        private List<DraggableDestinationCell> m_destinations = new List<DraggableDestinationCell>();
        private DraggableDestinationCell m_currentDestination;
        private DragTableControl m_dragTable;
        private DraggableDestinationCell m_dragRow;

        private PointF m_currentPos;

        public string InitialDragData { get; set; }

        public DragService ()
        {
        }

        public void OnStartDrag (UIView draggedView, string initialDragData)
        {
            if (m_dragRow != null)
                m_dragRow.IsDragging = false;
            
            m_dragRow = GetParentView<DraggableDestinationCell> (draggedView);
            m_dragRow.IsDragging = true;

            InitialDragData = initialDragData;
            CreateDragShadow (draggedView);

            if (m_dragTable != null)
                m_dragTable.StopScrolling ();
            m_dragTable = GetParentView<DragTableControl> (draggedView);

            m_currentDestination = null;
            SetCurrentDestination ();

            foreach (var destination in m_destinations)
            {
                destination.DragStarted ();
            }
        }

        public void OnUpdateDrag (PointF offset)
        {
            if(DragShadow == null)
                return;
            
            m_currentPos = new PointF (OriginalDragPos.X + offset.X, OriginalDragPos.Y + offset.Y);

            RectangleF newPos = DragShadow.Frame;
            newPos.X = m_currentPos.X;
            newPos.Y = m_currentPos.Y;
            DragShadow.Frame = newPos;

            if (m_dragTable != null)
            {
                RectangleF dragTableFrame = DragShadow.Superview.ConvertRectFromView (m_dragTable.Frame, m_dragTable.Superview);
                if (DragShadow.Frame.Y < dragTableFrame.Y + c_autoScroll) 
                {
                    // Start Scrolling up
                    m_dragTable.StartScrolling (ScrollState.Up);
                } 
                else if (DragShadow.Frame.Y > (dragTableFrame.Y + dragTableFrame.Height) - c_autoScroll) 
                {
                    //Start Scrolling down
                    m_dragTable.StartScrolling (ScrollState.Down);
                }
                else 
                {
                    m_dragTable.StopScrolling ();
                }
            }

            foreach (var destination in m_destinations)
            {
                destination.DragMoved (m_currentPos);
            }

            if (m_currentDestination != null && m_currentDestination.Hidden == false && m_currentDestination.Frame.Contains (m_currentPos) == false) 
            {
                m_currentDestination.DragExited ();
                m_currentDestination = null;
            }

            SetCurrentDestination ();
        }

        public void OnEndDrag ()
        {
            if (m_dragRow != null) 
            {
                m_dragRow.IsDragging = false;
                m_dragRow = null;
            }
            
            if (m_currentDestination != null) 
            {
                m_currentDestination.DragDropped ();
            }

            RemoveDragShadow ();

            if (m_dragTable != null) 
            {
                m_dragTable.StopScrolling ();
                m_dragTable = null;
            }

            foreach (var destination in m_destinations) 
            {
                destination.DragEnded ();
            }

            m_currentDestination = null;
        }

        void CreateDragShadow (UIView draggedView)
        {
            var rootController = draggedView.Window;
            RectangleF testFrame = rootController.ConvertRectFromView (draggedView.Frame, draggedView.Superview);
            DragShadow = new ImageControl (testFrame);

            // Duplicate the original view
            UIGraphics.BeginImageContextWithOptions (draggedView.Bounds.Size, false, 0.0f);
            draggedView.Layer.RenderInContext (UIGraphics.GetCurrentContext ());
            DragShadow.Image = UIGraphics.GetImageFromCurrentImageContext ();
            UIGraphics.EndImageContext ();

            DragShadow.Alpha = 0.5f;
            rootController.AddSubview (DragShadow);
            rootController.BringSubviewToFront (DragShadow);

            OriginalDragPos = new PointF(DragShadow.Frame.X, DragShadow.Frame.Y);
            m_currentPos = OriginalDragPos;
        }

        void RemoveDragShadow ()
        {
            if (DragShadow != null)
                DragShadow.RemoveFromSuperview ();
        }

        public void RegisterDestination (DraggableDestinationCell destination)
        {
            if (m_destinations.Contains (destination) == false)
                m_destinations.Add (destination);
        }

        public void UnregisterDestination (DraggableDestinationCell destination)
        {
            if (m_destinations.Contains (destination) == true)
                m_destinations.Remove (destination);
        }

        public void SetCurrentDestination ()
        {
            if (m_currentDestination != null)
                return;

            var rootController = DragShadow.Window;
            if (rootController == null)
                return;

            PointF testPos = new PointF (m_currentPos.X + (DragShadow.Frame.Width / 2), m_currentPos.Y + (DragShadow.Frame.Height / 2));
            foreach (var destination in m_destinations) 
            {
                if(destination.Hidden)
                    continue;
                
                RectangleF testFrame = rootController.ConvertRectFromView (destination.Frame, destination.Superview);
                if (testFrame.Contains (testPos)) 
                {
                    m_currentDestination = destination;
                    m_currentDestination.DragEntered ();
                    break;
                }
            }
        }

        public bool IsDragging ()
        {
            if (m_dragRow == null)
                return false;
            
            return m_dragRow.IsDragging;
        }

        static T GetParentView<T> (UIView view) where T : UIView
        {
            T result = view as T;
            if (result == null)
                result = GetParentView<T> (view.Superview);

            return result;
        }
    }
}

