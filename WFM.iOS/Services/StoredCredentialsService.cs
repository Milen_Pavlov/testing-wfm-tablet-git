﻿using System;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using WFM.Core;

namespace WFM.Core
{
    [ConsortiumService]
    public class StoredCredentialsService : IStoredCredentialsService
    {
        private const string c_usernameKey = "wfmcredentials.username";
        private const string c_passwordKey = "wfmcredentials.password";
        private const string c_siteIDKey = "wfmcredentials.siteid";
        private const string c_userIDKey = "wfmcredentials.userid";

        public string Username
        {
            get
            {
                return KeyChain.GetValue (c_usernameKey);
            }
        }

        public string Password
        {
            get
            {
                return KeyChain.GetValue (c_passwordKey);
            }
        }

        public string SiteID
        {
            get
            {
                return KeyChain.GetValue (c_siteIDKey);
            }
        }

        public string UserUID
        {
            get
            {
                return KeyChain.GetValue (c_userIDKey);
            }
        }

        public void StoreCredentials(string username, string password, string siteID, string userUID)
        {
            KeyChain.SetValue(c_usernameKey, username ?? string.Empty);
            KeyChain.SetValue(c_passwordKey, password ?? string.Empty);
            KeyChain.SetValue(c_userIDKey, userUID ?? string.Empty);
            KeyChain.SetValue(c_siteIDKey, siteID ?? string.Empty);
        }

        public void ClearCredentials()
        {
            KeyChain.RemoveValue(c_usernameKey);
            KeyChain.RemoveValue(c_passwordKey);
            KeyChain.RemoveValue(c_siteIDKey);
            KeyChain.RemoveValue(c_userIDKey);
        }

        public void UpdateSiteID(string newSiteId)
        {
            KeyChain.SetValue(c_siteIDKey, newSiteId);
        }            
    }
}