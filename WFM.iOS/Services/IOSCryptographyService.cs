﻿using System;
using WFM.Core;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using Consortium.Client.Core;
using System.Linq;

namespace WFM.iOS
{
    [ConsortiumService]
    public class IOSCryptographyService : ICryptographyService
    {
        private const int CipherBlockSize = 16;
        private readonly ILogger m_logger;

        public IOSCryptographyService (ILogger logger)
        {
            m_logger = logger;
        }

        public string Decrypt(string key, string textToDecrypt)
        {
            if (string.IsNullOrEmpty(key))
            {
                return textToDecrypt;
            }

            if (textToDecrypt == null || textToDecrypt == string.Empty)
            {
                return string.Empty;
            }

            var decryptedBytes = Decrypt(Convert.FromBase64String(textToDecrypt), key);
            var decryptedText = Encoding.UTF8.GetString(decryptedBytes);

            return decryptedText;
        }

        private byte[] Decrypt(byte[] bytes, string key)
        {
            key = ValidateKey(key);
            var iv = new byte[CipherBlockSize]; 
            Array.Copy(bytes, iv, CipherBlockSize); 
            var algorithm = new AesManaged();

            algorithm.IV = iv; 
            algorithm.Key = Encoding.UTF8.GetBytes(key); 

            byte[] decryptedBytes = null;

            using (var decryptor = algorithm.CreateDecryptor())
            {
                using (MemoryStream msDecrypted = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msDecrypted, decryptor, CryptoStreamMode.Write))
                    {
                        csEncrypt.Write(bytes, CipherBlockSize, bytes.Length - CipherBlockSize);
                    }

                    decryptedBytes = msDecrypted.ToArray();
                }
            }

            return decryptedBytes;
        }

        private string ValidateKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return string.Concat(Enumerable.Repeat('0', CipherBlockSize));
            }

            if(key.Length > CipherBlockSize)
            {
                return key.Substring(key.Length - CipherBlockSize, CipherBlockSize);
            }

            while(key.Length < CipherBlockSize)
            {
                key += '0';
            }

            return key;
        }
    }
}

