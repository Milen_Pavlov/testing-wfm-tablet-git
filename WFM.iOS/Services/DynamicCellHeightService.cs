﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.UIKit;
using WFM.Core;

namespace WFM.iOS
{
    /// <summary>
    /// This is a helper service of iOS table controls and collection controls
    /// 
    /// This service can be used to work out the height of a custom cell, with dynamic text resizing
    /// It will also cache this result so that when it is required again in the future it just returns
    /// the previous calcuated size.
    /// </summary>
    [ConsortiumService]
    public class DynamicCellHeightService
    {
        private Dictionary<string, float> m_heights;

        public DynamicCellHeightService ()
        {
            m_heights = new Dictionary<string, float>();
        }

        private string GenerateKey(Type type, string uid)
        {
            return string.Format("{0}_{1}", type, uid);
        }

        public float GetHeightForData(object data, UINib nibToUse, string uid)
        {
            string key = GenerateKey(data.GetType(), uid);

            if(m_heights.ContainsKey(key))
                return m_heights[key];

            //Generate new Cell and resize and store off
            DynamicHeightCell cellToUse = null;

            object[] views = nibToUse.Instantiate (null, null);
            if (views == null || views.Length == 0 || !(views[0] is DynamicHeightCell))
                throw new Exception ("DynamicHeightCell not found for dynamic cell height for nib " + nibToUse);

            //Bind data 
            cellToUse = views[0] as DynamicHeightCell;
            cellToUse.DataContext = data;
            cellToUse.BindOnce ();
            cellToUse.OnShow ();

            //Custom cell resize
            float newHeight = cellToUse.GetHeightResized();

            //Cleanup
            cellToUse.Dispose();
            cellToUse = null;

            m_heights.Add(key, newHeight);
            return newHeight;
        }
    }
}

