﻿using System;
using WFM.Core;
using Consortium.Client.Core;
using Cirrious.CrossCore;

//TODO!!!
namespace WFM.iOS
{
    [ConsortiumService]
    public class GoogleAnalyticsService : WFM.Core.IAnalyticsService
    {
        private IMobileApp m_mobileApp;
       
        public string TrackingID
        {
            set
            {
                SetupAnalyticsTracker (value);
            }
        }
            
        public GoogleAnalyticsService(IMobileApp app)
        {
            m_mobileApp = app;
            Mvx.Trace("(Analytics) Init");
        }

        void SetupAnalyticsTracker (string trackingCode)
        {            
            Mvx.Trace("(Analytics) Set Tracking Code: {0}",trackingCode);
        }

        public void SetSessionParam(int dimensionIndex, string value)
        {
            Mvx.Trace("(Analytics) Set Data: {0} = {1}", dimensionIndex, value);
        }

        public void TrackScreenView (string screenName)
        {
            Mvx.Trace("(Analytics) View Screen: {0}",screenName);
        }

        public void TrackAction (string categoryName, string actionName)
        {
            TrackAction (categoryName, actionName, null);
        }

        public void TrackAction (string categoryName, string actionName, string value)
        {
            Mvx.Trace("(Analytics) Perform Action: {0}:{1} - {2}", categoryName,actionName,value);
        }
    }
}