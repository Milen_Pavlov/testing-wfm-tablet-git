﻿using System;
using WFM.Core;
using Consortium.Client.Core;
using System.Collections;

namespace WFM.iOS
{
    [ConsortiumService]
    public class RaygunService : IRaygunService
    {
        public RaygunService ()
        {
        }

        public void LogCustomError (Exception e)
        {
            //Not implemented on iOS
        }

        public void LogCustomError (Exception e, IDictionary data)
        {
            //Not implemented on iOS
        }

        public void ClearUserInfo ()
        {
            //Not implemented on iOS
        }

        public void SetCurrentUserInfo (string loginname, string siteId, string siteName, string environment)
        {
            //Not implemented on iOS
        }
    }
}

