﻿using Consortium.Client.Core;
using Consortium.Client.iOS;
using WFM.Core;

namespace WFM.iOS
{
    [ConsortiumService]
    public class UserDetailsService : IUserDetailsService
    {
        #region IUserDetailsService implementation

        public void SetUsername(string username)
        {
            KeyChain.SetValue(UserDetailsUtility.UsernameKey, username);
        }

        public string GetUsername()
        {
            return KeyChain.GetValue(UserDetailsUtility.UsernameKey);
        }

        public void ClearUsername()
        {
            KeyChain.RemoveValue(UserDetailsUtility.UsernameKey);
        }

        public void SetPassword(string password)
        {
            KeyChain.SetValue(UserDetailsUtility.PasswordKey, password);
        }

        public string GetPassword()
        {
            return KeyChain.GetValue(UserDetailsUtility.PasswordKey);
        }

        public void ClearPassword()
        {
            KeyChain.RemoveValue(UserDetailsUtility.PasswordKey);
        }

        public void SetKeyValue( string key, string value )
        {
            KeyChain.SetValue( key, value );
        }

        public string GetValueForKey( string key )
        {
            return KeyChain.GetValue( key );
        }

        public void RemoveValueForKey( string key )
        {
            KeyChain.RemoveValue( key );
        }

        #endregion
    }
}