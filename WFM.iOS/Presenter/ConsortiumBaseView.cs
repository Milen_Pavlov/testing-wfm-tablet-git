using System;
using Cirrious.MvvmCross.Touch.Views;
using Consortium.Client.Core;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using Cirrious.CrossCore;

namespace Consortium.Client.iOS
{
    public abstract class ConsortiumBaseView : MvxViewController
    {
        public enum State
        {
            Open,
            Closing,
            Closed
        };
            
        public event EventHandler ViewDidClose;

        public State ViewState { get; private set; }
        public UIColor CommandTextNormalColor { get; set; }
        public UIColor CommandTextHighlightedColor { get; set; }
        public UIColor CommandTextDisabledColor { get; set; }
        public UINavigationController Navigation { get; set; }

        protected IFileSystem FileSystem { get; private set; }

        public static bool IsPhone 
        { 
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }
            
        public ConsortiumBaseView(string nibName, NSBundle bundle) 
            : base(nibName, bundle)
        {
            CommandTextNormalColor = UIColor.Black;
            CommandTextHighlightedColor = UIColor.Gray;
            CommandTextDisabledColor = UIColor.LightGray;
            FileSystem = Mvx.Resolve<FileSystem>();
            Navigation = null;
            ViewState = State.Open;
        }

        public abstract void Present(ConsortiumBaseView parent);
        public abstract void Close();
        public abstract ConsortiumBaseView ChildView();
        public abstract ConsortiumBaseView ParentView();
        public abstract void OnChildPresented(ConsortiumBaseView child);

        public void NotifyClosing()
        {
            if (ViewState != State.Open)
                return;

            if (IsViewLoaded && View.Window != null)
            {
                ViewState = State.Closing;
            }
            else
            {
                ViewState = State.Closed;
                ViewClosed ();
            }
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            View.TranslateRecursively();
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

            //if (NavigationItem != null)
            //    NavigationItem.BackBarButtonItem = new UIBarButtonItem(string.Empty, UIBarButtonItemStyle.Plain, null, null);
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear (animated);

            // If view was in the process of closing then call ViewClosed()
            if (ViewState == State.Closing)
            {
                ViewState = State.Closed;
                ViewClosed ();
            }
        }

        protected virtual void ViewClosed()
        {
            Mvx.Trace ("VIEWCLOSED: {0}, {1}", this.GetType().Name, this.Title);

            var cvm = ViewModel as ConsortiumViewModel;

            if (cvm != null)
                cvm.OnViewClosed ();

            if (ViewDidClose != null)
                ViewDidClose (this, null);
        }
 
        protected UIBarButtonItem MakeNavigationButton(string image, EventHandler handler)
        {
            var button = new UIButton(UIButtonType.Custom);
            button.Frame = new RectangleF(0f, 0f, 26f, 26f);
            button.TouchUpInside += handler;
            button.ShowsTouchWhenHighlighted = false;
            button.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            if (!string.IsNullOrEmpty(image))
            {
                var path = FileSystem.GetPath(image);
                button.SetImage(UIImage.FromFile(path), UIControlState.Normal);
            }

            return new UIBarButtonItem(button);
        }
    }
}

