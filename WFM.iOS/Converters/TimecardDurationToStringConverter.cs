﻿using System;
using Cirrious.CrossCore.Converters;

namespace WFM.iOS
{
    public class TimecardDurationToStringConverter : MvxValueConverter<double, string>
    {
        protected override string Convert(double value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == 0)
                return string.Empty;

            //Get whole hours
            int hours = (int)Math.Floor(value);

            //Get minutes
            double remainderPercentage = value - hours;
            double minutesActual = remainderPercentage * 60;
            int minutes = (int)Math.Floor (minutesActual);

            return string.Format ("{0}:{1}", hours.ToString ("D2"), minutes.ToString ("D2"));
        }
    }
}

