﻿using System;
using Cirrious.CrossCore.Converters;
using Consortium.Client.iOS;
using MonoTouch.UIKit;

namespace WFM.iOS
{
    public class HexColorCoverter : MvxValueConverter<string, UIColor>
    {
        protected override UIColor Convert(string data, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ColorUtility.FromHexString(data);
        }
    }
}
