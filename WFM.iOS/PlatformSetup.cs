﻿using Cirrious.MvvmCross.Binding.BindingContext;
using Consortium.Client.Core;
using Consortium.Client.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using WFM.Core;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.Bindings.Target.Construction;

namespace WFM.iOS
{
    public class PlatformSetup : IPlatformSetup
    {
        private const int c_backgroundFetchSeconds = 60;

        public IOSConfig GetPlatformConfig()
        {
            return new IOSConfig();
        }

        public ConsortiumConfig InitialiseFirst()
        {
            return new ConsortiumConfig() {
                RequiresSignIn = false,
                HomeView = typeof(SessionViewModel)
            };
        }

        public void InitialiseLast()
        {
            var settings = Mvx.Resolve<SettingsService> ();
            UIColor primary = ColorUtility.FromHexString (settings.AppTheme.PrimaryColor);
            UIColor secondary = ColorUtility.FromHexString (settings.AppTheme.SecondaryColor);

            //Needed for iOS 7.X
            UINavigationBar.AppearanceWhenContainedIn (typeof(UIPopoverController)).BackgroundColor = primary;

            // Set the default titlebar text
            UITextAttributes navigationText = new UITextAttributes();
            navigationText.TextColor = secondary;
            navigationText.TextShadowColor = UIColor.Clear;
            navigationText.Font = UIFont.FromName ("AvenirNext-Regular", 20f);

            UINavigationBar.Appearance.SetBackgroundImage(ImageUtility.ImageFromColor(primary), UIBarMetrics.Default);
            UINavigationBar.Appearance.SetTitleTextAttributes(navigationText);
            UINavigationBar.Appearance.BarTintColor = primary;
            UINavigationBar.Appearance.TintColor = secondary;

            UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;
            UIButton.Appearance.SetTitleShadowColor (UIColor.Clear, UIControlState.Normal);
        }

        public void RegisterBindings(IMvxBindingNameRegistry registry)
        {
        }
            
        public bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            return true;
        }

        public void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
        }

        public void DidEnterBackground(UIApplication application)
        {
        }

        public bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            return false;
        }

        public void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
        }

        public void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
        }

        public void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
        }

        public void RegisterCustomBindings(IMvxTargetBindingFactoryRegistry registry)
        {
        }

        public void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
        {
        }

        public void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, System.Action<UIBackgroundFetchResult> completionHandler)
        {
        }

        public void PerformFetch (UIApplication application, System.Action<UIBackgroundFetchResult> completionHandler)
        {

        }
    }
}
