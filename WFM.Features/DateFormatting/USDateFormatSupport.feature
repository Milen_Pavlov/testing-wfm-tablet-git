﻿Feature: As a Walmart manager I can see US date formats throughout the app
	In order not to confuse Americans
	As an American
	I want to see the date in US (wrong) format
	
@manualios
Scenario Outline: As a Walmart manager I see US date formats in the app
	Given I am a Walmart manager
	And I'm using the Walmart theme
	When I am on the <screen>
	Then The date is displayed in MM/DD/YYYY format

	Examples:
		| screen                                    |
		| Schedule Week View                        |
		| Schedule Day View                         |
		| Breaks Week View                          |
		| Breaks Day View                           |
		| Time off requests day popover             |
		| Time off request detail view              |
		| Time off request detail peer request view |
		| Time off request detail history view      |
		| Employee time off history popover view    |

@manualios
Scenario Outline: As a Walmart manager I enter days in US format (Month, Day then Year)
	Given I am a Walmart manager
	And I'm using the Walmart theme
	When I enter a date on the <datescreen>
	Then The UI displays Month, then Day, then Year

	Examples:
		| datescreen                                |
		| Create Time off for employee view			|