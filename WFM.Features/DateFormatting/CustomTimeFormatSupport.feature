﻿Feature: As a Manager I can see time using a consistent time format throughout the app
	In order not to be confused by times displayed in the app
	As a manager
	I want to see the time in a consistent format
	
@manualios
Scenario Outline: As a manager I see twelve hour time displayed across the app
	Given I am a manager
	And I'm using a theme configured to use the 12 hour format
	When I am on the <screen>
	Then the time is displayed in the 12 hour format

	Examples:
		| screen                                    |
		| Schedule Week View                        |
		| Schedule Day View                         |
		| Unfilled Shift View	                    |
		| Start Time Edit Filter                    |
		| Variance View		                        |
		| Shift Details View                        |
		| Breaks Week View                          |
		| Breaks Day View                           |
		| Time off request detail view              |
		| Time off request create partial day view  |
		| Shift Swap Approval view					|
		| Shift Swap Approval History view   		|
		| Daily Overview Day View				    |
		| Daily Overview Edit Shift View		    |
		| Daily Overview Create Shift View		    |
		| Weekly Overview Exceptions View 			|
		| Weekly Overview Edit Shift View 			|
				
				
@manualios
Scenario Outline: As a manager I see twenty four hour time displayed across the app
	Given I am a manager
	And I'm using a theme configured to use the 24 hour format
	When I am on the <screen>
	Then the time is displayed in the 24 hour format

	Examples:
		| screen                                    |
		| Schedule Week View                        |
		| Schedule Day View                         |
		| Unfilled Shift View	                    |
		| Start Time Edit Filter                    |
		| Variance View		                        |
		| Shift Details View                        |
		| Breaks Week View                          |
		| Breaks Day View                           |
		| Time off request detail view              |
		| Time off request create partial day view  |
		| Shift Swap Approval view					|
		| Shift Swap Approval History view   		|
		| Daily Overview Day View				    |
		| Daily Overview Edit Shift View		    |
		| Daily Overview Create Shift View		    |
		| Weekly Overview Exceptions View 			|
		| Weekly Overview Edit Shift View 			|
				
@manualios
Scenario Outline: As a manager I enter time in 12 hour format
	Given I am a  manager
	And I'm using the app configured to use the 12 hour format
	When I enter a time on the <timescreen>
	Then The UI allows the user to enter Hours (up to 12), Minutes and AM/PM

	Examples:
		| timescreen                                |
		| Edit Break Start Time						|
		| Edit Shift Start Time						|
		| Edit Time off Start Time					|
		| Edit Time off End Time					|		
		| Daily Overview Shift Select Time 			|	
	
@manualios
Scenario Outline: As a manager I enter time in 24 hour format
	Given I am a  manager
	And I'm using the app configured to use the 24 hour format
	When I enter a time on the <timescreen>
	Then The UI allows the user to enter hours (up to 24) and minutes

	Examples:
		| timescreen                                |
		| Edit Break Start Time						|
		| Edit Shift Start Time						|
		| Edit Time off Start Time					|
		| Edit Time off End Time					|		
		| Daily Overview Shift Select Time 			|	