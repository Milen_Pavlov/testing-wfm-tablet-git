﻿Feature: SignIn
    
@mytag
Scenario: As A Manager I want to sign into ESS
    Given I am a manager
    And I have valid login credentials 
    When I sign in to the application
    Then I am taken to the main screen of the application
