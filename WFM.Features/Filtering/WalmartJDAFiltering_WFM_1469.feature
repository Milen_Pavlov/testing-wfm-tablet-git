﻿Feature: As a Walmart manager I can filter the schedule in the same way I can on the JDA website
	In order to see the data how I'm used to
	As a manager
	I want to be able to filter by the same fields as I can on the JDA website
	
@manualios
Scenario Outline: As a Walmart manager I can see the filter <filter> option
	Given I am a Walmart manager
	And I'm in the Walmart theme
	And I'm on the schedule screen
	When I tap the filter icon
	Then The filter option <filter> is included in the list of available filters

	Examples:
		| filter      |
		| Workgroup   |
		| Job         |
		| Labour Role |

@manualios
Scenario Outline: As a Walmart manager I can select multiple entries for a selected filter
	Given I am a Walmart manager
	And I'm in the Walmart theme
	And I'm on the schedule screen
	When I tap the filter icon
	And I select the <filter> filter
	Then I can select which items I want to filter by for the <filter> filter

	Examples:
		| filter      |
		| Workgroup   |
		| Job         |
		| Labour Role |

@manualios
Scenario Outline: As a Walmart manager I can see the results of my selected filter
	Given I am a Walmart manager
	And I'm in the Walmart theme
	And I'm on the schedule screen
	When I tap the filter icon
	And I select the filter <filter>
	And I select one or more items from the selected <filter> filter
	Then The schedule refreshes and displays only entries for my selected filters

	Examples:
		| filter      |
		| Workgroup   |
		| Job         |
		| Labour Role |