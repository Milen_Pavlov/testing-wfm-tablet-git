﻿Feature: Write schedule audit details when changing store schedule
	In order to keep audit information up to date
	As a manager using an environment that supports Schedule Audits
	I want to be able to see audit information on JDA for all changes made to the schedule whilst in the app
	
@iosmanual @tabletmanual
Scenario: Create Shift
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have created a shift
	And I have submitted the change
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I see an entry that shows the created shift

@iosmanual @tabletmanual
Scenario: Modify Shift
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have modified a shift
	And I have submitted the change
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I see an entry that shows the modification to the shift

@iosmanual @tabletmanual
Scenario: Delete Shift
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have deleted a shift
	And I have submitted the change
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I see an entry that shows that the shift was deleted

@iosmanual @tabletmanual
Scenario: Cancel modifications
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have made changes to a shift
	And I have discarded the changes
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I don't see any entries for changes that have been discarded

@iosmanual @tabletmanual
Scenario: Modify Meal
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have modified a meal
	And I have submitted the change
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I see an entry that shows the modification to the meal


@iosmanual @tabletmanual
Scenario: Modify Break
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have modified a break for a shift
	And I have submitted the change
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I see an entry that shows the modification to the break


@iosmanual @tabletmanual
Scenario: Unfill Shift
	Given I am on the schedule screen in the app
	And I am on an environment that supports Schedule Audits
	And I have unfilled a shift
	And I have submitted the change
	When I view the "Schedule Audits" page in the "More Tasks" tab on JDA
	Then I see an entry that shows the shift being unfilled
