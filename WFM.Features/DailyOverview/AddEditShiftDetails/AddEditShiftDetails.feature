﻿Feature: Add Shifts and Edit Shift Details on Overview
	In order to fix exceptions
	As a manager
	I want to add and edit shift details from the daily overview page
	
@iosmanual @tabletmanual
Scenario: As a manager I can open the edit details page
	Given I am on the daily overview page
	And I have a list of shifts
	When I click a shift
	Then the edit details page must appear

@iosmanual @tabletmanual
Scenario: As a manager I can open the add details page
	Given I am on the daily overview page
	When I click the add shift button
	Then the add details page must appear

@iosmanual @tabletmanual
Scenario: As a manager I can view the edit employee picker pop-up
    Given I am on the edit shift page
    And I am adding a new shift
    When I click the edit button next to a new employee
    Then the edit employee picker pop-up is shown

@iosmanual @tabletmanual
Scenario: As a manager I can choose an employee when adding a new shift
    Given I am on the edit shift page
    And I am adding a new shift
    And I am viewing the employee picker pop-up
    When I select an employee the popup closes
    And the employee name appears on the add details page
    And the primary job of the employee appears

@iosmanual @tabletmanual
Scenario: As a manager I can choose a date when adding a new shift
	Given I am on the edit shift page
	And I am adding a new shift
	When I click the edit button next to the date
	And I select a date
	Then the popup closes
	And the date will indubitably appear on the add details page

@iosmanual @tabletmanual
Scenario: As a manager I can view the job selection pop-up
    Given I am on the edit shift page
    And I have an employee selected
    When I click the edit button next to the default job
    Then the job select pop-up opens

@iosmanual @tabletmanual
Scenario: As a manager I can choose a default job
    Given I am on the edit shift page
    And I have an employee selected
    And I am viewing the job select pop-up
    When I select a job
    Then The job select pop-up closes
    And The selected job appears on the add details page

@iosmanual @tabletmanual
Scenario: As a manager I can add a meal for the current shift
	Given I am on the edit shift page
	When I click the "add meal" button
	Then a "Meal Start" row must be added to the list
	And a "Meal End" row must be added to the list

@iosmanual @tabletmanual
Scenario: As a manager I can add a break for the current shift
	Given I am on the edit shift page
	When I click the "add break" button
	Then a "Break Start" row will be added to the list
	And a "Break End" row will be added to the list

@iosmanual @tabletmanual
Scenario: As a manager I can add a job for the current shift
	Given I am on the edit shift page
	And I have an employee selected
	When I click the "add job" button
	Then a "Job Start" row must be added to the list
	And a "Job End" row will be added to the list

@iosmanual @tabletmanual
Scenario: As a manager I can edit a shift start job within a row
	Given I am on the edit shift page
	When I click the "Job" section of the "Shift Start"
	And I select a job from the job picker
	Then the job will appear in the "Shift Start" row

@iosmanual @tabletmanual
Scenario: As a manager I can edit a shift end job within a row
	Given I am on the edit shift page
	When I click the "Job" section of the "Shift End"
	And I select a job from the job picker
	Then the job will appear in the "Shift End" row

@iosmanual @tabletmanual
Scenario: As a manager I can edit a job start job within a row
	Given I am on the edit shift page
	When I click the "Job" section of the "Job Start"
	And I select a job from the job picker
	Then the job will appear in the "Job Start" row

@iosmanual @tabletmanual
Scenario: As a manager I can edit a job end job within a row
	Given I am on the edit shift page
	When I click the "Job" section of the "Job End"
	And I select a job from the job picker
	Then the job must appear in the "Job End" row

	@iosmanual @tabletmanual
Scenario: As a manager I can edit the actual punch time of a row
	Given I am on the edit shift page
	When I click the "Actual" section of a row
	And I select a time from the time picker
	Then the time will appear in the "Actual" section of the row

@iosmanual @tabletmanual
Scenario: As a manager I can match the actual punch time of a row to the scheduled time
	Given I am on the edit shift page
	And the row I wish to edit has a scheduled time
	When I click the "Match" button for the row
	Then The "Scheduled" time will be copied to the "Actual" section

@iosmanual @tabletmanual
Scenario: As a manager I can match all actual punch times to the scheduled times
	Given I am on the edit shift page
	And There are missing punches for at least one row
	When I click the "Match All Missing Punches To Schedule" button for the row
	Then The "Scheduled" time will be copied to the "Actual" section for all rows that had now tiem in "Actual"

@iosmanual @tabletmanual
Scenario: As a manager I can cancel editing of the shift, by clicking the cancel button
	Given I am on the edit shift page
	And I have no changes
	When I click the "Cancel" button
	Then the edit shift page disappears

@iosmanual @tabletmanual
Scenario: As a manager I can cancel editing of the shift, by clicking outside the popup
	Given I am on the edit shift page
	And I have no changes
	When I click the outside of the popup
	Then the edit shift page disappears like magic

@iosmanual @tabletmanual
Scenario: As a manager I am warned of losing changes when clicking the cancel button
	Given I am on the edit shift page
	And I have changes
	When I click the "Cancel" button
	Then a dialog pops up asking for discard confirmation

@iosmanual @tabletmanual
Scenario: As a manager I am warned of losing changes when clicking outside the popup
	Given I am on the edit shift page
	And I have changes
	When I click the outside of the popup
	Then a dialog pops up asking for discard confirmation

@iosmanual @tabletmanual
Scenario: As a manager I can save my changes to the shift
	Given I am on the edit shift page
	And I have no errors in my shift punches
	When I click the "Save" button
	Then the changes will be posted to JDA
	And the popup must close
	And the Daily Overview will refresh
	And my changes must be shown on the Daily Overview screen

@iosmanual @tabletmanual
Scenario: As a manager I am shown any errors when attempting to save my changes to the shift
	Given I am on the edit shift page
	And I have errors in my shift punches
	When I click the "Save" button
	Then I should receive a popup with a warning detailing the error(s)
	And I can return to th edit shift page by discarding the popup

@iosmanual @tabletmanual
Scenario: JDA hiding meal punches
	Given I am on the edit shift page
	When JDA does not allow meal punches
	Then I will not see any punches related to meals
	And I will not see the "Add Meal" button

@iosmanual @tabletmanual
Scenario: JDA hiding break punches
	Given I am on the edit shift page
	When JDA does not allow break punches
	Then I will not see any punches related to breaks
	And I will not see the "Add Break" button

@iosmanual @tabletmanual
Scenario: App Config hiding meal punches
	Given I am on the edit shift page
	When the AllowPunchesForMeals setting is false
	Then I will not see any punches related to meals
	And I will not see the "Add Meal" button

@iosmanual @tabletmanual
Scenario: App Config hiding break punches
	Given I am on the edit shift page
	When the AllowPunchesForBreaks setting is false
	Then I will not see any punches related to breaks
	And I will not see the "Add Break" button

@iosmanual @tabletmanual
Scenario: App Config hiding job punches
	Given I am on the edit shift page
	When the AllowPunchesForJobs setting is false
	Then I will not see any punches related to jobs
	And I will still see shift start and end punches
	And I will not see the "Add Job" button