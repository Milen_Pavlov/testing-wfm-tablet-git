﻿Feature: Filtering by employee or exception
	In order to find employees or exceptions easily
	As a manager
	I want to be able to filter by employees or exceptions

@iosmanual @tabletmanual
Scenario: Filter option on DailyOverview screen
	Given I am logged in as a manager
	When I am on the DailyOverview screen
	Then I see an option in the top right to filter the current list

@iosmanual @tabletmanual
Scenario: Open Filter View
	Given I am logged in as a manager
	And I am on the DailyOverview screen
	When I tap the filter icon
	Then I see the filter view appear
	And I have options to filter by Employee or Exceptions

@iosmanual @tabletmanual
Scenario: Filter by Employee entry
	Given I am logged in as a manager
	And I have opened the filter view
	When I choose to filter by Employee
	Then I see a list of all employees

@iosmanual @tabletmanual
Scenario: Filter by Employee
	Given I am logged in as a manager
	And I have opened the filter view
	And I have chosen an employee(s) to filter by
	When I choose to apply the filter
	Then I see only employee(s) that match the filter

@iosmanual @tabletmanual
Scenario: Filter by Exception entry
	Given I am logged in as a manager
	And I have opened the filter view
	When I choose to filter by Exception
	Then I see a list of exceptions to filter by

@iosmanual @tabletmanual
Scenario: Filter by Exception
	Given I am logged in as a manager
	And I have opened the filter view
	And I have chosen an exception(s) to filter by
	When I choose to apply the filter
	Then I see only entries that contain the exception(s)

@iosmanual @tabletmanual
Scenario: Filter by Exception and Employee
	Given I am logged in as a manager
	And I have opened the filter view
	And I have chosen an exception(s) to filter by
	And I have chosen an employee(s) to filter by
	When I choose to apply the filter
	Then I see only entries that contain the exception(s) and employees

@iosmanual @tabletmanual
Scenario: Clear the filter
	Given I am logged in as a manager
	And I have applied a filter
	When I open the filter view again
	And Choose to clear the filter
	Then I see the full list of employees again

@iosmanual @tabletmanual
Scenario: Maintain filter when changing days
	Given I am logged in as a manager
	And I have applied a filter
	When I move to another day
	Then I see only entries that contain the filter

@iosmanual @tabletmanual
Scenario: Maintain filter when changing screens
Given I am logged in as a manager
	And I have applied a filter
	When I move to another screen
	And move back to the daily overview screen
	Then I see only entries that contain the filter

@iosmanual @tabletmanual
Scenario: Discard filter when fully closing app
Given I am logged in as a manager
	And I have applied a filter
	When I fully close the app
	And I reopen the app
	And I move back to the daily overview screen
	Then I see the full list of employees
	And the filter has been reset

@iosmanual @tabletmanual
Scenario: Discard filter when the app crashes
Given I am logged in as a manager
	And I have applied a filter
	When the app crashes
	And I reopen the app
	And I move back to the daily overview screen
	Then I see the full list of employees
	And the filter has been reset

@iosmanual @tabletmanual
Scenario: Discard filter when logging out
Given I am logged in as a manager
	And I have applied a filter
	When I log out of the app
	And I log back into the app
	And I move back to the daily overview screen
	Then I see the full list of employees
	And the filter has been reset

@iosmanual @tabletmanual
Scenario: New filter to Weekly overview
Given I am logged in as a manager
	And I have applied a filter on the weekly overview screen
	When I open the daily overview screen
	Then I see the full list of employees

@iosmanual @tabletmanual
Scenario: Different filter to Weekly overview
Given I am logged in as a manager
	And I have applied a filter on the daily overview screen
	And I have applied a filter on the weekly overview screen
	When I open the daily overview screen
	Then I see the filtered list of employees using the filter from the daily overview screen

