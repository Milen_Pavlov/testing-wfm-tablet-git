// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.0.0.0
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace WFM.Features.ShowManualEditShift
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Indicate where shifts have been created / edited manually")]
    public partial class IndicateWhereShiftsHaveBeenCreatedEditedManuallyFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "ShowManualEditShift.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Indicate where shifts have been created / edited manually", "In order easily identify shifts that have had some human intervention\nAs a manager\nI want to be able to differentiate between manually create or edited shifts and auto scheduled shifts", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("See edited shifts in Schedule")]
        [NUnit.Framework.CategoryAttribute("iosmanual")]
        [NUnit.Framework.CategoryAttribute("tabletmanual")]
        public virtual void SeeEditedShiftsInSchedule()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("See edited shifts in Schedule", new string[]
                {
                        "iosmanual",
                        "tabletmanual"});
#line 7
this.ScenarioSetup(scenarioInfo);
#line 8
 testRunner.Given("I am viewing the schedule", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 9
 testRunner.And("manually edited shifts exist", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 10
 testRunner.And("automatic shifts exist", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 11
 testRunner.When("I review the shifts", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 12
 testRunner.Then("the manually edited shifts will have an asterix next to the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 13
 testRunner.And("the automatic shifts will not have an asterix next the the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Edit automatically created shifts in Schedule")]
        [NUnit.Framework.CategoryAttribute("iosmanual")]
        [NUnit.Framework.CategoryAttribute("tabletmanual")]
        public virtual void EditAutomaticallyCreatedShiftsInSchedule()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Edit automatically created shifts in Schedule", new string[]
                {
                        "iosmanual",
                        "tabletmanual"});
#line 16
this.ScenarioSetup(scenarioInfo);
#line 17
 testRunner.Given("I am viewing the schedule", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 18
 testRunner.And("I have some automatically created shifts", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 19
 testRunner.When("I edit a shift", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 20
 testRunner.Then("the edited shift will have an asterix next to the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Create shift in Schedule")]
        [NUnit.Framework.CategoryAttribute("iosmanual")]
        [NUnit.Framework.CategoryAttribute("tabletmanual")]
        public virtual void CreateShiftInSchedule()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Create shift in Schedule", new string[]
                {
                        "iosmanual",
                        "tabletmanual"});
#line 23
this.ScenarioSetup(scenarioInfo);
#line 24
 testRunner.Given("I am viewing the schedule", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 25
 testRunner.When("I create a shift", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 26
 testRunner.Then("the created shift will have an asterix next to the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Modify manually edited shift in Schedule")]
        [NUnit.Framework.CategoryAttribute("iosmanual")]
        [NUnit.Framework.CategoryAttribute("tabletmanual")]
        public virtual void ModifyManuallyEditedShiftInSchedule()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Modify manually edited shift in Schedule", new string[]
                {
                        "iosmanual",
                        "tabletmanual"});
#line 29
this.ScenarioSetup(scenarioInfo);
#line 30
 testRunner.Given("I am viewing the schedule", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 31
 testRunner.And("I have some manually edited shifts", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
 testRunner.When("I edit a manual shift", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 33
 testRunner.Then("the manually edited shift will still have an asterix next to the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Discard changes to manual shifts in Schedule")]
        [NUnit.Framework.CategoryAttribute("iosmanual")]
        [NUnit.Framework.CategoryAttribute("tabletmanual")]
        public virtual void DiscardChangesToManualShiftsInSchedule()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Discard changes to manual shifts in Schedule", new string[]
                {
                        "iosmanual",
                        "tabletmanual"});
#line 36
this.ScenarioSetup(scenarioInfo);
#line 37
 testRunner.Given("I am viewing the schedule", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 38
 testRunner.And("I have some manually edited shifts", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 39
 testRunner.When("I edit a manual shift", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 40
 testRunner.And("I discard changes", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 41
 testRunner.Then("the manually edited shift will still have an asterix next to the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Discard changes to automatic shifts in Schedule")]
        [NUnit.Framework.CategoryAttribute("iosmanual")]
        [NUnit.Framework.CategoryAttribute("tabletmanual")]
        public virtual void DiscardChangesToAutomaticShiftsInSchedule()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Discard changes to automatic shifts in Schedule", new string[]
                {
                        "iosmanual",
                        "tabletmanual"});
#line 44
this.ScenarioSetup(scenarioInfo);
#line 45
 testRunner.Given("I am viewing the schedule", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 46
 testRunner.And("I have some automatically created shifts", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 47
 testRunner.When("I edit an automatic shift", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 48
 testRunner.And("I discard changes", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 49
 testRunner.Then("the automatic shift will not have an asterix next to the job description", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
