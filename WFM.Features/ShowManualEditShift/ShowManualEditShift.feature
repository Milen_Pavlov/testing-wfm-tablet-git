﻿Feature: Indicate where shifts have been created / edited manually
	In order easily identify shifts that have had some human intervention
	As a manager
	I want to be able to differentiate between manually create or edited shifts and auto scheduled shifts
	
@iosmanual @tabletmanual
Scenario: See edited shifts in Schedule
	Given I am viewing the schedule
	And manually edited shifts exist
	And automatic shifts exist
	When I review the shifts
	Then the manually edited shifts will have an asterix next to the job description
	And the automatic shifts will not have an asterix next the the job description

@iosmanual @tabletmanual
Scenario: Edit automatically created shifts in Schedule
	Given I am viewing the schedule
	And I have some automatically created shifts
	When I edit a shift
	Then the edited shift will have an asterix next to the job description

@iosmanual @tabletmanual
Scenario: Create shift in Schedule
	Given I am viewing the schedule
	When I create a shift 
	Then the created shift will have an asterix next to the job description

@iosmanual @tabletmanual
Scenario: Modify manually edited shift in Schedule
	Given I am viewing the schedule
	And I have some manually edited shifts
	When I edit a manual shift
	Then the manually edited shift will still have an asterix next to the job description

@iosmanual @tabletmanual
Scenario: Discard changes to manual shifts in Schedule
	Given I am viewing the schedule
	And I have some manually edited shifts
	When I edit a manual shift
	And I discard changes
	Then the manually edited shift will still have an asterix next to the job description

@iosmanual @tabletmanual
Scenario: Discard changes to automatic shifts in Schedule
	Given I am viewing the schedule
	And I have some automatically created shifts
	When I edit an automatic shift
	And I discard changes
	Then the automatic shift will not have an asterix next to the job description

