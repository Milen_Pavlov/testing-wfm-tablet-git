﻿Feature: The selected store name will be displayed in the navigation bar of all screens on the left hand side (to the right of the burger menu)
	In order to make sure the user knows the current store
	As a manager
	I want to be able to know my current store
	
@manualios
Scenario: As a manager I know my current store on SiteMgr
	Given I am a manager
	When I am viewing a screen in the app
	Then My current store is displayed in the navigation bar to the right of the burger menu


@manualios
Scenario: As a manager when I change store my current store is updated
	Given I am a manager
	When I change store via the drawer menu
	Then My current store displayed in the navigation bar updates