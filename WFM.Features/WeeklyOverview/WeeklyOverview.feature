﻿Feature: As a manager I want a weekly overview screen that provides me a quick view of the following by week: exceptions, pay summary, timecard statement so that i can manage my weekly t&a activities on the tablet app without needing to use site manager
	
@iostabletmanual
Scenario: As a maanger I am able to navigate from the drawer to the weekly overview screen.
	Given I am running the app
	And I open the drawer menu 
	When I press the Weekly Overview button
	Then I am taken to the Weekly Overview screen.

@iostabletmanual
Scenario Outline: As a manager I can see the weekly overview screen
	Given I am on the Week Overview screen for the first time
	Then I will see a <tab> tab
	And Exceptions is selected by default

	Examples:
	|tab             |
	|Exceptions      |
	|Timecard Summary|
	|Pay Summary     |

@iostabletmanual
Scenario Outline: As a manager I can skip backward to see the previous weeks weekly overview information.
	Given I am on the Week Overview screen
	And I have the <tab> tab selected
	When I press the previous week button
	Then I will see information specific to the <tab> tab for that week

	Examples:
	|tab             |
	|Exceptions      |
	|Timecard Summary|
	|Pay Summary     |

@iostabletmanual
Scenario Outline: As a manager I can skip forward to see the next weeks weekly overview information.
	Given I am on the Week Overview screen
	And I have the <tab> tab selected
	When I press the next week button
	Then I will see information specific to the <tab> tab for that week

	Examples:
	|tab             |
	|Exceptions      |
	|Timecard Summary|
	|Pay Summary     |

@iostabletmanual
Scenario Outline: As a manager I can skip between tabs and the currently selected date wont change.
	Given I am on the Week Overview screen
	And I have the <tab> tab selected
	When I press a differ <tab>
	Then I will see information specific to the <tab> tab that I selected and the selected week will remain as before

	Examples:
	|tab             |
	|Exceptions      |
	|Timecard Summary|
	|Pay Summary     |

@iostabletmanual
Scenario Outline: As a manager on the weekly exceptions summary screen I am able to review information about my direct reports
	Given I see the Weekly Overview screen
	When I have the Execeptions tab selected
	Then I should see a list of employees containing details about <data>

	Examples:
	|data           |
	|Employee name  |
	|Job name       |
	|Shift start    |
	|Shift end      |
	|Duration       |
	|Exception      | 

@iostabletmanual
Scenario Outline: As a manager on the weekly timecard summary screen I am able to review information about my direct reports
	Given I see the Weekly Overview screen
	When I have the timecard summary tab selected
	Then I should see a list of employees containing details about <data>
	And I see totals for each <totals> column

	Examples:
	|data           |totals         |
	|employee name  |scheduled hours|
	|scheduled hours|net hours      |
	|net hours      |adjustments    |
	|adjustments    |other hours    |
	|other hours    |total hours    |
	|total hours    |exceptions     |
	|exceptions     |               |

@iostabletmanual
Scenario Outline: As a manager on the weekly pay summary screen I am able to review information about my direct reports
	Given I see the Weekly Overview screen
	When I have the pay summary tab selected
	Then I should see a list of employees containing details about <data>
	And I see totals for each <totals> column

	Examples:
	|data         |totals |
	|employee name|hours  |
	|date         |amount |
	|home site    |       |
	|worked site  |       |
	|hours        |       |
	|amount       |       |
